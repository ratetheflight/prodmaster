/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SocialSetupController {
    global SocialSetupController() {

    }
    @RemoteAction
    global static void activateAutoSurvey(String socialAccountId, String surveyType, String daysAfter, String scope) {

    }
    @RemoteAction
    global static void deactivateAutoSurvey(String socialAccountId) {

    }
    @RemoteAction
    global static List<scs_twtSurveys.SocialSetupController.socialAccountCls> getExternalSocialAccounts() {
        return null;
    }
    @RemoteAction
    global static List<Document> getLogFiles() {
        return null;
    }
    @RemoteAction
    global static String revokeAccount(String socialAccountId) {
        return null;
    }
    @RemoteAction
    global static String validateAccount(String socialAccountId) {
        return null;
    }
global class socialAccountCls {
    global socialAccountCls() {

    }
}
}
