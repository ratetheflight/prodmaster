/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/FeedbackSurveySite/*')
global class TwitterSurveyController {
    global static String JSONString {
        get;
        set;
    }
    global TwitterSurveyController() {

    }
    global static scs_twtSurveys.TwitterSurveyController.FeedbackObject parseJSONString(String jsonStr) {
        return null;
    }
    global static Boolean personAccountsEnabled() {
        return null;
    }
    @HttpPost
    global static void updateFeedbackRecord() {

    }
global class FeedbackObject {
}
global class FeedbackResponse {
}
}
