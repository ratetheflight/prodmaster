/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TwitterSurveyInboundSocialPostHandler implements Social.InboundSocialPostHandler {
    global TwitterSurveyInboundSocialPostHandler() {

    }
    global virtual SObject createPersonaParent(SocialPersona persona) {
        return null;
    }
    global virtual String getDefaultAccountId() {
        return null;
    }
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        return null;
    }
    global String getPersonaFirstName(SocialPersona persona) {
        return null;
    }
    global String getPersonaLastName(SocialPersona persona) {
        return null;
    }
    global virtual Set<String> getPostTagsThatCreateCase() {
        return null;
    }
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, SocialPersona persona, Map<String,Object> rawData) {
        return null;
    }
}
