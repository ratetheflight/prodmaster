<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Customer_s_Average_CSAT_Score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Customer&apos;s Average CSAT Score</label>
        <precision>3</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_s_Average_NPS_Score__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Customer&apos;s Average NPS Score</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_s_Loyalty_Value__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Calculated from the average of the scores of the NPS surveys for the user. 0-6 Detractor, 7-8 Neutral and 9-10 Promoter.</inlineHelpText>
        <label>Customer&apos;s Loyalty Value</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>N/A</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Detractor</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Passive</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Promoter</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Customer_s_Loyalty__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(TEXT(Customer_s_Loyalty_Value__c) = &apos;Detractor&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyalty/detractor_face.png&quot;, &apos;No image&apos;, 25,25),
IF(TEXT(Customer_s_Loyalty_Value__c) = &apos;Passive&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyalty/neutral_face.png&quot;, &apos;No image&apos;, 25, 25),
IF(TEXT(Customer_s_Loyalty_Value__c) = &apos;Promoter&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyalty/promoter_face.png&quot;, &apos;No image&apos;, 25, 25), &apos;&apos;)))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Customer&apos;s Loyalty</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_s_Temper_Value__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Customer&apos;s Temper Value</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>N/A</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Very Dis­sat­is­fied</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Somewhat Dis­sat­is­fied</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Neutral</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Somewhat Sat­is­fied</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Very Sat­is­fied</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Customer_s_Temper__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(TEXT(Customer_s_Temper_Value__c) = &apos;Very Dissatisfied&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyaltyCSAT/CSAT_VD.png&quot;, &apos;No image&apos;, 25,25),
IF(TEXT(Customer_s_Temper_Value__c) = &apos;Somewhat Dissatisfied&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyaltyCSAT/CSAT_SD.png&quot;, &apos;No image&apos;, 25,25),
IF(TEXT(Customer_s_Temper_Value__c) = &apos;Neutral&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyaltyCSAT/CSAT_N.png&quot;, &apos;No image&apos;, 25,25),
IF(TEXT(Customer_s_Temper_Value__c) = &apos;Somewhat Satisfied&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyaltyCSAT/CSAT_SS.png&quot;, &apos;No image&apos;, 25, 25),
IF(TEXT(Customer_s_Temper_Value__c) = &apos;Very Satisfied&apos;, 
IMAGE(&quot;/resource/scs_twtSurveys__facesLoyaltyCSAT/CSAT_VS.png&quot;, &apos;No image&apos;, 25, 25), &apos;&apos;)))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Customer&apos;s Temper</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
</CustomObject>
