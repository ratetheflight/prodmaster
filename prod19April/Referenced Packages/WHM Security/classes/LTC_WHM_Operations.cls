/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class LTC_WHM_Operations {
    global LTC_WHM_Operations() {

    }
    global static String addSecrets(String name, String KEY, String IV) {
        return null;
    }
    global static String addTravelDbCreds(String name, String userName, String password) {
        return null;
    }
    global static String clearAllData() {
        return null;
    }
    global static String decryptData(String data, Integer intgr) {
        return null;
    }
    global static String encodeTravelDbCreds() {
        return null;
    }
    global static String encryptData(String data) {
        return null;
    }
    global static String getTravelDbCreds() {
        return null;
    }
    global static Boolean isKEYIVCorrect(String name, String KEY, String IV) {
        return null;
    }
    global static Boolean isTravelDbCredsValid(String name, String userName, String password) {
        return null;
    }
    global static Boolean resetKEYIV(String name, String KEY, String IV) {
        return null;
    }
    global static Boolean resetTravelDbCreds(String name, String userName, String password) {
        return null;
    }
    global static List<String> seeAllData() {
        return null;
    }
}
