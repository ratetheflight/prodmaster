/*************************************************************************************************
* File Name     :   AFKLM_AccountMergeBatch
* Description   :   This batch class is used to merge the accounts identified as duplicate using dupecatcher
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     15/03/2016      Nagavi         Created the class
****************************************************************************************************/
global class AFKLM_AccountMergeBatch implements Database.Batchable<sObject>{
    
    //query string 
    global final string query;
    global Map<Id,SFSSDupeCatcher__Scenario__c> criteriaMap;
    global List<Id> criteriaIds;
    //constructor 
    global AFKLM_AccountMergeBatch(){
        
        try{
            Map<Id,SFSSDupeCatcher__Scenario__c> criteriaMap=new Map<ID, SFSSDupeCatcher__Scenario__c>([SELECT Id, SFSSDupeCatcher__Deployed__c FROM SFSSDupeCatcher__Scenario__c where SFSSDupeCatcher__Deployed__c=true]);
            
            if(!criteriaMap.IsEmpty()){
                
                criteriaIds= new List<Id>();
                for(Id criteriaId:criteriaMap.keySet())
                    criteriaIds.add(criteriaId);
            
                System.Debug('**'+criteriaMap);
                string status='Manual';
                              
               query='SELECT id,SFSSDupeCatcher__Scenario__c,Status__c FROM SFSSDupeCatcher__Duplicate_Warning__c WHERE Status__c !=\''+Status+'\' '+'AND SFSSDupeCatcher__Scenario__c IN :criteriaIds';
                                 
               system.debug('query is'+query);
            }
            else
                return;
        }
        catch(Exception e){
            System.Debug('Exception occurred !!'+e);
        }
    }
    
        
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SFSSDupeCatcher__Duplicate_Warning__c> scope){
        
        List<Id> warningIds=new List<Id>();
        List<SFSSDupeCatcher__Potential_Duplicate__c> duplicatesList=new List<SFSSDupeCatcher__Potential_Duplicate__c>();
        
        Map<Id,SFSSDupeCatcher__Duplicate_Warning__c> alertMap=new Map<Id,SFSSDupeCatcher__Duplicate_Warning__c>();
        
        
        
        system.debug('**inside'+scope);       
        //try{
            if(!scope.isEmpty()){
                for(SFSSDupeCatcher__Duplicate_Warning__c warning:scope){
                    warningIds.add(warning.Id);
                    alertMap.put(warning.Id,warning);
                    
                }
                system.debug('**alertMap'+alertMap);
                //Query the duplicates associated with each alert
                if(!warningIds.isEmpty()){
                    duplicatesList=[select id,SFSSDupeCatcher__Duplicate_Warning__c,SFSSDupeCatcher__Warning_Display__c,SFSSDupeCatcher__Account__c from SFSSDupeCatcher__Potential_Duplicate__c where SFSSDupeCatcher__Duplicate_Warning__c IN :warningIds];
                }
                
                if(!duplicatesList.isEmpty()){
                    AFKLM_SCS_AccountMergeHandler.doMerge(duplicatesList,alertMap);
                }
                
            }
        //}
        /*catch(exception e){
            System.debug('Exception Occured:'+e);
        }*/
    }
    
    global void finish(Database.BatchableContext BC){
        //Query for all alerts whose accounts have been merged.
        List<SFSSDupeCatcher__Duplicate_Warning__c> alertList=new List<SFSSDupeCatcher__Duplicate_Warning__c>();
        List<Id> alertIds=new List<Id>();
        alertList=[SELECT id,SFSSDupeCatcher__Scenario__c,Status__c,No_Of_Duplicates__c FROM SFSSDupeCatcher__Duplicate_Warning__c WHERE Status__c !='Manual' AND No_Of_Duplicates__c <=1];
        if(!alertList.IsEmpty()){
            delete alertList;
        }
    }
 }