/*************************************************************************************************
* File Name     :   AFKLM_AccountMergeBatchScheduler 
* Description   :   Scheduler to call the batch class used to merge the accounts identified as duplicate using dupecatcher
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     19/04/2016      Nagavi         Created the class
****************************************************************************************************/
global class AFKLM_AccountMergeBatchScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    AFKLM_AccountMergeBatch accountMerge = new AFKLM_AccountMergeBatch();
    Database.executeBatch(accountMerge , 1);
  }
}