/* 
*******************************************************************************************************************************************
* File Name     :   AFKLM_AccountMergeTest
* Description   :   Class to test Account Merge Batch 
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    19/04/2016    Nagavi        Created the class.
******************************************************************************************************************************************* 
*/
@isTest
private class AFKLM_AccountMergeTest{
    
    static testmethod void accountMergeTest() {
        SFSSDupeCatcher__Scenario__c scenario1= new SFSSDupeCatcher__Scenario__c();
        scenario1.Name='BFNumber';
        scenario1.SFSSDupeCatcher__Scenario_Type__c='Account (Person)';
        scenario1.SFSSDupeCatcher__Bypass_Security__c=true;
        scenario1.SFSSDupeCatcher__Deployed__c=true;
        scenario1.SFSSDupeCatcher__Match_On_Insert_Action__c='Report Duplicate';
        scenario1.SFSSDupeCatcher__Match_On_Update_Action__c='Report Duplicate';
        insert scenario1;
        
        SFSSDupeCatcher__Scenario_Rule__c scenarioRule1=new SFSSDupeCatcher__Scenario_Rule__c();
        scenarioRule1.SFSSDupeCatcher__Scenario__c=scenario1.Id;
        scenarioRule1.SFSSDupeCatcher__Match_field__c='flying_blue_number__c';
        scenarioRule1.SFSSDupeCatcher__Target_Match_Field__c='flying_blue_number__c';
        scenarioRule1.SFSSDupeCatcher__Scenario_Type__c='Account (Person)';
        insert scenarioRule1;
        
        Account acc=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc.LastName='Test Account 1';
        acc.flying_blue_number__c='6789';
        insert acc;
        
        Account acc2=new Account();
        //acc2.recordtypeId='01220000000AFcl';
        acc2.LastName='Test Account 1';
        acc2.flying_blue_number__c='6789';
        acc2.Website='test@abc.com';
        acc2.sf4twitter__Twitter_Username__pc='Test Tweet';
        acc2.Facebook_Username__pc='FB_789';
        acc2.sf4twitter__Fcbk_Username__pc='TestFBUserName';
        acc2.sf4twitter__Fcbk_User_Id__pc='TestFBUserId';
        acc2.WeChat_User_ID__c='TestWechat';
        acc2.WeChat_Username__c='TestWcUser';
        insert acc2;
        
        Account acc3=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc3.LastName='Test Account 3';
        acc3.flying_blue_number__c='6789';
        insert acc3;
        
              
        List<SocialPersona> victimPersonas2=new List<SocialPersona>();
        victimPersonas2=AFKLM_TestDataFactory.insertpersona(3,acc.Id);
        insert victimPersonas2;
        
        List<SocialPersona> victimPersonas=new List<SocialPersona>();
        victimPersonas=AFKLM_TestDataFactory.insertpersona(2,acc2.Id);
        insert victimPersonas;
        
        List<SocialPersona> parentPersonas=new List<SocialPersona>();
        parentPersonas=AFKLM_TestDataFactory.insertpersona(4,acc3.Id);
        insert parentPersonas;
        
        Test.startTest();       
        Database.executeBatch(new AFKLM_AccountMergeBatch(), 1); 
               
        Test.stopTest();
        
        AFKLM_AccountMergeBatchScheduler accMerge = new AFKLM_AccountMergeBatchScheduler ();
        accMerge.execute(null);
    }
    
     static testmethod void accountMergeTestwithoutPersona() {
        SFSSDupeCatcher__Scenario__c scenario1= new SFSSDupeCatcher__Scenario__c();
        scenario1.Name='BFNumber';
        scenario1.SFSSDupeCatcher__Scenario_Type__c='Account (Person)';
        scenario1.SFSSDupeCatcher__Bypass_Security__c=true;
        scenario1.SFSSDupeCatcher__Deployed__c=true;
        scenario1.SFSSDupeCatcher__Match_On_Insert_Action__c='Report Duplicate';
        scenario1.SFSSDupeCatcher__Match_On_Update_Action__c='Report Duplicate';
        insert scenario1;
        
        SFSSDupeCatcher__Scenario_Rule__c scenarioRule1=new SFSSDupeCatcher__Scenario_Rule__c();
        scenarioRule1.SFSSDupeCatcher__Scenario__c=scenario1.Id;
        scenarioRule1.SFSSDupeCatcher__Match_field__c='flying_blue_number__c';
        scenarioRule1.SFSSDupeCatcher__Target_Match_Field__c='flying_blue_number__c';
        scenarioRule1.SFSSDupeCatcher__Scenario_Type__c='Account (Person)';
        insert scenarioRule1;
        
        Account acc=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc.LastName='Test Account 1';
        acc.flying_blue_number__c='6789';
        acc.Website='Testacc1@abc.com';
        acc.sf4twitter__Twitter_Username__pc='Tweetacc1';
        acc.Facebook_Username__pc='TestFB1';
        insert acc;
        
        Account acc2=new Account();
        //acc2.recordtypeId='01220000000AFcl';
        acc2.LastName='Test Account 1';
        acc2.flying_blue_number__c='6789';
        insert acc2;
        
        Account acc3=new Account();
        //acc2.recordtypeId='01220000000AFcl';
        acc3.LastName='Test Account 1';
        acc3.flying_blue_number__c='6789';
        acc3.VIP__pc=true;
        insert acc3;
        
        Account acc4=new Account();
        //acc2.recordtypeId='01220000000AFcl';
        acc4.LastName='Test Account 1';
        acc4.flying_blue_number__c='6789';
        acc4.WhatsApp_Pilot__c=true;
        insert acc4;
        
        Account acc5=new Account();
        //acc2.recordtypeId='01220000000AFcl';
        acc5.LastName='Test Account 1';
        acc5.flying_blue_number__c='6789';
        acc5.Restricted_Account__c=true;
        insert acc5;
        
        Account acc6=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc6.LastName='Test Account 6';
        acc6.flying_blue_number__c='6789';
        acc6.Website='Testacc6@abc.com';
        insert acc6;
        
        Account acc7=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc7.LastName='Test Account 7';
        acc7.flying_blue_number__c='6789';
        acc7.sf4twitter__Twitter_Username__pc='Tweetacc7';
        insert acc7;
        
        Account acc8=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc8.LastName='Test Account 8';
        acc8.flying_blue_number__c='6789';
        acc8.Influencer__c=true;
        insert acc8;
        
        Account acc9=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc9.LastName='Test Account 9';
        acc9.flying_blue_number__c='6789';
        acc.Facebook_Username__pc='TestFB9';
        insert acc9;
               
        Test.startTest();       
        Database.executeBatch(new AFKLM_AccountMergeBatch(), 1);     
        Test.stopTest();
        
    }
    
    static testmethod void pseudoUpdateAccount() {
        
        List<Account> accountList=new List<Account>();
        Account acc=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc.LastName='Test Account 1';
        acc.flying_blue_number__c='6789';
        accountList.add(acc);
        
        Account acc2=new Account();
        //acc2.recordtypeId='01220000000AFcl';
        acc2.LastName='Test Account 1';
        acc2.flying_blue_number__c='6789';
        acc2.Website='test@abc.com';
        acc2.sf4twitter__Twitter_Username__pc='Test Tweet';
        acc2.Facebook_Username__pc='FB_789';
        acc2.sf4twitter__Fcbk_Username__pc='TestFBUserName';
        acc2.sf4twitter__Fcbk_User_Id__pc='TestFBUserId';
        acc2.WeChat_User_ID__c='TestWechat';
        acc2.WeChat_Username__c='TestWcUser';
        accountList.add(acc2);
        
        Account acc3=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc3.LastName='Test Account 3';
        acc3.flying_blue_number__c='6789';
        accountList.add(acc3);
               
        Test.startTest(); 
        insert accountList;   
        //Database.executeBatch(new AFKLM_AccountMergeBatch(), 1); 
        Database.executeBatch(new AFKLM_SCS_PseudoAccountUpdateBatch(3), 1);   
        
        Test.stopTest();
        
    }
 }