/*************************************************************************************************
* File Name     :   AFKLM_AccountTriggerHandler 
* Description   :   Helper class for the Account trigger
* @author       :   Prasanna
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     21/10/2016      Prasanna         Created the class
* 1.1     03/04/2017      Prasanna         Updated klout score basic condition to 50 for influencer score 2
****************************************************************************************************/
Public class AFKLM_AccountTriggerHandler {
    
   
    public void onBeforeInsertUpdate(List<Account> objectsToInsert,Map<Id, Account> oldaccmap){
    
    
     List<Account> Listofaccs=new List<Account>();
    //Added by Prasanna
for(Account acc: objectsToInsert){
    if(acc.Bypass_Klout_Update__c!=TRUE && acc.klout_score__c !=oldaccmap.get(acc.id).klout_score__c){
        if(acc.klout_score__c >=0 && acc.klout_score__c <=49){
            acc.influencer_score_new__c='1';
            Listofaccs.add(acc);
        }
        else if (acc.klout_score__c >=50 && acc.klout_score__c <=59){
        acc.influencer_score_new__c='2';   
        Listofaccs.add(acc); 
        }
        else if (acc.klout_score__c >=60 && acc.klout_score__c <=69){
        acc.influencer_score_new__c='3';
        Listofaccs.add(acc);    
        }
        else if (acc.klout_score__c >=70 && acc.klout_score__c <=100){
        acc.influencer_score_new__c='4';
        Listofaccs.add(acc);    
        }
        
    }
    
       
}  
try{
            if(Listofaccs.size() > 0)
            update Listofaccs;
         }
         catch(Exception e){
             System.debug('Exception happened::'+e.getmessage());
         }
    
    }
    
    }