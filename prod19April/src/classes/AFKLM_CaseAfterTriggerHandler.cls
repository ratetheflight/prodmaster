/*************************************************************************************************
* File Name     :   AFKLM_CaseAfterTriggerHandler
* Description   :   Helper class for the case trigger
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     10/02/2016      Nagavi         Created the class
* 1.1     15/02/2016      Manuel Conde   Added call too Heroku 
* 1.2     09/05/2016      Nagavi         Added logic to bypass campaign cases -  ST-002977
* 1.3     05/07/2016      Narmatha       Added to fill the Return Flight information for iPad on Borad record type - ST-132
* 1.4     09/08/2016      Sai            Modified the condition to check for Users with more than the specified limit of cases. JIRA ST-599
* 1.5     05/10/2016      Narmatha       Added the logic to create a IPOb Msg automatically based on Checkbox field ST - 935
* 1.6     04/01/2017      Sai            Modified if conidtion on closed cases.JIRA ST-1509
* 1.7     06/01/2017      Pushkar        Auto Account/Contact creation for webshop cases - ST-004531
* 1.8     18/01/2017      Pushkar        Salutation update for webshop cases - ST-004933
* 1.9     11/04/2017      Pushkar        Auto Account/Contact creation for webchat cases - ST-2115
****************************************************************************************************/
Public class AFKLM_CaseAfterTriggerHandler{
    
    //Boolean to prevent recursion
    public static boolean onAfterInsert_FirstRun = true;
    public static boolean onAfterUpdate_FirstRun = true;
    public static boolean onAfterDelete_FirstRun= true;
    List<SocialPost> socpostlist=new List<SocialPost>();
   // List<Case> listcase=new List <Case>();
    //Method for after insert event
    public void onAfterInsert(List<Case> objectsToInsert)
    {
        if(onAfterInsert_FirstRun)
        {
            // PREVENT RECURSION
            onAfterInsert_FirstRun = false;
            
            /******Related to task - One agent can have only on case with In Progress status ***************/
            
            AFKLM_RecordTypeIds__c servicingRecType=AFKLM_RecordTypeIds__c.getValues('Servicing');
            AFKLM_Disable_Trigger__c checkUser=AFKLM_Disable_Trigger__c.getInstance();
                                
            List<Case> casesToBeChecked=new List<Case>(); 
            if(servicingRecType !=null && checkUser!=null && checkUser.Fire_Case_In_Progress__c){          
                for(Case cs:objectsToInsert) {
                    if(cs.RecordTypeId==servicingRecType.RecordType_Id__c && cs.Status==Label.AFKLM_CaseInProgress && cs.Is_Campaign__c!=true){ //Nagavi : Added logic to bypass campaign cases :ST-002977 on 09/05/2016
                        casesToBeChecked.add(cs);    
                    }
                }
            }
            
            
            if(!casesToBeChecked.isEmpty()){
                User currentUser= null;
                List<user> userList = [SELECT Id,Last_Case_In_Progress__c FROM User WHERE Id = :UserInfo.getUserId()]; 
                    
                if(!userList.isEmpty())
                    currentUser=userList[0]; 
                
                if(currentUser!=null)
                    checkCaseStatusOnInsertUpdate(casesToBeChecked,currentUser);
            }
            /*******************************************************************************/
            /******Related to fill the Return Flight Details for iPad on Board Cases - ST-132 and to create iPOB comment automatically ST-935***************/
            
            AFKLM_RecordTypeIds__c iPadRecType=AFKLM_RecordTypeIds__c.getValues('iPad on Board Case');
            Set<ID> caseidSet=new Set<ID>();
            if(iPadRecType!=null){
                for(Case cs:objectsToInsert){
                    if(cs.recordtypeid ==iPadRecType.RecordType_Id__c && cs.PNR__c!='' && cs.accountID!=null)
                        caseidSet.add(cs.id);
                }
            }
            system.debug('**'+Limits.getCallouts());
            if(caseidSet.size()>0 && Limits.getCallouts()<=Limits.getLimitCallouts()){
                try{
                    AFKLM_iPadReturnFlightHandler.returnFlightInfo(caseidSet);
                }
                catch(Exception e){
                    system.debug('Exception occured :'+e);
                }
            }
            
            /********************************************************************************/
            Id webshopRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Webshop').getRecordTypeId();
            System.debug('webshopRTId'+webshopRTId);
            Set<Case> caseForPACreationSet=new Set<Case>();
            
            if(webshopRTId != null){
            
                for(Case cs:objectsToInsert){
                        if(cs.RecordtypeId == webshopRTId && cs.accountID == null)
                            caseForPACreationSet.add(cs);
                }
                
                if(caseForPACreationSet.size() > 0){
                        try{
                            AFKLM_PersonAccountCreation.createMethod(caseForPACreationSet);
                        }
                        catch(Exception e){
                            System.debug('Exception occured :'+e);
                        }               
                }
            
            }
            
            /********************************************************************************/
            Id webchatRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Webchat').getRecordTypeId();
            Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            //System.debug('webshopRTId'+webshopRTId);
            Set<Case> webchatcaseForPACreationSet=new Set<Case>();
            List<Account> anymAccUpd = new List<Account>();
            List<Account> newAnymAcc = new List<Account>();
            List<Case> caseUpdList = new List<Case>();
            Anonymous_Account__c anx = Anonymous_Account__c.getvalues('Anonymous');
            //Account acch=[select id,firstname,lastname from account where id=:anx.accountid__c limit 1];
            Date mydate=Date.today();
            
            if(webchatRTId != null){
            
                for(Case cs:objectsToInsert){
                        if(cs.RecordtypeId == webchatRTId && cs.accountID == null && ((cs.Visitor_email__c != null && cs.Visitor_email__c != '') || (cs.Visitor_Flying_Blue_number__c != null && cs.Visitor_Flying_Blue_number__c != ''))){
                            webchatcaseForPACreationSet.add(cs);
                        }else if(cs.accountID == null && cs.RecordtypeId == webchatRTId){
                          if(!Test.isRunningTest()){

                            if(anx.transcriptcount__c > 499){
                                Account acch = new Account(id=anx.accountid__c);
                                acch.firstname='AnonymousAccount';
                                acch.lastname='History'+mydate;
                                anymAccUpd.add(acch);
                                Account acct=new Account();
                                acct.firstname='Anonymous';
                                acct.lastname='Account';
                                acct.recordtypeid=AccRecordTypeId;
                                newAnymAcc.add(acct);
                                
                                //update anx;
                            }
                            else{
                                anx.transcriptcount__c=anx.transcriptcount__c+1;
                            }
                            Case caseInst = new Case(id=cs.id);
                            caseInst.accountid=anx.accountid__c;
                            caseUpdList.add(caseInst);
                        }
                        }
                }
                
                if(webchatcaseForPACreationSet.size() > 0){
                        try{
                            AFKLM_PersonAccountCreation.createMethod(webchatcaseForPACreationSet);
                        }
                        catch(Exception e){
                            System.debug('Exception occured :'+e);
                        }               
                }
                
                if(anymAccUpd.size() > 0)
                    update anymAccUpd;
                if(newAnymAcc.size() > 0){
                    insert newAnymAcc;
                    anx.accountid__c=newAnymAcc[0].id;
                    anx.transcriptcount__c=1;
                }   
                    
                if(caseUpdList.size() > 0)
                    update caseUpdList;
                if(!Test.isRunningTest()){    
                    update anx;
                }
            
            }


        }
    }
    
    //Method for after update event
    public void onAfterUpdate(List<Case> oldCases,List<Case> objectsToUpdate,Map<Id,Case> oldCasesMap,Map<Id,Case> objectsToUpdateMap)
    {
   // system.debug('Prasanna:caseonafterupdate');
   
        if(onAfterUpdate_FirstRun)
        {
            // PREVENT RECURSION
            onAfterUpdate_FirstRun = false;
            
            /******Related to task - One agent can have only on case with In Progress status ***************/
            AFKLM_RecordTypeIds__c servicingRecType=AFKLM_RecordTypeIds__c.getValues('Servicing');
            AFKLM_Disable_Trigger__c checkUser=AFKLM_Disable_Trigger__c.getInstance();
            
               
            List<Case> casesToBeChecked=new List<Case>();
            List<Case> caseWithUserUpdate=new List<Case>();  
            
            Id webshopRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Webshop').getRecordTypeId();
            System.debug('webshopRTId'+webshopRTId);
            Set<Case> caseForPAUpdSet=new Set<Case>();
            
            if(webshopRTId != null){
            
                for(Case cs:objectsToUpdate){
              /*  system.debug('prasanna:firstpost'+cs.case_firstpost_type__c);
                if(cs.RecordTypeId==servicingRecType.RecordType_Id__c){
                socpostlist=[select id,MessageType,parentid from socialpost where parentid=:cs.id order by createddate asc limit 1];
                    if(socpostlist.size()>0){
                    cs.Case_FirstPost_Type__c=socpostlist[0].MessageType;
                   // update cs;
                   }
                    system.debug('Prasanna:socpostlist'+socpostlist);
                     system.debug('Prasanna:case'+cs.case_firstpost_type__c);
                } */
                        if(cs.RecordtypeId == webshopRTId && cs.accountID != null)
                            caseForPAUpdSet.add(cs);
                }
                
                if(caseForPAUpdSet.size() > 0){
                        try{
                            AFKLM_PersonAccountCreation.updateMethod(caseForPAUpdSet);
                        }
                        catch(Exception e){
                            System.debug('Exception occured :'+e);
                        }               
                }
            
            }
            
            if(servicingRecType !=null && checkUser!=null && checkUser.Fire_Case_In_Progress__c){       
                for(Case cs:objectsToUpdate) {
                    system.debug('Sai Test'+objectsToUpdateMap.get(cs.Id).Status);
                    system.debug('Sai Test'+oldCasesMap.get(cs.Id).Status);
                    system.debug('Sai Test'+objectsToUpdateMap.get(cs.Id).scs_twtSurveys__Auto_Survey_Job_Id__c) ;
                    system.debug('Sai Test'+objectsToUpdateMap.get(cs.Id).IsClosed);
                    if(cs.RecordTypeId==servicingRecType.RecordType_Id__c){ 
                        /*if(objectsToUpdateMap.get(cs.Id).Status!=oldCasesMap.get(cs.Id).Status && objectsToUpdateMap.get(cs.Id).Status==Label.AFKLM_CaseInProgress)
                            casesToBeChecked.add(cs);  
                        else if(objectsToUpdateMap.get(cs.Id).Status!=oldCasesMap.get(cs.Id).Status && objectsToUpdateMap.get(cs.Id).Status!=Label.AFKLM_CaseInProgress){
                            caseWithUserUpdate.add(cs);    
                        }*/
                        
                        //Nagavi : Added logic to bypass campaign cases :ST-002977 on 09/05/2016
                        
                        if(objectsToUpdateMap.get(cs.Id).Status!=oldCasesMap.get(cs.Id).Status && objectsToUpdateMap.get(cs.Id).Status==Label.AFKLM_CaseInProgress && objectsToUpdateMap.get(cs.Id).Is_Campaign__c!=true)
                            casesToBeChecked.add(cs);
                        else if(objectsToUpdateMap.get(cs.Id).Status==oldCasesMap.get(cs.Id).Status && objectsToUpdateMap.get(cs.Id).Status==Label.AFKLM_CaseInProgress){
                            if(objectsToUpdateMap.get(cs.Id).Is_Campaign__c!=oldCasesMap.get(cs.Id).Is_Campaign__c){
                                if(objectsToUpdateMap.get(cs.Id).Is_Campaign__c!=true){
                                    casesToBeChecked.add(cs);
                                }
                                else
                                    caseWithUserUpdate.add(cs);
                            }
                        }
                            
                        else if(objectsToUpdateMap.get(cs.Id).Status!=oldCasesMap.get(cs.Id).Status && objectsToUpdateMap.get(cs.Id).Status!=Label.AFKLM_CaseInProgress){
                            caseWithUserUpdate.add(cs);    
                        }  
                        //SAI: Added logic to close twitter survey cases. JIRA-1010
                        //start
                        //Modified the if condition. JIRA ST-1509.
                        else if(objectsToUpdateMap.get(cs.Id).IsClosed == True )
                        {
                            caseWithUserUpdate.add(cs);
                        }
                        //End.
                    }
                }
            }
            system.debug('**casesToBeChecked'+casesToBeChecked);
            system.debug('**caseWithUserUpdate'+caseWithUserUpdate);
            //Method Call to to check if the current user already has a case with In progress state on case Update
            if(!casesToBeChecked.isEmpty()){
                User currentUser= null;
                List<user> userList = [SELECT Id,Last_Case_In_Progress__c FROM User WHERE Id = :UserInfo.getUserId()]; 
                Boolean isUserUpdated=false;
    
                if(!userList.isEmpty())
                    currentUser=userList[0];
                
                if(currentUser!=null)
                    checkCaseStatusOnInsertUpdate(casesToBeChecked,currentUser);
            }
            
            //Method Call to clear the Last_Case_In_Progress__c field in User object
            if(!caseWithUserUpdate.isEmpty()){
                UpdateUserOnCaseUpdateDelete(caseWithUserUpdate);
            }
            /*******************************************************************************/   
        

            //Heroku
            //SCS_HerokuOutboundHandler herokuHandler = new SCS_HerokuOutboundHandler();
            //herokuHandler.sendCasesOnUpdateToHeroku(oldCases, objectsToUpdate, oldCasesMap, objectsToUpdateMap);
        }


    }
    
    //Method for after delete event
    public void onAfterDelete(List<Case> objectsToDelete)
    {
        if(onAfterDelete_FirstRun)
        {
            // PREVENT RECURSION
            onAfterDelete_FirstRun= false;
            
            /******Related to task - One agent can have only on case with In Progress status ***************/
            
            AFKLM_RecordTypeIds__c servicingRecType=AFKLM_RecordTypeIds__c.getValues('Servicing');
            
            AFKLM_Disable_Trigger__c checkUser=AFKLM_Disable_Trigger__c.getInstance();
            
                           
            List<Case> caseWithUserUpdate=new List<Case>();
            
            if(servicingRecType !=null && checkUser!=null && checkUser.Fire_Case_In_Progress__c){
                for(Case cs:objectsToDelete) {
                    //Check if the recordtype is of Servicing recordtype and its status is "In Progress"
                    if(cs.RecordTypeId==servicingRecType.RecordType_Id__c && cs.Status==Label.AFKLM_CaseInProgress){  
                        caseWithUserUpdate.add(cs);
                    }
                }
            }
            //Method Call to clear the Last_Case_In_Progress__c field in User object
            if(!caseWithUserUpdate.isEmpty()){
                UpdateUserOnCaseUpdateDelete(caseWithUserUpdate);
            }
           
        }
    }
    
    /**************** Independent Methods *****************************************************/
    //Method to check if the current user already has a case with In progress state on case insert/Update in After Event
    public static void checkCaseStatusOnInsertUpdate(List<Case> newCases,User currentUser){
    
        Boolean isUserUpdated=false;
        List<User> userUpdateList=new List<User>();
        Set<String> caseNumberSet=new Set<String>();
        Decimal noOfCases=0;
        
        AFKLM_SocialCustomerService__c noOfCase=AFKLM_SocialCustomerService__c.getValues('Max Number of Case In Progress');
                
        if(currentUser!=null && noOfCase!=null){
            noOfCases=noOfCase.Number_of_Case_in_Progress__c;
            
            if(currentUser.Last_Case_In_Progress__c!=null && currentUser.Last_Case_In_Progress__c!=''){
                caseNumberSet=AFKLM_CaseAfterTriggerHandler.getCaseNumbers(currentUser.Last_Case_In_Progress__c);
                
            }
            
            for(Case checkCase:newCases){
            //Changed the if condition to check for Users with more than the specified limit of cases
                if(caseNumberSet.size()>= noOfCases || (!caseNumberSet.contains(checkCase.casenumber) && caseNumberSet.size()>=noOfCases)){
                    system.debug('**inside error');
                    if(isUserUpdated){
                        update(currentUser);
                        system.debug('**currentUser'+currentUser);
                    }
                    
                    checkCase.addError(Label.AFKLM_Case_In_Progress_Error + currentUser.Last_Case_In_Progress__c); //need to move this to custom label
                }
                else if(!caseNumberSet.contains(checkCase.casenumber) && caseNumberSet.size()< noOfCases){
                    system.debug('**outside error');
                    if(currentUser.Last_Case_In_Progress__c == null || currentUser.Last_Case_In_Progress__c ==''){
                        currentUser.Last_Case_In_Progress__c=checkCase.casenumber;
                        caseNumberSet.add(checkCase.casenumber);
                        isUserUpdated=true;
                        system.debug('**isUserUpdated'+isUserUpdated);
                        system.debug('**caseNumberSet2'+caseNumberSet);
                    }
                    else {
                        currentUser.Last_Case_In_Progress__c=currentUser.Last_Case_In_Progress__c+','+checkCase.casenumber;
                        caseNumberSet.add(checkCase.casenumber);
                        isUserUpdated=true;
                        system.debug('**isUserUpdated'+isUserUpdated);
                        system.debug('**caseNumberSet3'+caseNumberSet);
                    }
                }
            }
                        
            if(isUserUpdated){
                update(currentUser);
                system.debug('**currentUser'+currentUser);
            }
        }
        
        
    
    }
        
    
    //Method to clear the Last_Case_In_Progress__c field in User object on Case Update/Delete in After Event
    Public static void UpdateUserOnCaseUpdateDelete(List<Case> caseWithUserUpdate){
        String[] caseFilters = new String[]{};
        //List<String> caseNumberList=new List<String>();
        for(case cs:caseWithUserUpdate){
            //caseNumberList.add(cs.casenumber);
            caseFilters.add('%'+cs.casenumber+'%');
        }
        
        List<User> userList = [SELECT Id,Last_Case_In_Progress__c FROM User WHERE Last_Case_In_Progress__c like: caseFilters];
        
        Map<Id,Set<String>> userCaseNumberMap=new Map<Id,Set<String>>();
        Map<Id,User> userMap=new Map<Id,User>();
        
        List<User> userToBeUpdated=new List<User>();
        Map<Id,User> userUpdateMap=new Map<Id,User>();
        
        system.debug('**userList '+userList);
        if(!userList.isEmpty()){
            for(User u:userList){
                Set<String> caseSet=new Set<String>();
                caseSet=AFKLM_CaseAfterTriggerHandler.getCaseNumbers(u.Last_Case_In_Progress__c);
                userCaseNumberMap.put(u.Id,caseSet);
                userMap.put(u.Id,u);
            }
        }
        system.debug('**userCaseNumberMap'+userCaseNumberMap);
        if(userCaseNumberMap!=null){
            for(Case checkCase:caseWithUserUpdate){
                for(Id usrId:userCaseNumberMap.keySet()){
                    Set<String> caseSet=new Set<String>();
                    caseSet=userCaseNumberMap.get(usrId);
                    if(caseSet.contains(checkCase.casenumber)){
                        caseSet.remove(checkCase.casenumber);
                        User tempUser=userMap.get(usrId);
                        tempUser.Last_Case_In_Progress__c=setCaseNumbers(caseSet); 
                        userUpdateMap.put(tempUser.Id,tempUser);
                    }
                }
            }
        }
        if(!userUpdateMap.isEmpty()){
            userToBeUpdated=userUpdateMap.values();     
            update userToBeUpdated;
            system.debug('**userToBeUpdated'+userToBeUpdated);
        }
           
    }
    
    public static set<String> getCaseNumbers(String caseInProgress){
        
        Set<String> caseNumberSet=new Set<String>();
        for(String key : caseInProgress.split(','))
        {
            caseNumberSet.add(key);
        }
        
        return caseNumberSet;
    }
    
    public static string setCaseNumbers(set<string> casesSet){
        String caseNumberString='';
        for(String key : casesSet)
        {
            if(caseNumberString!='')
                caseNumberString=caseNumberString+','+key;
            else
                caseNumberString=key;
        }
        
        return caseNumberString;
    }

}