/*************************************************************************************************
* File Name     :   AFKLM_CaseBeforeTriggerHandler 
* Description   :   Helper class for the Case trigger - before Update
* @author       :   Sathish Balu
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     20/01/2017      Sathish         Created the class:ST-1465
* 2.0     02/02/2017      Nagavi          ST-1592: Development - DG Check on Post and Case Level 
****************************************************************************************************/

public class AFKLM_CaseBeforeTriggerHandler {
    
    //V1.0 starts
    public void onbeforeupdate(list<case> caserecords)
    {
        try
        {
            set<Id>Caseid=new set<Id>();
            List<Socialpost> socialpostrecds=new list<Socialpost>();
            
            
            for(case cs:caserecords)
            {
                if((cs.Status).containsIgnoreCase('Closed'))
                {
                  caseid.add(cs.id);  
                    system.debug('caseid '+caseid);
                }
                
            }
            
            /*********** Logic to Capture Agent Processing Speed****************************/
            if(!caseid.isEmpty())
            {
                socialpostrecds=[select id,ParentId,Agent_Process_Speed__c,OwnerId from SocialPost where ParentId IN:caseid AND IsOutbound=FALSE];
                system.debug('**socialpostrecds** '+socialpostrecds);
            }
            
            
             if(!socialpostrecds.isEmpty())
             {
                
                 for(case csrec:caserecords)
                 {
                    csrec.Total_Post_per_Case__c=0;
                    csrec.Case_Agent_Process_Speed__c=0;
                     for(socialpost post:socialpostrecds)
                     {
                        if(csrec.Id==post.parentId)
                        {
                           
                           if(post.Agent_Process_Speed__c!=NULL)
                           {
                             csrec.Case_Agent_Process_Speed__c=csrec.Case_Agent_Process_Speed__c+ post.Agent_Process_Speed__c; 
                           }
                           
                           csrec.Total_Post_per_Case__c=csrec.Total_Post_per_Case__c+1;
                            system.debug('csrec.Total_Post_per_Case__c '+csrec.Total_Post_per_Case__c);
                           if(!string.valueOf(post.OwnerId).startsWith('00G'))
                           {
                                if(csrec.Agents_Name__c==NULL)
                              {
                                csrec.Agents_Name__c=post.OwnerId; 
                                csrec.Total_Agents_per_Case__c=1;
                              }
                              
                              else if(!csrec.Agents_Name__c.containsIgnoreCase(string.valueOf(post.OwnerId)))                          
                              {
                                 csrec.Agents_Name__c=csrec.Agents_Name__c+','+post.OwnerId;
                                 csrec.Total_Agents_per_Case__c=csrec.Total_Agents_per_Case__c+1;
                              }
                           }
                        }
                     }
                 }
             }
            
            /******************************************************************************/
            /******************** Logic to Capture DG Usage at Case Level -ST-1592 : Nagavi****************************/ 
            List<SocialPost> socialPostList=new List<SocialPost>();
            MAP<Id,List<SocialPost>> caseSocialPostmap=new Map<Id,List<SocialPost>>();
            List<SocialPost> tempSocialPostList;
            
            if(!caseid.isEmpty())
            {
                socialPostList=[select id,DG_prompt_used__c,parentId from SocialPost where ParentId IN:caseid AND DG_prompt_used__c =True and IsOutbound=true];
            }
            
            if(!socialPostList.isEmpty()){
                for(SocialPost sp:socialPostList)
                {
                    if(caseSocialPostmap.containsKey(sp.parentId))
                        caseSocialPostmap.get(sp.parentId).add(sp);
                    else
                        caseSocialPostmap.put(sp.parentId,new List<SocialPost>{sp});   
                }
                
                for(case cs:caserecords){
                    Decimal temp=0;
                    
                    tempSocialPostList=new List<SocialPost>();
                    tempSocialPostList=caseSocialPostmap.get(cs.Id);
                    system.debug('tempSocialPostList'+tempSocialPostList.size());
                    /*for(SocialPost post:tempSocialPostList){
                        temp=temp+1;
                    }*/
                    cs.No_of_Prompts_Used__c=tempSocialPostList.size(); 
                    system.debug('cs.No_of_Prompts_Used__c'+cs.No_of_Prompts_Used__c);                      
                }
                    
            }             
            /******************** Logic to Capture DG Usage at Case Level ****************************/ 
       
        }Catch(Exception e)
        {
            system.debug('Error in process '+e.getMessage()+'--'+e.getLineNumber());
        }
        
    }
    //V1.0 Ends
  
}