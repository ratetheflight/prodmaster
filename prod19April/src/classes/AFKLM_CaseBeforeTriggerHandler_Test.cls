/**********************************************************************
 Name:  AFKLM_CaseBeforeTriggerHandler_Test.cls
======================================================
Purpose: Test class for the Searchfor page and classes
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR            DATE            DETAIL                                 
    1.0     Sathish Balu    30/01/2017      Initial development
***********************************************************************/
@IsTest
public class AFKLM_CaseBeforeTriggerHandler_Test {
    
    static Testmethod void onbeforeupdate()
    {
        Account acc= new Account(
            Firstname='Test', 
            LastName='Account', 
            sf4twitter__Fcbk_User_Id__pc='FB_123456789');
        insert acc;
        
        Case cs = new Case(Status='New', AccountId=acc.id);
        insert cs;
        
        SocialPersona spersona = new SocialPersona(
            name='Test Persona', 
            provider = 'Facebook', 
            parentId = acc.id, 
            ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
            ExternalId='123456789');
        insert spersona;
        
        SocialPost spost = new SocialPost(
            name='Post from: Test Account',
            parentID=cs.id,
            Posted=System.now(),
            Handle='KLM',
            Provider='Facebook',
            Is_campaign__c=true,
            WhoId=acc.id,
            MessageType='Comment',
            chatHash__c='0000000111112345',
            Engaged_Date__c=system.now()-1,
            Replied_date__c =system.now(),
            ExternalPostId__c='1396604723809:f3da05e20a1267c585');
        insert spost;
        SocialPost spost1 = new SocialPost(
            name='Post from: Test Account',
            parentID=cs.id,
            Posted=System.now(),
            Handle='KLM',
            Provider='Facebook',
            Is_campaign__c=true,
            WhoId=acc.id,
            MessageType='Comment',
            Engaged_Date__c=system.now()-1,
            Replied_date__c =system.now(),
            chatHash__c='0000000111112345',
            ExternalPostId__c='1396604723809:f3da05e20a1267c585');
        insert spost1;
        test.startTest();
            cs.Case_Phase__c='Booking';
            cs.Case_Topic__c='Staff (BO)';
            cs.Case_Detail__c='Behavior';
            cs.Status='Closed';         
        update cs;        
        test.stopTest();
        
            

    }

    static Testmethod void onbeforeupdate2()
    {
        
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78944488');
        insert newPerson;
        
        //Insert persona
        List<SocialPersona> personaList=new List<SocialPersona>();
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList;
        
        ////Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(3,'New');
        insert caseList;
        
        //Data for SocialPost
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,5);                
        
        tempSocialPostList[0].IsOutbound=true;
        tempSocialPostList[0].parentId=caseList[0].Id;
        tempSocialPostList[0].DG_prompt_used__c=True;
        tempSocialPostList[1].IsOutbound=true;
        tempSocialPostList[1].parentId=caseList[1].Id;
        tempSocialPostList[1].DG_prompt_used__c=True;
        tempSocialPostList[2].IsOutbound=true;
        tempSocialPostList[2].parentId=caseList[2].Id;
        tempSocialPostList[2].DG_prompt_used__c=True;
        tempSocialPostList[3].IsOutbound=true;
        tempSocialPostList[4].IsOutbound=true;
        
        insert tempSocialPostList;
        
        caseList[0].status='Closed';
        caseList[0].Case_Phase__c='Orientation';
        caseList[0].Case_Topic__c='Health & Wellness (OR)';
        caseList[0].Case_Detail__c='Medical Conditions';
        update caseList[0];
        
    }
}