/********************************************************************** 
 Name:  AFKLM_CaseCloseWaitOnCustomerAFBatch
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This batch class will query for all cases that exceed 48 hours over the case_stage_datetime and 
    have the status 'Waiting on customer'. These cases will then be set to status 'Closed'
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      06/02/2015      Initial Development for AF. Separated because future logic
    1.1     Sai Choudhry        24/11/2016      To stop sending twitter survey on batch closure ST-1303.
    1.2     Sai Choudhry        02/03/2017       Removing the SOQL query for Record Type, JIRA ST-1868.
***********************************************************************/
global class AFKLM_CaseCloseWaitOnCustomerAFBatch implements Database.Batchable<Case> {
    global Iterable<Case> start(Database.BatchableContext bc){
        AFKLM_SCS_CaseAutoClose__c cac = AFKLM_SCS_CaseAutoClose__c.getOrgDefaults();
        
        List <Case> cl = new List <Case>(); 
        DateTime dt = Datetime.now().addDays(cac.Number_of_Days__c.intValue());

        //RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'AF Servicing' AND SobjectType = 'Case' LIMIT 1];
        //Modified by Sai.Removing the SOQL query for Record Type, JIRA ST-1868.
        Id rt = Schema.SObjectType.case.getRecordTypeInfosByName().get('AF Servicing').getRecordTypeId();
        //Modified  by sai. To include Case origin in the query. JIRA ST-1303
        if(!Test.isRunningTest()){
            cl = [SELECT id, Reference__c, Origin FROM Case WHERE RecordTypeId = :rt AND Status = 'Waiting for Client Answer' AND Case_Topic__c != 'Change Booking (Voluntary) (BO)' AND Case_stage_datetime__c < :dt AND Case_stage_datetime__c != null LIMIT 5000];        
        }else{
            cl = [SELECT id, Reference__c, Origin FROM Case WHERE RecordTypeId = :rt AND Status = 'Waiting for Client Answer' AND Case_Topic__c != 'Change Booking (Voluntary) (BO)' AND Case_stage_datetime__c < :dt AND Case_stage_datetime__c != null LIMIT 5];
        }
        return cl;
    }
    global void execute(Database.BatchableContext bc, LIST<Case> cl) {
        for(Case c:cl) {
            c.Status = 'Closed - No Response';
            //Added by Sai. To stop sending twitter survey on batch closure ST-1303
            //Start
            if(!Test.isRunningTest()){
                if(c.Origin != null && c.Origin.equals('Twitter'))
                {
                    c.Twitter_Survey_Stopper_On_Batch_Close__c = 'YES';
                }
            }    
            //end
        }      
        update cl;
    }
    global void finish(Database.BatchableContext bc) {
    }
}