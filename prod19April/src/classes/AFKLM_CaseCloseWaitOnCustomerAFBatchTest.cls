/********************************************************************** 
 Name:  AFKLM_CaseCloseWaitOnCustomerAFBatchTest
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Sai Choudhry      20/12/2016      Initial Development
***********************************************************************/
@isTest(SeeAllData=false)
private class AFKLM_CaseCloseWaitOnCustomerAFBatchTest {

    static testMethod void test0_AllCases() {
        insert new AFKLM_SCS_CaseAutoClose__c(Number_of_Days__c = -1.0);
        
        AFKLM_RecordTypeIds__c cs = new AFKLM_RecordTypeIds__c(Name = 'Servicing', RecordType_Id__c = '01220000000UYzA');
        insert cs;

        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'AF Servicing' AND SobjectType = 'Case' LIMIT 1];
            
        insertTestObject('test01', 'Waiting for Client Answer', 'Facebook', 'Negative', Datetime.now().addDays(-4), rt);
        insertTestObject('test02', 'Waiting for Client Answer', 'Twitter', 'Negative', Datetime.now().addDays(-3), rt);
        insertTestObject('test03', 'Waiting for Client Answer', 'WeChat', 'Negative', Datetime.now().addDays(-1), rt);
        insertTestObject('test04', 'New', 'Social', 'Negative', Datetime.now().addDays(-3), rt);
        insertTestObject('test05', 'Reopened', 'Social', 'Negative', Datetime.now().addDays(-2), rt);
        insertTestObject('test06', 'Closed - No Response', 'Social', 'Negative', Datetime.now().addDays(-1), rt);

        Case testObj1 = [select Status from Case WHERE Reference__c = 'test01'];     
        Case testObj2 = [select Status from Case WHERE Reference__c = 'test02']; 
        Case testObj3 = [select Status from Case WHERE Reference__c = 'test03']; 
        Case testObj4 = [select Status from Case WHERE Reference__c = 'test04']; 
        Case testObj5 = [select Status from Case WHERE Reference__c = 'test05']; 
        Case testObj6 = [select Status from Case WHERE Reference__c = 'test06']; 

        Test.startTest();       
        Database.executeBatch(new AFKLM_CaseCloseWaitOnCustomerAFBatch(), 3000);     
        Test.stopTest();
        
        System.assertEquals(testObj1.Status, 'Waiting for Client Answer', 'Status for test 01 is not equal to Waiting for client answer');
        System.assertEquals(testObj2.Status, 'Waiting for Client Answer', 'Status for test 02 is not equal to Waiting for client answer');
        System.assertEquals(testObj3.Status, 'Waiting for Client Answer', 'Status for test 03 is not equal to Waiting for Client Answer');
        System.assertEquals(testObj4.Status, 'New', 'Status for test 04 is not equal to New');
        System.assertEquals(testObj5.Status, 'Reopened', 'Status for test 05 is not equal to Reopened');
        System.assertEquals(testObj6.Status, 'Closed - No Response', 'Status for test 06 is not equal to Closed');
    }
    
    private static void insertTestObject(String reference, String status, String origin, String ssent, DateTime stagedt, RecordType rt){
        Case c = new Case(
            Reference__c = reference,
          Status = status,
          Origin = origin,
          chathash__c='1234',
          Case_stage_datetime__c = stagedt,
          RecordTypeId = rt.Id
        );

        insert c;
    }
}