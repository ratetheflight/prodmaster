/********************************************************************** 
 Name:  AFKLM_CaseCloseWaitOnCustomerBatch
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This batch class will query for all cases that exceed 48 hours over the case_stage_datetime and 
    have the status 'Waiting on customer'. These cases will then be set to status 'Closed'
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      21/01/2014      Initial Development (based on Jelle Terpstra - jelle.terpstra@accenture.com)
    1.1     Stevano Cheung      13/02/2014      Only do it for recordtype 'Servicing'
    1.2     Manuel Conde        20/02/2016      Added Call to Heroku
    1.3     Sai Choudhry        24/11/2016      To stop sending twitter survey on batch closure ST-1303
    1.4     Nagavi              10/03/2017      ST-1358 - Development- Case Topic Filling by DG in UAT
    1.5     Karthikeyan V       20/02/2017      To avoid batch issues with error "Attempt to de-reference a null object".
    1.6     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868.
***********************************************************************/
global class AFKLM_CaseCloseWaitOnCustomerBatch implements Database.Batchable<Case>, Database.AllowsCallouts {
    global Iterable<Case> start(Database.BatchableContext bc){
       
        AFKLM_SCS_CaseAutoClose__c cac = AFKLM_SCS_CaseAutoClose__c.getOrgDefaults();

        
        List <Case> cl = new List <Case>(); 
        DateTime dt = Datetime.now().addDays(cac.Number_of_Days__c.intValue());

        //RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Servicing' AND SobjectType = 'Case' LIMIT 1];
        //Modified by Sai.Removing the SOQL query for Record Type, JIRA ST-1868.
        Id rt = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
        
        if(!Test.isRunningTest()) 
            cl = [SELECT id, Reference__c,Origin,chatHash__c,Data_for_Prefill_By_DG__c FROM Case WHERE RecordTypeId = :rt AND Status = 'Waiting for Client Answer' AND Case_stage_datetime__c < :dt AND Case_stage_datetime__c != null LIMIT 5000];        
        else
            cl = [SELECT id, Reference__c,Origin,chatHash__c,Data_for_Prefill_By_DG__c FROM Case WHERE RecordTypeId = :rt AND Status = 'Waiting for Client Answer' AND Case_stage_datetime__c != null LIMIT 50];        
        System.debug('Case List Size****'+cl.size());
        System.debug('Case List****'+cl);
        return cl;
    }
    global void execute(Database.BatchableContext bc, LIST<Case> cl) {
        List<Case> casesClosedToBeSentToHeroku = new List<Case>();
        for(Case c:cl) {
            c.Data_for_Prefill_By_DG__c='';
            c.Status = 'Closed - No Response';
            if((c.chatHash__c != null && !c.chatHash__c.equals('')) && (c.Origin != null && c.Origin.equals('Facebook'))){
                casesClosedToBeSentToHeroku.add(c);
            }
            //Added by Sai. To stop sending twitter survey on batch closure ST-1303
            //Start
            //Added null check by Karthikeyan to avoid batch issues with error "Attempt to de-reference a null object" 
           if(!Test.isRunningTest()) {
          if(c.Origin != null && c.Origin.equals('Twitter'))
           {
            c.Twitter_Survey_Stopper_On_Batch_Close__c='YES';
          }
          }
            //end
        }      
        
        if(!casesClosedToBeSentToHeroku.isEmpty()){
            List<Case> emptyList = new List<Case>();
            SCS_HerokuOutboundHandler handler = new SCS_HerokuOutboundHandler();
            handler.sendCases2Heroku(casesClosedToBeSentToHeroku, emptyList);
        }

        update cl;

    }
    global void finish(Database.BatchableContext bc) {



    }
}