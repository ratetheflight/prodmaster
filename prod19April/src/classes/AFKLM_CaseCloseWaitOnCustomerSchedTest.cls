/********************************************************************** 
 Name:  AFKLM_CaseCloseWaitOnCustomerSchedTest
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      21/01/2014      Initial Development (based on Jelle Terpstra - jelle.terpstra@accenture.com)
***********************************************************************/
@isTest(SeeAllData=true)
private class AFKLM_CaseCloseWaitOnCustomerSchedTest {
    static testMethod void myUnitTest() {
        Test.startTest();
        
        SchedulableContext sc;
        AFKLM_CaseCloseWaitOnCustomerSchedulable cw = new AFKLM_CaseCloseWaitOnCustomerSchedulable();
        
        if(!Test.isRunningTest())
        	cw.execute(sc);    
        
        Test.stopTest();
    }
}