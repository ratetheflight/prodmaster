/*************************************************************************************************
* File Name     :   AFKLM_CaseClosureBatch
* Description   :   This batch class is used to close any open cases based on group case status
*                   and last modified date
* @author       :   Ataullah
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     17/02/2016      Ataullah         Created the class
* 1.1     18/04/2016      Narmatha         Included the logic for closing the Campaign cases - ST-002492
* 1.2     24/11/2016      Sai Choudhry     To stop sending twitter survey on batch closure ST-1303
* 1.3     10/03/2017      Nagavi           ST-1358 - Development- Case Topic Filling by DG in UAT
****************************************************************************************************/
global class AFKLM_CaseClosureBatch implements Database.Batchable<sObject>{
    
    //query string 
    global string query;
    // final status that should be populated on the selected cases  
    global string closureStatus;
    //holds the historic date for cases
    global final string lastmodified;
    //holds list of status to work on
    global List<String> statusList;
    global List<string> recordTypeIds;
    public class MyException extends Exception{}
    //getting the Servicing recordtype id
    AFKLM_RecordTypeIds__c servicingRecType=AFKLM_RecordTypeIds__c.getValues('Servicing');
    
    //constructor that accepts single case status as a search criteria
    global AFKLM_CaseClosureBatch(string grpCode, integer days,string caseStatus, string caseClosureStatus){
        
        try{
            //calculating exact datetime for the case records
            lastmodified = System.Now().addDays(-days).format('yyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            
            if(checkStatusValidity(caseClosureStatus))
                closureStatus = caseClosureStatus;
            else
                throw new MyException('Closure status is incorrect!!');
            
            //if(!checkStatusValidity(caseStatus))
            //    throw new MyException('Case status is incorrect!!');
            
            RecordTypeIds = new List<string>();
            RecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId());

            //Modified  by sai. To include Case origin in the query. JIRA ST-1303
            query =  'SELECT id, '+
                     'status, '+
                     'Case_Detail__c, '+
                     'Origin, '+
                     'Sentiment_at_start_of_case__c, '+
                     'Sentiment_at_close_of_case__c '+
                     'FROM CASE '+
                     'WHERE isClosed = False '+ 
                     'AND Group__c = \''+ grpCode+'\' '+ 
                     'AND status = \''+caseStatus+'\' '+
                     'AND recordTypeId IN :RecordTypeIds '+
                     'AND LastModifiedDate < '; 
            query= query + lastmodified;    
        }   
        catch(myException e){
            System.Debug('Exception occurred !!'+e);
        }       
    }
    //constructor that accepts multiple case status as a search criteria
    global AFKLM_CaseClosureBatch(string grpCode, integer days,List<string> caseStatus,string caseClosureStatus){
        
        try{
            lastmodified = System.Now().addDays(-days).format('yyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            if(checkStatusValidity(caseClosureStatus))
                closureStatus = caseClosureStatus;
            else    
                throw new MyException('Closure status is incorrect!!');
            
            statusList = new List<String>();
            statusList.addAll(caseStatus);
            /*for(string s: caseStatus){
                
                if(!checkStatusValidity(s)){
                    throw new MyException('Closure status is incorrect!!');
                }
                else
                    statusList.add(s);
            }*/
            RecordTypeIds = new List<string>();
            RecordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId());

            //Modified  by sai. To include Case origin in the query. JIRA ST-1303
            query = 'SELECT id, '+
                     'status, '+
                     'Case_Detail__c, '+
                     'Origin, '+
                     'Sentiment_at_start_of_case__c, '+
                     'Sentiment_at_close_of_case__c '+
                     'FROM CASE '+
                     'WHERE isClosed = false'+ 
                     ' AND Group__c = \''+grpCode+ '\' '+
                     ' AND status IN :statusList'+
                     ' AND  recordTypeId IN :RecordTypeIds '+
                     ' AND LastModifiedDate < ';
                     
            query=query + lastmodified ;
         system.debug('BatchQuery'+query);
        }
        catch(myException e){
            System.Debug('Exception occurred !!'+e);
        }       
    }
    
    //constructor that accepts all other case status as a search criteria
    global AFKLM_CaseClosureBatch(){
        
        try{
            AFKLM_CaseClosure_Settings__c closureSettings = AFKLM_CaseClosure_Settings__c.getInstance('ClosureCondition');
            if(closureSettings != Null){
                
                if(checkStatusValidity(closureSettings.ClosureStatus__c))
                    closureStatus = closureSettings.ClosureStatus__c;
                else    
                    throw new MyException('Closure status is incorrect!!');
                
                statusList = new List<String>();
                recordTypeIds = new List<string>();
                List<string> recordTypeNames = new List<string>();
                
                lastmodified = System.Now().addDays(-((Integer)closureSettings.Days__c)).format('yyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                string combinedStatus = ''; 
                
                if(!string.isBlank(closureSettings.RecordTypeName__c))
                    recordTypeNames = closureSettings.RecordTypeName__c.split(',');
                
                for(string loopVar: recordTypeNames){
                    recordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByName().get(loopVar).getRecordTypeId());
                }
                if(!string.isBlank(closureSettings.Status__c) && !string.isBlank(closureSettings.Status2__c)){
                    combinedStatus = closureSettings.Status__c + ','+closureSettings.Status2__c;
                }
                else if(!string.isBlank(closureSettings.Status__c)){
                    combinedStatus = closureSettings.Status__c;
                }
                else if(!string.isBlank(closureSettings.Status2__c)){
                    combinedStatus = closureSettings.Status2__c;
                }
                if(combinedStatus != ''){
                    for(string s : combinedStatus.split(','))
                        
                            StatusList.add(s);
                }
                                       

                //Modified  by sai. To include Case origin in the query. JIRA ST-1303
                query = 'SELECT id, '+
                         'status, '+
                         'Case_Detail__c, '+
                         'Origin, '+
                         'Sentiment_at_start_of_case__c, '+
                         'Sentiment_at_close_of_case__c '+
                         'FROM CASE '+
                         'WHERE isClosed = false'+ 
                         ' AND status IN:StatusList '+
                         'AND  recordTypeId IN :RecordTypeIds '+
                         'AND LastModifiedDate < ';
              query=query + lastmodified ;
              system.debug('BatchQuery'+query);
            }
        }
        catch(myException e){
            System.Debug('Exception occurred !!'+e);
        }
    }
    
    
    // Constuctor to close the Campaign Cases based on the given criteria- Ticket- ST-002492
     global AFKLM_CaseClosureBatch(String status)
    {
        try{
        query='';
        if(servicingRecType.RecordType_Id__c!=null) {
              query='SELECT id From Case WHERE'+
                    +' Case_Phase__c=\''+'Non Travel'+'\'' +
                    'AND Case_Topic__c=\''+'Marketing & Brand (NT)' +'\'' +
                    'AND Case_Detail__c=\''+'Posts & Campaigns'+'\''+
                    'AND status=\''+'In Progress'+'\''+
                    'AND (language__c=\''+'English'+'\''+ 'OR Language__c=\''+'Dutch'+'\''+')'+
                    'AND Origin=\''+'Facebook'+'\''+
                    'AND Recordtypeid=\''+servicingRecType.RecordType_Id__c +'\''+
                    'AND Group__c=\''+'Cygnific'+'\'';
          } 
        closureStatus='Closed By KLM Social Administrator';
        }
        catch(Exception e){
             System.Debug('Exception occurred !!'+e);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        System.debug('Actual Query::'+query + lastmodified + ':::ClosureStatus::'+ closureStatus);
        return Database.getQueryLocator(query );
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<Case> updatedCases = new List<Case>();
        
        try{

            //loop through each record and update status        
            for(case loopVar: (List<Case>)scope){
                loopVar.Data_for_Prefill_By_DG__c='';
                loopVar.status = closureStatus;
                //Added by Sai. To stop sending twitter survey on batch closure ST-1303
                //Start
                if(loopVar.Origin.equals('Twitter'))
                {
                    loopVar.Twitter_Survey_Stopper_On_Batch_Close__c= 'YES';
                }
                //End
                updatedCases.add(loopVar);
            }
           
            update updatedCases;
        }
        catch(exception e){
            System.debug('Exception Occured:'+e);
        }
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
    
    //Method to validate if the supplied status exists on case object
    private Boolean checkStatusValidity(string status){

        Boolean flag = false;
        Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple){
          if(f.getValue() == status){
            flag = true;  
          }
        }       
        return flag;
    }
    
}