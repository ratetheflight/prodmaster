/* 
*******************************************************************************************************************************************
* File Name     :   AFKLM_CaseClosureBatch_Test
* Description   :   Class to test CaseClosuer batch 
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    23/02/2016    Ata        Created the class.
* 1.1    20/04/2016    Narmatha   Included a test ethod for Campaign Case Close- ST-002492
******************************************************************************************************************************************* 
*/
@isTest
private class AFKLM_CaseClosureBatch_Test{
    
    static testmethod void singleStatusTest() {
        
        List<Case> caseList = AFKLM_TestDataFactory.insertKLMCase(100,'New');
        for(case LoopVar: caseList){
            LoopVar.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
            LoopVar.Group__c = 'IGT';            
        }
        
        insert caseList;
        
        Test.startTest();
            AFKLM_CaseClosureBatch batch = new AFKLM_CaseClosureBatch('IGT',3,'New','Closed By KLM Social Administrator');
            Database.executeBatch(batch);
        Test.stopTest();
    }
    
    static testmethod void singleStatusNegativeTest() {
       try{     
            List<Case> caseList = AFKLM_TestDataFactory.insertKLMCase(100,'ABC');
            for(case LoopVar: caseList){
                LoopVar.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
                LoopVar.Group__c = 'IGT';            
            }
            
            insert caseList;
            
            Test.startTest();
                AFKLM_CaseClosureBatch batch = new AFKLM_CaseClosureBatch('IGT',3,'New','Closed By KLM Social Administrator');
                Database.executeBatch(batch);
            Test.stopTest();
       }
       catch(exception e){
       system.debug('Exception ::'+e);
       }
    }

    static testmethod void MultipleStatusTest() {
        
        List<Case> caseList = AFKLM_TestDataFactory.insertKLMCase(100,'New');
        integer i= 0;
        for(case LoopVar: caseList){
            LoopVar.Group__c = 'IGT';
            LoopVar.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
            if(math.mod(i,2) == 0) 
                LoopVar.status = 'Escalated';          
            i++;
        }
        
        insert caseList;
        List<string> statusList = new List<string>{'New','Escalated'};
        
        Test.startTest();
            AFKLM_CaseClosureBatch batch = new AFKLM_CaseClosureBatch('IGT',3,statusList,'Closed By KLM Social Administrator');
            Database.executeBatch(batch);
        Test.stopTest();
    }
    
    static testmethod void MultipleStatusNegativeTest() {
        
        List<Case> caseList = AFKLM_TestDataFactory.insertKLMCase(100,'New1');
        integer i= 0;
        for(case LoopVar: caseList){
            LoopVar.Group__c = 'IGT';
            LoopVar.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
            if(math.mod(i,2) == 0) 
                LoopVar.status = 'Escalated';          
            i++;
        }
        
        insert caseList;
        List<string> statusList = new List<string>{'New','Escalated'};
        
        Test.startTest();
            AFKLM_CaseClosureBatch batch = new AFKLM_CaseClosureBatch('IGT',3,statusList,'Closed By KLM Social Administrator');
            Database.executeBatch(batch);
        Test.stopTest();
    }

    static testmethod void AutomationTestCustomSetting() {
        
        AFKLM_CaseClosure_Settings__c settings = new AFKLM_CaseClosure_Settings__c();
        settings.Name = 'ClosureCondition';
        settings.ClosureStatus__c = 'Closed By KLM Social Administrator';
        settings.status__c = 'New,Waiting for Client Answer,In Progress';
        settings.Days__c = 365;
        settings.recordtypeName__c = 'Servicing';
        insert settings;
        
        List<Case> caseList = AFKLM_TestDataFactory.insertKLMCase(100,'In Progress');
        integer i= 0;
        for(case LoopVar: caseList){
            LoopVar.Group__c = 'IGT';
            LoopVar.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
            if(math.mod(i,3) == 0) 
                LoopVar.status = 'New';          
            if(math.mod(i,5) == 0) 
                LoopVar.status = 'Waiting for Client Answer';
            i++;
        
        }
        
        insert caseList;
                
        Test.startTest();
            AFKLM_CaseClosureBatch batch = new AFKLM_CaseClosureBatch();
            Database.executeBatch(batch);
        Test.stopTest();
        
    }
    static testmethod void AutomationNegativeTest() {
       
       try{ 
            AFKLM_CaseClosure_Settings__c settings = new AFKLM_CaseClosure_Settings__c();
            settings.Name = 'ClosureCondition';
            settings.ClosureStatus__c = 'Closed By KLM Social Administrator';
            settings.status__c = 'New,Waiting for Client Answer,In Progress';
            settings.Days__c =365;
            settings.recordtypeName__c = 'Servicing';
            insert settings;
            
            List<Case> caseList = AFKLM_TestDataFactory.insertKLMCase(100,'In Progress');
            
            integer i= 0;
            for(case LoopVar: caseList){
                LoopVar.Group__c = 'IGT';
                LoopVar.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
                if(math.mod(i,3) == 0) 
                    LoopVar.status = 'New';          
                if(math.mod(i,5) == 0) 
                    LoopVar.status = 'Waiting for Client Answer';
                i++;
                
            }
            
            insert caseList;
                    
            Test.startTest();
                AFKLM_CaseClosureBatch batch = new AFKLM_CaseClosureBatch();
                Database.executeBatch(batch);
            Test.stopTest();
        }
        catch(Exception e){
            system.debug('Exception::'+e);    
        }
    }
    
    static testmethod void campaignCaseCloseTest() {
       
       try{ 
            AFKLM_RecordTypeIds__c settings = new AFKLM_RecordTypeIds__c ();
            settings.name='Servicing';
            settings.RecordType_Id__c=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
            insert settings;
            List<Case> caseList = AFKLM_TestDataFactory.insertKLMCase(200,'In Progress');
            List<Case> insertcaseList=new List<Case>();
            integer i= 0;
            for(case LoopVar: caseList){
               
                LoopVar.recordTypeId = settings.RecordType_Id__c;
                LoopVar.Case_Phase__c='Non Travel';
                LoopVar.Case_Topic__c='Marketing & Brand (NT)';
                LoopVar.Case_Detail__c='Posts & Campaigns';
                LoopVar.Origin='Facebook';
                LoopVar.Group__c='Cygnific';
                if(math.mod(i,3) == 0) 
                    LoopVar.language__c='Spanish';
                else
                    LoopVar.language__c='English';
            }
            
            insert caseList;
             
            Test.startTest();
                AFKLM_CaseClosureBatch obj= new AFKLM_CaseClosureBatch('Test');
                Database.executeBatch(obj);
                     
            Test.stopTest();
            System.assertEquals('Closed By KLM Social Administrator', insertcaseList[0].status) ; 
            System.assertEquals('In Progress', insertcaseList[3].status) ; 
        }
        catch(Exception e){
            system.debug('Exception::'+e);    
        }
    }
}