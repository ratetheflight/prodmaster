public with sharing class AFKLM_ChangeSPCmpnyTPNController {
        public String companyName{get;set;}
        public String selectedTopicProfile{get;set;}
        public String selectedRecordType{get;set;}
        public String currentCompany;
        public String currentTopicProfileName;
        public String currentContent='';
        public Boolean updateCaseRecType{get;set;}
       
        public String recordTypeName='';
        SocialPost sp = new SocialPost();
        Case cs = new Case();
        RecordType rc = new RecordType();  
        List<RecordType> caseRecordTypes = new List<RecordType>();
          
        AFKLM_SCS_ChangeCompanySettings__c custSettings = AFKLM_SCS_ChangeCompanySettings__c.getValues('Settings');
        
        public AFKLM_ChangeSPCmpnyTPNController(){
                sp = getSelectedSocialPost();
                cs = getAssociatedCase(sp);
                caseRecordTypes.addAll(getAllCaseRecordTypes());
                   
                if(cs != null){
                	rc = getCaseRecordType(cs);  
                }
        }
        
        public void changeSocialPost(){ 
			if(companyName != null && selectedTopicProfile != null){
				sp.Company__c = companyName;
	            sp.TopicProfileName = selectedTopicProfile;
	        }
	        if(updateCaseRecType==true){
	        	updateCaseRecordType(cs);
	        }
            
            	update sp; 
                    
        }

        
        public List<SelectOption> getRecordTypes(){
        	Set<String> recTypes = new Set<String>(custSettings.SocialCaseRecordTypes__c.split(';',custSettings.SocialCaseRecordTypes__c.length()));
        	
        	List<SelectOption> recordTypes = new List<SelectOption>();
        	
        	for(String s : recTypes){
        		for(RecordType rct : caseRecordTypes){
	        		if(rct.id.equals(s)){
	        			if('Servicing'.equals(rct.Name)){
	        				recordTypes.add(new SelectOption(s,'KLM Case'));
	        			} else {
	        				recordTypes.add(new SelectOption(s,'AirFrance Case'));
	        			}
	        		}
        		}
        	}
        	
        	return recordTypes;
        }
        
        public List<SelectOption> getCompanyNames(){
        		List<String> companies = new List<String>(custSettings.Company__c.split(';',custSettings.Company__c.length()));
        		/*if(companyName==null){
        			companyName ='KLM';
        		} else {
        			companyName='';
        			companyName = getSelectedCompany();
        		}*/
        		
                List<SelectOption> companyNames = new List<SelectOption>();
                
                for(String s : companies){
                	companyNames.add(new SelectOption(s,s));
                }
                
                return companyNames;
        }
        
        public String getSelectedCompany(){
        	return companyName; 
        }
        
        public List<SelectOption> getTopicProfileNames(){
        	List<String> topicProfiles = new List<String>{};
        	        	
        	if('KLM'.equals(companyName)){
        		//Uncomment in case business decide to make this option available also for facebook/other social networks
        		/*if('Facebook'.equals(sp.Provider)){
        			topicProfiles.addAll(custSettings.KLM__c.split(';',custSettings.KLM__c.length()));
        		} else {*/
        			topicProfiles.addAll(custSettings.TW_KLM__c.split(';',custSettings.TW_KLM__c.length()));
        		//}
        	}

        	if('AirFrance'.equals(companyName)){
        		//Uncomment in case business decide to make this option available also for facebook/other social networks
        		/*if('Facebook'.equals(sp.Provider)){
        			topicProfiles.addAll(custSettings.AirFrance__c.split(';',custSettings.AirFrance__c.length()));
        			System.debug('^^^Cust settings '+topicProfiles);
        		} else {*/
        			topicProfiles.addAll(custSettings.TW_AirFrance__c.split(';',custSettings.TW_AirFrance__c.length()));
        		//}
        	} 
        	
                List<SelectOption> topicProfileNames = new List<SelectOption>();
                
                for(String s : topicProfiles){
                        topicProfileNames.add(new SelectOption(s,s));
                }
                
                return topicProfileNames;
        }
        
        public String getCurrentCompany(){
                currentCompany = sp.Company__c;
                return currentCompany;
        }
        
        public String getCurrentTopicProfileName(){
                currentTopicProfileName = sp.TopicProfileName;
                return currentTopicProfileName;
        }
        
        public String getContent(){
                currentContent = sp.Content;
                return currentContent;
        }
        
        public String getRecordTypeName(){
        	if('Servicing'.equals(rc.Name)){
        		recordTypeName = 'KLM Case';
        	} else if('AF Servicing'.equals(rc.Name)) {
        		recordTypeName = 'AirFrance Case';
        	}
        		return recordTypeName;
        }
        
        private SocialPost getSelectedSocialPost(){
                SocialPost socPost = [SELECT Id, Content, Company__c, TopicProfileName, Provider, ParentId FROM SocialPost where id =: ApexPages.currentPage().getParameters().get('id') LIMIT 1];
                
                return socPost;
        }
        
        private Case getAssociatedCase(SocialPost sp){
        	if(sp.ParentId != null){
        		Case cs = [SELECT id, RecordTypeId FROM Case WHERE id =: sp.parentId];
        		
        		if(cs != null){
        			return cs;
        		}
        	}
        		return null;
        }
        
        private RecordType getCaseRecordType(Case cs){
        	if(cs.RecordTypeId != null){
        		RecordType rc = [SELECT Id, Name FROM RecordType WHERE Id =: cs.RecordTypeId];
        		return rc;
        	}
        	
        	return null;
        }
        
        
        public void updateCaseRecordType(Case cs){
        	if(!''.equals(cs.id)){
        		cs.RecordTypeId = selectedRecordType;
        		update cs;
        	}
        	
        }
        
        private List<RecordType> getAllCaseRecordTypes(){
        	List<RecordType> listOfRecordTypes = [SELECT id, Name FROM RecordType WHERE SObjecttype = 'Case'];
        	return listOfRecordTypes;
        }
}