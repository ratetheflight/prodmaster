/*************************************************************************************************
* File Name     :   AFKLM_DGCaseCompanionHandler 
* Description   :   Helper class for the DG case Companion trigger
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     07/02/2017      Nagavi         Created the class

****************************************************************************************************/
Public class AFKLM_DGCaseCompanionHandler{
    
    //Boolean to prevent recursion
    public static boolean onAfterInsert_FirstRun = true;
    
    Map<Id,dgai__DG_Companion__c > postDGMap=new Map<Id,dgai__DG_Companion__c>();
    List<SocialPost> postList=new List <SocialPost>();
    List<SocialPost> updatedPostList=new List <SocialPost>();
    //Method for after insert event
    public void onAfterInsert(List<dgai__DG_Companion__c> objectsToInsert)
    {
        if(onAfterInsert_FirstRun)
        {
            // PREVENT RECURSION
            onAfterInsert_FirstRun = false;
            
                        
            for(dgai__DG_Companion__c dgs:objectsToInsert) 
            {
                if(dgs.dgai__DG_Companion_Type__c=='Message' && dgs.dgai__Prompt_Text__c!=null && dgs.dgsh__Post__c!=null)
                {
                    postDGMap.put(dgs.dgsh__Post__c,dgs);
                }
            }
            
            if(!postDGMap.IsEmpty())
            {
                postList=[Select Id,DG_Companion__c,DG_Prompt_Text__c,DG_prompt_used__c,DG_Suggestion_Id__c,DG_Template_Id__c,DG_Prompt_Type__c from SocialPost where Id In:postDGMap.KeySet()];    
            }
            
            if(!postList.isEmpty())
            {
                for(SocialPost sp:postList)
                {
                    dgai__DG_Companion__c dg=postDGMap.get(sp.Id);
                    sp.DG_Companion__c=dg.Id;
                    sp.DG_Prompt_Text__c=dg.dgai__Prompt_Text__c;
                    sp.DG_prompt_used__c=True;
                    sp.DG_Suggestion_Id__c=dg.dgai__Suggestion_ID__c;
                    sp.DG_Template_Id__c=dg.dgai__Template_ID__c;
                    sp.DG_Prompt_Type__c=dg.dgsh__Message_Status__c;
                    updatedPostList.add(sp);
                }
            }
            
            if(!updatedPostList.isEmpty())
            {
                try
                {
                  System.debug(updatedPostList.size());
                
                   updatePostFuture(JSON.serialize(updatedPostList));
                  //update updatedPostList;
                  
                }
                Catch(Exception ex)
                {
                   System.debug(ex.getMessage());
                }
            }
            
  
        }
    }

@future
public static void updatePostFuture(String postJOSN) {

List<SocialPost> updatedPostList = (list<SocialPost>) JSON.deserialize(postJOSN,List<SocialPost>.class);
system.debug(updatedPostList);

update updatedPostList;
}    
    
}