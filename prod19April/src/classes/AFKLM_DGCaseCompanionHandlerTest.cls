/********************************************************************** 
 Name:  AFKLM_DGCaseCompanionHandlerTest
 Runs on: AFKLM_DGCaseCompanionTrigger,AFKLM_DGCaseCompanionHandler
====================================================== 
Purpose: 
    This class contains unit tests for validating the DG Companion records 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR            DATE            DETAIL                                 
    1.0     Nagavi Babu       27/02/2017      Initial Development
 
***********************************************************************/
@isTest
private class AFKLM_DGCaseCompanionHandlerTest{
    
   static testMethod void validateDGCompanion() {
        AFKLM_TestDataFactory.loadCustomSettings();
                
        List<SocialPersona> personaList=new List<SocialPersona>();
                
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78956788');
        insert newPerson;
        
        //Insert persona
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList;
        
        ////Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(3,'New');
        insert caseList;
        
        //Data for SocialPost
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,5);
        tempSocialPostList[0].ParentId=caseList[0].Id;
        tempSocialPostList[0].Isoutbound=True;
        tempSocialPostList[1].ParentId=caseList[1].Id;
        tempSocialPostList[1].Isoutbound=True;
        tempSocialPostList[2].ParentId=caseList[2].Id;
        tempSocialPostList[2].Isoutbound=True;
        tempSocialPostList[2].PostTags='IGT English';
        tempSocialPostList[2].Language__c='English';
        try {
            insert tempSocialPostList;            
        } 
        catch(Exception ex) {
          
        }
        
        //Data for DG Companion records
        List<dgai__DG_Companion__c> DGCompanionList= new List<dgai__DG_Companion__c>();
        DGCompanionList=AFKLM_TestDataFactory.insertDGCompanion(5,tempSocialPostList);
        
        try {
            insert DGCompanionList;            
        } 
        catch(Exception ex) {
          
        }

    }
    
}