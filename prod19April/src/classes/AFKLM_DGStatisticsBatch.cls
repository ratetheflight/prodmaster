/*************************************************************************************************
* File Name     :   AFKLM_DGStatisticsBatch 
* Description   :   This is batch that will be used for DG reporting temporarily
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     26/12/2016      Nagavi         Created the class

****************************************************************************************************/
global class AFKLM_DGStatisticsBatch implements Database.Batchable<Sobject>{
    
    //query string 
    global string query;
    
    //To calculate last modified date range of the records to be deleted
    global final string lastmodified;
    global List<String> actionList=new List<String>();
    global string day;
    global final List<String> status=new List<String>();
     //constructor that accepts all other case status as a search criteria
    global AFKLM_DGStatisticsBatch(Integer noOfDays){    
        
        //List<String> actionList=new List<String>();
        actionList.add('In Progress->Closed');
        actionList.add('In Progress->Reopened');
        actionList.add('In Progress->Closed - No Response');
        
        status.add('In Progress');
        
        //Calculate the date range
        lastmodified = System.Now().addDays(-noOfDays).format('yyy-MM-dd\'T\'HH:mm:ss\'Z\''); 
                   
        if(Test.isRunningTest()){
            this.query =  'SELECT id, LastModifiedDate, Case__c, Action__c, Handling_Time_in_minutes__c, Case_Topic__c FROM Status_Tracker__c'; 
        }
        else{
            
            query = 'Select Id, LastModifiedDate, Case__c, Action__c, Handling_Time_in_minutes__c, Case_Topic__c FROM Status_Tracker__c where LastModifiedDate > ' + lastmodified + ' AND Old_Status__c IN : status ' + ' Order By LastModifiedDate DESC';
            
        }
        
        system.debug('BatchQuery'+query);
    }
        
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        System.debug('Actual Query::'+query);   
        return Database.getQueryLocator(query);            
        
    }
    
    global void execute(Database.BatchableContext BC, List<Status_Tracker__c> scope){
        
        Set<Id> updatesCasesSet=new Set<Id>();
        List<dgai__DG_Companion__c> caseCompList=new List<dgai__DG_Companion__c>();
        Map<Id,Id> promptUsed=new Map<Id,Id>();
        List<DG_Statistics__c> dgStatList= new List<DG_Statistics__c>();
        try{
            for(Status_Tracker__c st:scope){
                updatesCasesSet.add(st.Case__c);
            }
            if(!updatesCasesSet.isEmpty()){
                caseCompList=[Select id,dgai__Case__c from dgai__DG_Companion__c where dgai__Case__c IN: updatesCasesSet];
            }
            
            if(!caseCompList.isEmpty()){
                for(dgai__DG_Companion__c cc:caseCompList){
                    promptUsed.put(cc.dgai__Case__c,cc.id);
                }
            }
            
            for(Status_Tracker__c st:scope){
                DG_Statistics__c dgStat=new DG_Statistics__c();
                dgStat.Case__c=st.Case__c;
                dgStat.Case_Handling_time__c=st.Handling_Time_in_minutes__c;
                dgStat.Case_Topic__c=st.Case_Topic__c;
                dgStat.Modified_Date_Time__c=st.LastModifiedDate;
                dgStat.Case_ID__c=st.Case__c;
                dgStat.DG_Companion_Id__c=promptUsed.get(st.Case__c);
                dgStat.StatusTracker_Id__c=st.Id;
                dgStat.Action__c=st.Action__c;
                if(!promptUsed.IsEmpty() && promptUsed.containsKey(st.Case__c))
                    dgStat.Prompts_Used__c=True;
                else
                    dgStat.Prompts_Used__c=false;
                dgStatList.add(dgStat);
            }
            
            if(!dgStatList.isEmpty()){
                //insert dgStatList StatusTracker_Id__c;
                upsert dgStatList StatusTracker_Id__c;
            }
        }
        catch(exception e){
            System.debug('Exception Occured:'+e);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =:BC.getJobId()];

       // Send an email to the Apex job's submitter notifying of job completion.

       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Object Cleanup Job :' + a.Status);
       mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
    }    
}