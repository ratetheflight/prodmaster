/* 
*******************************************************************************************************************************************
* File Name     :   AFKLM_DGStatisticsBatchTest
* Description   :   Class to test AFKLM_DGStatisticsBatch class
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    26/12/2016    Nagavi        Created the class.

******************************************************************************************************************************************* 
*/
@isTest
private class AFKLM_DGStatisticsBatchTest{
    
    static testmethod void searchTest() {
        
        List<Case> caseList=AFKLM_TestDataFactory.insertKLMCase(5,'New');
        insert caseList;
        
        caseList[0].Status='In Progress';
        caseList[1].Status='In Progress';
        update caseList;
                                
        List<Old_status_Tracker__c> oldSTList = AFKLM_TestDataFactory.insertOldStatusTracker(100);  
                
        oldSTList[0].Case__c=caseList[0].Id;
        oldSTList[0].Old_Status__c='In Progress';
        oldSTList[0].New_Status__c='Closed';
        
        oldSTList[1].Case__c=caseList[1].Id;
        oldSTList[1].Old_Status__c ='In Progress';
        oldSTList[0].New_Status__c='Closed';
        insert oldSTList ;
        
        system.debug('**'+ oldSTList[0]);
        system.debug('**'+ oldSTList[1]);
        
        List<dgai__DG_Companion__c> dgCompList= new List<dgai__DG_Companion__c>();
        
        dgai__DG_Companion__c dg1=new dgai__DG_Companion__c();
        dg1.dgai__Case__c=caseList[0].Id;
        dg1.Name='Test1';
        dgCompList.add(dg1);
        dgai__DG_Companion__c dg2=new dgai__DG_Companion__c();
        dg2.dgai__Case__c=caseList[1].Id;
        dg2.Name='Test2';
        dgCompList.add(dg2);
        insert dgCompList;
        
        Test.startTest();
            AFKLM_DGStatisticsBatchOld batch = new AFKLM_DGStatisticsBatchOld();
            Database.executeBatch(batch);
        Test.stopTest();
    }
    static testmethod void searchTest2() {
        
        List<Case> caseList=AFKLM_TestDataFactory.insertKLMCase(5,'New');
        insert caseList;
        
        caseList[0].Status='In Progress';
        caseList[1].Status='In Progress';
        update caseList;
                                
        //List<Old_status_Tracker__c> oldSTList = AFKLM_TestDataFactory.insertOldStatusTracker(100);  
        List<Status_Tracker__c> oldSTList = new List<Status_Tracker__c>();
        
        for(integer i=0;i<=1;i++){
            
            Status_Tracker__c shadowTracker = new Status_Tracker__c();
            shadowTracker.Type__c = 'Handled';
            oldSTList.add(shadowTracker);
        }
        
        oldSTList[0].Case__c=caseList[0].Id;
        oldSTList[0].Old_Status__c='In Progress';
        oldSTList[0].New_Status__c='Closed';
        
        oldSTList[1].Case__c=caseList[1].Id;
        oldSTList[1].Old_Status__c ='In Progress';
        oldSTList[0].New_Status__c='Closed';
        insert oldSTList ;
        
        system.debug('**'+ oldSTList[0]);
        system.debug('**'+ oldSTList[1]);
        
        List<dgai__DG_Companion__c> dgCompList= new List<dgai__DG_Companion__c>();
        
        dgai__DG_Companion__c dg1=new dgai__DG_Companion__c();
        dg1.dgai__Case__c=caseList[0].Id;
        dg1.Name='Test1';
        dgCompList.add(dg1);
        dgai__DG_Companion__c dg2=new dgai__DG_Companion__c();
        dg2.dgai__Case__c=caseList[1].Id;
        dg2.Name='Test2';
        dgCompList.add(dg2);
        insert dgCompList;
        
        Test.startTest();
            AFKLM_DGStatisticsBatch batch = new AFKLM_DGStatisticsBatch(2);
            Database.executeBatch(batch);
            
            AFKLM_DGStatisticsScheduler dgstat = new AFKLM_DGStatisticsScheduler();
            dgstat.execute(null);
       
        Test.stopTest();
    }
    static testmethod void searchNegativeTest() {
        
                
        Test.startTest();
            AFKLM_DGStatisticsBatch batch = new AFKLM_DGStatisticsBatch(1);
            Database.executeBatch(batch);
        Test.stopTest();
    }
       
}