/*************************************************************************************************
* File Name     :   AFKLM_DGStatisticsScheduler 
* Description   :   Scheduler class for the AFKLM_dGStatisticsScheduler. 
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author              Modification
*--------------------------------------------------------------------------------------------------
* 1.0     12/01/2017      Nagavi Babu         Initial version 

****************************************************************************************************/
global class AFKLM_DGStatisticsScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    AFKLM_DGStatisticsBatch BatchTmp = new AFKLM_DGStatisticsBatch(7);
    Database.executeBatch(BatchTmp,200);
  }
}