/**********************************************************************
 Name:  AFKLM_EmailSenderUtility 
=======================================================================
Purpose: This email utility will be used to send emails
=======================================================================
History                                                            
-------                                                            
VERSION     AUTHOR             DATE            DETAIL                                 
    1.0     Nagavi Babu        17/07/2013      Initial development
  
***********************************************************************/    
public class AFKLM_EmailSenderUtility {
    
    public void sendMail(String subject,String message,String publicGroup) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(getEmailAddresses(publicGroup));
        mail.setSubject(subject);
        mail.setPlainTextBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    
    public List<String> getEmailAddresses(String publicGroup) {
        List<String> idList = new List<String>();
        List<String> mailToAddresses = new List<String>();
        Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = : publicGroup Limit 1];
        for (GroupMember gm : g.groupMembers) {
            idList.add(gm.userOrGroupId);
        }
        User[] usr = [SELECT email FROM user WHERE id IN :idList];
        for(User u : usr) {
            mailToAddresses.add(u.email);
        }
        return mailToAddresses;
    }
    
}