/**********************************************************************
 Name:  AFKLM_InboundOutboundCalculator 
=======================================================================
Purpose: This scheduler will run every 5 minutes to process records within the system
=======================================================================
History                                                            
-------                                                            
VERSION     AUTHOR             DATE            DETAIL                                 
    1.0     Nagavi Babu        17/07/2013      Initial development
  
***********************************************************************/    
global with sharing class AFKLM_InboundOutboundCalculator implements Schedulable {

  public static final String FACEBOOK = 'Facebook';
  
  private Integer periodHours;
 
   /*public AFKLM_InboundOutboundCalculator(Integer periodHours) {
     this.periodHours = periodHours;
   }*/
 
  public static void scheduleJobs() {
    // Schedule a job each 5 minutes to create a rolling window effect. 
    System.schedule('InboundOutboundPostsCount *:00, Last 1 Hour', '0 0 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:05, Last 1 Hour', '0 5 * * * ? *', new AFKLM_InboundOutboundCalculator());    
    System.schedule('InboundOutboundPostsCount *:10, Last 1 Hour', '0 10 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:15, Last 1 Hour', '0 15 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:20, Last 1 Hour', '0 20 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:25, Last 1 Hour', '0 25 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:30, Last 1 Hour', '0 30 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:35, Last 1 Hour', '0 35 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:40, Last 1 Hour', '0 40 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:45, Last 1 Hour', '0 45 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:50, Last 1 Hour', '0 50 * * * ? *', new AFKLM_InboundOutboundCalculator());
    System.schedule('InboundOutboundPostsCount *:55, Last 1 Hour', '0 55 * * * ? *', new AFKLM_InboundOutboundCalculator());
    
  }
  
   /*
  * Method called by the Apex scheduler.
  */
  global void execute(SchedulableContext sc) {
    
    //Schedule the outbound posts counter
    (new InboundPostCountCalculator()).checkPostsCount();
  }
  
  
  
   /*
    * Class that implements the calculation of number of outbound social posts for Cygnific. - Added By Nagavi for ST-640
    */
  
  public class InboundPostCountCalculator{
    private Integer periodHours;
    private DateTime periodStartDate;
    private DateTime periodEndDate;
    private List<String> origins;
    private List<String> message;
    private DateTime alertDateTime;
    
    public void checkPostsCount(){
        
               
        origins=new List<string>{'Facebook'};        
        System.debug('@@@ Start calculating inbound');
        periodEndDate = DateTime.now();
        periodStartDate = periodEndDate.addMinutes(-1 * 5);
        message=new List<string>{'Private'}; 
            
                
            List<SocialPost> inboundList=new List<SocialPost>();
            inboundList= getInboundCount();           
            Integer inboundResults=inboundList.size();
            System.debug('@@@ inboundResults'+inboundResults);
            
            List<SocialPost> outboundList=new List<SocialPost>();
            outboundList=getOutboundCount();
            Integer outboundResults=outboundList.size();
            System.debug('@@@ outboundResults'+outboundResults);
            
            //Check and send inbound alerts
            checkandsendPostsCount('Inbound',inboundResults,periodEndDate);
            
            //Check and send outbound alerts
            checkandsendPostsCount('Outbound',outboundResults,periodEndDate);
                        
            
          
       }
                      
    public List<SocialPost> getInboundCount() {
      return [
        select Id 
        from SocialPost
        where SCS_IsOutbound__c = False
        and   Posted > :periodStartDate
        and   Posted <= :periodEndDate
        and   Provider in :origins
        and   MessageType in :message
        order by Posted asc
      ];
    } 
    
     public List<SocialPost> getOutboundCount() {
      return [
        select Id 
        from SocialPost
        where SCS_IsOutbound__c = True
        and   Posted > :periodStartDate
        and   Posted <= :periodEndDate
        and   Provider in :origins
        and   MessageType in :message
        order by Posted asc
      ];
    }
    
    public string checkPostsinboundoutboundCount(Integer inbound,Integer outbound){
        string temp='';
    
        if(inbound < 1 && outbound < 1)
            temp='Both';
        else if(inbound < 1 && outbound >= 1 )
            temp='Inbound';
        else if(outbound < 1 && inbound >= 1)
            temp='Outbound';
        else if(outbound >= 1 && inbound >= 1)
            temp='No Issue';
    
        return temp;
    }  
    
   
    Public void sendAlerts(string Status,String alertType, Decimal frequency,DateTime periodTime){
        AFKLM_EmailSenderUtility emailAlert=new AFKLM_EmailSenderUtility ();
        if(status=='Inbound'){ 
            System.debug('inside 5');
            if(alerttype=='Level 1 Alert' || alerttype=='Level 3 Alert'){ 
                if(!Test.isRunningTest()){
            string msg='Hi All,'+'\n';
            msg=msg+'No Inbound Posts/Messages have entered Salesforce for the Past ' + frequency +' minutes.'+'\n';
            msg=msg+'Thanks,'+'\n';
            msg=msg+'SFDC Admin';
            emailAlert.sendMail('Inbound Posts Failed - '+ alertType + ':' + periodTime,msg,alertType);
                }
            }
            if(alerttype=='Level 2 Alert' || alerttype=='Level 3 Alert'){ 
                if(!Test.isRunningTest()){
                AFKLM_MessageSenderUtility.sendMessage('Inbound Posts Failed - '+ alertType,alertType);
            /*string msg='Hi All,'+'\n';
            msg=msg+'No Inbound Posts/Messages have entered Salesforce for the Past ' + frequency +' minutes.'+'\n';
            msg=msg+'Thanks,'+'\n';
            msg=msg+'SFDC Admin';
            emailAlert.sendMail('Inbound Posts Failed - '+ alertType + ':' + periodTime,msg,alertType);*/
                }
            }
            
        }
        else if(status=='Outbound'){
            System.debug('inside 6');
            if(alerttype=='Level 1 Alert' || alerttype=='Level 3 Alert'){ 
                if(!Test.isRunningTest()){
            string msg='Hi All,'+'\n';
            msg=msg+'No Outbound Posts/Messages have delivered from Salesforce for the Past ' + frequency +' minutes.'+'\n';
            msg=msg+'Thanks,'+'\n';
            msg=msg+'SFDC Admin';
            emailAlert.sendMail('Outbound Posts Failed - '+ alertType + ':' + periodTime,msg,alertType);
                }
            }
            if(alerttype=='Level 2 Alert' || alerttype=='Level 3 Alert'){ 
                if(!Test.isRunningTest()){
                AFKLM_MessageSenderUtility.sendMessage('Outbound Posts Failed - '+ alertType,alertType);
            /*string msg='Hi All,'+'\n';
            msg=msg+'No Outbound Posts/Messages have delivered from Salesforce for the Past ' + frequency +' minutes.'+'\n';
            msg=msg+'Thanks,'+'\n';
            msg=msg+'SFDC Admin';
            emailAlert.sendMail('Outbound Posts Failed - '+ alertType + ':' + periodTime,msg,alertType);*/
                }    
             }
        }

        /* else{
            System.debug('inside 7');
            string msg='Hi All, No Posts have been Sent To/Received From from Salesforce for the Past' + frequency +' minutes. Kindly do the needful.Thanks';
            emailAlert.sendMail('Inbound/Outbound Posts Failed -' + alertType + ':' + periodTime,msg,alertType);
        }*/                                    
    }
    
    Public void checkandsendPostsCount(String status,Integer postsCount,DateTime periodEndDate){
        
        AFKLM_Proactive_Msg_Alert__c alerts=AFKLM_Proactive_Msg_Alert__c.getValues('Facebook Messenger');
        
        if(alerts!=null){
        
            if(postsCount > 0){ // If there are posts , clear datetime field
                System.debug('@@@ No Issue');
                if(status=='Inbound'){
                    alerts.Alert_Date_Time__c=null;
                    alerts.Level_2_Alert_Date_Time__c=null;
                    alerts.Level_3_Alert_Date_Time__c=null;
                    alerts.Level_2_Alert_Date_Time_Updated__c=NULL;
                    alerts.Level_1_alert__c=true;
                }
                else{
                    alerts.Outbound_Alert_Date_Time__c=null;
                    alerts.Outbound_Level_2_Alert_Date_Time__c=null;
                    alerts.Outbound_Level_3_Alert_Date_Time__c=null;
                    alerts.Outbound_Level_2_Alert_Date_Time_Updated__c=NULL;
                    alerts.Outbound_Level_1_Alert__c=true;
                }
                
                alerts.Level_2_alert__c=true;
                alerts.Level_3_Alert__c=true;
            }
            else{
                if(status=='Inbound'){
                    if(alerts.Alert_Date_Time__c!=null || alerts.Level_2_Alert_Date_Time__c!=null){
                        Decimal timeDiff;
                        Decimal level2updatedtimediff;
                        if(alerts.Alert_Date_Time__c!=null && alerts.Level_2_Alert_Date_Time__c==null){
                            timeDiff=((periodEndDate.getTime())/1000/60)-((alerts.Alert_Date_Time__c.getTime())/1000/60);
                            System.debug('level2 timdiff1 line206');
                        
                        if(alerts.Level_2_Alert_Date_Time_Updated__c!=NULL)
                            {
                              level2updatedtimediff=((periodEndDate.getTime())/1000/60)-((alerts.Level_2_Alert_Date_Time_Updated__c.getTime())/1000/60);  
                              System.debug('level2 timdiff2 line211'+level2updatedtimediff);
                            }
                        }
                        else if(alerts.Level_2_Alert_Date_Time__c!=null){
                            timeDiff=((periodEndDate.getTime())/1000/60)-((alerts.Level_2_Alert_Date_Time__c.getTime())/1000/60);                            
                            if(alerts.Level_2_Alert_Date_Time_Updated__c!=NULL)
                            {
                              level2updatedtimediff=((periodEndDate.getTime())/1000/60)-((alerts.Level_2_Alert_Date_Time_Updated__c.getTime())/1000/60);  
                              System.debug('level2 timdiff3 line219'+level2updatedtimediff);
                            } 
                        }
                        /*else if(alerts.Level_3_Alert_Date_Time__c!=null){
                            Decimal timeDiff=((periodEndDate.getTime())/1000/60)-((alerts.Level_3_Alert_Date_Time__c.getTime())/1000/60);
                        } */   
                        System.debug('@@@ timeDiff'+timeDiff);
                        system.debug('***alerts.Level_2_Alert_Date_Time__c '+alerts.Level_2_Alert_Date_Time__c);
                        if(timeDiff >= alerts.Level_2_Alert_Frequency__c && timeDiff < alerts.Level_3_Alert_Frequency__c && alerts.Level_2_alert__c==true){
                            System.debug('inside 1');
                            alerts.Level_1_alert__c=false;  
                            if(alerts.Level_2_Alert_Date_Time_Updated__c!=NULL)
                            {
                              level2updatedtimediff=((periodEndDate.getTime())/1000/60)-((alerts.Level_2_Alert_Date_Time_Updated__c.getTime())/1000/60);  
                              System.debug('level2 timdiff4 line233'+level2updatedtimediff);
                            }
                            if(timediff==alerts.Level_2_Alert_Frequency__c || level2updatedtimediff==alerts.Level_2_Alert_Frequency__c)
                            {
                               sendAlerts(Status,'Level 2 Alert',alerts.Level_2_Alert_Frequency__c,DateTime.now()); 
                               alerts.Level_2_Alert_Date_Time_Updated__c=DateTime.now(); 
                            }
                            
                                                         
                        }
                        else if(timeDiff==alerts.Level_3_Alert_Frequency__c && alerts.Level_3_Alert__c==true){
                            System.debug('inside 2');
                            
                            alerts.Level_2_Alert_Date_Time__c=DateTime.now();                            
                            sendAlerts(Status,'Level 3 Alert',alerts.Level_3_Alert_Frequency__c,DateTime.now());
                                
                        }
                        else if(alerts.Level_1_alert__c==true && timeDiff < alerts.Level_2_Alert_Frequency__c){
                            System.debug('inside 3');
                            sendAlerts(Status,'Level 1 Alert',5,DateTime.now());
                        }
                    }
                    // Set the Date and Time for the first alert
                    else{
                        if(alerts.Level_1_alert__c==true){
                            System.debug('inside 4');
                            alerts.Alert_Date_Time__c=(DateTime.now().addMinutes(-1 * 5));
                            sendAlerts(Status,'Level 1 Alert',5,DateTime.now());
                        }
                    }
                }
                
                if(status=='Outbound'){
                    if(alerts.Outbound_Alert_Date_Time__c!=null || alerts.Outbound_Level_2_Alert_Date_Time__c!=null ){
                        Decimal timeDiffOB;
                        Decimal level2updatedtimediffOB;
                        if(alerts.Outbound_Alert_Date_Time__c!=null && alerts.Outbound_Level_2_Alert_Date_Time__c==null)
                        {
                            timeDiffOB=((periodEndDate.getTime())/1000/60)-((alerts.Outbound_Alert_Date_Time__c.getTime())/1000/60);
                        if(alerts.Outbound_Level_2_Alert_Date_Time_Updated__c!=NULL)
                            {
                              level2updatedtimediffOB=((periodEndDate.getTime())/1000/60)-((alerts.Outbound_Level_2_Alert_Date_Time_Updated__c.getTime())/1000/60);  
                              System.debug('level2 timdiff5 line275'+level2updatedtimediffOB);
                            }
                        }
                        else if(alerts.Outbound_Level_2_Alert_Date_Time__c!=null){
                            timeDiffOB=((periodEndDate.getTime())/1000/60)-((alerts.Outbound_Level_2_Alert_Date_Time__c.getTime())/1000/60);
                            if(alerts.Outbound_Level_2_Alert_Date_Time_Updated__c!=NULL)
                            {
                              level2updatedtimediffOB=((periodEndDate.getTime())/1000/60)-((alerts.Outbound_Level_2_Alert_Date_Time_Updated__c.getTime())/1000/60);  
                              System.debug('level2 timdiff6 line283'+level2updatedtimediffOB);
                            }
                        }                        
                        
                        System.debug('@@@ timeDiffOB'+timeDiffOB);
                        if(timeDiffOB >= alerts.Level_2_Alert_Frequency__c && timeDiffOB < alerts.Level_3_Alert_Frequency__c && alerts.Level_2_alert__c==true){
                            System.debug('inside Outbound 1');
                            alerts.Outbound_Level_1_Alert__c=false;
                            if(alerts.Outbound_Level_2_Alert_Date_Time_Updated__c!=NULL)
                            {
                              level2updatedtimediffOB=((periodEndDate.getTime())/1000/60)-((alerts.Outbound_Level_2_Alert_Date_Time_Updated__c.getTime())/1000/60);  
                              System.debug('level2 timdiff7 line293'+level2updatedtimediffOB);
                            }
                            if(timeDiffOB==alerts.Level_2_Alert_Frequency__c || level2updatedtimediffOB == alerts.Level_2_Alert_Frequency__c)
                            {
                               sendAlerts(Status,'Level 2 Alert',alerts.Level_2_Alert_Frequency__c,DateTime.now()); 
                               alerts.Outbound_Level_2_Alert_Date_Time_Updated__c=DateTime.now();
                            }
                            
                            
                        }
                        else if(timeDiffOB==alerts.Level_3_Alert_Frequency__c && alerts.Level_3_Alert__c==true){
                            System.debug('inside Outbound 2');
                            alerts.Outbound_Level_2_Alert_Date_Time__c=DateTime.now();
                            sendAlerts(Status,'Level 3 Alert',alerts.Level_3_Alert_Frequency__c,DateTime.now());
                                
                        }
                        else if(alerts.Outbound_Level_1_Alert__c==true && timeDiffOB < alerts.Level_2_Alert_Frequency__c){
                            System.debug('inside Outbound 3');
                            sendAlerts(Status,'Level 1 Alert',5,DateTime.now());
                        }
                    }
                    // Set the Date and Time for the first alert
                    else{
                        if(alerts.Outbound_Level_1_Alert__c==true){
                            System.debug('inside Outbound 4');
                            alerts.Outbound_Alert_Date_Time__c=(DateTime.now().addMinutes(-1 * 5));
                            sendAlerts(Status,'Level 1 Alert',5,DateTime.now());
                        }
                    }
                }
            }
            
            try{
                upsert alerts;
            }
            Catch(exception e){
            
            }
            System.debug('@@@ Completed calculating inbound');
        }
    }
  }
}