@isTest
private class AFKLM_InboundOutboundCalculatorTest{
    static testMethod void testAFKLM_InboundOutboundCalculator() {
        insertAlertSettings('Facebook Messenger');
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc.checkPostsCount();
        
        AFKLM_Proactive_Msg_Alert__c setting2 =AFKLM_Proactive_Msg_Alert__c.getValues('Facebook Messenger');
        
        setting2.Level_2_Alert_Date_Time__c=DateTime.Now().addMinutes(-20);
        setting2.Outbound_Level_2_Alert_Date_Time__c=DateTime.Now().addMinutes(-20);
        upsert setting2;
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc11 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc11.checkPostsCount();
        
        setting2.Outbound_Alert_Date_Time__c=DateTime.Now().addMinutes(-5);
        setting2.Outbound_Level_2_Alert_Date_Time__c=null;
        upsert setting2;
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc12 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc12.checkPostsCount();
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc13 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc13.checkPostsinboundoutboundCount(1,1);
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc14 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc14.checkandsendPostsCount('Outbound',1,DateTime.Now());
    }
    
    static testMethod void testAFKLM_InboundOutboundCalculator2() {
        insertAlertSettings('Facebook Messenger');
        AFKLM_Proactive_Msg_Alert__c setting1 =AFKLM_Proactive_Msg_Alert__c.getValues('Facebook Messenger');
        //setting1.
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc2 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc2.checkPostsCount();
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc3 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc3.checkPostsCount();
        
        setting1.Level_2_Alert_Date_Time_Updated__c=DateTime.Now().addMinutes(-5);
        setting1.Level_2_Alert_Date_Time__c=DateTime.Now().addMinutes(-10);
        setting1.Outbound_Level_2_Alert_Date_Time__c=DateTime.Now().addMinutes(-5);
        setting1.Outbound_Level_2_Alert_Date_Time_Updated__c=DateTime.Now().addMinutes(-10);
        upsert setting1;
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc4 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc4.checkPostsCount();
        
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78956788');
        insert newPerson;
        
        //Insert persona
        List<SocialPersona> personaList=new List<SocialPersona>();
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList;
                       
        //Insert for SocialPost
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,5);
        insert tempSocialPostList;
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc5 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc5.checkPostsCount();
        
        setting1.Level_2_Alert_Date_Time__c=DateTime.Now().addMinutes(-20);
        setting1.Outbound_Level_2_Alert_Date_Time__c=DateTime.Now().addMinutes(-20);
        upsert setting1;
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc11 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc11.checkPostsCount();
        
        setting1.Outbound_Alert_Date_Time__c=DateTime.Now().addMinutes(-5);
        setting1.Outbound_Level_2_Alert_Date_Time__c=null;
        upsert setting1;
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc12 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc12.checkPostsCount();
    }
    
    private static void insertAlertSettings(string settingName){
        AFKLM_Proactive_Msg_Alert__c setting1 =AFKLM_Proactive_Msg_Alert__c.getValues(settingName);
        if(setting1 == null)
        {           
            AFKLM_Proactive_Msg_Alert__c newRec = new AFKLM_Proactive_Msg_Alert__c();
            newRec.Name = settingName;
            newRec.Outbound_Level_1_Alert__c=true;
            newRec.Level_1_alert__c=true;
            newRec.Level_2_alert__c=true;
            newRec.Level_2_Alert_Frequency__c =10;
            newRec.Level_3_Alert_Frequency__c =20;
                    
            insert newRec;   
        }        
    }
    
    private static testMethod void testAFKLM_InboundOutboundCalculator3() {
        insertAlertSettings('Facebook Messenger');
        AFKLM_Proactive_Msg_Alert__c setting1 =AFKLM_Proactive_Msg_Alert__c.getValues('Facebook Messenger');
        //setting1.
        
       
        setting1.Level_2_Alert_Frequency__c=10;
        setting1.Level_3_Alert_Frequency__c=15;
        setting1.Level_2_alert__c=true;
        setting1.Outbound_Level_2_Alert_Date_Time__c=DateTime.Now().addMinutes(-10);
        setting1.Outbound_Level_2_Alert_Date_Time_Updated__c=DateTime.Now().addMinutes(-15);
        upsert setting1;
        
        AFKLM_InboundOutboundCalculator.InboundPostCountCalculator calc2 = new AFKLM_InboundOutboundCalculator.InboundPostCountCalculator();
        calc2.checkPostsCount();
        //AFKLM_InboundOutboundCalculator.scheduleJobs();
        }
    
}