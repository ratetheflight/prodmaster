/********************************************************************** 
 Name:  AFKLM_LiveChatTranscript_Test
 Task:    N/A
 Runs on: 
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Prasanna Kumar   14/03/2017      Initial Development
***********************************************************************/
@isTest
private class AFKLM_LiveChatTranscript_Test {

 public static Id getRecordTypeId(String ObjectName,String recordTypeName){
        Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType s = sObjectMap.get(ObjectName) ; 
        Schema.DescribeSObjectResult resSchema = s.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); 
        Id rtId = recordTypeInfo.get(recordTypeName).getRecordTypeId();
        return rtId;

    }
static testmethod void livetranscripttest(){
    List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1];
    Id RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Webchat').getRecordTypeId();          
        User testUser = new User();
        testUser.userName = 'test@webchatemail.com';
        testUser.communityNickname = 'testing';
        testUser.email = 'test@email.com';
        testUser.ProfileId = profiles[0].Id;
        testUser.FirstName = 'Firstname';
        testUser.LastName = 'LastName';
        testUser.Alias = 'test';
        testUser.TimeZoneSidKey = 'America/Los_Angeles';
        testUser.languagelocalekey = 'en_US';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'UTF-8';
        insert testUser;
       RecordType personAccount=[Select ID from RecordType where Name =:'Person Account' Limit 1];
        Account testAccount=new Account();
        testAccount.LastName='Test';
        testAccount.Flying_Blue_Number__c='11111';
        testAccount.personemail='test@test.com';
        testAccount.recordtypeID=personAccount.ID;
        insert testAccount;
      LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;
         Case newCase=  new Case();
            newCase.Status='New';  
            newCase.RecordTypeId = RecordTypeId;          
            newCase.SuppliedEmail = 'test@test.com';
            newCase.SuppliedName = '11111';
            insert newCase;
      LiveChatTranscript trans = new LiveChatTranscript(
                                                            LiveChatVisitorId = visitor.Id,
                                                            Body = 'Some chat.  Blah Blah',
                                                            caseid=newCase.id
                                                            );
        Test.startTest();   
        insert trans;
        Test.stopTest();

}

}