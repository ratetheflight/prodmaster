/**********************************************************************
 Name:  AFKLM_MessageSenderUtility 
=======================================================================
Purpose: This email utility will be used to send emails
=======================================================================
History                                                            
-------                                                            
VERSION     AUTHOR             DATE            DETAIL                                 
    1.0     Nagavi Babu        17/07/2013      Initial development
  
***********************************************************************/    
public class AFKLM_MessageSenderUtility {
    
       
    @future(callout=true)
    public static void sendMessage(String subject,String publicGroup) {
         AFKLM_Message_Alert malert=new AFKLM_Message_Alert();
         List<User> userlist=new List<User>();
         userlist=getUsers(publicGroup);
         if(!userlist.IsEmpty()){
             malert.sendmessage(subject,userlist);
         }
    }
 
        
    public static List<User> getUsers(String publicGroup) {
        List<String> idList = new List<String>();
        List<User> mailToUsers = new List<User>();
        Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = : publicGroup Limit 1];
        if(g!=null){
            for (GroupMember gm : g.groupMembers) {
                idList.add(gm.userOrGroupId);
            }
            User[] usr = [SELECT email,MobilePhone,IsActive,Send_High_Severity_SMS_Alerts__c FROM user WHERE id IN :idList AND MobilePhone!=null AND IsActive=true];
            if(!usr.IsEmpty()){
                for(User u : usr) {
                    mailToUsers.add(u);
                }
            }
        }
        return mailToUsers;
    }
}