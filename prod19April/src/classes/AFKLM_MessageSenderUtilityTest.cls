@isTest
private class AFKLM_MessageSenderUtilityTest{
    static testMethod void testMsgUtility() {
        // Insert group
        Group grp = new Group();
        grp.Name = 'Level 1 Alert';
        insert grp;
        
        //Insert User
        User usr=AFKLM_TestDataFactory.createUser('Test','User','System Administrator');
        insert usr;
        
        groupMember grpMem=new groupMember();
        grpMem.GroupId=grp.Id;
        grpMem.UserorGroupId=usr.Id;
        insert grpMem;
        
        //em =new AFKLM_MessageSenderUtility ();
        AFKLM_MessageSenderUtility.sendMessage('Inbound Failed','Level 1 Alert');
        
        AFKLM_EmailSenderUtility emailSender=new AFKLM_EmailSenderUtility();
        emailSender.sendMail('Inbound Failed','Inbound Failed','Level 1 Alert');
    }
}