/*************************************************************************************************
* File Name     :   AFKLM_Message_Alert
* Description   :   Reusable component for sending sms alerts to users
* @author       :   Prasanna
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     29/12/2016      Prasanna         Created the class
* 2.0     14/02/2017      Nagavi           Added null check 
****************************************************************************************************/

Public class AFKLM_Message_Alert {

    public void sendmessage(String bodycontent,List<User> userlist){
        List<String> phonenumbers=new List<String>();
        TwilioConfig__c tconfig = TwilioConfig__c.getValues('New');
        if(tconfig!=null){
            String accountsid = tconfig.AccountSid__c;
            String authtoken=tconfig.AuthToken__c;
            String phonenumber=tconfig.phone_number__c;
            for(User usr: userlist){
                phonenumbers.add(usr.MobilePhone);
            }
            system.debug('phonenumbers'+phonenumbers);
            if(phonenumbers.size() > 0){
            
                String ACCOUNT_SID = accountsid;
                String AUTH_TOKEN = authtoken;
                TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
                system.debug('inside message alert class');
                for(String pnum: phonenumbers){
                    
                    Map<String,String> properties = new Map<String,String> {
                            'To'   => pnum,
                            'From' => phonenumber,
                            'Body' => bodycontent
                    };
                    if(!Test.isRunningTest()){
                        Twiliomessage message = client.getAccount().getMessages().create(properties);
                    }
                
                }   
        
            }
        }
    }
}