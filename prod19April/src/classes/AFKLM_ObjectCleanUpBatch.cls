/*************************************************************************************************
* File Name     :   AFKLM_ObjectCleanUpBatch 
* Description   :   This is generic batch that will be used to cleanup objects
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     05/10/2016      Nagavi         Created the class

****************************************************************************************************/
global class AFKLM_ObjectCleanUpBatch implements Database.Batchable<Sobject>{
    
    //query string 
    global string query;
    
    //To calculate last modified date range of the records to be deleted
    global final string lastmodified;
            
    //constructor that accepts all other case status as a search criteria
    global AFKLM_ObjectCleanUpBatch(string obj,integer noOfDays){
        
        //Calculate the date range
        lastmodified = System.Now().addDays(-noOfDays).format('yyy-MM-dd\'T\'HH:mm:ss\'Z\''); 
            
        if(Test.isRunningTest()){
            query =  'SELECT id FROM ' + obj ; 
        }
        else{
            
            //Frame the query       
            query =  'SELECT id FROM ' + obj +' WHERE LastModifiedDate < ' + lastmodified; 
        }
        
        system.debug('BatchQuery'+query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        System.debug('Actual Query::'+query);   
        return Database.getQueryLocator(query);            
        
    }
    
    global void execute(Database.BatchableContext BC, List<Sobject> scope){
                       
        try{
            
            Delete Scope;    
                 
        }
        catch(exception e){
            System.debug('Exception Occured:'+e);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =:BC.getJobId()];

       // Send an email to the Apex job's submitter notifying of job completion.

       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Object Cleanup Job :' + a.Status);
       mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
    }    
}