/* 
*******************************************************************************************************************************************
* File Name     :   AFKLM_ObjectCleanUpBatchTest
* Description   :   Class to test AFKLM_ObjectCleanUpBatch class
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    07/10/2016    Nagavi        Created the class.

******************************************************************************************************************************************* 
*/
@isTest
private class AFKLM_ObjectCleanUpBatchTest{
    
    static testmethod void deleteTest() {
        
        List<Old_status_Tracker__c > oldSTList = AFKLM_TestDataFactory.insertOldStatusTracker(100);
              
        insert oldSTList ;
        
        Test.startTest();
            AFKLM_ObjectCleanUpBatch batch = new AFKLM_ObjectCleanUpBatch('Old_status_Tracker__c',7);
            Database.executeBatch(batch);
        Test.stopTest();
    }
    static testmethod void deleteNegativeTest() {
        
                
        Test.startTest();
            AFKLM_ObjectCleanUpBatch batch = new AFKLM_ObjectCleanUpBatch('Old_status_Tracker__c',7);
            Database.executeBatch(batch);
        Test.stopTest();
    }
       
}