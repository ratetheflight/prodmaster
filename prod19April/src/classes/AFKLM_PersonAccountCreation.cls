/*===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     06/01/2017      Pushkar        Auto Account/Contact creation for webshop cases - ST-004531
* 1.1     18/01/2017      Pushkar        Salutation update for webshop cases - ST-004933
* 1.2     11/04/2017      Pushkar        Auto Account/Contact creation for webchat cases - ST-2115
****************************************************************************************************/

public class AFKLM_PersonAccountCreation{

     public static void createMethod(Set<Case> caseIDSet){
     
        Map<String,Account> accMap = new Map<String,Account>();
        Map<String,Account> accFBMap = new Map<String,Account>();
        List<Case> caseList = new List<Case>();
        Set<String> caseEmailSet = new Set<String>();
        Set<String> caseFBSet = new Set<String>();
        Map<String,Account> accountEmailMap = new Map<String,Account>();
        Map<String,Account> accountFBMap = new Map<String,Account>();
        Map<Id,Id> ContAcctIdMap = new Map<Id,Id>();
        List<Account> newExistingAccounts = new List<Account>();

        
        for(Case cs : caseIDSet){
            if(cs.SuppliedEmail != null && cs.SuppliedEmail != '')
                caseEmailSet.add(cs.SuppliedEmail);
            else if(cs.Email__c != '' && cs.Email__c != null)
                caseEmailSet.add(cs.Email__c);
            else if(cs.Visitor_email__c != '' && cs.Visitor_email__c != null)
                caseEmailSet.add(cs.Visitor_email__c);
            else if(cs.Flying_Blue_number__c !='' && cs.Flying_Blue_number__c !=null)
                caseFBSet.add(cs.Flying_Blue_number__c);
            else if(cs.Visitor_Flying_Blue_number__c !='' && cs.Visitor_Flying_Blue_number__c !=null)
                caseFBSet.add(cs.Visitor_Flying_Blue_number__c);

        }
        
        for(Account acc : [Select id,PersonEmail,Flying_Blue_Number__c from Account where (PersonEmail IN : caseEmailSet and PersonEmail !='' and PersonEmail !=null) or (Flying_Blue_Number__c IN :caseFBSet and Flying_Blue_Number__c !='' and Flying_Blue_Number__c !=null)]){
                accountEmailMap.put(acc.PersonEmail,acc);
                accountFBMap.put(acc.Flying_Blue_Number__c,acc);
        }
      
        
        for(Case caseInst : caseIDSet){

            if((!accountEmailMap.containsKey(caseInst.SuppliedEmail) && caseInst.SuppliedEmail != null && caseInst.SuppliedEmail != '') || 
               (!accountEmailMap.containsKey(caseInst.Email__c) && caseInst.SuppliedEmail == null) ||
               (!accountFBMap.containsKey(caseInst.Flying_Blue_number__c) && caseInst.SuppliedEmail == null && caseInst.Email__c == '') || 
               ((!accountEmailMap.containsKey(caseInst.Visitor_email__c) && (caseInst.Visitor_email__c != null && caseInst.Visitor_email__c != '')) || (!accountFBMap.containsKey(caseInst.Visitor_Flying_Blue_number__c) && (caseInst.Visitor_Flying_Blue_number__c != null && caseInst.Visitor_Flying_Blue_number__c != '')))
               ){

                Account acc = new Account();
                acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
                acc.FirstName = (caseInst.Visitor_First_Name__c != null)?caseInst.Visitor_First_Name__c:((caseInst.First_Name__c != '')?caseInst.First_Name__c:'');
                acc.LastName = (caseInst.Visitor_Last_Name__c != null)?caseInst.Visitor_Last_Name__c:((caseInst.SuppliedName != null && caseInst.SuppliedName != '')?caseInst.SuppliedName:caseInst.Last_Name__c);
                System.debug('***'+caseInst.RecordTypeId);
                if(caseInst.RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get('Webchat').getRecordTypeId()){
                System.debug('***'+caseInst.Visitor_First_Name__c );
                System.debug('***'+caseInst.Visitor_Last_Name__c );
                    if(caseInst.Visitor_First_Name__c == null && caseInst.Visitor_Last_Name__c == null){
                        acc.LastName = 'Webchat Visitor';
                        acc.FirstName = '';
                    }
                    else if(caseInst.Visitor_First_Name__c == null || caseInst.Visitor_Last_Name__c == null){
                        acc.LastName = (caseInst.Visitor_First_Name__c != null?caseInst.Visitor_First_Name__c:'') + (caseInst.Visitor_Last_Name__c != null?caseInst.Visitor_Last_Name__c:'');
                        acc.FirstName = '';
                    }
                }
                
                acc.PersonEmail = (caseInst.Visitor_email__c != null && caseInst.Visitor_email__c != '')?caseInst.Visitor_email__c:((caseInst.SuppliedEmail != null && caseInst.SuppliedEmail != '')?caseInst.SuppliedEmail:caseInst.Email__c);
                acc.Flying_Blue_Number__c = (caseInst.Visitor_Flying_Blue_number__c != null && caseInst.Visitor_Flying_Blue_number__c != '')?caseInst.Visitor_Flying_Blue_number__c:caseInst.Flying_Blue_number__c;
                if(caseInst.Salutation__c != null && caseInst.Salutation__c != ''){
                    if(caseInst.Preferred_Language__c == 'Dutch'){
                        if(caseInst.Salutation__c == 'Mr.')
                            acc.Salutation = 'dhr.';
                        else if(caseInst.Salutation__c == 'Mrs.')
                            acc.Salutation = 'mevr.';
                        else
                            acc.Salutation = caseInst.Salutation__c;                            
                    }else{
                        acc.Salutation = caseInst.Salutation__c;
                    } 
                }   
                acc.Phone = caseInst.Telephone_Number__c;
                acc.Country_of_Residence__c = caseInst.Country__c;
                accMap.put(((caseInst.Visitor_email__c != null && caseInst.Visitor_email__c != '')?caseInst.Visitor_email__c:((caseInst.SuppliedEmail != null && caseInst.SuppliedEmail != '')?caseInst.SuppliedEmail:caseInst.Email__c)),acc);
                accFBMap.put((caseInst.Visitor_Flying_Blue_number__c != null && caseInst.Visitor_Flying_Blue_number__c != '')?caseInst.Visitor_Flying_Blue_number__c:((caseInst.Flying_Blue_number__c != '')?caseInst.Flying_Blue_number__c:''),acc);   
                System.debug('accMap'+accMap);

            }
            
        }
        
        List<Account> newAccounts = accMap.values();
        System.debug('***'+accMap.values());
        insert newAccounts;
        
        newExistingAccounts.addAll(accountEmailMap.values());
        newExistingAccounts.addAll(newAccounts);
        
        if(newExistingAccounts.size() > 0){
            for(Contact cont : [Select Id,AccountId from Contact where AccountId IN :newExistingAccounts])
                ContAcctIdMap.put(cont.AccountId,cont.Id);
                
        }
            
        for(Case caseObj : caseIDSet){
            Account newAcnt;
            
            if(accMap.containsKey(caseObj.SuppliedEmail) || accMap.containsKey(caseObj.Email__c) || accMap.containsKey(caseObj.Visitor_email__c))
                newAcnt = (caseObj.Visitor_email__c != null)?accMap.get(caseObj.SuppliedEmail):((caseObj.SuppliedEmail != null && caseObj.SuppliedEmail != '')?accMap.get(caseObj.SuppliedEmail):accMap.get(caseObj.Email__c));
            if(accountEmailMap.containsKey(caseObj.SuppliedEmail) || accountEmailMap.containsKey(caseObj.Email__c) || accMap.containsKey(caseObj.Visitor_email__c))
                newAcnt = (caseObj.Visitor_email__c != null)?accountEmailMap.get(caseObj.SuppliedEmail):((caseObj.SuppliedEmail != null && caseObj.SuppliedEmail != '')?accountEmailMap.get(caseObj.SuppliedEmail):accountEmailMap.get(caseObj.Email__c));
            if(accountFBMap.containsKey(caseObj.Visitor_Flying_Blue_number__c) || accFBMap.containsKey(caseObj.Visitor_Flying_Blue_number__c))
                newAcnt = (accountFBMap.containsKey(caseObj.Visitor_Flying_Blue_number__c))?accountFBMap.get(caseObj.Visitor_Flying_Blue_number__c):accFBMap.get(caseObj.Visitor_Flying_Blue_number__c);
            else if(accountFBMap.containsKey(caseObj.Flying_Blue_number__c) || accFBMap.containsKey(caseObj.Flying_Blue_number__c)) 
                newAcnt = (accountFBMap.containsKey(caseObj.Flying_Blue_number__c))?accountFBMap.get(caseObj.Flying_Blue_number__c):accFBMap.get(caseObj.Flying_Blue_number__c);
            Case cas = new Case (id = caseObj.Id);
            cas.AccountId = newAcnt.Id;
            Cas.ContactId = ContAcctIdMap.get(newAcnt.Id);
            caseList.add(cas);
        }
        
      
        if(caseList.size() > 0)
            update caseList;
            
        
        
        
     }
     
     public static void updateMethod(Set<Case> caseIDSet){
         
        Map<Id,String> accSalutationMap = new Map<Id,String>();
        Map<Id,String> accLangMap = new Map<Id,String>();
        List<Account> accToUpd = new List<Account>();
        
        for(Case cs : caseIDSet){ 
            accSalutationMap.put(cs.accountID,cs.Salutation__c);
            accLangMap.put(cs.accountID,cs.Preferred_Language__c);
        }
        for(Account acc : [select id,Salutation from Account where Id IN :accSalutationMap.keySet()]){
            Account acct = new Account(id = acc.id);
            if(accSalutationMap.get(acc.id) != null && accSalutationMap.get(acc.id) != ''){
                if(accLangMap.get(acc.id) == 'Dutch'){
                    if(accSalutationMap.get(acc.id) == 'Mr.')
                        acct.Salutation = 'dhr.';
                    else if(accSalutationMap.get(acc.id) == 'Mrs.')
                        acct.Salutation = 'mevr.';
                    else
                        acct.Salutation = accSalutationMap.get(acc.id);                            
                }else{
                    acct.Salutation = accSalutationMap.get(acc.id);
                } 
            }
            accToUpd.add(acct);
        }   
        
        if(accToUpd.size() > 0)
            update accToUpd;
            
     }
}