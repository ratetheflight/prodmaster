/********************************************************************** 
 Name:  AFKLM_SCS_AFBitlyController
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    The class is used for AirFrance copied URL information by agents.
    Shortened by integrating with Bitly.com.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      10/03/2015      Initial Development
***********************************************************************/
global class AFKLM_SCS_AFBitlyController {
    public String mode;
    public String orgUrl { get;set; }
    public String shortUrl { get;set; }
    public String SelectedCheckboxTag { get;set; }
    private AFKLM_SCS_Bitly__c afBitly = AFKLM_SCS_Bitly__c.getValues('AFBitly');
    private static AFKLM_SCS_Bitly__c bitlyTag = AFKLM_SCS_Bitly__c.getValues('AFTag');    
    
    private Set<String> websitesToTag = new Set<String>();

    private Case cs;
    public AFKLM_SCS_AFBitlyController(){}

    public AFKLM_SCS_AFBitlyController(ApexPages.StandardController stdController) {
        // Works fine on old way of working on Case     
        try {
            this.cs = (Case)stdController.getRecord();
            this.myCaseId = [SELECT Id, Origin FROM Case WHERE Id =: cs.Id LIMIT 1].Origin; // TODO: Will fail on social console 'List has no rows for assignment to SObject' try / catch needed
        } catch(Exception e) {
            System.debug('**** Exception: '+e);
        }
    }
    
    // Returns a null as String for the tagging      
    public String myCaseId {
        get {
            if(myCaseId == null || myCaseId.trim().equals('')) {
                return 'null';
            } 
            return myCaseId;
        } 
        set;
    }
    
    public klmf_ly__c newBitLy {
        get {
            if(newBitly == null) {
                newBitly = new klmf_ly__c();
            }

            return newBitly;
        }
        set;
    }

    /**
     * Get bitly custom settings which mode to execute. Build the Webservice request.
     * Invoke the webservice call.
     *
     * @return the readXMLResponse
     */
    public String getbitly() {
        // Set is as parameter in the Custom Settings
        mode = afBitly.SCS_Bitly_Mode__c;

        XmlStreamReader reader;
        HttpResponse res;

        if(bitlyTag.SCS_Bitly_urlsToTag__c != null){
            websitesToTag.addAll(bitlyTag.SCS_Bitly_urlsToTag__c.split(';',bitlyTag.SCS_Bitly_urlsToTag__c.length()));
        }
        if(bitlyTag.SCS_Bitly_urlsToTag_2__c != null){
            websitesToTag.addAll(bitlyTag.SCS_Bitly_urlsToTag_2__c.split(';',bitlyTag.SCS_Bitly_urlsToTag_2__c.length()));
        }

        Boolean exists = false;

        for(String site: websitesToTag) {

            if(orgUrl.contains(site)){
                exists = true;
                break;
            }
        }

        if(exists == true) {

            orgUrl = stripTagWTSegWTAc(orgUrl);

            if(orgURL.contains('?')) {
                orgUrl = orgUrl + '&' + buildTagLogic(orgUrl);
            } else {
                orgUrl = orgUrl + '?' + buildTagLogic(orgUrl);
            }
        }

        if(this.checkIfUrlExists(orgUrl) == false){

            // set raw url (with tagging)
            newBitly.URL__c = orgUrl;

            //First, build the http request
            Http h = new Http();
            HttpRequest req = buildWebServiceRequest(bitlyTag.SCS_Bitly_RedirectURL__c + orgURL);

            //Second, invoke web service call 
            if (mode=='live') {
                res = invokeWebService(h, req);    
                reader = res.getXmlStreamReader();     
            }
            else {
                String str = '<bitly><results shortUrl="http://bit.ly/QqHEm">Setup basic string message</results></bitly>';
                reader = new XmlStreamReader(str);
            }

            shortUrl = readXMLResponse(reader,'shortUrl');
            newBitly.klmf_ly__c = shortUrl;
        }

        return null;
    }
    

    /**
     * Build the Webservice request.
     *
     * @return HTTPRequest
     */
    public HttpRequest buildWebServiceRequest(String purl) {
      
        HttpRequest req = new HttpRequest();
        String endpoint = buildTagBitlyInfo(purl);
        req.setEndpoint(endpoint); 
        req.setMethod('GET');
        return req;
    }


    /**
     *
     * @return
     */
    private klmf_ly__c getBitlyBySUrl(String burl) {
        
        klmf_ly__c[] oldBitly = [SELECT Id, Url__c FROM klmf_ly__c WHERE klmf_ly__c=:burl LIMIT 1];
        if(oldBitly.size() > 0) {     
            
            return oldBitly[0];
        }

        return null;
    }


    /**
     *
     *
     * @return
     */
    public void saveShortenedUrl() {
        
        try {
        
            if(newBitly.URL__c != null &&
                newBitly.klmf_ly__c != null){

                klmf_ly__c oldBitly = getBitlyBySUrl(newBitly.klmf_ly__c);
                if(oldBitly == null) {

                    newBitly.name = 'Bitly';
                    insert newBitly;
                } else {

                    oldBitly.Url__c = newBitly.URL__c;
                    upsert oldBitly;
                }

                // reset newBitly
                newBitly = new klmf_ly__c();
            } 

        } catch(Exception e) {

            system.debug('Insert klmf_ly__c failed: '+e);
        }
    }

    /**
     * Invoke the webservice call.
     *
     * @return HttpResponse
     */
    public static HttpResponse invokeWebService(Http h, HttpRequest req) {
        //Invoke Web Service
        HttpResponse res = h.send(req);
        return res;
    }

    /**
    * Read through the XML
    *
    * @return String
    */
    public static String readXMLResponse(XmlStreamReader reader, String sxmltag) {
        string retValue;
        // Read through the XML
        // system.debug('**** Reader: '+reader.toString());

        while(reader.hasNext()) {
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
               // System.debug('**** reader.getLocalName(): '+reader.getLocalName());
                              
               if (reader.getLocalName() == sxmltag) {
                    reader.next();
                    // System.debug('**** reader.getEventType(): '+reader.getEventType());
                    if (reader.getEventType() == XmlTag.characters) {
                        retValue = reader.getText();    
                    }
                } else {
                  reader.next();
                    if (reader.getEventType() == XmlTag.characters) {
                        System.debug('**** Info about the error message or other: '+reader.getText());    
                    }
                }
            }
            reader.next();
        }
        return retValue;
    }

    public Pagereference getTagSelected() {
        return null;
    }

    /**
    * Use a Custom Setting bitly template for removing parameters in URL.
    * Remove internal campaign tracking from the URL’s e.g. WT.ac or WT.seg_3:
    *
    * @return String without WT.ac or WT.seg_3
    */
    private static String stripTagWTSegWTAc(String urlInfo) {
        String newBuildURL = '';
        
        // Split URL by tokens and sort it
        Map<String, String> tokenURLS = new Map<String, String>();
        Integer urlItems = 1;
        String itemKey = '';
        
        for(String urlToken: urlInfo.split('[?]')) {
            itemKey = 'item'+String.valueOf(urlItems);

            // Skip the first url token since it is the base http://... after add the '?' for rebuilding the URL
            if(urlItems == 1) {
                tokenURLS.put(itemKey, urlToken);
            } else {
                tokenURLS.put(itemKey, '?' + urlToken);
            }
            
            for(String tokenToRemove: bitlyTag.SCS_Tag_Remover__c.split(';')) {
       
                if(tokenURLS.get(itemKey).contains(tokenToRemove)) {
                    tokenURLS.remove(itemKey);
                    break;
                }
            }
            urlItems++;
        }
        
        // Build up the URL
        List<String> sortUrlKeySet = new List<String>();
        sortUrlKeySet.addAll(tokenURLS.keySet());
        sortUrlKeySet.sort();
        
        for(String sortedKey: sortUrlKeySet) {
            newBuildURL = newBuildURL + tokenURLS.get(sortedKey);
        }
        
        return newBuildURL;
    }

    /**
    * Build the bitly endpoint information for integration.
    *
    * @return String endpoint for the bitly
    */
    private String buildTagBitlyInfo(String urlInfo) {
        String newEndpoint = '';
        
        if(urlInfo == null || urlInfo.trim().equals('')) {
            newEndpoint = afBitly.SCS_Bitly_Endpoint__c + '?' 
                + 'version=' + afBitly.SCS_Bitly_Version__c + '&'
                + 'format=' + afBitly.SCS_Bitly_Format__c + '&'
                + 'history='+ afBitly.SCS_Bitly_History__c + '&'
                + 'longUrl=' + urlInfo + '&'
                + 'login=' + afBitly.SCS_Bitly_Login__c + '&'
                + 'apiKey=' + afBitly.SCS_Bitly_ApiKey__c;
        } else {
            newEndpoint = afBitly.SCS_Bitly_Endpoint__c + '?' 
                + 'version=' + afBitly.SCS_Bitly_Version__c + '&'
                + 'format=' + afBitly.SCS_Bitly_Format__c + '&'
                + 'history='+ afBitly.SCS_Bitly_History__c + '&'
                + 'longUrl=' + EncodingUtil.URLENCODE(urlInfo,'UTF-8') + '&'
                + 'login=' + afBitly.SCS_Bitly_Login__c + '&'
                + 'apiKey=' + afBitly.SCS_Bitly_ApiKey__c;
        }
        return newEndpoint;
    }

    /**
    * Build the KLM business requirement for tagging. 
    * To be used for analytics, e.g. which 'Social Media' Twitter, Facebook, etc.
    *
    * @return String tag to be appended on the URL
    */
    private String buildTagLogic(String urlInfo) {
        String tagString = '';

        tagString = bitlyTag.SCS_Tag_Appended__c;
        tagString = tagString.replace('param1', 'c');
        tagString = tagString.replace('param2', 'WW');
        tagString = tagString.replace('param3', 'socialads'); 
        tagString = tagString.replace('param4', 'servicing'); // Should be dynamic on Case level
        tagString = tagString.replace('param5', 'servicing'); // Should be recordType name      
        tagString = tagString.replace('param6', 'null'); // Should be Femke Schreuders description
        tagString = tagString.replace('param7', 'null');
        tagString = tagString.replace('param8', 'null');

        return tagString;
    }

    /**
     *
     * @return 
     */
    private Boolean checkIfUrlExists(String purl) {

        klmf_ly__c[] urlExists = [SELECT Id, Url__c, klmf_ly__c FROM klmf_ly__c WHERE URL__c=:purl LIMIT 1];
        if(urlExists.size() > 0) {
         
            shortUrl = urlExists[0].klmf_ly__c;

            return true;
        }

        return false;
    }    
    
    /**
    * Gets the caseId from the Console when Case is openend on Event.
    * Uses the Origin, e.g. Twitter, Facebook to set the 4th tag parameter
    *
    * @return PageReference
    */ 
    public PageReference caseIdConsole(){

        try {
            
            if(!myCaseId.equals('null')) {
                this.myCaseId = [SELECT Origin FROM Case WHERE Id = :myCaseId].Origin;
            }
        
        } catch(Exception e) {            
            System.debug('**** Exception: '+e);
        }
        
        return null;
    }
    
    public PageReference ClearBitly(){

    	orgURL = shortUrl = '';
    	
    	return null;
    }
}