/********************************************************************** 
 Name:  AFKLM_SCS_AccessTokenRefreshCustAPIBatch
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    Refresh the Customer API token within the Custom Settings
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      21/05/2014      Initial Development
***********************************************************************/
global class AFKLM_SCS_AccessTokenRefreshCustAPIBatch implements Database.Batchable<AFKLM_SocialCustomerService__c>, Database.AllowsCallouts {
    global Iterable<AFKLM_SocialCustomerService__c> start(Database.BatchableContext bc){
        List<AFKLM_SocialCustomerService__c> cl = new List<AFKLM_SocialCustomerService__c>();
        cl.add(AFKLM_SocialCustomerService__c.getOrgDefaults());
        return cl;
    }

    global void execute(Database.BatchableContext bc, LIST<AFKLM_SocialCustomerService__c> cl) {
        AFKLM_SCS_AccessTokenRefreshCustAPI atrca = new AFKLM_SCS_AccessTokenRefreshCustAPI();
        atrca.reprocessOAuthToken();
    }
    global void finish(Database.BatchableContext bc) {
       
        //This will query for batch status
 AsyncApexJob a = [Select Id, Status, NumberOfErrors, ExtendedStatus  from AsyncApexJob where Id = :bc.getJobId() limit 1]; 
//Rerun the batch if there is token generation issue
if(a.ExtendedStatus!=null && a.NumberOfErrors >=1){
Database.executebatch(new AFKLM_SCS_AccessTokenRefreshCustAPIBatch(),1);
		}
    }
}