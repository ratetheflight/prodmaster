/********************************************************************** 
 Name:  AFKLM_SCS_AccessTokenRefreshCustAPISched
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This schedulable class will be able to schedule the AFKLM_SCS_AccessTokenRefreshCustAPIBatch apex batch.
        e.g. Every One hour: (Run in anonymous on Force.com or Developer mode)
            AFKLM_SCS_AccessTokenRefreshCustAPISched atrcas = new AFKLM_SCS_AccessTokenRefreshCustAPISched();
            String cronStr = '0 00 * * * ?';
            System.schedule('Customer API refresh Access Token Every hour', cronStr, atrcas);
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      21/05/2014      Initial Development
***********************************************************************/
global class AFKLM_SCS_AccessTokenRefreshCustAPISched implements Schedulable {
    global void execute(SchedulableContext scMain) {
        AFKLM_SCS_AccessTokenRefreshCustAPIBatch atrcab = new AFKLM_SCS_AccessTokenRefreshCustAPIBatch();
        ID idBatch = Database.executeBatch(atrcab, 100);
    }
}