/*************************************************************************************************
* File Name     :   AFKLM_SCS_AccountMergeHandler
* Description   :   Helper class for the Account Merge Batch
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     16/03/2016      Nagavi         Created the class
* 2.0     03/06/2016      Nagavi         Included logic to merge all fields in the account object

****************************************************************************************************/
Public class AFKLM_SCS_AccountMergeHandler{
    
    Public static void doMerge(List<SFSSDupeCatcher__Potential_Duplicate__c> duplicatesList,Map<Id,SFSSDupeCatcher__Duplicate_Warning__c> alertMap){
        List<Id> dupeAccIdsList=new List<Id>();
        Map<Id,List<Id>> alertAccountsMap = new Map<Id,List<Id>>();
        List<Account> dupeAccountList=new List<Account>();
        List<Account> accountDateList=new List<Account>();
        Map<Id,Account> accountIdMap=new Map<Id,Account>();
        Map<Id,DateTime> accountDateMap=new Map<Id,DateTime>();
        Map<Id,string> accountCreatedUserMap=new Map<Id,string>();
        List<SocialPersona> relatedPersonaList=new List<SocialPersona>();
        Map<Id,List<SocialPersona>> accountSocialPersonaMap = new Map<Id,List<SocialPersona>>();
        Map<Id,Id> alertSurvivorMap=new map<Id,Id>();
        
        for(SFSSDupeCatcher__Potential_Duplicate__c dupes:duplicatesList){
            dupeAccIdsList.add(dupes.SFSSDupeCatcher__Account__c); //Get the duplicate account Ids
            
            // Map to store the alert and its corresponding accounts            
            if(alertAccountsMap.containsKey(dupes.SFSSDupeCatcher__Duplicate_Warning__c)){
                 alertAccountsMap.get(dupes.SFSSDupeCatcher__Duplicate_Warning__c).add(dupes.SFSSDupeCatcher__Account__c);
            }
            else{
                alertAccountsMap.put(dupes.SFSSDupeCatcher__Duplicate_Warning__c,new list<Id>{dupes.SFSSDupeCatcher__Account__c});
            }
        }
                
        system.debug('**alertAccountsMap'+alertAccountsMap);
        
        //Query the accounts associated with the duplicates.
        if(!dupeAccIdsList.isEmpty()){
            dupeAccountList=getAllData(dupeAccIdsList);
            
            /*dupeAccountList=[Select id,VIP__pc ,Ambassador__c ,Personal_Assistance__pc , Flying_Blue_Number__c, Website,
                            WhatsApp_Pilot__c, sf4twitter__Twitter_Username__pc, Facebook_Username__pc, 
                            sf4twitter__Fcbk_Username__pc, sf4twitter__Fcbk_User_Id__pc,WeChat_User_ID__c,
                            WeChat_Username__c,Restricted_Account__c,Influencer__c from Account where id in:dupeAccIdsList AND IsPersonAccount =true];
                            
            */
            
            //Separate query to get the latest account      
            accountDateList=[Select Id,LastModifiedDate,CreatedById from Account where id in:dupeAccIdsList];   
            
            if(!dupeAccountList.isEmpty()){
                for(Account acc:dupeAccountList)
                    accountIdMap.put(acc.Id,acc);
                    
                for(Account acnt:accountDateList){
                    accountDateMap.put(acnt.Id,acnt.LastModifiedDate);
                    accountCreatedUserMap.put(acnt.Id,acnt.CreatedById);
                }
                        
                //Query the persona associated with each account
                    relatedPersonaList=[select Id,parentId from SocialPersona where ParentId in: accountIdMap.keyset()];
                
                //Map to store all persona associated with each account         
                if(!relatedPersonaList.isEmpty()){
                    for(SocialPersona socPersona:relatedPersonaList){
                        if(accountSocialPersonaMap.containsKey(socPersona.parentId)){
                            accountSocialPersonaMap.get(socPersona.parentId).add(socPersona);
                        }
                        else{
                            accountSocialPersonaMap.put(socPersona.parentId,new list<SocialPersona>{socPersona});
                        }
                    }
                                
                }
                system.debug('**accountSocialPersonaMap'+accountSocialPersonaMap);
                
                //To store the survivor account for each alert
                alertSurvivorMap=getSurvivorMap(alertAccountsMap,accountSocialPersonaMap,accountDateMap);
                system.debug('**alertSurvivorMap'+alertSurvivorMap);
            }
        }
                
        // Iterate through each alert and perform merge one by one
        if(!alertMap.IsEmpty()){
            List<SFSSDupeCatcher__Duplicate_Warning__c> toBeUpdated= new List<SFSSDupeCatcher__Duplicate_Warning__c>();
            for(Id alerts:alertMap.Keyset()){
                if(alertAccountsMap.containsKey(alerts)){
                    string mergeType='';
                    string errormessage='';
                    system.debug('**mergeType in'+mergeType);                            
                    List<Id> relatedAccountIds=alertAccountsMap.get(alerts); //Get the accounts related to each alert
                    Id survivor;
                    if(alertSurvivorMap.containsKey(alerts)){
                        survivor=alertSurvivorMap.get(alerts); //Get the survivor Id
                    }
                    for(Id accIds:relatedAccountIds){
                        if(survivor!=null && survivor!=accIds){
                            List<SocialPersona> toBeUpdatedSP=new List<SocialPersona>();
                                        
                            //Check if the account is eligible for merge
                            if(compareAccounts(accountIdMap.get(survivor),accountIdMap.get(accIds))){
                                            
                                //If eligible for merge , update the required fields in survivor
                                Account updatedSurvivor=new Account();
                                updatedSurvivor = updateSurvivorFields(accountIdMap.get(survivor),accountIdMap.get(accIds),accountCreatedUserMap);
                                            
                                //If ready to merge , reparent persona
                                List<SocialPersona> toBeReparented=new List<SocialPersona>();
                                if(accountSocialPersonaMap.containsKey(accIds)){                                                      
                                    toBeReparented=accountSocialPersonaMap.get(accIds);
                                    for(SocialPersona SP:toBeReparented){
                                        SP.ParentId=survivor;
                                        toBeUpdatedSP.add(SP);
                                    }
                                                    
                                }
                                
                                Boolean allSuccess=true; // Boolean to indicate if all persona for the account has been reparented
                                if(!toBeUpdatedSP.IsEmpty()){
                                    Database.SaveResult[] updateResult = Database.Update(toBeUpdatedSP,false);
                                    //Boolean allSuccess=true;
                                    if(updateResult !=null && updateResult.size()>0){
                                                    
                                        for(Integer i=0;i<updateResult.size();i++)
                                        {
                                            if(!updateResult[i].isSuccess())
                                            {  
                                                allSuccess=false;
                                                if(errormessage!='')
                                                    errormessage=errormessage+' ,'+updateResult[i].getErrors()[0].getMessage(); 
                                                else
                                                    errormessage=updateResult[i].getErrors()[0].getMessage();
                                            }
                                        }
                                    }
                                }
                                
                                // If no persona present or the persona reparenting is successful, then proceed with merge
                                if(toBeUpdatedSP.IsEmpty() || (!toBeUpdatedSP.IsEmpty() && allSuccess)) {       
                                        
                                    system.debug('allSuccess'+allSuccess);   
                                    //Perform Merge
                                    system.debug('**inside merge'); 
                                    Database.MergeResult mergeResult = Database.merge(updatedSurvivor, accountIdMap.get(accIds), false);
                                    system.debug('accountmap'+accountIdMap);
                                    if(mergeResult != null && !mergeResult.isSuccess()){
                                        system.debug('**mergeType in'+mergeType);
                                        system.debug('**mergeResult'+mergeResult.getErrors()[0].getMessage());
                                        mergeType='Manual';
                                        if(errormessage!='')
                                            errormessage=errormessage+' ,'+mergeResult.getErrors()[0].getMessage();
                                        else
                                            errormessage=mergeResult.getErrors()[0].getMessage();
                                    }
                                    else if(mergeResult != null && mergeResult.isSuccess()){
                                        accountIdMap.put(updatedSurvivor.Id,updatedSurvivor);
                                    }
                                    
                                }                                                                                                
                                       
                                else{ // Error in Persona reparenting
                                    mergeType='Manual';
                                }
                            }
                            else{
                                //If no survivor is chosen, do not merge
                                mergeType='Manual';
                                if(errormessage!='')
                                    errormessage=errormessage+' ,'+Label.MergeErrorFieldCriteria;
                                else
                                    errormessage=Label.MergeErrorFieldCriteria;
                            }            
                               
                        }                              
                                                             
                    }
                    
                    system.debug('**mergeType out'+mergeType);
                    //Update the status of alert as Manual.
                    if(mergeType!='' && mergeType=='Manual'){
                        if(alertMap.containsKey(alerts)){
                            SFSSDupeCatcher__Duplicate_Warning__c relatedAlert=alertMap.get(alerts);
                            relatedAlert.Status__c=mergeType;
                            relatedAlert.Error_Message__c=errormessage;
                            toBeUpdated.add(relatedAlert);
                        }
                    }
                }                    
                
            }
            if(!toBeUpdated.isEmpty())
                update toBeUpdated; //Update the alerts whose merge has failed
          
        }
        
        
    }
    
    Public static Map<Id,Id> getSurvivorMap(Map<Id,List<Id>> alertAccountsMap,Map<Id,List<SocialPersona>> accountSocialPersonaMap,Map<Id,DateTime> accountDateMap){
        Map<Id,Id> alertSurvivorMap =new Map<Id,Id>();
        //Iterate through each alert and determine the survivor/Master Account for each alert
        for(Id alert:alertAccountsMap.keyset()){
            List<Id> relatedAccIds=new List<Id>();
            relatedAccIds=alertAccountsMap.get(alert);
            if(!relatedAccIds.IsEmpty()){
                Integer tempNoPersona=0;
                Id survivorId;
                for(Id accId:relatedAccIds){
                    if(tempNoPersona==0){
                        if(accountSocialPersonaMap.containsKey(accId)){
                            tempNoPersona=accountSocialPersonaMap.get(accId).size();
                            survivorId=accId;
                        }
                    }
                    else {
                        if(accountSocialPersonaMap.containsKey(accId)){
                            if(tempNoPersona < accountSocialPersonaMap.get(accId).size()){
                                tempNoPersona=accountSocialPersonaMap.get(accId).size();
                                survivorId=accId;
                            }
                        }
                    }
                                        
                }
                Id tempsurvivor;
                if(tempNoPersona==0){
                    DateTime temp=null;
                    for(Id acntId:relatedAccIds){
                        if(temp==null){
                            if(accountDateMap.containsKey(acntId)){
                                temp=accountDateMap.get(acntId);
                                tempsurvivor=acntId;
                            }
                        }
                        else{
                            if(accountDateMap.containsKey(acntId) && accountDateMap.get(acntId) > temp){
                                temp=accountDateMap.get(acntId);
                                tempsurvivor=acntId;
                            }
                        }
                    }
                }
                if(tempNoPersona!=0)
                    alertSurvivorMap.put(alert,survivorId);
                else
                    alertSurvivorMap.put(alert,tempsurvivor);
            }
        }
        return alertSurvivorMap;
    }
    
    
    public static Boolean compareAccounts(Account survivor, Account victim){
        Boolean isEligible=true;
        
        /*if(survivor.VIP__pc ==true || victim.VIP__pc ==true || survivor.Ambassador__c ==true || victim.Ambassador__c ==true || survivor.Personal_Assistance__pc ==true || victim.Personal_Assistance__pc ==true || survivor.Restricted_Account__c==true || victim.Restricted_Account__c==true){
            isEligible=false;
            return isEligible;
        }*/
        
        if(survivor.Personal_Assistance__pc ==true || victim.Personal_Assistance__pc ==true || survivor.Restricted_Account__c==true || victim.Restricted_Account__c==true || survivor.Influencer__c ==true || victim.Influencer__c==true){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.Flying_Blue_Number__c!=null && victim.Flying_Blue_Number__c!=null && survivor.Flying_Blue_Number__c!=victim.Flying_Blue_Number__c){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.Website!=null && victim.Website!=null && survivor.Website!=victim.Website){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.WhatsApp_Pilot__c!=victim.WhatsApp_Pilot__c){
            isEligible=false;
            return isEligible;
        }
        if(survivor.sf4twitter__Twitter_Username__pc!=null && victim.sf4twitter__Twitter_Username__pc!=null && survivor.sf4twitter__Twitter_Username__pc!=victim.sf4twitter__Twitter_Username__pc){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.Facebook_Username__pc!=null && victim.Facebook_Username__pc!=null && survivor.Facebook_Username__pc!=victim.Facebook_Username__pc){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.sf4twitter__Fcbk_Username__pc!=null && victim.sf4twitter__Fcbk_Username__pc!=null && survivor.sf4twitter__Fcbk_Username__pc!=victim.sf4twitter__Fcbk_Username__pc){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.sf4twitter__Fcbk_User_Id__pc!=null && victim.sf4twitter__Fcbk_User_Id__pc!=null && survivor.sf4twitter__Fcbk_User_Id__pc!=victim.sf4twitter__Fcbk_User_Id__pc){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.WeChat_User_ID__c!=null && victim.WeChat_User_ID__c!=null && survivor.WeChat_User_ID__c!=victim.WeChat_User_ID__c){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.WeChat_Username__c!=null && victim.WeChat_Username__c!=null && survivor.WeChat_Username__c!=victim.WeChat_Username__c){
            isEligible=false;
            return isEligible;
        }
        
       /* if(survivor.Restricted_Account__c!=victim.Restricted_Account__c){
            isEligible=false;
            return isEligible;
        }
        
        if(survivor.Influencer__c!=victim.Influencer__c){
            isEligible=false;
            return isEligible;
        }*/
        
        return isEligible;
    }
    
    /*public static Account updateSurvivorFields2(Account survivor, Account victim){
                
        if((survivor.Flying_Blue_Number__c==null || survivor.Flying_Blue_Number__c=='') && victim.Flying_Blue_Number__c!=null){
            survivor.Flying_Blue_Number__c=victim.Flying_Blue_Number__c;
        }
        
        if((survivor.Website==null || survivor.Website=='')&& victim.Website!=null){
            survivor.Website=victim.Website;
        }
        
        if((survivor.sf4twitter__Twitter_Username__pc==null || survivor.sf4twitter__Twitter_Username__pc=='') && victim.sf4twitter__Twitter_Username__pc!=null){
            survivor.sf4twitter__Twitter_Username__pc=victim.sf4twitter__Twitter_Username__pc;
        }
        
        if((survivor.Facebook_Username__pc==null || survivor.Facebook_Username__pc=='') && victim.Facebook_Username__pc!=null){
            survivor.Facebook_Username__pc=victim.Facebook_Username__pc;
        }
        
        if((survivor.sf4twitter__Fcbk_Username__pc == null || survivor.sf4twitter__Fcbk_Username__pc=='' )&& victim.sf4twitter__Fcbk_Username__pc!=null){
            survivor.sf4twitter__Fcbk_Username__pc=victim.sf4twitter__Fcbk_Username__pc;
        }
        
        if((survivor.sf4twitter__Fcbk_User_Id__pc == null || survivor.sf4twitter__Fcbk_User_Id__pc =='') && victim.sf4twitter__Fcbk_User_Id__pc!=null){
            survivor.sf4twitter__Fcbk_User_Id__pc=victim.sf4twitter__Fcbk_User_Id__pc;
        }
        
        if((survivor.WeChat_User_ID__c==null || survivor.WeChat_User_ID__c=='') && victim.WeChat_User_ID__c!=null){
            survivor.WeChat_User_ID__c=victim.WeChat_User_ID__c;
        }
        
        if((survivor.WeChat_Username__c==null || survivor.WeChat_Username__c=='') && victim.WeChat_Username__c!=null){
            survivor.WeChat_Username__c=victim.WeChat_Username__c;
        }
        
        if(survivor.VIP__pc ==true || victim.VIP__pc ==true){
            survivor.VIP__pc =true;
        }
        
        if(survivor.Ambassador__c ==true || victim.Ambassador__c ==true){
            survivor.Ambassador__c =true;
        }
        
            
        return survivor;
    }*/
    
   //Method to query all the fields in Account object - Nagavi (06/03/2016 - ST-002308)
    public static List<Account> getAllData(List<Id> accountList)
    {
        string ObjectName='Account';
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Map<String, Schema.SObjectField> fieldMap = SObjectTypeObj.getDescribe().fields.getMap();
        //
        String sFields = '';
        
        for (String s: fieldMap.keySet()){
            system.debug('&&'+s);  
            
            if(fieldMap.get(s).getDescribe().isUpdateable() && s!='name'){
                sFields = sFields + s + ',';
            }
        }
        if(sFields!='')
            sFields = sFields.substring(0, sFields.length()-1);
        
        return Database.query('SELECT ' + sFields +' FROM Account Where id IN: accountList AND IsPersonAccount =true');
    }
    
    //Method to compare the survivor and victim and update the survivor accordingly- Nagavi (06/03/2016 - ST-002308)
    public static Account updateSurvivorFields(Account survivor, Account victim, Map<Id,string> accountCreatedUserMap){
         
                
        String objectType ='Account';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType accSchema = schemaMap.get(objectType);
        Map<String, Schema.SObjectField> fieldMap = accSchema.getDescribe().fields.getMap();
                         
        for(string s:fieldMap.keyset()){
            Schema.DisplayType fielddataType = fieldMap.get(s).getDescribe().getType();
            if(fieldMap.get(s).getDescribe().isUpdateable()){  //To filter the non updatable fields
                system.debug('** victim- user'+accountCreatedUserMap.get(victim.Id));
                system.debug('** mobile- user'+Label.KLM_iPad_mobile_team_User);
                if(fielddataType == Schema.DisplayType.Boolean){
                    if(survivor.get(s) == false && victim.get(s)== true)
                        survivor.put(s,true);    
                }
                else if(s!='name' && (s=='firstname' || s=='lastname') && accountCreatedUserMap.get(victim.Id) == Label.KLM_iPad_mobile_team_User){
                    if(s=='firstname')
                        survivor.put(s,victim.get(s));
                    else if(s=='lastname')
                        survivor.put(s,victim.get(s));
                }
                else if(s!='name' && s!='firstname' && s!='lastname'){
                    if((survivor.get(s)== null || survivor.get(s)== '') && (victim.get(s)!=null || victim.get(s)!=''))
                        survivor.put(s,victim.get(s));
                }
            }
        }
         
         survivor.Last_Merged_Date__c=System.Now();
         
         return survivor;
    }

}