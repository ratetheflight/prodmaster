/********************************************************************** 
 Name:  AFKLM_SCS_BitlyController
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    The class is used for KLM.com copied URL information by agents.
    The URL is appended with KLM required tags and shortened by integrating with Bitly.com.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      07/03/2014      Initial Development
***********************************************************************/
global class AFKLM_SCS_BitlyController {
    public String mode;
    public String orgUrl { get;set; }
    public String shortUrl { get;set; }
    public String SelectedCheckboxTag { get;set; }
    private static AFKLM_SCS_Bitly__c bitly = AFKLM_SCS_Bitly__c.getValues('Bitly');
    private static AFKLM_SCS_Bitly__c bitlyTag = AFKLM_SCS_Bitly__c.getValues('Tag');
    public String activeCaseID{
        get{
            if(activeCaseID==null ) {
            return 'null';
            }
            return activeCaseID;
            }
       
        set;
        }
    public Boolean isSurvey{get;set;}
    public string surveylink{get;set;}
    
    private Case cs;
    public AFKLM_SCS_BitlyController(){}

    public AFKLM_SCS_BitlyController(ApexPages.StandardController stdController) {
        // Works fine on old way of working on Case     
        try {
            this.cs = (Case)stdController.getRecord();
            myCaseId = [SELECT Id, Origin FROM Case WHERE Id =: cs.Id LIMIT 1].Origin; // TODO: Will fail on social console 'List has no rows for assignment to SObject' try / catch needed
        } catch(Exception e) {
            System.debug('**** Exception: '+e);
        }
    }
    
    // Returns a null as String for the tagging  
    public String myCaseId {
        get {
            if(myCaseId == null || myCaseId.trim().equals('')) {
                return 'null';
            } 
            return myCaseId;
        } 
        set;
    }
    /*public String myCaseId {get;set;}*/

    public klmf_ly__c newBitLy {
        get {
            if(newBitly == null) {
                newBitly = new klmf_ly__c();
            }

            return newBitly;
        }
        set;
    }

    private Id bitlyId { get;set; }

    /**
    * Get bitly custom settings which mode to execute. Build the Webservice request.
    * Invoke the webservice call.
    *
    * @return the readXMLResponse
    */
    public PageReference getbitly() {
        // Set is as parameter in the Custom Settings
        String mode = bitly.SCS_Bitly_Mode__c;
        // String shorten;
        XmlStreamReader reader;
        HttpResponse res;

        if(orgUrl.contains('www.klm.com')) {

            orgUrl = stripTagWTSegWTAc(orgUrl);
                    
            // Skip 'customer_support' tagging, else do tagging
            if(!orgUrl.contains('customer_support')) {
                
                orgUrl = orgUrl + buildTagLogic(orgUrl);
            }
        }

        if(this.checkIfUrlExists(orgUrl) == false){
       
            // set raw url (with tagging)
            newBitly.URL__c = orgUrl;

            //First, build the http request
            Http h = new Http();
            HttpRequest req = buildWebServiceRequest(orgURL);

            //Second, invoke web service call 
            if (mode=='live') {
                res = invokeWebService(h, req);    
                reader = res.getXmlStreamReader();     
            }
            else {
                String str = '<bitly><history><shortUrl><results shortUrl="http://bit.ly/QqHEm">Setup basic string message</results></shortUrl></history></bitly>';
                reader = new XmlStreamReader(str);
            }

            shortUrl = readXMLResponse(reader,'shortUrl');
            newBitly.klmf_ly__c = shortUrl;
        }

        return null;
    }
    
    /**
    * Build the Webservice request.
    *
    * @return HTTPRequest
    */
    public HttpRequest buildWebServiceRequest(String purl) {
        String endpoint;

        HttpRequest req = new HttpRequest();
        endpoint = buildTagBitlyInfo(purl);
        req.setEndpoint(endpoint); 
        req.setMethod('GET');
        return req;
    }

    /**
     *
     * @return 
     */
    private Boolean checkIfUrlExists(String purl) {

        klmf_ly__c[] urlExists = [SELECT Id, Url__c, klmf_ly__c FROM klmf_ly__c WHERE URL__c=:purl LIMIT 1];
        if(urlExists.size() > 0) {
         
            shortUrl = urlExists[0].klmf_ly__c;

            return true;
        }

        return false;
    }

    /**
     *
     * @return
     */
    private klmf_ly__c getBitlyBySUrl(String burl) {
        
        klmf_ly__c[] oldBitly = [SELECT Id, Url__c FROM klmf_ly__c WHERE klmf_ly__c=:burl LIMIT 1];
        if(oldBitly.size() > 0) {     
            
            return oldBitly[0];
        }

        return null;
    }

    /**
     *
     * @return
     */
    public void saveShortenedUrl() {
        
        try {
        
            if(newBitly.URL__c != null &&
                newBitly.klmf_ly__c != null){

                klmf_ly__c oldBitly = getBitlyBySUrl(newBitly.klmf_ly__c);
                if(oldBitly == null) {

                    newBitly.name = 'Bitly';
                    insert newBitly;
                } else {

                    oldBitly.Url__c = newBitly.URL__c;
                    upsert oldBitly;
                }

                // reset newBitly
                newBitly = new klmf_ly__c();
            } 

        } catch(Exception e) {

            system.debug('Insert klmf_ly__c failed: '+e);
        }
    }

    /**
    * Invoke the webservice call.
    *
    * @return HttpResponse
    */
    public static HttpResponse invokeWebService(Http h, HttpRequest req) {
         //Invoke Web Service
         HttpResponse res = h.send(req);
         return res;
    }

    /**
    * Read through the XML
    *
    * @return String
    */
    public static String readXMLResponse(XmlStreamReader reader, String sxmltag) {
        String retValue;
        // Read through the XML
        // system.debug('**** Reader: '+reader.toString());

        while(reader.hasNext()) {
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
               // System.debug('**** reader.getLocalName(): '+reader.getLocalName());
                              
               if (reader.getLocalName() == sxmltag) {
                  reader.next();
                  // System.debug('**** reader.getEventType(): '+reader.getEventType());
                      if (reader.getEventType() == XmlTag.characters) {
                           retValue = reader.getText();    
                      }
                } else {
                  reader.next();
                  if (reader.getEventType() == XmlTag.characters) {
                      System.debug('**** Info about the error message or other: '+reader.getText());    
                  }
                }
            }
       
            reader.next();
        }
        return retValue;
    }

    public Pagereference getTagSelected() {
        return null;
    }

    /**
    * Build the bitly endpoint information for integration.
    *
    * @return String endpoint for the bitly
    */
    private String buildTagBitlyInfo(String urlInfo) {
        String newEndpoint = '';

        if(urlInfo == null || urlInfo.trim().equals('')) {

            newEndpoint = bitly.SCS_Bitly_Endpoint__c + '?' 
                            + 'version=' + bitly.SCS_Bitly_Version__c + '&'
                            + 'format=' + bitly.SCS_Bitly_Format__c + '&'
                            + 'history='+ bitly.SCS_Bitly_History__c + '&'
                            + 'longUrl=' + urlInfo + '&'
                            + 'login=' + bitly.SCS_Bitly_Login__c + '&'
                            + 'apiKey=' + bitly.SCS_Bitly_ApiKey__c;
        } else {
            
           newEndpoint = bitly.SCS_Bitly_Endpoint__c + '?' 
                            + 'version=' + bitly.SCS_Bitly_Version__c + '&'
                            + 'format=' + bitly.SCS_Bitly_Format__c + '&'
                            + 'history='+ bitly.SCS_Bitly_History__c + '&'
                            + 'longUrl=' + EncodingUtil.URLENCODE(urlInfo,'UTF-8') + '&'
                            + 'login=' + bitly.SCS_Bitly_Login__c + '&'
                            + 'apiKey=' + bitly.SCS_Bitly_ApiKey__c;
        }

        return newEndpoint;
    }

    /**
    * Build the KLM business requirement for tagging. 
    * To be used for analytics, e.g. which 'Social Media' Twitter, Facebook, etc.
    *
    * @return String tag to be appended on the URL
    */
    private String buildTagLogic(String urlInfo) {
        String tagString = '';

        tagString = bitlyTag.SCS_Tag_Appended__c;        

        if(myCaseId == 'WeChat') {

            tagString = tagString.replace('param1', 'L');
            tagString = tagString.replace('param2', 'CN');
            tagString = tagString.replace('param5', 'SocialCommerce');
            tagString = tagString.replace('param6', 'null');

        } else {

            tagString = tagString.replace('param1', 'C');
            tagString = tagString.replace('param2', 'WW');     
            tagString = tagString.replace('param5', 'Servicing'); // Should be recordType name      
            tagString = tagString.replace('param6', setTagParam6Value(urlInfo)); // Should be Femke Schreuders description
        }
        
        tagString = tagString.replace('param3', 'SocialCampaign');
        tagString = tagString.replace('param4', myCaseId); // Should be dynamic on Case level       
        tagString = tagString.replace('param7', 'null');
        tagString = tagString.replace('param8', 'null');

        return tagString;
    }

    /**
    * Requirement: Femke Schreuders
    * Use a Custom Setting bitly template tagging and replace the param.
    *
    * On the 6th place (where the value is string <null> as default). Salesforce will adjust the tag parameter.
    * Set the param6 value with <Planandbook> or <Preparefortravel>
    *
    * Example 6th parameter:
    * Value: <Planandbook> if the page is: http://www.klm.com/travel/nl_nl/plan_and_book/index.htm
    * Value: <Preparefortravel> if the page is: http://www.klm.com/travel/nl_nl/prepare_for_travel/index.htm
    *
    * @return String tag on the 6th parameter
    */
    private static String setTagParam6Value(String urlInfo) {
        String newTagParameter = 'null';
        Map<String, String> paramMap = new Map<String, String>();
        
        // Split the Custom settings key value pair and set the new param6 tag value
        for(String pair: bitlyTag.SCS_Tag_Param6__c.split(';')) {
            String[] kv = pair.split(',');
            
            if(urlInfo.contains(kv[0])) {
                newTagParameter = kv[1];
            }
        }
        return newTagParameter;
    }

    /**
    * Use a Custom Setting bitly template for removing parameters in URL.
    * Remove internal campaign tracking from the URL’s e.g. WT.ac or WT.seg_3:
    *
    * @return String without WT.ac or WT.seg_3
    */
    private static String stripTagWTSegWTAc(String urlInfo) {
        String newBuildURL = '';
        
        // Split URL by tokens and sort it
        Map<String, String> tokenURLS = new Map<String, String>();
        Integer urlItems = 1;
        String itemKey = '';
        
        for(String urlToken: urlInfo.split('[?]')) {
            itemKey = 'item'+String.valueOf(urlItems);

            // Skip the first url token since it is the base http://... after add the '?' for rebuilding the URL
            if(urlItems == 1) {
                tokenURLS.put(itemKey, urlToken);
            } else {
                tokenURLS.put(itemKey, '?' + urlToken);
            }
            
            for(String tokenToRemove: bitlyTag.SCS_Tag_Remover__c.split(';')) {
       
                if(tokenURLS.get(itemKey).contains(tokenToRemove)) {
                    tokenURLS.remove(itemKey);
                    break;
                }
            }
            urlItems++;
        }
        
        // Build up the URL
        List<String> sortUrlKeySet = new List<String>();
        sortUrlKeySet.addAll(tokenURLS.keySet());
        sortUrlKeySet.sort();
        
        for(String sortedKey: sortUrlKeySet) {
            newBuildURL = newBuildURL + tokenURLS.get(sortedKey);
        }
        
        return newBuildURL;
    }

    // TODO: Christiaan-van-de.Koppel@klm.com, see documentation for Webtrends where # needs to be put on the end
    /* private static String addHashTagToEnd(String buildUrl) {
        String hashTagAppendedUrl = '';
        String itemKey = '';
        Integer urlItems = 1;
        
        Map<String, String> tokenURLS = new Map<String, String>();
        Map<String, String> tokenHashTags = new Map<String, String>();
        
        // First split it by ? tokens again
        for(String urlToken: buildUrl.split('[?]')) {
            itemKey = 'item'+String.valueOf(urlItems);
            
            if(urlItems == 1) {
                tokenURLS.put(itemKey, urlToken);
            } else {
                tokenURLS.put(itemKey, '?' + urlToken);
            }
            
            // Put the # tags at the end of the line
            if(urlToken.contains('#')) {

                Integer hashTagCounter = 1;
                
                for(String hashToken: urlToken.split('[#]')) {
                    // Replace the information before the hashtag on the first one, else add it to the token hash tag map
                    if(hashTagCounter == 1) {
                        tokenURLS.put(itemKey, hashToken);
                    } else {
                        tokenHashTags.put('hashtagnr'+hashTagCounter, '#' + urlToken);
                    }
                    hashTagCounter++;
                }
            }
        }               

        List<String> sortUrlKeySet = new List<String>();
        sortUrlKeySet.addAll(tokenURLS.keySet());
        sortUrlKeySet.sort();

        for(String tokenKey: sortUrlKeySet) {
            hashTagAppendedUrl = hashTagAppendedUrl + tokenURLS.get(tokenKey);
        }

        sortUrlKeySet.clear();
        sortUrlKeySet.addAll(tokenHashTags.keySet());
        sortUrlKeySet.sort();

        for(String tokenHashKey: sortUrlKeySet) {
            hashTagAppendedUrl = hashTagAppendedUrl + tokenHashTags.get(tokenHashKey);
        }

        return hashTagAppendedUrl;
    } */

    /**
    * Gets the caseId from the Console when Case is openend on Event.
    * Uses the Origin, e.g. Twitter, Facebook to set the 4th tag parameter
    *
    * @return PageReference
    */ 
    public PageReference caseIdConsole(){
        try {
            if(!myCaseId.equals('null')) {
                this.myCaseId = [SELECT Origin FROM Case WHERE Id = :myCaseId].Origin;
            }
        } catch(Exception e) {
            System.debug('**** Exception: '+e);
        }
        
        return null;
    }
    
    public PageReference ClearBitly(){
        
        orgURL = shortUrl = surveylink= '';
        
        return null;
    }
    /**
    * Gets the active CaseId from the Console 
    * Form the URL for Survey which was created by external site with active CaseID
    * Display the Bitly Shortened URL
    * @return PageReference
    */ 
    public PageReference activeCase(){
        system.debug('&&&'+activeCaseID);
        if(activeCaseID!='null' ){
            String longURL=Label.AFKLM_SurveyExternalSiteURL+activeCaseID;
            XmlStreamReader reader;
            HttpResponse res;
            //First, build the http request
            Http h = new Http();
            HttpRequest req = buildWebServiceRequest(longURL);
            res = invokeWebService(h, req);
            system.debug('###'+res); 
            reader = res.getXmlStreamReader(); 
            surveylink= readXMLResponse(reader,'shortUrl');
            System.debug('***'+surveylink);
        }
        else
            surveylink=null;
        return null;
    }
     
    public PageReference formSurveyURL(){
        return null;
    
    }
    
}