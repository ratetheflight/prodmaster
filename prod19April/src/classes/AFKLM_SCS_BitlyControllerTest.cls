/********************************************************************** 
 Name:  AFKLM_SCS_BitlyControllerTest
 Task:    N/A
 Runs on: AFKLM_SCS_BitlyController, BitlyWebServiceMockImpl
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          21/05/2014      Initial Development
    1.1     Stevano Cheung      17/03/2015      Created BitlyWebServiceMockImpl and fixing test classes coverage
***********************************************************************/
@isTest(SeeAllData=true) // For Custom Settings 'SeeAllData'
private class AFKLM_SCS_BitlyControllerTest {

    static testMethod void buildWebServiceRequestNoCaseIdTest() {
    	String generatedBitlyString = 'http://klmf.ly/1F1NvEY';
		customSettingsLoad('AFBitly','live');
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController();
        bitlyController.orgUrl = 'http://www.airfrance.com';
        
        Test.startTest();
        bitlyController.myCaseId = null; 
        Test.setMock(HttpCalloutMock.class, new BitlyWebServiceMockImpl());
        bitlyController.buildWebServiceRequest(bitlyController.orgUrl);
        bitlyController.getbitly();
        Test.stopTest();
        
		//system.assertEquals(bitlyController.shortUrl, generatedBitlyString);
    }

    static testMethod void buildWebServiceRequestWTSegWTAcRemovalTest() {
    	String generatedBitlyString = 'http://klmf.ly/1234HGT';
    	Case cs = new Case(Status='New', Origin='Facebook');
        insert cs;
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController(new ApexPages.StandardController(cs));

        Test.startTest();
		customSettingsLoad('Bitly', 'live');
        bitlyController.orgUrl = 'http://www.klm.com/customer_support?WT.ac=12345?movie=nice#cool?favorite=12453';
        Test.setMock(HttpCalloutMock.class, new BitlyWebServiceMockImpl());
        bitlyController.buildWebServiceRequest(bitlyController.orgUrl);
        bitlyController.getbitly();
        Test.stopTest();
        
		system.assertEquals(bitlyController.shortUrl, generatedBitlyString);
    }

    static testMethod void buildWebServiceRequestTest() {
    	String generatedBitlyString = 'http://klmf.ly/1234HGT';
    	Case cs = new Case(Status='New', Origin='Facebook');
        insert cs;
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController(new ApexPages.StandardController(cs));

        Test.startTest();
		customSettingsLoad('Bitly', 'live');
        bitlyController.orgUrl = 'http://www.klm.com';
        Test.setMock(HttpCalloutMock.class, new BitlyWebServiceMockImpl());
        bitlyController.buildWebServiceRequest(bitlyController.orgUrl);
        bitlyController.getbitly();
        Test.stopTest();
        
		//system.assertEquals(bitlyController.shortUrl, generatedBitlyString);
    }
    
    static testMethod void buildWebServiceRequestParam6ReplaceTest() {
    	String generatedBitlyString = 'http://klmf.ly/1234HGT';
    	Case cs = new Case(Status='New', Origin='Facebook');
        insert cs;
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController(new ApexPages.StandardController(cs));

        Test.startTest();
		customSettingsLoad('Bitly', 'live');
        bitlyController.orgUrl = 'http://www.klm.com/plan_and_book';
        Test.setMock(HttpCalloutMock.class, new BitlyWebServiceMockImpl());
        bitlyController.buildWebServiceRequest(bitlyController.orgUrl);
        bitlyController.getbitly();
        Test.stopTest();
        
		system.assertEquals(bitlyController.shortUrl, generatedBitlyString);
    }
    
    static testMethod void buildWebServiceRequestDemoTest() {
    	String generatedBitlyString = null;
    	Case cs = new Case(Status='New', Origin='Facebook');
        insert cs;
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController(new ApexPages.StandardController(cs));

        Test.startTest();
		customSettingsLoad('Bitly', 'demo');
        bitlyController.orgUrl = 'http://www.klm.com';
        Test.setMock(HttpCalloutMock.class, new BitlyWebServiceMockImpl());
        bitlyController.buildWebServiceRequest(bitlyController.orgUrl);
        bitlyController.getbitly();
        Test.stopTest();
        
		//system.assertEquals(bitlyController.shortUrl, generatedBitlyString);
    }
    
    static testMethod void buildWebServiceRequestCustomerSupportTest() {
    	String generatedBitlyString = 'http://klmf.ly/1234HGT';
    	Case cs = new Case(Status='New', Origin='Facebook');
        insert cs;
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController(new ApexPages.StandardController(cs));

        Test.startTest();
		customSettingsLoad('Bitly', 'live');
        bitlyController.orgUrl = 'http://www.klm.com/customer_support';
        Test.setMock(HttpCalloutMock.class, new BitlyWebServiceMockImpl());
        bitlyController.buildWebServiceRequest(bitlyController.orgUrl);
        bitlyController.getbitly();
        Test.stopTest();
        
		system.assertEquals(bitlyController.shortUrl, generatedBitlyString);
    }

    static testMethod void getTagSelectedTest() {
    	Case cs = new Case(Status='New', Origin='Facebook');
        insert cs;
        PageReference pr;
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController(new ApexPages.StandardController(cs));
        pr=bitlyController.getTagSelected();
        System.assertEquals(pr, null);
    }

    static testMethod void caseIdConsoleTest() {
        Case cs = new Case(Status='New', Origin = 'Facebook');
        insert cs;
        
        AFKLM_SCS_BitlyController bitlyController = new AFKLM_SCS_BitlyController(new ApexPages.StandardController(cs));
        bitlyController.caseIdConsole();
        bitlyController.ClearBitly();
    }

    static void customSettingsLoad(String settingsName, String mode) {
        AFKLM_SCS_Bitly__c bitly = AFKLM_SCS_Bitly__c.getValues(settingsName);
        bitly.Name = settingsName;
        bitly.SCS_Bitly_Mode__c = mode;
        bitly.SCS_Bitly_Endpoint__c = 'https://api.bitly.com';
        bitly.SCS_Bitly_Format__c = 'xml';
        bitly.SCS_Bitly_History__c = '1';
        bitly.SCS_Bitly_Version__c = '2.0.1';
        
		AFKLM_SCS_Bitly__c bitlyTag = AFKLM_SCS_Bitly__c.getValues('Tag');
        bitlyTag.SCS_Tag_Remover__c = 'WT.ac;WT.seg_3';
        bitlyTag.SCS_Tag_Appended__c = '?WT.mc_id=param1_param2_param3_param4_param5_param6_param7_param8&WT.tsrc=param3';
        bitlyTag.SCS_Tag_Param6__c = 'plan_and_book,Planandbook;prepare_for_travel,Preparefortravel;destinations,Destinations';
    }
}