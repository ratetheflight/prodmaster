/********************************************************************** 
 Name:  AFKLM_SCS_CaseAfterTriggerTest
 Runs on: AFKLM_SCS_CaseAfterTrigger,AFKLM_CaseAfterTriggerHandler
====================================================== 
Purpose: 
    This class contains unit tests for validating the case after trigger
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR            DATE            DETAIL                                 
    1.0     Nagavi Babu       13/02/2016      Initial Development
    1.1     Narmatha          05/07/2016      iPad related class-validateCaseAfteriPadOnBoard()
***********************************************************************/
@isTest
private class AFKLM_SCS_CaseAfterTriggerTest{
    
   static testMethod void validateCaseAfterInsertError() {
        AFKLM_TestDataFactory.loadCustomSettings();
        
        //Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(2,'In Progress');
        insert caseList;
        List<Case> tempcaseList= new List<Case>();
        tempcaseList=AFKLM_TestDataFactory.insertKLMCase(1,'In Progress');
               
        try {
            insert tempcaseList;            
        } 
        catch(Exception ex) {
          System.Assert(ex.getMessage().contains('You already have a case In Progress with Case Number :caseList[1].casenumber'));
        }
    }
    
    static testMethod void validateCaseAfterBulkInsertError() {
        AFKLM_TestDataFactory.loadCustomSettings();
        
        //Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(100,'New');
        insert caseList;
        
        List<Case> tempcaseList= new List<Case>();
        tempcaseList=AFKLM_TestDataFactory.insertKLMCase(20,'In Progress');
                
        try {
            insert tempcaseList;            
        } 
        catch(Exception ex) {
          System.Assert(ex.getMessage().contains('You already have a case In Progress with Case Number :'));
        }
    }
    
    static testMethod void validateCaseAfterUpdateError() {
        AFKLM_TestDataFactory.loadCustomSettings();
        User usr = AFKLM_TestDataFactory.createUser('Test','CaseUser','System Administrator');
        insert usr;
        //Insert Cases
        System.runAs(usr){
        
        Test.startTest();

            List<Case> caseList= new List<Case>();
            caseList=AFKLM_TestDataFactory.insertKLMCase(1,'In Progress');
            insert caseList;

            List<user> userList = [SELECT Id,Last_Case_In_Progress__c FROM User WHERE Id = :UserInfo.getUserId()]; 
            
            Case testObj1 = [select Status, isclosed,casenumber from Case WHERE id = :caseList[0].id];

            System.assertEquals(testObj1.casenumber, userList[0].Last_Case_In_Progress__c, '(1) Casenumber does not match with what is in Last_Case_In_Progress__c field');

            try {
                List<Case> caseList2= new List<Case>();
                caseList2=AFKLM_TestDataFactory.insertKLMCase(1,'New');
                insert caseList2;
                caseList2[0].Status='In Progress';
                update caseList2[0];

                List<user> userList1 = [SELECT Id,Last_Case_In_Progress__c FROM User WHERE Id = :UserInfo.getUserId()]; 

                Case testObj2 = [select Status, isclosed,casenumber from Case WHERE id = :caseList2[0].id];

                System.assertEquals(testObj1.casenumber+','+testObj2.casenumber, userList1[0].Last_Case_In_Progress__c, '(2) Casenumber does not match with what is in Last_Case_In_Progress__c field');

                           
            } 
            catch(Exception ex) {
              system.debug(ex.getMessage());
              System.Assert(ex.getMessage().contains('You already have a case In Progress with Case Number :'));
            }      
        Test.stopTest();
    }
}
    
    static testMethod void validateCaseAfterBulkUpdateError() {
        AFKLM_TestDataFactory.loadCustomSettings();
        User usr = AFKLM_TestDataFactory.createUser('Test','CaseUser','System Administrator');
        insert usr;
        //Insert Cases
        System.runAs(usr){
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(1,'In Progress');
        insert caseList;
        
        try {
            List<Case> caseList2= new List<Case>();
            caseList2=AFKLM_TestDataFactory.insertKLMCase(100,'New');
            insert caseList2;
            
            List<Case> updateCaseList= new List<Case>();
            for(integer i=5;i<=50;i++){
                caseList2[i].Status='In Progress';
                updateCaseList.add(caseList2[i]);
            }
            update updateCaseList;
                       
        } 
        catch(Exception ex) {
          system.debug(ex.getMessage());
          System.Assert(ex.getMessage().contains(Label.AFKLM_Case_In_Progress_Error));
        }      
        }
    }
    
    static testMethod void validateCaseAfterUpdateError2() {
        AFKLM_TestDataFactory.loadCustomSettings();
        
        //Insert Cases
                
        List<Case> caseList2= new List<Case>();
        caseList2=AFKLM_TestDataFactory.insertKLMCase(1,'In Progress');
        insert caseList2;
       
        caseList2[0].Status='New';
        update caseList2[0];            
        
    }
    
  /*  static testMethod void validateCaseAfterDeleteError() {
        AFKLM_TestDataFactory.loadCustomSettings();
        
        //Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(2,'In Progress');
        insert caseList;
        
        delete caseList;
    }*/
    
   /* static testMethod void validateCaseAfterBulkDeleteError() {
        AFKLM_TestDataFactory.loadCustomSettings();
        
        //Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(100,'New');
        insert caseList;
        
        caseList[10].Status='In Progress';
        update caseList[10];
        
        List<Case> deleteCaseList= new List<Case>();
        for(integer i=5;i<=50;i++){
            deleteCaseList.add(caseList[i]);
        }
        delete deleteCaseList;
    }*/
    static testMethod void validateCaseAfteriPadOnBoard() {
        AFKLM_TestDataFactory.loadCustomSettings();
        AFKLM_TestDataFactory.insertWSMangerSetting();
        //insert Person Account
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('2016825763');
        insert insertaccount;
        
        //Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertiPadonBoardCase(5);
        caseList[0].accountID=insertaccount.id;
        caseList[0].PNR__c='65EVPM';
        insert caseList;     
        try{
            Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());  
        }
        catch(Exception e)
        {
            system.debug('Exception e:'+e);
        }
    }
     
}