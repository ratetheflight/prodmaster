public with sharing class AFKLM_SCS_CaseOwnerFutureMethod {

  public AFKLM_SCS_CaseOwnerFutureMethod() {}

  @future
  public static void setCaseOwner(Set<String> listOfIDs) {
    //Set<Case> checkCases = new Set<Case>();
    //List<Case> casesToUpdate = new List<Case>();
    Map<String, Case> caseIDs = new Map<String, Case>();
    Map<String, Case> caseIDToUpdate = new Map<String, Case>();

    List<Case> listCases = [SELECT id, OwnerId FROM Case WHERE id IN: listOfIDs];

    for(Case cs : listCases){
        caseIDs.put(cs.Id, cs);
    }


    List<SocialPost> lsp = [SELECT Id, OwnerId, ParentId, CreatedDate FROM SocialPost WHERE ParentId IN: listOfIDs AND MessageType = 'Private' ORDER BY CreatedDate ASC];
    for(SocialPost sp : lsp){
      if(caseIDs.get(sp.ParentId) != null){
        caseIDs.get(sp.ParentId).OwnerId = sp.OwnerId;
        if(!caseIDToUpdate.containsKey(caseIDs.get(sp.ParentId).id)){
          //checkCases.add(caseIDs.get(sp.ParentId));
          caseIDToUpdate.put(caseIDs.get(sp.ParentId).id,caseIDs.get(sp.ParentId));
        }
      }
    }

    if(!caseIDToUpdate.isEmpty()){
      //casesToUpdate.addAll(caseIDToUpdate.values());

      update caseIDToUpdate.values();
    }
  }
}