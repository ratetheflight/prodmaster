@isTest
private class AFKLM_SCS_CaseOwnerFutureMethodTest {

	private static Group g;
	private static QueueSObject q;
	private static QueueSObject qq;

	private static List<Case> listOfCases = new List<Case>();

	
	static testMethod void setCaseOwnerTest() {
		Set<String> listOfIDs = prepareData();

		//test.startTest();
		AFKLM_SCS_CaseOwnerFutureMethod cofm = new AFKLM_SCS_CaseOwnerFutureMethod();
		//test.stopTest();
		Test.startTest();
		AFKLM_SCS_CaseOwnerFutureMethod.setCaseOwner(listOfIDs);
		Test.stopTest();
		List<Case> caseList = [SELECT id, OwnerId FROM Case WHERE Id IN: listOfIDs];

		for(Case cs : caseList){
			System.assertEquals(g.id, cs.OwnerId);
		}
		
	}
	
	private static Set<String> prepareData() {
		Set<String> listOfIDs = new Set<String>();
		
		List<SocialPost> listOfPosts = new List<SocialPost>();

		//Queue - social post/case owner
		g = new Group(Type='Queue', Name='Testing queue');
		insert g;

		q = new QueueSObject(SobjectType='SocialPost', QueueId=g.Id);
		insert q;

		qq = new QueueSObject(SobjectType='Case', QueueId=g.Id);
		insert qq;

		//Multiple cases
		for(Integer c=1;c<=10;c++){
			Case cs = new Case(
				Status = 'New',
				Subject = 'Testing Case '+c
				);

			listOfCases.add(cs);
		}
		insert listOfCases;

		Integer x = 1;
		Datetime dt = System.now();

		//Multiple social posts
		for(Case tstCase : listOfCases){
			SocialPost sp1 = new SocialPost(
				Name = 'Private message from Testing User no. '+x ,
				MessageType = 'Private',
				Handle = 'Testing User',
				Posted = dt + x,
				OwnerId = g.Id,
				ParentId = tstCase.id
				);
			listOfPosts.add(sp1);

			SocialPost sp2 = new SocialPost(
				Name = 'Private message from Testing User no. '+x ,
				MessageType = 'Private',
				Handle = 'Testing User',
				Posted = dt + x,
				OwnerId = g.Id,
				ParentId = tstCase.id
				);
			listOfPosts.add(sp2);
			x++;
		}

		insert listOfPosts;

		for(SocialPost sp : listOfPosts){
			if(!listOfIDs.contains(sp.ParentId)){
				listOfIDs.add(sp.ParentId);
			}
		}

		return listOfIDs;
	}
	
}