public with sharing class AFKLM_SCS_CaseRecordTypeSingleton {
    private static final AFKLM_SCS_CaseRecordTypeSingleton classInstance = new AFKLM_SCS_CaseRecordTypeSingleton();

    public RecordType klmRt{get;private set;}
    public RecordType afRt{get;private set;}

    private AFKLM_SCS_CaseRecordTypeSingleton() {

        for(RecordType rc: [SELECT Id, Name FROM RecordType WHERE (Name = 'AF Servicing' OR Name = 'Servicing') AND SobjectType = 'Case' LIMIT 2]){

            if(rc.Name == 'Servicing') {
                klmRt = rc;
            } else if (rc.Name == 'AF Servicing') {
                afRt = rc;
            }
        }
    }

    public static AFKLM_SCS_CaseRecordTypeSingleton getInstance() {
        return classInstance;
    }
}