/**********************************************************************
 Name:  AFKLM_SCS_CaseSummaryExtension
 Task:    N/A
 Runs on: SCS_CaseHighlightConsole
======================================================
Purpose: 
        The Case highlight console extension which is custom Visualforce build
        
        Note: Performance issue on formula fields 'sf4twitter__Fcbk_User_Id__pc'
        - It is possible to ask SF to create Custom Index: https://developer.salesforce.com/blogs/engineering/2013/02/force-com-soql-best-practices-nulls-and-formula-fields.html
        - Reference: http://help.salesforce.com/apex/HTViewSolution?id=000181277&language=en_US

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          Unknown         INITIAL DEVELOPMENT
    1.1     Stevano Cheung      07/08/2014      Fixed query execution. First use the parent Id for getting the profileURL. After the non-indexed formula field sf4twitter__Fcbk_User_Id__pc
    1.2     Stevano Cheung      02/12/2014      Added Klout feature score  
    1.3     Ata                 09/06/2016      Influencer score
    1.4     Ata                 06/09/2016      Added tier n booking Icons on the case highlights 
    1.5     Prasanna            25/10/2016      Updated the influencer score field from formula to picklist
***********************************************************************/
public class AFKLM_SCS_CaseSummaryExtension {
    private final Case cs;
    private final Account acc;
    private Integer kloutScr = 0;
    //Added by ata
    public integer InfluencerNo {get;set;}
    public string tier {get;set;}
    public string booking {get;set;}
    
    public AFKLM_SCS_CaseSummaryExtension (ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'AccountId','isClosed','Id'});
        }
        this.cs = (Case) stdController.getRecord();
         //added Influencer_score__c in the query by Ata
         List<Account> acc = [SELECT id,
                                     sf4twitter__Fcbk_User_Id__pc,
                                     Influencer_score__c,
                                     tier_level__c,
                                     Booking_Class__c,
                                     Klout_Score__c 
                                     FROM 
                                     Account 
                                     WHERE 
                                     id=:cs.AccountId LIMIT 1];
         //added by Ata to get Influencer score on VF
         //Updated by Prasanna to check whether case is open
         if(cs.isclosed != false){
        List<SocialPost> spt=[SELECT id,parent.id,tier_level__c,booking_class__c,Influencer_Score__c,Klout_score__c from socialpost where parent.id=:cs.id and (SCS_Status__c <> '') limit 1];
        if(spt.size() >0){
        InfluencerNo  = Integer.valueof(spt[0].Influencer_score__c);    
        if(spt[0].tier_level__c != '' && spt[0].tier_level__c != Null)
                 tier  = spt[0].tier_level__c;
             
             if(spt[0].Booking_Class__c!= '' && spt[0].Booking_Class__c != Null)
                 booking = spt[0].Booking_Class__c;
                 
             if(spt[0].Klout_Score__c != Null)
                 kloutScr = Integer.valueof(spt[0].Klout_Score__c);
             else
                 kloutScr = 0;  
        }    
             
             
         }
         
         else if(cs.isclosed !=true){
         if(acc.size() > 0){
             InfluencerNo  = Integer.valueof(acc[0].Influencer_score__c);
             
             if(acc[0].tier_level__c != '' && acc[0].tier_level__c != Null)
                 tier  = acc[0].tier_level__c;
             
             if(acc[0].Booking_Class__c!= '' && acc[0].Booking_Class__c != Null)
                 booking = acc[0].Booking_Class__c;
                 
             if(acc[0].Klout_Score__c != Null)
                 kloutScr = Integer.valueof(acc[0].Klout_Score__c);
             else
                 kloutScr = 0;
         }
         }
    }
   
    public String getProfileURL(){
        String profurl;
        //added Influencer_score__c in the query by Ata
        List<Account> acc = [SELECT id,sf4twitter__Fcbk_User_Id__pc,Influencer_score__c FROM Account WHERE id=:cs.AccountId LIMIT 1];   
        try{  
               
            if(cs.AccountId != null) {
                profurl = [SELECT ProfileURL FROM SocialPersona WHERE parentId =: cs.AccountId].ProfileURL;
            } else {
                profurl = [SELECT ProfileURL FROM SocialPersona WHERE FB_User_Id__c=: acc[0].sf4twitter__Fcbk_User_Id__pc].ProfileURL;
            }
        } catch(Exception e){
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'No Social Persona attached to the account or incorrect ID. ' ) );
            System.Debug(e.getMessage());
        }
        return profurl;
    }
    
    public /*Integer*/void getKloutScore() {
        Integer kloutScore = 0;
        System.debug('M in action method!');
        List<Account> acc = [SELECT id, sf4twitter__Twitter_User_Id__pc, sf4twitter__Twitter_Username__pc,Klout_Score__c FROM Account WHERE id=:cs.AccountId AND Bypass_Klout_Update__c = false LIMIT 1];
        
        try {
            if(cs.AccountId != null && acc != Null && acc.size() > 0) {
                String kloutId = '';
                if(acc[0].sf4twitter__Twitter_User_Id__pc != null) {
                    system.debug('1st if+'+acc[0].sf4twitter__Twitter_User_Id__pc);
                    kloutId = (String) kloutObjects('http://api.klout.com/v2/identity.json/tw/'+acc[0].sf4twitter__Twitter_User_Id__pc+'?key=us4k68rumkgwwnqkk797hsm4').get('id');
                    
                } else if(acc[0].sf4twitter__Twitter_Username__pc != null) {
                    system.debug('1st if+'+acc[0].sf4twitter__Twitter_Username__pc );
                    kloutId = (String) kloutObjects('http://api.klout.com/v2/identity.json/twitter?screenName='+acc[0].sf4twitter__Twitter_Username__pc+'&key=us4k68rumkgwwnqkk797hsm4').get('id');
                }
                system.debug('NEW KLOUT:::'+kloutid);
                if(!''.equals(kloutId)) {
                    Decimal kloutDecimal = Decimal.valueOf(Double.valueOf(kloutObjects('http://api.klout.com/v2/user.json/'+kloutId+'/score?key=us4k68rumkgwwnqkk797hsm4').get('score')));
                    kloutScore = Integer.valueOf(kloutDecimal.divide(1, 0, System.RoundingMode.UP));
                    kloutScr = kloutScore;
                    saveKloutScore();
                    // kloutScore = Integer.valueOf(kloutObjects('http://api.klout.com/v2/user.json/'+kloutId+'/score?key=us4k68rumkgwwnqkk797hsm4').get('score'));
                }
            }
        } catch(Exception e) {
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'Klout Score error. ' ) );
            System.Debug(e.getMessage());
        }

        //return kloutScore;
    }
    
    private Map<String, Object> kloutObjects(String endpoint) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('GET');
        
        HttpResponse res = h.send(req);
        return (Map<String, Object>) JSON.deserializeUntyped(res.getbody());
    }
    
    private void saveKloutScore(){
        
        Account account = [SELECT Klout_Score__c, Influencer__c FROM Account WHERE id =: cs.AccountId LIMIT 1];
        if(account.Klout_Score__c != kloutScr){
            account.Klout_Score__c = kloutScr;
            if(kloutScr >= 60 && account.Influencer__c == false){
                account.Influencer__c = true;
            }
            update account;
        }
    }   
    
    public Integer getKloutScoreNumber(){
        return kloutScr;
    }
}