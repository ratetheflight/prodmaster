/********************************************************************** 
 Name:  AFKLM_SCS_ControllerHelper
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPostController
====================================================== 
Purpose: 
    Helper class for Social Post Controller.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      06/02/2014      Initial Development
    1.1     Ivan Botta          07/02/2014      Added Javadoc comments 
    1.2     Nagavi              06/07/2016      Added logic to check the is campaign flag of case based on social post - JIRA:143
    1.3     Nagavi              15/09/2016      Urgent Case Automation :ST=803
    1.4     Sai Choudhry        12/10/2016      Populating Chat hash for AF-WeChat Cases. JIRA ST-484
    1.5     Sai Choudhry        01/11/2016      Populating Mark As reviewed date. JIRA ST-1231 
    1.6     Nagavi              01/31/2016      ST-1358 - Development- Case Topic Filling by DG in UAT
    1.7     Prasanna            02/15/2017      ST-1762 - Populating first post type of the case
    1.8     Sai Choudhry        23/02/2017      Attaching the existing case on engage. JIRA ST-1645
    1.9     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868 & ST-1869
***********************************************************************/
public with sharing class AFKLM_SCS_ControllerHelper {
    private static final Integer DEFAULT_CASE_CLOSED = 5;
    public List<AFKLM_SCS_SocialPostWrapperCls> socPostList {get;set;} 

    //Manuel Conde ST-002786
    //AFKLM_SCS_CaseRecordTypeSingleton crt = AFKLM_SCS_CaseRecordTypeSingleton.getInstance();
    //added by sai.Removing the SOQL query for Record Type, JIRA ST-1868 & ST-1869
    public string rtKLM = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
    public string rtAF = Schema.SObjectType.case.getRecordTypeInfosByName().get('AF Servicing').getRecordTypeId();

    
    
    /** 
    * Helper class method to mark the SocialPosts as reviewed for wrapper controller
    *
    * @param socPostList - List of AFKLM_SCS_SocialPostWrapperCls object to be selected for mark as reviewed
    * @param isSpam - Boolean field to set if the Social Post is Spam.
    */
    public void markReviewed(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, List<SocialPost> listSocialPosts, Boolean isSpam) {
        List<SocialPost> updateSocPostObjList = new List<SocialPost>();

        if(listSocialPosts != null && listSocialPosts.size() > 0){
            for(SocialPost sp : listSocialPosts){
                sp.SCS_MarkAsReviewed__c = true;
                sp.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                sp.SCS_Status__c = 'Mark as reviewed';
                //added by sai.Populating Mark As reviewed date. JIRA ST-1231 
                sp.Mark_As_Reviewed_Date__c = Datetime.now();
                sp.Ignore_for_SLA__c = true;
                sp.OwnerId = userInfo.getUserId();
  
                if(isSpam) {
                    sp.SCS_Spam__c  = isSpam;
                }
              
                updateSocPostObjList.add(sp);
            }
        } else {

            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList){
                if(wrapperSocialPost.isSelected == true){
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark as reviewed';
                    //added by sai.Populating Mark As reviewed date. JIRA ST-1231.
                    wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
      
                    if(isSpam) {
                        wrapperSocialPost.cSocialPost.SCS_Spam__c  = isSpam;
                    }
                  
                    updateSocPostObjList.add(wrapperSocialPost.cSocialPost);
                }
            }
        }

        if(updateSocPostObjList != null && updateSocPostObjList.size() > 0) {
            try {
                // SocialCustomerServiceUtils.userSetSkipValidation(true);          
                update updateSocPostObjList;
                // SocialCustomerServiceUtils.userSetSkipValidation(false);
                socPostList = null;
            } catch(Exception ex) {
                System.debug('AFKLM_SCS_ControllerHelper - markReviewed: Exception in updating Social Post: '+ex.getMessage());
            } finally {
                //SocialCustomerServiceUtils.userSetSkipValidation(false);
            }       
        }
    }
    
    
    /** 
    * Helper class method to create new cases for the SocialPost in the Controller
    * Method is called by agent clicking on "Engage" button, or when new Wall Post or Private Message is created.
    * 
    *@param postMap - map consists of SocialPost ID and SocialPost
    *@param newCaseId - Id of the new Case
    *@param newCaseNumber - Number of new Case
    *@param error - error message
    *
    *@return returnCaseId as a List value
    */
    public List<String> createNewCases(Map<String, SocialPost> postMap, String newCaseId, String newCaseNumber, Boolean error) {
        String errorMessage = '';
        Map<String, Case> case2insert = new Map<String, Case>();
        List<String> returnCaseId = new List<String>();
        
        System.debug('--+ postMap: '+postMap);

        for( SocialPost post: postMap.values() ){
            if( post.ParentId != null ){
                newCaseId = post.ParentId;
                error = true;
            } else {
                if( post.ParentId == null ){
                    Case newCase = checkCreatePersonAccount(post);
                    case2insert.put(post.Id, newCase);
                }
            }
        }

        system.debug('--+ newCaseId: '+newCaseId);

        system.debug('************ case2insert.values **********: '+case2insert.values()); 
        //insert case2insert.values();
        upsert case2insert.values();
        // system.debug('  **********: '+case2insert.keySet());

        for( String x : case2insert.keySet() ){
            postMap.get( x ).ParentId = case2insert.get(x).Id;
            //postMap.get( x ).SCS_Status__c = 'Actioned';
            newCaseId = case2insert.get(x).Id;
            newCaseNumber = case2insert.get(x).CaseNumber;
            returnCaseId.add(newCaseId);
            System.debug('Return Case Id in controller Helper class '+returncaseId);
        }

        system.debug('--+ toReturn: '+returncaseId);

        update postMap.values();
        return returnCaseId;
    }
    
    /**
    * The "Person Account" should exist for the "Social Persona" (Master Detail).
    * If "Social Persona" parent name is "TemporaryPersonAccount": 
    *  - It will re-allocate and create a new "Person Account" when "Engage" button is selected by agents manually.
    *  - The SocialPost ownerid is changed to the user which press 'Engage' button or via automatic Radian6/SocialHub Inboundhandler 
    *
    * @param    SocialPost to check the handle name and create the PersonAccount if the handle name doesn't exist
    * @return   Case to be inserted
    */
    private Case checkCreatePersonAccount(SocialPost post) {
        Case newCase = new Case();
        // We need to query for validation of the fields between SocialPersona and PersonAccount.
        List<SocialPost> socialPost = [SELECT Id, Name,whoid,Content,SCS_IsOutbound__c,IsOutbound,SCS_Post_Tags__c,Proposed_Urgent_Keyword__c,Is_Campaign__c,Language__c, OwnerId, Provider, ExternalPostId, RecipientsNumber__c,Company__c, Entity__c, MessageType, TopicProfileName, Chat_Medium__c FROM SocialPost WHERE Id = :post.Id LIMIT 1];
        system.debug('*** socialPost'+socialPost );
        if('Twitter'.equals(socialPost[0].Provider)) {
            List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId,  Name, ParentId, followers, RealName, FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id
            List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,WhatsApp_User_Id__c, WhatsApp_Username__c,SinaWeibo_User_ID__c, SinaWeibo_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
            if (accountHandleNameList==null || accountHandleNameList.isEmpty()) {
                //DVTH not possible without an Account // 
                newCase = createSocialCase(socialPost, new Account(), socPersonaHandleNameList);
            } else {
                newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
            }
            socialPost[0].OwnerId = userInfo.getUserId();
            update socialPost;
        } else if('Facebook'.equals(socialPost[0].Provider)) {
            List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId, Name, ParentId, RealName, followers, FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id

            if(socPersonaHandleNameList.size() > 0) {
            
                List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,WhatsApp_User_Id__c, WhatsApp_Username__c,SinaWeibo_User_ID__c, SinaWeibo_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
                if (accountHandleNameList==null || accountHandleNameList.isEmpty()) {
                    //DVTH not possible without an Account // 
                    newCase = createSocialCase(socialPost, new Account(), socPersonaHandleNameList);
                } else {
                    newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
                }
            }
            else{
                //Manuel Conde ST-002786
                if(socialPost != null && socialPost[0].TopicProfileName != null){
                    if(socialPost[0].TopicProfileName.contains('France')){
                      //AirFrance
                      newCase.RecordTypeId = rtAF;
                    } else {
                      //KLM
                      newCase.RecordTypeId = rtKLM;
                    }
                }
                //Manuel Conde ST-002786

            }
            
            System.debug('Proposed_Urgent_Keyword__c'+socialPost[0].Proposed_Urgent_Keyword__c);
              // Added by Nagavi for Urgent Case Automation - 15th Sep: ST=803
            if(socialPost[0].Proposed_Urgent_Keyword__c!=null){
                newCase.Proposed_Urgent__c=true;
                system.debug('--+ Proposed_Urgent__c:'+newCase.Proposed_Urgent__c);
            }
            System.debug('newCase:Proposed_Urgent__c out'+newCase.Proposed_Urgent__c);
            
            if('Comment'.equals(socialPost[0].MessageType)){
                socialPost[0].OwnerId = userInfo.getUserId();
                update socialPost;
            }
            
            //Added by Nagavi for Jira:143
            if(socialPost[0].Is_Campaign__c==true){
                system.debug('**inside case check');
                newCase.Is_Campaign__c=true;
            }
            
        } else if('WeChat'.equals(socialPost[0].Provider)) {
            List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId,  Name, ParentId, followers, RealName,FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id
            List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,WhatsApp_User_Id__c, WhatsApp_Username__c,SinaWeibo_User_ID__c, SinaWeibo_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
            if (accountHandleNameList==null || accountHandleNameList.isEmpty()) {
                //DVTH not possible without an Account // 
                newCase = createSocialCase(socialPost, new Account(), socPersonaHandleNameList);
            } else {
                newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
            }
            socialPost[0].OwnerId = userInfo.getUserId();
            update socialPost;
        } else if('WhatsApp'.equals(socialPost[0].Provider)) {
            List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId,  Name, ParentId, followers, RealName,FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id
            List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,WhatsApp_User_Id__c, WhatsApp_Username__c,SinaWeibo_User_ID__c, SinaWeibo_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
            if (accountHandleNameList==null || accountHandleNameList.isEmpty()) {
                //DVTH not possible without an Account // 
                newCase = createSocialCase(socialPost, new Account(), socPersonaHandleNameList);
            } else {
                newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
            }
            socialPost[0].OwnerId = userInfo.getUserId();
            update socialPost;
        }/*else if('SinaWeibo'.equals(socialPost[0].Provider)) {
            List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId, Name, ParentId, RealName, FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id
            List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,Messenger_User_ID__c, Messenger_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
            newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
        }*/
        system.debug('***Case value:newCase '+newCase);
        return newCase;
    }

    /**
    * The helper method will create a new Case object. 
    * Also a new PersonAccount object is created if the TW, FB external Id from SocialPersona and PersonAccount object are not the same.  
    * Otherwise it will create a new Case object and attach the existing PersonAccount to the SocialPost.
    *
    * @param    socialPost to create a new case for and attach the newly created person account
    * @param    socialHandleFieldName the social handle fieldname from Account object to verify, e.g. TW, FB
    * @param    socPersonaHandleNameList the social handle of the persona to check with the appropriate person account handle name 
    * @return   Case the newly created case
    */
    private Case createSocialCase(List<SocialPost> socialPost, Account account, List<SocialPersona> socPersonaHandleNameList) {
        Case newCase = new Case();
        String companyName;
        //Modified by Sai.Removing the SOQL query for Record Type, JIRA ST-1869.
        String personAccountRecTypeId =Schema.SObjectType.account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
         //[SELECT Id FROM RecordType WHERE (Name='Person Account') AND (SobjectType='Account')].Id;
        Account newPersonAccount = new Account();
        String realName = '';
        
        if(socialPost[0].TopicProfileName != null){
            if(socialPost[0].TopicProfileName.contains('France')){
              companyName = 'AirFrance';
            } else {
              companyName = 'KLM';
            }
        }

        //DvtH13OKT2015 Truncate the name to the field size
        realName = socPersonaHandleNameList[0].RealName;
        if (realName!=null && realName.length()>80) {   
            realName = realName.subString(0,80);
        }

        if(!socPersonaHandleNameList[0].FB_User_ID__c.equals(account.sf4twitter__Fcbk_User_Id__pc) && 'Facebook'.equals(socialPost[0].Provider)) {
            List<Account> persAcc = [SELECT id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc =: socPersonaHandleNameList[0].FB_User_ID__c LIMIT 1];
            
            if(!persAcc.isEmpty()){
                socPersonaHandleNameList[0].ParentId=persAcc[0].id;
                update socPersonaHandleNameList[0];
                
                newPersonAccount = persAcc[0];
            } else {
                newPersonAccount = new Account(
                    LastName = realName,
                    RecordTypeId = personAccountRecTypeId,
                    sf4twitter__Fcbk_Username__pc = socPersonaHandleNameList[0].Name,
                    sf4twitter__Fcbk_User_Id__pc = socPersonaHandleNameList[0].FB_User_ID__c,
                    Company__c = companyName
                );
                
                insert newPersonAccount;
            }
            
            socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
            update socPersonaHandleNameList[0];
            
            String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
            newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
                   
            
            socialPost[0].WhoId = newPersonAccount.Id;
            update socialPost[0];
            

    } else if(!socPersonaHandleNameList[0].Name.equals(account.sf4twitter__Twitter_Username__pc) && 'Twitter'.equals(socialPost[0].Provider) && !socPersonaHandleNamelist[0].ExternalId.equals(account.sf4twitter__Twitter_User_Id__pc)) {
            newPersonAccount = new Account(
                LastName = realName,
                RecordTypeId = personAccountRecTypeId,
                sf4twitter__Twitter_Username__pc = socPersonaHandleNameList[0].Name,
                sf4twitter__Twitter_User_Id__pc = socPersonaHandleNameList[0].Externalid,
                Company__c = companyName
            );
            
            insert newPersonAccount;

            
            socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
            update socPersonaHandleNameList[0];
            
            String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
            newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
            
            socialPost[0].WhoId = newPersonAccount.Id;
            update socialPost[0];

    } else if(!socPersonaHandleNameList[0].ExternalId.equals(account.WeChat_User_ID__c) && 'WeChat'.equals(socialPost[0].Provider)) {
            newPersonAccount = new Account(
                LastName = realName,
                RecordTypeId = personAccountRecTypeId,
                WeChat_Username__c = socPersonaHandleNameList[0].Name,
                WeChat_User_ID__c = socPersonaHandleNameList[0].Externalid,
                sf4twitter__Twitter_Follower_Count__pc = socPersonaHandleNameList[0].Followers,
                Company__c = companyName
            );
            
            insert newPersonAccount;

            
            socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
            update socPersonaHandleNameList[0];
            
            String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
            newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
            
            socialPost[0].WhoId = newPersonAccount.Id;
            update socialPost[0];

    } else if(!socPersonaHandleNameList[0].ExternalId.equals(account.WhatsApp_User_ID__c) && 'WhatsApp'.equals(socialPost[0].Provider)) {
            newPersonAccount = new Account(
                LastName = realName,
                RecordTypeId = personAccountRecTypeId,
                WhatsApp_Username__c = socPersonaHandleNameList[0].Name,
                WhatsApp_User_ID__c = socPersonaHandleNameList[0].Externalid,
                Company__c = companyName
            );
            
            insert newPersonAccount;

            
            socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
            update socPersonaHandleNameList[0];
            
            String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
            newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
            
            socialPost[0].WhoId = newPersonAccount.Id;
            update socialPost[0];
            
    }/*else if(!socPersonaHandleNameList[0].ExternalId.equals(account.SinaWeibo_User_ID__c) && 'SinaWeibo'.equals(socialPost[0].Provider)) {
            newPersonAccount = new Account(
              LastName = realName,
              RecordTypeId = personAccountRecTypeId,
              Messenger_Username__c = socPersonaHandleNameList[0].Name,
              Messenger_User_ID__c = socPersonaHandleNameList[0].ExternalId,
              Company__c = companyName
            );
            
            insert newPersonAccount;
          
          
          socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
          update socPersonaHandleNameList[0];
          
          String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
          newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
          socialPost[0].WhoId = newPersonAccount.Id;
          update socialPost[0]; 
    }*/ else {
            if(account.Company__c == null){
              account.Company__c = companyName;
              }
              
              if('Twitter'.equals(socialPost[0].Provider) && socPersonaHandleNamelist[0].ExternalId.equals(account.sf4twitter__Twitter_User_Id__pc)){
                if(account.Company__c == null){
                  account.Company__c = companyName;
                }
                account.sf4twitter__Twitter_Username__pc = socPersonaHandleNameList[0].Name;
                account.sf4twitter__Twitter_Follower_Count__pc = socPersonaHandleNameList[0].Followers;
                
              }
            update account;
            String personAccountId = [SELECT Id FROM Contact WHERE accountId = :socPersonaHandleNameList[0].ParentId].Id;
            newCase = createNewCase(socPersonaHandleNameList[0].ParentId, personAccountId, socialPost[0]);
            socialPost[0].WhoId = socPersonaHandleNameList[0].ParentId;
            update socialPost[0];
            }
             system.debug('***Case value:newCase 2'+newCase);
            return newCase;
    }


    /**
    * Helper method for "checkCreatePersonAccount" method
    * Creates a new Case for SocialPost
    *
    * @param    personAccountId - ID of "Person Account"
    * @param    contactPersonAccountId - ID of Contact related to Account
    * @param    socialPost - to provide additional informations for new case
    *
    * @return   new case
    */
  private Case createNewCase(String personAccountId, String contactPersonAccountId, SocialPost socialPost) {
    Case cs = new Case();
    //Prasanna:Added new field to populate firstpost type
    if('AirFrance'.equals(socialPost.Company__c)) {
      //Modified by Sai.Removing the SOQL query for Record Type, JIRA ST-1868.
      //RecordType rtAF = [SELECT Id, Name FROM RecordType WHERE Name = 'AF Servicing' AND SobjectType = 'Case' LIMIT 1];
      
      cs = new Case(
              accountId = personAccountId,
              contactId = contactPersonAccountId,
              RecordTypeId = rtAF,
              subject = socialPost.Name,
              ownerId = socialPost.OwnerId,
              Entity__c = socialPost.Entity__c,
              Language__c = socialPost.Language__c,
              Origin = socialPost.Provider,

              suppliedName = userInfo.getUserName()                           
          );
        //added by sai for AF-Wechat.JIRA ST-484
        if('WeChat'.equals(socialPost.Provider)){
              cs.chatHash__c = socialPost.ExternalPostId;  
        }
    } else {
          //Modified by Sai.Removing the SOQL query for Record Type, JIRA ST-1868.
          //RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Servicing' AND SobjectType = 'Case' LIMIT 1];
          //Update the Case Owner field as who is engages the unpublished post when its Unpublished post from KLM-ST-002644 - Narmatha
          ID caseOwnerId;
          if(!('Post'.equals(SocialPost.MessageType) || 'Private'.equals(socialPost.MessageType)) && socialPost.SCS_Post_Tags__c=='KLM-Unpulished')
              caseOwnerId=userinfo.getUserID(); 
          else
              caseOwnerId=socialPost.OwnerId;
              
          cs = new Case(
              accountId = personAccountId,
              contactId = contactPersonAccountId,
              RecordTypeId = rtKLM,
              subject = socialPost.Name,
              ownerId = caseOwnerId,
              Language__c = socialPost.Language__c,
              Origin = socialPost.Provider,

              suppliedName = userInfo.getUserName()
              );
           //Added by sai. To find the existing open case from other list views and link with them. ST-1645
           //Start 
           if(socialPost.Provider != 'Facebook'){
                List<Case> existingcase=[select id,origin,subject,ownerid,language__c from case where origin=: socialPost.Provider and accountid =:personAccountId  and isclosed=FALSE order by createddate desc limit 1];  
                System.debug('existingcase:'+existingcase);
                if(existingcase.size()>0){
                    cs.origin=existingcase[0].origin;
                    cs.subject=existingcase[0].subject;
                    cs.language__c=existingcase[0].language__c;
                    cs.id=existingcase[0].id;}
                    system.debug('case info:'+cs);}
           //End
                    

          if(!('Post'.equals(SocialPost.MessageType) || 'Private'.equals(socialPost.MessageType))&& !('Direct'.equals(socialPost.MessageType) && socialPost.TopicProfileName.contains('KLM') && 'Twitter'.equals(socialPost.Provider)) && !('WeChat'.equals(socialPost.Provider))){
              cs.Status = 'In Progress';
          }
          //Added By Nagavi for ST-1358 - Development- Case Topic Filling by DG in UAT

          else if('Facebook'.equals(socialPost.Provider) && socialPost.content!=null && socialPost.IsOutbound==false && socialPost.TopicProfileName!=null && !socialPost.TopicProfileName.contains('France')){ 
              cs.Data_for_Prefill_By_DG__c=gettruncatedString(cs.Data_for_Prefill_By_DG__c,socialPost.content);
              cs.dgAI2__DG_Fill_Status__c='Pending';
              //dgAI2.DG_PredictionTriggerHandler.doPrediction(new List<Case>{cs});

          }
          

          //Modified by Sai. To populate the chathash on manual case creation of Facebook Messenger post.
          if('WeChat'.equals(socialPost.Provider) || ((cs.chatHash__c== null || cs.chatHash__c == '') && 'Facebook'.equals(socialPost.Provider) && 'Private'.equals(socialPost.MessageType))){
              cs.chatHash__c = socialPost.ExternalPostId;
              if(socialPost.Chat_Medium__c != null)
              {
                  cs.Chat_Medium__c = socialPost.Chat_Medium__c;
              }
              cs.status = 'In Progress';                           
          } else if('WhatsApp'.equals(socialPost.Provider)){
              cs.WhatsApp_Number__c = socialPost.RecipientsNumber__c;
              if(socialPost.RecipientsNumber__c != '31683808006'){
                  cs.Type = 'Question';
                  cs.Case_Phase__c = 'Day of Travel';
                  cs.Case_Topic__c = 'Flight Disruptions (DOT)';
                  cs.Case_Detail__c = 'Involuntary Rebooking';
                  cs.Sentiment_at_start_of_case__c = 'Neutral';
                  //Is it good idea to set sentiment at close of case on case creation????
                  cs.Sentiment_at_close_of_case__c = 'Neutral';
              }
          }
    }
    
    return cs;
  }
  
  //Added by Nagavi for ST-1358 - Development- Case Topic Filling by DG in UAT
  public static string gettruncatedString(string oldString,string newString){
      String temp='';
      string truncatedString='';
      if(oldString==null || oldString =='')
          temp=newString;
      else
          temp=oldString+';'+newString;

      
      if(temp.length()>=32768)
          truncatedString=temp.subString(0,32767);
      else
          truncatedString=temp; 

          
      return truncatedString;

  }
}