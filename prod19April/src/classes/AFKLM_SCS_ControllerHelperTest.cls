/********************************************************************** 
 Name:  AFKLM_SCS_ControllerHelperTest
 Task:    N/A
 Runs on: AFKLM_SCS_ControllerHelper
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          19/05/2014      Initial Development
***********************************************************************/
@isTest
private class AFKLM_SCS_ControllerHelperTest {

    private static ApexPages.StandardSetController myStandardSetController() {
            List<SocialPost> socialPostList = new List<SocialPost>();
            socialPostList = getSocialPostList(socialPostList, '');
            ApexPages.StandardSetController SocPostSetController = 
            new ApexPages.StandardSetController(socialPostList);
            return SocPostSetController;
    }
    
    private static List<SocialPost> getSocialPostList(List<SocialPost> socialPostList, String operator) {
        Case tstCase = new Case(
            Priority = 'Normal',
            Status = 'New'
        );
        
        insert tstCase; 
        
        Datetime postedDate = null;
        
        for (Integer x=0; x < 5; x++){
            if(operator == 'substract') {
                postedDate = Datetime.now()-x;
            } else {
                postedDate = Datetime.now()+x;
            }
            
            SocialPost tstSocialPost = new SocialPost(
                                        Name = 'Test '+x,
                                        Posted = postedDate,
                                        Company__c='AirFrance',
                                        TopicProfileName='SCS Air France',
                                        ParentId = tstCase.Id
                                    );
            socialPostList.add(tstSocialPost);
        }
        return socialPostList;
    }

//  TODO disabled failing test because of LE deployment of LTC 2.0 on 11-02-2015

    static testMethod void markReviewedTest() {
        //List<AFKLM_SCS_SocialPostWrapperCls> socPostList = contHelper.socPostList;
        List<SocialPost> listSocialPost = new List<SocialPost>();
        listSocialPost = getSocialPostList(listSocialPost,'');
        Account acc = new Account(FirstName='Test', LastName='Account', Company__c='AirFrance', sf4twitter__Fcbk_User_Id__pc='FB_123456789');
        insert acc;
        ApexPages.StandardSetController SocPostSetController = myStandardSetController();
        List<SocialPost> sp= new List<SocialPost>{new SocialPost(Content='Content content content', Company__c='AirFrance', TopicProfileName='SCS Air France', whoid=acc.id, Name='Post from: Test Account',Provider='Facebook', MessageType='Comment'), new SocialPost(Content='Content content content',Name='Post from: Test Account', WhoId=acc.id,Provider='Facebook', MessageType='Comment'),new SocialPost(Content='Content content content',Name='Comment from: Test Account', WhoId=acc.id, MessageType='Comment',Provider='Facebook')};
        insert sp;
        
        test.startTest(); 
        List<AFKLM_SCS_SocialPostWrapperCls> socPostList = new List<AFKLM_SCS_SocialPostWrapperCls>();
        AFKLM_SCS_SocialPostController myController = new AFKLM_SCS_SocialPostController(SocPostSetController);
        for(SocialPost c : sp){
            socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            socPostList[0].isSelected=true;
        }
        AFKLM_SCS_ControllerHelper contHelper = new AFKLM_SCS_ControllerHelper();
        contHelper.markReviewed(socPostList, listSocialPost, false);
        test.stopTest();

        listSocialPost.clear();
        contHelper.markReviewed(socPostList, listSocialPost, false);
    }
  
     static testMethod void createNewCasesFacebookTest() {
         AFKLM_SCS_ControllerHelper contHelper = new AFKLM_SCS_ControllerHelper();
        String newCaseId;
        String newCaseNumber;
        Boolean error=false;
        String extId = '123456789';
        Map<String, SocialPost> postMap = new Map<String, SocialPost>();
        Account acc = new Account(FirstName='Test', LastName='Account', Company__c='AirFrance', sf4twitter__Fcbk_User_Id__pc='FB_123456789', sf4twitter__Twitter_Username__pc='Test user');
        insert acc;
        SocialPersona persona = new SocialPersona(Name='Test Persona', ParentId=acc.id, Provider='Facebook', ExternalId=extId,RealName = 'Test User',ProfileURL='http://www.testingurlfortestuser.com/id=123456789');
        insert persona;
        Case cs = new Case(Status='New');
        insert cs;
        List<SocialPost> sp= new List<SocialPost>{new SocialPost(whoid=acc.id, PersonaId=persona.id, Company__c='AirFrance', TopicProfileName='SCS Air France', Name='Comment from: Test Account',Provider='Facebook', MessageType='Comment')};
        insert sp;
        postMap.put(sp[0].id, sp[0]);
        contHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
    }
    
    static testMethod void createNewCasesFacebookElseStatementTest() {
         AFKLM_SCS_ControllerHelper contHelper = new AFKLM_SCS_ControllerHelper();
        String newCaseId;
        String newCaseNumber;
        Boolean error=false;
        String extId = '12345678987654321';
        Map<String, SocialPost> postMap = new Map<String, SocialPost>();
        Account acc = new Account(FirstName='Test', LastName='Account', Company__c='AirFrance', sf4twitter__Fcbk_User_Id__pc='FB_123456789876542', sf4twitter__Twitter_Username__pc='Test user');
        insert acc;
        SocialPersona persona = new SocialPersona(Name='Test Persona', ParentId=acc.id, Provider='Facebook', ExternalId=extId, RealName = 'Test User',ProfileURL='http://www.testingurlfortestuser.com/id=12345678987654321');
        insert persona;
        Case cs = new Case(Status='New');
        insert cs;
        List<SocialPost> sp= new List<SocialPost>{new SocialPost(whoid=acc.id, PersonaId=persona.id, Company__c='AirFrance', TopicProfileName='SCS Air France', Name='Comment from: Test Account',Provider='Facebook', MessageType='Comment')};
        insert sp;
        postMap.put(sp[0].id, sp[0]);
        contHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
    }
    
    static testMethod void SocialPostParentIdNotNullTest(){
     AFKLM_SCS_ControllerHelper contHelper = new AFKLM_SCS_ControllerHelper();
        String newCaseId;
        String newCaseNumber;
        Boolean error=false;
        String extId = '123456789';
        Map<String, SocialPost> postMap = new Map<String, SocialPost>();
        Account badAccount = new Account(FirstName='Testing', LastName='Account',Company__c='AirFrance', sf4twitter__Fcbk_User_Id__pc='FB_12345898765432', sf4twitter__Twitter_Username__pc='Test user');
        insert badAccount;
        Account acc = new Account(FirstName='Test', LastName='Account',Company__c='AirFrance', sf4twitter__Fcbk_User_Id__pc='FB_123456789',sf4twitter__Twitter_Username__pc='Test user');
        insert acc;
        SocialPersona persona = new SocialPersona(Name='Test Persona', ParentId=badAccount.id, Provider='Facebook', ExternalId=extId,RealName = 'Test User',ProfileURL='http://www.testingurlfortestuser.com/id=123456789');
        insert persona;
        Case cs = new Case(Status='New');
        insert cs;
        List<SocialPost> sp= new List<SocialPost>{new SocialPost(whoid=acc.id, PersonaId=persona.id, Name='Comment from: Test Account',Company__c='AirFrance', TopicProfileName='SCS Air France', Provider='Facebook', MessageType='Comment', ParentId=cs.id)};
        insert sp;
        postMap.put(sp[0].id, sp[0]);
        contHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
    }
    
    static testMethod void persAccNotEmptyTest() {
         AFKLM_SCS_ControllerHelper contHelper = new AFKLM_SCS_ControllerHelper();
        String newCaseId;
        String newCaseNumber;
        Boolean error=false;
        String extId = '123456789';
        Map<String, SocialPost> postMap = new Map<String, SocialPost>();
        Account badAccount = new Account(FirstName='Testing', LastName='Account',Company__c='AirFrance',sf4twitter__Fcbk_User_Id__pc='FB_12345898765432',sf4twitter__Twitter_Username__pc='Test user');
        insert badAccount;
        Account acc = new Account(FirstName='Test', LastName='Account',Company__c='AirFrance', sf4twitter__Fcbk_User_Id__pc='FB_123456789', sf4twitter__Twitter_Username__pc='Test user');
        insert acc;
        SocialPersona persona = new SocialPersona(Name='Test Persona', ParentId=badAccount.id, Provider='Facebook', ExternalId=extId,RealName = 'Test User',ProfileURL='http://www.testingurlfortestuser.com/id=123456789');
        insert persona;
        Case cs = new Case(Status='New');
        insert cs;
        List<SocialPost> sp= new List<SocialPost>{new SocialPost(whoid=acc.id, PersonaId=persona.id,Company__c='AirFrance', TopicProfileName='SCS Air France', Name='Comment from: Test Account',Provider='Facebook', MessageType='Comment')};
        insert sp;
        postMap.put(sp[0].id, sp[0]);
        contHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
    }

    static testMethod void testAll(){
        AFKLM_SCS_ControllerHelper helper = new AFKLM_SCS_ControllerHelper();
        List<SocialPost> postList = new List<SocialPost>();
        postList = prepareData();
        Map<String, SocialPost> postMap = new Map<String, SocialPost>();

        String newCaseId;
        String newCaseNumber;
        Boolean error=false;

        for(SocialPost sp : postList){
            postMap.put(sp.Id, sp);
            //postMap.clear();
        }

        helper.createNewCases(postMap, newCaseId, newCaseNumber, error);
    }

    private static List<SocialPost> prepareData(){
        List<SocialPost> listSocialPosts = new List<SocialPost>();
        Datetime postedDate = null;
        List<SocialPersona> personas = new List<SocialPersona>();

        Account acc = new Account(FirstName='Test', LastName='Account',Company__c='AirFrance'); //sf4twitter__Fcbk_User_Id__pc='FB_123456789', sf4twitter__Twitter_Username__pc='Test user',WeChat_User_ID__c = '123456789' ,WeChat_Username__c = 'Test User' ,WhatsApp_User_Id__c = '123456789', WhatsApp_Username__c='Test User',sf4twitter__Twitter_User_Id__pc='123456789', sf4twitter__Fcbk_Username__pc='Test User'
        insert acc;
        //SocialPersona persona = new SocialPersona(Name='Test Persona', ParentId=acc.id, Provider='Facebook', ExternalId='123456789',RealName = 'Test User',ProfileURL='http://www.testingurlfortestuser.com/id=123456789');
        //insert persona;

        for(Integer x = 1;x<=3;x++){

            SocialPersona persona = new SocialPersona(Name='Test Persona', ParentId=acc.id, Provider='Facebook', ExternalId='123456789'+x,RealName = 'Test User'+x,ProfileURL='http://www.testingurlfortestuser.com/id=123456789');
            personas.add(persona);
        }
        insert personas;

        for(Integer i=0;i<=3;i++){
            postedDate = Datetime.now()+i;

            SocialPost newPost = new SocialPost(
                Name = 'Test '+i,
                Posted = postedDate,
                //WhoId = acc.id,
               
                Handle = 'Testing handle'
                );

            if(Math.mod(i,3)==0){
                newPost.Company__c='AirFrance';
                newPost.TopicProfileName='SCS Air France';
                newPost.Provider = 'Twitter';
                newPost.PersonaId = personas[0].id;
            } else if (Math.mod(i,3)==1){
                newPost.Company__c='KLM';
                newPost.TopicProfileName='SCS KLM';
                newPost.Provider = 'WeChat';
                newPost.MessageType = 'Private';
                newPost.PersonaId = personas[1].id;
            } else{
                newPost.Company__c='KLM';
                newPost.TopicProfileName='SCS KLM';
                newPost.Provider = 'WhatsApp';
                newPost.MessageType = 'Private';
                newPost.PersonaId = personas[2].id;
            }

            listSocialPosts.add(newPost);
        }
        insert listSocialPosts;

        return listSocialPosts;
    }


    //ST-002786
    static testMethod void createNewCasesWithoutAccountTest() {
        
        AFKLM_SCS_CaseRecordTypeSingleton crt = AFKLM_SCS_CaseRecordTypeSingleton.getInstance();

        AFKLM_SCS_ControllerHelper contHelper = new AFKLM_SCS_ControllerHelper();
        Map<String, SocialPost> postMap = new Map<String, SocialPost>();

        List<SocialPost> sp= new List<SocialPost>{new SocialPost(TopicProfileName='SCS Air France', Name='Post from: Test Account',Provider='Facebook', MessageType='Comment')};
        insert sp;
        postMap.put(sp[0].id, sp[0]);
        contHelper.createNewCases(postMap, '', '', true);

        List<SocialPost> aPost = [select parentId from SocialPost where id=:sp[0].id];

        List<Case> aCase = [select id,recordTypeId from Case where id=:aPost[0].parentId];
        if(!aCase.isEmpty()){

            System.debug('aCase ' + aCase[0].recordTypeId);
            System.assertEquals(crt.afRt.Id, aCase[0].recordTypeId,'Case created with wrong RecordType');

        }
    }


}