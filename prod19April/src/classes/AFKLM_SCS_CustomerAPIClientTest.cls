/**********************************************************************
 Name:  AFKLM_SCS_CustomerAPIClientTest
 Task:    N/A
 Runs on: AFKLM_SCS_CustomerAPIClient, AFKLM_SCS_RestResponse, AFKLM_SCS_JSONParserUtil, AFKLM_SCS_RestException 
====================================================== 
Purpose: 
    This class contains unit tests for: 
    - Customer API Client 
    - Response
    - JSON Parser 
    - Exception
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung     	03/06/2014      Initial Development
***********************************************************************/
@isTest
global class AFKLM_SCS_CustomerAPIClientTest {

	@IsTest
	public static void setupTest() {
		AFKLM_SCS_CustomerAPIClient cac = new AFKLM_SCS_CustomerAPIClient();
        cac.setNumRetries(3);
		system.assert(cac.getNumRetries() == 3, '\n\nWARNING: Number of retries is not 3.');
	}
	
	@IsTest
	public static void mockCalloutTest() {
        Test.setMock(HttpCalloutMock.class, new AFKLM_SCS_HttpCalloutMock());
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.klm.com/customerapi/customer');
        req.setMethod('GET');
        Http h = new Http();
        HttpResponse res = h.send(req);
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        String expectedValue = '{"foo":"bar"}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200,res.getStatusCode());
    }
    
    @isTest(SeeAllData=true)
    public static void customerAPIClientTest() {
        AFKLM_SCS_CustomerAPIClient client = new AFKLM_SCS_CustomerAPIClient();
        
        client.setNumRetries(3);
        System.assertEquals(3, client.getNumRetries());
        
        Exception e = null;
        try {
            client.safeRequest('https://api.klm.com/customerapi/customer', 'POST', new Map<String,String>{'key'=>'val'});
        } catch (AFKLM_SCS_RestException tre) {
            e = tre;
        }
        System.assert(e instanceof AFKLM_SCS_RestException);
        
        e = null;
        try {
            client.safeRequest('https://api.klm.com/customerapi/customer', 'GET', new Map<String,String>{'key'=>'val'});
        } catch (AFKLM_SCS_RestException tre) {
            e = tre;
        }
        System.assert(e instanceof AFKLM_SCS_RestException);        
        
		e = null;
		try {
			client.safeRequest('api.klm.com/customerapi/customer','GET', new Map<String, String>{'key'=>'val'});
		} catch(AFKLM_SCS_RestException tre) {
            e = tre;
		}
		System.assert(e instanceof AFKLM_SCS_RestException);  
    }
    
	@IsTest
    global static void restResponseTest() {
        String url = 'https://api.klm.com/customerapi/customer/';
        String queryString = '12341244';
        String responseText = 'This is a test';
        AFKLM_SCS_RestResponse response = new AFKLM_SCS_RestResponse(url+'?'+queryString,responseText,200);
        
        System.assertEquals(url, response.getUrl());
        System.assertEquals(responseText, response.getResponseText());
        System.assertEquals(200, response.getHttpStatus());
        System.assertEquals(queryString, response.getQueryString());
        System.assertEquals(false, response.isError());
        System.assertEquals(false, response.isClientError());
        System.assertEquals(false, response.isServerError());
        System.assertEquals(false, response.isJson());
        System.assertEquals(false, response.isXml());
        
        response.setError(true);
        System.assert(response.isError());
        
        response.setHttpStatus(400);
        System.assertEquals(400, response.getHttpStatus());
        System.assertEquals(true, response.isClientError());
        System.assertEquals(false, response.isServerError());
        
        response.setHttpStatus(503);
        System.assertEquals(503, response.getHttpStatus());
        System.assertEquals(false, response.isClientError());
        System.assertEquals(true, response.isServerError());
        
        String json = '{"postalAddresses":[{"postalAddress":{"country":"NL","city":"AERDENHOUT","streetHousenumber":"TEDING VAN BERKHOUTLAAN 17","usageType":"Business","company":{"name":"KLM"},"postalCode":"2111 ZA"}},{"postalAddress":{"country":"NL","city":"AERDENHOUT","streetHousenumber":"TEDING VAN BERKHOUTLAAN 17","usageType":"Private","postalCode":"2111 ZA"}}],"firstName":"MAARTEN","phoneNumbers":[{"usageType":"Private","phoneType":"Mobile","fullNumber":"+31622458182"}],"emailAccount":{"email":"maarten.deruyter@gmail.com"},"familyName":"DE RUYTER","travelDocuments":[{"travelDocument":{"identifier":"NY1234712","type":"Passport","issueDate":"2010-03-04","nationality":"NL","expirationDate":"2015-03-04"}}],"title":"MR","dateOfBirth":"1969-03-24"}';
        
        response.setResponseText(json);
        System.assertEquals(json, response.getResponseText());
        
        response.setContentType('application/json');
        System.assertEquals(true, response.isJson());
        System.assertEquals(false, response.isXml());
        Map<String,Object> jsonMap = response.toMap();
        System.assertEquals(8, jsonMap.size());
        System.assert(jsonMap.containsKey('title'));
        System.assert(jsonMap.containsKey('dateOfBirth'));
        System.assertEquals('MR', jsonMap.get('title'));
        System.assertEquals('1969-03-24', jsonMap.get('dateOfBirth'));
        
        response.setContentType('application/xml');
        System.assertEquals(false, response.isJson());
        System.assertEquals(true, response.isXml());
        
        Exception restEx = null;
        
        try {
            Map<String,Object> xmlMap = response.toMap();
        } catch (Exception e) {
            restEx = e;
        }
        System.assert(restEx instanceof AFKLM_SCS_RestException);
        
        response.setUrl('test');
        System.assertEquals('test', response.getUrl());
        
        response.setQueryString('test');
        System.assertEquals('test', response.getQueryString());
        
        // Overload constructor
        AFKLM_SCS_RestResponse response2 = new AFKLM_SCS_RestResponse(responseText,200);
        System.assertEquals(200, response2.getHttpStatus());
    }
    
    static testMethod void restExceptionTest() {
    	AFKLM_SCS_RestException re = new AFKLM_SCS_RestException();
    	
        try {
            re = new AFKLM_SCS_RestException('HTTP401',1);
        } catch (AFKLM_SCS_RestException tre) {
            re = tre;
        }
        
        try {
            re = new AFKLM_SCS_RestException('HTTP401',1,'Additional HTTP401 Errormessage');
        } catch (AFKLM_SCS_RestException tre) {
            re = tre;
        }
        
        String url = 'https://api.klm.com/customerapi/customer/';
        String queryString = '12341244';
        String responseText = 'This is a test';
        AFKLM_SCS_RestResponse response = new AFKLM_SCS_RestResponse(url+'?'+queryString,responseText,200);
        String json = '{"postalAddresses":[{"postalAddress":{"country":"NL","city":"AERDENHOUT","streetHousenumber":"TEDING VAN BERKHOUTLAAN 17","usageType":"Business","company":{"name":"KLM"},"postalCode":"2111 ZA"}},{"postalAddress":{"country":"NL","city":"AERDENHOUT","streetHousenumber":"TEDING VAN BERKHOUTLAAN 17","usageType":"Private","postalCode":"2111 ZA"}}],"firstName":"MAARTEN","phoneNumbers":[{"usageType":"Private","phoneType":"Mobile","fullNumber":"+31622458182"}],"emailAccount":{"email":"maarten.deruyter@gmail.com"},"familyName":"DE RUYTER","travelDocuments":[{"travelDocument":{"identifier":"NY1234712","type":"Passport","issueDate":"2010-03-04","nationality":"NL","expirationDate":"2015-03-04"}}],"title":"MR","dateOfBirth":"1969-03-24"}';
        response.setResponseText(json);
        response.setContentType('application/json');
        
        try {
            re = new AFKLM_SCS_RestException(response);
        } catch (AFKLM_SCS_RestException tre) {
            re = tre;
        }
    }
}