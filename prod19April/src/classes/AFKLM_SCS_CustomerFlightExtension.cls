/********************************************************************** 
 Name:  AFKLM_SCS_CustomerFlightExtension
 Task:    N/A
 Runs on: AFKLM_SCS_CustomerFlightExtension
======================================================  
Purpose: 
    Getting the Customer Flights information from the 'Customer API' based on FB#, E-mail, Social Handle/Social Network. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      13/05/2014      Initial Development
    1.1     Patrick Brinksma    16 Jul 2014     Added handling Account standard controller
    1.2		Stevano Cheung		9 Dec 2014		Customer API 2.0 modification 'CustomerId' is 'id'
***********************************************************************/
public with sharing class AFKLM_SCS_CustomerFlightExtension {
    private static final String SEARCH_CUSTOMER_URL = 'https://api.klm.com/customerapi/customer';
    private static final String SEARCH_CUSTOMER_URL_BY_ID = 'https://api.klm.com/customerapi/customers/';

    private Case cs;
    private Account acc;
    private String customerId; 
    private String socialId = '';
    private String socialNetwork = '';
    private String eMail = ''; 
    private String flyingBlueNumber = ''; 

    private Map<String, String> departureDateList;
    private Map<String, String> arrivalDateList;
    private Map<String, String> flightNumberList;
    private Map<String, String> pnrList;

    private Map<String, String> pastDestinationRouteList;    
    private Map<String, String> todayDestinationRouteList;
    private Map<String, String> futureDestinationRouteList;

    private List<DateTime> departureDatesToSort;
    private List<DateTime> futureDatesToSort;
    private List<DateTime> todayDatesToSort;
    private List<DateTime> pastDatesToSort;

    private List<String> PastListOrdered;
    private List<String> TodayListOrdered;
    private List<String> UpcomingListOrdered;
    
    private Map<String, String> cabinClass = new Map<String, String>{'1'=>'First class','2'=>'Business class','3'=>'Economy and premium economy class','4'=>'Economy class','5'=>'Premium economy class','6'=>'Specific class'};
    private Map<String, String> cabinClassList;
    
    /** 
    * Constructor to get the fields from Case and set some parameters FB#, e-mail
    */
    public AFKLM_SCS_CustomerFlightExtension(ApexPages.StandardController controller) {
        Id thisId = (Id) controller.getId();
        if (thisId.getSObjectType() == Case.SObjectType){
            if(!Test.isRunningTest()){
                controller.addFields(new List<String>{'AccountId', 'Origin'});
            }

            this.cs = (Case) controller.getRecord();
            thisId = this.cs.AccountId;
            //this.socialNetwork = cs.Origin;          
          } else if (thisId.getSobjectType() == Account.SObjectType) {
                this.acc = (Account) controller.getRecord();
                thisId = this.acc.Id;          
          }
      
        try {
            setAccountParameters(thisId);
            getIdCustomerAPI();
            getCustomerAPIAccountDetails(customerId);
        } catch (DmlException e) {
            // Try to reconnect instead of the Websrv callout, it's not possible to do a DML during Websrv callout
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Connection refused');
            ApexPages.addMessage(myMsg);
        }
    }

    /** 
    * Set the Flying Blue number, E-mail and SocialHandle, SocialNetwork parameters
    */
    private void setAccountParameters(String accountId) {
        List<Account> accnt = [SELECT Flying_Blue_Number__c, PersonEmail, sf4twitter__Fcbk_Username__pc, sf4twitter__Fcbk_User_Id__pc, sf4twitter__Twitter_Username__pc, sf4twitter__Twitter_User_Id__pc FROM Account WHERE Id =: accountId LIMIT 1];
        if(accnt.size() > 0 ) {
            if(accnt[0].Flying_Blue_Number__c != null) {
                this.flyingBlueNumber = accnt[0].Flying_Blue_Number__c;
            }

            if(accnt[0].PersonEmail != null) {
                this.eMail = accnt[0].PersonEmail;
            }

            // Not needed now but can be used in the future
            if(accnt[0].sf4twitter__Fcbk_User_Id__pc != null) {
                this.socialId = accnt[0].sf4twitter__Fcbk_User_Id__pc.replace('FB_','');
                this.socialNetwork = 'Facebook';
            } else {
                this.socialId = accnt[0].sf4twitter__Twitter_User_Id__pc;
                this.socialNetwork = 'Twitter';
            }
        }
    }

    /** 
    * Search the Customer API Id based on Flying Blue number, E-mail and SocialHandle, SocialNetwork parameters
    */
    private void getIdCustomerAPI() {
    
        try {
            String sb = '';
      
            if( !''.equals(flyingBlueNumber) ) {
                sb = '/?flying-blue-number=' + flyingBlueNumber;
            } else if(!''.equals(eMail)) {
                sb = '/?email-address=' + eMail;
            } 
            else if(!''.equals(socialId) && !''.equals(socialNetwork)) {
                sb = '/?social-network=' +socialNetwork+'&social-identity-identifier=' +socialId;
            }
      
            AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
            AFKLM_SCS_RestResponse response = customerAPIClient.safeRequest(SEARCH_CUSTOMER_URL + sb, 'GET', new Map<String,String>());
            setFlyingBlueFieldValues(response.toMap());
        } catch(Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display information <br> - Please check Flying blue number, e-mail');
            ApexPages.addMessage(myMsg);
        }
    }

    /** 
    * Get the Customer API account details.
    */
    private void getCustomerAPIAccountDetails(String customerId) {
        try {
            if(customerId != null) {
                AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
                AFKLM_SCS_RestResponse response = customerAPIClient.safeRequest(SEARCH_CUSTOMER_URL_BY_ID + customerId + '/flight-reservations', 'GET', new Map<String,String>());
                setFlyingBlueFieldValues(response.toMap());
            }
        } catch(Exception e) {
            /* ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display Customer Id: ' + customerId);
            ApexPages.addMessage(myMsg);*/
        }
    }
        
    /** 
     * Find the values from the Customer API and set the values
     */
    private void setFlyingBlueFieldValues(Map<String, Object> response) {
        cabinClassList = new Map<String, String>();
        departureDateList = new Map<String, String>();
        arrivalDateList = new Map<String, String>();
        pnrList = new Map<String, String>();
        flightNumberList = new Map<String, String>();
                
        pastDestinationRouteList = new Map<String, String>();
        futureDestinationRouteList = new Map<String, String>();
        todayDestinationRouteList = new Map<String, String>();
        
        departureDatesToSort = new List<DateTime>();
        futureDatesToSort = new List<DateTime>();
        todayDatesToSort = new List<DateTime>();
        pastDatesToSort = new List<DateTime>();

        //DvtH22JUL2014 moved out of the loops
        //for
        String serializedFlightReservations;
        JSONParser parserFlightReservations;
        //while
        //while
        Integer i;
        Integer ticketNr;
        Reservations res;                       
        //for
        //for
        // GMT(UTC) timezone cast to user profile -> PB: Check with class AFKLM_WS_Manager, and method formatDateTimeToString
        TimeZone tz = UserInfo.getTimeZone();
        Integer timeZoneHours = tz.getOffset(DateTime.newInstance(2012,10,23,12,0,0))/3600000;  
        Date dToday = Date.today();     
        Date dTomorow = Date.today().addDays(1);        

        //
        for (String customerApiFields : response.keySet()) {
            // system.debug('*******'+ customerApiFields + '*** values: ' +response.get(customerApiFields));
                        
            if('id'.equals(customerApiFields)) {
                this.customerId = (String) response.get(customerApiFields);
            }
                        
            if('reservations'.equals(customerApiFields)) {
                serializedFlightReservations = JSON.serialize(response);
                // Replace reserved word in JSON number to number_x
                serializedFlightReservations = serializedFlightReservations.replace('"number":', '"number_x":');
                parserFlightReservations = JSON.createParser(serializedFlightReservations);
                                
                while (parserFlightReservations.nextToken() != null) {
                    if (parserFlightReservations.getCurrentToken() == JSONToken.START_ARRAY) {
                        while (parserFlightReservations.nextToken() != null) {
                            i = 0;
                            ticketNr = 0;
                            res = (Reservations) parserFlightReservations.readValueAs(Reservations.class);
                                    
                            for(Connections connections: res.flightReservation.itinerary.connections) {
                                for(Segments segments: connections.segments) {
                                                                                  
                                    if(segments.departureAirport.code != null || !''.equals(segments.departureAirport.code)) {
                                        // Check flight date for future, past and today
                                        if(DateTime.valueOf(segments.localDepartureTime.replace('T', ' ')) > dTomorow) {
                                            futureDestinationRouteList.put(res.identifier+' ('+i + ')', segments.departureAirport.code + ' -> '+ segments.arrivalAirport.code );
                                            DateTime dt = DateTime.valueOf(segments.localDepartureTime.replace('T',' ')).addHours(timeZoneHours);
                                            futureDatesToSort.add( dt );
                                            departureDateList.put(formatDateTimeToString( dt ), res.identifier+' ('+i + ')');
                                            arrivalDateList.put(res.identifier+' ('+i + ')', formatDateTimeToString(DateTime.valueOf(segments.localArrivalTime.replace('T',' ')).addHours(timeZoneHours)));
                                        } else if (DateTime.valueOf(segments.localDepartureTime.replace('T',' ')) < dToday) {
                                            pastDestinationRouteList.put(res.identifier+' ('+i + ')', segments.departureAirport.code + ' -> '+ segments.arrivalAirport.code );
                                            DateTime dt = DateTime.valueOf(segments.localDepartureTime.replace('T',' ')).addHours(timeZoneHours);
                                            pastDatesToSort.add( dt );
                                            departureDateList.put(formatDateTimeToString( dt ), res.identifier+' ('+i + ')');
                                            arrivalDateList.put(res.identifier+' ('+i + ')', formatDateTimeToString(DateTime.valueOf(segments.localArrivalTime.replace('T',' ')).addHours(timeZoneHours)));
                                        } else if(DateTime.valueOf(segments.localDepartureTime.replace('T',' ')) > dToday && 
                                            DateTime.valueOf(segments.localDepartureTime.replace('T',' ')) < dTomorow) {
                                            todayDestinationRouteList.put(res.identifier+' ('+i + ')', segments.departureAirport.code + ' -> '+ segments.arrivalAirport.code );
                                            DateTime dt = DateTime.valueOf(segments.localDepartureTime.replace('T',' ')).addHours(timeZoneHours);
                                            todayDatesToSort.add( dt );
                                            departureDateList.put(formatDateTimeToString( dt ), res.identifier+' ('+i + ')');
                                            arrivalDateList.put(res.identifier+' ('+i + ')', formatDateTimeToString(DateTime.valueOf(segments.localArrivalTime.replace('T',' ')).addHours(timeZoneHours)));
                                        }

                                        if(cabinClass.containsKey(segments.cabinClass)) {
                                            cabinClassList.put(res.identifier+' ('+i + ')', cabinClass.get(segments.cabinClass));
                                        } else {
                                            cabinClassList.put(res.identifier+' ('+i + ')', 'Not available');
                                        }

                                        flightNumberList.put(res.identifier+' ('+i + ')', segments.marketingFlight.marketingCarrier.code + segments.marketingFlight.flightNumber);                                                      
                                        
                                        // SORT BY DATE
                                        //DateTime dt = DateTime.valueOf(segments.localDepartureTime.replace('T',' ')).addHours(timeZoneHours);
                                        //departureDatesToSort.add( dt );
                                        //departureDateList.put(formatDateTimeToString( dt ), res.identifier+' ('+i + ')');
                                        
                                        //arrivalDateList.put(res.identifier+' ('+i + ')', formatDateTimeToString(DateTime.valueOf(segments.localArrivalTime.replace('T',' ')).addHours(timeZoneHours)));
                                                        
                                        // For adding E-Ticket numbers
                                        for(Passengers passengers: res.flightReservation.passengers) {
                                            if(passengers.tickets != null) {
                                                for(Tickets tickets: passengers.tickets) {
                                                    if(tickets.number_x != null || !''.equals(tickets.number_x)) {
                                                        pnrList.put(res.identifier+' ('+i + ')', tickets.number_x);
                                                    } else {
                                                        pnrList.put(res.identifier+' ('+i + ')', '');
                                                    }
                                                }
                                            } else {
                                                pnrList.put(res.identifier+' ('+i + ')', '');
                                            }
                                        }
                                                        
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * getSortedKeyset - only support sort by date
     * @param   toSort  Map to sort
     * @param   order   Possible values asc, desc - sort order
     * @return          Sorted list of datetime values      
     */
    private List<DateTime> getSortedKeyset(List<DateTime> toSort, String order) {

        if(toSort != NULL) {

            // sort list of dates
            toSort.sort();

            if(order == 'desc') {

                List<DateTime> descOrdList = new List<DateTime>();
                for(Integer i=toSort.size()-1; i>=0; i--) {
                    descOrdList.add( toSort.get(i) );
                }

                toSort.clear();
                toSort = descOrdList.clone();
            }        
        }

        return toSort;
    }

    /**
     * sortByDate - create list of PNR's sorted by date
     */
    private void sortByDate() {
        //this.departureDatesToSort != NULL
        if((this.futureDatesToSort != NULL || this.todayDatesToSort != NULL || this.pastDatesToSort != NULL) &&
            this.PastListOrdered == NULL && this.TodayListOrdered == NULL && this.UpcomingListOrdered == NULL) {

            this.PastListOrdered = new List<String>();
            this.TodayListOrdered = new List<String>();
            this.UpcomingListOrdered = new List<String>();
            
            Map<String, String> tmpDepartureDateList = new Map<String, String>();
            /*for(DateTime key: getSortedKeyset(this.departureDatesToSort, 'desc')){

                String keyToCheck = formatDateTimeToString( key );

                if( pastDestinationRouteList.containsKey( this.departureDateList.get( keyToCheck ) ) == true ) {

                    this.PastListOrdered.add( departureDateList.get( keyToCheck ) );
                    tmpDepartureDateList.put( this.departureDateList.get( keyToCheck ), keyToCheck );
                } else if ( todayDestinationRouteList.containsKey( this.departureDateList.get( keyToCheck ) ) == true ) {

                    this.TodayListOrdered.add( departureDateList.get( keyToCheck ) );
                    tmpDepartureDateList.put( this.departureDateList.get( keyToCheck ), keyToCheck );
                } else if ( futureDestinationRouteList.containsKey( this.departureDateList.get( keyToCheck ) ) == true ) {

                    this.UpcomingListOrdered.add( departureDateList.get( keyToCheck ) );
                    tmpDepartureDateList.put( this.departureDateList.get( keyToCheck ), keyToCheck );
                }
            }*/
            for(DateTime key: getSortedKeyset(this.pastDatesToSort, 'desc')){
                String keyToCheck = formatDateTimeToString( key );

                if( pastDestinationRouteList.containsKey( this.departureDateList.get( keyToCheck ) ) == true ) {

                    this.PastListOrdered.add( departureDateList.get( keyToCheck ) );
                    tmpDepartureDateList.put( this.departureDateList.get( keyToCheck ), keyToCheck );
                }
            }

            for(DateTime key: getSortedKeyset(this.todayDatesToSort, 'asc')){
                String keyToCheck = formatDateTimeToString( key );

                if ( todayDestinationRouteList.containsKey( this.departureDateList.get( keyToCheck ) ) == true ) {

                    this.TodayListOrdered.add( departureDateList.get( keyToCheck ) );
                    tmpDepartureDateList.put( this.departureDateList.get( keyToCheck ), keyToCheck );
                }
            }

             for(DateTime key: getSortedKeyset(this.futureDatesToSort, 'asc')){
                String keyToCheck = formatDateTimeToString( key );

                if ( futureDestinationRouteList.containsKey( this.departureDateList.get( keyToCheck ) ) == true ) {

                    this.UpcomingListOrdered.add( departureDateList.get( keyToCheck ) );
                    tmpDepartureDateList.put( this.departureDateList.get( keyToCheck ), keyToCheck );
                }
            }

            this.departureDateList.clear();
            this.departureDateList = tmpDepartureDateList.clone();
        }
    }

    /** 
    * Return datetime in String of Date + Time (GMT)
    * Need to look at Date (local time etc)
    */
    private static String formatDateTimeToString(Datetime thisDT) {
        Date thisD = thisDT.date();
        String sD = thisD.format();
        return thisDT.format('dd-MM-YYYY') + ' ' + String.valueof(thisDT.time()).left(5);
    }
        
    // Get the Customer API Id
    public String getCustomerId() {
        return customerId;
    }
  
    public Map<String, String> getPastDestinationRouteList() {
        return this.pastDestinationRouteList;
    }

    public Integer getTotalPastDestination() {
        if(this.pastDestinationRouteList == null) {
            return 0;
        } else
            return this.pastDestinationRouteList.size();
    }

    public Map<String, String> getFutureDestinationRouteList() {
        return this.futureDestinationRouteList;
    }
        
    public Integer getTotalFutureDestination() {
        if(this.futureDestinationRouteList == null) {
            return 0;
        } else
            return this.futureDestinationRouteList.size();
    }
        
    public Map<String, String> getTodayDestinationRouteList() {
        return this.todayDestinationRouteList;
    }
        
    public Integer getTotalTodayDestination() {
        if(this.todayDestinationRouteList == null) {
            return 0;
        } else
            return this.todayDestinationRouteList.size();
    }
        
    public Map<String, String> getDepartureDate() {
        return this.departureDateList;
    }

    /**
     * Main iterators
     */
    public List<String> getPastListOrdered() {

        // sort flights by date
        this.sortByDate();
        return this.PastListOrdered;
    }

    public List<String> getTodayListOrdered() {

        // sort flights by date
        this.sortByDate();
        return this.TodayListOrdered;
    }

    public List<String> getUpcomingListOrdered() {

        // sort flights by date
        this.sortByDate();
        return this.UpcomingListOrdered;
    }

    public Map<String, String> getArrivalDateList() {
        return this.arrivalDateList;
    }

    public Map<String, String> getPnrList() {
        return this.pnrList;
    }
        
    public Map<String, String> getFlightNumberList() {
        return this.flightNumberList;
    }
        
    public Map<String, String> getCabinClassList() {
        return this.cabinClassList;
    }
    
    /** 
     * Inner classes used for serialization by readValuesAs() for previousFlight, currentFlight, futureFlights. 
     */
    public class Reservations {
        public String identifier;
        public FlightReservation flightReservation;
                
        public Reservations(String identifier) {
            this.identifier = identifier;
        }
    }
        
    // FlightReservation
    public class FlightReservation {
        public Itinerary itinerary;
        public Passengers[] passengers;
        
        public FlightReservation() {
            this.passengers = new List<Passengers>();
        }
    }
        
    // Itinerary
    public class Itinerary {
        public DestinationsCities[] destinationsCities;
        public Connections[] connections;
        
        public Itinerary() {
            this.destinationsCities = new List<DestinationsCities>();
            this.connections = new List<Connections>();
        }
    }
        
    public class DestinationsCities {
        public String code;
            
        public DestinationsCities(String code) {
            this.code = code;
        }
    }
        
    // Connections
    public class Connections {
        public OriginAirport originAirport;
        public DestinationAirport destinationAirport;
        public Segments[] segments;
        
        public Connections(){
            this.segments = new List<Segments>();
        }
    }
        
    public class OriginAirport {
        public String code;
        
        public OriginAirport(String code) {
            this.code = code;
        }
    }
        
    public class DestinationAirport {
        public String code;
        
        public DestinationAirport(String code) {
            this.code = code;
        }
    }

    // Segments
    public class Segments {
        public String localDepartureTime;
        public String localArrivalTime;
        public String cabinClass;
        public ArrivalAirport arrivalAirport;
        public DepartureAirport departureAirport;
        public MarketingFlight marketingFlight;
        
        public Segments(String localDepartureTime, String localArrivalTime, String cabinClass) {
            this.localDepartureTime = localDepartureTime;
            this.localArrivalTime = localArrivalTime;
            this.cabinClass = cabinClass;
        }
    }
        
    public class ArrivalAirport {
        public String code;
        
        public ArrivalAirport(String code) {
            this.code = code;
        }
    }
        
    public class DepartureAirport {
        public String code;
        
        public DepartureAirport() {
            this.code = code;
        }
    }
        
    public class MarketingFlight {
        public String flightNumber;
        public MarketingCarrier marketingCarrier;
        
        public MarketingFlight(String flightNumber) {
            this.flightNumber = flightNumber;
        }
    }
        
    public class MarketingCarrier {
        public String code;
        
        public MarketingCarrier(String code) {
            this.code = code;
        }
    }
        
    // Passengers
    public class Passengers {
        public Tickets[] tickets;
        
        public Passengers() {
            this.tickets = new List<Tickets>();
        }
    }
        
    public class Tickets {
        public String number_x;
        
        public Tickets(String number_x) {
            this.number_x = number_x;
        }
    }
}