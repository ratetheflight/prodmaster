/*************************************************************************************************
* File Name     :   AFKLM_SCS_DeleteOldStatusTrackersBatch
* Description   :   
* @author       :   
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     31/05/2016      Ata         Added Agent Employee Number field
****************************************************************************************************/
global class AFKLM_SCS_DeleteOldStatusTrackersBatch implements Database.Batchable<Status_tracker__c> {
        
    global AFKLM_SCS_DeleteOldStatusTrackersBatch() {
        
    }
    
    global Iterable<Status_Tracker__c> start(Database.BatchableContext bc){
        return getDataSet();
    }

    global List<Status_Tracker__c> getDataSet() {
        AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('Settings');

        Integer days;

        if(Test.isRunningTest()){
            days = 30;
        } else {
            days = (Integer) custSettings.SCS_DeleteStatusTrackersAfterDays__c;
        }

        DateTime dt = Datetime.now().addDays(-days);
        //Added fields in the query-TCS-22nd Feb
        List<Status_Tracker__c> queriedTrackersToDelete = [SELECT id,
                                                                  Case__c,
                                                                  Changed_by__c,
                                                                  Agent_Employee_Nr__c,
                                                                  End_Time__c,
                                                                  New_Status__c,
                                                                  Old_Status__c,
                                                                  Start_Time__c,
                                                                  Type__c,
                                                                  CreatedDate 
                                                                  FROM Status_Tracker__c 
                                                                  WHERE CreatedDate <=: dt LIMIT 50000];

        if(!queriedTrackersToDelete.isEmpty()){
            return queriedTrackersToDelete;
        }

        return null;         
    }
    
    //Method to create shadow tracker records for reporting purpose.Added by TCS-22nd Feb
    global Old_status_Tracker__c createShadowTracker(Status_Tracker__c tracker){
        
        
            Old_status_Tracker__c shadowTracker = new Old_status_Tracker__c();
       
            shadowTracker.Case__c = tracker.Case__c;
            shadowTracker.Changed_by__c = tracker.Changed_by__c;
            shadowTracker.CreatedDate__c = tracker.CreatedDate;
            shadowTracker.End_Time__c = tracker.End_Time__c;
            shadowTracker.New_Status__c = tracker.New_Status__c;
            shadowTracker.Old_Status__c = tracker.Old_Status__c;
            shadowTracker.Start_Time__c = tracker.Start_Time__c;
            shadowTracker.Type__c = tracker.Type__c;
            shadowTracker.Status_Tracker_ID__c = tracker.Id;
            //Added by Ata
            shadowTracker.Agent_Employee_Nr__c= tracker.Agent_Employee_Nr__c;
            return shadowTracker;
        
         
            
    }
    
    global void execute(Database.BatchableContext BC, List<Status_Tracker__c> queriedTrackersToDelete) {
        Map<String,Status_Tracker__c> trackersToDelete = new Map<String,Status_Tracker__c>();
        List<Old_status_Tracker__c> shadowTrackerList = new List<Old_status_Tracker__c>();
        
        if(queriedTrackersToDelete != Null){
            for(Status_Tracker__c st : queriedTrackersToDelete){
                if(!trackersToDelete.containsKey(st.Id)){
                    
                    //Creating an instance of old tracker from existing tracker record-TCS-22nd Feb
                    Old_status_Tracker__c oSTracker = createShadowTracker(st);
                    trackersToDelete.put(st.Id, st);
                    //populating the shadowtrackerlist with tracker records-TCS-22nd Feb
                    shadowTrackerList.add(oSTracker);
                }
            }
        }    
        
        Savepoint sPoint = Database.setSavepoint();

        try{
            //Added the If block to archieve tracker records in old tracker object-by TCS 22nd Feb
            if(shadowTrackerList.size() > 0)
               Insert shadowTrackerList;
               
            delete trackersToDelete.values();
        }
        catch(Exception e){
            System.debug('Exception Occurred!!');
            Database.rollback(sPoint);
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('It\'s deleted');
    }
        
}