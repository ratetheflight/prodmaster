global class AFKLM_SCS_DeleteStatTrackersSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		AFKLM_SCS_DeleteOldStatusTrackersBatch deleteTrackers = new AFKLM_SCS_DeleteOldStatusTrackersBatch();
		database.executebatch(deleteTrackers,200);
	}
}