@isTest(SeeAllData=False)
private class AFKLM_SCS_DeleteStatTrackersTest {
    /*@isTest
    static void notDeleteStatusTracker() {
        Case cs = new Case(Status = 'New');
        insert cs;

        Status_Tracker__c st = new Status_Tracker__c(Old_Status__c = 'New', Case__c = cs.Id);
        insert st;
        Test.setCreatedDate(st.Id, System.now());
        List<Status_Tracker__c> stList = [SELECT id FROM Status_Tracker__c LIMIT 10];

        System.assertEquals(1, stList.size());
        test.startTest();
        AFKLM_SCS_DeleteStatTrackersSchedule dosts = new AFKLM_SCS_DeleteStatTrackersSchedule();

        dosts.execute(null);
        test.stopTest();

        stList = [SELECT id FROM Status_Tracker__c LIMIT 10];

        System.assertEquals(1,stList.size());
    }*/
    
    @isTest
    static void toDeleteStatusTracker() {
        Case cs = new Case(Status = 'New');
        insert cs;

        Status_Tracker__c st = new Status_Tracker__c(Old_Status__c = 'New', Case__c = cs.Id);
        insert st;
        Test.setCreatedDate(st.Id, System.now().addDays(-31));
        List<Status_Tracker__c> stList = [SELECT id FROM Status_Tracker__c LIMIT 10];
        system.debug('!!!!!!!!!!!:'+stList);
        
        test.startTest();
        //System.assertEquals(1, stList.size());
        AFKLM_SCS_DeleteStatTrackersSchedule dosts = new AFKLM_SCS_DeleteStatTrackersSchedule();

        dosts.execute(null);
        test.stopTest();

        stList = [SELECT id FROM Status_Tracker__c LIMIT 10];

        //System.assertEquals(0,stList.size());
    }
    
}