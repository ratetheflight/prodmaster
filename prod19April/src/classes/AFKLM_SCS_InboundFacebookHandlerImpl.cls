/**********************************************************************
 Name:  AFKLM_SCS_InboundFacebookHandlerImpl 
 Task:    N/A
 Runs on: AFKLM_SCS_InboundSocialPostHandlerImpl
======================================================
Purpose: 
   Inbound Social Handler for facebook based on Social Hub rules and data sources. 
   Creates Social Posts and handles business logic with Social Customer Service enablement.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      19/03/2014      INITIAL DEVELOPMENT
    1.1     Stevano Cheung      19/03/2014      FindParentCase() method has multiple similar if. Is refactored for seperate method.
    1.2     Ata                 18/05/2016      Copy Language info from latest actioned Social post to the new social post.
    1.3     Nagavi              17/06/2016      Added logic to check the campaign field of case if the related post belongs to campaign posts - Jira:143
    1.4     Sai Choudhry        06-01-2017      Modified the language update logic on unsupported posts. JIRA ST-1498
    1.6     Sai choudhry        10/02/2017      Campagin case attaching and post parenting logic. JIRA ST-1415
    1.7     Nagavi              17/02/2017      ST-1358 - Development- Case Topic Filling by DG in UAT 
    1.8     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868.
    1.9     Sai Choudhry        10/03/2017      Adding a Record type check on Finding Parent. JIRA ST-1936.
***********************************************************************/
global virtual class AFKLM_SCS_InboundFacebookHandlerImpl {


    private Map<String, Id> recordTypeIds {
        get {

            if(recordTypeIds == NULL) {
                recordTypeIds = new Map<String, Id>();
                ID klmRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
                recordTypeIds.put('Servicing',klmRtId);
                ID afRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('AF Servicing').getRecordTypeId();
                recordTypeIds.put('AF Servicing',afRtId);
                //Removing the SOQL query for Record Type, JIRA ST-1868.
                /*for(RecordType rc: [SELECT Id, Name FROM RecordType WHERE (Name = '' OR Name = '') AND SobjectType = 'Case']){
                    recordTypeIds.put(rc.Name, rc.Id);
                }*/
            }
            return recordTypeIds;
        }
        set;
    }



    // Reopen case if it has not been closed for more than this number
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        // SCH: SLA reopen the case and attach the reply to a case after 24 hours = 1 day
        return 1;
    }
    
    /**
    * Method to check if wall post is from Global page, and set Is campaign to true on SocialPost object.
    */
    //Modified the returntype of the method from void to Social Post - Nagavi:Jira:143
    public SocialPost checkCampaign(SocialPost post) {
        
        AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('SCS_Campaing_Social_Handles');
        //takes social handles from custom setting SocialCustomerService -> SCS_CampaignSocialHandles
        Set<String> handles = new Set<String>(custSettings.SCS_CampaignSocialHandles__c.split(';',custSettings.SCS_CampaignSocialHandles__c.length()));
        if(custSettings.SCS_CampaignSocialHandles2__c != null){
            handles.addAll(custSettings.SCS_CampaignSocialHandles2__c.split(';',custSettings.SCS_CampaignSocialHandles2__c.length()));
        }
        if(custSettings.SCS_CampaignSocialHandles3__c != null){
            handles.addAll(custSettings.SCS_CampaignSocialHandles3__c.split(';',custSettings.SCS_CampaignSocialHandles3__c.length()));
        }
        //Set<String> handles2 = new Set<String>(custSettings.SCS_CampaignSocialHandles2__c.split(';',custSettings.SCS_CampaignSocialHandles2__c.length()));
        
        //hardcoded social handles
     // Set<String> handles = new Set<String>{'Social Airline Italian','KLM', 'KLM Italia', 'KLM Kazakhstan', 'KLM Russia', 'KLM Japan', 'KLM Brasil', 'KLM Norway','KLM Dutch Caribbean','KLM Suriname','KLM Philippines', 'Air France', 
      //    'KLM Portugal', 'KLM China'};
        if(handles.contains(post.Handle) && post.Provider.equals('Facebook')){
              post.Is_campaign__c = true;
              //update post;
        }
        
               
        return post;
    }
    
    /**
    * This method deletes the last case feed item. 
    * Restrict: creation double case feed
    */
    private void deleteLastCaseFeed(SocialPost post){
        List<FeedItem> fi=[SELECT id FROM FeedItem WHERE parentId=:post.Id ORDER BY CreatedDate DESC LIMIT 1];
        try{
            delete fi[0];
        } catch(Exception e) {
            System.debug('************'+e.getMessage());
        }
    }
    
    global virtual Boolean usePersonAccount() {
        return false;
    }

    global virtual String getDefaultAccountId() {
        return null;
    }
    
    /** 
    * Method for automatic creation of cases for Wall Posts and Private Messages
    * Use "createNewCases" method in ControllerHelper class
    * 
    */
    private void autoCreateCase(SocialPost post){
        Case newCase=null;
        //we can delete this if, since the separation based on message type is handled in line:128
        if('Private'.equals(post.MessageType) || 'Post'.equals(post.MessageType)){
            AFKLM_SCS_ControllerHelper helper = new AFKLM_SCS_ControllerHelper();
            Map<String, SocialPost> postMap = new Map<String, SocialPost>();
            postMap.put(post.Id, post);     
            helper.createNewCases(postMap, '', '', false);
            
            
        }
    }
    
    private void handleNotCorrectPersonas(SocialPersona persona){
        if(persona.ProfileUrl.length()>255){
            persona.ProfileUrl = 'https://www.facebook.com/'+persona.ProfileUrl.substring(persona.ProfileUrl.lastIndexOf('/')+1,persona.ProfileUrl.length());
        }
    }
    /**
    * Inbound Social Handler for 'Social Post' and 'Social Persona' inserted by Radian6 / Social Hub rules and data sources.
    *
    */
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, 
                                                        SocialPersona persona,
                                                        Map<String, Object> rawData) {
        Boolean createSocialPersonaWithPostLink = false;    //DvtH14SEP2019 TempAccountFix
        Social.InboundSocialPostResult facebookResult = new Social.InboundSocialPostResult();
        facebookResult.setSuccess(true);
        
        handleNotCorrectPersonas(persona);
        if (post.Id != null) {
                update post;
            if (persona.id != null) {
                try{
                    update persona;
                } catch (Exception e) {
                    System.debug(e.getMessage());
                }
            }
            return facebookResult;
        }
        
            findReplyTo(post, rawData);

        // List<Account> facebookWithPersonAccount = [SELECT Id, sf4twitter__Fcbk_Username__pc FROM Account WHERE sf4twitter__Fcbk_Username__pc = :persona.Name LIMIT 1];
        String personaExternalIdWithPrefix = 'FB_' + persona.ExternalId;
        
        List<Account> facebookWithPersonAccount = [SELECT Id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc = :personaExternalIdWithPrefix LIMIT 1];
        Case parentCase;

        if (persona.Id == null) {
            // Logic to check if "Person Account" exist first, else create a Persona with default temporary "Person Account"
            if(!facebookWithPersonAccount.isEmpty()) {
                persona.parentId = facebookWithPersonAccount[0].Id;
                insert persona;

                // Fix for adding the Social Post to an existing personAccount
                post.WhoId = facebookWithPersonAccount[0].Id;
            } else {
                //createPersona(persona); //DvtH14SEP2019 TempAccountFix: Nopped out!
                createSocialPersonaWithPostLink = true; //DvtH14SEP2019 TempAccountFix: 
            }
        }
        else {
            // Fix for adding the Social Post to an existing personAccount
            if(!facebookWithPersonAccount.isEmpty()) {
                post.WhoId = facebookWithPersonAccount[0].Id;
            }
            try{
                update persona;
            } catch (Exception e) {
                System.debug(e.getMessage());
            }
        }
        
        post.PersonaId = persona.Id;
        
        //Added by Sai. To link post with the exisitng case for KLM. JIRA ST-1415.
        parentCase =  findParentCase(post, persona, rawData);
        
        if(post.Content != null && post.Content.length()> 255){
            if(post.Content.length()>5000){
                post.Content = post.Content.substring(0,5000);
            }
            post.Short_Content__c = post.Content.substring(0,255);
        } else {
            post.Short_Content__c = post.Content;
        }
        
        

        insert post;
        //DvtH14SEP2019 TempAccountFix: --> 
        if(createSocialPersonaWithPostLink) {
            if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) || String.isBlank(persona.Provider)) {
                //In this case we cannot create the persona!    
            } else {
                //create the persona with a link back to the SocialPost
                persona.parentId = post.Id;
                insert persona;
                post.PersonaId = persona.Id;
                update post;
            }
        }
        //<---
        if(('Private'.equals(post.MessageType) || 'Post'.equals(post.MessageType))){
             autoCreateCase(post);
             deleteLastCaseFeed(post);
        }
        
        return facebookResult;  
    }

    /** 
    * Method for finding parent cases and attaching related comments and replies to the same case
    * 
    */
    private Case findParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        SocialPost replyToPost = null;
        Boolean caseUpdateflag =false;
        Boolean updatefromcase =false;
        
        List<Case> cs = new List<Case>();
        //<RecordType> recordTypeIds = [SELECT Id FROM RecordType WHERE (Name = 'AF Servicing' OR Name = 'Servicing') AND SobjectType = 'Case'];
        String seg = 'Servicing';
        if(post.TopicProfileName.contains('france') || post.TopicProfileName.contains('France') /*|| post.TopicProfileName.contains('Social Airline Italian') */) {
            seg = 'AF Servicing';
        }
        //modified by sai. To find parent for Post. JIRA ST-1415.
        If(('Post'.equals(post.MessageType) || 'Comment'.equals(post.MessageType) ||'Reply'.equals(post.MessageType)) && String.isNotBlank(post.Recipient))
        {
            //read rawdata to find the radian6 post Id  
            string r6PstId = String.valueof(rawdata.get('r6ParentPostId'));
            // read rawdata to find External post id
            string extPostId = string.valueof(rawdata.get('replyToExternalPostId'));
            replyToPost = post.ReplyTo;
            
            //added by sai. To find parent. JIRA ST-1415.
            if('Post'.equals(post.MessageType))
            {
                post = checkCampaign(post);
            }
            if(((post.Is_campaign__c == True && !'Post'.equals(post.MessageType)) || (post.Is_campaign__c == False && 'Post'.equals(post.MessageType))) && post.whoid != null)
            {
                //By Sai. Adding a Record type check on Finding Parent. JIRA ST-1936.
                cs = [SELECT Id, Status, isClosed, ClosedDate,Language__c,Is_Campaign__c ,RecordTypeId,Data_for_Prefill_By_DG__c,dgAI2__DG_Fill_Status__c FROM Case WHERE accountid =: post.whoid AND Is_campaign__c = False  AND RecordTypeId =: recordTypeIds.get(seg) order by createddate desc LIMIT 1];
                if(cs!=null && cs.size() >0)
                {
                    updatefromcase=True;
                    if (cs[0].isClosed && !(cs[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase()))) {


                        cs.clear();
                        updatefromcase =False;
                        }
                }
            }
            
            if(replyToPost != null && !(updatefromcase)) {
                if(replyToPost.ParentId != null) {
                    cs = [SELECT Id, Status, isClosed, ClosedDate,Language__c,Is_Campaign__c ,RecordTypeId,Data_for_Prefill_By_DG__c,dgAI2__DG_Fill_Status__c FROM Case WHERE Id =: replyToPost.ParentId AND RecordTypeId IN: recordTypeIds.values() LIMIT 1];
                }
            }
            else{
                if((r6PstId != Null || extPostId != Null) && !(updatefromcase)){
                    cs = [Select Id,Language__c,Status,Is_Campaign__c ,Data_for_Prefill_By_DG__c,dgAI2__DG_Fill_Status__c from Case where Id IN (Select ParentId from SocialPost where (R6PostId =: r6PstId OR ExternalPostId =: extPostId)) AND RecordTypeId IN: recordTypeIds.values() LIMIT 1];
                }
            }
            //Modified the language update logic on unsupported posts. JIRA ST-1498     
            if (cs!=null && !cs.isEmpty()) {
                //Added by Ata for ST-2953
                if((cs[0].Is_Campaign__c == False)&&((post.posttags.toLowerCase().contains('unsupported')) || (post.posttags == '' || post.posttags == Null))){
                
                    //pick the latest actioned post to get its language
                    list<SocialPost> sPost = [Select id,language__c,PostTags from socialpost where Parentid =: cs[0].id And IsOutbound=False AND SCS_Status__c = 'Actioned' AND (language__c != '' OR language__c != 'Unpublished' OR language__c != 'Unsupported') order by Createddate Desc limit 1]; 
                    
                    //editing post's language
                    if(!sPost.isEmpty()){
                        post.oldposttag__c= post.posttags;
                        post.PostTags = sPost[0].posttags;
                        
                        if(cs[0].Language__c == '' || cs[0].Language__c == Null || cs[0].Language__c.toLowerCase().contains('unsupported')
                            || cs[0].Language__c.toLowerCase() != sPost[0].language__c.toLowerCase()){
                            
                            cs[0].Language__c = sPost[0].language__c;
                            caseUpdateflag = true;
                        }
                    } 
                } //end                  
                if(!cs[0].isClosed){
                        
                    if(cs[0].RecordTypeId != NULL && cs[0].RecordTypeId != recordTypeIds.get(seg)) {
                        return null;
                    }

                    //added by Sai.To attach parent. JIRA ST-1415.
                    if(updatefromcase)
                    {
                        post.ParentId = cs[0].id;
                    }
                    else
                    {
                        post.ParentId = replyToPost.ParentId;
                    }
                    if(post.replyTo.Is_Campaign__c == true){
                        post.Is_Campaign__c = true;
                        
                    }
                    
                    if(post.Is_Campaign__c == true){
                        //Modified By Nagavi for Jira:143 to check Is campaign field of cases
                        if(cs[0].Is_Campaign__c != true){
                            cs[0].Is_Campaign__c = true;
                            caseUpdateflag=true;
                        }
                    }
                    
                    //Added for ST-1358 : Case Topic Filling by DG : Nagavi 12/29/2016
                    if(post.content!=null && cs[0].Status=='New' && cs[0].RecordTypeId == recordTypeIds.get(seg) && post.IsOutbound==false){
                        cs[0].Data_for_Prefill_By_DG__c=AFKLM_SCS_ControllerHelper.gettruncatedString(cs[0].Data_for_Prefill_By_DG__c,post.content);
                        //if(cs[0].dgAI2__DG_Fill_Status__c!='Pending')
                            cs[0].dgAI2__DG_Fill_Status__c='Pending'; 
                        //dgAI2.DG_PredictionTriggerHandler.doPrediction(new List<Case>{cs[0]});  
                        caseUpdateflag=true;
                    }

                    if(caseUpdateflag)
                        update cs[0];
                    
                    return cs[0];
                }
            
                if(cs[0].isClosed || 'Closed - No Response'.equals(cs[0].Status)){
                    if(post.Is_Campaign__c == true){
                        post.parentId = cs[0].id;
                        //Modified By Nagavi for Jira:143 to check Is campaign field of cases
                        if(cs[0].Is_Campaign__c != true){
                            cs[0].Is_Campaign__c = true;
                            caseUpdateflag=true;
                        }
                        
                        if(caseUpdateflag)
                            update cs[0];
                    
                        return cs[0];
                    }else{

                        if(cs[0].RecordTypeId != NULL && cs[0].RecordTypeId != recordTypeIds.get(seg)) {
                            return null;
                        }

                        reopenCase(cs[0]);

                        post.ParentId = cs[0].Id;
                        return cs[0];
                    }
                }
            }
        }
        else if ('Private'.equals(post.MessageType) && String.isNotBlank(post.Recipient)) {
            String personaPrefixFBId = 'FB_'+persona.ExternalId;
            //IBO:Fix for persona with "TemporaryPersonAccount"
            List<Account> acc=[SELECT id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc =: personaPrefixFBId LIMIT 1];
            if(!acc.isEmpty()){
                cs = [SELECT Id, ClosedDate, isClosed, RecordTypeId,Data_for_Prefill_By_DG__c,dgAI2__DG_Fill_Status__c FROM Case WHERE AccountId =: acc[0].Id AND RecordTypeId =: recordTypeIds.get(seg) ORDER BY CreatedDate DESC LIMIT 1];
                if(!cs.isEmpty()) {
                        if(!cs[0].isClosed){
                            if(cs[0].RecordTypeId != NULL && cs[0].RecordTypeId != recordTypeIds.get(seg)) {
                                return null;
                            }

                            post.ParentId = cs[0].Id;
                            //post.ParentId = cs[0].ParentId;
                            return cs[0];
                        }
                        
                    if(cs[0].isClosed || 'Closed - No Response'.equals(cs[0].Status)){
                        if (cs[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase())) {
                            if(cs[0].RecordTypeId != NULL && cs[0].RecordTypeId != recordTypeIds.get(seg)) {
                                return null;
                            }
                            //Customer_Response_Case_Open_Settings__mdt cmdt = [select id,Customer_Response__c from Customer_Response_Case_Open_Settings__mdt where DeveloperName = 'Customer_Response'];
                            //if(!(cmdt.Customer_Response__c).contains(post.Content))         
                                reopenCase(cs[0]);
                            post.ParentId = cs[0].Id;
                            return cs[0];
                        }
                    }
                }
            }
        }
        return null;
    } 
    
    /** 
    * Method for reopening case
    * IF case status is closed and user replies on social post within 24 hours, case status should be set to "Reopened"
    *
    */
    private void reopenCase(Case parentCase){
        parentCase.Status = 'Reopened'; 
        parentCase.SCS_Reopened_Date__c = Datetime.now();
        parentCase.Sentiment_at_close_of_case__c = '';
        update parentCase;
    }

    /** 
    * findReplyTo
    * 
    */ 
    private void findReplyTo(SocialPost post, Map<String, Object> rawData) {
        // Fixed By Ata--ticket#2993 conversion issue
        String replyToId = String.valueof(rawData.get('replyToExternalPostId'));
        if (String.isBlank(replyToId)) 
            return;

        List<SocialPost> postList = [SELECT Id, Status, ParentId, IsOutbound, PersonaId, ExternalPostId, Is_Campaign__c FROM SocialPost WHERE ExternalPostId = :replyToId LIMIT 1];
        if (!postList.isEmpty()) {
            post.ReplyToId = postList[0].id;
            post.ReplyTo = postList[0];
            //post.ParentId = postList[0].ParentId;
            post.Is_Campaign__c = postList[0].Is_Campaign__c;
        }
    }
        
    /** 
    * Method for creating SocialPersona
    * SocialPersona is created automatically with Parent "TemporaryPersonAccount"
    * 
    */  
    private void createPersona(SocialPersona persona) {
        if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) ||
            String.isBlank(persona.Provider)) 
            return;

        // Select/Create by personAccount.Name "TemporaryPersonAccount"?
        //  this is the "TemporaryPersonAccount" since the creation of new "Person Account" should be done when "Create Case" is selected by agents. 
        List<Account> accountList = [SELECT Id FROM Account WHERE Name = 'TemporaryPersonAccount' LIMIT 1];
        
        if ( !accountList.isEmpty()) {
            persona.parentId = accountList[0].Id;
        }        
        insert persona;
    }
}