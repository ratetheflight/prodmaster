/**********************************************************************
 Name:  AFKLM_SCS_InboundMessengerHandlerImpl 
 Task:    N/A
 Runs on: AFKLM_SCS_InboundMessengerHandlerImpl 
======================================================
Purpose: 
   Inbound Social Handler for facebook Messenger
   Creates Social Posts and handles business logic with Social Customer Service enablement.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR        DATE             DETAIL                                 
    1.0     Nagavi        02/17/2017       ST-1358 - Development- Case Topic Filling by DG in UAT 
    1.1     Sai Choudhry  02/03/2017       Removing the SOQL query for Record Type, JIRA ST-1868.
    1.2     Sai Choudhry  04/04/2017       Populating the chat Medium. JIRA ST-2207.
    2.0     Nagavi        01/31/2016       ST-1358 - Development- Case Topic Filling by DG in UAT
***********************************************************************/
global with sharing class AFKLM_SCS_InboundMessengerHandlerImpl {
    private List<SocialPersona> personasToInsert = new List<SocialPersona>();
    private Set<SocialPersona> personasToInsertSet = new Set<SocialPersona>();
    
    private Map<Id, SocialPersona> personasToUpdate = new Map<Id, SocialPersona>();
    private Map<Id,Case> casesMapToUpdate = new Map<Id,Case>();

    private List<SocialPost> postsToInsert = new List<SocialPost>();    
    //private Set<SocialPost> setPostsToUpdate = new Set<SocialPost>();   
    private Map<Id, SocialPost>setPostsToUpdate = new Map<Id, SocialPost>();
    private List<SocialPost> postsToUpdate = new List<SocialPost>(); 
    private List<SocialPost> autoCreateCasePosts = new List<SocialPost>();    
    
    private List<Case> cs = new List<Case>();
    private List<Case> createdCases = new List<Case>();
    //private List<SocialPost> existingPosts = new List<SocialPost>();

    //private List<Account> createdAccounts = new List<Account>();
    private Set<String> tempAccountSet = new Set<String>();

    private List<String> personaExternalIds = new List<String>();
    private List<String> prefixPersonaExternalIds = new List<String>();
    private List<String> accountIds = new List<String>();
    //private List<String> postExternalIds = new List<String>();
    private List<String> postChatIds = new List<String>();
    
    Private Set<ID> accountIdSet= new Set<ID>();
    
    //Manuel Conde ST-002810
    //Modified by Sai. Removing the SOQL query for Record Type, JIRA ST-1868.
    //AFKLM_SCS_CaseRecordTypeSingleton crt = AFKLM_SCS_CaseRecordTypeSingleton.getInstance();
    public ID klmRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();

    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        // SCH: SLA reopen the case and attach the reply to a case after 24 hours = 1 day
        return 1;
    }
           
    /**
    * Inbound Social Handler for 'Social Post' and 'Social Persona'
    *
    */
    public PageReference handleInboundSocialPost(List<SocialPost> posts, List<SocialPersona> personas, List<String> externalIds) {

        AFKLM_SCS_MessengerAutocreationOfCase messengerAutoCreateCases = new AFKLM_SCS_MessengerAutocreationOfCase(); 

        // clear all the lists
        personasToUpdate.clear();
        casesMapToUpdate.clear();
        personasToInsert.clear();
        personasToInsertSet.clear();

        //List<SocialPost> existingPosts = new List<SocialPost>();
        Set<String> existingIds = new Set<String>();
        for(SocialPost post : [SELECT ExternalPostId FROM SocialPost WHERE CreatedDate IN(YESTERDAY, TODAY) AND ExternalPostId IN :externalIds])
            existingIds.add(post.ExternalPostId);

        for(SocialPost spost : posts){
            if(!existingIds.contains(spost.ExternalPostId__c)) {
                postChatIds.add(spost.chatHash__c);
            }
        }

        for(SocialPersona spersona : personas){
            if(spersona != null && spersona.ExternalId != null){
               /* if(spersona.parentid != null)
                {
                    accountIdSet.add(spersona.parentid);
                }*/
                personaExternalIds.add( spersona.ExternalId );
                //system.debug('--+ add persona: '+spersona.ExternalId);                            
            }
        }

        //Query linked accounts to already existing personas to query cases created via radian6 flow -> update with chatHash
       
        // List of personas
        //List<SocialPersona> personaList = [SELECT Id, ParentId, ExternalId FROM SocialPersona WHERE ExternalId IN :personaExternalIds AND Provider = 'Facebook' LIMIT 200];
        //Manuel Conde: Short term fix to resolve App Scoped Id issue
        //system.debug('--+ before the query: '+personaExternalIds);
        List<SocialPersona> personaList = [SELECT Id, ParentId, ExternalId, DynamicExternalId__c FROM SocialPersona WHERE DynamicExternalId__c IN :personaExternalIds AND Provider = 'Facebook' LIMIT 200];

        //system.debug('--+ personaList: '+personaList);

        //check/create personas and attach them to created accounts.
        for(SocialPersona persona : personas) {
            if(matchPersona(persona, personaList) == false) {
                createPersona(persona);        
            } 
            prefixPersonaExternalIds.add('FB_'+persona.ExternalId); 
        }
        //system.debug('--+ prefixPersonaExternalIds: '+prefixPersonaExternalIds);

        for(Account acc : [SELECT id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc IN: prefixPersonaExternalIds]){
            accountIds.add(acc.Id);
        }
        /*
        for(Account acc : [SELECT id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc IN: accountIdSet]){
            accountIds.add(acc.Id);
        }
        */

        //Logic: Query last opened case for specific customer...Query only case but put all cases based on posts.ExternalId into one list
        //
        //createdCases = [SELECT Id, ClosedDate, AccountId, isClosed, chatHash__c,Data_for_Prefill_By_DG__c,dgAI2__DG_Fill_Status__c,Status FROM Case WHERE (Delete_Case__c = false AND CreatedDate IN(THIS_WEEK,LAST_WEEK)) AND (chatHash__c IN : postChatIds OR AccountId IN: accountIds) and recordtypeID=:crt.klmRt.Id ORDER BY CreatedDate DESC];

        createdCases = [SELECT Id, ClosedDate, AccountId, isClosed, chatHash__c,Status,recordtypeid,Data_for_Prefill_By_DG__c,dgAI2__DG_Fill_Status__c FROM Case WHERE Delete_Case__c = false AND (chatHash__c IN : postChatIds OR AccountId IN: accountIds) AND (Isclosed = False OR (Isclosed = True AND Closeddate IN(THIS_WEEK,LAST_WEEK))) and recordtypeID=:klmRtId ORDER BY CreatedDate DESC];



        if(!personasToUpdate.isEmpty()) {
            update personasToUpdate.values();
        }

        //system.debug('--+ personasToInsert: '+personasToInsert);

        // Deduplicate
        if(!personasToInsert.isEmpty()){

            personasToInsertSet.addAll( personasToInsert );
            personasToInsert.clear();        
            personasToInsert.addAll( personasToInsertSet );           
        }
        
        //upsert personasToInsert; //DvtH05OCT2019 TempAccountFix: done later
        // NEW CODE





        for(SocialPost post : posts){          




            if(!existingIds.contains(post.ExternalPostId__c)) {

                for(SocialPersona persona : personasToUpdate.values()){
                    //added if because of deployment/test class SeeAllData=true
                    if(post != null && persona != null && post.Handle == persona.Name){
                            
                        post.PersonaId = persona.Id;
                        String pid = persona.parentId;
                        if(pid != null && !pid.substring(0,3).equals('0ST')) {
                            post.WhoId = pid;
                        }

                        break;
                    }
                }

                cs.clear();
                for(Case tmpCase : createdCases){


                    if(tmpCase.chatHash__c != null && tmpCase.chatHash__c == post.chatHash__c){
                            
                        cs.add(tmpCase);
                        break;

                    } else {
                            
                        if(tmpCase.AccountId == post.WhoId)
                        {
                         //added by sai. Populating the chat Medium. JIRA ST-2207.
                         //Start
                         if(tmpCase.chatHash__c != post.chatHash__c){
                             tmpCase.chatHash__c = post.chatHash__c;
                          }
                         if(post.Chat_Medium__c!= null && tmpCase.Chat_Medium__c != post.Chat_Medium__c){
                             tmpCase.Chat_Medium__c = post.Chat_Medium__c;
                          }
                          //end.
                            cs.add(tmpCase);

                            if(!casesMapToUpdate.containsKey(tmpCase.id) && !tempAccountSet.contains(tmpCase.AccountId)){
                                casesMapToUpdate.put(tmpCase.id,tmpCase);
                                tempAccountSet.add(tmpCase.AccountId);                                
                            }
                            break;
                        }
                    }
                }

                 
                findParentCase(post, cs);                               

                if(post.Content != null) {
                    if(post.Content.length()> 255){
                        post.Short_Content__c = post.Content.substring(0,255);
                    } else {
                        post.Short_Content__c = post.Content;
                    }
                }

                postsToInsert.add(post);
            }

        }
        // END OF NEW CODE

        //if case was reopened, it needs to be updated
        update casesMapToUpdate.values();

        //insert all social posts from bulk
        //upsert postsToInsert;
        if(postsToInsert.size() > 0){
            Database.UpsertResult[] results = Database.upsert( postsToInsert, SocialPost.Fields.ExternalPostId__c ,false ) ;
            for(Integer i=0;i<results.size();i++){
                if (!results.get(i).isSuccess()){
                    Database.Error err = results.get(i).getErrors().get(0);
                    System.debug('Error - '+err.getMessage() + '\nStatus Code : '+err.getStatusCode()+'\n Fields : '+err.getFields());
                }
            }
        }

        //DvtH05OCT2019 TempAccountFix: -->
        //update persona's
        for (SocialPersona persona : personasToInsert) {
            if (persona.parentId==null) { //If it is filled in then there is no need to overwrite
                for (SocialPost post : posts) {
                    if (post != null && post.Handle == persona.Name) {
                            
                        persona.parentId = post.Id;
                        break;                        
                    }
                }
            }
        }

        Integer i = 0;
        while(i<personasToInsert.size()){
            
            if(personasToInsert.get(i).parentId==null) {
                personasToInsert.remove(i);
            }
                
            i++;
        }

        insert personasToInsert;

        postsToUpdate.clear();
        if (!personasToInsert.isEmpty()) {
            for(SocialPersona persona : personasToInsert){ //Use only to insert list to update posts
                for(SocialPost post : posts){

                    if(post.Handle == persona.Name){
                        
                        post.PersonaId = persona.Id;
                        if(!setPostsToUpdate.containsKey(post.Id)){

                            setPostsToUpdate.put(post.Id, post);
                        }
                    }
                }
            }
        }

        if(setPostsToUpdate != null && !setPostsToUpdate.isEmpty()) {
            postsToUpdate.addAll(setPostsToUpdate.values());
            update postsToUpdate;
        }

        //new auto creation of cases from 210 - 220...also commented out lines 186 - 189
        for(SocialPost socPost : posts){
            if(socPost.parentId == null && socPost.SCS_isOutbound__c == false){
                autoCreateCasePosts.add(socPost);
            }
        }

        if(!autoCreateCasePosts.isEmpty()){
            messengerAutoCreateCases.autoCreateCase(autoCreateCasePosts); 
            deleteLastCaseFeed(autoCreateCasePosts);     
        }    

        return null;
    }

    /**
     * Match persona - check if persona already exists
     *
     */
    private Boolean matchPersona(SocialPersona persona, List<SocialPersona> personas) {

        for(SocialPersona socPersonaFromList : personas){

            //if(persona.ExternalId == socPersonaFromList.ExternalId){
            //Manuel Conde: Short term fix to resolve App Scoped Id issue
            //system.debug('--+ persona.ExternalId: '+persona.ExternalId);
            //system.debug('--+ socPersonaFromList.DynamicExternalId__c: '+socPersonaFromList.DynamicExternalId__c);
            if(persona.ExternalId == socPersonaFromList.DynamicExternalId__c){

                persona.Id = socPersonaFromList.Id;
                persona.ParentId = socPersonaFromList.ParentId;
                persona.ExternalId = socPersonaFromList.ExternalId;

                if(personasToUpdate.size() > 0) {

                    if( personasToUpdate.containsKey(persona.Id) == false)
                        personasToUpdate.put(persona.Id, persona);
                } else {

                    personasToUpdate.put(persona.Id, persona);
                }

                return true;
            }
        }

        return false;
    }

    /** 
    * Method for finding parent cases and attaching related messages to the same case
    * 
    */
    private Case findParentCase(SocialPost post, List<Case> cs) {
              
        if (!cs.isEmpty()) {
            cs[0] = reopenClosedCaseLogic(cs, post);
            
            if(cs[0] != null) {
                return cs[0];
            }
        }

        return null;
    } 
    
    private Case reopenClosedCaseLogic(List<Case> cases, SocialPost post) {
        
        Case returnCase = null;
        if (!cases[0].IsClosed) { 
            post.ParentId = cases[0].Id;
            //Added by Nagavi for ST-1358 - Development- Case Topic Filling by DG in UAT
            if(post.content!=null && cases[0].Status=='New' && cases[0].recordtypeID==klmRtId && post.IsOutbound==false){
                cases[0].Data_for_Prefill_By_DG__c=AFKLM_SCS_ControllerHelper.gettruncatedString(cases[0].Data_for_Prefill_By_DG__c,post.content);
                //if(cs[0].dgAI2__DG_Fill_Status__c!='Pending')
                   cases[0].dgAI2__DG_Fill_Status__c='Pending';         
                //dgAI2.DG_PredictionTriggerHandler.doPrediction(new List<Case>{cs[0]});    
                }
                if(!casesMapToUpdate.containsKey(cases[0].id)){
                    casesMapToUpdate.put(cases[0].id,cases[0]);
                }
                

            
            returnCase = cases[0];
        }

        if (cases[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase())) {

            //Manuel Conde trello #26: Stop reopening Cases on Outbound messages
             if(!post.IsOutbound){
                try{
                String message = post.Content;
                String regex = '[^a-zA-Z0-9]';
                Pattern regexPattern = Pattern.compile(regex);
                Matcher regexMatcher = regexPattern.matcher(message);
                String replacedMessage;
                String [] finalMsg = new String []{};
                if(regexMatcher.find()) {
                   finalMsg = ((message.replaceAll( '\\s+', '')).replaceAll(regex, '|')).split('\\|',2) ;
                   replacedMessage = finalMsg[0];
                }else{
                   replacedMessage =  message.replaceAll( '\\s+', '');
                   finalMsg.add(replacedMessage);
                }
                System.debug(replacedMessage);
                Customer_Response_Case_Open_Settings__mdt cmdt = [select id,Customer_Response__c from Customer_Response_Case_Open_Settings__mdt where DeveloperName = 'Customer_Response'];
                if(replacedMessage !=''){
                    if(finalMsg.size()>1){
                        if(!(cmdt.Customer_Response__c).contains(replacedMessage.toUpperCase()) || finalMsg[1].length()>5)
                            reopenCase(cases[0]);       
                    }else{
                        if(!(cmdt.Customer_Response__c).contains(replacedMessage.toUpperCase()))
                            reopenCase(cases[0]);
                    }
                }
               }catch(Exception e){reopenCase(cases[0]);}
   
            }

            post.ParentId = cases[0].Id;
            returnCase = cases[0];
        }
        
        return returnCase;
    }
    
    /** 
    * Method for reopening case
    * IF case status is closed and user replies on social post within 24 hours, case status should be set to "Reopened"
    *
    */
    private void reopenCase(Case parentCase) {
        parentCase.Status = 'Reopened'; 
        parentCase.SCS_Reopened_Date__c = Datetime.now();
        parentCase.Sentiment_at_close_of_case__c = '';
        //update parentCase;
        if(!casesMapToUpdate.containsKey(parentCase.id)){
            casesMapToUpdate.put(parentCase.id,parentCase);
        }
    }

    /** 
    * Method for creating SocialPersona
    * SocialPersona is created automatically with Parent "TemporaryPersonAccount"
    * 
    */  
    private void createPersona(SocialPersona persona) {
            
        if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) 
            || String.isBlank(persona.Name) || String.isBlank(persona.Provider)) 
            return;

        personasToInsert.add(persona);
    }

    private void deleteLastCaseFeed(List<SocialPost> postsToCheck){
        List<String> forDuplicates = new List<String>();
        Map<FeedItem,FeedItem> feedItemsToDelete = new Map<FeedItem,FeedItem>();

        for(SocialPost sp : postsToCheck){
            forDuplicates.add(sp.Id);
        }

        List<FeedItem> fi=[SELECT id FROM FeedItem WHERE parentId IN: forDuplicates ORDER BY CreatedDate];

        //fi.sort();

        for(FeedItem f : fi){
            if(!feedItemsToDelete.containsKey(f)){
                feedItemsToDelete.put(f, f);
            }
        }
        
        try{
            delete feedItemsToDelete.values();
        } catch(Exception e) {
            System.debug('************'+e.getMessage());
        }
    }

}