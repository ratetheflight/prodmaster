@isTest
private class AFKLM_SCS_InboundMessengerHandlerTest {
    
    @isTest static void handleInboundSocialPostTest() {
        
        AFKLM_SCS_InboundMessengerHandlerImpl handlerTest = new AFKLM_SCS_InboundMessengerHandlerImpl();

        List<SocialPost> posts = new List<SocialPost>();        
        List<SocialPersona> personas = new List<SocialPersona>();
        List<String> externalIdsList = new List<String>();

        Integer objectCount = 20;

        for(Integer i=0;i<=objectCount;i++){

            SocialPersona persona = new SocialPersona(
                Name = 'Testing Persona '+i,
                Provider = 'Other',
                ExternalPictureURL = 'picUrl',
                RealName = 'Testing Persona '+i,
                ExternalId = '123456787654'+i           
                );

            personas.add(persona);
        }

        Integer j=0;
        for(SocialPersona p : personas){

            SocialPost spost = new SocialPost(
                Name = 'Message from: '+p.Name,
                Content = 'This is content for testing social posts no. This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social. This content can be a little longer so we can hit the limit and cut it. Is it long enough? '+j,
                Handle = p.RealName,
                Language__c = 'English',
                SCS_Status__c = 'New',
                Posted = System.now(),
                ExternalPostId__c = '32778084357823458321481420897347890067_5039452hghjghj'+j,
                IsOutbound = false,
                Provider = 'Facebook', 
                Company__c = 'KLM',
                TopicProfileName = 'KLM',
                MessageType = 'Private',
                Notified__c = TRUE
                );

            posts.add(spost);
            externalIdsList.add(spost.ExternalPostId__c);

            j++;

        }

        handlerTest.handleInboundSocialPost(posts, personas, externalIdsList);
                
    }

    static testMethod void findParentCaseClosedCommentReOpenTest(){

        List<SocialPost> posts = new List<SocialPost>();        
        List<SocialPersona> personas = new List<SocialPersona>();
        List<String> externalIdsList = new List<String>();

        Account acc= new Account(
            Firstname='Test', 
            LastName='Account', 
            sf4twitter__Fcbk_User_Id__pc='FB_123456789');
        insert acc;
        
        Case cs = new Case(Status='Closed - No Response', AccountId=acc.id);
        insert cs;

        SocialPersona spersona = new SocialPersona(
            name='Test Persona', 
            provider = 'Facebook', 
            parentId = acc.id, 
            ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
            ExternalId='123456789');
        insert spersona;
        
        SocialPost spost = new SocialPost(
            name='Post from: Test Account',
            parentID=cs.id,
            Posted=System.now(),
            Handle='KLM',
            Provider='Facebook',
            Is_campaign__c=true,
            WhoId=acc.id,
            MessageType='Comment',
            chatHash__c='0000000111112345',
            ExternalPostId__c='1396604723809:f3da05e20a1267c585');
        insert spost;
        
        Test.startTest();

        SocialPost post = new SocialPost(
            name='Comment from: Test Account',
            Handle = 'Test Persona',
            Posted=System.now(),
            Recipient='663023410425460', 
            Provider='Facebook', 
            Is_campaign__c=false, 
            WhoId=acc.id,
            Content='Lorem Ipsum ...',
            MessageType='Comment',
            chatHash__c='0000000111112345',
            ExternalPostId__c='1396604723809:f3da05e20a1267c577',
            ReplyTo=spost);
        posts.add(post);
        externalIdsList.add(post.ExternalPostId__c);
        
        SocialPersona persona = new SocialPersona(
            name='Test Persona', 
            provider = 'Facebook', 
            parentId = acc.id, 
            ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
            ExternalId='123456789');
        personas.add(persona);

        AFKLM_SCS_InboundMessengerHandlerImpl handlerTest = new AFKLM_SCS_InboundMessengerHandlerImpl();
        handlerTest.handleInboundSocialPost(posts, personas, externalIdsList); 

        Test.stopTest();
    }


    static testMethod void findParentCaseClosedCommentNoReopenTest(){

        List<SocialPost> posts = new List<SocialPost>();        
        List<SocialPersona> personas = new List<SocialPersona>();
        List<String> externalIdsList = new List<String>();

        Account acc= new Account(
            Firstname='Test', 
            LastName='Account', 
            sf4twitter__Fcbk_User_Id__pc='FB_123456789');
        insert acc;
        
        Case cs = new Case(Status='Closed - No Response', AccountId=acc.id);
        insert cs;

        SocialPersona spersona = new SocialPersona(
            name='Test Persona', 
            provider = 'Facebook', 
            parentId = acc.id, 
            ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
            ExternalId='123456789');
        insert spersona;
        
        SocialPost spost = new SocialPost(
            name='Post from: Test Account',
            parentID=cs.id,
            Posted=System.now(),
            Handle='KLM',
            Provider='Facebook',
            Is_campaign__c=true,
            WhoId=acc.id,
            MessageType='Comment',
            chatHash__c='0000000111112345',
            ExternalPostId__c='1396604723809:f3da05e20a1267c585');
        insert spost;
        
        Test.startTest();

        SocialPost post = new SocialPost(
            name='Comment from: Test Account',
            Posted=System.now()-1,
            Recipient='663023410425460', 
            Provider='Facebook', 
            Is_campaign__c=false, 
            WhoId=acc.id,
            Content='Lorem Ipsum is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw, toen een onbekende drukker een zethaak met letters nam en ze door elkaar husselde om een font-catalogus te maken. Het heeft niet alleen vijf eeuwen overleefd maar is ook, vrijwel onveranderd, overgenomen in elektronische letterzetting. Het is in de jaren -60 populair geworden met de introductie van Letraset vellen met Lorem Ipsum passages en meer recentelijk door desktop publishing software zoals Aldus PageMaker die versies van Lorem Ipsum bevatten',
            MessageType='Comment',
            chatHash__c='0000000111112345',
            ExternalPostId__c='1396604723809:f3da05e20a1267c577',
            ReplyTo=spost);
        posts.add(post);
        externalIdsList.add(post.ExternalPostId__c);
        
        SocialPersona persona = new SocialPersona(
            name='Test Persona', 
            provider = 'Facebook', 
            parentId = acc.id, 
            ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
            ExternalId='123456789');
        personas.add(persona);

        AFKLM_SCS_InboundMessengerHandlerImpl handlerTest = new AFKLM_SCS_InboundMessengerHandlerImpl();
        handlerTest.handleInboundSocialPost(posts, personas, externalIdsList); 

        Test.stopTest();
    }    


    static testMethod void getMaxNumberOfDaysClosedToReopenCaseTest(){
        AFKLM_SCS_InboundMessengerHandlerImpl handlerTest = new AFKLM_SCS_InboundMessengerHandlerImpl();
        Integer numberOfDaysClosedToReopenCase = handlerTest.getMaxNumberOfDaysClosedToReopenCase();
        System.assertEquals(numberOfDaysClosedToReopenCase,1);
    }


    @isTest(SeeAllData=true) 
     static void triggerTestMethod() {
        Integer i = 50;
        List<Nexmo_Post__c> nexPost = new List<Nexmo_Post__c>();

        for(Integer j=0;j<=i;j++){
            Nexmo_Post__c np = new Nexmo_Post__c(
                From_User_Id__c='1234567'+j,
                Chat_Hash__c='1234567chatHash'+j,
                Language__c='English',
                Company__c='KLM',
                Message_Id__c='1234567messsageId'+j,
                Send_Time__c=System.now(),
                Source__c='Inbound',
                Text__c='This is testing content on NexmoPost object '+j,
                To_Id__c='1234567toId',
                Type__c='text',
                User_Real_Name__c='TestingRealName '+j,
                Notified__c=true,
                Social_Network__c='Facebook1',
                TopicProfileName__c='KLM Special'
                );

            nexPost.add(np);
        }
        
        test.startTest();
        insert nexPost;
        test.stopTest();        
    }

    
    @isTest(SeeAllData=true) 
    static void triggerTestOutboundMethod() {
        Integer i = 50;
        List<Nexmo_Post__c> nexPost = new List<Nexmo_Post__c>();

        for(Integer j=0;j<=i;j++){
            Nexmo_Post__c np = new Nexmo_Post__c(
                From_User_Id__c='1234567'+j,
                Chat_Hash__c='1234567chatHash'+j,
                Message_Id__c='1234567messsageId'+j,
                Send_Time__c=System.now(),
                Language__c='Unsupported',
                Company__c='KLM',               
                Source__c='Outbound',
                Text__c='This is testing content on NexmoPost object '+j,
                To_Id__c='1234567toId',
                Type__c='text',
                User_Real_Name__c='TestingRealName '+j,
                Social_Network__c='Facebook'
                );

            nexPost.add(np);
        }

        test.startTest();
        insert nexPost;
        test.stopTest();        
    }
}