/**********************************************************************
 Name:  AFKLM_SCS_InboundSocialPostHandlerImpl
 Task:    N/A
 Runs on: AFKLM_SCS_InboundFacebookHandlerImpl, AFKLM_SCS_InboundTwitterHandlerImpl 
======================================================
Purpose: 
   Inbound Social Handler for 'Social Post' and 'Social Persona' inserted by Radian6 / Social Hub rules and data sources. 
   Creates Social Posts, Persona after Social Customer Service enablement.

   Requirements on google docs: 1 / 2 / 10 / 12 / 27 / 31 
   The apex code delivered with the social customer service need to be updated in order to:
   - 1/2: Avoid create cases automatically 
   - 10: Remove the creation of person accounts automatically. 
   - 12: When customer replies back Social Post should be attached to same case. When te case is closed and SLA 24 hours.
   - 27: Attach the Social Post of same customer to the same case (with reply or mention only)
   - 31: Social Post are not attached to the same person account (Peter Vlucht). It's doing it after 'the 'Create Case' logic
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      06/01/2014      INITIAL DEVELOPMENT
    1.1     Stevano Cheung      07/01/2014      Modify changes: Requirement 1/2/10
    1.2     Stevano Cheung      19/02/2014      Modify changes: Requirement 12
    1.3     Stevano Cheung      21/02/2014      Modify changes: Requirement 27 / 31
    1.4     Stevano Cheung      19/04/2014      The logic for different Social media: e.g. Twitter, Facebook should be put into separate classes (easier to implement Business Logic)
    1.5     Stevano Cheung      07/10/2014      Winter 15 matchPersona(...) SOQL improvements
***********************************************************************/
global virtual class AFKLM_SCS_InboundSocialPostHandlerImpl implements Social.InboundSocialPostHandler
{
    /**
    * The method is used to handle the inbound 'Social Post' from Radian6 / Social Hub based on different Social Network, e.g. Twitter, Facebook.
    *
    * @param    SocialPost to insert and check within Salesforce
    * @param    SocialPersona to insert and check within Salesforce
    * @param    rawData get the attributes, fields value from Radian6
    * @return   result if the inbound Social Post is handled succesfully
    */
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        System.debug('@@@@@@@@@@@@@@@@@@ What kind of message type: ' +post.MessageType+ ' @@@@@@@@@@ MediaType: '+post.MediaType+ ' @@@@@@@@ Provider:' +post.Provider);
        
        Social.InboundSocialPostResult result = new Social.InboundSocialPostResult();
        
        if('Twitter'.equals(post.Provider)) { 
            AFKLM_SCS_InboundTwitterHandlerImpl twitterResult = new AFKLM_SCS_InboundTwitterHandlerImpl();
            matchPost(post);
            matchPersona(persona);
            result = twitterResult.handleInboundSocialPost(post, persona, rawData);
        } else if('Facebook'.equals(post.Provider)) {
            AFKLM_SCS_InboundFacebookHandlerImpl facebookResult = new AFKLM_SCS_InboundFacebookHandlerImpl();
            matchPost(post);
            matchPersona(persona);
            result = facebookResult.handleInboundSocialPost(post, persona, rawData);
        }
        
        return result;
    }

    /**
    * For every 'Social Media', e.g. Twitter, Facebook and so on there should be a 'Social Post'.
    * It will check if the incomming 'Social Post' from Radian6 matches an existing Salesforce 'Social Post'.
    */    
    private void matchPost(SocialPost post) {
        if (post.Id != null || post.R6PostId == null) {
            return;
        }

        List<SocialPost> postList = [SELECT Id FROM SocialPost WHERE R6PostId = :post.R6PostId LIMIT 1];
        if (!postList.isEmpty()) {
            post.Id = postList[0].Id;
        }
    }

    /**
    * For every 'Social Media', e.g. Twitter, Facebook and so on there should be a 'Social Persona'.
    * It will check if the incomming 'SocialPersona' from Radian6 matches an existing Salesforce 'SocialPersona.
    */  
    private void matchPersona(SocialPersona persona) {
        if (persona != null && String.isNotBlank(persona.ExternalId)) {
            List<SocialPersona> personaList = [SELECT Id, ParentId FROM SocialPersona WHERE Provider = :persona.Provider AND ExternalId = :persona.ExternalId LIMIT 1];
            
            if ( !personaList.isEmpty()) {
                persona.Id = personaList[0].Id;
                persona.ParentId = personaList[0].ParentId;
                //update on persona commented out because of the issue UNABLE TO LOCK ROW from Salesforce - SF release 195 should fix this
                try { 
                
                    update persona; 
                
                } catch(Exception e) { 

                    System.debug('--+ Error updating social persona: ' + e.getMessage()); 
                } 

            }
        }
    }
}