/**********************************************************************
 Name:  AFKLM_SCS_InboundTwitterHandlerImpl
 Task:    N/A
 Runs on: AFKLM_SCS_InboundSocialPostHandlerImpl
======================================================
Purpose: 
   Inbound Social Handler for Twitter based on Social Hub rules and data sources. 
   Creates Social Posts and handles business logic with Social Customer Service enablement.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      19/03/2014      INITIAL DEVELOPMENT
    1.1     Stevano Cheung      19/03/2014      FindParentCase() method has multiple similar if. Is refactored for seperate method.
    1.2     Ata                 18/05/2016      Copy Language info from latest actioned Social post to the new social post.
    1.3     Sai Choudhry        13/09/2016      Marking the social post related to twitter Survey as outbound message.JIRA ST-840.
    1.4     Sai Choudhry        29/11/2016      Clearing the Twitter survey batch close filed on case reopen. JIRA ST-1303.
    1.5     Sai Choudhry        06/01/2017      Modified the Language update logic for unsupported posts. JIRA ST-1498.
    1.6     Sai Choudhry        21/02/2017      Auto Case creation for Twitter Messages. JIRA ST-1645.
    1.7     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868.
    1.8     Sai Choudhry        10/03/2017      Mark the English Retweets automatically as reviewed. JIRA ST-1896.
    1.9     Aruna Palanisamy    31/03/2017      Update Agent Status as Survey sent JIRA ST-1888.
***********************************************************************/
global virtual class AFKLM_SCS_InboundTwitterHandlerImpl implements Social.InboundSocialPostHandler {


    private Map<String, Id> recordTypeIds {

        get {

            if(recordTypeIds == NULL) {

                recordTypeIds = new Map<String, Id>();
                ID klmRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
                recordTypeIds.put('Servicing',klmRtId);
                ID afRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('AF Servicing').getRecordTypeId();
                recordTypeIds.put('AF Servicing',afRtId);
                //Removing the SOQL query for Record Type, JIRA ST-1868.
                /*for(RecordType rc: [SELECT Id, Name FROM RecordType WHERE (Name = 'AF Servicing' OR Name = 'Servicing') AND SobjectType = 'Case']){
                    recordTypeIds.put(rc.Name, rc.Id);
                }*/
            }

            return recordTypeIds;
        }
        set;
    }


    // Reopen case if it has not been closed for more than this number.
    // SLA reopen the case and attach the reply to a Case after 1 day (Value 1 = 24 hours). 
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        return 1;
    }

    //not using this method. Can be deleted?
    global virtual Boolean usePersonAccount() {
        return false;
    }
    
    //not using this method. Can be deleted?
    global virtual String getDefaultAccountId() {
        return null;
    }
    
    //Added by Sai. For Auto case creation. JIRA ST-1645
    /** 
    * Method for automatic creation of cases for Twitter
    * Use "createNewCases" method in ControllerHelper class
    * 
    */
    private void autoCreateCase(SocialPost post){
        Case newCase=null;
        if(post.MessageType =='Direct')
        {
            AFKLM_SCS_ControllerHelper helper = new AFKLM_SCS_ControllerHelper();
            Map<String, SocialPost> postMap = new Map<String, SocialPost>();
            postMap.put(post.Id, post);
            system.debug('**postMap'+postMap);            
            helper.createNewCases(postMap, '', '', false);
            
            
        }
    }
    
    //Added by sai. To delete multiple feed creation on soical post. JIRA ST-1645
    //Start
    /**
    * This method deletes the last case feed item. 
    * Restrict: creation double case feed
    */
     private void deleteLastCaseFeed(SocialPost post){
        List<FeedItem> fi=[SELECT id FROM FeedItem WHERE parentId=:post.Id ORDER BY CreatedDate DESC Limit 1];
        try{
                delete fi[0];
        } catch(Exception e) {
            System.debug('************'+e.getMessage());
        }
    }
    //End

    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, 
                                                        SocialPersona persona,
                                                        Map<String, Object> rawData) {

        Boolean createSocialPersonaWithPostLink = false;    //DvtH14SEP2019 TempAccountFix
        Social.InboundSocialPostResult twitterResult = new Social.InboundSocialPostResult();
        twitterResult.setSuccess(true);

        if (post.Id != null) {
                update post;
            if (persona.id != null) {
                try{
                    update persona;
                } catch (Exception e) {
                    System.debug(e.getMessage());
                }
            }
            return twitterResult;
        }

        findReplyTo(post, rawData);
        

        List<Account> twitterWithPersonAccount = [SELECT Id, sf4twitter__Twitter_Username__pc FROM Account WHERE sf4twitter__Twitter_Username__pc = :persona.Name LIMIT 1];

        if (persona.Id == null) {
            // Logic to check if "Person Account" exist first, else create a Persona with default temporary "Person Account"
            if(!twitterWithPersonAccount.isEmpty()) {
                persona.parentId = twitterWithPersonAccount[0].Id;
                insert persona;

                // Fix for adding the Social Post to an existing personAccount
                post.WhoId = TwitterWithPersonAccount[0].Id;
            } else {
                //createPersona(persona); //DvtH14SEP2019 TempAccountFix: Nopped out!
                createSocialPersonaWithPostLink = true; //DvtH14SEP2019 TempAccountFix: 
            }
        }
        else {
            // Fix for adding the Social Post to an existing personAccount
            if(!twitterWithPersonAccount.isEmpty()) {
                post.WhoId = TwitterWithPersonAccount[0].Id;
            }
            try{
                update persona;
            } catch (Exception e) {
                System.debug(e.getMessage());
            }
        }

        post.PersonaId = persona.Id;
        case parenttCase;
        Boolean deleteFeed = False;
        
        //Added by Sai. For twitter survey prompt message to be converted as Outbound. JIRA ST-840
         //start
        if (post.Provider == 'Twitter' && post.MessageType == 'Direct')
            {
                for(scs_twtSurveys__Social_Feedback_Survey__c sfS : [SELECT Id, scs_twtSurveys__Case__c,CreatedById, scs_twtSurveys__Direct_Message_ID__c FROM scs_twtSurveys__Social_Feedback_Survey__c WHERE scs_twtSurveys__Direct_Message_ID__c = :post.ExternalPostId LIMIT 1]) 
                    {
                        post.ParentId = sfS.scs_twtSurveys__Case__c;
                        post.IsOutbound = true;
                        post.whoid=null;
                        post.personaid=null;
                        post.OwnerId= sfs.CreatedById;
                        post.Status='sent';
                //Added by Aruna -- Update Agent Status as Survey sent JIRA ST-1888.
                        post.SCS_Status__c='Survey Sent';
                        post.PostTags='';
                    }
                }
        if(post.ParentId == null)
        {
            parenttCase = findParentCase(post, persona, rawData);
        }
        //end
        if(post.Content != null && post.Content.length() > 144){
                post.Short_Content__c = post.Content.substring(0,144);
        } else {
                post.Short_Content__c = post.Content;
        }
        
        insert post;
        
        //DvtH14SEP2019 TempAccountFix: --> 
        if(createSocialPersonaWithPostLink) {
            if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) || String.isBlank(persona.Provider)) {
                //In this case we cannot create the persona!    
            } else {
                //create the persona with a link back to the SocialPost
                persona.parentId = post.Id;
                insert persona;
                post.PersonaId = persona.Id;
                
            }
        }
        //Added by Sai. Auto Case Creation and feed deletion. JIRA ST-1645
        //Start
        if(post.ParentId == null && post.MessageType == 'Direct' && post.TopicProfileName.contains('KLM'))
        {
            autoCreateCase(post);
            deleteFeed = True;
        }
        //End
        update post;
        if(deleteFeed)
        {
            deleteLastCaseFeed(post);
        }
        
        
        return twitterResult;   
    }
    
    private Case findParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {

        SocialPost replyToPost = null;
        
        String seg = 'Servicing';
        if(post.TopicProfileName.contains('france') || post.TopicProfileName.contains('France') /*|| post.TopicProfileName.contains('GreatAirline'*/) {
            seg = 'AF Servicing';
        }
        
        if ('Retweet'.equals(post.MessageType)){
            post.parentId = null;
            if(post.PostTags.contains('English') && post.TopicProfileName.contains('KLM'))
            {
                post.SCS_Status__c='Mark As Reviewed Auto';
                post.SCS_MarkAsReviewed__c= true;
                post.Mark_As_Reviewed_Date__c= DateTime.Now();
                post.Ignore_for_SLA__c = true;
            }
            return null;
        }        
        if (persona.ParentId != null && persona.Id !=null) { //DvtH05OCT2015 Wrong Case selection fix
             if (post.ReplyTo != null && (post.isOutbound || post.ReplyTo.PersonaId == persona.Id)) {
                replyToPost = post.ReplyTo;
            } else if (post.MessageType == 'Direct' && String.isNotBlank(post.Recipient)){

                // Find the latest outbound post that the DM is responding to
                List<SocialPost> posts = [SELECT Id, ParentId FROM SocialPost WHERE  OutboundSocialAccount.ProviderUserId = :post.Recipient AND ReplyTo.Persona.Id = :persona.Id ORDER BY CreatedDate DESC LIMIT 1];
    
                if (!posts.isEmpty()) { 
                    replyToPost = posts[0];
                }
    
                // 2014-01-23: DM should also be attach to the same case
                //Modified by sai. To include Twitter_Survey_Stopper_On_Batch_Close__c field to the query.JIRA ST-1303.
                List<Case> cs = [SELECT Id, ClosedDate,Language__c, isClosed, RecordTypeId,Twitter_Survey_Stopper_On_Batch_Close__c FROM Case WHERE Delete_Case__c = false AND AccountId = :persona.ParentId AND RecordTypeId  =:recordTypeIds.get(seg) ORDER BY CreatedDate DESC LIMIT 1];
             
                if (!cs.isEmpty()) {
                  
                    if(cs[0].RecordTypeId != NULL && cs[0].RecordTypeId != recordTypeIds.get(seg)) {

                        return null;
                    }
                    cs[0] = reopenClosedCaseLogic(cs, post);
                    
                    if(cs[0] != null) {
                        return cs[0];
                    }
                }
                
            }
        }
        // TODO: Assumption Can be removed because Twitter doesn't have replyToPost
        if (replyToPost != null) {
            //Modified by sai. To include Twitter_Survey_Stopper_On_Batch_Close__c field to the query.JIRA ST-1303.
            List<Case> cases = [SELECT Id, IsClosed, Status,Language__c, ClosedDate, RecordTypeId,Twitter_Survey_Stopper_On_Batch_Close__c FROM Case WHERE Delete_Case__c = false AND Id = :replyToPost.ParentId AND RecordTypeId IN :recordTypeIds.values()];
           
            if (!cases.isEmpty()) {
                
                if(cases[0].RecordTypeId != NULL && cases[0].RecordTypeId != recordTypeIds.get(seg)) {

                    return null;
                }                
                cases[0] = reopenClosedCaseLogic(cases, post);
                
                if(cases[0] != null) {
                    return cases[0];
                }
            }
        } else if (persona.ParentId != null) { //DvtH05OCT2015 Wrong Case selection fix
            // Fix for additonal cases to be attached when engaging with the customer.
            //Modified by sai. To include Twitter_Survey_Stopper_On_Batch_Close__c field to the query.JIRA ST-1303.
            List<Case> cs = [SELECT Id, ClosedDate,Language__c, RecordTypeId, isClosed,Twitter_Survey_Stopper_On_Batch_Close__c FROM Case WHERE Delete_Case__c = false AND AccountId = :persona.ParentId AND RecordTypeId = :recordTypeIds.get(seg) ORDER BY CreatedDate DESC LIMIT 1];
            if (!cs.isEmpty()) {
                
                if(cs[0].RecordTypeId != NULL && cs[0].RecordTypeId != recordTypeIds.get(seg)) {

                    return null;
                }

                cs[0] = reopenClosedCaseLogic(cs, post);
                
                if(cs[0] != null) {
                    return cs[0];
                }
            }
        }
        return null;
    }

    private Case reopenClosedCaseLogic(List<Case> cases, SocialPost post) {
        Case returnCase = null;
        Boolean caseUpdateflag =false;
        system.debug('the case:::'+cases[0]);
        system.debug('the postTags:::'+post.posttags.toLowerCase());
        //Added by Ata for language issue
        //Modified by sai , For the language update on Unsupported posts.JIRA ST-1498
        if( cases[0] != Null && ((post.posttags.toLowerCase().contains('unsupported')) || post.posttags == '' || post.posttags == Null)){//add condition to filter out unsupported/unpublished 
        
            //pick the latest actioned post to get its language
            list<SocialPost> sPost = [Select id,language__c,PostTags from socialpost where Parentid =: cases[0].id And IsOutbound=False AND SCS_Status__c = 'Actioned' AND (language__c != '' OR language__c != 'Unpublished' OR language__c != 'Unsupported') order by Createddate Desc limit 1]; 
            system.debug('ABCD!@#:'+sPost );
            //editing post's language
            if(!sPost.isEmpty()){
                // To have the track of incoming original posttag.
                post.oldposttag__c= post.posttags;
                post.PostTags = sPost[0].posttags;
                system.debug('1234!@#'+post);
                if(cases[0].Language__c == '' || cases[0].Language__c == Null || cases[0].Language__c.toLowerCase().contains('unsupported')
                    || cases[0].Language__c.toLowerCase() != sPost[0].language__c.toLowerCase()){
                    
                    cases[0].Language__c = sPost[0].language__c;
                    caseUpdateflag = true;
                }
            }
        }//end
        
        if (!cases[0].IsClosed) { 
            post.ParentId = cases[0].Id;
            //added by Ata for language issue
            system.debug('1234!@#$'+caseUpdateflag);
            if(caseUpdateflag){
                update cases[0];
            }//end
            returnCase = cases[0];
        }
        //Modified by Sai. To reopen cases for replies irrespective of Closed Date. JIRA ST-1645.
        if (cases[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase())) {
            reopenCase(cases[0]);
            post.ParentId = cases[0].Id;
            returnCase = cases[0];
        }
        system.debug('ABCD!@#$%'+returnCase);
        return returnCase;
    }

    private void reopenCase(Case parentCase) {
        parentCase.Status = 'Reopened'; 
        parentCase.SCS_Reopened_Date__c = Datetime.now();
        //added by sai.To clear the Twitter_Survey_Stopper_On_Batch_Close__c on case reopen. Jira ST-1303;
        if(parentCase.Twitter_Survey_Stopper_On_Batch_Close__c == 'YES')
        { 
            parentCase.Twitter_Survey_Stopper_On_Batch_Close__c = 'NO'; 
        }
        update parentCase;
    }

    private void findReplyTo(SocialPost post, Map<String, Object> rawData) {
        String replyToId = (String)rawData.get('replyToExternalPostId');

        if (String.isBlank(replyToId)) 
            return;

        List<SocialPost> postList = [SELECT Id, Status, ParentId, PersonaId FROM SocialPost WHERE ExternalPostId = :replyToId LIMIT 1];

        if (!postList.isEmpty()) {
            post.ReplyToId = postList[0].id;
            post.ReplyTo = postList[0];
            //post.ParentId = postList[0].ParentId;
        }
    }

    /*private void createPersona(SocialPersona persona) {
        if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) ||
            String.isBlank(persona.Provider)) 
            return;

        // Select/Create by personAccount.Name "TemporaryPersonAccount"?
        //  this is the "TemporaryPersonAccount" since the creation of new "Person Account" should be done when "Create Case" is selected by agents. 
        List<Account> accountList = [SELECT Id FROM Account WHERE Name = 'TemporaryPersonAccount' LIMIT 1];

        if ( !accountList.isEmpty()) {
            persona.parentId = accountList[0].Id;
        }
        
        insert persona;
    }*/
}