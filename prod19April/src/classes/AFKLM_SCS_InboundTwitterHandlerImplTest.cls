/********************************************************************** 
 Name:  AFKLM_SCS_InboundTwitterHandlerImplTest
 Task:    N/A
 Runs on: AFKLM_SCS_InboundTwitterHandlerImpl
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          19/05/2014      Initial Development
***********************************************************************/
@isTest
private class AFKLM_SCS_InboundTwitterHandlerImplTest {

    static testMethod void getMaxNumberOfDaysClosedToReopenCaseTest(){
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        Integer numberOfDaysClosedToReopenCase = InTwitterHandlImpl.getMaxNumberOfDaysClosedToReopenCase();
        System.assertEquals(numberOfDaysClosedToReopenCase,1);
    }

    static testMethod void usePersonAccountTest(){
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        Boolean persAcc = InTwitterHandlImpl.usePersonAccount();
        System.assertEquals(persAcc, false);
    }
    
    static testMethod void getDefaultAccountIdTest(){
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        String defaultAccountId= InTwitterHandlImpl.getDefaultAccountId();
        System.assertEquals(defaultAccountId, null);
    }
    
    static testMethod void handleInboundSocialPostTest() {
        Account acc= new Account(Firstname='Test', LastName='Account', sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Provider='Twitter', WhoId=acc.id,TopicProfileName='AirFrance');
        SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        
        AFKLM_SCS_InboundTwitterHandlerImpl InTwittterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        InTwittterHandlImpl.handleInboundSocialPost(spost, sp, rawData);
    }
    
     static testMethod void handleInboundSocialPostPersonaInsertedTest() {
        Account acc= new Account(Firstname='Test', LastName='Account', sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Provider='Twitter', WhoId=acc.id,TopicProfileName='AirFrance');
        insert spost;
        SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        insert sp;
        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        
        AFKLM_SCS_InboundTwitterHandlerImpl InTwittterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        InTwittterHandlImpl.handleInboundSocialPost(spost, sp, rawData);
    }
    
    static testMethod void createPersonaTest(){
        Account acc= new Account(Firstname='Test', LastName='Account',sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        Account temporary = new Account(LastName='TemporaryPersonAccount');
        insert temporary;
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Provider='Twitter', WhoId=acc.id,TopicProfileName='AirFrance');
        SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Twitter', ProfileURL='http://www.testingurlfortestuser.com/id=12345678', ExternalId='12345678',parentId=acc.id);
        insert sp;
        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        test.startTest();
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        Social.InboundSocialPostResult TwitterResult = new Social.InboundSocialPostResult();
        TwitterResult = InTwitterHandlImpl.handleInboundSocialPost(spost, sp, rawData);
        test.stopTest();
    }
    
      static testMethod void handleInboundSocialPersonaInsertedTest() {
        Account acc= new Account(Firstname='Test', LastName='Account', sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        insert sp;
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Provider='Twitter', WhoId=acc.id,personaId=sp.id,TopicProfileName='AirFrance');
        insert spost;
        SocialPost post = new SocialPost(name='Comment from: Test Account',PostTags='English', Posted=System.now(), Recipient='663023410425460', Provider='Twitter', WhoId=acc.id, ReplyTo=spost,TopicProfileName='AirFrance');
        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        
        AFKLM_SCS_InboundTwitterHandlerImpl InTwittterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        InTwittterHandlImpl.handleInboundSocialPost(post, sp, rawData);
    }
    
    static testMethod void findParentCaseTest(){
        Account acc= new Account(Firstname='Test', LastName='Account',sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        Case cs = new Case(Status='New', AccountId=acc.id, Recordtypeid='01220000000UYzA');
        insert cs;
        String extPostId='12345678987654321';
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', parentID=cs.id, Provider='Twitter', WhoId=acc.id, ExternalPostId=extPostId,TopicProfileName='KLM',SCS_Status__c = 'Actioned', Language__c = 'English');
        insert spost;
        SocialPost post = new SocialPost(name='Comment from: Test Account',PostTags='English', Posted=System.now(), Recipient='663023410425460', Provider='Twitter', WhoId=acc.id, ReplyTo=spost,TopicProfileName='KLM');
        SocialPersona persona = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        
        Map<String, Object> rawData = new Map<String, Object>();
        rawData.put('replyToExternalPostId', spost.ExternalPostId);
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        InTwitterHandlImpl.handleInboundSocialPost(post, persona, rawData); 
        
        cs.status='Closed - No Response';
        cs.Twitter_Survey_Stopper_On_Batch_Close__c = 'YES';
        update cs;
        SocialPost unsupportedPost = new SocialPost(name='Comment from: Test Account',PostTags='Unsupported', Posted=System.now(), Recipient='663023410425460', Provider='Twitter', WhoId=acc.id, ReplyTo=spost,TopicProfileName='KLM');
        InTwitterHandlImpl.handleInboundSocialPost(unsupportedPost, persona, rawData);
        
        Map<String, Object> rawData1 = new Map<String, Object>();
        rawData1.put('replyToExternalPostId','2543156845235' );
        SocialPersona persona1 = new SocialPersona(name='Test Persona1',RealName='Test Persona1', provider = 'Twitter', ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='9876543210');
        SocialPost unsupportedPost1 = new SocialPost(name='Comment from: Test Account',PostTags='Unsupported', Posted=System.now(), Recipient='663023410425460', Provider='Twitter',TopicProfileName='KLM',MessageType= 'Tweet');
        InTwitterHandlImpl.handleInboundSocialPost(unsupportedPost1, persona1, rawData1);
    }
    
    static testMethod void autocasecreatetest(){
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        String extPostId='5542552515415155';
        Map<String, Object> rawData1 = new Map<String, Object>();
        rawData1.put('replyToExternalPostId','2543156845235' );
        SocialPersona persona1 = new SocialPersona(name='Test Persona1',RealName='Test Persona1', provider = 'Twitter', ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='9876543210');
        SocialPost unsupportedPost1 = new SocialPost(name='Comment from: Test Account',PostTags='Unsupported', Posted=System.now(), Recipient='663023410425460', Provider='Twitter',TopicProfileName='KLM',MessageType= 'Direct');
        InTwitterHandlImpl.handleInboundSocialPost(unsupportedPost1, persona1, rawData1);
        
        SocialPost unsupportedPost2 = new SocialPost(name='Comment from: Test Account',PostTags='Unsupported', Posted=System.now(), Recipient='663023410425460', Provider='Twitter',TopicProfileName='KLM',MessageType= 'Retweet');
        InTwitterHandlImpl.handleInboundSocialPost(unsupportedPost2 , persona1, rawData1);
        }
        
    static testMethod void twitterSurveyMessageTest()
    {
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        Account acc= new Account(Firstname='Test', LastName='Account',sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        
        Case cs = new Case(Status='New', AccountId=acc.id, Recordtypeid='01220000000UYzA');
        insert cs;
        
        scs_twtSurveys__Social_Feedback_Survey__c surveyRecord = new scs_twtSurveys__Social_Feedback_Survey__c();
        surveyRecord.scs_twtSurveys__Feedback_Card_Id__c='123456789';
        surveyRecord.scs_twtSurveys__Feedback_Type__c='NPS';
        surveyRecord.scs_twtSurveys__Case__c=cs.id; 
        surveyRecord.scs_twtSurveys__Direct_Message_ID__c='123456789';
        insert surveyRecord;
        
        Map<String, Object> rawData1 = new Map<String, Object>();
        rawData1.put('replyToExternalPostId','2543156845235' );
        SocialPersona persona1 = new SocialPersona(name='Test Persona1',RealName='Test Persona1', provider = 'Twitter', ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='9876543210');
        SocialPost unsupportedPost1 = new SocialPost(name='Comment from: Test Account',PostTags='Unsupported', Posted=System.now(), Recipient='663023410425460', Provider='Twitter',TopicProfileName='KLM',MessageType= 'Direct',ExternalPostId ='123456789');
        unsupportedPost1.content='testing the content size of the message. it should be greater than the character limit size that has been defined in the code components of the inbound handler class of the Twitter controller class';
        InTwitterHandlImpl.handleInboundSocialPost(unsupportedPost1, persona1, rawData1);
        
        
    }
    
    static testMethod void findParentCaseDirectMessageTest(){
        String ProviderUserId = '663023410425460';
        Account acc= new Account(Firstname='Test', LastName='Account',sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        Case cs = new Case(Status='New', AccountId=acc.id,Recordtypeid='01220000000UYzA');
        insert cs;
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', parentID=cs.id, Provider='Twitter', WhoId=acc.id, Recipient='',ExternalPostId='1396604723809:f3da05e20a1267c585',TopicProfileName='KLM',SCS_Status__c = 'Actioned', Language__c = 'English');
        insert spost;
        SocialPost post = new SocialPost(name='DM from: Test Account',PostTags='English', Posted=System.now(), Recipient='663023410425460', Provider='Twitter', WhoId=acc.id, MessageType='Direct',TopicProfileName='KLM');
        SocialPersona persona = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        
        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        InTwitterHandlImpl.handleInboundSocialPost(post, persona, rawData); 
        
        cs.status='Closed - No Response';
        update cs;
        SocialPost unsupportedPost = new SocialPost(name='DM from: Test Account',PostTags='Unsupported', Posted=System.now(), Recipient='663023410425460', Provider='Twitter', WhoId=acc.id, MessageType='Direct',TopicProfileName='KLM');
        InTwitterHandlImpl.handleInboundSocialPost(unsupportedPost, persona, rawData);
    }
    
    static testMethod void findParentCaseDirectMessage2Test(){
        String ProviderUserId = '663023410425460';
        Account acc= new Account(Firstname='Test', LastName='Account',sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;
        Case cs = new Case(Status='Closed - No Response', AccountId=acc.id);
        insert cs;
        SocialPersona persona = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        insert persona;
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Posted=System.now(),parentID=cs.id, Provider='Twitter', WhoId=acc.id, Recipient=null,ExternalPostId='1396604723809:f3da05e20a1267c585', personaId=persona.id,TopicProfileName='AirFrance');
        insert spost;
        SocialPost post = new SocialPost(name='DM from: Test Account',PostTags='English', Posted=System.now(), Recipient=' ', Provider='Twitter', WhoId=acc.id, ReplyTo=spost, MessageType='Direct',TopicProfileName='AirFrance');
        
        
        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        InTwitterHandlImpl.handleInboundSocialPost(post, persona, rawData); 
    }
    
    static testMethod void createPersonaElseStatementTest(){
        Account acc= new Account(Firstname='Test', LastName='Account');
        insert acc;
        Account temporary = new Account(LastName='TemporaryPersonAccount');
        insert temporary;
        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Posted=System.now(), Handle='KLM', Provider='Twitter', WhoId=acc.id,TopicProfileName='AirFrance');
        SocialPersona sp = new SocialPersona(name='Test Persona',RealName='Test Persona' ,provider = 'Twitter', ProfileURL='http://www.testingurlfortestuser.com/id=12345678', ExternalId='12345678');
        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        test.startTest();
        AFKLM_SCS_InboundTwitterHandlerImpl InTwitterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        InTwitterHandlImpl.handleInboundSocialPost(spost, sp, rawData);
        test.stopTest();
    }

     static testMethod void kloutScoreTest() {

        Account acc;
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new KloutWebServiceMockImpl());

        acc= new Account(Firstname='Test', LastName='Account', sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;            

        SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        insert sp;

        List<Account> act1 = [select id, klout_Score__c from Account where id=:acc.id];

        System.assertEquals(null, act1.get(0).klout_Score__c, 'Klout Score should be null');

        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Provider='Twitter', WhoId=acc.id,TopicProfileName='AirFrance');        
        insert spost;

        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        
        AFKLM_SCS_InboundTwitterHandlerImpl InTwittterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        
        InTwittterHandlImpl.handleInboundSocialPost(spost, sp, rawData);

        Test.stopTest();

        List<Account> act2 = [select id, klout_Score__c from Account where id=:acc.id];
        
        System.assertNotEquals(null, act2.get(0).klout_Score__c, 'Klout Score should not be null');

        

    }

     static testMethod void kloutScoreTest1() {

        Account acc= new Account(Firstname='Test', LastName='Account', sf4twitter__Twitter_Username__pc='Test Persona');
        insert acc;    
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new KloutWebServiceMockImpl());


        SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Twitter', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789');
        insert sp;

        List<Account> act1 = [select id, klout_Score__c from Account where id=:acc.id];

        System.assertEquals(null, act1.get(0).klout_Score__c, 'Klout Score should be null');

        SocialPost spost = new SocialPost(name='Post from: Test Account',PostTags='English', Provider='Twitter', WhoId=acc.id,TopicProfileName='AirFrance');        
        insert spost;

        Map<String, SocialPost> rawData = new Map<String, SocialPost>();
        
        AFKLM_SCS_InboundTwitterHandlerImpl InTwittterHandlImpl = new AFKLM_SCS_InboundTwitterHandlerImpl();
        
        InTwittterHandlImpl.handleInboundSocialPost(spost, sp, rawData);

        Test.stopTest();

        List<Account> act2 = [select id, klout_Score__c from Account where id=:acc.id];
        
        System.assertNotEquals(null, act2.get(0).klout_Score__c, 'Klout Score on account should not be null');

        List<SocialPost> post1 = [select id, klout_Score__c,TopicProfileName,PostTags from SocialPost where id=:spost.id];

        System.assertNotEquals(null, post1.get(0).klout_Score__c, 'Klout Score on post should not be null');
        

    }

}