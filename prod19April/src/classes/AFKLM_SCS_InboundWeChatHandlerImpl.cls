/**********************************************************************
 Name:  AFKLM_SCS_InboundWeChatHandlerImpl
 Task:    N/A
 Runs on: AFKLM_SCS_InboundWeChatHandlerImpl
======================================================
Purpose: 
   Inbound Social Handler for Wechat based on Social Hub rules and data sources. 
   Creates Social Posts and handles business logic with Social Customer Service enablement.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.1     Sai Choudhry        01/11/2016      Stopping Wechat cases from reopening on outbound Messages. ST-649.
	1.2     Sai Choudhry        02/02/2017      Parenid assigning to the Outbound messages. JIRA ST-1682.
***********************************************************************/
global with sharing class AFKLM_SCS_InboundWeChatHandlerImpl {
    private List<SocialPersona> personasToInsert = new List<SocialPersona>();
    private Set<SocialPersona> personasToInsertSet = new Set<SocialPersona>();
    
    private Map<Id, SocialPersona> personasToUpdate = new Map<Id, SocialPersona>();
    private Map<Id,Case> casesMapToUpdate = new Map<Id,Case>();

    private List<SocialPost> postsToInsert = new List<SocialPost>();    
    private List<SocialPost> postsToUpdate = new List<SocialPost>();    
    
    private List<Case> cs = new List<Case>();
    private List<Case> createdCases = new List<Case>();

    private List<String> personaExternalIds = new List<String>();
    private List<String> postExternalIds = new List<String>();
    

    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        // SCH: SLA reopen the case and attach the reply to a case after 24 hours = 1 day
        return 1;
    }
           
    /**
    * Inbound Social Handler for 'Social Post' and 'Social Persona'
    *
    */
    public PageReference handleInboundSocialPost(List<SocialPost> posts, List<SocialPersona> personas) {
        String pid = '';

        // clear all the lists
        personasToUpdate.clear();
        casesMapToUpdate.clear();
        personasToInsert.clear();
        personasToInsertSet.clear();

        for(SocialPost spost : posts){

            postExternalIds.add( spost.ExternalPostId );
        }

        for(SocialPersona spersona : personas){

            if(spersona != null && spersona.ExternalId != null){
                personaExternalIds.add( spersona.ExternalId );
            }
        }

        //Logic: Query last opened case for specific customer...Query only case but put all cases based on posts.ExternalId into one list        
        createdCases = [SELECT Id, ClosedDate, isClosed, chatHash__c FROM Case WHERE Delete_Case__c = false AND CreatedDate IN(THIS_WEEK,LAST_WEEK) AND chatHash__c IN : postExternalIds];
        //System.debug('--+ createdCases: '+createdCases);

        // List of personas
        List<SocialPersona> personaList = [SELECT Id, ParentId, ExternalId FROM SocialPersona WHERE ExternalId IN :personaExternalIds AND Provider = 'WeChat' LIMIT 200];
        //System.debug('--+ personaList: '+personaList);

        //check/create personas and attach them to created accounts. If there is no account, should be attached to TemporaryPersonAccount
        for(SocialPersona persona : personas) {

            if(matchPersona(persona, personaList) == false) {

                createPersona(persona);         
            } 
        }

        //System.debug('--+ personasToUpdate: '+personasToUpdate);
        update personasToUpdate.values();

        System.debug('--+ personasToInsert before deduplication: '+personasToInsert);

        // Deduplicate
        personasToInsertSet.addAll( personasToInsert );
        personasToInsert.clear();
        personasToInsert.addAll( personasToInsertSet );

        System.debug('--+ personasToInsert: '+personasToInsert);
        
        //upsert personasToInsert; //DvtH05OCT2019 TempAccountFix: done later
        for(SocialPost post : posts){          
            //for(SocialPersona persona : personas){  //DvtH05OCT2019 TempAccountFix: use only database list
            for(SocialPersona persona : personasToUpdate.values()){
                //added if because of deployment/test class SeeAllData=true
                if(post != null && persona != null){
                    if(post.Handle == persona.Name){
                        post.PersonaId = persona.Id;
                        pid = persona.parentId;
                        if(pid != null && !pid.substring(0,3).equals('0ST')) {
                            post.WhoId = pid;
                        }
                    }
                }
                cs.clear();
                for(Case tmpCase : createdCases){

                    system.debug('--+ tmpCase.chatHash__c == post.ExternalId: ' + tmpCase.chatHash__c+'=='+post.ExternalPostId);
                    if(tmpCase.chatHash__c == post.ExternalPostId){
                        cs.add(tmpCase);
                    }

                }
                
                //findParentCase(post, persona, cs);                  
                findParentCase(post, persona, cs);

                if(post.Content != null) {

                    if(post.Content.length()> 255){
                        post.Short_Content__c = post.Content.substring(0,255);
                    } else {
                        post.Short_Content__c = post.Content;
                    }
                }
            }

            postsToInsert.add(post);
        }

        //if case was reopened, it needs to be updated
        update casesMapToUpdate.values();

        //insert all social posts from bulk
        upsert postsToInsert;

        //DvtH05OCT2019 TempAccountFix: -->
        //update persona's
        for (SocialPersona persona : personasToInsert) {
          if (persona.parentId==null) { //If it is filled in then there is no need to overwrite
            for (SocialPost post : posts) {
                  if (post != null) {
                      if (post.Handle == persona.Name) {
                        persona.parentId = post.Id;
                      }
                  }
            }
          }
        }
        
        insert personasToInsert;
    postsToUpdate.clear();
    if (!personasToInsert.isEmpty()) {
          for(SocialPersona persona : personasToInsert){ //Use only to insert list to update posts
            if (persona != null) {
              for(SocialPost post : posts){
                    if(post != null){
                        if(post.Handle == persona.Name){
                            post.PersonaId = persona.Id;
                            //Should only be Persona's that do not yet have an Account so do not fill in the WhoId
                            //if(persona.parentId != null && persona.parentId != post.Id ){
                            //    post.WhoId = persona.parentId;
                            //}
                            postsToUpdate.add(post);
                        }
                    }
                }
            }
          }
          if (!postsToUpdate.isEmpty()) {
            update postsToUpdate;
          }        
    }    
    //<--
        return null;
    }

    /**
     * Match persona - check if persona already exists
     *
     */
    private Boolean matchPersona(SocialPersona persona, List<SocialPersona> personas) {

        for(SocialPersona socPersonaFromList : personas){

            if(persona.ExternalId == socPersonaFromList.ExternalId){
                
                persona.Id = socPersonaFromList.Id;
                persona.ParentId = socPersonaFromList.ParentId;

                if(personasToUpdate.size() > 0) {

                    if( personasToUpdate.containsKey(persona.Id) == false)
                        personasToUpdate.put(persona.Id, persona);
                } else {

                    personasToUpdate.put(persona.Id, persona);
                }
                //this.personasToUpdateTmp.add( persona );

                return true;
            }
        }

        return false;
    }

    /** 
    * Method for finding parent cases and attaching related messages to the same case
    * 
    */
    private Case findParentCase(SocialPost post, SocialPersona persona, List<Case> cs) {
        
        if (!cs.isEmpty()) {
            cs[0] = reopenClosedCaseLogic(cs, post);
            
            system.debug('--+ cs[0]: '+cs[0]);
            if(cs[0] != null) {
                return cs[0];
            }
        }
        System.debug('--+ findParentCase {Case}: '+cs);
        return null;
    } 
    
    private Case reopenClosedCaseLogic(List<Case> cases, SocialPost post) {
        
        Case returnCase = null;
        if (!cases[0].IsClosed) { 
            post.ParentId = cases[0].Id;
            returnCase = cases[0];
        }
        //added by sai.Stopping Wechat cases from reopening on outbound Messages. ST-649.
        //modified by sai.Parenid assigning to the Outbound messages. JIRA ST-1682.
        if (cases[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase()) ) {
            if(post.IsOutbound == False)
            {
                reopenCase(cases[0]);
            }
            post.ParentId = cases[0].Id;
            returnCase = cases[0];
        }
        
        return returnCase;
    }
    
    /** 
    * Method for reopening case
    * IF case status is closed and user replies on social post within 24 hours, case status should be set to "Reopened"
    *
    */
    private void reopenCase(Case parentCase) {
        parentCase.Status = 'Reopened'; 
        parentCase.SCS_Reopened_Date__c = Datetime.now();
        parentCase.Sentiment_at_close_of_case__c = '';
        //update parentCase;
        if(!casesMapToUpdate.containsKey(parentCase.id)){
            casesMapToUpdate.put(parentCase.id,parentCase);
        }
    }

            
    /** 
    * Method for creating SocialPersona
    * SocialPersona is created automatically with Parent "TemporaryPersonAccount"
    * 
    */  
    private void createPersona(SocialPersona persona) {
            if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) ||
                String.isBlank(persona.Provider)) 
                return;

            //TemporaryPersonAccount is set in the trigger for creation of Social Post and Social Persona
            //maybe move it from trigger to this method??? 

            personasToInsert.add(persona);
    }
}