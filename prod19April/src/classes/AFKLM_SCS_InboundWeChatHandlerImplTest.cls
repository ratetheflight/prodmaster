@isTest
private class AFKLM_SCS_InboundWeChatHandlerImplTest {
	
	@isTest static void handleInboundSocialPostTest() {
		
		AFKLM_SCS_InboundWeChatHandlerImpl handlerTest = new AFKLM_SCS_InboundWeChatHandlerImpl();

		List<SocialPost> posts = new List<SocialPost>();
		
		List<SocialPersona> personas = new List<SocialPersona>();
		Case cs;
		
		List<Case> cases = new List<Case>();

		Account temporary = new Account(LastName = 'TemporaryPersonAccount');
		insert temporary;

		for(Integer x=0;x<=10;x++){
			
				 	cs = new Case(
					Status = 'New',
					Subject = 'New case '+x,
					//ContactId = temporary.id,
					AccountId = temporary.id,
					chatHash__c = '32778084357823458321481420897347890067_5039452hghjghj'+x
					);
				cases.add(cs);
		}

		insert cases;

		Integer objectCount = 20;

		for(Integer i=0;i<=objectCount;i++){

			SocialPersona persona = new SocialPersona(
					Name = 'Testing Persona '+i,
					ParentId = temporary.id,
					Provider = 'WeChat',
					ExternalPictureURL = 'picUrl',
					RealName = 'Testing Persona '+i,
					ExternalId = '123456787654'+i			
				);

			personas.add(persona);
		}

		insert  personas;

		Integer j=0;

		for(SocialPersona p : personas){


			//handlerTest.createPersona(p);

			SocialPost sp = new SocialPost(
					Name = 'Message from: '+p.Name,
					Content = 'This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social'+j,
					Handle = p.RealName,
					WhoId = p.ParentId,
					SCS_Status__c = 'New',
					Posted = System.now(),
					ExternalPostId = '32778084357823458321481420897347890067_5039452hghjghj'+j,
					IsOutbound = false,
					Provider = 'WeChat', 
					Company__c = 'KLM'
					);

			posts.add(sp);
			j++;

		}
		insert posts;
		
			handlerTest.handleInboundSocialPost(posts, personas);
				
	}

	@isTest static void handleInboundSocialPostTest2() {
		
		AFKLM_SCS_InboundWeChatHandlerImpl handlerTest = new AFKLM_SCS_InboundWeChatHandlerImpl();

		List<SocialPost> secondPosts = new List<SocialPost>();
		List<SocialPersona> personas = new List<SocialPersona>();
		List<SocialPersona> notInsertedPersonas = new List<SocialPersona>();

		Account temporary = new Account(LastName = 'TemporaryPersonAccount');
		insert temporary;

		
		Integer objectCount = 100;

		for(Integer i=0;i<=objectCount;i++){

			SocialPersona persona = new SocialPersona(
					Name = 'Testing Persona '+i,
					ParentId = temporary.id,
					Provider = 'WeChat',
					ExternalPictureURL = 'picUrl',
					RealName = 'Testing Persona '+i,
					ExternalId = '123456787654'+i			
				);

			personas.add(persona);
		}

		//insert  personas;

		Integer j=0;

		for(SocialPersona p : personas){

			j++;

			SocialPost spost = new SocialPost(
					Name = 'Message from: '+p.Name+' Copy',
					Content = 'This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social'+j,
					Handle = p.Name+' Copy',
					WhoId = p.ParentId,
					SCS_Status__c = 'New',
					Posted = System.now(),
					ExternalPostId = '327780843578234kg954j897347890067_5039452hghjghj'+j,
					IsOutbound = false,
					Provider = 'WeChat', 
					Company__c = 'KLM'
					);

			p.Name = p.Name+' Copy';
			p.ExternalId = p.ExternalId+'1234';
			notInsertedPersonas.add(p);
			secondPosts.add(spost);
		}
		
		insert secondPosts;

		handlerTest.handleInboundSocialPost(secondPosts, notInsertedPersonas);
					
	}
	
	static testMethod void getMaxNumberOfDaysClosedToReopenCaseTest(){
		AFKLM_SCS_InboundWeChatHandlerImpl handlerTest = new AFKLM_SCS_InboundWeChatHandlerImpl();
		Integer numberOfDaysClosedToReopenCase = handlerTest.getMaxNumberOfDaysClosedToReopenCase();
		System.assertEquals(numberOfDaysClosedToReopenCase,1);
	}

	@isTest(SeeAllData=true) 
	 static void triggerTestMethod() {
		Integer i = 50;
		List<Nexmo_Post__c> nexPost = new List<Nexmo_Post__c>();

		for(Integer j=0;j<=i;j++){
			Nexmo_Post__c np = new Nexmo_Post__c(
				From_User_Id__c ='1234567'+j,
				Chat_Hash__c='1234567chatHash'+j,
				Message_Id__c='1234567messsageId'+j,
				Send_Time__c=System.now(),
				Source__c='Inbound',
				Text__c='This is testing content on NexmoPost object '+j,
				To_Id__c='1234567toId',
				Type__c='text',
				User_Real_Name__c='TestingRealName '+j
				);

			nexPost.add(np);
		}

		
	test.startTest();
		insert nexPost;
	test.stopTest();
		
	}

	/*@isTest(SeeAllData=true) 
	 static void triggerTestOutboundMethod() {
		Integer i = 50;
		List<Nexmo_Post__c> nexPost = new List<Nexmo_Post__c>();

		for(Integer j=0;j<=i;j++){
			Nexmo_Post__c np = new Nexmo_Post__c(
				From_User_Id__c ='1234567'+j,
				Chat_Hash__c='1234567chatHash'+j,
				Message_Id__c='1234567messsageId'+j,
				Send_Time__c=System.now(),
				Source__c='Outbound',
				Text__c='This is testing content on NexmoPost object '+j,
				To_Id__c='1234567toId',
				Type__c='text',
				User_Real_Name__c='TestingRealName '+j
				);

			nexPost.add(np);
		}

		insert nexPost;
		
	}*/

}