@isTest
private class AFKLM_SCS_InboundWhatsAppHandlerTest {
	
	@isTest static void handleInboundSocialPostTest() {
		
		AFKLM_SCS_InboundWhatsappHandlerImpl handlerTest = new AFKLM_SCS_InboundWhatsappHandlerImpl();

		List<SocialPost> posts = new List<SocialPost>();
		
		List<SocialPersona> personas = new List<SocialPersona>();
		Case cs;
		
		List<Case> cases = new List<Case>();

		Account temporary = new Account(LastName = 'TemporaryPersonAccount');
		insert temporary;

		for(Integer x=0;x<=10;x++){
			
				 	cs = new Case(
					Status = 'New',
					Subject = 'New case '+x,
					//ContactId = temporary.id,
					AccountId = temporary.id
					);
				cases.add(cs);
		}

		insert cases;

		Integer objectCount = 20;

		for(Integer i=0;i<=objectCount;i++){

			SocialPersona persona = new SocialPersona(
					Name = 'Testing Persona '+i,
					ParentId = temporary.id,
					Provider = 'Whatsapp',
					ExternalPictureURL = 'picUrl',
					RealName = 'Testing Persona '+i,
					ExternalId = '123456787654'+i			
				);

			personas.add(persona);
		}

		insert  personas;

		Integer j=0;

		for(SocialPersona p : personas){


			//handlerTest.createPersona(p);

			SocialPost sp = new SocialPost(
					Name = 'Message from: '+p.Name,
					Content = 'This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social posts no.This is content for testing social'+j,
					Handle = p.RealName,
					WhoId = p.ParentId,
					SCS_Status__c = 'New',
					Posted = System.now(),
					ExternalPostId = '32778084357823458321481420897347890067_5039452hghjghj'+j,
					IsOutbound = false,
					Provider = 'Whatsapp', 
					Company__c = 'KLM'
					);

			posts.add(sp);
			j++;

		}
		insert posts;
		
		for(Integer i=0;i<posts.size();i++){
			handlerTest.handleInboundSocialPost(posts[i], personas[i]);
		}
				
	}

	static testMethod void testUsePersonAccount(){
		AFKLM_SCS_InboundWhatsappHandlerImpl handlerTest = new AFKLM_SCS_InboundWhatsappHandlerImpl();
		Boolean usePersonAccount = false;
		System.assertEquals(usePersonAccount,handlerTest.usePersonAccount());
	}

	static testMethod void testgetDefaultAccountId(){
		AFKLM_SCS_InboundWhatsappHandlerImpl handlerTest = new AFKLM_SCS_InboundWhatsappHandlerImpl();
		
		System.assertEquals(null,handlerTest.getDefaultAccountId());
	}

	static testMethod void getMaxNumberOfDaysClosedToReopenCaseTest(){
		AFKLM_SCS_InboundWhatsappHandlerImpl handlerTest = new AFKLM_SCS_InboundWhatsappHandlerImpl();
		Integer numberOfDaysClosedToReopenCase = handlerTest.getMaxNumberOfDaysClosedToReopenCase();
		System.assertEquals(numberOfDaysClosedToReopenCase,1);
	}

	static testMethod void testFindParentCaseAndFindReplyTo(){
		AFKLM_SCS_InboundWhatsappHandlerImpl handlerTest = new AFKLM_SCS_InboundWhatsappHandlerImpl();

		Account testingAccount = new Account(
			LastName = 'Testing User',
			Whatsapp_user_id__c = '532672664'
			);
		insert testingAccount;

		SocialPersona createdPersona = new SocialPersona(
			Name = 'Testing User',
			ExternalId = '532672664',
			Provider = 'Whatsapp',
			ParentId = testingAccount.id,
			ExternalPictureURL = 'http://www.testingpictureurl.com'
			);

		insert createdPersona;

		String recTypeId = [SELECT Id FROM RecordType WHERE Name = 'Servicing' AND SobjectType = 'Case' LIMIT 1].id;

		Case openCase = new Case(
			Status = 'New',
			AccountId = testingAccount.id,
			RecordTypeId = recTypeId
			);
		insert openCase;

		Case closedCase = new Case(
			Status = 'Closed',
			AccountId = testingAccount.id,
			RecordTypeId = recTypeId,
			Sentiment_at_start_of_case__c = 'Negative',
			Sentiment_at_close_of_case__c = 'Negative',
			Case_Phase__c = 'Prepare',
			Case_Topic__c = 'Options (PFT)',
			Case_Detail__c = 'Paid Meal'
			);
		insert closedCase;

		SocialPost replyToPost = new SocialPost(
			IsOutbound = false,
			Provider = 'Whatsapp',
			Handle = 'Testing User',
			Content = 'Content for testing',
			Name = 'Private Message from: Testing User',
			MessageType = 'Private',
			Posted = System.now(),
			PersonaId = createdPersona.id,
			ParentId = openCase.id
			);
		insert replyToPost;

		SocialPost socPost1 = new SocialPost(
			Handle = 'Testing User',
			IsOutbound = false,
			Provider = 'Whatsapp',
			Content = 'Content for testing',
			Name = 'Private Message from: Testing User',
			MessageType = 'Private',
			Posted = System.now()
						
			);

		SocialPost socPost2 = new SocialPost(
			Handle = 'Testing User',
			IsOutbound = false,
			Provider = 'Whatsapp',
			Content = 'Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. Content for testing. ',
			Name = 'Private Message from: Testing User',
			MessageType = 'Private',
			Posted = System.now()
			);

		

		SocialPersona matchPersonaTest = new SocialPersona (
			Name = 'Testing User',
			ExternalId = '532672664',
			Provider = 'Whatsapp',
			ExternalPictureURL = 'http://www.testingpictureurl.com'
			);

		SocialPersona createPersonaTest = new SocialPersona (
			);

		

		for(Integer i=1;i<=2;i++){
			if(i==1){
				replyToPost.ParentId = openCase.id;
				update replyToPost;
				handlerTest.handleInboundSocialPost(socPost1, matchPersonaTest);
			} else {
				replyToPost.ParentId = closedCase.id;
				update replyToPost;
				handlerTest.handleInboundSocialPost(socPost2, createdPersona);
			}
		}
	}

	@isTest(SeeAllData=true) 
	 static void triggerTestMethod() {
		Integer i = 50;
		List<WhatsappPost__c> waPost = new List<WhatsappPost__c>();

		for(Integer j=0;j<=i;j++){
			WhatsappPost__c wap = new WhatsappPost__c(
				From__c ='42195812548'+j+'@s.whatsapp.net',
				MessageId__c='1234567messsageId'+j,
				Posted__c=System.now(),
				Content__c='This is testing content on WhatsAppPost object '+j,
				To__c='31613020560@s.whatsapp.net',
				Type__c='text',
				Full_Name__c='TestingRealName '+j
				);

			waPost.add(wap);
		}

		for(Integer x=0;x<=i;x++){
			WhatsappPost__c wap = new WhatsappPost__c(
				From__c ='42195812548'+x+'@s.whatsapp.net',
				MessageId__c='1234567messsageId'+x,
				Posted__c=System.now(),
				Caption__c='This is testing content on WhatsAppPost object '+x,
				To__c='31613020560@s.whatsapp.net',
				Type__c='image',
				URL__c = 'http://www.picturetest.testcom',
				Full_Name__c='TestingRealName '+x
				);

			waPost.add(wap);
		}

		
	test.startTest();
		insert waPost;
	test.stopTest();
		
	}

	//private void getCaseRecordType(){
	//	String recordType = [SELECT Id FROM RecordType WHERE Name = 'Servicing' AND SobjectType = 'Case' LIMIT 1].id;

	//	//return recordType;
	//}

	/*@isTest(SeeAllData=true) 
	 static void triggerTestOutboundMethod() {
		Integer i = 50;
		List<Nexmo_Post__c> nexPost = new List<Nexmo_Post__c>();

		for(Integer j=0;j<=i;j++){
			Nexmo_Post__c np = new Nexmo_Post__c(
				From_User_Id__c ='1234567'+j,
				Chat_Hash__c='1234567chatHash'+j,
				Message_Id__c='1234567messsageId'+j,
				Send_Time__c=System.now(),
				Source__c='Outbound',
				Text__c='This is testing content on NexmoPost object '+j,
				To_Id__c='1234567toId',
				Type__c='text',
				User_Real_Name__c='TestingRealName '+j
				);

			nexPost.add(np);
		}

		insert nexPost;
		
	}*/
	
}