/**********************************************************************
 Name:  AFKLM_SCS_InboundWhatsappHandlerImpl 
 Task:    N/A
 Runs on: AFKLM_SCS_InboundWhatsappHandlerImpl 
======================================================
Purpose: 
   Inbound Social Handler for Whatsapp.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868.
***********************************************************************/
global without sharing class AFKLM_SCS_InboundWhatsappHandlerImpl {

    // Reopen case if it has not been closed for more than this number
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        // SCH: SLA reopen the case and attach the reply to a case after 24 hours = 1 day
        return 1;
    }
       
    /**
    * This method deletes the last case feed item. 
    * Restrict: creation double case feed
    */
    /*private void deleteLastCaseFeed(SocialPost post){
        List<FeedItem> fi=[SELECT id FROM FeedItem WHERE parentId=:post.Id ORDER BY CreatedDate DESC LIMIT 1];
        try{
            delete fi[0];
        } catch(Exception e) {
            System.debug('************'+e.getMessage());
        }
    }*/
    
    global virtual Boolean usePersonAccount() {
        return false;
    }

    global virtual String getDefaultAccountId() {
        return null;
    }
    
    private void matchPersona(SocialPersona persona) {
        if (persona != null && String.isNotBlank(persona.ExternalId)) {
            List<SocialPersona> personaList = [SELECT Id, ParentId, ExternalPictureURL FROM SocialPersona WHERE Provider = :persona.Provider AND ExternalId = :persona.ExternalId LIMIT 1];

            if (personaList.size()>0 && personaList != null) {
                persona.Id = personaList[0].Id;
                persona.ParentId = personaList[0].ParentId;
                persona.ExternalPictureURL = personaList[0].ExternalPictureURL;
                
                update persona;
            }
        }
    }
       
    /**
    * Inbound Social Handler for 'Social Post' and 'Social Persona' inserted by Radian6 / Social Hub rules and data sources.
    *
    */
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, 
                                                        SocialPersona persona) {
        Boolean createSocialPersonaWithPostLink = false;    //DvtH14SEP2019 TempAccountFix
                
        

        if (post.Id != null) {
                update post;
            if (persona.id != null) {
                try{
                    update persona;
                } catch (Exception e) {
                    System.debug(e.getMessage());
                }
            }
            return null;
        }
        
        findReplyTo(post);

        
        List<Account> whatsAppWithPersonAccount = [SELECT Id FROM Account WHERE WhatsApp_User_Id__c=:persona.ExternalId ORDER BY CreatedDate LIMIT 1];
        // Case parentCase = null;

        matchPersona(persona);

        System.debug('^^^persona '+persona);

        if (persona.Id == null) {
            // Logic to check if "Person Account" exist first, else create a Persona with default temporary "Person Account"
            if(!whatsAppWithPersonAccount.isEmpty()) {
                persona.parentId = whatsAppWithPersonAccount[0].Id;
                insert persona;

                // Fix for adding the Social Post to an existing personAccount
                post.WhoId = whatsAppWithPersonAccount[0].Id;
            } else {
                //createPersona(persona); //DvtH14SEP2019 TempAccountFix: Nopped out!
                createSocialPersonaWithPostLink = true; //DvtH14SEP2019 TempAccountFix: 
            }
        }
        else {
            // Fix for adding the Social Post to an existing personAccount
            if(!whatsAppWithPersonAccount.isEmpty()) {
                post.WhoId = whatsAppWithPersonAccount[0].Id;
            }
            try{
                update persona;
            } catch (Exception e) {
                System.debug(e.getMessage());
            }
        }
        
        post.PersonaId = persona.Id;
        
        findParentCase(post, persona);
       
        
        
        if(post.Content != null && post.Content.length()> 255){
            if(post.Content.length()>5000){
                post.Content = post.Content.substring(0,5000);
            }
            post.Short_Content__c = post.Content.substring(0,255);
        } else {
            post.Short_Content__c = post.Content;
        }
        
        

        insert post;
        //DvtH14SEP2019 TempAccountFix: --> 
        if(createSocialPersonaWithPostLink) {
            //create the persona with a link back to the SocialPost
            persona.parentId = post.Id;
            insert persona;
            post.PersonaId = persona.Id;
            update post;
        }
        //<---
                
        return null;  
    }

    /** 
    * Method for finding parent cases and attaching related comments and replies to the same case
    * 
    */
    private Case findParentCase(SocialPost post, SocialPersona persona) {
        SocialPost replyToPost = null;
        //Modified by Sai.  Removing the SOQL query for Record Type, JIRA ST-1868.
        //Start
        List<ID> recordTypeIds = new List<ID>();
        ID klmRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
        recordTypeIds.add(klmRtId);
        ID afRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('AF Servicing').getRecordTypeId();
        recordTypeIds.add(afRtId);
        //End
        
            replyToPost = post.ReplyTo;
            
            if(replyToPost != null) {
                if(replyToPost.ParentId != null) {
                    List<Case> cs = [SELECT Id, Status, isClosed, ClosedDate FROM Case WHERE Id =: replyToPost.ParentId AND RecordTypeId IN: recordTypeIds LIMIT 1];
                    
                    if (cs!=null && !cs.isEmpty()) {
                        if(!cs[0].isClosed){
                            post.ParentId = replyToPost.ParentId;
                            
                            return cs[0];
                        }
                    
                        if((cs[0].isClosed || 'Closed - No Response'.equals(cs[0].Status)) && cs[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase())) {
                                reopenCase(cs[0]);
                                post.ParentId = cs[0].Id;
                                return cs[0];
                        }
                    }
                }
            }
       
        return null;
    } 
    
    /** 
    * Method for reopening case
    * IF case status is closed and user replies on social post within 24 hours, case status should be set to "Reopened"
    *
    */
    private void reopenCase(Case parentCase) {
        parentCase.Status = 'Reopened'; 
        parentCase.SCS_Reopened_Date__c = Datetime.now();
        parentCase.Sentiment_at_close_of_case__c = '';
        update parentCase;
    }

    /** 
    * findReplyTo
    * 
    */ 
    private void findReplyTo(SocialPost post) {
        
        List<SocialPost> postList = [SELECT Id, Status, ParentId, IsOutbound, PersonaId, ExternalPostId, Is_Campaign__c FROM SocialPost WHERE Handle =: post.Handle AND Provider = 'Whatsapp' AND IsOutbound = false ORDER BY Posted DESC LIMIT 1];
        if (!postList.isEmpty()) {
            post.ReplyToId = postList[0].id;
            post.ReplyTo = postList[0];
           
        }
    }
        
    /** 
    * Method for creating SocialPersona
    * SocialPersona is created automatically with Parent "TemporaryPersonAccount"
    * 
    */  
    /*private void createPersona(SocialPersona persona) {
        if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) ||
            String.isBlank(persona.Provider)) 
            return;

        // Select/Create by personAccount.Name "TemporaryPersonAccount"?
        //  this is the "TemporaryPersonAccount" since the creation of new "Person Account" should be done when "Create Case" is selected by agents. 
        List<Account> accountList = [SELECT Id FROM Account WHERE Name = 'TemporaryPersonAccount' LIMIT 1];
        
        if ( !accountList.isEmpty()) {
            persona.parentId = accountList[0].Id;
        }        
        insert persona;
    }*/

}