/**********************************************************************
 Name:  AFKLM_SCS_JSONParserUtil
 Task:    N/A
 Runs on: AFKLM_SCS_RestResponse
======================================================
Purpose: 
   Generic Customer API JSON ParserUtil handling
======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		Stevano Cheung		02/06/2014		INITIAL DEVELOPMENT
***********************************************************************/
/*
Copyright (c) 2012 Twilio, Inc.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
public with sharing class AFKLM_SCS_JSONParserUtil {
	/**
	 * Helper method to convert JSON to a canonical object map.
	 * 
	 * @return a normalized Map of objects. Repeated elements are List values,
	 *         sub-objects are Map values. All other types are String values.
	 */
	public static Map<String,Object> jsonToMap(String jsonContent) {
		return jsonToMap(JSON.createParser(jsonContent));
	}
	
	/* public static List<Object> jsonToList(String jsonContent) {
		return jsonToList(JSON.createParser(jsonContent));
	}*/
	
	/**
	 * Helper method to convert JSON to a canonical object map.
	 * 
	 * @return a normalized Map of objects. Repeated elements are List values,
	 *         sub-objects are Map values. All other types are String values.
	 */
	private static Map<String,Object> jsonToMap(JSONParser parser) {
		Map<String,Object> mapped = new Map<String,Object>();
		
		System.debug('*** BEGIN jsonToMap(JSONParser) : currentToken='+parser.getCurrentToken()+', currentName='+parser.getCurrentName()+', text='+parser.getText());
		
		if (parser.getCurrentToken()==null) {
			parser.nextToken(); // Advance to the start object marker.
		}
		if (parser.getCurrentToken() != JSONToken.START_OBJECT) {
			throw new AFKLM_SCS_JSONParseException('A JSON Object must start with \'{\'');
		}
		System.debug('*** PREWHILE jsonToMap(JSONParser) : currentToken='+parser.getCurrentToken()+', currentName='+parser.getCurrentName()+', text='+parser.getText());
		
		while (parser.nextToken() != null) {
			JSONToken curr = parser.getCurrentToken();
			System.debug('*** WHILE jsonToMap(JSONParser) : currentToken='+parser.getCurrentToken()+', currentName='+parser.getCurrentName()+', text='+parser.getText());
			
			if (curr == JSONToken.END_OBJECT) {
				// reached end of object
				break;
			} else if (curr == JSONToken.FIELD_NAME) {
				System.debug('*** FIELD_NAME jsonToMap(JSONParser) : currentToken='+parser.getCurrentToken()+', currentName='+parser.getCurrentName()+', text='+parser.getText());
					
				String fieldName = parser.getText();
				curr = parser.nextToken();
				
				if (curr == JSONToken.START_OBJECT) {
					// value is a JSON object
					System.debug('*** START_OBJECT jsonToMap(JSONParser) : currentToken='+parser.getCurrentToken()+', currentName='+parser.getCurrentName()+', text='+parser.getText());
					mapped.put(fieldName, jsonToMap(parser));
				} else if (curr == JSONToken.START_ARRAY) {
					// value is a JSON array
					mapped.put(fieldName, jsonToList(parser));
				}else if (curr == JSONToken.VALUE_NULL) {
					mapped.put(fieldName, null);
				} else {
					// value is a JSON primitive
					mapped.put(fieldName, parser.getText());
				}
			} else {
				throw new AFKLM_SCS_JSONParseException('Unexpected JSON value: '+parser.getText());
			}
		}
		System.debug('*** RETURN: '+mapped);
		
		return mapped;
	}

	private static List<Object> jsonToList(JSONParser parser) {
		List<Object> jsonArray = new List<Object>();
		
		if (parser.getCurrentToken()==null) {
			parser.nextToken(); // Advance to the start object marker.
		}

		if (parser.getCurrentToken() != JSONToken.START_ARRAY) {
			throw new AFKLM_SCS_JSONParseException('A JSON Array must start with \'[\'');
		}
		
		while (parser.nextValue() != null) {
        	JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.END_ARRAY) {
            	break;
            } else if (curr == JSONToken.START_ARRAY) {
                jsonArray.add(jsonToList(parser));
            } else if (curr == JSONToken.START_OBJECT){
                jsonArray.add(jsonToMap(parser));
            } else {
            	jsonArray.add(parser.getText());
            }
        }
        return jsonArray;
	}	

    private class AFKLM_SCS_JSONParseException extends Exception {}
}