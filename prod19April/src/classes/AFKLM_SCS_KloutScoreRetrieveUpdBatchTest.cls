/**********************************************************************
 Name:  AFKLM_SCS_KloutScoreRetrieveUpdBatchTest
 Task:    N/A
 Runs on: Account
====================================================== 
Purpose: 
    To Test Batch to retrieve Klout Score for all 
    Twitter accounts in SFDC
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
1.0         Manuel Conde        02/05/2016      Initial Development    
***********************************************************************/
@isTest
private class AFKLM_SCS_KloutScoreRetrieveUpdBatchTest
{
    @isTest
    static void itShould()
    {
        integer totalRecs = 50;
        List<Account> totalActs = new List<Account>();  
        integer i=0;
        while(i<totalRecs){

            Account acc6=new Account();
            acc6.LastName='AFKLM_SCS_KloutScoreRetrieveUpdBatchTest Account '+i;
            acc6.flying_blue_number__c='6789'+i;
            acc6.Website=acc6.LastName+'@abc.com';
            acc6.sf4twitter__Twitter_User_Id__pc=acc6.LastName;
            acc6.klout_score__c=40+i;
            totalActs.add(acc6);
            i++;
        }
        insert totalActs;

        Test.startTest(); 
        totalActs[0].klout_score__c=44;
        update totalActs[0];
        totalActs[1].klout_score__c=56;
        update totalActs[1];
        totalActs[2].klout_score__c=68;
        update totalActs[2];
        Test.setMock(HttpCalloutMock.class, new KloutWebServiceMockImpl());
        AFKLM_SCS_KloutScoreRetrieveUpdateBatch c = new AFKLM_SCS_KloutScoreRetrieveUpdateBatch ();
        Database.executeBatch(c,50);
        AFKLM_SCS_UpdateinfluencerscorenewBatch u=new AFKLM_SCS_UpdateinfluencerscorenewBatch();
        Database.executeBatch(u,50);
        Test.stopTest();

        List<Account> listAct = [select id,klout_score__c from account where name like 'AFKLM_SCS_KloutScoreRetrieveUpdBatchTest Account%' and klout_score__c <> null];

        //System.assertEquals(listAct.size(),50,'Account list is not correct');
    }
}