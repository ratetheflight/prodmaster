/**********************************************************************
 Name:  AFKLM_SCS_KloutScoreRetrieveUpdateBatch
 Task:    N/A
 Runs on: Account
====================================================== 
Purpose: 
    To retrieve Klout Score for all Twitter accounts in SFDC
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
1.0         Manuel Conde        25/04/2016      Initial Development
1.1          Ata                13/06/2016      Added where class in start method to bypass klout update on account
1.2          Ata                28/06/2016      Added logic to populate KloutId on account as well as limiting klout
                                                Id call-out if it is already populated for a personAccount
                                                
***********************************************************************/
global class AFKLM_SCS_KloutScoreRetrieveUpdateBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{
    
    public static Integer DAYS_INTERVAL_TIME_KLOUT = 5;

    String query;
    
    global AFKLM_SCS_KloutScoreRetrieveUpdateBatch() {
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        //Initialize
        string KSlastUpdateTime = System.Now().addDays(-DAYS_INTERVAL_TIME_KLOUT).format('yyy-MM-dd\'T\'HH:mm:ss\'Z\'');
     // modified by Ata new where clause added (Bypass_Klout_Update__c = false)
     if(!Test.isRunningTest()){
       query = 'select id,name,KloutId__c,Klout_score__c,sf4twitter__Twitter_Username__pc, sf4twitter__Twitter_User_Id__pc,Influencer__c from Account where (Klout_Score_Last_Update_Time__c = null OR Klout_Score_Last_Update_Time__c < ' + KSlastUpdateTime +
                ') and (sf4twitter__Twitter_User_Id__pc <> null OR sf4twitter__Twitter_Username__pc <> null) and Bypass_Klout_Update__c = false';
   }else{
     query = 'select id,name,Klout_score__c,KloutId__c,sf4twitter__Twitter_Username__pc, sf4twitter__Twitter_User_Id__pc,Influencer__c from Account where (Klout_Score_Last_Update_Time__c = null OR Klout_Score_Last_Update_Time__c < ' + KSlastUpdateTime +
          ') and Bypass_Klout_Update__c = false and (sf4twitter__Twitter_User_Id__pc <> null OR sf4twitter__Twitter_Username__pc <> null) and createdDate = TODAY limit 150';

   }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Account> accToUpdate = new List<Account>();
        for(sObject a: scope){
            Account b = (Account)a;
            System.debug('Account: ' + b.name + ' Score: ' + b.Klout_score__c);
            kloutDetails kloutInfo = getKloutScore(b);
            //If condition added by Ata based on klout object
            if(b.Klout_score__c != kloutInfo.kloutScore){
                Account c = new Account(id=b.id);   
                c.Klout_score__c = kloutInfo.kloutScore;
                c.Klout_Score_Last_Update_Time__c =  System.Now();
                c.KloutId__c = kloutInfo.kloutId;
                
                if(kloutInfo.kloutScore >= 60 && b.Influencer__c == false){
                    c.Influencer__c = true;
                }

                accToUpdate.add(c);
                
            }   
        }

        if(!accToUpdate.isEmpty()){
            update accToUpdate;
        }
    
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    //Wrapper class for klout score and Klout ID by Ata
    private class kloutDetails{
        
        string kloutId;
        Integer kloutScore;
        
    }
    
    //Get Klout Score externally
    private kloutDetails getKloutScore(Account acc) {
        Integer kloutScore = 0;
        String kloutId = '';
        //declaring klout objet class
        kloutDetails kloutInfo = new kloutDetails();
        try {
            //added if else ladder to by pass KloutID callout
            if(acc.KloutId__c == Null){
                if(acc.sf4twitter__Twitter_User_Id__pc != null) {
                    kloutId = (String) kloutObjects('http://api.klout.com/v2/identity.json/tw/'+acc.sf4twitter__Twitter_User_Id__pc+'?key=us4k68rumkgwwnqkk797hsm4').get('id');
                    
                } else if(acc.sf4twitter__Twitter_Username__pc != null) {
                    kloutId = (String) kloutObjects('http://api.klout.com/v2/identity.json/twitter?screenName='+acc.sf4twitter__Twitter_Username__pc+'&key=us4k68rumkgwwnqkk797hsm4').get('id');
                }               
            }
            else{
                kloutId = acc.KloutId__c;
            }
            
            if(!''.equals(kloutId)) {
                Decimal kloutDecimal = Decimal.valueOf(Double.valueOf(kloutObjects('http://api.klout.com/v2/user.json/'+kloutId+'/score?key=us4k68rumkgwwnqkk797hsm4').get('score')));
                kloutScore = Integer.valueOf(kloutDecimal.divide(1, 0, System.RoundingMode.UP));
            }
            
            //preparing the klout object class
            kloutInfo.kloutId = kloutId;
            kloutInfo.kloutScore = kloutScore;
            
        } catch(Exception e) {  
            System.Debug('exception::'+e);          
            System.Debug(e.getMessage());
        }

        return kloutInfo;
    }

    private Map<String, Object> kloutObjects(String endpoint) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('GET');
        system.debug('request::'+req);
        HttpResponse res = h.send(req);        
        system.debug('response::'+res);
        return (Map<String, Object>) JSON.deserializeUntyped(res.getbody());
    }    

}