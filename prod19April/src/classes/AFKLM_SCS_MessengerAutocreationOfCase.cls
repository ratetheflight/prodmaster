/*************************************************************************************************
* File Name     :   AFKLM_SCS_MessengerAutocreationOfCase
* Description   :   class for auto case creation in messenger
* @author       :   
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     06/10/2016      Sai Choudhry   Marking case as urgent for Ambassador and Vip. JIRA ST-853.
* 1.1     31/01/2017      Nagavi         ST-1358 - Development- Case Topic Filling by DG in UAT
* 1.2     02/03/2017     Sai Choudhry    Removing the SOQL query for Record Type, JIRA ST-1868 &1869.

****************************************************************************************************/
public with sharing class AFKLM_SCS_MessengerAutocreationOfCase {
  private Map<String,SocialPost> posts = new Map<String,SocialPost>();
  private Map<String,SocialPersona> idsForAccounts = new Map<String,SocialPersona>();
  private List<SocialPersona> personaList = new List<SocialPersona>();
  private List<Account> accountsToInsert = new List<Account>();
  private List<Contact> contactList = new List<Contact>();
  private Map<String, Contact> contactMap = new Map<String,Contact>();
  private List<SocialPost> postsToUpdate = new List<SocialPost>();
  private Set<SocialPost> setOfPostsToUpdate = new Set<SocialPost>();
  private Map<String,Case> casesToInsert = new Map<String, Case>();
  private Map<String, String> postsToUpdateWithAccounts = new Map<String, String>();

  public List<Account> accountList = new List<Account>();

  private Map<String, SocialPost> postMap = new Map<String,SocialPost>();

  private List<String> accIds = new List<String>();

  public void autoCreateCase(List<SocialPost> socialPosts){

    //to have social pots in ascending order
    socialPosts.sort();

    for(SocialPost sp : socialPosts){
      if(!posts.containsKey(sp.personaId) && sp.personaId != null){
        posts.put(sp.personaId, sp);
      }
    }

    if(!posts.isEmpty()){
      personaList = [SELECT Id, RealName, ParentId, Name, FB_User_ID__c FROM SocialPersona WHERE Id IN: posts.keySet()];
    }

    if(!personaList.isEmpty()){
      for(SocialPersona persona : personaList){
        if(persona.FB_User_ID__c != null && (!idsForAccounts.containsKey(persona.FB_User_ID__c))){
          idsForAccounts.put(persona.FB_User_ID__c, persona);
        }
        if(!postMap.containsKey(persona.FB_User_ID__c)){
          postMap.put(persona.FB_User_ID__c,posts.get(persona.Id));
        }
      }
    }

    if(!idsForAccounts.isEmpty()){

      accountList = [SELECT Id, Company__c, sf4twitter__Fcbk_User_Id__pc,  sf4twitter__Fcbk_Username__pc FROM Account WHERE sf4twitter__Fcbk_User_Id__pc IN: idsForAccounts.keySet()];

      // return and add to the list    
      checkCreatePersonAccount(personaList);

      for(Account a : accountList){
        accIds.add(a.id);
      }

      contactList = [SELECT Id, AccountId FROM Contact WHERE accountId IN: accIds];
      for(Contact c: contactList){
        if(!contactMap.containsKey(c.accountId)){
          contactMap.put(c.accountId, c);
        }
      }

      //call case creation with all necessary data
      Case newCase = new Case();
      for(String s : postMap.keySet()){

        for(Account personAccount : accountList){
          
          if(s == personAccount.sf4twitter__Fcbk_User_Id__pc){
            newCase = createCase(personAccount.Id,contactMap.get(personAccount.Id).Id,postMap.get(personAccount.sf4twitter__Fcbk_User_Id__pc));
            //postMap.get(personAccount.sf4twitter__Fcbk_User_Id__pc).ParentId = newCase.id;
            postMap.get(personAccount.sf4twitter__Fcbk_User_Id__pc).WhoId = personAccount.Id;

            //setOfPostsToUpdate.add(postMap.get(personAccount.sf4twitter__Fcbk_User_Id__pc));
            postsToUpdateWithAccounts.put(postMap.get(personAccount.sf4twitter__Fcbk_User_Id__pc).Handle,personAccount.Id);
            casesToInsert.put(postMap.get(personAccount.sf4twitter__Fcbk_User_Id__pc).Handle,newCase);

            break;
          }
        }
        
        newCase = null;        
      }

      insert casesToInsert.values();
      //update postsToUpdate;
    }

    //UPDATE ALL SOCIAL POSTS FROM INITIAL LIST (FROM MAP HANDLE,CASE NO???)
    for(SocialPost relatedSocialPost : socialPosts){

      if(relatedSocialPost.ParentId == null &&
          casesToInsert.get(relatedSocialPost.Handle) != null){

        relatedSocialPost.ParentId = casesToInsert.get(relatedSocialPost.Handle).Id;
        relatedSocialPost.WhoId = postsToUpdateWithAccounts.get(relatedSocialPost.Handle);
        if(!setOfPostsToUpdate.contains(relatedSocialPost)){
          setOfPostsToUpdate.add(relatedSocialPost);
        }
      }
    }

    postsToUpdate.addAll(setOfPostsToUpdate);

    update postsToUpdate;

  }

  private void checkCreatePersonAccount(List<SocialPersona> personaList){
    List<SocialPersona> personasToUpdate = new List<SocialPersona>();
    //AFKLM_SCS_PersonAccountRecTypeSingleton part = AFKLM_SCS_PersonAccountRecTypeSingleton.getInstance();
    //Modified by Sai.Removing the SOQL query for Record Type, JIRA ST-1868 &1869.
    Id persAccRecordTypeId = Schema.SObjectType.account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    
    for(SocialPersona persona : personaList){
      for(Account acc : accountList){
        if(persona.FB_User_ID__c == acc.sf4twitter__Fcbk_User_Id__pc){
          persona.ParentId = acc.Id;

          personasToUpdate.add(persona);
          break;
        }
      }
      if(persona.ParentId !=null && (String.valueOf(persona.ParentId).startsWith('0ST'))){
        Account newAccount =  new Account(              
              RecordTypeId = persAccRecordTypeId,
              sf4twitter__Fcbk_Username__pc = persona.Name,
              sf4twitter__Fcbk_User_Id__pc = persona.FB_User_ID__c,
              Company__c = 'KLM'
              );

  
                if(persona.RealName != null) {

                    if(persona.RealName.length() > 80){
                        newAccount.LastName = persona.RealName.substring(0,80);
                    } else {
                        newAccount.LastName = persona.RealName;
                    }
                }

        accountsToInsert.add(newAccount);
      }
    }
    
    if(!accountsToInsert.isEmpty()) {

      insert accountsToInsert;
      accountList.addAll(accountsToInsert);
    }

    for(Account accnt : accountsToInsert){
      for(SocialPersona prsna : personaList){
        if(prsna.FB_User_ID__c == accnt.sf4twitter__Fcbk_User_Id__pc){
          prsna.ParentId = accnt.id;

          personasToUpdate.add(prsna);
          break;
        }
      }
    }
    
    update personasToUpdate;
  }

  private Case createCase(String personAccountId, String contactPersonAccountId, SocialPost socialPost){
    //singleton

    //AFKLM_SCS_CaseRecordTypeSingleton crt = AFKLM_SCS_CaseRecordTypeSingleton.getInstance();
    ID rt = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();






    Case cs = new Case(
          accountId = personAccountId,
          contactId = contactPersonAccountId,

          RecordTypeId = rt,
          subject = socialPost.Name,
          ownerId = userInfo.getUserId(),
          //ownerId = socialPost.OwnerId,
          Language__c = socialPost.Language__c,
          Origin = socialPost.Provider,
          suppliedName = userInfo.getUserName(),
          chatHash__c = socialPost.chatHash__c);
          
          if(socialPost.Chat_Medium__c != null)
          {
              cs.Chat_Medium__c = socialPost.Chat_Medium__c ;
          }


          
          //Added by Nagavi - ST-1358 - Development- Case Topic Filling by DG in UAT
          if(socialPost.content!=null && socialPost.IsOutbound==false && socialPost.TopicProfileName!=null && (!socialPost.TopicProfileName.contains('France')|| !socialPost.TopicProfileName.contains('AF'))) {  
              cs.Data_for_Prefill_By_DG__c=AFKLM_SCS_ControllerHelper.gettruncatedString(cs.Data_for_Prefill_By_DG__c,socialPost.content);
              //cs.dgAI2__DG_Fill_Status__c='Pending';
          }




      

      return cs;
  }
}