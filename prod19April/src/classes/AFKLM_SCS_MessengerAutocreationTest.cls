@isTest
private class AFKLM_SCS_MessengerAutocreationTest {

    static testMethod void messengerAutocreationCasesTest1(){

    	List<SocialPost> posts = new List<SocialPost>();
    	
    	Account acc= new Account(
    		Firstname='Test', 
    		LastName='Account', 
    		sf4twitter__Fcbk_User_Id__pc='FB_123456789');
    	insert acc;

    	SocialPersona spersona = new SocialPersona(
    		ParentId=acc.Id,
    		Name='Test Persona', 
    		Provider='Facebook', 
    		ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
    		ExternalId='123456789');
    	insert spersona;
    	
    	for(Integer i=0;i<=10;i++) {

	    	SocialPost spost = new SocialPost(
	    		name='Post from: Test Account',
	    		Posted=System.now(),
	    		Provider='Facebook',
	    		Is_campaign__c=false,
	    		MessageType='Private',
	    		chatHash__c='0000000111112345',
	    		Handle='Test Persona',
	    		PersonaId=spersona.Id,
	    		ExternalPostId__c='1396604723809:f3da05e20a1267c585'+i);
	    	insert spost;

	    	posts.add(spost);
		}

    	Test.startTest();

    	AFKLM_SCS_MessengerAutocreationOfCase tc = new AFKLM_SCS_MessengerAutocreationOfCase();
    	tc.autoCreateCase(posts); 

    	Test.stopTest();
    }

    static testMethod void messengerAutocreationCasesTest2(){

    	List<SocialPost> posts = new List<SocialPost>();
  
	    SocialPost sp = new SocialPost(
	    	name='Post from: Test Account',
	    	Posted=System.now(),
	    	Provider='Facebook',
	    	Is_campaign__c=false,
	    	MessageType='Private',
	    	chatHash__c='0000000111112345',
	    	Handle='Test Persona',
	    	ExternalPostId__c='1396604723809:f3da05e20a1267c585');
	   	insert sp;


    	SocialPersona spersona = new SocialPersona(
    		ParentId=sp.Id,
    		Name='Test Persona', 
    		RealName='Test Persona',
    		Provider='Facebook', 
    		ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
    		ExternalId='123456789');
    	insert spersona;
    	
    	for(Integer i=0;i<=10;i++) {

	    	SocialPost spost = new SocialPost(
	    		name='Post from: Test Account',
	    		Posted=System.now(),
	    		Provider='Facebook',
	    		Is_campaign__c=false,
	    		MessageType='Private',
	    		chatHash__c='0000000111112345',
	    		Handle='Test Persona',
	    		PersonaId=spersona.Id,
	    		ExternalPostId__c='1396604723809:f3da05e20a1267c585'+i);
	    	insert spost;

	    	posts.add(spost);
		}

    	Test.startTest();

    	AFKLM_SCS_MessengerAutocreationOfCase tc = new AFKLM_SCS_MessengerAutocreationOfCase();
    	tc.autoCreateCase(posts); 

    	Test.stopTest();
    }


   static testMethod void messengerAutocreationCasesTest3(){

    	List<SocialPost> posts = new List<SocialPost>();
  
	    SocialPost sp = new SocialPost(
	    	name='Post from: Test Account',
	    	Posted=System.now(),
	    	Provider='Facebook',
	    	Is_campaign__c=false,
	    	MessageType='Private',
	    	chatHash__c='0000000111112345',
	    	Handle='Test Persona',
	    	ExternalPostId__c='1396604723809:f3da05e20a1267c585');
	   	insert sp;


    	SocialPersona spersona = new SocialPersona(
    		ParentId=sp.Id,
    		Name='Test Persona', 
    		RealName='Test Personaludingh Marualinght Vantualinh Karulogulis Pankawaliczjus Warkowolinomuszijuhasus',
    		Provider='Facebook', 
    		ProfileURL='http://www.testingurlfortestuser.com/id=123456789', 
    		ExternalId='123456789');
    	insert spersona;
    	
    	for(Integer i=0;i<=10;i++) {

	    	SocialPost spost = new SocialPost(
	    		name='Post from: Test Account',
	    		Posted=System.now(),
	    		Provider='Facebook',
	    		Is_campaign__c=false,
	    		MessageType='Private',
	    		chatHash__c='0000000111112345',
	    		Handle='Test Persona',
	    		PersonaId=spersona.Id,
	    		ExternalPostId__c='1396604723809:f3da05e20a1267c585'+i);
	    	insert spost;

	    	posts.add(spost);
		}

    	Test.startTest();

    	AFKLM_SCS_MessengerAutocreationOfCase tc = new AFKLM_SCS_MessengerAutocreationOfCase();
    	tc.autoCreateCase(posts); 

    	Test.stopTest();
    }
}