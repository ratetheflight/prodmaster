/**********************************************************************
 Name:  AFKLM_SCS_PA_DetailsExtension.cls
======================================================
Purpose: 
    1. Controller for SCS_PA_Details VF Page
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    22 Jul 2014     Initial development
    1.1     Luuk Vermaas        23 Sep 2014     Modified Custom Object reference
***********************************************************************/ 
public with sharing class AFKLM_SCS_PA_DetailsExtension {

    private final Account thisAccnt;

    public List<Relative__c> listOfDoc {
        get{
            if (listOfDoc == null){
                listOfDoc = [
                    select
                        Id,
                        Name__c,
                        Date_Of_Birth__c,
                        Document_Number__c,
                        Expiration_Date__c
                    from 
                        Relative__c 
                    where
                        Account__c = :thisAccnt.Id
                ];
            }
            return listOfDoc;
        }
        private set;
    }

    public AFKLM_SCS_PA_DetailsExtension(ApexPages.StandardController stdController) {
        this.thisAccnt = (Account)stdController.getRecord();
    }

}