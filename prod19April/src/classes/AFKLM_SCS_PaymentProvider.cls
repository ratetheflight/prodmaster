public class AFKLM_SCS_PaymentProvider
{
    public User paymentUser;
    public AFKLM_SCS_PaymentProvider() {
        paymentUser = [SELECT FirstName, LastName, SocialPaymentProviderUser__c, SocialPaymentProviderPassword__c, WebChatPaymentProviderUser__c, WebChatPaymentProviderPassword__c FROM User WHERE id= :UserInfo.getUserID()];
    }
    
    public User getUser() {
        return paymentUser;
    }
}