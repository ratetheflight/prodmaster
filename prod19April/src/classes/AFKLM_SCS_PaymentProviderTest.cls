/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AFKLM_SCS_PaymentProviderTest {

    static testMethod void myUnitTest() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User u1 = new User(Alias = 'newUser', Email='newuser@testorg.com', 
            EmailEncodingKey='UTF-8', FirstName='Testing', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='newuser@klm.com',
            SocialPaymentProviderUser__c='TestUser', SocialPaymentProviderPassword__c='TestPassword',
            WebChatPaymentProviderUser__c='WebchatTestUser', WebChatPaymentProviderPassword__c='WebchatTestPassword');
        
        System.runAs(u1) {
            AFKLM_SCS_PaymentProvider paymentProvider = new AFKLM_SCS_PaymentProvider();
            User paymentUser = paymentProvider.getUser();
            
            System.assertEquals(u1.LastName, paymentUser.LastName);
            System.assertEquals(u1.FirstName, paymentUser.FirstName);
            System.assertEquals(u1.SocialPaymentProviderUser__c, paymentUser.SocialPaymentProviderUser__c);
            System.assertEquals(u1.SocialPaymentProviderPassword__c, paymentUser.SocialPaymentProviderPassword__c);
            System.assertEquals(u1.WebChatPaymentProviderUser__c, paymentUser.WebChatPaymentProviderUser__c);
            System.assertEquals(u1.WebChatPaymentProviderPassword__c, paymentUser.WebChatPaymentProviderPassword__c);
        }
    }
}