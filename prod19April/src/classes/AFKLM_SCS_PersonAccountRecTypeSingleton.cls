public with sharing class AFKLM_SCS_PersonAccountRecTypeSingleton {
	
    private static final AFKLM_SCS_PersonAccountRecTypeSingleton classInstance = new AFKLM_SCS_PersonAccountRecTypeSingleton();

    public RecordType persAccountRecType{get;private set;}
    
    private AFKLM_SCS_PersonAccountRecTypeSingleton() {
        persAccountRecType = [SELECT Id FROM RecordType WHERE (Name='Person Account') AND (SobjectType='Account')];
      
    }

    public static AFKLM_SCS_PersonAccountRecTypeSingleton getInstance() {
        return classInstance;
    }

}