/********************************************************************** 
 Name:  AFKLM_SCS_PreviousCasesExtension 
 Task:    N/A
 Runs on: SCS_PreviousCases
====================================================== 
Purpose: 
    AFKLM_SCS_PreviousCasesExtension shows last 5 cases based on Person Account in the right part of the Social Post console.
    Added functionality: Shows last 5 Fidelio (ccomplaint) cases for customer 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          11/03/2014      Initial Development
    1.1     Ivan Botta          12/05/2014      Cleaning up the code. Reduce SOQL Queries.
    1.2     Ivan Botta          15/07/2015      Added functionality Fidelio cases
    1.3     Sai Choudhry        17/10/2016      Modified query to include NPS Score. JIRA ST-961
***********************************************************************/


public class AFKLM_SCS_PreviousCasesExtension {
    
    public Case cs;
    public String customerId {get;set;} // CHANGE TO PRIVATE AFTERWARDS
    private String socialId = '';
    private String socialNetwork;
    private String flyingBlueNumber;
    private String eMail = '';

    private static final String SEARCH_CUSTOMER_URL = 'https://api.klm.com/customerapi/customer';
    private static final String SEARCH_CUSTOMER_URL_BY_ID = 'https://api.klm.com/customerapi/customers/';
    
    public ComplaintsResponseInfo fidelioCases {
        get {
            if(fidelioCases == null) {
                fidelioCases = new ComplaintsResponseInfo();
            }
            return fidelioCases;
        }
        private set;
    }

    public Integer fCaseCount {get;set;}

    public AFKLM_SCS_PreviousCasesExtension() {}

    public AFKLM_SCS_PreviousCasesExtension (ApexPages.StandardController stdController) {
        Id thisId = (Id) stdController.getId();


        if(!Test.isRunningTest()){
           stdController.addFields(new List<String>{'AccountId', 'CreatedDate', 'Sentiment_at_close_of_case__c','Origin'});
        }
        this.cs = (Case)stdController.getRecord();
        this.socialNetwork = cs.Origin;
        thisId = this.cs.AccountId;

        try {
                
            setAccountParameters(thisId);
            setIdCustomerAPI();
            getCustomerAPIAccountDetails(customerId);
            
        } catch (DmlException e) {
            // Try to reconnect instead of the Websrv callout, it's not possible to do a DML during Websrv callout
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Connection refused');
            ApexPages.addMessage(myMsg);
        }
    }
    

    /** 
    * Get casenumber, status of 3 cases based on Person Account
    *
    * @return cases based on Person Account
    */  
    public List<Case> getCases() {
        List<Case> cases = new List<Case>();
        //Included Origin in Query for ST-002929 Narmatha
        //Included NPS Score in Query. JIRA ST-961 
        if(cs.AccountId != null){
            cases = [SELECT id, casenumber, createddate, status,case_topic__c, case_detail__c, Sentiment_at_close_of_case__c, PNR__c,origin,scs_twtSurveys__Feedback_Score_Value__c FROM Case WHERE AccountId =: cs.AccountId ORDER BY CreatedDate DESC LIMIT 5];
        } 
        //List <Case> cs = [SELECT id, casenumber, createddate, status,case_topic__c, case_detail__c  FROM Case WHERE (AccountId =: cs.AccountId AND id !=: cs.id AND CreatedDate <: cs.CreatedDate) ORDER BY CreatedDate DESC LIMIT 3];
        //IBO: Commented soql query shows 3 previous cases.
        return cases;
    }
    
     /** 
    * Get the count of cases based on Person Account
    *
    * @return number of cases
    */
    public Integer getCaseCount(){
        Integer casecount;

        if(cs.AccountId != null){
           casecount=[SELECT COUNT() FROM Case WHERE AccountID=:cs.AccountId];
           //return casecount;
        } 
        return casecount;
    }
    
     /** 
    * Get the the name of the person account which owns actual case and other cases
    *
    * @return name of the account
    */
    public String getAccountName(){

        try{
            
            Account acc=[SELECT name FROM Account WHERE id=:cs.AccountId LIMIT 1];
            return acc.name;

        }catch(Exception e){

            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display information <br> - Please check Account of the customer');
            ApexPages.addMessage(myMsg);
            return ' ';
        }
    }

    //*********************FIDELIO INTEGRATION************************************


    /** 
     * Set the Flying Blue number, E-mail and SocialHandle, SocialNetwork parameters
     */
    private void setAccountParameters(String accountId) {
        List<Account> accnt = [SELECT Flying_Blue_Number__c, PersonEmail, sf4twitter__Fcbk_Username__pc, sf4twitter__Fcbk_User_Id__pc, sf4twitter__Twitter_Username__pc, sf4twitter__Twitter_User_Id__pc FROM Account WHERE Id =: accountId LIMIT 1];
        
        if(accnt.size() > 0 ) {
            if(accnt[0].Flying_Blue_Number__c != null) {
                this.flyingBlueNumber = accnt[0].Flying_Blue_Number__c;
            }
            
            if(accnt[0].PersonEmail != null) {
                this.eMail = accnt[0].PersonEmail;
            }
            
            // Not needed now but can be used in the future
            if(accnt[0].sf4twitter__Fcbk_User_Id__pc != null) {
                this.socialId = accnt[0].sf4twitter__Fcbk_User_Id__pc.replace('FB_','');
                this.socialNetwork = 'Facebook';
            } else {
                this.socialId = accnt[0].sf4twitter__Twitter_User_Id__pc;
                this.socialNetwork = 'Twitter';
            }
        }
    }

    /** 
     * Search the Customer API Id based on Flying Blue number, E-mail and SocialHandle, SocialNetwork parameters
     */
    private void setIdCustomerAPI() {
        
        try {
            String sb = '';
            
            if( !''.equals(flyingBlueNumber) &&  flyingBlueNumber != null) {
                sb = '/?flying-blue-number=' + flyingBlueNumber;
            } else if(!''.equals(eMail) && eMail != null) {
                sb = '/?email-address=' + eMail;
            } else if(!''.equals(socialId) && !''.equals(socialNetwork)) {
                sb = '/?social-network=' +socialNetwork+'&social-identity-identifier=' +socialId;
            }
            
            AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
            AFKLM_SCS_RestResponse response = customerAPIClient.safeRequest(SEARCH_CUSTOMER_URL + sb, 'GET', new Map<String,String>());

            Map<String, Object> rMap = response.toMap();
            this.customerId = (String) rMap.get('id');

        } catch(Exception e) {

            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display information <br> - Please check Flying blue number, e-mail');
            ApexPages.addMessage(myMsg);
        } 
    }

    /** 
     * Get the Customer API account details.
     */
    private void getCustomerAPIAccountDetails(String customerId) {

        try {
            
            if(customerId != null) {

                AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
                AFKLM_SCS_RestResponse resComplaints = customerAPIClient.safeRequest(SEARCH_CUSTOMER_URL_BY_ID+this.customerId+'/complaints/?max-claims-returned=99', 'GET', new Map<String,String>());

                if( !resComplaints.error ){
                    
                    String response = resComplaints.getResponseText().replace('"number":','"number_x" :');
                    system.debug('--+ response json: '+response);   
                    if(!String.isBlank( response )){
                            
                        fidelioCases = (ComplaintsResponseInfo) JSON.deserialize(response, ComplaintsResponseInfo.class);
                        system.debug('--+ fidelioCases: '+fidelioCases);
                        for(Integer i=fidelioCases.complaints.size()-1; i>=0; i--){

                            if(generalCase( fidelioCases.complaints[i].fileId )) {

                                fidelioCases.complaints.remove(i);
                            }
                        }

                        this.fCaseCount = fidelioCases.complaints.size();
                        System.debug('--+ fidelioCases: '+fidelioCases);
                    } else {
                    
                        System.debug( 'No complaints found' );
                    }
                }
            }

        } catch(Exception e) {
            // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display Customer Id: ' + customerId+ ' Exception: '+e);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display all customer Complaints for Customer Id: ' + customerId+ '. Please contact your Administrator.');
            ApexPages.addMessage(myMsg);
            System.debug('--+ Error msg.:'+e.getMessage());
        } 
    }

    /**
     * This function is to check if case is general or individual (we want to filter out general ones)
     * The check is made based on fileId (ending with 000 is general, whilst idividual 001, 002, 003)
     */
    private Boolean generalCase(String fileId) {

        String rex = '(.*)000$';
        Pattern p = Pattern.compile(rex);
        Matcher m = p.matcher(fileId);
        
        return m.matches();
    }
}