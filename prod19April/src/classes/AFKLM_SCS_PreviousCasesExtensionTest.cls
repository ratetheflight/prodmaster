/********************************************************************** 
 Name:  AFKLM_SCS_PreviousCasesExtensionTest
 Task:    N/A
 Runs on: AFKLM_SCS_PreviousCasesExtension
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta      	24/04/2014      Initial Development
***********************************************************************/
@isTest
private class AFKLM_SCS_PreviousCasesExtensionTest {

	private AFKLM_SCS_PreviousCasesExtensionTest(){
		AFKLM_SCS_PreviousCasesExtension pce = new AFKLM_SCS_PreviousCasesExtension();
	}

     static testMethod void getCasesTest() {
     	Account acc = new Account(FirstName='Test', LastName='Account');
     	insert acc;
     	Case css = new Case(Status='New', AccountId=acc.id);
    	List<Case> cs= new List<Case>{new Case(Status='New', AccountId = acc.id),new Case(Status='New', AccountId = acc.id),new Case(Status='New', AccountId = acc.id)};
    	insert cs;
     	AFKLM_SCS_PreviousCasesExtension pc = new AFKLM_SCS_PreviousCasesExtension();
     	pc.cs=css;
     	List<Case> caseList = pc.getCases();
    	System.assertEquals(cs.size(), caseList.size());
    }      
    
    static testMethod void getAccountNameTest(){
    	Account acc = new Account(FirstName='Test', LastName='Account');
     	insert acc;
    	Case cs= new Case(Status = 'New', accountId = acc.Id);
    	insert cs;
    	String aaa;
    	AFKLM_SCS_PreviousCasesExtension pc = new AFKLM_SCS_PreviousCasesExtension();
    	pc.cs= cs; 
		aaa = pc.getAccountName();
    	System.assertEquals('Test Account', aaa);
    }
    
    static testMethod void getCaseCountTest(){
    	Account acc = new Account(FirstName='Test', LastName='Account');
     	insert acc;
    	AFKLM_SCS_PreviousCasesExtension pc = new AFKLM_SCS_PreviousCasesExtension();    	
    	Case cs= new Case(Status = 'New', accountId = acc.Id);
		insert cs;
		pc.cs = cs;
    	Integer cscount = pc.getCaseCount();
    	System.assertEquals(1, cscount);
    	
    }
    
}