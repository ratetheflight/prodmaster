/*************************************************************************************************
* File Name     :   AFKLM_SCS_PseudoAccountUpdateBatch
* Description   :   This batch class is used to pseudo update the accounts in such a way that it enters dupecatcher.
                    Set the batch size to 1 for that.
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     15/03/2016      Nagavi         Created the class
****************************************************************************************************/
global class AFKLM_SCS_PseudoAccountUpdateBatch implements Database.Batchable<sObject>{
    
    //query string 
    global string query;
    global final string lastmodified;
    
    //constructor 
    global AFKLM_SCS_PseudoAccountUpdateBatch(integer days){
                 
        try{
            //calculating exact datetime for the case records
            lastmodified = System.Now().addDays(-days).format('yyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                                 
            query =  'SELECT Id FROM ACCOUNT WHERE LastModifiedDate < '+lastmodified +'Limit 100000'; 
            //query =  'SELECT Id FROM ACCOUNT WHERE LastModifiedDate < '+lastmodified; 
            //query= query + lastmodified; 
            //query=query +' Limit 100';   
        }   
        catch(Exception e){
            System.Debug('Exception occurred !!'+e);
        }         
    }
    
        
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        
        try{
            update scope;
        }
        catch(exception e){
            System.debug('Exception Occured:'+e);
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
 }