/********************************************************************** 
 Name:  AFKLM_SCS_ResponseTimeLogic 
 Task:    N/A
 Runs on: SCS_SocialPostAfterInsertUpdate (Trigger)
====================================================== 
Purpose: 
    AFKLM_SCS_ResponseTimeLogic - The first 'SocialPost' which is replied,actioned upon one 'Case' will be used for the FB, TW and KLM.com monitoring logic.
	- Check all the Social Post which are status as 'Replied' and 'Replied' date is the oldest
	- Mark checkbox IncludeResponseTime__c true first engaged SP for 'Response time' to be used by the monitoring
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          17/07/2014      Initial Development
***********************************************************************/
public with sharing class AFKLM_SCS_ResponseTimeLogic {
	public static void socialPostResponseTime(String caseId) {
		List<SocialPost> sp = [SELECT id, IncludeResponseTime__c FROM SocialPost WHERE parentId=:caseId AND Status = 'Replied' ORDER BY Replied_date__c ASC LIMIT 1];
		sp[0].IncludeResponseTime__c = true;
		update sp;
	}
}