@isTest
private class AFKLM_SCS_SearchCustomerTest {

	final static String ACCOUNT_FIRST_NAME = 'TEST';
	final static String ACCOUNT_LAST_NAME = 'USER';

	static {
		setupCustomSettings();
	}

	@isTest
	static void testSearchByFlyingBlueNumberInternalPADeskOnly() {
		Account a = insertTestAccount( 'x' );
		final String QUERY = a.Flying_Blue_Number__c;
		Account b = new Account();
		b.Flying_Blue_Number__c = a.Flying_Blue_Number__c;
		b.FirstName = 'ssss';
		b.LastName = 'ssss';
		b.Personal_Assistance__pc = true;
		insert b;
		List<AFKLM_SCS_SearchCustomer.SearchResultItem> results = AFKLM_SCS_SearchCustomer.searchAccounts( QUERY, true, 10 );
		System.assertEquals( 1, results.size() );
		System.assertEquals( b.LastName, results[0].lastName );
	}

	@isTest
	static void testSearchByFlyingBlueNumberInternal() {
		Account a = insertTestAccount( 'x' );
		final String QUERY = a.Flying_Blue_Number__c;
		Account b = new Account();
		b.Flying_Blue_Number__c = a.Flying_Blue_Number__c;
		b.FirstName = 'ssss';
		b.LastName = 'ssss';
		b.Personal_Assistance__pc = true;
		insert b;
		List<AFKLM_SCS_SearchCustomer.SearchResultItem> results = AFKLM_SCS_SearchCustomer.searchAccounts( QUERY, false, 10 );
		System.assertEquals( 2, results.size() );
		System.assertEquals( a.Flying_Blue_Number__c, results[0].flyingBlueNumber );
	}

	@isTest
	static void testSearchByFlyingBlueNumberExternal() {
		final String QUERY = '1234567890';
		// mockup service will always return a result
		List<AFKLM_SCS_SearchCustomer.SearchResultItem> results = AFKLM_SCS_SearchCustomer.searchAccounts( QUERY, false, 10 );
		System.assertEquals( 1, results.size() );
		System.assert( results[0].isExternal );
	}

	@isTest
	static void testSearchByNameStartsWith() {
		Account a = insertTestAccount( 'x' );
		Account b = insertPADeskTestAccount( 'x' );
		List<Id> fixedSearchResults = new List<Id>{ a.Id, b.Id };
		Test.setFixedSearchResults( fixedSearchResults );
		final String QUERY = ACCOUNT_LAST_NAME.substring(0, 3);
		List<AFKLM_SCS_SearchCustomer.SearchResultItem> results = AFKLM_SCS_SearchCustomer.searchAccounts( QUERY, false, 10 );
		System.assertEquals( 2, results.size() );
	}

	@isTest
	static void testSearchRestrictedByNameStartsWith() {
		final String QUERY = ACCOUNT_LAST_NAME.substring(0, 3);
		System.runAs( generatePADeskAgentUser() ) {
			Account p = insertPADeskTestAccount( 'x' );
			Account r = insertRestrictedTestAccount( 'x' );
			List<Id> fixedSearchResults = new List<Id>{ p.Id, r.Id };
			Test.setFixedSearchResults( fixedSearchResults );
			List<AFKLM_SCS_SearchCustomer.SearchResultItem> results = AFKLM_SCS_SearchCustomer.searchAccounts( QUERY, true, 10 );
			System.assertEquals( 2, results.size() );
		}
	}

	@isTest
	static void testSearchByNameWildcards() {
		final String QUERY = '*' + ACCOUNT_LAST_NAME.substring( 1, ACCOUNT_LAST_NAME.length()-1 ) + '*';
		System.runAs( generatePADeskAgentUser() ) {
			Account a = insertTestAccount( 'x' );
			List<AFKLM_SCS_SearchCustomer.SearchResultItem> results = AFKLM_SCS_SearchCustomer.searchAccounts( QUERY, false, 10 );
			System.assertEquals( 1, results.size() );
			System.assertEquals( a.Id, results[0].Id );
		}
	}

	@isTest
	static void testCreateAccount() {
		final String firstName = 'John';
		final String lastName = 'Doe';
		final String flyingBlueNumber = '1234567890';
		Account a = AFKLM_SCS_SearchCustomer.createAccount( firstName, lastName, flyingBlueNumber );
		System.assertNotEquals( null, a.Id );
		System.assertEquals( firstName, a.FirstName );
		System.assertEquals( lastName, a.LastName );
		System.assertEquals( flyingBlueNumber, a.Flying_Blue_Number__c );
	}

	@isTest
	static void testCreatePADeskCase() {
		System.runAs( generatePADeskAgentUser() ) {
			Account a = insertPADeskTestAccount( 'x' );
			Case c = AFKLM_SCS_SearchCustomer.createCase( a.Id );
			System.assertNotEquals( null, c.Id );
			System.assertEquals( a.Id, c.AccountId );
			c = [ SELECT RecordType.DeveloperName FROM Case WHERE Id = :c.Id ];
			System.assertEquals( c.RecordType.DeveloperName, 'PA_Desk' );
		}
	}

	static Account insertTestAccount( String postfix ) {
		return insertTestAccount(
			ACCOUNT_FIRST_NAME + postfix,
			ACCOUNT_LAST_NAME + postfix,
			generateFlyingBlueNumber(),
			false,
			false
		);
	}

	static Account insertPADeskTestAccount( String postfix ) {
		return insertTestAccount(
			ACCOUNT_FIRST_NAME + postfix,
			ACCOUNT_LAST_NAME + postfix,
			generateFlyingBlueNumber(),
			true,
			false
		);
	}

	static Account insertRestrictedTestAccount( String postfix ) {
		return insertTestAccount(
			ACCOUNT_FIRST_NAME + postfix,
			ACCOUNT_LAST_NAME + postfix,
			null, // restricted accounts do not tend to have a fb number set
			true,
			true
		);
	}

	static Account insertTestAccount( String firstName, String lastName, String fbNumber, Boolean isPADesk, Boolean isRestricted ) {
		Account a = new Account();
		a.FirstName = firstName;
		a.LastName = lastName;
		a.Flying_Blue_Number__c = fbNumber;
		a.Personal_Assistance__pc = isPADesk;
		a.Restricted_Account__c = isRestricted;
		insert a;
		System.assertNotEquals( null, a.Id );
		a = [ SELECT Id, Name, FirstName, LastName, Flying_Blue_Number__c, Personal_Assistance__pc, Restricted_Account__c FROM Account WHERE Id = :a.Id ];
		System.debug( a );
		return a;
	}

	static void setupCustomSettings() {
		// little hack to avoid DML_MIX transaction exception
		System.runAs( new User( Id = UserInfo.getUserId() ) ) {
			AFKLM_SocialCustomerService__c settings = new AFKLM_SocialCustomerService__c();
			settings.Name = 'CustomerAPI';
			settings.SCS_CustomerAPIOAuthAccessToken__c = EncodingUtil.base64Encode( Blob.valueOf( 'test_access_token' ) );
			insert settings;
		}
	}

	static User generatePADeskAgentUser() {
		Profile p = [ SELECT Id FROM Profile WHERE Name = 'PA-Desk Agent' ]; 
		User u = new User(
			Alias = 'standt',
			Email='standarduser@testorg.com',
      		EmailEncodingKey='UTF-8',
      		LastName='Testing',
      		LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			ProfileId = p.Id,
			TimeZoneSidKey='America/Los_Angeles',
			UserName='padeskagent@klm.com'
		);
		insert u;
		return u;
	}

	static User generatePSLAgentUser() {
		Profile p = [ SELECT Id FROM Profile WHERE Name = 'PSL Agent' ]; 
		User u = new User(
			Alias = 'standt',
			Email='standarduser@testorg.com',
      		EmailEncodingKey='UTF-8',
      		LastName='Testing',
      		LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			ProfileId = p.Id,
			TimeZoneSidKey='America/Los_Angeles',
			UserName='pslagent@klm.com'
		);
		insert u;
		assignPSLConsolePermissionSet( u );
		return u;
	}

	static String generateFlyingBlueNumber() {
		// i am pretty sure this is not very efficient code
		final Integer numOfDigits = 10;
		String flyingBlueNumber = '';
		for ( Integer i = 0; i < numOfDigits ; i++ ) {
			Integer x = Math.floor( 10 * Math.random() ).intValue();
			flyingBlueNumber += String.valueOf( x );
		}
		return flyingBlueNumber;
	}

	static void assignPSLConsolePermissionSet( User u ) {
		PermissionSet ps = [ SELECT Id From PermissionSet WHERE Name = 'PSL_Console' ];
		PermissionSetAssignment psa = new PermissionSetAssignment(
			AssigneeId = u.Id,
			PermissionSetId = ps.Id
		);
		insert psa;
	}

}