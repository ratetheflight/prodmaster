@isTest
private class AFKLM_SCS_SingletonsTest {
    
    
    @isTest static void testSkipValidationSingleton() {
        List<SocialPost> lsp = new List<SocialPost>();

        Case cs = new Case(
            Status = 'New',
            Subject = 'Testing case'
            );
        insert cs;

        SocialPost sPost = new SocialPost(
            Name = 'Testing post',
            MessageType = 'Post',
            Provider = 'Facebook',
            ParentId = cs.id
            );
        insert sPost;

        SocialPost sReply = new SocialPost(
            Name = 'Testing post',
            MessageType = 'Reply',
            Provider = 'Facebook',
            ParentId = cs.id
            );
        insert sReply;


        //sPost.ParentId = null;
        //sReply.ParentId = null;
        lsp.add(sPost);
        lsp.add(sReply);

        //update lsp;

        //System.assertEquals(null,sPost.ParentId);
        //System.assertEquals(null,sReply.ParentId);

    }
    
    @isTest static void testRecordTypeSingleton() {
        List<Case> lcs = new List<Case>();
        
        
      // Get User Settings       
      Profile p = [SELECT Id FROM Profile WHERE Name='SCS - Agent']; 
      
      User u1 = new User(Alias = 'stan12', Email='stand1212@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='T211ing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='st21212r@testorg.com');
      
        System.RunAs(u1){                    
        Case cs1 = new Case(
            Status = 'New',
            Subject = 'Testing case',
            RecordTypeId = '01220000000UYzAAAW'
            );
        insert cs1;

        Case cs2 = new Case(
            Status = 'New',
            Subject = 'Testing case',
            RecordTypeId = '01220000000UYzAAAW'
            );
        insert cs2;

        
        cs1.Status = 'Reopened';
        cs2.Subject = 'Test case';
        lcs.add(cs1);
        lcs.add(cs2);
        update lcs;
        }
    }
    
}