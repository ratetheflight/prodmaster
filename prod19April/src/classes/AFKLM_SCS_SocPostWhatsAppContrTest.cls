@isTest
private class AFKLM_SCS_SocPostWhatsAppContrTest {
	
	static testMethod void myUnitTest() {
    	Case tstCase = new Case(
				Priority = 'Normal',
				Status = 'New'
				
			);
				
		insert tstCase; 
    	Account acc= new Account(Firstname='Test', LastName='Account', WhatsApp_User_Id__c='123456789', WhatsApp_Username__c='TestTest');  
    	insert acc;
    	SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Whatsapp', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789', realname='Test Account');
    	insert sp;
    	SocialPost postTest = new SocialPost(parentId = tstCase.id, name='Private Message from: Test Account', MessageType='Private', Posted=System.now(), Handle='TemporaryPersonAccountTest', Provider='Whatsapp', SCS_Status__c = 'New', personaId = sp.id);
    	insert postTest;
    	
    	List<AFKLM_SCS_SocialPostWrapperCls> socialPostListTest = new List<AFKLM_SCS_SocialPostWrapperCls>();
		ApexPages.StandardSetController SocPostSetControllerTest = myStandardSetController(1);
    	AFKLM_SCS_SocialPostWhatsAppController whatsAppController = new AFKLM_SCS_SocialPostWhatsAppController();
    	
    	String thisTestCaseID;
    	String newCaseIdTest;
    	String newCaseNumberTest;
    	Boolean errorTest = false;
    	String existingWhatsAppCaseNumberTest=tstCase.id;
    	String tempSocialHandleTest='TemporaryPersonAccountTest';
    	Map<String, SocialPost> postMap = new Map<String, SocialPost>();
    	List<SocialPost> listSocialPostsTest = new List<SocialPost>();
    	
		for(SocialPost c : (list<SocialPost>)SocPostSetControllerTest.getRecords()) {
			socialPostListTest.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetControllerTest));
        }
        
        socialPostListTest.sort();
		
        Test.startTest();
        thisTestCaseId = whatsAppController.createCasesWhatsApp(socialPostListTest, postMap, errorTest, SocPostSetControllerTest, newCaseIdTest, newCaseNumberTest, existingWhatsAppCaseNumberTest, listSocialPostsTest, postTest, tempSocialHandleTest); 
        Test.stopTest();
    }
    
    static testMethod void noParentIDControllerTest(){
    	Account acc= new Account(Firstname='Test', LastName='Account', WhatsApp_User_Id__c='123456789', WhatsApp_Username__c='TestTest');  
    	insert acc;
    	SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Whatsapp', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789', realname='Test Account');
    	insert sp;
    	SocialPost postTest = new SocialPost(name='Private Message from: Test Account', MessageType='Private', Posted=System.now(), Handle='TemporaryPersonAccountTest', Provider='Whatsapp', SCS_Status__c = 'New', personaId = sp.id);
    	insert postTest;
    	
    	List<AFKLM_SCS_SocialPostWrapperCls> socialPostListTest = new List<AFKLM_SCS_SocialPostWrapperCls>();
		ApexPages.StandardSetController SocPostSetControllerTest = myStandardSetController(1);
    	AFKLM_SCS_SocialPostWhatsAppController whatsAppController = new AFKLM_SCS_SocialPostWhatsAppController();
    	
    	String thisTestCaseID;
    	String newCaseIdTest;
    	String newCaseNumberTest;
    	Boolean errorTest = false;
    	String existingWhatsAppCaseNumberTest='';
    	String tempSocialHandleTest='TemporaryPersonAccountTest';
    	Map<String, SocialPost> postMap = new Map<String, SocialPost>();
    	List<SocialPost> listSocialPostsTest = new List<SocialPost>();
    	
		for(SocialPost c : (list<SocialPost>)SocPostSetControllerTest.getRecords()) {
			socialPostListTest.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetControllerTest));
        }
        
        socialPostListTest.sort();
		
        Test.startTest();
        thisTestCaseId = whatsAppController.createCasesWhatsApp(socialPostListTest, postMap, errorTest, SocPostSetControllerTest, newCaseIdTest, newCaseNumberTest, existingWhatsAppCaseNumberTest, listSocialPostsTest, postTest, tempSocialHandleTest); 
        Test.stopTest();
    }
    
    
    
    private static ApexPages.StandardSetController myStandardSetController(Integer option) {
		List<SocialPost> socialPostList = new List<SocialPost>();
		socialPostList = getSocialPostList(socialPostList, '', option);
		ApexPages.StandardSetController SocPostSetController = 
    		new ApexPages.StandardSetController(socialPostList);
    	
    	return SocPostSetController;
    }
    
	// Helper test method for initialize and insert SocialPost objects
	// TODO: it's double on wrapper also. Create public helper class for testmethods.
	private static List<SocialPost> getSocialPostList(List<SocialPost> socialPostList, String operator, Integer option) {
		if(option == 1){	
			Case tstCase = new Case(
				Priority = 'Normal',
				Status = 'New'
			);
				
		insert tstCase; 
		Datetime postedDate = null;
 		
 		for (Integer x=0; x < 5; x++){
			if(operator == 'substract') {
 				postedDate = Datetime.now()-x;
 			} else {
 				postedDate = Datetime.now()+x;
 			}
 			
			SocialPost tstSocialPostWeChat1 = new SocialPost(
										Name = 'Test WA '+x,
										Provider = 'WhatsApp',
										Posted = postedDate,
										ParentId = tstCase.Id,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat1;
			socialPostList.add(tstSocialPostWeChat1);
			
			SocialPost tstSocialPostWeChat2 = new SocialPost(
										Name = 'Test WA '+x,
										Provider = 'WhatsApp',
										Posted = postedDate,
										ParentId = tstCase.Id,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat2;
			socialPostList.add(tstSocialPostWeChat2);
		}
	} else {
			Datetime postedDate = null;
 		
 		for (Integer x=0; x < 5; x++){
			if(operator == 'substract') {
 				postedDate = Datetime.now()-x;
 			} else {
 				postedDate = Datetime.now()+x;
			}
 			
			SocialPost tstSocialPostWeChat1 = new SocialPost(
										Name = 'Test WA '+x,
										Provider = 'WhatsApp',
										Posted = postedDate,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat1;
			socialPostList.add(tstSocialPostWeChat1);
			
			SocialPost tstSocialPostWeChat2 = new SocialPost(
										Name = 'Test WA '+x,
										Provider = 'WhatsApp',
										Posted = postedDate,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat2;
			socialPostList.add(tstSocialPostWeChat2);
				}
 			}
 		
 		
		return socialPostList;
	}
	
}