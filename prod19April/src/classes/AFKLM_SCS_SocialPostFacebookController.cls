/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostFacebookController
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPosController
====================================================== 
Purpose: 
    Social Post Controller Class to utilize existing List View in Apex with Pagination Support with logic used for Facebook.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          22/07/2014      Initial Development
    1.1     Sai Choudhry        06/10/2016      Populating the engagement time. JIRA ST-1069
    1.2     Sai Choudhry        01/11/2016      Populating Mark As reviewed date and Agent status. JIRA ST-1231 & ST-1232
    1.3     Pushkar             20/01/2017      A new agent status 'Engaged' in place of 'Mark as reviewed'. JIRA ST-1263
    1.4     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868.
***********************************************************************/
public with sharing class AFKLM_SCS_SocialPostFacebookController {

    public String createCasesFacebook(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();
        //Modified by Sai. Removing the SOQL query for Record Type, JIRA ST-1868.
        ID caseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();


        if(post.ParentId == null) {
            //IBO: Code below needs to be changes as only social post on which it was replied on should be "Actioned"
            //After engaging all social posts from same handle should be MAR (plus other fields: Ignore_for_SLA__c,SCS_Status__c,SCS_MarkAsReviewed__c, SCS_MarkAsReviewedBy__c, etc)
            //Trigger SCS_SocialPostAfterInsertUpdate (or other name) will change the one social post to Actioned 
            //Modified by sai. Populating Agent status. JIRA ST-1232. 
            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed", "Engaged" OR "Actioned" and being handled by another agent. ' ) );
                return null;
            }

            post.SCS_Status__c = 'Engaged';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();
            //Added by sai, Populating the engagement time. JIRA ST-1069
            post.Engaged_Date__c = Datetime.now();

            listSocialPosts.add(post);
            postMap.put( post.Id, post );
        } else {
            //Modified by sai. Populating Agent status. JIRA ST-1232. 
            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );
                return null;
            }

            //Set Status "In Progress" only for KLM -- Based on record type Servicing
            //Modified by Sai.Removing the SOQL query for Record Type, JIRA ST-1868.
            List<Case> caseToUpdate = [SELECT Id, Status FROM Case WHERE Id =: post.ParentId AND RecordTypeId =: caseRecordTypeId LIMIT 1];

            if(!caseToUpdate.isEmpty() && caseToUpdate.size() != 0 ){
                caseToUpdate[0].Status = 'In Progress';
                
                //Update the Case Owner field as who is engages the unpublished post when its Unpublished post from KLM-ST-002644
                if(post.SCS_Post_Tags__c=='KLM-Unpublished')
                  caseToUpdate[0].OwnerId =UserInfo.getUserID() ;
                  
                update caseToUpdate;

                newCaseId = caseToUpdate[0].Id;
            } else {
                newCaseId = post.Parentid;
            }
            //newCaseId = post.Parentid;
 
            post.SCS_Status__c = 'Engaged';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();
            //Added by Sai, Populating the engagement time. JIRA ST-1069
            post.Engaged_Date__c = Datetime.now();

                
            listSocialPosts.add(post);
        }

        if( !postMap.IsEmpty() ){
            returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);

            if(returnedCaseId.size() > 0) {
                               
                newCaseId = returnedCaseId[0];
                if('Private'.equals(post.MessageType)){
                    if(tempSocialHandle != null){
                        this.getAllRelatedPosts(socPostList, newCaseId, tempSocialHandle, SocPostSetController);
                    }
                }
            }

        } else {
            if('Private'.equals(post.MessageType)){
                if(tempSocialHandle != null){
                    this.getAllRelatedPosts(socPostList, newCaseId, tempSocialHandle, SocPostSetController); 
                }
            }       
        }

        if( !error ){
            return newCaseId;
        }
        return null;
    }


    // Engage on case view
    public String createCasesFacebookCaseView(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();
        //Modified by Sai. Removing the SOQL query for Record Type, JIRA ST-1868.
        ID caseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
        //Modified by sai. Populating Agent status. JIRA ST-1232. 
        if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {

            if(post.ParentId == null) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed", "Engaged" OR "Actioned" and being handled by another agent. ' ) );
            } else {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );                
            }
            
            return null;
        }
        
        post.SCS_Status__c = 'Engaged';
        post.SCS_MarkAsReviewed__c = true;
        post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
        post.Ignore_for_SLA__c = true;
        post.OwnerId = userInfo.getUserId();
        //Added  by Sai. Populating the engagement time. JIRA ST-1069
        post.Engaged_Date__c = Datetime.now();
                        
        if(post.ParentId == null) {

            postMap.put( post.Id, post );
        } else {
            //Set Status "In Progress" only for KLM -- Based on recordType
            //Modified by sai.Removing the SOQL query for Record Type, JIRA ST-1868.
            List<Case> caseToUpdate = [SELECT Id, Status FROM Case WHERE Id =: post.ParentId AND RecordTypeId =: caseRecordTypeId LIMIT 1];

            if(!caseToUpdate.isEmpty() && caseToUpdate.size() != 0){
                caseToUpdate[0].Status = 'In Progress';
                
                 //Update the Case Owner field as who is engages the unpublished post when its Unpublished post from KLM-ST-002644
                  if(post.SCS_Post_Tags__c=='KLM-Unpublished')
                  caseToUpdate[0].OwnerId =UserInfo.getUserID() ;

                update caseToUpdate;

                newCaseId = caseToUpdate[0].Id;
            } else {
                newCaseId = post.Parentid;
            }

            //newCaseId = post.Parentid;
        }

        listSocialPosts.add(post);

        if( !postMap.IsEmpty() ){
            returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);

            if(returnedCaseId.size() > 0) {
                               
                newCaseId = returnedCaseId[0];
                if('Private'.equals(post.MessageType) && tempSocialHandle != null){
                        
                    this.getAllRelatedPostsCaseView(post, newCaseId, tempSocialHandle, SocPostSetController);
                }
            }

        } else {
            if('Private'.equals(post.MessageType) && tempSocialHandle != null){
                    
                this.getAllRelatedPostsCaseView(post, newCaseId, tempSocialHandle, SocPostSetController); 
            }       
        }

        if( !error ) return newCaseId;

        return null;
    }

    private void getAllRelatedPostsCaseView(SocialPost post, String newCaseId, String tempSocialHandle, ApexPages.StandardSetController SocPostSetController) {

        Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1];
        List<SocialPost> toUpdate = new List<SocialPost>();

        List<SocialPost> relatedPosts = [SELECT id, Handle, SCS_Status__c, SCS_MarkAsReviewed__c, SCS_MarkAsReviewedBy__c, Ignore_for_SLA__c, OwnerId, ParentId, WhoId FROM SocialPost WHERE Handle=:tempSocialHandle AND SCS_CaseNumber__c=:post.SCS_CaseNumber__c AND id!=:post.Id];
        if(relatedPosts.size() > 0) {

            // The non selected to merge by Handle   
            for(SocialPost sp : relatedPosts) {
                //modified by sai. JIRA ST-1231 & ST-1232
                if(tempSocialHandle.equals(sp.Handle) && !('Actioned').equals(sp.SCS_Status__c) && !('Mark as reviewed').equals(sp.SCS_Status__c) && !('Engaged').equals(sp.SCS_Status__c)) {
                    
                    if(sp.ParentId == null) sp.ParentId = newCaseId;
                    
                    //Modified by sai. Populating Agent status. JIRA ST-1232.          
                    sp.SCS_Status__c = 'Mark As Reviewed Auto';
                    sp.SCS_MarkAsReviewed__c = true;
                    sp.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    //Modified by sai. Populating Mark As reviewed date ST-1231.
                    sp.Mark_As_Reviewed_Date__c = Datetime.now();
                    sp.Ignore_for_SLA__c = true;
                    sp.WhoId = personAccountId.accountId;
                    sp.OwnerId = userInfo.getUserId();

                    toUpdate.add(sp);
                }
                            
            }

            update toUpdate;
        }
    }

    private void getAllRelatedPosts(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, String newCaseId, String tempSocialHandle, ApexPages.StandardSetController SocPostSetController) {
              
        Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1]; 
        List<SocialPost> listWrapperSocialPost = new List<SocialPost>();

               
        while(SocPostSetController.getHasNext()) {
            SocPostSetController.next();
            for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {
                socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            }
        }
        //IBO:This needs to be modified as it takes only social posts on only one page ("SocPostList" which size is 200)
        //other posts from the same customer are not taken and not MAR (will not disappear from the view)
        //also logic should not take mark as reviewed posts... now it checks only if the post is actioned
        

        // The non selected to merge by Handle   
        for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
        //for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : testingList) {
            //modified by sai. JIRA ST-1231 & ST-1232 
            if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && wrapperSocialPost.isselected == false) {
                
                if(wrapperSocialPost.cSocialPost.ParentId == null) {
                    wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                } 
                
                //Modified by sai. Populating Agent status. JIRA ST-1232.            
                wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark As Reviewed Auto';
                wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                //Modified by sai. Populating Mark As reviewed date ST-1231.
                wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                // SC: This should not be done after test. The TemporaryPersonAccount will be on the SocialPost instead the user Account.
                wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();

                listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
            }
                        
        }

        update listWrapperSocialPost;
    }

    
}