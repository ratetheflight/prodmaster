/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostTwitterController
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPosController
====================================================== 
Purpose: 
    Social Post Controller Class to utilize existing List View in Apex with Pagination Support with logic used for Twitter.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          22/07/2014      Initial Development
    1.1     Sai Choudhry        06/10/2016      Populating the engagement time. JIRA ST-1069
    1.2     Sai Choudhry        01/11/2016      Populating Mark As reviewed date and Agent status. JIRA ST-1231 & ST-1232
    1.3     Pushkar             20/01/2017      A new agent status 'Engaged' in place of 'Mark as reviewed'. JIRA ST-1263
    1.4     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868. 
***********************************************************************/
public without sharing class AFKLM_SCS_SocialPostTwitterController {
        
    public String createCasesTwitter(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, String existingTwitterCaseNumber, List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();
        //Modified by Sai. Removing the SOQL query for Record Type, JIRA ST-1868.
        ID caseRecordTypeId =Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
        //Case personAccountId;

         while(SocPostSetController.getHasNext()) {
            SocPostSetController.next();
            for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {
                socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            }
        }
        
        if(existingTwitterCaseNumber.equals('')) { 
            ////modified by sai. JIRA ST-1231 & ST-1232
            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed", "Engaged" OR "Actioned" and being handled by another agent. ' ) );
                return null;
            }
        
            post.SCS_Status__c = 'Engaged';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();
            //added by sai, Populating the engagement time. JIRA ST-1069
            post.Engaged_Date__c = Datetime.now();
            listSocialPosts.add(post);
            postMap.put( post.Id, post );
            
            
            
            if( !postMap.IsEmpty() ) {
                returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
                Case personAccountId = [SELECT accountId FROM Case WHERE id =: returnedCaseId[0] LIMIT 1];
                if(returnedCaseId.size() > 0) {
                    newCaseId = returnedCaseId[0];
                    List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
                                    
                    // The non selected to merge by Handle   
                    for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                        //modified by sai. JIRA ST-1231 & ST-1232
                         if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && (wrapperSocialPost.isselected == False)) {
                            if(wrapperSocialPost.cSocialPost.ParentId == null) {
                                wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                            } 
                            
                            //Modified by sai. Populating Agent status. JIRA ST-1232.
                            wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark As Reviewed Auto';
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                            //Modified by sai. Populating Mark As reviewed date ST-1231.
                            wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                            wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                            // SC: This should not be done after test. The TemporaryPersonAccount will be on the SocialPost instead the user Account.
                            wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                            wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                            listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
                        }
                        
                    }
                    update listWrapperSocialPost;
                }
            
                if( !error ){
                    return newCaseId;
                }
            }
                        
       } else {
            //modified by sai. JIRA ST-1231 & ST-1232
            if(('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );
                return null;
            }
            //Modified by Sai. Removing the SOQL query for Record Type, JIRA ST-1868.
            List<Case> caseToUpdate = [SELECT Id, Status FROM Case WHERE Id =: existingTwitterCaseNumber AND RecordTypeId =: caseRecordTypeId LIMIT 1];

            if(!caseToUpdate.isEmpty() && caseToUpdate.size() != 0 ){
                caseToUpdate[0].Status = 'In Progress';

                update caseToUpdate;

                newCaseId = caseToUpdate[0].Id;
            } else {
                newCaseId = existingTwitterCaseNumber;
            }
            
            //newCaseId = existingTwitterCaseNumber;
            Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1];
            List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
            
            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                //added by sai.JIRA ST-1232
                //Start
                if(wrapperSocialPost.isselected == True)
                {
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Engaged';
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                    wrapperSocialPost.cSocialPost.Engaged_Date__c = Datetime.now();
                }
                //end
                //modified by sai. JIRA ST-1231 & ST-1232
                if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && (wrapperSocialPost.isselected == False)) {
                    wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                    //Modified by sai. Populating Agent status. JIRA ST-1232.
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark As Reviewed Auto';
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    //Modified by sai. Populating Mark As reviewed date ST-1231.
                    wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    // SC: This should not be done after test. The TemporaryPersonAccount will be on the SocialPost instead the user Account.
                    // IBO: Fix done... Needs to be properly tested.
                    wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                }
                listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
            }
            update listWrapperSocialPost;
            
            return newCaseId;
            
        }
    return null;
    }   
}