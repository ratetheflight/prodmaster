/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostWeChatController
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPosController
====================================================== 
Purpose: 
    Social Post Controller Class to utilize existing List View in Apex with Pagination Support with logic used for WeChat.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          22/07/2014      Initial Development
    1.1     Sai Choudhry        06/10/2016      Populating the engagement time. JIRA ST-1069
    1.2     Sai Choudhry        09/11/2016      Populating Mark As Reviewed Auto and Mark as reviewed Date. JIRA ST-1231 & 1232
    1.3     Pushkar             20/01/2017      A new agent status 'Engaged' in place of 'Mark as reviewed'. JIRA ST-1263
    
***********************************************************************/
public with sharing class AFKLM_SCS_SocialPostWeChatController {
        
    public String createCasesWeChat(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, String existingCaseNumber,List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();

         while(SocPostSetController.getHasNext()) {
            SocPostSetController.next();
            for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {
                socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            }
        }
        
        if(existingCaseNumber.equals('')) {
            //modified by sai. JIRA ST-1231 & ST-1232
            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed", "Engaged" OR "Actioned" and being handled by another agent. ' ) );
                return null;
            }
        
            post.SCS_Status__c = 'Engaged';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();
            //added by sai, Populating the engagement time. JIRA ST-1069
            post.Engaged_Date__c = Datetime.now();
            listSocialPosts.add(post);
                
            postMap.put( post.Id, post );
            
            if( !postMap.IsEmpty() ) {
                returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
                Case personAccountId = [SELECT accountId FROM Case WHERE id =: returnedCaseId[0] LIMIT 1];
                if(returnedCaseId.size() > 0) {
                    newCaseId = returnedCaseId[0];
                    List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
                                    
                    // The non selected to merge by Handle   
                    for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                        ////modified by sai. JIRA ST-1231 & ST-1232
                         if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && wrapperSocialPost.isselected == False) {
                            if(wrapperSocialPost.cSocialPost.ParentId == null) {
                                wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                            } 
                            //added by sai. Populating mark as reviewed auto value. JIRA ST-1232.
                            wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark as reviewed Auto';
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                            //added by sai. Populating mark as reviewed date.JIRA ST-1231.
                            wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                            wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                            wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                            wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                            
                            listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
                        }
                        
                    }
                    update listWrapperSocialPost;
                }
            
                if( !error ){
                    return newCaseId;
                }
            }
                        
       } else {
            newCaseId = existingCaseNumber;
            Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1];
            //modified by sai. JIRA ST-1231 & ST-1232
            if(('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );
                return null;
            }
            

            
            List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
            
            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                //added by sai. JIRA ST-1231 &ST-1232
                //Start
                if(wrapperSocialPost.isselected == True)
                {
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Engaged';
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                    wrapperSocialPost.cSocialPost.Engaged_Date__c = Datetime.now();
                }
                //End
                //modified by sai. JIRA ST-1231 & ST-1232
                if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && (wrapperSocialPost.isselected == False)) {
                    wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                    //added by sai. Populating mark as reviewed auto value. JIRA ST-1232
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark as Reviewed Auto';
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    //added by sai. Populating mark as reviewed date.JIRA ST-1231.
                    wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                    wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                }
                listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
            }
            update listWrapperSocialPost;
            
            return newCaseId;
            
        }
    return null;
    }   

}