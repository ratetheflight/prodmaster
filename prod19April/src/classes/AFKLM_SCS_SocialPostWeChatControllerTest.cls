@isTest
private class AFKLM_SCS_SocialPostWeChatControllerTest {
	
	static testMethod void myUnitTest() {
    	Case tstCase = new Case(
				Priority = 'Normal',
				Status = 'New'
				
			);
				
		insert tstCase; 
    	Account acc= new Account(Firstname='Test', LastName='Account', sf4Twitter__Twitter_User_Id__pc='123456789', sf4Twitter__Twitter_Username__pc='TestTest');  
    	insert acc;
    	SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'WeChat', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789', realname='Test Account');
    	insert sp;
    	SocialPost postTest = new SocialPost(parentId = tstCase.id, name='Post from: Test Account', MessageType='Comment', Posted=System.now(), Handle='TemporaryPersonAccountTest', Provider='WeChat', SCS_Status__c = 'New', personaId = sp.id);
    	insert postTest;
    	
    	List<AFKLM_SCS_SocialPostWrapperCls> socialPostListTest = new List<AFKLM_SCS_SocialPostWrapperCls>();
		ApexPages.StandardSetController SocPostSetControllerTest = myStandardSetController(1);
    	AFKLM_SCS_SocialPostWeChatController WeChatController = new AFKLM_SCS_SocialPostWeChatController();
    	
    	String thisTestCaseID;
    	String newCaseIdTest;
    	String newCaseNumberTest;
    	Boolean errorTest = false;
    	String existingWeChatCaseNumberTest=tstCase.id;
    	String tempSocialHandleTest='TemporaryPersonAccountTest';
    	Map<String, SocialPost> postMap = new Map<String, SocialPost>();
    	List<SocialPost> listSocialPostsTest = new List<SocialPost>();
    	
		for(SocialPost c : (list<SocialPost>)SocPostSetControllerTest.getRecords()) {
			socialPostListTest.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetControllerTest));
        }
        
        socialPostListTest.sort();
		
        Test.startTest();
        thisTestCaseId = WeChatController.createCasesWeChat(socialPostListTest, postMap, errorTest, SocPostSetControllerTest, newCaseIdTest, newCaseNumberTest, existingWeChatCaseNumberTest, listSocialPostsTest, postTest, tempSocialHandleTest); 
        Test.stopTest();
    }
    
    static testMethod void noParentIDControllerTest(){
    	Account acc= new Account(Firstname='Test', LastName='Account', sf4Twitter__Fcbk_User_Id__pc='123456789', sf4Twitter__Fcbk_Username__pc='TestTest');  
    	insert acc;
    	SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'WeChat', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789', ExternalId='123456789', realname='Test Account');
    	insert sp;
    	SocialPost postTest = new SocialPost(name='Tweet from: Test Account', Posted=System.now(), Handle='TemporaryPersonAccountTest', Provider='WeChat', SCS_Status__c = 'New', personaId = sp.id);
    	insert postTest;
    	
    	List<AFKLM_SCS_SocialPostWrapperCls> socialPostListTest = new List<AFKLM_SCS_SocialPostWrapperCls>();
		ApexPages.StandardSetController SocPostSetControllerTest = myStandardSetController(0);
    	AFKLM_SCS_SocialPostWeChatController WeChatController = new AFKLM_SCS_SocialPostWeChatController();
    	
    	String thisTestCaseID;
    	String newCaseIdTest;
    	String newCaseNumberTest;
    	Boolean errorTest = false;
    	String existingWeChatCaseNumberTest='';
    	String tempSocialHandleTest='TemporaryPersonAccountTest';
    	Map<String, SocialPost> postMap = new Map<String, SocialPost>();
    	List<SocialPost> listSocialPostsTest = new List<SocialPost>();
    	
		for(SocialPost c : (list<SocialPost>)SocPostSetControllerTest.getRecords()) {
			socialPostListTest.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetControllerTest));
        }
        
        socialPostListTest.sort();
		
        Test.startTest();
        thisTestCaseId = WeChatController.createCasesWeChat(socialPostListTest, postMap, errorTest, SocPostSetControllerTest, newCaseIdTest, newCaseNumberTest, existingWeChatCaseNumberTest, listSocialPostsTest, postTest, tempSocialHandleTest); 
        Test.stopTest();
    }
    
    
    
    private static ApexPages.StandardSetController myStandardSetController(Integer option) {
		List<SocialPost> socialPostList = new List<SocialPost>();
		socialPostList = getSocialPostList(socialPostList, '', option);
		ApexPages.StandardSetController SocPostSetController = 
    		new ApexPages.StandardSetController(socialPostList);
    	
    	return SocPostSetController;
    }
    
	// Helper test method for initialize and insert SocialPost objects
	// TODO: it's double on wrapper also. Create public helper class for testmethods.
	private static List<SocialPost> getSocialPostList(List<SocialPost> socialPostList, String operator, Integer option) {
		if(option == 1){	
			Case tstCase = new Case(
				Priority = 'Normal',
				Status = 'New'
			);
				
		insert tstCase; 
		Datetime postedDate = null;
 		
 		for (Integer x=0; x < 5; x++){
			if(operator == 'substract') {
 				postedDate = Datetime.now()-x;
 			} else {
 				postedDate = Datetime.now()+x;
 			}
 			
			SocialPost tstSocialPostWeChat1 = new SocialPost(
										Name = 'Test TW '+x,
										Provider = 'WeChat',
										Posted = postedDate,
										ParentId = tstCase.Id,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat1;
			socialPostList.add(tstSocialPostWeChat1);
			
			SocialPost tstSocialPostWeChat2 = new SocialPost(
										Name = 'Test TW '+x,
										Provider = 'WeChat',
										Posted = postedDate,
										ParentId = tstCase.Id,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat2;
			socialPostList.add(tstSocialPostWeChat2);
		}
	} else {
			Datetime postedDate = null;
 		
 		for (Integer x=0; x < 5; x++){
			if(operator == 'substract') {
 				postedDate = Datetime.now()-x;
 			} else {
 				postedDate = Datetime.now()+x;
			}
 			
			SocialPost tstSocialPostWeChat1 = new SocialPost(
										Name = 'Test TW '+x,
										Provider = 'WeChat',
										Posted = postedDate,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat1;
			socialPostList.add(tstSocialPostWeChat1);
			
			SocialPost tstSocialPostWeChat2 = new SocialPost(
										Name = 'Test TW '+x,
										Provider = 'WeChat',
										Posted = postedDate,
										Handle ='TemporaryPersonAccountTest'
									);
			insert tstSocialPostWeChat2;
			socialPostList.add(tstSocialPostWeChat2);
				}
 			}
 		
 		
		return socialPostList;
	}
	
}