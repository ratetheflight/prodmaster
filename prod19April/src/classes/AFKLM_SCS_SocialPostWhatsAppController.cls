/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostWhatsAppController
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPosController
====================================================== 
Purpose: 
    Social Post Controller Class to utilize existing List View in Apex with Pagination Support with logic used for WhatsApp.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          16/09/2015      Initial Development
    1.1     Sai Choudhry        06/10/2016      Populating the engagement time. JIRA ST-1069.
    1.2     Sai Choudhry        01/11/2016      Populating Mark As reviewed date and Agent status. JIRA ST-1231 & ST-1232
    1.3     Pushkar             20/01/2017      A new agent status 'Engaged' in place of 'Mark as reviewed'. JIRA ST-1263
    
***********************************************************************/
public with sharing class AFKLM_SCS_SocialPostWhatsAppController {
    public String createCasesWhatsApp(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, String existingCaseNumber,List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();

        if(''.equals(existingCaseNumber)){
            existingCaseNumber = checkExistingCase(post,tempSocialHandle,existingCaseNumber);
        }

         while(SocPostSetController.getHasNext()) {
            SocPostSetController.next();
            for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {
                socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            }
        }
        System.debug('^^^existingCaseNumber '+existingCaseNumber);
        if(existingCaseNumber.equals('')) {
            //modified by sai. JIRA ST-1231 & ST-1232
            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed", "Engaged" OR "Actioned" and being handled by another agent. ' ) );
                return null;
            }
        
            post.SCS_Status__c = 'Engaged';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();
            //Added by sai. Populating the engagement time. JIRA ST-1069
            post.Engaged_Date__c = Datetime.now();
            listSocialPosts.add(post);
                
            postMap.put( post.Id, post );
            
            if( !postMap.IsEmpty() ) {
                returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
                Case personAccountId = [SELECT accountId FROM Case WHERE id =: returnedCaseId[0] LIMIT 1];
                if(returnedCaseId.size() > 0) {
                    newCaseId = returnedCaseId[0];
                    List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
                                    
                    // The non selected to merge by Handle   
                    for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                        //modified by sai. JIRA ST-1231 & ST-1232
                         if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && wrapperSocialPost.isselected == False) {
                            if(wrapperSocialPost.cSocialPost.ParentId == null) {
                                wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                            } 
                            
                            //Modified by sai. Populating Agent status. JIRA ST-1232.
                            wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark As Reviewed Auto';
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                            //Modified by sai. Populating Mark As reviewed date ST-1231.
                            wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                            wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                            wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                            wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                            
                            listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
                        }
                        
                    }
                    update listWrapperSocialPost;
                }
            
                if( !error ){
                    return newCaseId;
                }
            }
                        
       } else {
            newCaseId = existingCaseNumber;
            Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1];
            //modified by sai. JIRA ST-1231 & ST-1232
            if(('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || ('Engaged').equals(post.SCS_Status__c) || ('Mark as reviewed auto').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );
                return null;
            }
            List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
            
            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                //Added by Sai. Jira ST-1231 ans ST-1232
                //start
                if(wrapperSocialPost.isselected == True)
                {
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Engaged';
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                    wrapperSocialPost.cSocialPost.Engaged_Date__c = Datetime.now();
                }
                //end
                //modified by sai. JIRA ST-1231 & ST-1232
                if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && wrapperSocialPost.isselected == False) {
                    wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                    //Modified by sai. Populating Agent status. JIRA ST-1232.
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark As Reviewed Auto';
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    //Modified by sai. Populating Mark As reviewed date ST-1231.
                    wrapperSocialPost.cSocialPost.Mark_As_Reviewed_Date__c = Datetime.now();
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                    wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                }
                listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
            }
            update listWrapperSocialPost;
            
            return newCaseId;
            
        }
    return null;
    } 

    private String checkExistingCase(SocialPost post, String tempSocialHandle, String existingCaseNumber){
        List<Case> cs = new List<Case>();

        if(post.Persona != null && post.Persona.ParentId != post.Id){
            cs = [SELECT id FROM Case WHERE AccountId =: post.Persona.ParentId AND isClosed = false LIMIT 1];
        }

        System.debug('^^^Case '+cs);
        if(cs.size()>0 && !cs.IsEmpty()){
            //existingCaseNumber = cs[0].id;
            System.debug('^^^existingCaseNumber inside check '+existingCaseNumber);
            return cs[0].id;
        }

        return '';
    }  

}