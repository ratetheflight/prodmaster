@isTest
private class AFKLM_SCS_StatusTrackerTest {
	
	@isTest static void insertUpdateTest() {

		RecordType servicingRecType = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND Name = 'Servicing'];

		Case cs = new Case(
			Status = 'New',
			RecordTypeId = servicingRecType.Id
			);

		insert cs;

		Status_Tracker__c st = [SELECT id, Old_Status__c FROM Status_Tracker__c WHERE Case__c =:cs.Id LIMIT 1];

		System.assertEquals('New',st.Old_Status__c);


		//Update test

		cs.Status = 'In Progress';

		update cs;

		Status_Tracker__c updatedTracker = [SELECT id, Old_Status__c, New_Status__c FROM Status_Tracker__c WHERE Id =: st.Id LIMIT 1];
		System.assertEquals('New',updatedTracker.Old_Status__c);
		System.assertEquals('In Progress',updatedTracker.New_Status__c);
		
		Status_Tracker__c newInsertedTracker = [SELECT id, Old_Status__c, New_Status__c, Type__c FROM Status_Tracker__c WHERE Case__c=:cs.Id ORDER BY CreatedDate DESC LIMIT 1];
		//System.assertEquals('In Progress', newInsertedTracker.Old_Status__c);
		//System.assertEquals(null,newInsertedTracker.New_Status__c);
		//System.assertEquals('Handled', newInsertedTracker.Type__c);
	}

}