/*************************************************************************************************
* File Name     :   AFKLM_SCS_TierLevelDataBatch
* Description   :   This batch is used to populate Tier Level/Booking class data on account records provided
*                    FB# is already present on them.
* @author       :   
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     25/07/2016      Ata           Created this batch class
****************************************************************************************************/
global class AFKLM_SCS_TierLevelDataBatchDelta implements Database.Batchable<sobject>,
Database.AllowsCallouts,Database.Stateful {
    
    //Variable pool-------------------------
    //SFDC IP sent to CAPI as a part of authentication
    global final string sfdcIp;
    //Total# of accounts processed 
    global integer summary;
    // Total# of Updated accounts
    global integer processed;
    
    public string query = '';
    
    Map<String,Integer> constants= new Map<string,Integer>{'P' =>3,'C' => 2,'M' => 1};
    //--------------------------------------
    public class MyException extends Exception{}
    
    //constructor
    global AFKLM_SCS_TierLevelDataBatchDelta(){
    
      summary = 0;
      processed = 0;
      //get Salesforce IP from custom setting      
      AFKLM_SFDCIP__c ip = AFKLM_SFDCIP__c.getValues('Integration');
      
      if(ip != Null){
        sfdcIp = ip.SFDCIP__c;
      }
      else{
        throw new MyException('SFDC IP not found in the custom setting or wrong IP!');
      }     
    }
    
    global Database.Querylocator start(Database.BatchableContext BC){
    
        
    
        if(! test.isRunningTest()){
            if(query == ''){
                  query = 'Select id,Name,'+
                             ' Flying_Blue_Number__c,'+
                             ' personEmail,'+
                             ' Email_Flying_Blue__c,'+
                             ' tier_level__c,'+
                             ' Preferred_Language__c,'+
                             ' Booking_Date__c,'+
                             ' booking_class__c,'+
                             ' Country_of_Residence__c,'+
                             ' PersonMobilePhone,'+ 
                             ' Mobile_Flying_Blue__c,'+
                             ' Phone,'+ 
                             ' GIN__c'+
                             //' BillingStreet,'+
                             //' BillingPostalCode,'+
                             //' BillingCity,'+
                             //' BillingCountry'+
                             ' from Account'+
                             ' where (Flying_Blue_Number__c <> \'\' AND Flying_Blue_Number__c <> NULL AND Tier_level__c =\'\')';
                             //' where id = \'00120000016uc0G\'';
            }                 
        }
        else{
                     //string accId = '0017E00000BWZKX';
                     query = 'Select id,Name,'+
                             ' Flying_Blue_Number__c,'+
                             ' personEmail,'+
                             ' Email_Flying_Blue__c,'+
                             ' Preferred_Language__c,'+
                             ' tier_level__c,'+
                             ' Booking_Date__c,'+
                             ' booking_class__c,'+
                             ' Country_of_Residence__c,'+
                             ' PersonMobilePhone,'+ 
                             ' Mobile_Flying_Blue__c,'+
                             ' Phone,'+ 
                             ' GIN__c'+
                             //' BillingStreet,'+
                             //' BillingPostalCode,'+
                             //' BillingCity,'+
                             //' BillingCountry'+
                             ' from Account';
                             
        }
      
      return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sobject> scope){
      
      List<Account> accList =  new List<Account>();
      
      //setting summary size
      summary += scope.size();
      try{
          //iterating over all account records and set 
          //Booking class and tier level info.    
          for(Account acc : (List<Account>)scope){
             Boolean skipcallout = false;
             //flag to check if any field is updated on account
             //flag = true means account needs to be updated otherwise not.
             Boolean flag = false;
             Account tempAcc = new Account(id = acc.id);
             //creating Flying Blue Object
             AFKLM_WS_Manager.flyingBlueMember fbMember = new  AFKLM_WS_Manager.flyingBlueMember();
             //make callout
             try{ 
                  if(!Test.isRunningTest()){
                     fbMember = AFKLM_WS_Manager.searchOnFBNumber(acc.Flying_Blue_Number__c , sfdcIp);
                  }
                  else{
                     fbMember.tierLevel = 'Ivory';
                  }
             }
             catch(Exception ep){
                 skipcallout = true;
                 System.debug('Exception in batch:'+ acc.id+'--'+ep);
             }
             //Update Tier Leveldata on account
             //Update Email and Language on account : ST-004548
             /*
             ST-1334 changes
             ---------------
             Update Email(Flying Blue), Mobile (Flying Blue) - new fields created for values from the batch callouts to avoid existing field override
             Update GIN,Country of Residence - existing fields used.
       
             */
             
             if(fbMember != Null && !(skipcallout)){
                 if(fbMember.tierLevel != Null){
                    tempAcc.tier_level__c = fbMember.tierLevel;
                    flag = true;
                 }
                 if(fbMember.emailAddress != Null){
                    tempAcc.Email_Flying_Blue__c = fbMember.emailAddress;
                    flag = true;
                 }
                 if(fbMember.language != Null){
                    tempAcc.Preferred_Language__c = fbMember.language;
                    flag = true;
                 } 
                 if(fbMember.mobilePhone != Null){
                    tempAcc.Mobile_Flying_Blue__c = fbMember.mobilePhone;
                    flag = true;
                 }
                 /*if(fbMember.homePhone != Null){
                    tempAcc.Phone = fbMember.homePhone;
                    flag = true;
                 }*/
                 if(fbMember.country != Null){
                    tempAcc.Country_of_Residence__c = fbMember.country ;
                    flag = true;
                 }
                 if(fbMember.clientNumber != Null){
                    tempAcc.GIN__c = fbMember.clientNumber;
                    flag = true;
                 }
                    
             }
                     
             
             Map<String, AFKLM_WS_Manager.pnrEntry> mapOfKeyToPNR = new  Map<String, AFKLM_WS_Manager.pnrEntry>();
             
             try{
                if(!(skipcallout)){                  
                     if(!Test.isRunningTest()){
                        mapOfKeyToPNR = AFKLM_WS_Manager.getPNRListForFBMember(acc.Flying_Blue_Number__c);
                     }
                     else{
                         mapOfKeyToPNR = getFakePnrEntry();
                     }
                }
             }  
             catch(Exception ex){
                 
                 System.debug('Exception in batch for record:'+acc.id+'---'+ex);
             }    
             //if we get a response
             if(mapOfKeyToPNR != NUll && ! (mapOfKeyToPNR.isEmpty())){
                
                List<pnrWrapper> pnrList = new List<pnrWrapper>();
                
                for(AFKLM_WS_Manager.pnrEntry pnrLineItem: mapOfKeyToPNR.values()){
                    pnrList.add( new pnrWrapper(pnrLineItem)); 
                }
                
                pnrList.sort();
          
                Date dt = Date.today();
                Boolean prevDate;
                Boolean upcomingDate;
                integer index =0;
                Date tempDate;
                String PastBClass  = '';
                Date pastApiDate;
               
                //List of booking class data from integration 
                for(pnrWrapper entry: pnrList){
                    
                    string TDate = entry.PE.theDate.split(' ')[0];
                    List<string> dtComp = TDate.split('-');
                    //creating date instance from booking date field that came thro' integration
                    Date apiDate = Date.newinstance(Integer.valueof(dtComp[2]),Integer.valueof(dtComp[1]),Integer.valueof(dtComp[0]));  
                    
                    
                    if(dt == apiDate){
                        //today's booking found    
                        tempAcc.booking_class__c = entry.PE.theClass;
                        tempAcc.Booking_Date__c = apiDate;
                        flag = true;
                        tempDate = apiDate;
                        index++;
                        break;
                    }
                    else if(dt > apiDate){
                        //old date booking info. found
                        //system.debug('comparision:11:'+pastApiDate+'---'+tempAcc.Booking_Date__c);
                        
                        if(tempAcc.booking_class__c == Null)
                            tempAcc.booking_class__c = entry.PE.theClass;
                        
                        if(tempAcc.Booking_Date__c == Null)
                            tempAcc.Booking_Date__c = apiDate;
                        
                        PastBClass = entry.PE.theClass; 
                        pastApiDate = apiDate;
                        
                        if(tempAcc.Booking_Date__c < pastApiDate){
                            tempAcc.Booking_Date__c = pastApiDate;
                            tempAcc.booking_class__c = entry.PE.theClass;
                        
                        }
                        
                        if(pastApiDate == tempAcc.Booking_Date__c){ 
                            if(constants.get(PastBClass) > constants.get(tempAcc.booking_class__c)){
                                tempAcc.booking_class__c = PastBClass;
                               
                            }
                             
                        }
                        
                        flag = true;
                        index++;
                        //system.debug('comparision:22:'+pastApiDate+'---'+tempAcc.Booking_Date__c);
                    }
                    else if(dt < apiDate){
                        //future day booking is found
                        tempAcc.booking_class__c = entry.PE.theClass; 
                        tempAcc.Booking_Date__c = apiDate;
                        tempDate = apiDate;
                        flag = true;
                        index++;
                        break;
                    }
                
                }
                
                List<pnrWrapper> pwrap = pnrList;
                List<AFKLM_WS_Manager.pnrEntry> pEntry = new List<AFKLM_WS_Manager.pnrEntry>();
                
                for(pnrWrapper p: pnrList){
                    pEntry.add(p.PE);
                }
                
                //List<AFKLM_WS_Manager.pnrEntry> pEntry = mapOfKeyToPNR.values();
                //system.debug('ABCD::::'+Index+'------'+pEntry.size());
                if(index != pEntry.size()){
                    for( ; index < pEntry.size() ; index++){
                        
                        string TDate = pEntry[index].theDate.split(' ')[0];
                        List<string> dtComp = TDate.split('-');
                        //creating date instance from booking date field that came thro' integration
                        Date apiDate = Date.newinstance(Integer.valueof(dtComp[2]),Integer.valueof(dtComp[1]),Integer.valueof(dtComp[0]));  
                        system.debug('pnrdata:'+pEntry[index]);
                        if(tempDate == apiDate){
                            if(constants.get(tempAcc.booking_class__c) < constants.get(pEntry[index].theClass)){
                                tempAcc.booking_class__c = pEntry[index].theClass;
                            }
                            //index++;
                        }
                        else{
                            break;
                        }
                    }
                }    
             }
             
             if(flag)
                accList.add(tempAcc);
          }
          
          system.debug('acclist:::'+accList);
          //update Account list
          if(accList.size() > 0)
            update accList;
          
          // No. of accounts updated
          processed += accList.size();
      } 
      catch(Exception e){
        System.debug('System Exception::: '+e);
        
      }  
    }

    global void finish(Database.BatchableContext BC){
    
    //Send email after batch completion to the person who scheduled it.        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String [] toaddress=new string[]{UserInfo.getUserEmail()};
        String[] ccAddresses=new String[]{UserInfo.getUserEmail()};
        email.setSubject('Tier Level Batch Status');
        email.setHtmlBody('Dear Sir /Madam,<br/><br/> Batch status is as follows:<br/> Total Accounts processed: '+ summary+'<br/> Total Updated Accounts: '+ processed+'<br/><br/>Sincerely,<br><br/><br/>Date :'+string.valueof(system.today()));
        email.setToAddresses(toaddress);
        email.setCcAddresses(ccAddresses);
        Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
    }
    
    //Method added to leverage the testing process
    private Map<String, AFKLM_WS_Manager.pnrEntry> getFakePnrEntry(){
        
        Map<String, AFKLM_WS_Manager.pnrEntry> pEntryMap = new Map<String, AFKLM_WS_Manager.pnrEntry>();
        
        AFKLM_WS_Manager.pnrEntry pEntry4 = new AFKLM_WS_Manager.pnrEntry();
        //pEntry4.theDate = Date.Today().day()+'-'+Date.Today().month()+'-'+Date.Today().year();
        pEntry4.theDate = (Date.Today()-1).day()+'-'+(Date.Today()-1).month()+'-'+(Date.Today()-1).year();
        pEntry4.theClass = 'C';
        
        pEntryMap.put('4',pEntry4);
        
        AFKLM_WS_Manager.pnrEntry pEntry5 = new AFKLM_WS_Manager.pnrEntry();
        //pEntry4.theDate = Date.Today().day()+'-'+Date.Today().month()+'-'+Date.Today().year();
        pEntry5.theDate = (Date.Today()-1).day()+'-'+(Date.Today()-1).month()+'-'+(Date.Today()-1).year();
        pEntry5.theClass = 'P';
        
        pEntryMap.put('5',pEntry5);
        
        AFKLM_WS_Manager.pnrEntry pEntry = new AFKLM_WS_Manager.pnrEntry();
        pEntry.theDate = Date.Today().day()+'-'+Date.Today().month()+'-'+Date.Today().year();
        //pEntry.theDate = (Date.Today()-1).day()+'-'+(Date.Today()-1).month()+'-'+(Date.Today()-1).year();
        pEntry.theClass = 'M';
        
        pEntryMap.put('1',pEntry);
        
        AFKLM_WS_Manager.pnrEntry pEntry2 = new AFKLM_WS_Manager.pnrEntry();
        pEntry2.theDate = Date.Today().day()+'-'+Date.Today().month()+'-'+Date.Today().year();
        //pEntry2.theDate = (Date.Today()-1).day()+'-'+(Date.Today()-1).month()+'-'+(Date.Today()-1).year();
        pEntry2.theClass = 'P';
        
        pEntryMap.put('2',pEntry2);
        
        AFKLM_WS_Manager.pnrEntry pEntry3 = new AFKLM_WS_Manager.pnrEntry();
        pEntry3.theDate = Date.Today().day()+'-'+Date.Today().month()+'-'+Date.Today().year();
        //pEntry3.theDate = (Date.Today()-1).day()+'-'+(Date.Today()-1).month()+'-'+(Date.Today()-1).year();
        pEntry3.theClass = 'C';
        
        pEntryMap.put('3',pEntry3);
        
        return pEntryMap;
    }

}