/**
 * @author (s)      : Prasanna Kumar
 * @requirement id  : 
 * @description     : Scheduler class for the AFKLM_SCS_TierLevelDataBatchDelta
 * @log:            : 08/11/2016
 */
global class AFKLM_SCS_TierLevelScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    AFKLM_SCS_TierLevelDataBatchDelta tierdelta = new AFKLM_SCS_TierLevelDataBatchDelta();
    Database.executeBatch(tierdelta, 50);
  }
}