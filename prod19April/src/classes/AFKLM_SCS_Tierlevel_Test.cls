/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AFKLM_SCS_Tierlevel_Test {

    static testMethod void myUnitTest() {
        
        id accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('SCS Person Account').getRecordTypeId();
        AFKLM_SFDCIP__c abc = new AFKLM_SFDCIP__c();
        abc.name= 'Integration';
        abc.SFDCIP__c = '101.102.202.9';
        insert abc;
        
        List<Account> accList = new List<Account>();
        for(integer i=0;i<20;i++){
            
            Account acc = new Account();
            acc.Flying_Blue_Number__c = String.valueof(232356665 + i);
            acc.LastName = 'XYZ'+i;
            acc.recordtypeID = accRecordType;
            acc.tier_level__c='Ivory';
            accList.add(acc);
        }
        
        insert accList;
        
        
        Test.StartTest();
            
            //Test.setMock(WebServiceMock.class, new AFKLM_SCS_BookingClass_MockTest());
            //Test.setMock(WebServiceMock.class, new AFKLM_SCS_TierData_MockTest());
            AFKLM_SCS_TierLevelDataBatch tLevelBatch = new AFKLM_SCS_TierLevelDataBatch();
            Database.executebatch(tLevelBatch);
           AFKLM_SCS_UpdateTierLevelDummyBatch dummyBatch=new AFKLM_SCS_UpdateTierLevelDummyBatch();
           Database.executebatch(dummyBatch);
           AFKLM_SCS_TierLevelScheduler tiersch=new AFKLM_SCS_TierLevelScheduler();
           tiersch.execute(null);
        Test.StopTest();
            
    }
}