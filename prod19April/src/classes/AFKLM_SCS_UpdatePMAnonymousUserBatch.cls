/*************************************************************************************************
* File Name     :   AFKLM_SCS_UpdatePMAnonymousUserBatch
* Description   :   This batch class is used to update the anonymous users related to the private messages
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     10/05/2016      Nagavi         Created the class
****************************************************************************************************/
global class AFKLM_SCS_UpdatePMAnonymousUserBatch implements Database.Batchable<sObject>{
    
    global final String Query;
    
    global AFKLM_SCS_UpdatePMAnonymousUserBatch(String q){
        Query=q;          
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<FB_vcard_Temp__c> scope){
    
        List<string> commerceTokens=new List<string>(); 
        Map<string,FB_vcard_Temp__c> tokenMap=new Map<string,FB_vcard_Temp__c>();
        List<SocialPersona> relatedPersona=new List<SocialPersona>();   
        List<SocialPersona> personaToBeUpdated=new List<SocialPersona>();
        List<Id> relatedAccountIds=new List<Id>();
        Map<Id,FB_vcard_Temp__c> relatedAccountMap=new Map<Id,FB_vcard_Temp__c>();
        List<Account> relatedAccountList=new List<Account>();
        List<Account> accountToBeUpdated=new List<Account>();
        List<FB_vcard_Temp__c> vCardToBeUpdated=new List<FB_vcard_Temp__c>();
        
        for(FB_vcard_Temp__c vcard:scope){
            commerceTokens.add('ott:facebook:'+vcard.Commercetoken__c);
            tokenMap.put('ott:facebook:'+vcard.Commercetoken__c,vcard);

        }
        
        //Query the related persona
        relatedPersona=[select id,ProfileUrl,ASID_Social_Provider__c,ExternalId,Name,RealName,ParentId from SocialPersona where ExternalId IN:commerceTokens];
        
        //Process the persona
        if(!relatedPersona.isEmpty()){
            for(SocialPersona persona:relatedPersona){
                if(tokenMap.containsKey(persona.ExternalId)){
                    relatedAccountIds.add(persona.ParentId);
                    relatedAccountMap.put(persona.ParentId,tokenMap.get(persona.ExternalId));
                    
                    FB_vcard_Temp__c vCardTemp=tokenMap.get(persona.ExternalId);
                    
                    //Update the required fields
                    persona.Name=vCardTemp.FirstName__c+' '+vCardTemp.LastName__c;
                    persona.RealName=vCardTemp.FirstName__c+' '+vCardTemp.LastName__c;
                    if(vCardTemp.KlmAppScopedId__c!=null || vCardTemp.KlmAppScopedId__c!=''){
                        persona.ExternalId=vCardTemp.KlmAppScopedId__c;
                        persona.ProfileUrl='https://facebook.com/'+vCardTemp.KlmAppScopedId__c;
                    }
                    if(vCardTemp.Radian6AppScopedId__c!=null || vCardTemp.Radian6AppScopedId__c!='')
                        persona.ASID_Social_Provider__c=vCardTemp.Radian6AppScopedId__c;
                                        
                    persona.ExternalPictureURL=vCardTemp.Image__c;
                    
                    //Update vcard records
                    vCardTemp.Is_Processed__c=true;
                    vCardToBeUpdated.add(vCardTemp);
                    
                    personaToBeUpdated.add(persona);
                }
        
            }
        }
        
        //Query and process the related accounts
        if(!relatedAccountIds.isEmpty()){
            relatedAccountList=[Select Id ,FirstName,LastName,sf4twitter__Fcbk_User_Id__pc from Account where Id IN:relatedAccountIds];
            
            if(!relatedAccountList.isEmpty()){
                for(Account acc:relatedAccountList){
                    FB_vcard_Temp__c temp=relatedAccountMap.get(acc.Id);
                
                    //Update the account fields
                    acc.FirstName=temp.FirstName__c;
                    acc.LastName= temp.LastName__c;
                    acc.sf4twitter__Fcbk_Username__pc=temp.FirstName__c+' '+temp.LastName__c;
                    if(temp.Radian6AppScopedId__c!=null && temp.Radian6AppScopedId__c!='')
                        acc.sf4twitter__Fcbk_User_Id__pc='FB_'+temp.Radian6AppScopedId__c;
                    else
                        acc.sf4twitter__Fcbk_User_Id__pc='FB_'+temp.KlmAppScopedId__c;

                    accountToBeUpdated.add(acc);
                }
            }
        }
        
        if(!personaToBeUpdated.isEmpty())
            update personaToBeUpdated;
            
        if(!accountToBeUpdated.isEmpty())
            update accountToBeUpdated;
            
        if(!vCardToBeUpdated.isEmpty())
            update vCardToBeUpdated;
        
        
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
 }