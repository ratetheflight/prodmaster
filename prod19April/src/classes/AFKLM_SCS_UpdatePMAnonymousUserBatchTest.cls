/* 
*******************************************************************************************************************************************
* File Name     :   AFKLM_SCS_UpdatePMAnonymousUserBatchTest
* Description   :   Test Class for AFKLM_SCS_UpdatePMAnonymousUserBatch
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    19/04/2016    Nagavi        Created the class.
******************************************************************************************************************************************* 
*/
@isTest
private class AFKLM_SCS_UpdatePMAnonymousUserBatchTest{
    
    static testmethod void AUserUpdateTest() {
        
        FB_vcard_Temp__c vcard=new FB_vcard_Temp__c();
        vcard.Commercetoken__c='yui546';
        vcard.FirstName__c='Test Anono';
        vcard.LastName__c='User';
        vcard.KlmAppScopedId__c='890576';
        vcard.Radian6AppScopedId__c='324546';
        vcard.Image__c='https://fbook.com/yui';
        insert vcard;
        
        Account acc=new Account();
        //acc.recordtypeId='01220000000AFcl';
        acc.LastName='Test Account 1';
        acc.flying_blue_number__c='6789';
        insert acc;
                             
        List<SocialPersona> accPersona=new List<SocialPersona>();
        accPersona=AFKLM_TestDataFactory.insertpersona(1,acc.Id);
        accPersona[0].ExternalId='ott:facebook:yui546';
        insert accPersona[0];
        
        Test.startTest();
        //string query='select id,Commercetoken__c,FirstName__c,Image__c,Is_Processed__c,KlmAppScopedId__c,LastName__c,Radian6AppScopedId__c from FB_vcard_Temp__c';       
        AFKLM_SCS_UpdatePMAnonymousUserBatch batch= new AFKLM_SCS_UpdatePMAnonymousUserBatch('select id,Commercetoken__c,FirstName__c,Image__c,Is_Processed__c,KlmAppScopedId__c,LastName__c,Radian6AppScopedId__c from FB_vcard_Temp__c');
         
        Database.executeBatch(batch,100);     
        Test.stopTest();
    }    
    
 }