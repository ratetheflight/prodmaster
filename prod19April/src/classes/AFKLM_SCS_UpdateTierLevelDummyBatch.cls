global class AFKLM_SCS_UpdateTierLevelDummyBatch implements Database.Batchable<sobject> {
global string query;
   
global Database.Querylocator start(Database.BatchableContext BC){
  
  query = 'Select id,'+
                             ' tier_level__c,'+
                             ' Tier_Level_dummy__c'+
                             ' from Account'+
                             ' where (tier_level__c <>\'\' AND Tier_level_dummy__c = \'\')'; 
                             system.debug('queried info:'+query);
                             
    return Database.getQueryLocator(query);
    
    }
global void execute(Database.BatchableContext BC, List<sobject> scope){
      
      List<Account> accList =  new List<Account>();
    
      for(Account acc : (List<Account>)scope){
      acc.tier_level_dummy__c=acc.tier_level__c;
      accList.add(acc);
      }
      system.debug('account list:'+accList);
      try{
      update accList;
      }
      catch(Exception e){
      System.debug('Issue while doing update'+e);
      }
    }
global void finish(Database.BatchableContext BC){}

}