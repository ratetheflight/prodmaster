global class AFKLM_SCS_UpdateinfluencerscorenewBatch implements Database.Batchable<sobject> {
global string query;
   
global Database.Querylocator start(Database.BatchableContext BC){
  
  query = 'Select id,'+
                             ' influencer_score__c,klout_score__c,'+
                             ' influencer_score_new__c'+
                             ' from Account'+
                             ' where (klout_score__c <> null)'; 
                             system.debug('queried info:'+query);
                             
    return Database.getQueryLocator(query);
    
    }
global void execute(Database.BatchableContext BC, List<sobject> scope){
      
      List<Account> accList =  new List<Account>();
    
      for(Account acc : (List<Account>)scope){
      if(acc.klout_score__c <=49){
      acc.influencer_score_new__c='1';}
      else if(acc.klout_score__c >=50 && acc.klout_score__c <=59){
      acc.influencer_score_new__c='2';}
      else if(acc.klout_score__c >=60 && acc.klout_score__c <=69){
      acc.influencer_score_new__c='3';}
      else if(acc.klout_score__c>=70 && acc.klout_score__c <=100){
      acc.influencer_score_new__c='4';}
           
     // acc.influencer_score_new__c=String.valueof(acc.influencer_score__c);
      accList.add(acc);
      }
      system.debug('account list:'+accList);
      try{
      update accList;
      }
      catch(Exception e){
      System.debug('Issue while doing update'+e);
      }
    }
global void finish(Database.BatchableContext BC){}

}