public with sharing class AFKLM_SCS_UserSkipValidationSingleton {
    private static final AFKLM_SCS_UserSkipValidationSingleton classInstance = new AFKLM_SCS_UserSkipValidationSingleton();

    public List<User> getUsr{get;private set;}

    private AFKLM_SCS_UserSkipValidationSingleton() {
        getUsr = [SELECT SkipValidation__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];
    }

    public static AFKLM_SCS_UserSkipValidationSingleton getInstance() {
    	
        return classInstance;
    }
}