/* 
*******************************************************************************************************************************************
* File Name     :   AFKLM_SCS_iPOBControllerTest
* Description   :   Test Class for AFKLM_iPad_iPOBPageController and AFKLM_ipad_chatterAndTaskUpdate
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver       Date            Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0       24/10/2016      Narmatha        Created the class.
******************************************************************************************************************************************* 
*/

@isTest
private class AFKLM_SCS_iPOBControllerTest{
    
    //TestMethod 1 - Fixed Date code coverage part 
    static testMethod void ipobPage() {
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('2016825763');
        insert insertaccount;
       
        iPad_on_board__c ipad=new iPad_on_board__c();
        ipad.When_close_comment__c='Fixed Date';
        ipad.PersonAccount__c=insertaccount.id;
        insert ipad;
        
        Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());
        ApexPages.StandardController sc = new ApexPages.standardController(ipad); 
        AFKLM_iPad_iPOBPageController ipob=new AFKLM_iPad_iPOBPageController(sc);
        ipob.pageURL='hell test';
        ipob.dosave();
        ipob.cancel();
        ipob.tohideJourneys();
        ipob.toshowjourneys();
        ipob.doSaveandNew();
    }
    
    //Testmethod 2 - Valid for Specific Flight coverage part
    static testMethod void ipobPage2() {
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('2016825763');
        insert insertaccount;
        
       iPad_on_board__c ipad=new iPad_on_board__c();
       ipad.When_close_comment__c='Valid for Specific Flight';
       ipad.PersonAccount__c=insertaccount.id;
        //insert ipad;
       Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());
       ApexPages.StandardController sc = new ApexPages.standardController(ipad); 
       AFKLM_iPad_iPOBPageController ipob=new AFKLM_iPad_iPOBPageController(sc);
       ipob.pageURL='searchGatewaypage';
       AFKLM_WS_Manager.pnrEntry ob=new AFKLM_WS_Manager.pnrEntry();
       ob.departureDate=system.today();
       ob.departureDateTime=System.now();
       ob.departureDateTimeString='29-10-2016 09:00';
       ob.selectedFlight=True;
       ob.theArrival='WEH';
       ob.theClass='M';
       ob.theDeparture='AMS';
       ob.theFlightNumber='0Z12';
       system.debug('Hello'+ob);
       ipob.mapOfKeyToPNR.put('Test123',ob);
       ipob.dosave();
       ipob.toshowjourneys();
       ipob.cancel();
    }
    
    //Testmethod 3 - Valid for Next Flight Code coverage
    static testMethod void ipobPage3() {
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('2016825763');
        insert insertaccount;
        List<Case> caseList= new List<Case>();
       /* Case cs=new Case();
        cs.status='New';
        cs.AccountID=insertAccount.id;    
        cs.RecordTypeid='01220000000Uann';
        insert cs;*/
        
        caseList=AFKLM_TestDataFactory.insertiPadonBoardCase(1);
        caseList[0].accountID=insertaccount.id;
        insert caseList;  
        iPad_on_board__c ipad=new iPad_on_board__c();
        ipad.When_close_comment__c='Valid for Next Flight';
        ipad.PersonAccount__c=insertaccount.id;
        ipad.Case__c=caseList[0].id;
        insert ipad;
        AFKLM_TestDataFactory.insertWSMangerSetting();
        Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());
          
        ApexPages.StandardController sc = new ApexPages.standardController(ipad); 
        AFKLM_iPad_iPOBPageController ipob=new AFKLM_iPad_iPOBPageController(sc);
        Test.StartTest();
        ipob.pageURL='searchGateway';
        ipob.dosave();
        ipob.tohideJourneys();
        ipob.toshowjourneys();
        ipob.doSaveandNew();
        ipad.iPoBComment__c='Test';
        ipob.doSave();
        ipob.cancel();
        Test.StopTest();
    }
   
    //Testmethod 4 - Account's Flying Blue number empty
    static testMethod void ipobPage4() {
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('');
        insert insertaccount;
        iPad_on_board__c ipad=new iPad_on_board__c();
        ipad.When_close_comment__c='Valid for Next Flight';
        ipad.PersonAccount__c=insertaccount.id;
        insert ipad;
        AFKLM_TestDataFactory.insertWSMangerSetting();
        Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());
   
        ApexPages.StandardController sc = new ApexPages.standardController(ipad); 
        AFKLM_iPad_iPOBPageController ipob=new AFKLM_iPad_iPOBPageController(sc);
        ipob.pageURL='Test';
        ipob.dosave();
    }
   
   //TestMethod 5 - Vaild for Specific Flight with selected Flight
    static testMethod void ipobPage5() {
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('2016825763');
        insert insertaccount;
        
        iPad_on_board__c ipad=new iPad_on_board__c();
        ipad.When_close_comment__c='Valid for Specific Flight';
        ipad.PersonAccount__c=insertaccount.id;
        //insert ipad;
        Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());
        ApexPages.StandardController sc = new ApexPages.standardController(ipad); 
        AFKLM_iPad_iPOBPageController ipob=new AFKLM_iPad_iPOBPageController(sc);
        ipob.pageURL='searchGatewaypage';
        AFKLM_WS_Manager.pnrEntry ob=new AFKLM_WS_Manager.pnrEntry();
        ob.departureDate=system.today();
        ob.departureDateTime=System.now();
        ob.departureDateTimeString='29-10-2016 09:00';
        ob.selectedFlight=false;
        ob.theArrival='WEH';
        ob.theClass='M';
        ob.theDeparture='AMS';
        ob.theFlightNumber='0Z12';
        ipob.mapOfKeyToPNR.put('Test123',ob);
        ipob.dosave();
        ipob.toshowjourneys();
        ipob.cancel();
    }
    
    //TestMethod 6 - To update the comment with Purser reply(AFKLM_ipad_chatterAndTaskUpdate)
    static testMethod void ipobPage6() {
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('2016825763');
        insert insertaccount;
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertiPadonBoardCase(1);
        caseList[0].accountID=insertaccount.id;
        insert caseList;    
        iPad_on_board__c ipad=new iPad_on_board__c();
        ipad.When_close_comment__c='Close by reply purser';
        ipad.PersonAccount__c=insertaccount.id;
        ipad.case__c=caseList[0].id;
        //insert ipad;
        //Test.setMeock(WebServiceMock.class, new AFKLM_WS_MockTest());
        ApexPages.StandardController sc = new ApexPages.standardController(ipad); 
        AFKLM_iPad_iPOBPageController ipob=new AFKLM_iPad_iPOBPageController(sc);
        ipob.pageURL='Test';
        ipad.Purser_reply__c='Test Reply';
        ipob.doSave();
    }
    
    //TestMethod 7 - To update the comment with Purser reply(AFKLM_ipad_chatterAndTaskUpdate)
    static testMethod void ipobPage7() {
        Account insertaccount=new Account();
        insertaccount=AFKLM_TestDataFactory.createPersonAccount('2016825763');
        insert insertaccount;
          
        iPad_on_board__c ipad=new iPad_on_board__c();
        ipad.When_close_comment__c='Close by reply purser';
        ipad.PersonAccount__c=insertaccount.id;
        
        //insert ipad;
        //Test.setMeock(WebServiceMock.class, new AFKLM_WS_MockTest());
        ApexPages.StandardController sc = new ApexPages.standardController(ipad); 
        AFKLM_iPad_iPOBPageController ipob=new AFKLM_iPad_iPOBPageController(sc);
        ipob.pageURL='Test';
        ipad.Purser_reply__c='Test Reply';
        ipob.doSave();
    }

}