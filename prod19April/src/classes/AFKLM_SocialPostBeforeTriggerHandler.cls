/*************************************************************************************************
* File Name     :   AFKLM_SocialPostBeforeTriggerHandler 
* Description   :   Helper class for the Social Post trigger - Before Insert
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     10/02/2016      Nagavi         Created the class
* 1.1     10/18/2016      Nagavi         Added logic to Switch On/OFF Urgent Case, added more filters ST- 1108,1126
* 1.2     11/30/2016      Nagavi         Added Logic to include IGT Private messages in Urgent Auto - ST:1295  
* 1.3     01/24/2017      Sathish        Added logic to include Language in keywords and according to language in post the keywords will be checked.ST-1553
* 1.4     07/02/2017      Sai            Updating the Last outbound message related to the post. JIRA ST-1199
* 1.5     17/02/2017      Nagavi         Opened up all languages for facebook and its private messages
* 1.6     22/02/2017      Sai            Opnend urgency for Twitter. JIRA ST-1752.
****************************************************************************************************/
Public class AFKLM_SocialPostBeforeTriggerHandler {
    
    //Boolean to prevent recursion
    public static boolean onBeforeInsert_FirstRun = true;
    public static boolean onBeforeUpdate_FirstRun = true;
    public static boolean isParentUpdated = true;

    //Is Case Urgency Automation Switched On
    public boolean isSwitchedOn = false;
      
    
    //Method for after insert event
    public void onBeforeInsert(List<SocialPost> objectsToInsert)
    {
        if(onBeforeInsert_FirstRun)
        {
            // PREVENT RECURSION
            onBeforeInsert_FirstRun = false;
            
            /************* Keyword Search Logic - START *********************************/
            AFKLM_SocialCustomerService__c checkUrgent = AFKLM_SocialCustomerService__c.getValues('Settings');
            if(checkUrgent!=null){
                isSwitchedOn=checkUrgent.IsUrgencyRunning__c; 
            } 
            if(isSwitchedOn){
                List<Keywords_Library__c> keyWordsLibList=new List<Keywords_Library__c>();
                set<String> keywordsList=new set<String>();
                List<Id> relatedCaseIdsList=new List<Id>();
                List<Case> relatedCasesList=new List<Case>();
                Map<string,set<string>> languageKeywordMap=new map<string,set<string>>();
                Set<string> postlanguage=new set<string>();
                //1.3 adding language from social post to get the Keywords for those Languages
                    for(socialpost sp:objectsToInsert)
                    {
                        if(sp.PostTags!=NULL)
                        {
                            if(sp.PostTags=='English' || sp.PostTags=='IGT English')
                                postlanguage.add('English'); 
                            else
                                postlanguage.add(sp.PostTags);   
                        }
                        postlanguage.add('All');
                       
                    }
                //Get the list of keywords from object - Keywords Library
                //1.3 Passing language set to get the keywords matching with that languages
                    keyWordsLibList= getKeyWordsList(postlanguage);
                    
                    //1.3 commented to the code as we are directly taking the object list for iteration @ line 82
                    if(!keyWordsLibList.isEmpty()){
                        for(Keywords_Library__c key:keyWordsLibList){
                            if(languageKeywordMap.containskey(key.Language__c))
                                languageKeywordMap.get(key.Language__c).add(key.Keyword__c);
                            else
                                languageKeywordMap.put(key.Language__c,new set<String>{key.Keyword__c});
                        }
                        //system.debug('languageKeywordMap '+languageKeywordMap);
                        // Add the current month in the list
                        String currentMonth=system.now().format('MMMM');
                        
                        if(languageKeywordMap.containskey('English'))
                            languageKeywordMap.get('English').add(currentMonth);
                        else
                            languageKeywordMap.put('English',new set<String>{currentMonth});
                    }
                
                
                
                //Logic for the keyword search and to propose the post as urgent
                if(!keyWordsLibList.isEmpty()){
                    for(SocialPost sp:objectsToInsert) {
                        //system.debug('**sp.content'+sp.content);
                        //Modified by Sai. Added Twitter to the condition. JIRA ST-1752
                        if(sp.content!=null && sp.IsOutbound!=true && (sp.Provider=='Facebook' || sp.Provider == 'Twitter')){
                            keywordsList= new set<string>();
                            if(sp.PostTags!=null){
                                if(!(sp.PostTags).equalsIgnoreCase('unsupported') && !(sp.PostTags).equalsIgnoreCase('unpublished'))
                                {
                                 if(sp.PostTags=='English' || sp.PostTags=='IGT English'){
                                    if(languageKeywordMap.containskey('English'))
                                        keywordsList=languageKeywordMap.get('English');
                                 }
                                 else{
                                    if(languageKeywordMap.containskey(sp.PostTags))
                                        keywordsList=languageKeywordMap.get(sp.PostTags);
                                 }
                                }
                                
                                if(!keywordsList.IsEmpty())
                                    keywordsList.addAll(languageKeywordMap.get('All'));
                                else
                                    keywordsList=languageKeywordMap.get('All');
                                //if(isEligible(sp)){  //To check the eligibility of the post
                                 //1.3 Iterating the Keyword object list to match the language and keywords
                                 //Starts
                                //system.debug('keywordsList'+keywordsList);
                                if(!keywordsList.IsEmpty()){
                                    for(String str:keywordsList){
                                       
                                        if(keywordSearch(sp.content,str)){ 
                                            sp.Proposed_Urgent_Keyword__c=str; 
                                            if(sp.parentId!=null){
                                                //system.debug('sp.parentId '+sp.parentId);
                                                relatedCaseIdsList.add(sp.parentId);
                                                Break;
                                            }    
                                            
                                        }
                                        
                                    }
                                }
                                //1.3Ends
                            }
                        }        
                    }
                }
                //Logic to update the urgent flag in case object
                if(!relatedCaseIdsList.isEmpty()){
                    relatedCasesList=[Select id,Is_Urgent__c,Proposed_Urgent__c from case where id IN:relatedCaseIdsList AND Proposed_Urgent__c=FALSE];  
                    if(!relatedCasesList.isEmpty()){
                        for(Case cs:relatedCasesList){
                            cs.Proposed_Urgent__c=true;
                        }
                        //system.debug('relatedCasesList '+relatedCasesList);
                        update relatedCasesList;
                        //system.debug('after update relatedCasesList '+relatedCasesList);
                    }  
                }
            }   
            /************** Keyword Search Logic - END ***************************/
        }
    }
    
    //Method for after update event
    public void onBeforeUpdate(List<SocialPost> oldSPs,List<SocialPost> objectsToUpdate,Map<Id,SocialPost> oldSPMap,Map<Id,SocialPost> objectsToUpdateMap)
    {
         List<Id> relatedCaseIdsList=new List<Id>();
         List<Case> relatedCasesList=new List<Case>();
          
         if(isParentUpdated){
            for(SocialPost sp:objectsToUpdate) {
              
                if(oldSPMap.get(sp.Id).ParentId==null && sp.ParentId!=null && sp.Proposed_Urgent_Keyword__c!=null && sp.IsOutbound!=true){
                    //system.debug('relatedCaseIdsList +relatedCaseIdsList');
                    relatedCaseIdsList.add(sp.ParentId);
                }
            }
            if(!relatedCaseIdsList.isEmpty()){
                relatedCasesList=[Select id,Is_Urgent__c,Proposed_Urgent__c from case where id IN:relatedCaseIdsList AND Proposed_Urgent__c=FALSE];  
                if(!relatedCasesList.isEmpty()){
                    for(Case cs:relatedCasesList){
                        cs.Proposed_Urgent__c=true;
                    }
                     
                    update relatedCasesList;
                    isParentUpdated=false;
                }  
            }
         }
    }
    
    
    //Method to query for the keyword from the Keyword Library
    //1.3 Modified the query to filter the language
    Public static List<Keywords_Library__c> getKeyWordsList(set<String> Language){
        return [Select id, Keyword__c,Keyword_Type__c,Language__c from Keywords_Library__c where Language__c IN:Language limit 50000];
    }
    
    //Method to searh for the keyword in the post content
    Public static Boolean keywordSearch(String input, String keywordPhrase)
    {
        //return Pattern.compile('(?i)\\b\\w' + keywordPhrase + '\\b\\w').matcher(input).find();
        Pattern startsWordBoundary = Pattern.compile('^\\b'); // ^ matches start of string
        Pattern endsWordBoundary = Pattern.compile('\\b$');   // $ matches end of string
        
        String expression = '(?i)'; // To make it case insensitive

        if (startsWordBoundary.matcher(keywordPhrase).find()) expression += '\\b';
            
            expression += escapeExpression(keywordPhrase);
            
        if (endsWordBoundary.matcher(keywordPhrase).find()) expression += '\\b';

        return Pattern.compile(expression).matcher(input).find();
    }
    
    //Method to replace the special characters
    public static String escapeExpression(String phrase)
    {
        //system.debug('phrase'+phrase.replaceAll('([^\\w\\s])', '\\\\$0'));
        return phrase.replaceAll('([^\\w\\s])', '\\\\$0');
    }
    
    //Method to check the eligibility for urgent case auto
    /*public static Boolean isEligible(SocialPost sp)
    {
        Boolean temp=false;
        
        //Criteria for Cygnific
        if((sp.PostTags=='English' || sp.PostTags =='Dutch') && (sp.TopicProfileName =='KLM' || sp.TopicProfileName =='KLM Test' || sp.TopicProfileName =='KLM Dutch Caribbean' || sp.TopicProfileName =='KLM Suriname')){
            temp=true;
            return temp;
        }  
        
        //Criteria for IGT 
        if(sp.PostTags =='IGT English' || (sp.PostTags =='English' && (sp.TopicProfileName !='KLM' || sp.TopicProfileName !='KLM Dutch Caribbean' || sp.TopicProfileName !='KLM Suriname'))){
            temp=true;
            return temp;
        }
        
        return temp;
    }*/
    //Function to find the lastoutbound message. JIRA ST-1199
    //Start
    public void findLastOutBound(List<socialpost> sPost)
    {
        if(onBeforeUpdate_FirstRun )
        {
            onBeforeUpdate_FirstRun = False;
            Map<ID, List<SocialPost>> inboundPostMap= new Map<ID, List<SocialPost>>();
            Map<ID,SocialPost> outBoundPostMap = new Map<ID,SocialPost>();
    
            for(socialpost newPosts : sPost)
            {
                List<SocialPost> inboundList = new List<SocialPost>();
                if(inboundPostMap.containskey(newPosts.parentid))
                {
                    inboundPostMap.get(newPosts.parentid).add(newPosts);
                      
                }
                else
                {
                    inboundPostMap.put(newPosts.parentid,new List<SocialPost>{newPosts});
                }
                
            }
            if(!inboundPostMap.isEmpty())
            {
                List<socialpost> outBoundPost = [select id,createddate,parentid from socialpost where parentid IN : inboundPostMap.keySet() and isoutbound = True order by createddate desc limit 1000];
                for(socialpost op : outBoundPost)
                {
                    if(!outBoundPostMap.containsKey(op.parentid))
                        outBoundPostMap.put(op.parentid,op);
                }
                if(outBoundPostMap != null)
                {
                    List<Socialpost> inboundpostList = new List<socialpost>();
                    for(Socialpost post: outBoundPostMap.Values() )
                    {        
                        inboundpostList = inboundPostMap.get(post.parentid);
                        for(socialpost sp: inboundpostList)
                        {
                            If((sp.parentid == post.parentid) && (sp.createddate >= post.createddate))
                                sp.Last_OutBound_Message__C = post.createddate;
                        }
                    }
                }
            }
        }
   }
   //End
    
}