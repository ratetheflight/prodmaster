/********************************************************************** 
 Name:  AFKLM_SocialPostBeforeTriggerTest
 Runs on: AFKLM_SocialPostBeforeTrigger,AFKLM_SocialPostBeforeTriggerHandler
====================================================== 
Purpose: 
    This class contains unit tests for validating the social post after trigger
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR            DATE            DETAIL                                 
    1.0     Nagavi Babu       13/02/2016      Initial Development
 
***********************************************************************/
@isTest
private class AFKLM_SocialPostBeforeTriggerTest{
    
   static testMethod void validatePostAfterInsertError() {
        AFKLM_TestDataFactory.loadCustomSettings();
                
        List<Keywords_Library__c> keyWordsLibList= new List<Keywords_Library__c>();
        List<String> keysList=new List<string>{'urgent','today','tomorrow'}; //keywords list
        List<SocialPersona> personaList=new List<SocialPersona>();
        
        //Insert Keywords in keyword library
        keyWordsLibList=AFKLM_TestDataFactory.insertKeywords(keysList,'urgent');
        insert keyWordsLibList;
        
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78956788');
        insert newPerson;
        
        //Insert persona
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList;
        
        ////Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(3,'New');
        insert caseList;
        
        //Data for SocialPost
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,5);
        tempSocialPostList[0].ParentId=caseList[0].Id;
        tempSocialPostList[0].PostTags='Dutch';
        tempSocialPostList[0].Language__c='Dutch';
        tempSocialPostList[1].ParentId=caseList[1].Id;
        tempSocialPostList[1].PostTags='Spanish';
        tempSocialPostList[1].Language__c='Spanish';
        tempSocialPostList[2].ParentId=caseList[2].Id;
        tempSocialPostList[2].PostTags='IGT English';
        tempSocialPostList[2].Language__c='English';
        try {
            insert tempSocialPostList;            
        } 
        catch(Exception ex) {
          
        }
        
        List<SocialPost> tempoutboundPostList= new List<SocialPost>();
        tempoutboundPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,2);
        tempoutboundPostList[0].IsOutbound=True;
        tempoutboundPostList[0].ParentId=caseList[0].Id;
        tempoutboundPostList[1].IsOutbound=True;
        tempoutboundPostList[1].ParentId=caseList[0].Id;
        tempoutboundPostList[1].provider='WeChat';
        
        try{
        insert tempoutboundPostList;
        }
        catch(Exception e)
        {}
        
        List<SocialPost> inboundTestPostList= new List<SocialPost>();
        inboundTestPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,2);
        inboundTestPostList[0].provider='WeChat';
        inboundTestPostList[0].TopicProfileName ='';
        
        try{
        insert inboundTestPostList;
        }
        catch(Exception e)
        {}
        List<Socialpost> toUpdateList = new List<Socialpost>();
        for(socialpost sp :inboundTestPostList)
        {
        sp.company__c='KLM';
        toUpdateList .add(sp);
        }
        upsert toUpdateList;
    }
    
}