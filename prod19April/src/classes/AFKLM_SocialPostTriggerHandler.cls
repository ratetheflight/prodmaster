/*************************************************************************************************
* File Name     :   AFKLM_SocialPostTriggerHandler 
* Description   :   Helper class for the Social Post trigger
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     03/02/2016      Nagavi         Created the class to implement all functionalities in socialpost trigger
* 1.1     21/03/2017      Sai Choudhry   Agent Status on flyingblue bug fix. JIRA ST-1996.
* 1.2     22/03/2017      Sathish        Workflow Process builder Changes. JIRA ST-1970.
****************************************************************************************************/
Public class AFKLM_SocialPostTriggerHandler{
    
    //Boolean to prevent recursion
    public static boolean onBeforeInsert_FirstRun = true;
    public static boolean onBeforeUpdate_FirstRun = true;
    public static boolean onBeforeDelete_FirstRun= true;
    public static boolean onAfterInsert_FirstRun = true;
    public static boolean onAfterUpdate_FirstRun = true;
    public static boolean onAfterDelete_FirstRun= true;
    
    //Method for before insert event
    public void onBeforeInsert(List<SocialPost> objectsToInsert)
    {
        if(onBeforeInsert_FirstRun)
        {
            AFKLM_SocialPostBeforeTriggerHandler beforehandler=new AFKLM_SocialPostBeforeTriggerHandler ();
            SocialPostTriggerHandler trghandler=new SocialPostTriggerHandler();
            Map<Id, SocialPost> socialPostIds = new Map<Id, SocialPost>();
            Set<Id> listOfAccForKloutScoreUpd = new Set<Id>(); //Manuel Conde: AF Segmentation
            List<String> listOfPostsForKloutScoreUpd = new List<String>(); //Manuel Conde: AF Segmentation
            List<socialpost> toBeUpdatedPosts = new List<socialpost>(); //Sai: Bulkification of tier level update.JIRA ST-1682 
            list<socialpost> newpostlist=new list<socialpost>(); //ST-1755,JIRA ST-1752
             Map<ID,List<Socialpost>> updateLanaguegMap = new Map<ID,List<Socialpost>>();//ST-1725
            // PREVENT RECURSION
            //onBeforeInsert_FirstRun = false;
            for(SocialPost post:objectsToInsert){
            
                //To populate the Post Tags value
                post.SCS_Post_Tags__c = post.PostTags;
                
                //To gather posts for which the privileged values like VIP , Elite etc from parent account to its related social post
                if (post.whoId != null) { // Check if linked to Account already
                    socialPostIds.put(post.whoId, post);
                    //added by Sai.Bulkification of tier level update.JIRA ST-1682 
                    //toBeUpdatedPosts.add(post);
                }else{
                    // filtering only AirFrance twitter posts here
                    if(post.provider != Null && post.TopicProfileName != Null){ 
                        if(post.provider.toLowerCase().contains('twitter') && post.TopicProfileName.toLowerCase().contains('airfrance'))
                            listOfPostsForKloutScoreUpd.add(post.handle);
                    }
                }
                //End of code to gather data to populate privileged values
                
                //To gather data for auto urgency for Twitter and Facebook ST-1755,JIRA ST-1752
                if(post.content!=null && post.IsOutbound!=true  && post.TopicProfileName!=null && (post.TopicProfileName.contains('KLM') || post.TopicProfileName=='KLM') && (post.Provider=='Facebook' || ( post.Provider == 'Twitter' && post.MessageType.contains('Direct'))))
                {
                    if(post.PostTags!=NULL)
                    {
                        newpostlist.add(post);
                    }
                }
                //To gather data to Update language in outbound socialposts ST-1725
                if(post.isoutbound && post.parentid!=null)
                {
                    if(updateLanaguegMap.containskey(post.parentid))
                    {
                        updateLanaguegMap.get(post.parentid).add(post);
                    }
                    else
                    {
                        updateLanaguegMap.put(post.parentid,new List<SocialPost>{post});
                    }
                }
                            
            }
            
            //Start of logic to update the privileged fields on post from Account
            if(!socialPostIds.IsEmpty()){
                listOfAccForKloutScoreUpd=updatePostFields(socialPostIds);
            }
                
            //Manuel Conde: AF Segmentation. Initiate a callout to populate the fields
            if(!listOfAccForKloutScoreUpd.isEmpty() || !listOfPostsForKloutScoreUpd.isEmpty()){
                FutureMethodRecordProcessing.retrieveUpdateKloutScore(listOfAccForKloutScoreUpd, listOfPostsForKloutScoreUpd);
            }
            //End of logic to set the privileged values
            
            //Logic to perform auto urgency for Twitter and Facebook ST-1755,JIRA ST-1752
            if(!newpostlist.IsEmpty()){
                beforehandler.onBeforeInsert(newpostlist); 
            }
             //language on outbound posts ST-1725
            if(!updateLanaguegMap.isEmpty())
            {
                trghandler.updateoutboundlanguage(updateLanaguegMap);
            }
        }   
    }
    
    //Method for after update event
    public void onBeforeUpdate(List<SocialPost> oldPosts,List<SocialPost> objectsToUpdate,Map<Id,SocialPost> oldPostsMap,Map<Id,SocialPost> objectsToUpdateMap)
    {
        //if(onBeforeUpdate_FirstRun)
       // {
            // PREVENT RECURSION
            //onBeforeUpdate_FirstRun = false;
            
            AFKLM_SocialPostBeforeTriggerHandler beforeupdatehandler=new AFKLM_SocialPostBeforeTriggerHandler();
            AFKLM_SCS_UserSkipValidationSingleton Usr = AFKLM_SCS_UserSkipValidationSingleton.getInstance(); // To check if user has skip validation enabled
            set<Id> WhoIdset = new set<Id>(); //List to get the posts whose parent has changed
            List<SocialPost> socialPostList = new List<SocialPost>();
            
            for(SocialPost sp: objectsToUpdate) {
                if(Usr.getUsr.size() > 0) {
                    if(!Usr.getUsr[0].SkipValidation__c) {
                        //Taken from SCS_SocialPostWallPostParentIdNotChanged
                        if ('Facebook'.equals(sp.Provider) && sp.ParentId == null && 'Post'.equals(sp.MessageType)) {
                              sp.ParentId.addError('The Wallpost from Facebook with Case cannot be changed');
                        }

                        if ('Facebook'.equals(sp.Provider) && sp.ParentId == null && 'Reply'.equals(sp.MessageType)) {
                              sp.ParentId.addError('Sorry, you are not allowed to create a case on Facebook Reply level.');
                        }
                        
                        //To gather posts for which influencer field needs to be updated- Ata
                        if(sp.whoId != Null && sp.whoId != oldPostsMap.get(sp.Id).whoId){
                            WhoIdset.add(sp.whoId);
                        }
                        
                        System.debug('I m here '+WhoIdset); 
                    }
                }
                
                //To gather data to Update the Last outbound message related to the post. JIRA ST-1199
                if(sp.TopicProfileName != null)
                {
                    if(sp.parentid!=null && sp.Last_OutBound_Message__C == null && sp.Isoutbound == False && (sp.TopicProfileName.contains('KLM') || sp.TopicProfileName.contains('Airline')))
                    {                    
                        socialPostList.add(sp);
                    }
                }
                else
                {
                    if(sp.parentid!=null && sp.Last_OutBound_Message__C == null && sp.Isoutbound == False && sp.provider == 'wechat' && (sp.company__c == null || sp.company__c !='airfrance') )
                    {
                        socialPostList.add(sp);
                    }
                }
                
            } 

                        
            // Logic to populate influencer field on posts - Ata
            if(!WhoIdset.IsEmpty()){
                setInfluencerOnUpdate(WhoIdset,objectsToUpdate,oldPostsMap);   
            }
            //End of logic to populate influencer field of posts
            
            //Logic to update the case urgency to parent case if the parent field is updated to post during update event
            //Added boolean check condition by Sathish for ST-1970
            if(onBeforeUpdate_FirstRun){
            // PREVENT RECURSION
            onBeforeUpdate_FirstRun = false;
            beforeupdatehandler.onBeforeUpdate(oldPosts,objectsToUpdate,oldPostsMap,objectsToUpdateMap); 
            }
            
            //Logic to Update the Last outbound message related to the post. JIRA ST-1199
            if(!socialPostList.IsEmpty()){
                beforeupdatehandler.findLastOutBound(socialPostList);
            }
       // }   
    }
    
    //Method for after delete event
    public void onBeforeDelete(List<SocialPost> objectsToDelete)
    {
        if(onBeforeDelete_FirstRun)
        {
            // PREVENT RECURSION
            //onBeforeDelete_FirstRun = false;
            
            
        }
    }
    
    //Method for after insert event
    public void onAfterInsert(List<SocialPost> objectsToInsert)
    {
        if(onAfterInsert_FirstRun)
        {
            // PREVENT RECURSION
            //onAfterInsert_FirstRun = false;
            
            List<String> socialPostsExternalIds = new List<String>();
            List<String> socialPostsReplyTo = new List<String>();
            List<SocialPost> socialPosts = new List<SocialPost>();
            Set<String> extIdSet = New Set<String>();
            Map<Id, DateTime> newPostsMap = new Map<Id, DateTime>();            
            list<socialpost> newpostrecs=new list<socialpost>();
            SocialPostTriggerHandler trghandler=new SocialPostTriggerHandler(); 
            //Modified By Sai. JIRA ST-1996.
            Map<Id,Id> flyingBluePostMap=new Map<Id,Id>();
            //Added for ST-1970.
            Set<ID> parentIdSet = New Set<ID>();
            try{
                
                              
                for(SocialPost post : objectsToInsert){
                    
                    if(post.isOutbound){
                       if(post.parentid != null)
                        {
                            //Modified By Sai. JIRA ST-1996.
                            flyingBluePostMap.put(post.parentid,post.ownerid);
                        }
                        newPostsMap.put( post.Id, post.Posted );
                        if('WeChat'.equals( post.Provider ) || ('Facebook'.equals(post.Provider) && post.Heroku_Message__c == true)) {
                            socialPostsExternalIds.add( post.ExternalPostId );
                        } else {
                            socialPostsReplyTo.add( post.ReplyToId );
                        }
                    }
        
                    //1.6 To gather data to update Case status for flying blue records when a inbound post created.JIRA ST-1548      
                    if(post.ParentId!=NULL && post.IsOutbound==FALSE && post.FlyingBlue__c==TRUE )
                    {
                        newpostrecs.add(post);
                    } 
                    //Added condition by Sathish for ST-1970                
                    if(post.ParentId!=null && post.IsOutbound==FALSE && post.SCS_MarkAsReviewed__c==FALSE && post.Provider=='Facebook' )
                    {
                      parentIdSet.add(post.parentid);
                    }   
                    
                }
                
                //Logic to bulkify the code for wechat and other media channels
                // WeChat Bulkification
                if( socialPostsExternalIds.size() > 0 ) {
                    //external Id is replaced every day even for the same chat sessions. just to be sure we narrow down to the last 2 weeks 
                    // Added the filter " SCS_Status__c != 'Actioned' " in the below query as part of ticket ST-002830 on 21st april 16 - Nagavi
                    //Modified the query to populate the replied date and agent status. JIRA ST-1079.Sai
                    for (SocialPost sp : [SELECT ParentId, Replied_date__c, Status, Provider, OwnerId, SCS_Status__c, SCS_MarkAsReviewedBy__c, 
                                SCS_MarkAsReviewed__c, IncludeResponseTime__c, Ignore_for_SLA__c, Heroku_Message__c, ExternalPostId 
                        FROM SocialPost WHERE CreatedDate IN (TODAY,LAST_N_DAYS:5) and ExternalPostId IN : socialPostsExternalIds AND isOutbound = false AND SCS_Status__c != 'Actioned' AND Engaged_Date__c!= null
                        ORDER BY CreatedDate DESC LIMIT 10000]) {
                        if (!extIdSet.contains(sp.ExternalPostId)) {
                            extIdSet.add(sp.ExternalPostId);
                            socialPosts.add(sp);
                        }
                    }
                }
                // Facebook, Twitter, etc. 
                if( socialPostsReplyTo.size() > 0 ) {

                    for (SocialPost sp : [SELECT ParentId, Replied_date__c, Status, Provider, OwnerId, SCS_Status__c, SCS_MarkAsReviewedBy__c, 
                                            SCS_MarkAsReviewed__c, IncludeResponseTime__c, Ignore_for_SLA__c, ExternalPostId 
                                    FROM SocialPost WHERE Id IN : socialPostsReplyTo AND isOutbound = false LIMIT 10000]) {
                        socialPosts.add(sp);
                    }
                }
                if(!socialPosts.IsEmpty()){
                    updatePostField(socialPosts,newPostsMap);
                }    
                
                //Logic to Update Case status for flying blue records when a inbound post created.JIRA ST-1548
                if(newpostrecs.size()>0){
                    trghandler.updatecasestatusforflyingblue(newpostrecs);
                }    
                
                //To Update the Agent Status when Flying Blue agents engage/reply for the post(ST-1878) 
                //Modified By Sai. JIRA ST-1996.
                if(flyingBluePostMap.size()>0)
                {
                  //system.debug('Map details'+flyingBluePostMap);
                  trghandler.updatespstatus(flyingBluePostMap);
                }
                //Added method by Sathish for ST-1970
                if(parentIdSet.size()>0){
                 trghandler.updatenewpostspercase(parentIdSet);      
              }
            }
            catch(Exception e){
                System.debug(e);
            }
            
        }   
    }
    
    //Method for after update event
    public void onAfterUpdate(List<SocialPost> oldPosts,List<SocialPost> objectsToUpdate,Map<Id,SocialPost> oldPostsMap,Map<Id,SocialPost> objectsToUpdateMap)
    {
        if(onAfterUpdate_FirstRun)
        {
            // PREVENT RECURSION
            //onAfterUpdate_FirstRun = false;
            
            Map<Id, String> parentCaseIds = new Map<Id, String>();
            List<id> vipPostParentId = new List<id>();
            Map<String,SocialPost> socialPostWithCases = new Map<String,SocialPost>();
            
            SocialPostTriggerHandler trghandler=new SocialPostTriggerHandler();
            list<socialpost> newpostrecs=new list<socialpost>();                       
            Set<ID> parentIdSet = New Set<ID>(); 
            
            for(SocialPost sp : objectsToUpdate) {
                
                //To gather data to populate the case entity field for inbound posts Sai:JIRA ST-1311.
                if (sp.ParentId != null) {
                
                    //Added additional if condition to bypass outbound messages.JIRA ST-1311.
                    //Added entity check condition by Sathish for ST-1970
                    if('AirFrance'.equals(sp.Company__c) && sp.IsOutbound == False && sp.Entity__c!=oldPostsMap.get(sp.Id).Entity__c) {
                        parentCaseIds.put(sp.ParentId, sp.Entity__c);
                    }
                }
                
                //To gather data  to Mark the social post from VIP and Ambassador as Urgent. ST-853.
                if(!sp.Is_Urgent__c && !sp.Proposed_Urgent__c && sp.TopicProfileName == 'KLM' && (sp.Language__c == 'English' || sp.Language__c == 'Dutch'))
                {
                    if((sp.VIP__c || sp.Ambassador__c) && !(sp.IsOutbound) && (sp.MessageType == 'Private') && (sp.Provider == 'Facebook'))
                    {
                        vipPostParentId.add(sp.parentId);
                    }
                }
                
                //To gather data to update the case Group and the post group
                socialPostWithCases.put(sp.ParentId,sp);
                
                //To gather data to Update Case status for flying blue records when a inbound post created.JIRA ST-1548                
                if(sp.ParentId!=NULL && sp.ParentId!=oldPostsMap.get(sp.Id).ParentId && sp.FlyingBlue__c==TRUE && sp.IsOutbound==FALSE)
                {
                    newpostrecs.add(sp);
                }
                
                //To gather data for Show on post in view how many NEW posts are in the case .JIRA ST-1766
                //Added parent check condition by Sathish for ST-1970                
                if(sp.ParentId!=null && sp.ParentId!=oldPostsMap.get(sp.Id).ParentId && sp.IsOutbound==FALSE && sp.SCS_MarkAsReviewed__c==FALSE && sp.Provider=='Facebook' )
                {
                  parentIdSet.add(sp.parentid);
                }   
            }
            
            //Logic to populate the case entity field for inbound posts Sai:JIRA ST-1311.
            if(!parentCaseIds.IsEmpty()){
                updateCaseEntity(parentCaseIds);
            }
            
            //Logic to Mark the social post from VIP and Ambassador as Urgent. ST-853.
            if(!vipPostParentId.IsEmpty()){
                updateUrgencyForPost(vipPostParentId);
            }
            
            //Logic to update the case Group field and the post group field
            if(!socialPostWithCases.IsEmpty()){
                updateCasePostGroup(socialPostWithCases,oldPostsMap);
            }
            
            //Logic to Update Case status for flying blue records when a inbound post created.JIRA ST-1548
            if(!newpostrecs.IsEmpty()){
                trghandler.updatecasestatusforflyingblue(newpostrecs);
            }
            //Show on post in view how many NEW posts are in the case .JIRA ST-1766
             if(parentIdSet.size()>0){
                 trghandler.updatenewpostspercase(parentIdSet);      
              }
        }   


    }
    
    //Method for after delete event
    public void onAfterDelete(List<SocialPost> objectsToDelete)
    {
        if(onAfterDelete_FirstRun)
        {
            // PREVENT RECURSION
            //onAfterDelete_FirstRun= false;
            
            
        }
    }
    
    /**************** Independent Methods *****************************************************/
    //Logic to update the privileged fields on post from Account
    Public Static Set<Id> updatePostFields(Map<Id,SocialPost> postIdsMap){
        Set<Id> tempSet=new Set<Id>();
        //Logics to set the privileged values like VIP , Elite etc from parent account to its related social post
        List<Account> parentAccount = [Select Id,Elite_non_elite__c,Influencer__c,VIP__pc,Ambassador__c,Klout_score__c,Influencer_Score__c,
                                           Tier_Level__c,Booking_Class__c From Account a
                                           Where a.Id in :postIdsMap.keySet()];
        
        if(!parentAccount.IsEmpty()){
            for(Account accountToProcess: parentAccount) {
                for(socialpost sp: postIdsMap.values()){ //added by Sai.Bulkification of tier level update.JIRA ST-1682 
                    if(sp.whoid == accountToProcess.id){ //added by Sai.Bulkification of tier level update.JIRA ST-1682 
                        sp.Elite__c = accountToProcess.Elite_non_elite__c; 
                        sp.Influencers_SP__c = accountToProcess.Influencer__c;
                        sp.Vip__c = accountToProcess.VIP__pc; 
                        sp.Ambassador__c = accountToProcess.Ambassador__c; 
                        //Added By Ata-ST-583,ST-580
                        sp.Tier_Level__c = accountToProcess.Tier_Level__c;
                        sp.Booking_Class__c = accountToProcess.Booking_Class__c;
                        sp.Influencer_Score__c = accountToProcess.Influencer_Score__c;
                        //Manuel Conde: AF Segmentation
                        if(accountToProcess.Klout_score__c <> null){ 
                            sp.Klout_score__c = accountToProcess.Klout_score__c; 
                        }
                        else{
                            tempSet.add(accountToProcess.Id);
                        }
                    }
                    
                }
            }
        }
        return tempSet;
    }
    
    // Logic to populate influencer field on posts - Ata
    Public Static void setInfluencerOnUpdate(set<Id> WhoIdset,List<SocialPost> objectsToUpdate,Map<Id,SocialPost> oldPostsMap){
        //Added by Ata
        Map<id,Account> accMap = new Map<id,Account>([select id, Influencer_Score__c from Account where Id IN: WhoIdset]);    
        if(accMap!= Null && !accMap.isEmpty()){
            for(SocialPost posts: objectsToUpdate){
                if(posts.whoId != Null && accMap.containsKey(posts.WhoId) && posts.whoId != oldPostsMap.get(posts.Id).whoId){            
                    posts.Influencer_Score__c = (Integer)accMap.get(posts.whoId).Influencer_Score__c;              
                }    
            }     
        }       
    
    }
    
    //Logic to populate the case entity field for inbound posts Sai:JIRA ST-1311.
    Public Static void updateCaseEntity(Map<Id, String> parentCaseIds){
        List<Case> parentCases = [Select c.Id, c.Entity__c From Case c Where c.Id in :parentCaseIds.keySet()];
        List<Case> parentCasesToUpdate = new List<Case>();

        for (Case parent: parentCases) {
        //Added an if condtion to avoid recurrsive updates to entity field. JIRA ST-1311.
            if(parent.Entity__c !=parentCaseIds.get(parent.Id))
            {
                parent.Entity__c = parentCaseIds.get(parent.Id);
                parentCasesToUpdate.add(parent);  
            }
        }
        
        //Bulk update
        if (parentCasesToUpdate.size()>0) {
            update parentCasesToUpdate;
        }
    
    }
    
    //Logic to Mark the social post from VIP and Ambassador as Urgent. ST-853.
    Public static void updateUrgencyForPost(List<Id> vipPostParentId){
        List<case> casesToBeUpdated = new List<case>();
        
        for(Case cs:[Select id,is_urgent__C from case where id IN :vipPostParentId])
        {
            cs.is_urgent__c = True;
            casesToBeUpdated.add(cs);
        }
        
        if(!casesToBeUpdated.isEmpty())
        {
            Database.SaveResult[] results =Database.Update(casesToBeUpdated,false);
            
            for(Integer i=0;i<results.size();i++)
            {
                if (!results.get(i).isSuccess())
                {
                    Database.Error err = results.get(i).getErrors().get(0);
                    System.debug('Error - '+err.getMessage() + '\nStatus Code : '+err.getStatusCode()+'\n Fields : '+err.getFields());
                }
            }
            casesToBeUpdated.clear();
        }
    }  
    
    //Logic to update the case Group field and the post group field
    Public Static Void updateCasePostGroup(Map<String,SocialPost> socialPostWithCases,Map<id,Socialpost> oldpostmap){
        
        Map<Id,Case> updateCases = new Map<Id,Case>();
        Map<Id,SocialPost> updateSocialPosts = new Map<Id,SocialPost>();
        List<SocialPost> spToUpdate = new List<SocialPost>();
        List<Id> usersToCheck = new List<Id>();
        //Added for ST-1970-Starts
        set<Id> caserecds=new set<Id>();
        List<Case> cases =new list <Case>();
        for(SocialPost sp : socialPostWithCases.values())
        {
            if(sp.ParentId != null && ('IGT English'.equals(sp.SCS_Post_Tags__c) || 'IGT Spanish'.equals(sp.SCS_Post_Tags__c))) {
              if(oldpostmap.containsKey(Sp.Id))
               {
                 if(sp.Group__c!=oldpostmap.get(sp.Id).Group__c)
                 {
                    caserecds.add(sp.ParentId);
                 }
               }
            }
        }
        if(!caserecds.isEmpty()){
           cases = [SELECT id, Group__c FROM Case WHERE Id IN: caserecds];
        }
        //ST-1970 Ends

        for(SocialPost sp : socialPostWithCases.values()){
            //IBO:For the future 'IGT Spanish' needs to be added to IF statement
            //if(sp.ParentId != null && ('IGT English'.equals(sp.SCS_Post_Tags__c) || 'IGT Spanish'.equals(sp.SCS_Post_Tags__c))) {
            //Added condition to check the list is empty - ST-1970
            if(!cases.isEmpty()){
                for(Case cs : cases){
                    if(cs.id == sp.ParentId){
                        cs.Group__c = sp.Group__c;

                        if( !updateCases.containsKey(cs.id) ){
                            updateCases.put(cs.id,cs); 
                        }
                    }
                }
             }

                /*if( !cs.isEmpty() ){
                    cs[0].Group__c = sp.Group__c;
                    
                    if( !updateCases.containsKey(cs[0].id) ){
                        updateCases.put(cs[0].id,cs[0]); 
                    }
                }*/
           // }

            if('Unsupported'.equals(sp.Language__c) && 
                ('Actioned'.equals(sp.SCS_Status__c) || 'Mark as reviewed'.equals(sp.SCS_Status__c) || 'Engaged'.equals(sp.SCS_Status__c)) &&
                    (sp.Group__c == null || sp.Group__c == '')) {

                updateSocialPosts.put(sp.Id, sp);
                usersToCheck.add(sp.OwnerId);
            }
        }

        // fix for ST-001137
        if( !usersToCheck.isEmpty() ){

            Map<Id, User> owners = new Map<Id,User>([SELECT Id, Division FROM User WHERE id IN : usersToCheck]);

            if( !owners.isEmpty() ){

                for(SocialPost sptu : updateSocialPosts.values()) {

                    if( owners.containsKey( sptu.OwnerId ) ) {

                        User tmpUser = owners.get(sptu.OwnerId);

                        // Only this values allowed: Cygnific, IGT, TWC
                        if( 'Cygnific'.equals(tmpUser.Division) || 
                            'IGT'.equals(tmpUser.Division) ||
                                'CX'.equals(tmpUser.Division)) {

                            SocialPost newsp = new SocialPost(Id = sptu.Id);

                            newsp.Group__c = tmpUser.Division;
                            spToUpdate.add(newsp);
                        }
                    }
                }
            }
        }

        if( !spToUpdate.isEmpty() ){
            update spToUpdate;
        }
       
        if( !updateCases.isEmpty() ){
            update updateCases.values();
        }
    }
    
    //Logic to update the social post fields
    public static void updatePostField(List<SocialPost> socialPosts,Map<Id, DateTime> newPostsMap){
        List<SocialPost> socialPostsToUpdate = new List<SocialPost>();
        
        for( SocialPost spost : socialPosts ) {

            // Fix for agents replying always on the first incomming social post
            if( spost.Replied_date__c == null ) {

                spost.Replied_date__c = newPostsMap.get( spost.Id ); // previous sp.Posted 
            }
                
            spost.Status = 'Replied';
            if(!'WeChat'.equals(spost.Provider) && !('Facebook'.equals(spost.Provider) && spost.Heroku_Message__c == true)){
                spost.OwnerId = userInfo.getUserId();
                if (spost.ParentId != null && spost.IncludeResponseTime__c == false ) {
                    spost.IncludeResponseTime__c = true;
                }
            }
                
            //if( 'Twitter'.equals(spost.Provider) || 'WeChat'.equals(spost.Provider) ) {
                
                if(spost.SCS_Status__c != 'Actioned') {
                        
                    spost.SCS_Status__c = 'Actioned';
                    spost.SCS_MarkAsReviewedBy__c = '';
                    spost.SCS_MarkAsReviewed__c = false;
                    spost.Ignore_for_SLA__c = false;
                }
            //}

            // Workaround for AFKLM_SCS_ResponseTimeLogic
            //if(spost.ParentId != null && spost.IncludeResponseTime__c == false) {
            //
            //    socialPostsResponseTime.add( spost.ParentId );
            //}

            socialPostsToUpdate.add( spost );       
            if (socialPostsToUpdate.size() == 200) {
                update socialPostsToUpdate;
                socialPostsToUpdate.clear();
            }
        }
    
        system.debug('--+ socialPostsToUpdate: '+socialPostsToUpdate);
        if (!socialPostsToUpdate.isEmpty()) 
            update socialPostsToUpdate;

        
    }
            
}