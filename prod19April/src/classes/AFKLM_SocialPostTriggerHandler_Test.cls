/**********************************************************************
 Name:  AFKLM_SocialPostTriggerHandler_Test.cls
======================================================
Purpose: Test class for AFKLM_SocialPostTriggerHandler
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR            DATE            DETAIL                                 
    1.0     Sathish Balu    17/03/2017      Initial development
***********************************************************************/
@IsTest
public class AFKLM_SocialPostTriggerHandler_Test {

    static Testmethod void Socialposttrghandler(){
        
        AFKLM_SocialPostTriggerHandler trghandler=new AFKLM_SocialPostTriggerHandler();
        set<id>whoidset=new set<id>();
        Map<id,Socialpost>oldPostsMap=new map<id,socialpost>();
        Map<Id, String> parentCaseIds=new Map<Id, String>();
        List<Id> vipPostParentId= new List<Id>();
        Map<String,SocialPost> socialPostWithCases=new map<string,socialpost>();
        List<SocialPost> socialPosts= new list<socialpost>();
        Map<Id, DateTime> newPostsMap= new Map<Id, DateTime>();
        //Insert User
        User us=AFKLM_TestDataFactory.createUser('Test','User','Social Administrator');
        us.Entity__c='Santiago';
        us.Division='CX';
        Insert us;
        
        system.runAs(us)
        {
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78944488');
        insert newPerson;
        Account newPerson1=AFKLM_TestDataFactory.createPersonAccount('7800944488');
        newPerson1.Influencer_score_new__c='2';
        insert newPerson1;
        //Insert persona
        List<SocialPersona> personaList=new List<SocialPersona>();
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList; 
        
        //Insert Cases
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(3,'New');
        caselist[0].Entity__c='test';
        insert caseList;
        
        //Data for SocialPost
        
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,4);
        tempSocialPostList[0].whoid=newPerson.id;
        tempSocialPostList[0].parentid=caseList[0].id;
        tempSocialPostList[0].Company__c='Airfrance';
        tempSocialPostList[0].Group__c='IGT';
        tempSocialPostList[0].SCS_Post_Tags__c='IGT Spanish';
        tempSocialPostList[1].parentid=caseList[1].id;
        tempSocialPostList[2].IsOutbound=TRUE;  
        tempSocialPostList[3].IsOutbound=TRUE;  
        tempSocialPostList[3].Provider='WeChat';  
        insert tempSocialPostList;
        tempSocialPostList[0].Content='Airfrance';
        tempSocialPostList[1].Language__c='Unsupported';
        tempSocialPostList[1].SCS_Status__c='Actioned';
        tempSocialPostList[1].OwnerId=us.Id;
        update tempSocialPostList;
        List<SocialPost> tempSocialPostList1= new List<SocialPost>();
        tempSocialPostList1=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,1);
        insert tempSocialPostList1;
        whoidset.add(newPerson.Id);
        oldPostsMap.put(tempSocialPostList[0].id,tempSocialPostList[0]);    
        AFKLM_SocialPostTriggerHandler.setInfluencerOnUpdate(whoidset,tempSocialPostList,oldPostsMap);
        parentCaseIds.put(tempSocialPostList[0].parentid,tempSocialPostList[0].Entity__c);
        AFKLM_SocialPostTriggerHandler.updateCaseEntity(parentCaseIds);
        vipPostParentId.add(caselist[0].id);
        AFKLM_SocialPostTriggerHandler.updateUrgencyForPost(vipPostParentId);
        socialPostWithCases.put(tempSocialPostList[0].parentid, tempSocialPostList[0]);
        socialPostWithCases.put(tempSocialPostList[1].parentid, tempSocialPostList[1]);
        AFKLM_SocialPostTriggerHandler.updateCasePostGroup(socialPostWithCases,oldPostsMap);
        socialPosts.add(tempSocialPostList[0]);
        socialPosts.add(tempSocialPostList[1]);  
        newPostsMap.put(tempSocialPostList[0].id,tempSocialPostList[2].Posted);
        newPostsMap.put(tempSocialPostList[1].id,tempSocialPostList[3].Posted);
        AFKLM_SocialPostTriggerHandler.updatePostField(socialPosts,newPostsMap);
        }
    }


}