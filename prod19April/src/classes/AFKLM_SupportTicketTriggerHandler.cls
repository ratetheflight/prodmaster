/*************************************************************************************************
* File Name     :   AFKLM_SupportTicketTriggerHandler 
* Description   :   Helper class for the Support Ticket trigger
* @author       :   Prasanna
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     29/12/2016      Prasanna         Created the class
* 
****************************************************************************************************/
Public class AFKLM_SupportTicketTriggerHandler {
    
   
   @future(callout=true)
    public static void onAfterInsert(List<Id> checkhighsevid){
    
    AFKLM_Message_Alert malert=new AFKLM_Message_Alert();
    List<Support_ticket__c> ListOfTickets=[select id,Name,Subject__c,sms_sent__c from support_ticket__c where id in:checkhighsevid];
    List<Support_ticket__c> toUpdateTkts=new List<Support_ticket__c>();
    String messagebody='';
    List<User> userlist=new List<User>();
    try{
          userlist=[select id,MobilePhone,Send_High_Severity_SMS_Alerts__c from user where Send_High_Severity_SMS_Alerts__c=true];
        }
       catch(Exception e){
           System.debug('Exception happened::'+e.getmessage());
       } 
        for(Support_ticket__c tkt: ListOfTickets){
       
          //  if(tkt.subject__c.length()>150){
           // messagebody=tkt.name+':'+tkt.subject__c.left(150);
           // }
          //  else{
            messagebody='High Severity '+tkt.name+': '+tkt.subject__c;
          //  }   
            if(userlist.size() > 0 && tkt.sms_sent__c !=True){
            system.debug('message alert called from handler class');
            system.debug('customlable'+messagebody);
            try{
                malert.sendmessage(messagebody,userlist);
            }
            catch(exception e)
            {
            
            }
            tkt.sms_sent__c=True;
            toUpdateTkts.add(tkt);
            //update tkt;
            }
            messagebody=''; 
               
        }   
        if(toUpdateTkts.size()>0){
        update toUpdateTkts;
        }
         
    }    
}