/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class AFKLM_SupportTicketTriggerHandler_Test {

    static testMethod void myUnitTest() {
        
        User testUser=AFKLM_TestDataFactory.createUser('Test','User','Social Administrator');
            insert testUser;
            testUser.Send_High_Severity_SMS_Alerts__c=true;
            testUser.mobilephone='+919994137177';
            update testUser;
            List<User> userlist=new List<User>();
            userlist.add(testUser);
           // List<Id> userids=new List<User>();
            //userids.add(testUser.id);
            TwilioConfig__c customsettings=new TwilioConfig__c();
            customsettings.name='Test';
            customsettings.Accountsid__c='AC772a1e830c42f30398f52fe2fc1e43e1';
            customsettings.authtoken__c='43ead727caa44926d670a8b257feda41';
            customsettings.phone_number__c='+15005550006';
            insert customsettings;
        Support_Ticket__c highsevrecord=new Support_Ticket__c();
        highsevrecord.severity__c='High (SOCIAL ONLY)';
        highsevrecord.In_case_of_HighSev__c='YES My Supervisor Is Informed';
        highsevrecord.subject__c='High severity record';
    highsevrecord.social_network__c='No Social Network';
       // Test.StartTest();
        insert highsevrecord;
       // AFKLM_SupportTicketTriggerHandler shandler= new AFKLM_SupportTicketTriggerHandler();
       // shandler.onAfterInsert(userids);
       // String message='Test message';
       // AFKLM_Message_Alert malert=new AFKLM_Message_Alert();
       // malert.sendmessage(message,userlist);
       // Test.StopTest();
            
    }
}