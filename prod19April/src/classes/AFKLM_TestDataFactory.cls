/* 
*******************************************************************************************************************************************
* File Name     :   AFKLM_TestDataFactory
* Description   :   To be used in test classes to insert records in different objects
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    12/02/2016    Nagavi        Created the class.
* 1.1    05/07/2016    Narmatha      Included iPob related items
* 1.2    25/07/2016    Sai Choudhry  Included a method for social post creation
* 1.3    29/09/2016    Nagavi        Added methods to insert FB related methods to insert social post and persona & Keywords library records
* 1.4    19/11/2016    Nagavi        Added methods to insert nexmo posts and posts with external Id
* 1.5    03/01/2017    Pushkar       Utility method to return a webshop case
* 1.6    20/02/2017    Prasanna      Added AirFrance casecreation and AF recordtype
* 1.7    27/02/2017    Nagavi        Added DG Companion record insertion
******************************************************************************************************************************************* 
*/
@isTest
public class AFKLM_TestDataFactory{
    
    //To insert custom settings
    public static void loadCustomSettings(){  
         insertRecordTypeSettings('Servicing','Case'); 
         insertRecordTypeSettings('iPad on Board Case' ,'Case');
         insertRecordTypeSettings('AF Servicing' ,'Case');
         insert new AFKLM_Disable_Trigger__c(SetupOwnerId=UserInfo.getOrganizationId(), Fire_Case_In_Progress__c=true);
         insertCaseInProgressSettings(2,'Max Number of Case In Progress');
         insertSwitchSettings(true,'Settings');
    }
    
    public static Id getRecordTypeId(String ObjectName,String recordTypeName){
        Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
        Schema.SObjectType s = sObjectMap.get(ObjectName) ; 
        Schema.DescribeSObjectResult resSchema = s.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); 
        Id rtId = recordTypeInfo.get(recordTypeName).getRecordTypeId();
        return rtId;

    }
    
    private static void insertRecordTypeSettings(String recTypeName, String objType){
        AFKLM_RecordTypeIds__c rec1 =AFKLM_RecordTypeIds__c.getValues(recTypeName);
        if(rec1 == null)
        {           
            AFKLM_RecordTypeIds__c newRec = new AFKLM_RecordTypeIds__c();
            newRec.Name = recTypeName;
            newRec.RecordType_Id__c = getRecordTypeId(objType,recTypeName);
            insert newRec;   
        }        
    }
    
    private static void insertCaseInProgressSettings(Integer noOfCases,string settingName){
        AFKLM_SocialCustomerService__c setting1 =AFKLM_SocialCustomerService__c.getValues(settingName);
        if(setting1 == null)
        {           
            AFKLM_SocialCustomerService__c newRec = new AFKLM_SocialCustomerService__c ();
            newRec.Name = settingName;
            newRec.Number_of_Case_in_Progress__c=noOfCases;
            insert newRec;   
        }        
    }
    private static void insertSwitchSettings(Boolean switchOn,string settingName){
        AFKLM_SocialCustomerService__c setting2 =AFKLM_SocialCustomerService__c.getValues(settingName);
        if(setting2 == null)
        {           
            AFKLM_SocialCustomerService__c newRec2 = new AFKLM_SocialCustomerService__c ();
            newRec2.Name = settingName;
            newRec2.IsUrgencyRunning__c=switchOn;
            insert newRec2;   
        }        
    }
    
    //To insert KLM cases
    Public static List<Case> insertKLMCase(Integer numofRecords,String caseStatus){
        List<Case> caseList =  new List<Case>();
        for(integer i=0;i<numofRecords;i++){
            Case newCase=  new Case();
            newCase.Status=caseStatus;  
            newCase.RecordTypeId = getRecordTypeId('Case','Servicing');          
            caseList.add(newCase);
        }
        
        return caseList;    
    }
    
      //To insert AF cases
    Public static List<Case> insertAFCase(Integer numofRecords,String caseStatus){
        List<Case> caseList =  new List<Case>();
        for(integer i=0;i<numofRecords;i++){
            Case newCase=  new Case();
            newCase.Status=caseStatus;  
            newCase.RecordTypeId = getRecordTypeId('Case','AF Servicing');          
            caseList.add(newCase);
        }
        
        return caseList;    
    }
    
    //To insert Webshop cases
    Public static Case insertWebshopCase(String SuplEmail,String Email, String FBNumber){
        List<Case> caseList =  new List<Case>();
            Case newCase=  new Case();
            newCase.Status='New';  
            newCase.RecordTypeId = getRecordTypeId('Case','Webshop');          
            newCase.SuppliedEmail = SuplEmail;
            newCase.Email__c = Email;
            newCase.Flying_Blue_number__c = FBNumber;
            return newCase;    
    }
    
     //To insert Social Persona
    Public static List<SocialPersona> insertpersona(Integer numofRecords,string accountId){
        List<SocialPersona> personaList =  new List<SocialPersona>();
        for(integer i=0;i<numofRecords;i++){
            SocialPersona newpersona=  new SocialPersona();
            newpersona.Name='Handle'+i;
            newpersona.Provider='Twitter';
            newpersona.ExternalId='789456'+i;
            newpersona.ParentId=accountId;
                    
            personaList .add(newpersona);
        }
        
        return personaList ;    
    }
    
    //To insert Old status tracker records
    public static List<Old_status_Tracker__c > insertOldStatusTracker(integer numOfRecords)
    {
        List<Old_status_Tracker__c> trackerList=new List<Old_status_Tracker__c >();
        for(integer i=0;i<=numOfRecords;i++){
            
            Old_status_Tracker__c shadowTracker = new Old_status_Tracker__c();
            shadowTracker.New_Status__c = 'In Progress';
            shadowTracker.Old_Status__c = 'Closed';
            shadowTracker.Type__c = 'Handled';
            trackerList.add(shadowTracker);
        }
        return trackerList;
     }   
            
    //To insert twitter Social Post
    public static List<SocialPost> insertSocialPost(List<SocialPersona> socialPersonaList,string accountid,integer numOfRecords)
    {
        List<SocialPost> socialPostList=new List<SocialPost>();
        for(Socialpersona persona:socialPersonaList)
        {
            for(integer i=0;i<=numOfRecords;i++)
            {
                socialpost postToCreate=new socialpost();
                postToCreate.Name='Tweet from'+persona.Name;
                postToCreate.Posted=DateTimE.now();
                postToCreate.MessageType='Tweet';
                postToCreate.PersonaId=persona.Id;
                //postToCreate.whoid=accountid;
                socialPostList.add(postToCreate);
            }
        }
        return socialPostList;
    }
    
    //To insert FB Social Post
    public static List<SocialPost> insertFBSocialPost(List<SocialPersona> socialPersonaList,string accountid,integer numOfRecords)
    {
        List<SocialPost> socialPostList=new List<SocialPost>();
        for(Socialpersona persona:socialPersonaList)
        {
            for(integer i=0;i<=numOfRecords;i++)
            {
                socialpost postToCreate=new socialpost();
                postToCreate.Name='Comment From'+persona.Name;
                postToCreate.Posted=DateTimE.now();
                postToCreate.MessageType='Private';
                postToCreate.PersonaId=persona.Id;
                postToCreate.TopicProfileName='KLM';
                postToCreate.Content='Hi.. This is an urgent test post';
                postToCreate.provider='Facebook';
                postToCreate.PostTags='English';
                postToCreate.IsOutbound=false;
                postToCreate.ExternalPostId__c='5678'+i;
                //postToCreate.whoid=accountid;
                socialPostList.add(postToCreate);
            }
        }
        return socialPostList;
    }
    
    //To insert Nexmo Posts
    public static List<Nexmo_Post__c> insertNexmoSocialPost(string msg,integer numOfRecords)
    {
        List<Nexmo_Post__c> nexmoList=new List<Nexmo_Post__c>();
        for(integer i=0;i<=numOfRecords;i++)
            {
                Nexmo_Post__c postToCreate=new Nexmo_Post__c();
                postToCreate.Chat_Hash__c='gyt6587687yj'+i;
                postToCreate.Send_Time__c=DateTimE.now();
                postToCreate.From_User_Id__c='TestUser'+i;
                postToCreate.Message_Id__c=msg+i;
                postToCreate.User_Real_Name__c='KLMTest';
                nexmoList.add(postToCreate);
            }
        
        return nexmoList;
    }
    
    
     //To insert Social Persona
    Public static List<SocialPersona> insertFBpersona(Integer numofRecords,string accountId, string network){
        List<SocialPersona> personaList =  new List<SocialPersona>();
        for(integer i=0;i<numofRecords;i++){
            SocialPersona newpersona=  new SocialPersona();
            newpersona.Name='Handle'+i;
            newpersona.Provider=network;
            newpersona.ExternalId='789456'+i;
            newpersona.ParentId=accountId;
                    
            personaList .add(newpersona);
        }
        
        return personaList ;    
    }
    
    //To insert Keywords in the Keywords_Library__c object
    public static List<Keywords_Library__c> insertKeywords(List<string> keywordsList,string type)
    {
        List<Keywords_Library__c> keywordsLibList=new List<Keywords_Library__c>();
        for(string key:keywordsList)
        {
            Keywords_Library__c keyword=new Keywords_Library__c();
            keyword.Keyword_Type__c=type;
            keyword.Keyword__c=key;
            keywordsLibList.add(keyword);
           
        }
        return keywordsLibList;
    }
    
    //To insert DG Companion records
    Public static List<dgai__DG_Companion__c> insertDGCompanion(Integer numofRecords,List<SocialPost> postList){
        List<dgai__DG_Companion__c> dgCompanionList =  new List<dgai__DG_Companion__c>();
        for(integer i=0;i<numofRecords;i++){
            dgai__DG_Companion__c newDGComp=  new dgai__DG_Companion__c();
            newDGComp.Name='Test';  
            newDGComp.dgai__Suggestion_ID__c = '1149411';     
            newDGComp.dgai__Prompt_Text__c='All contact details of KLM can be found via: http://klmf.ly/1DkDcMi. Of course we are more than happy to assist via Social Media. Please let us know if and how we can assist.';    
            newDGComp.dgsh__Message_Status__c='Prompted';
            newDGComp.dgai__DG_Companion_Type__c='Message';
            newDGComp.dgsh__Post__c=postList[i].Id;
            dgCompanionList.add(newDGComp);
        }
        
        return dgCompanionList;    
    }
    
    //To insert User     
    public static User createUser(String firstname,String lastname,String ProfileName){

        List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = :profileName limit 1];
        String uniqueNameString = String.valueOf(System.now().getTime()) + '@test.com';
        
        User testUser = new User();
        testUser.userName = uniqueNameString;
        testUser.communityNickname = uniqueNameString.right(13);
        testUser.email = uniqueNameString;
        testUser.ProfileId = profiles[0].Id;
        testUser.FirstName = firstname;
        testUser.LastName = Lastname;
        testUser.Alias = uniqueNameString.right(3);
        testUser.TimeZoneSidKey = 'America/Los_Angeles';
        testUser.languagelocalekey = 'en_US';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'UTF-8';
        return testUser;
    }
    
    //To insert Person Account
    public static Account createPersonAccount(String flyingblueno) {
        RecordType personAccount=[Select ID from RecordType where Name =:'Person Account' Limit 1];
        Account testAccount=new Account();
        testAccount.LastName='Test';
        testAccount.Flying_Blue_Number__c=flyingblueno;
        testAccount.recordtypeID=personAccount.ID;
        return testAccount;
    }
    
    // To insert iPad on Board case
    public static List<Case> insertiPadonBoardCase(Integer numofRecords ){
        List<Case> caseList =  new List<Case>();
        for(integer i=0;i<numofRecords;i++){
            Case newCase=  new Case();
            newCase.Status='New';  
            newCase.RecordTypeId = getRecordTypeId('Case','iPad on Board Case');          
            caseList.add(newCase);
        }
        
        return caseList;   
    }
    
    //To insert WS Manager Custom setting -iPad
    public static void insertWSMangerSetting()
    {
        Organization org=[select id from organization Limit 1];
        List<AFKLM_WS_Settings__c> wsSettingList=new List<AFKLM_WS_Settings__c>();
        AFKLM_WS_Settings__c wsSetting=new AFKLM_WS_Settings__c();
        wsSetting.Name='BookedPassengerList CRM Prod';
        wsSetting.Org_Id__c=org.id;
        wsSetting.Timeout__c=60.00;
        wsSetting.Username__c='w11751256';
        wsSetting.Password__c='rNT43JOW8y';
        wsSetting.Web_Service__c='BookedPassengersListParam-v1';
        wsSetting.Endpoint__c='https://ws.airfrance.fr/passenger/distribmgmt/000200v01';
        wsSettingList.add(wsSetting);
        
        AFKLM_WS_Settings__c wsSetting2=new AFKLM_WS_Settings__c();
        wsSetting2.Name='FBRecognition Prod';
        wsSetting2.Org_Id__c=org.id;
        wsSetting2.Timeout__c=20.00;
        wsSetting2.Username__c='w11751256';
        wsSetting2.Password__c='rNT43JOW8y';
        wsSetting2.Web_Service__c='passenger_FBRecognition-v4';
        wsSetting2.Endpoint__c='https://ws.airfrance.fr/passenger/marketing/000031v04';
        wsSettingList.add(wsSetting2);
        
        AFKLM_WS_Settings__c wsSetting3=new AFKLM_WS_Settings__c();
        wsSetting3.Name='IssueRefundRequest';
        wsSetting3.Org_Id__c=org.id;
        wsSetting3.Timeout__c=10.00;
        wsSetting3.Username__c='username';
        wsSetting3.Password__c='password';
        wsSetting3.Web_Service__c='IssueRefundRequest-v1';
        wsSetting3.Endpoint__c='https://kl.iscits.com/staging/csrservice/csrservice.asmx';
        wsSetting3.Certificate__c='CSR_Refund_Request';
        wsSettingList.add(wsSetting3);
        
        AFKLM_WS_Settings__c wsSetting4=new AFKLM_WS_Settings__c();
        wsSetting4.Name='providePaxListInThePassenger Prod';
        wsSetting4.Org_Id__c=org.id;
        wsSetting4.Timeout__c=60.00;
        wsSetting4.Username__c='w11751256';
        wsSetting4.Password__c='rNT43JOW8y';
        wsSetting4.Web_Service__c='ProvidePaxListInThePassengerHandlingWindow-v3';
        wsSetting4.Endpoint__c='https://ws.airfrance.fr/passenger/groundservices/000304v03';
        wsSettingList.add(wsSetting4);
        
        AFKLM_WS_Settings__c wsSetting5=new AFKLM_WS_Settings__c();
        wsSetting5.Name='ProvidePNRListForACustomer Prod';
        wsSetting5.Org_Id__c=org.id;
        wsSetting5.Timeout__c=20.00;
        wsSetting5.Username__c='w11751256';
        wsSetting5.Password__c='rNT43JOW8y';
        wsSetting5.Web_Service__c='ProvidePNRListForACustomerParam-v3';
        wsSetting5.Endpoint__c='https://ws.airfrance.fr/passenger/distribmgmt/000338v03';
        wsSettingList.add(wsSetting5);
        
        AFKLM_WS_Settings__c wsSetting6=new AFKLM_WS_Settings__c();
        wsSetting6.Name='SearchHomonymsIndividualType Prod';
        wsSetting6.Org_Id__c=org.id;
        wsSetting6.Timeout__c=60.00;
        wsSetting6.Username__c='w11751256';
        wsSetting6.Password__c='rNT43JOW8y';
        wsSetting6.Web_Service__c='SearchHomonymsIndividualType';
        wsSetting6.Endpoint__c='https://ws.airfrance.fr/passenger/marketing/000543v01';
        wsSettingList.add(wsSetting6);
        
        insert wsSettingList;
    }
    
}