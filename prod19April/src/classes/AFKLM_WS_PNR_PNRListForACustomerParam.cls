//Generated by wsdl2apex

public class AFKLM_WS_PNR_PNRListForACustomerParam {
    public class Service {
        public String type_x;
        public String code;
        public Boolean chargeableIndicator;
        public String airline;
        public String ticketTattoo;
        public String[] productTattoosList;
        private String[] type_x_type_info = new String[]{'type','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','ServiceTypeEnum','1','1','false'};
        private String[] code_type_info = new String[]{'code','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','serviceCodeType','0','1','false'};
        private String[] chargeableIndicator_type_info = new String[]{'chargeableIndicator','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] airline_type_info = new String[]{'airline','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] ticketTattoo_type_info = new String[]{'ticketTattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','1','false'};
        private String[] productTattoosList_type_info = new String[]{'productTattoosList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','code','chargeableIndicator','airline','ticketTattoo','productTattoosList'};
    }
    public class DataIn {
        public Boolean recentTrip;
        public String tripAge;
        public AFKLM_WS_PNR_PNRListForACustomerParam.PassengerIdentifier paxIdentifier;
        public AFKLM_WS_PNR_PNRListForACustomerParam.FlightIdentifier flightIdentifier;
        private String[] recentTrip_type_info = new String[]{'recentTrip','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] tripAge_type_info = new String[]{'tripAge','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','TripAgeEnum','0','1','false'};
        private String[] paxIdentifier_type_info = new String[]{'paxIdentifier','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','PassengerIdentifier','1','1','false'};
        private String[] flightIdentifier_type_info = new String[]{'flightIdentifier','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','FlightIdentifier','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'recentTrip','tripAge','paxIdentifier','flightIdentifier'};
    }
    public class DataOut {
        public AFKLM_WS_PNR_PNRListForACustomerParam.TravelRecord[] travelRecordsList;
        public String incompleteResults;
        private String[] travelRecordsList_type_info = new String[]{'travelRecordsList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','TravelRecord','0','-1','false'};
        private String[] incompleteResults_type_info = new String[]{'incompleteResults','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'travelRecordsList','incompleteResults'};
    }
    public class Specificity {
        public String type_x;
        public String code;
        public String[] productTattoosList;
        private String[] type_x_type_info = new String[]{'type','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','SpecificityTypeEnum','1','1','false'};
        private String[] code_type_info = new String[]{'code','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','specificityCodeType','0','1','false'};
        private String[] productTattoosList_type_info = new String[]{'productTattoosList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','code','productTattoosList'};
    }
    public class Product {
        public String tattoo;
        private String[] tattoo_type_info = new String[]{'tattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'tattoo'};
    }
    public class Contact {
        public String tattoo;
        public String value;
        public String type_x;
        public String[] paxTattoos;
        private String[] tattoo_type_info = new String[]{'tattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','1','1','false'};
        private String[] value_type_info = new String[]{'value','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] paxTattoos_type_info = new String[]{'paxTattoos','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'tattoo','value','type_x','paxTattoos'};
    }
    public class DetailedPassenger {
        public String firstName;
        public String lastName;
        public String passengerType;
        public String babyFirstName;
        public String babyLastName;
        public String[] emailList; 
        public String tattoo;
        public String uci;               
        public AFKLM_WS_PNR_PNRListForACustomerParam.Classification[] classificationsList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Specificity[] specificitiesList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Service[] servicesList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Ticket[] ticketsList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Upi[] upiList;
        private String[] firstName_type_info = new String[]{'firstName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','1','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','1','1','false'};
        private String[] passengerType_type_info = new String[]{'passengerType','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','PassengerTypeEnum','1','1','false'};
        private String[] babyFirstName_type_info = new String[]{'babyFirstName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','0','1','false'};
        private String[] babyLastName_type_info = new String[]{'babyLastName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','0','1','false'};
        private String[] emailList_type_info = new String[]{'emailList','http://www.w3.org/2001/XMLSchema','string','0','-1','false'};
        private String[] tattoo_type_info = new String[]{'tattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','1','1','false'};
        private String[] uci_type_info = new String[]{'uci','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','uciType','0','1','false'};                
        private String[] classificationsList_type_info = new String[]{'classificationsList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Classification','0','-1','false'};
        private String[] specificitiesList_type_info = new String[]{'specificitiesList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Specificity','0','-1','false'};
        private String[] servicesList_type_info = new String[]{'servicesList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Service','0','-1','false'};
        private String[] ticketsList_type_info = new String[]{'ticketsList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Ticket','0','-1','false'};
        private String[] upiList_type_info = new String[]{'upiList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Upi','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'firstName','lastName','passengerType','babyFirstName','babyLastName','emailList','tattoo','uci','classificationsList','specificitiesList','servicesList','ticketsList','upiList'};
    }
    public class Passenger {
        public String firstName;
        public String lastName;
        public String passengerType;
        public String babyFirstName;
        public String babyLastName;
        public String[] emailList;
        private String[] firstName_type_info = new String[]{'firstName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','1','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','1','1','false'};
        private String[] passengerType_type_info = new String[]{'passengerType','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','PassengerTypeEnum','1','1','false'};
        private String[] babyFirstName_type_info = new String[]{'babyFirstName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','0','1','false'};
        private String[] babyLastName_type_info = new String[]{'babyLastName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','paxNameType','0','1','false'};
        private String[] emailList_type_info = new String[]{'emailList','http://www.w3.org/2001/XMLSchema','string','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'firstName','lastName','passengerType','babyFirstName','babyLastName','emailList'};
    }
    public class TypePnr {
        public String identifier;
        public String code;
        public String[] segmentTattoosList;
        public String[] paxTattoos;
        private String[] identifier_type_info = new String[]{'identifier','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] code_type_info = new String[]{'code','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] segmentTattoosList_type_info = new String[]{'segmentTattoosList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','-1','false'};
        private String[] paxTattoos_type_info = new String[]{'paxTattoos','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'identifier','code','segmentTattoosList','paxTattoos'};
    }
    public class FlightIdentifier {
        public String airlineCode;
        public String flightNumber;
        public String flightSuffix;
        public DateTime departureDate;
        public Boolean isHeaderDepartureDate;
        public String deparureAirportCode;
        public String arrivalAirportCode;
        public String offerType;
        private String[] airlineCode_type_info = new String[]{'airlineCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] flightNumber_type_info = new String[]{'flightNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] flightSuffix_type_info = new String[]{'flightSuffix','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] departureDate_type_info = new String[]{'departureDate','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] isHeaderDepartureDate_type_info = new String[]{'isHeaderDepartureDate','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] deparureAirportCode_type_info = new String[]{'deparureAirportCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] arrivalAirportCode_type_info = new String[]{'arrivalAirportCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] offerType_type_info = new String[]{'offerType','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','OfferTypeEnum','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'airlineCode','flightNumber','flightSuffix','departureDate','isHeaderDepartureDate','deparureAirportCode','arrivalAirportCode','offerType'};
    }
    public class AssociatedPNR {
        public String reclocAmd;
        private String[] reclocAmd_type_info = new String[]{'reclocAmd','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','reclocAMDType','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'reclocAmd'};
    }
    public class Upi {
        public String upi;
        public String segmentTattoo;
        private String[] upi_type_info = new String[]{'upi','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','upiType','1','1','false'};
        private String[] segmentTattoo_type_info = new String[]{'segmentTattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'upi','segmentTattoo'};
    }
    public class Ticket {
        public String ticketNumber;
        public Boolean ticketBaby;
        public String tattoo;
        public String productTattoo;
        private String[] ticketNumber_type_info = new String[]{'ticketNumber','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tktNumberType','1','1','false'};
        private String[] ticketBaby_type_info = new String[]{'ticketBaby','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] tattoo_type_info = new String[]{'tattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','1','1','false'};
        private String[] productTattoo_type_info = new String[]{'productTattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'ticketNumber','ticketBaby','tattoo','productTattoo'};
    }
    public class ContractIdentifier {
        public String contractType;
        public String contractNumber;
        private String[] contractType_type_info = new String[]{'contractType','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','ContractTypeEnum','1','1','false'};
        private String[] contractNumber_type_info = new String[]{'contractNumber','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'contractType','contractNumber'};
    }
    public class Classification {
        public String type_x;
        public String code;
        public String level;
        public String airline;
        public String contractNumber;
        public String[] productTattoosList;
        private String[] type_x_type_info = new String[]{'type','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','ClassificationTypeEnum','1','1','false'};
        private String[] code_type_info = new String[]{'code','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','classificationCodeType','1','1','false'};
        private String[] level_type_info = new String[]{'level','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','classificationLevelType','0','1','false'};
        private String[] airline_type_info = new String[]{'airline','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','airlineType','0','1','false'};
        private String[] contractNumber_type_info = new String[]{'contractNumber','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','contractNumberType','0','1','false'};
        private String[] productTattoosList_type_info = new String[]{'productTattoosList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','code','level','airline','contractNumber','productTattoosList'};
    }
    public class Segment {
        public String tattoo;
        public String airlineCode;
        public String flightNumber;
        public String flightSuffix;
        public String bookingStatus;
        public String boardingPoint;
        public String offPoint;
        public DateTime departureDate;
        public DateTime arrivalDate;
        public String bookingClass;
        public String transportClass;
        public String serviceClass;
        public String operatingAirlineCode;
        public String operatingFlightNumber;
        public String operatingFlightSuffix;
        public String operatingTransportClass;
        public String operatingBookingClass;
        public String negoId;
        public Boolean isOpen;
        public String offerType;
        private String[] tattoo_type_info = new String[]{'tattoo','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','tattooType','1','1','false'};        
        private String[] airlineCode_type_info = new String[]{'airlineCode','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','airlineType','1','1','false'};
        private String[] flightNumber_type_info = new String[]{'flightNumber','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','flightNumberType','0','1','false'};
        private String[] flightSuffix_type_info = new String[]{'flightSuffix','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','suffixType','0','1','false'};
        private String[] bookingStatus_type_info = new String[]{'bookingStatus','http://www.af-klm.com/services/passenger/Common-v1/xsd','BookingStatusEnum','0','1','false'};
        private String[] boardingPoint_type_info = new String[]{'boardingPoint','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','airportCodeType','1','1','false'};
        private String[] offPoint_type_info = new String[]{'offPoint','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','airportCodeType','1','1','false'};
        private String[] departureDate_type_info = new String[]{'departureDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] arrivalDate_type_info = new String[]{'arrivalDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] bookingClass_type_info = new String[]{'bookingClass','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','classType','0','1','false'};
        private String[] transportClass_type_info = new String[]{'transportClass','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','classType','0','1','false'};
        private String[] serviceClass_type_info = new String[]{'serviceClass','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','classType','0','1','false'};
        private String[] operatingAirlineCode_type_info = new String[]{'operatingAirlineCode','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','airlineType','0','1','false'};
        private String[] operatingFlightNumber_type_info = new String[]{'operatingFlightNumber','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','flightNumberType','0','1','false'};
        private String[] operatingFlightSuffix_type_info = new String[]{'operatingFlightSuffix','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','suffixType','0','1','false'};
        private String[] operatingTransportClass_type_info = new String[]{'operatingTransportClass','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','classType','0','1','false'};
        private String[] operatingBookingClass_type_info = new String[]{'operatingBookingClass','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','classType','0','1','false'};
        private String[] negoId_type_info = new String[]{'negoId','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','negoRefType','0','1','false'};
        private String[] isOpen_type_info = new String[]{'isOpen','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] offerType_type_info = new String[]{'offerType','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','OfferTypeEnum','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'tattoo','airlineCode','flightNumber','flightSuffix','bookingStatus','boardingPoint','offPoint','departureDate','arrivalDate','bookingClass','transportClass','serviceClass','operatingAirlineCode','operatingFlightNumber','operatingFlightSuffix','operatingTransportClass','operatingBookingClass','negoId','isOpen','offerType'};
    }
    public class PassengerIdentifier {
        public String documentNumber;
        public String firstName;
        public String lastName;
        public Boolean incompleteFirstName;
        public String email;
        public AFKLM_WS_PNR_PNRListForACustomerParam.ContractIdentifier contractIdentifier;
        public AFKLM_WS_PNR_PNRListForACustomerParam.PnrIdentifier[] pnrIdentifierList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.ServiceIdentifier[] serviceIdentifierList;
        private String[] documentNumber_type_info = new String[]{'documentNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] incompleteFirstName_type_info = new String[]{'incompleteFirstName','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] email_type_info = new String[]{'email','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] contractIdentifier_type_info = new String[]{'contractIdentifier','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','ContractIdentifier','0','1','false'};
        private String[] pnrIdentifierList_type_info = new String[]{'pnrIdentifierList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','PnrIdentifier','0','-1','false'};
        private String[] serviceIdentifierList_type_info = new String[]{'serviceIdentifierList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','ServiceIdentifier','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'documentNumber','firstName','lastName','incompleteFirstName','email','contractIdentifier','pnrIdentifierList','serviceIdentifierList'};
    }
    public class TravelRecord {
        public String reclocAmd;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Contact[] contacts;
        public DateTime creationDate;
        public DateTime purgeDateAmd;
        public AFKLM_WS_PNR_Common.PointOfSale creator;
        public AFKLM_WS_PNR_Common.PointOfSale lastUpdater;
        public AFKLM_WS_PNR_Common.PointOfSale owner;
        public String responsibleOfficeId;
        public Integer nbPassengers;
        public Boolean isGroup;
        public String groupName;
        public Integer nbNoNamedPassengers;
        public Boolean isMarketedBSPNR;
        public AFKLM_WS_PNR_PNRListForACustomerParam.AssociatedPNR[] associatedPNRsList;
        public AFKLM_WS_PNR_Common.PointOfSale technicalLastUpdater;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Passenger[] accompanyingPaxList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.DetailedPassenger[] requestedPaxList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Segment[] segmentsList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.TypePnr[] typePnrList;
        public AFKLM_WS_PNR_PNRListForACustomerParam.Contact[] contact;
        private String[] reclocAmd_type_info = new String[]{'reclocAmd','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','reclocAMDType','1','1','false'};
        private String[] contacts_type_info = new String[]{'contacts','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Contact','0','-1','false'};
        private String[] creationDate_type_info = new String[]{'creationDate','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] purgeDateAmd_type_info = new String[]{'purgeDateAmd','http://www.w3.org/2001/XMLSchema','dateTime','1','1','false'};
        private String[] creator_type_info = new String[]{'creator','http://www.af-klm.com/services/passenger/Common-v1/xsd','PointOfSale','1','1','false'};
        private String[] lastUpdater_type_info = new String[]{'lastUpdater','http://www.af-klm.com/services/passenger/Common-v1/xsd','PointOfSale','1','1','false'};
        private String[] owner_type_info = new String[]{'owner','http://www.af-klm.com/services/passenger/Common-v1/xsd','PointOfSale','1','1','false'};
        private String[] responsibleOfficeId_type_info = new String[]{'responsibleOfficeId','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','officeIdType','0','1','false'};
        private String[] nbPassengers_type_info = new String[]{'nbPassengers','http://www.w3.org/2001/XMLSchema','int','1','1','false'};
        private String[] isGroup_type_info = new String[]{'isGroup','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] groupName_type_info = new String[]{'groupName','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','groupNameType','0','1','false'};
        private String[] nbNoNamedPassengers_type_info = new String[]{'nbNoNamedPassengers','http://www.w3.org/2001/XMLSchema','int','0','1','false'};
        private String[] isMarketedBSPNR_type_info = new String[]{'isMarketedBSPNR','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] associatedPNRsList_type_info = new String[]{'associatedPNRsList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','AssociatedPNR','0','-1','false'};
        private String[] technicalLastUpdater_type_info = new String[]{'technicalLastUpdater','http://www.af-klm.com/services/passenger/Common-v1/xsd','PointOfSale','1','1','false'};
        private String[] accompanyingPaxList_type_info = new String[]{'accompanyingPaxList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Passenger','0','-1','false'};
        private String[] requestedPaxList_type_info = new String[]{'requestedPaxList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','DetailedPassenger','0','-1','false'};
        private String[] segmentsList_type_info = new String[]{'segmentsList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Segment','0','-1','false'};
        private String[] typePnrList_type_info = new String[]{'typePnrList','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','TypePnr','0','-1','false'};
        private String[] contact_type_info = new String[]{'contact','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','Contact','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'reclocAmd','contacts','creationDate','purgeDateAmd','creator','lastUpdater','owner','responsibleOfficeId','nbPassengers','isGroup','groupName','nbNoNamedPassengers','isMarketedBSPNR','associatedPNRsList','technicalLastUpdater','accompanyingPaxList','requestedPaxList','segmentsList','typePnrList','contact'};
    }
    public class ProvidePNRListForACustomerFaultBusinessFault {
        public String ErrorCode;
        public String FaultDescription;
        private String[] ErrorCode_type_info = new String[]{'ErrorCode','http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','ProvidePNRListForACustomerFault','1','1','false'};
        private String[] FaultDescription_type_info = new String[]{'FaultDescription','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'ErrorCode','FaultDescription'};
    }
    public class PnrIdentifier {
        public String pnrType;
        public Boolean isPresent;
        private String[] pnrType_type_info = new String[]{'pnrType','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] isPresent_type_info = new String[]{'isPresent','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'pnrType','isPresent'};
    }
    public class ServiceIdentifier {
        public String serviceType;
        public Boolean isChargeable;
        public Boolean isPresent;
        private String[] serviceType_type_info = new String[]{'serviceType','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] isChargeable_type_info = new String[]{'isChargeable','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] isPresent_type_info = new String[]{'isPresent','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/ProvidePNRListForACustomerParam-v3/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'serviceType','isChargeable','isPresent'};
    }
}