//Generated by wsdl2apex

public class AFKLM_WS_SHI_SearchHomonymsIndividual {
    public class SearchHomonymsIndividualService_v1_soap11http {
        public String endpoint_x = 'ToBeSpecified';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualType-v1/xsd', 'AFKLM_WS_SHI_Search', 'http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd', 'AFKLM_WS_SHI_SoftcomputingType', 'http://www.af-klm.com/services/common/SystemFault-v1/xsd', 'AFKLM_WS_SHI_Systemfault_2', 'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualService-v1/wsdl', 'AFKLM_WS_SHI_SearchHomonymsIndividual', 'http://www.af-klm.com/services/passenger/SicIndividuType-v1/xsd', 'AFKLM_WS_SHI_SicIndividuType', 'http://www.af-klm.com/services/passenger/SicDataType-v1/xsd', 'AFKLM_WS_SHI_SicDataType', 'http://www.af-klm.com/services/passenger/SicCommonDataType-v1/xsd', 'AFKLM_WS_SHI_SicCommonDataType'};
        
		// Added the following custom properties:
		// Username for WS-Security Token
        public String wsUsername;
        // Password for WS-Security Token
        public String wsPassword;
        // WS-Security Token element
		public AFKLM_WS_UsernameToken10.Security_element Security;
		// WS-Security Token namespace
        private String Security_hns = 'Security=http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';        
        
        /*
        public AFKLM_WS_SHI_Search.SearchHomonymsResponse searchHomonyms(String applicationCode,String searchDriving,String lastName,String lastNameSearchType,String firstName,String firstNameSearchType,String civility,DateTime birthdate,String phoneNumber,String numberAndStreet,String district,String city,String citySearchType,String zipCode,String zipCodeSearchType,String stateCode,String countryCode,String email) {
            
			// WS-Security in SOAP Header
			Security = new AFKLM_WS_UsernameToken10.Security_element(wsUsername, wsPassword);            
            
            AFKLM_WS_SHI_Search.SearchHomonymsRequest request_x = new AFKLM_WS_SHI_Search.SearchHomonymsRequest();
            AFKLM_WS_SHI_Search.SearchHomonymsResponse response_x;
            request_x.applicationCode = applicationCode;
            request_x.searchDriving = searchDriving;
            request_x.lastName = lastName;
            request_x.lastNameSearchType = lastNameSearchType;
            request_x.firstName = firstName;
            request_x.firstNameSearchType = firstNameSearchType;
            request_x.civility = civility;
            request_x.birthdate = birthdate;
            request_x.phoneNumber = phoneNumber;
            request_x.numberAndStreet = numberAndStreet;
            request_x.district = district;
            request_x.city = city;
            request_x.citySearchType = citySearchType;
            request_x.zipCode = zipCode;
            request_x.zipCodeSearchType = zipCodeSearchType;
            request_x.stateCode = stateCode;
            request_x.countryCode = countryCode;
            request_x.email = email;
            Map<String, AFKLM_WS_SHI_Search.SearchHomonymsResponse> response_map_x = new Map<String, AFKLM_WS_SHI_Search.SearchHomonymsResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualService-v1/searchHomonyms',
              'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualType-v1/xsd',
              'SearchHomonymsRequestElement',
              'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualType-v1/xsd',
              'SearchHomonymsResponseElement',
              'AFKLM_WS_SHI_Search.SearchHomonymsResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        */

        public AFKLM_WS_SHI_Search.SearchHomonymsResponse searchHomonymsMinimum(String applicationCode,String searchDriving,String lastName,String lastNameSearchType,String firstName,String firstNameSearchType,String civility,DateTime birthdate,String phoneNumber,String numberAndStreet,String district,String city,String citySearchType,String zipCode,String zipCodeSearchType,String stateCode,String countryCode,String email) {
            
			// WS-Security in SOAP Header
			Security = new AFKLM_WS_UsernameToken10.Security_element(wsUsername, wsPassword);            
            
            AFKLM_WS_SHI_Search.SearchHomonymsRequestSmall request_x = new AFKLM_WS_SHI_Search.SearchHomonymsRequestSmall();
            AFKLM_WS_SHI_Search.SearchHomonymsResponse response_x;
            request_x.applicationCode = applicationCode;
            request_x.searchDriving = searchDriving;
            request_x.lastName = lastName;
            request_x.lastNameSearchType = lastNameSearchType;
            request_x.firstName = firstName;
            request_x.firstNameSearchType = firstNameSearchType;
            request_x.city = city;
            request_x.citySearchType = citySearchType;
            
            Map<String, AFKLM_WS_SHI_Search.SearchHomonymsResponse> response_map_x = new Map<String, AFKLM_WS_SHI_Search.SearchHomonymsResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualService-v1/searchHomonyms',
              'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualType-v1/xsd',
              'SearchHomonymsRequestElement',
              'http://www.af-klm.com/services/passenger/SearchHomonymsIndividualType-v1/xsd',
              'SearchHomonymsResponseElement',
              'AFKLM_WS_SHI_Search.SearchHomonymsResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }   
              
    }
}