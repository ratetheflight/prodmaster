//Generated by wsdl2apex

public class AFKLM_WS_SHI_SoftcomputingType {
    public class SoftComputingResponse {
        public String noErreur;
        public String libelleErreur;
        public String noErrNormail;
        public String adrMailingL1;
        public String adrMailingL2;
        public String adrMailingL3;
        public String adrMailingL4;
        public String adrMailingL5;
        public String adrMailingL6;
        public String adrMailingL7;
        public String adrMailingL8;
        public String adrMailingL9;
        private String[] noErreur_type_info = new String[]{'noErreur','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] libelleErreur_type_info = new String[]{'libelleErreur','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] noErrNormail_type_info = new String[]{'noErrNormail','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL1_type_info = new String[]{'adrMailingL1','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL2_type_info = new String[]{'adrMailingL2','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL3_type_info = new String[]{'adrMailingL3','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL4_type_info = new String[]{'adrMailingL4','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL5_type_info = new String[]{'adrMailingL5','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL6_type_info = new String[]{'adrMailingL6','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL7_type_info = new String[]{'adrMailingL7','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL8_type_info = new String[]{'adrMailingL8','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] adrMailingL9_type_info = new String[]{'adrMailingL9','http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.af-klm.com/services/passenger/SoftComputingType-v1/xsd','false','false'};
        private String[] field_order_type_info = new String[]{'noErreur','libelleErreur','noErrNormail','adrMailingL1','adrMailingL2','adrMailingL3','adrMailingL4','adrMailingL5','adrMailingL6','adrMailingL7','adrMailingL8','adrMailingL9'};
    }
}