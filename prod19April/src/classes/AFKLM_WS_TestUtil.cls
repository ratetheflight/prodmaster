/**********************************************************************
 Name:  AFKLM_WS_TestUtil.cls
======================================================
Purpose: Test Utils for Web Service testing
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    21 Jul 2014		Initial development
***********************************************************************/  
@isTest
public class AFKLM_WS_TestUtil {

	/*
	 * Create Web Service Setting record in custom setting
	 */
	public static void createWSSetting(String wsName){
		
		AFKLM_WS_Settings__c wsSetting = new AFKLM_WS_Settings__c(
													Name = wsName.left(20),
													Endpoint__c = 'https://url.to.ws',
													Org_Id__c = Userinfo.getOrganizationId(),
													Password__c = 'pwd',
													Timeout__c = 10000,
													Username__c = 'user',
													Web_Service__c = wsName
													);
		insert wsSetting;
	}	

	/*
	 * Retrieve RecordType Id of RecordType 'Person Account' 
	 */
    public static Id getPersonAccountRecordTypeId(){
        return [select Id from RecordType where SobjectType = 'Account' and Name = 'Person Account'].Id;
    }	

	/*
	 * Create Test Account
	 */ 
	public static Account createAccount(){
		return createAccount(true);
	}

	public static Account createAccount(Boolean doInsert){
		Account tstAccount = new Account(
			FirstName = 'FirstName', 
			LastName = 'LastName', 
			Flying_Blue_Number__c = '001234567890',
			PersonEmail = 'test@test.nl',
			RecordTypeId = AFKLM_WS_TestUtil.getPersonAccountRecordTypeId()
		);
		if (doInsert) insert tstAccount;	
		return tstAccount;		
	}

	/*
	 * Create Test Case based on Account
	 */ 
	public static Case createCase(Account tstAccount){
		return createCase(tstAccount, true);
	}

	public static Case createCase(Account tstAccount, Boolean doInsert){
		Case tstCase = new Case(
			AccountId = tstAccount.Id, 
            Flight_Date__c = System.today(),
            Flight_Number__c = 'KL1001',
            PNR__c = 'PNRPNR',
            Seat_Nr__c = '1A'
		);
		if (doInsert) insert tstCase;	
		return tstCase;
	}  

	/*
	 * Create Test Refund based on Account
	 */ 
	public static Refund__c createRefund(Account tstAccount){
		return createRefund(tstAccount, true);
	} 

	public static Refund__c createRefund(Account tstAccount, Boolean doInsert){
		Refund__c tstRefund = new Refund__c(
			Account__c = tstAccount.Id, 
			Email__c = 'test@test.nl',
            Flight_Date__c = System.today(),
            Flight_Number__c = 'KL1001',
            Language__c = 'EN',
            Paid_Service__c = 'PAID MEAL',
            PNR__c = 'PNRPNR',
            Remarks__c = 'Test remarks',
            Seat_Number__c = '1A',
            Status__c = 'New'
		);
		if (doInsert) insert tstRefund;	
		return tstRefund;			
	}	  

}