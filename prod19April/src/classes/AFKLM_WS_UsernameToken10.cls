public class AFKLM_WS_UsernameToken10 {
	public class Security_element{
		
		public Security_element(String username, String password) {
		
			usernameToken = new UsernameToken_element(username,password);
			
		}
		public UsernameToken_element usernameToken;
		private String[] usernameToken_type_info = new String[]{'UsernameToken','http://www.w3.org/2001/XMLSchema','element','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'usernameToken'};
        
	}

    public class UsernameToken_element {
		
		public UsernameToken_element(String username, String password) {
		
			this.username = username;
			this.password = password;
		
		}
		
        public String username;
		public String password;
        private String[] username_type_info = new String[]{'Username','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] password_type_info = new String[]{'Password','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'username','password'};
    }
}