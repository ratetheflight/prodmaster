public with sharing class AFKLM_createCaseFromFeedbackController {
/**********************************************************************
 Name:  AFKLM_createCaseFromFeedbackController.cls
======================================================
Purpose: Controller class for VF Page createCaseFromFeedback.page
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma	04/02/2013		Initial development
***********************************************************************/
	public AFKLM_createCaseFromFeedbackController(){}
	
	public PageReference createCase(){
		// Get feedback id from URL
		String feedbackId = ApexPages.currentPage().getParameters().get('feedbackId');
		// If Id given, process
		if (feedbackId != null){
			// validate if this is a corect feedbackId
			feedbackId = String.escapeSingleQuotes(feedbackId).trim();
			List<KLM_Feedback__c> fbList = [select Case__c, Id, Account__c, Account__r.PersonContactId, Account__r.Flying_Blue_Number__c, Category__c, Subcategory__c, Feedback__c, Flight_Date__c, Flight_Number__c from KLM_Feedback__c where Id = :feedbackId];
			if (!fbList.isEmpty()){
				// Create case from feedback record
				KLM_Feedback__c f = fbList[0];
				Case c = new Case();
				c.AccountId = f.Account__c;
				c.ContactId = f.Account__r.PersonContactId;
				c.Origin = 'Feedback';
				c.Type = 'Feedback';
				c.Reason = f.Category__c;
				c.Sub_Reason__c = f.Subcategory__c;
				c.Subject = (f.Flight_Date__c != null ? f.Flight_Date__c.format() + ' ': '') + 
					f.Flight_Number__c + ': ' + 
					(f.Feedback__c != null ? f.Feedback__c.left(50) : '');
				c.KLM_Feedback__c = f.Id;
				c.Description = f.Feedback__c;
				List<Contact_List_Account__c> clas = [select PNR__c from Contact_List_Account__c where KLM_Feedback__c = :f.Id limit 1];
				if (!clas.isEmpty()) {
					c.PNR__c = clas[0].PNR__c;
				}
				
				RecordType rt = [select Id from RecordType where Name = 'Customer Feedback Tool Case' and SobjectType = 'Case' limit 1];
				c.RecordTypeId = rt.Id; 
				 
				insert c;

				f.Case__c = c.Id;
				update f;
				
				PageReference p = new ApexPages.StandardController(c).edit();
				p.getParameters().put('retUrl', Page.searchGateway.getUrl() + '?Id=' + f.Account__c);
				p.getParameters().put('saveUrl', Page.searchGateway.getUrl() + '?Id=' + f.Account__c);
				p.setRedirect(true);
				return p;
			}				
		}
		// Feedback record not found or invalid Id, etc, show error message
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.ERROR, Label.AFKLM_INVALID_FEEDBACK);
		ApexPages.addMessage(myMsg);
		return null;		
	}

}