/*************************************************************************************************
* File Name     :   AFKLM_ipadReturnFlightHandler
* Description   :   For filling the Return Flight Information details for iPad on Board recordtype case - ST-132
* @author       :   Narmatha
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     05/07/2016      Narmatha          Created the class
* 2.0     26/12/2016      Narmatha          Included the condition for Medical cases ST-1404
* 3.0     03/02/2017      Narmatha          Assigned ReturnFlight depature Datetime in a field(Return_Flight_Date_time__c) ST-1523
****************************************************************************************************/

Public class AFKLM_iPadReturnFlightHandler{
   
   @future(callout=true)
    public static void returnFlightInfo(Set<ID> caseIDSet){
        
        Map<String, AFKLM_WS_Manager.pnrEntry> mapOfKeyToPNR =new Map<String,AFKLM_WS_Manager.pnrEntry>();
        Map<id,Case> updateCaseIdMap=new Map<id,Case>();
        List<String> FlightList=new List<String>();
        List<Datetime> nextReturnFlightDateTime=new List<DateTime>();
        
        List<Case> caseList=[select id,Flying_Blue_number__c,description,accountid,account.Flying_Blue_Number__c,account.TA_account__c,subject,Return_Flight__c ,Return_Flight_Info__c, pnr__c,Return_Flight_Date_time__c from Case where id in:caseIDSet];
        Group highPriorityQueue= [SELECT Id, Name, DeveloperName, Email, Type FROM Group where Type='Queue' and Name='Ipob High Priority Queue'];
        for(Case cs:caseList){
            cs.Return_Flight_Info__c='';
            //Making an API call based on account's Flying Blue Number in Case object
            if(cs.account.Flying_Blue_Number__c !=null){           
                mapOfKeyToPNR=AFKLM_WS_Manager.getPNRListForFBMember(cs.account.Flying_Blue_Number__c);
                if(mapOfKeyToPNR!=null){
                    for(String pnrKey:mapOfKeyToPNR.keySet()){
                        if(mapOfKeyToPNR.get(pnrKey).departureDateTime<System.now() )
                            mapOfKeyToPNR.remove(pnrKey);
                        
                    }
                    for(AFKLM_WS_Manager.pnrEntry apiList:mapOfKeyToPNR.values())
                    {
                        //updating the Return Flight information based on PNR number
                        if(cs.pnr__c == apiList.thePNRNumber){
                            nextReturnFlightDateTime.add(apiList.departureDateTime);
                            cs.Return_Flight_Info__c+=apiList.departureDateTimeString + ' / '+apiList.theFlightNumber+ ' / '+apiList.theDeparture+' - '+apiList.theArrival+ ' / '+apiList.theUpgradeClass+ ' / Arr: '+apiList.theTimeArrival+'\n';
                            cs.Return_Flight__c=true;
                            if(cs.account.TA_account__c==null){  
                                if(cs.subject!=null){
                                    if(!(cs.Subject.contains('Pre-order Webshop')) && !(cs.Subject.contains('Medical')) && !(cs.Subject.contains('Passenger > injury')))
                                        cs.ownerid=highPriorityQueue.Id;
                                }
                                else
                                cs.ownerid=highPriorityQueue.Id;
                            }
                            updateCaseIdMap.put(cs.id,cs);
                        }
                    }
                   //To take the Last Flight Departure Datetime from Return Flight Info field
                    if(nextReturnFlightDateTime.size()>0){
                        Integer listSize=nextReturnFlightDateTime.size();
                        cs.Return_Flight_Date_time__c=nextReturnFlightDateTime[listSize-1];
                    }
                }
            }
        }
        try{
            //update the case 
            update updateCaseIdMap.values(); 
        }
        catch(Exception e){
            System.debug('Exception e'+e);
        }
    }

}