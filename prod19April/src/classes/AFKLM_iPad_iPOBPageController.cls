/**********************************************************************
 Name:  AFKLM_iPad_iPOBPageController.cls
======================================================
Purpose: Controller class for VF Page - AFKLM_iPad_iPOBPage (New and Edit Page)
         Jira ST-341
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Narmatha    16/08/2016      Initial development
    2.0     Narmatha    12/09/2016      Created a relationship with Case object ST-859 and its related subtasks
    3.0     Narmatha    03/01/2017       Included the validation for When to Close Comment
***********************************************************************/
global with sharing class AFKLM_iPad_iPOBPageController{
    //Variables
    public iPad_on_board__c ipad {get;set;}
    global Map<String, AFKLM_WS_Manager.pnrEntry> mapOfKeyToPNR {get;set;}
    public Boolean showFlightDetails{get;set;}
    public Account acc;
    public boolean isError{get;set;}
    public Boolean isValidationError;
    public Case cs;
    public String pageURL;
    public Boolean showFixedDate{get;set;}
    public Boolean showNextFlight{get;set;}
    public List<String> flightList;
    
    //Constructor
    public AFKLM_iPad_iPOBPageController(ApexPages.StandardController controller) {
        ipad=(iPad_on_board__c)controller.getRecord();
        ipad=getCurrentRecord(ipad);
        //to check whether it is opened from Account360 page, Case or Account detail page
        pageURL=ApexPages.currentPage().getUrl();
        isError=false;
        mapOfKeyToPNR=new Map<String, AFKLM_WS_Manager.pnrEntry>();
        if(ipad.Case__c!=null){
             cs=[Select id,Accountid,recordtypeid,CaseNumber from case where id=:ipad.case__c];
            if(ipad.id==null)
            ipad.PersonAccount__c=cs.Accountid; 
        }
        
        ID Accountid=ipad.PersonAccount__c;
        if(ipad.id==null){
            if(Accountid==null){
                Accountid=ApexPages.currentPage().getParameters().get('CF'+Label.AFKLM_SCS_iPad_AccField_ID+'_lkid');
                ipad.PersonAccount__c=Accountid;
            }
            else
                ipad.PersonAccount__c=Accountid;
        }
        if(accountid!=null)
        acc=[select Flying_Blue_Number__c,Name from Account where id=:Accountid Limit 1];
        if(!Test.isRunningTest() && accountid!=null && acc.Flying_Blue_Number__c!=null)
            getPNRListdata(acc.Flying_Blue_Number__c);
        //We cannot create a comment for non FB Accounts
        else if(Accountid==null || acc.Flying_Blue_Number__c==null)
        {
          isError=true;
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'We cannot create Touchpoint Messages for Non Flying-Blue members'));  
        }
        if(ipad.When_close_comment__c=='Valid for Specific Flight')
            showFlightDetails=true;
        else if(ipad.When_close_comment__c=='Fixed date')
            showFixedDate=true;
        else if(ipad.When_close_comment__c=='Valid for Next Flight')
            showNextFlight=true;
    }
    
    // Get PNR List for FB Member
    global void getPNRListdata(String fbNumber){
        mapOfKeyToPNR =AFKLM_WS_Manager.getPNRListForFBMember(fbNumber);
        flightList=new list<String>();
        if (mapOfKeyToPNR != null){
            //to remove previous date journeys
            for(String pnrKey:mapOfKeyToPNR.keySet()){
                if(mapOfKeyToPNR.get(pnrKey).departureDateTime<System.now() )
                    mapOfKeyToPNR.remove(pnrKey);
                else
                    flightList.add(pnrKey);
            }
            //To get the first upcoming Flight, when it is "Valid for Next Flight" 
            flightList.sort();
            //To select the selected fight automatically on Edit Page
            if(ipad.Specified_Flight__c!=null){
                for(AFKLM_WS_Manager.pnrEntry pr:mapOfKeyToPNR.values()){
                    String toCheckSelectedFlight=pr.theFlightNumber+pr.departureDate;
                    //Datetime dt=Datetime.valueOf(pr.departureDateTimeString);
                    if(pr.departureDateTime==ipad.Specified_Flight_Departure_Date_Time__c)
                        pr.selectedFlight=true;
                }
            }
       }
    }
    
    //for Edit method
    public iPad_on_board__c getCurrentRecord(iPad_on_board__c ipad){
        for(iPad_on_board__c recordEdit:[Select id,Class__c,Department__c, Fixed_date__c,Flight_date_reply__c,When_close_comment__c,Action_Taken__c ,
                                        Flight_number_reply__c,Specified_Flight_Departure_Date_Time__c ,Flying_Blue_Number__c,iPoBComment__c,PersonAccount__c,case__c,Purser_reply__c,Specified_Flight__c, Status__c,Status_in_UI__c from iPad_on_board__c where id=:ipad.id]){
            return recordEdit;
        }                                
        return (ipad) ;                                
    }
    
    //Save Method
    public Pagereference dosave(){
       isValidationError=false;
       if(ipad.When_close_comment__c=='Valid for Specific Flight' && mapOfKeyToPNR!=null){
           for(AFKLM_WS_Manager.pnrEntry flight:mapOfKeyToPNR.values()){
               if(flight.selectedFlight==true){
               ipad.Specified_Flight__c=flight.theFlightNumber;
               ipad.Specified_Flight_Departure_Date_Time__c=flight.departureDateTime;
               //ipad.Specified_Flight_Departure_Date_Time__c=Datetime.valueOf('2016-10-04 09:00:00');
               //ipad.Specific_Flight_Information__c=flight.theFlightNumber+flight.departureDate;
               }
            }
        }
        /*created another status field which displays in UI and original status field will be available only at backend.
          If Selected Flight Departuretime is more than 30 hours then UI status will be 'Pending' status and Integration Status field will be in "Closed" status. Then Based on Time Based workflow rule the status has been changed automatically to "New"(Both Status fields)
        */
        if(ipad.When_close_comment__c=='Valid for Specific Flight'){
            if(System.now().addhours(30) < ipad.Specified_Flight_Departure_Date_Time__c){
                ipad.Status_in_UI__c='Pending';
                ipad.status__c='Closed';
            }
            else{
                ipad.Status_in_UI__c='New';
                ipad.Status_in_UI__c='New';
            }
        }
        else
            ipad.Status_in_UI__c='New';
            
       
        //Validation to select the Specific Flight Date,if Valid for Specific Flight is selected
        if(ipad.When_close_comment__c=='Valid for Specific Flight' && ipad.Specified_Flight__c==null){
            isValidationError=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.AFKLM_SCS_ipob_valid_for_Specific_Flight));
            return null;
        } 
        //Validation to fill the Fixed date ,if Fixed Date is selected
        if(ipad.When_close_comment__c=='Fixed date' && ipad.Fixed_date__c==null){
            isValidationError=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.AFKLM_SCS_ipob_Error_msg));
            return null;
        } 
        //When to close comment is mandatory field
        if(ipad.When_close_comment__c==null){
            isValidationError=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.AFKLM_SCS_ipob_WhentoCloseComment));
            return null;
        }

        try{
            upsert ipad;
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception:'+e));
        }
        
        Pagereference newPage;
        //Redirect to Case detail page, if it is initiated from Case Detail page
        if(ipad.case__c!=null){
            newPage=new Pagereference('/'+ipad.case__c);
            }
        //Redirect to Account 360 view page, if it is initiated from Account 360 page
        else if(pageURL.contains('searchGateway')){
            newPage=new PageReference('/apex/searchGateway?id='+ipad.PersonAccount__c);
        }
        //Redirect to Account detail page, if it is initiated from Person Account Page
        else if(ipad.PersonAccount__c!=null){
            newPage=new Pagereference('/'+ipad.PersonAccount__c);
            
        }
        newPage.setRedirect(true);
        return newPage;
    } 
    
    //to display the relevant fields dynamically based on When to Close field 
    public void toshowJourneys(){
        if(ipad.When_close_comment__c=='Valid for Specific Flight'){
            showFlightDetails=true;
            showFixedDate=false;
            showNextFlight=false;
        }
        else if(ipad.When_close_comment__c=='Fixed date'){
            showFixedDate=true;
            showFlightDetails=false;
            showNextFlight=false;
        }
        else if(ipad.When_close_comment__c=='Valid for Next Flight'){
            showNextFlight=true;
            showFixedDate=false;
            showFlightDetails=false;
            if(mapOfKeyToPNR!=null){
            if( mapOfKeyToPNR.size()>0){
                ipad.Specified_Flight__c=mapOfKeyToPNR.get(flightList[0]).theFlightNumber;
                ipad.Specified_Flight_Departure_Date_Time__c=mapOfKeyToPNR.get(flightList[0]).departureDateTime;
            }
            }
        }
    }
    //to hide the fields if When to Close Comment field is empty
    public void tohideJourneys(){
       showNextFlight=false;
       showFixedDate=false;
       showFlightDetails=false;
    }
    
    //Save and New method
    public pagereference doSaveAndNew(){
        Pagereference pr=dosave(); 
        if(isValidationError)
            return pr;
        else{
            pagereference pr2;
            if(ipad.case__c!=null)
                pr2 = new Pagereference('/apex/AFKLM_iPad_iPOBPage?CF'+Label.AFKLM_SCS_iPad_CaseField_ID+'='+cs.CaseNumber +'&CF'+Label.AFKLM_SCS_iPad_CaseField_ID+'_lkid='+ipad.case__c+'&sfdc.override=1');
            else
                pr2 = new Pagereference('/apex/AFKLM_iPad_iPOBPage?CF'+Label.AFKLM_SCS_iPad_AccField_ID+'='+acc.Name+'&CF'+Label.AFKLM_SCS_iPad_AccField_ID+'_lkid='+ipad.PersonAccount__c);
            pr2.setRedirect(true);
            return pr2;
        }
    } 
    //Cancel method
    public Pagereference cancel(){
       Pagereference newPage;
       if(ipad.case__c!=null)
            newPage=new Pagereference('/'+ipad.case__c);
       else if(pageURL.contains('searchGateway'))
            newPage=new PageReference('/apex/searchGateway?id='+ipad.PersonAccount__c);
       else if(ipad.PersonAccount__c!=null)
            newPage=new Pagereference('/'+ipad.PersonAccount__c);
            
       return newPage;
    }
   
 }