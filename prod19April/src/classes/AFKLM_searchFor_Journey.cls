public class AFKLM_searchFor_Journey { 
/**********************************************************************
 Name:  AFKLM_searchFor_Journey.cls
======================================================
Purpose: Data construct for Previous, Current and Next flights
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma	11/11/2012		Initial development
***********************************************************************/
    public String theDate{get; set;}
    public String theDeparture{get; set;}
    public String theArrival{get; set;}
    public String theFlightNumber{get; set;}
    public String theCode{get; set;}
    public String theClass{get; set;}
    public String theTimeDeparture{get; set;}
    public String theTimeArrival{get; set;}
    public String thePNRNumber{get; set;}
    public String theDelayed{get; set;}
    public String theDelayedMinutes{get; set;}
    public String theDelayedDescription{get; set;}
    public String theUpgrade{get; set;}
    public String theUpgradeClass{get; set;}
    public Datetime departureDateTime{get;set;}
    
    public AFKLM_searchFor_Journey(){
		theDate = '';
		theDeparture = '';
		theArrival = '';
		theFlightNumber = '';
		theCode = '';
		theClass = '';
		theTimeDeparture = '';
		theTimeArrival = '';
		thePNRNumber = '';
		theDelayed = '';
		theDelayedMinutes = '';
		theDelayedDescription = '';
		theUpgrade = '';
		theUpgradeClass = '';    	
    }
}