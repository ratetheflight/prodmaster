public class AFKLM_searchFor_PassengerList { 
/**********************************************************************
 Name:  AFKLM_searchFor_PassengerList.cls
======================================================
Purpose: Data construct for Passenger List
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL    
    1.1     Aard-Jan v Kesteren 04/09/2013      Added the unique id                             
    1.0     Patrick Brinksma	11/11/2012		Initial development
***********************************************************************/
    public String theTierLevel {get; set;}
    public String theFlightNumber {get; set;}
    public String theFlyingBlueNumber {get; set;}
    public String theFirstName {get; set;}
    public String theLastName {get; set;}
    public String theCountry {get; set;}
    public String theBookingClass {get; set;}
    public String theClassFlown {get; set;}
    public String theCheckedInPaxStatus {get; set;}
    public String thePNRNumber {get; set;}
	public String thePaxServices {get; set;}
	public Boolean theCorporate {get; set;}
	public String theAwardOrUpgrade {get; set;}
	public String theAwardOrUpgradeFBNumber {get; set;}
	public String theSortKey {get; set;}
	public Boolean selected {get; set;}
	public String theIataCode {get; set;}
	public String theLevel {get; set;}
	public String theUniqueId {get; set;}
	
	public String getTheFlyingBlueNumberOrElseTheUniqueId() {
		return theFlyingBlueNumber != null ? theFlyingBlueNumber : theUniqueId;
	}
}