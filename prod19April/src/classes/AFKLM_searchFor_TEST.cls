@IsTest
public with sharing class AFKLM_searchFor_TEST {
/**********************************************************************
 Name:  AFKLM_searchFor_TEST.cls
======================================================
Purpose: Test class for the Searchfor page and classes
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma	11/11/2012		Initial development
***********************************************************************/
/* TODO - Need to rewrite tests due to changed code
    static testMethod void getSearchFor(){
        Test.startTest();
        
        PageReference pageRef = Page.AFKLM_VFP_searchFor;
        Test.setCurrentPage(pageRef);
      
        AFKLM_VFC_searchFor controller = new AFKLM_VFC_searchFor();
        controller.theFlyingBlueNumber = '12345678';
        controller.theFirstName = 'Stefane';
        controller.theLastName = 'Phantomasi';
        controller.wsName = 'FBRecognition';
        controller.getWebServices();
        
        controller = new AFKLM_VFC_searchFor();
        controller.theAirlineCode = 'AF';
        controller.theFlightNumber = '6164';
        controller.theFlightDate = '11/05/2012';
        controller.wsName = 'BookedPassengersList';
        controller.getWebServices();
        
        controller.search();
        
        controller.theFirstName = 'Stefane';
        controller.theLastName = 'Phantomasi';
        List<Account> accounts = [SELECT id, FirstName, LastName, Flying_Blue_Number__c, Owner.Name FROM Account WHERE Flying_Blue_Number__c = '12345678'];
        Account currentAccount = accounts[0];
        controller.currentAccount = currentAccount;
        controller.updateCurrentAccount();
        controller.goCurrentAccount();    
        
        controller.getPreviousJourneys();
        controller.getUpcomingJourneys();  
          
        Test.stopTest();
    }
*/
}