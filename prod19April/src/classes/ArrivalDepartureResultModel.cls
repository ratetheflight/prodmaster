/**                     
 * @author (s)      : David van 't Hooft
 * @description     : Model class to capture the arrival and departure information
 * @log:   08AUG2014: version 1.0
 */
global class ArrivalDepartureResultModel {

	public String mDate;
	public String day;
	public String mTime;
	public Data data;

	global class Data {
		public List<Flight> flights;
	}

	global class Flight implements Comparable {
		public String pFlightnumber;
		public String sFlightnumber;
		public String prefix;
		public String suffix;
		public String airport;
		public String scheduled;
		public String status;
		public String hall;
		public String checkinRows;
		public String carrier;
		public String followTxt = '';	//Value added on the page!
		
	    /**
		* Sorting by date and time
		* @param    compareTo Object and field which needed to be sorted
		* @return   Integer return positive or negative value to verify if the case is higher than the previous record
		*/
		global Integer compareTo(Object compareTo) {
		    Integer returnValue = -1;
		    Flight fl = (Flight)compareTo;
			if (scheduled == fl.scheduled) {
	        	returnValue = 0;
			} else if(scheduled > fl.scheduled) {
		        returnValue = 1;
			}        
		    return returnValue;       
		}       
	}
	
	public static ArrivalDepartureResultModel parse(String json) {
		return (ArrivalDepartureResultModel) System.JSON.deserialize(json, ArrivalDepartureResultModel.class);
	}	
}