/**
 * @author (s)    :	David van 't Hooft
 * @description   : schiphol service class to retrieve flight information
 * @log:  5Aug2014: test
 */
public class ArrivalDepartureService {
 	public ArrivalDepartureService() {
 	}

    private String result = '';
    
    /**
     * Get Arrival information 
     */
    public ArrivalDepartureResultModel getArrivalInfo() {
    	//retry max 2 times on failure
    	for (Integer cnt = 0; cnt < 2; cnt ++) {
    		try {
				return getArrivalDepartureInfo('http://www.schiphol.nl/nosj2va/arrivals_today_passenger.txt');
    		} catch (Exception ex) {
    			System.debug('ArrivalDepartureService failed on getArrivalInfo: '+ex.getMessage());
    		}
    	}    	
		return null;
    }
        
    /**
     * Get Departure information 
     */
    public ArrivalDepartureResultModel getDepartureInfo() {
    	//retry max 2 times on failure
    	for (Integer cnt = 0; cnt < 2; cnt ++) {
    		try {
				return getArrivalDepartureInfo('http://www.schiphol.nl/nosj2va/departures_today_passenger.txt');    	
			} catch (Exception ex) {
				System.debug('ArrivalDepartureService failed on getDepartureInfo: '+ex.getMessage());
			}
    	}
		return null;
    }    
    
   /**
    * @callType String flight/allflights
    * @flightType: departure, arrival
    * @ travelDate String format '12-05-2014'
    **/
    private ArrivalDepartureResultModel getArrivalDepartureInfo(String url) {    
        http h = new http();
        httprequest req = new httprequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
        req.setTimeout(60000);
        if (Test.isRunningTest()) {        	
	        //Rest call is not performed during test, so send back mock data
        	result = '{\"specialFlight\":null,\"flights\":[{\"registrationCode\":\"PH-BVI\",\"totalScheduledFlyingTime\":54000000,\"totalScheduledTransferTime\":4500000,\"currentLeg\":0,\"legs\":[{\"origin\":\"AMS\",\"status\":\"DELAYED\",\"destination\":\"SIN\",\"legNumber\":0,\"timeToArrival\":8592321,\"scheduledArrivalDate\":\"2014-05-12\",\"scheduledArrivalTime\":\"15:20\",\"actualArrivalTime\":null,\"estimatedArrivalTime\":\"15:59\",\"arrivalDelay\":2340000,\"scheduledDepartureDate\":\"2014-05-11\",\"scheduledDepartureTime\":\"21:00\",\"actualDepartureTime\":\"21:34\",\"estimatedDepartureTime\":null,\"timeToDeparture\":-36107679,\"departureDelay\":2040000,\"actualArrivalDate\":null,\"actualDepartureDate\":\"2014-05-11\",\"arrivalDate\":\"12-05-2014\",\"departureDate\":\"11-05-2014\",\"estimatedArrivalDate\":\"2014-05-12\",\"estimatedDepartureDate\":null},{\"origin\":\"SIN\",\"status\":\"DELAYED\",\"destination\":\"DPS\",\"legNumber\":1,\"timeToArrival\":22272321,\"scheduledArrivalDate\":\"2014-05-12\",\"scheduledArrivalTime\":\"19:15\",\"actualArrivalTime\":null,\"estimatedArrivalTime\":\"19:47\",\"arrivalDelay\":1920000,\"scheduledDepartureDate\":\"2014-05-12\",\"scheduledDepartureTime\":\"16:35\",\"actualDepartureTime\":null,\"estimatedDepartureTime\":null,\"timeToDeparture\":12672321,\"departureDelay\":1920000,\"actualArrivalDate\":null,\"actualDepartureDate\":null,\"arrivalDate\":\"12-05-2014\",\"departureDate\":\"11-05-2014\",\"estimatedArrivalDate\":\"2014-05-12\",\"estimatedDepartureDate\":null}],\"operatingFlightCode\":\"KL0835\",\"codeShares\":[\"KL6035\"],\"positions\":[{\"when\":1399872345056,\"latitude\":\"17.7768\",\"longitude\":\"92.8689\"},{\"when\":1399872405056,\"latitude\":\"17.6707\",\"longitude\":\"92.9451\"}]}]}';        	
        } else {
        	httpresponse res = h.send(req);
        	result = res.getBody();
        }
        result = result.replaceAll('date', 'mDate');
        result = result.replaceAll('time', 'mTime');
        return ArrivalDepartureResultModel.parse(result);
    }
 
 	/**
 	 * Get Result for the visual force page
 	 **/
    public string getResult() {
        return this.result;
    }
    
    /**
 	 * Se Result for the Test class
 	 **/
    public void setResult(String result) {
        this.result = result;
    } 
     
}