/**
 * @author (s)      : David van 't Hooft
 * @requirement id  : 
 * @description     : Batch to delete all the Case records that are marked to be deleted with the Delete Case field. 
 * @log:            : 21JUL2015 v1.0 
 */
global class CaseDeleteBatch implements Database.Batchable<Case> {
	
    global Iterable<Case> start(Database.BatchableContext bc){
        return getDataSet();
    }

	global LIST<Case> getDataSet() {
        List <Case> cList = [Select c.id, status, c.Delete_Case__c, c.CreatedDate From Case c where c.Delete_Case__c = true and Status = 'Closed - No Response' order by c.CreatedDate asc limit 50000];
        return cList;         
	}
    
    global void execute(Database.BatchableContext bc, LIST<Case> cList) {
    	//check if no cases added that have socal posts attached
    	List <Case> toDeleteList = new List<Case>();
    	set<Id> pSet = new Set<Id>();
    	for (case c : cList) {
    		pSet.add(c.Id);
    	}
    	
    	List <SocialPost> spList = [Select s.ParentId From SocialPost s where s.ParentId in :pSet limit 50000];
    	pSet = new Set<Id>(); 
    	for (SocialPost sp : spList) {
    		pSet.add(sp.ParentId);
    	}
    	
	    for (case c : cList) {
	    	if (!pSet.contains(c.Id)) {
	    		toDeleteList.add(c);	
	    	}
	    	
	    }
    	
   		delete toDeleteList;
   		system.debug('**** Cases Deleted ****');
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}