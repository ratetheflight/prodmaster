/**
 * @author (s)    	: David van 't Hooft
 * @requirement id  : 
 * @description   	: Scheduler class for the CaseDeletebatch
 * @log:            : 21JUL2015 v1.0 
 */
global class CaseDeleteScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    CaseDeleteBatch caseDeleteBatchTmp = new CaseDeleteBatch();
    Database.executeBatch(caseDeleteBatchTmp, 200);
  }
}