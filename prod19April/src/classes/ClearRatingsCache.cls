/**
 * @author (s)      : Ata
 * @requirement id  : 
 * @description     : Batch to delete all Cached rating code older than 1 day
 * @log:            : 2MAR2017 v1.0 
 */
global class ClearRatingsCache implements Database.Batchable<LtcRatings_Cache__c> {
    
    global Iterable<LtcRatings_Cache__c> start(Database.BatchableContext bc){
        return getDataSet();
    }

    global LIST<LtcRatings_Cache__c> getDataSet() {
        List <LtcRatings_Cache__c> cacheList = [Select id,CreatedDate From LtcRatings_Cache__c where CreatedDate < TODAY limit 50000];
        return cacheList;         
    }
    
    global void execute(Database.BatchableContext bc, LIST<LtcRatings_Cache__c> cacheList) {
        delete cacheList;
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}