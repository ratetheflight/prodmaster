public with sharing class CustomerInfo {

	public class FlyingBlueAccount {
		public Long membershipNumberAsId;
		public String emailAddressAsId;
	}

	public class Companions {
		public String title;
		public String firstName;
		public String familyName;
		public EmailAccount emailAccount;
	}

	public class EmailAccount {
		public String address;
		public String status;
	}

	public class DestinationAddress {
		public String streetHousenumber;
		public String postalCode;
		public String city;
		public String state;
	}

	public class Membership {
		public FlyingBlueMembership flyingBlueMembership;
	}

	public class PassengerInformation {
		public DestinationAddress destinationAddress;
		public String countryOfResidence;
	}

	public class Account {
		public FlyingBlueAccount flyingBlueAccount;
	}

	public class FlyingBlueMembership {
		public Long number_x;
		public String level;
		public String type;
		public String enrollPointOfSale;
		public Integer awardMilesBalance;
		public String awardMilesExpiryDate;
		public Boolean awardMilesExpiryProtection;
		public Integer levelMilesBalance;
		public Integer levelFlightsBalance;
		public String levelStartDate;
		public Integer numberOfYearsPlatinum;
		public String lastTransactionDate;
		public Integer carriedOverLevelMiles;
		public Integer carriedOverLevelFlights;
	}

	public class Individual {
		public String title;
		public String firstName;
		public String familyName;
		public EmailAccount emailAccount;
		public String dateOfBirth;
		public List<PhoneNumber> phoneNumbers;
		public List<PostalAddress> postalAddresses;
		public PassengerInformation passengerInformation;
	}

	public class FlightPreference {
		public String seat;
	}

	public class PostalAddressPreference {
		public String usageType;
	}

	public class CommunicationPreference {
		public String language;
		public PostalAddressPreference postalAddressPreference;
	}

	public Integer id;
	public Individual individual;
	public Account account;
	public Membership membership;
	public Integer profileFilledPercentage;
	public List<Companions> companions;
	public Preference preference;

	public class PostalAddress {
		public String usageType;
		public String streetHousenumber;
		public String postalCode;
		public String city;
		public String country;
		public String status;
	}

	public class Preference {
		public CommunicationPreference communicationPreference;
		public FlightPreference flightPreference;
		public LocationPreference locationPreference;
		public EmailAccount wheelchairPreference;
	}

	public class PhoneNumber {
		public String number_x;
		public String countryPrefix;
		public String countryCode;
		public String phoneType;
		public String status;
	}

	public class LocationPreference {
		public EmailAccount arrivalAirport;
		public EmailAccount departureAirport;
	}

}