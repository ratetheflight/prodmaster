/**********************************************************************
 Name:  FeedbackBeforeInsertUpdate_Test
======================================================
Purpose: 

1. Test class for trigger FeedbackBeforeInsertUpdate

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		Patrick Brinksma	25/09/2012		INITIAL DEVELOPMENT
	1.1		Patrick Brinksma	27/02/2013		Added Area / Subarea test cases
***********************************************************************/
@isTest
private class FeedbackBeforeInsertUpdate_Test {

    static testMethod void testFeedback() {
        // Build Test Data
        // Create Station Manager record
        long sysTime = System.now().getTime();
        List<Station_Manager__c> statManList = new List<Station_Manager__c>(); 
        Station_Manager__c statMan = new Station_Manager__c(Name = 'XXX',
        													Airport_Name__c = 'TESTING AIRPORT XXX',
        													Station_Manager_Email_Address__c = 'test1@' + sysTime +  '.com',
        													Station_Manager_Name__c = 'TESTING STATION MANAGER 1');
        statManList.add(statMan);
        statMan = new Station_Manager__c(Name = 'YYY',
        													Airport_Name__c = 'TESTING AIRPORT YYY',
        													Area__c = 'boarding',
        													Station_Manager_Email_Address__c = 'test2@' + sysTime +  '.com',
        													Station_Manager_Name__c = 'TESTING STATION MANAGER 2');        
        statManList.add(statMan);
        statMan = new Station_Manager__c(Name = 'YYY',
        													Airport_Name__c = 'TESTING AIRPORT YYY',
        													Area__c = 'boarding',
        													Subarea__c = 'gate_c',
        													Station_Manager_Email_Address__c = 'test3@' + sysTime +  '.com',
        													Station_Manager_Name__c = 'TESTING STATION MANAGER 3');        
        statManList.add(statMan);
        insert statManList;
        // Create feedback records
        List<feedback__c> fbList = new List<feedback__c>();
        for (Integer i=0; i<100; i++){
        	feedback__c f;
        	if (i<30){
		        f = new feedback__c(airportCode__c = 'XXX',
    								Station_Manager_Email_Address__c='',
    								area__c = 'boarding',
    								subArea__c = 'gate__c',
    								keyword__c = 'seats'
    								);        		
        	} else if (i>=30 && i < 60) {
		        f = new feedback__c(airportCode__c = 'YYY',
    								Station_Manager_Email_Address__c='',
    								area__c = 'boarding',
    								subArea__c = 'gate',
    								keyword__c = 'seats'
    								);        		
        	} else if (i>=60 && i<90) {
		        f = new feedback__c(airportCode__c = 'YYY',
    								Station_Manager_Email_Address__c='',
    								area__c = 'boarding',
    								subArea__c = 'gate_c',
    								keyword__c = 'seats'
    								);          		
        	} else {
		        f = new feedback__c(airportCode__c = 'ZZZ',
    								Station_Manager_Email_Address__c='',
    								area__c = 'boarding',
    								subArea__c = 'gate_c',
    								keyword__c = 'seats'
    								);         		
        	}
        	fbList.add(f);
        }

        // Start test and insert feedback record to test trigger
        Test.startTest();
        insert fbList;
        Test.stopTest();
        // Assert results
        fbList = [SELECT Name, airportCode__c, area__c, Subarea__c, Station_Manager_Email_Address__c FROM feedback__c];
        for (feedback__c f : fbList){
        	if (f.airportCode__c == 'XXX'){
        		System.assertEquals('test1@' + sysTime +  '.com', f.Station_Manager_Email_Address__c);
        	} else if (f.airportCode__c == 'YYY'){
        		if (f.subArea__c == 'gate_c'){
        			System.assertEquals('test3@' + sysTime +  '.com', f.Station_Manager_Email_Address__c);
        		} else {
        			System.assertEquals('test2@' + sysTime +  '.com', f.Station_Manager_Email_Address__c);
        		}
        	} else {
        		System.assertEquals(null, f.Station_Manager_Email_Address__c);
        	}
        }
    }
}