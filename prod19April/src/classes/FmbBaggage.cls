/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Baggage request process class
 * @log             : 20MAY2014: version 1.0
 *                  : 06JUL2015: version 2.0 changed Mal to Mbl
 *                  : 08SEP2015: version 2.1 added trackingTimeWindow
 *                  : 13OCT2015: version 2.2 Response Model redesigned for bagg pilot oktober 2015
 *                  : 22OCT2015: version 2.3 Queries outside loops
 */
public with sharing class FmbBaggage {
	private Set<String> airportCodes = new Set<String>();
	private Set<String> translationCodes = new Set<String>();
	private Map<String, String> translations = new Map<String, String>();
	private Map<String, Bag_Airport__c> codeBagAirportMap = new Map<String, Bag_Airport__c>();
	private Map<String, LtcAirport__c> codeLtcAirportMap = new Map<String, LtcAirport__c>();
	private Map<String, Carrier__c> codeCarrierMap = new Map<String, Carrier__c>();
	private DateTime now = Datetime.now();
	private String nowString = now.format('yyyy-MM-dd\'T\'hh:mm:ss'); 

	/**
	 * Get the baggage information
	 **/
	public FmbBaggageResponseModel getBaggage(String tagType, String issuerCode2C, String issuerCode3D, String tagSequence, String familyName, String language, String country) {
		FmbBaggageResponseModel baggageResponse;
		if (isGeneralSwitchOff()) {
			baggageResponse = new FmbBaggageResponseModel();
			baggageResponse.message = 'OutOfOrder';
			return baggageResponse;
		}

		// sanitize optional issuer parms
		if (issuerCode2C == 'null' || issuerCode2C == '' || issuerCode2C == null) { 
			issuerCode2C = null;
		} else {
			issuerCode2C = issuerCode2C.toUppercase();
		}	
		if (issuerCode3D == 'null' || issuerCode3D == '' || issuerCode3D == null) { 
			issuerCode3D = null;
		}

		FmbMblService fmbMblService = new FmbMblService();		
		FmbMblGetBaggageInfoResponseModel mblBaggageResponse = fmbMblService.retrieveBaggageInfo(tagType, issuerCode2C, issuerCode3D, tagSequence, familyName, language, country );

		if (!'NOK'.equals(mblBaggageResponse.status)) {
			baggageResponse = new FmbBaggageResponseModel(mblBaggageResponse, tagType, issuerCode2C, issuerCode3D, tagSequence, language);

			// enrich baggageResponse with PAFI flight and belt info (passengerBags typically contains one or two records)
			for (FmbMblGetBaggageInfoResponseModel.PassengerBag passengerBag : mblBaggageResponse.passengerBags) {
				Integer nrOfBags = passengerBag.bags.size();
				for (Integer i = 0; i < nrOfBags; i++) {
					FmbMblGetBaggageInfoResponseModel.Bag bag = passengerBag.bags.get(i);
					if (bag != null && bag.inbound != null) {
						FmbMblGetFLightInfoResponseModel flightInfoResponse;
						if ('NOTONBOARDRUSH'.equals(bag.missedStatus)) {
							System.debug('load flightdata from last flight in bag');
							if (bag.flights != null && bag.flights.size() > 0) {
								FmbMblGetBaggageInfoResponseModel.Flight flight = bag.flights.get(bag.flights.size() - 1);
								if (flight != null) {
									String flightDate = flight.flightDate == null ? null : flight.flightDate.replaceAll('-', '');
									flightInfoResponse = fmbMblService.retrieveFlightInfo(
											country, language, flight.operatingAirline, flight.flightNumber, flight.destination, flightDate);
								}
							}
						} else {
							System.debug('load flight data from inbound flight');
							FmbMblGetBaggageInfoResponseModel.Inbound inbound = bag.inbound;
							String flightDateGmt = inbound.flightDateGmt == null ? null : inbound.flightDateGmt.replaceAll('-', '');
							flightInfoResponse = fmbMblService.retrieveFlightInfo(
									country, language, inbound.operatingAirline, inbound.flightNumber, inbound.destination, flightDateGmt);
						}
						FmbBaggageResponseModel.Baggage baggage = baggageResponse.bagTagNumber.bags.get(i);
						baggage.departureArrivalTimeWindow = determineDepartureArrivalTimeWindow(now,
							flightInfoResponse.getLastKnownDepDat(), flightInfoResponse.getLastKnownDepTim(),
							flightInfoResponse.getLastKnownArrDat(), flightInfoResponse.getLastKnownArrTim());
						
						if (baggage.departureArrivalTimeWindow == null) {
							baggageResponse.message = 'NotAvailable';
						}

						try {
							Integer nrOfBeltInfoos = flightInfoResponse.flightId.strLegFlightDate[0].strArrInformation.strFlightPosition.strBagBelt.size();
							baggage.baggageBelts = new List<FmbBaggageResponseModel.BaggageBelt>();
							for (Integer bi = 0; bi < nrOfBeltInfoos; bi++) {
								
								FmbBaggageResponseModel.BaggageBelt belt = new FmbBaggageResponseModel.BaggageBelt();
								belt.beltNumber = flightInfoResponse.flightId.strLegFlightDate[0].strArrInformation.strFlightPosition.strBagBelt[bi].bagBelt;
								baggage.baggageBelts.add(belt);
							}
						} catch (Exception e) {
							// problem getting belt info, probably just not available in response: consume error
						}
					}
				}
			}
			
			for (FmbBaggageResponseModel.Baggage bag : baggageResponse.bagTagNumber.bags) {
				for (FmbBaggageResponseModel.Segment segment : bag.originalItinerary) {
					collectSegmentAirportCodes(segment);
				}
				if (bag.rushItinerary != null) {
					for (FmbBaggageResponseModel.Segment segment : bag.rushItinerary) {
						collectSegmentAirportCodes(segment);
					}
				}
				if (bag.lastStatus != null && bag.lastStatus.lastTrackingStation != null) {
					airportCodes.add(bag.lastStatus.lastTrackingStation.code);
				}
			}

			collectCarriers();
	        collectLtcAirports();
			collectBagAirports();
			collectTranslations(language);

			// enrich Segments with collected airport names and eligability indicators
			for (FmbBaggageResponseModel.Baggage bag : baggageResponse.bagTagNumber.bags) {
				for (FmbBaggageResponseModel.Segment segment : bag.originalItinerary) {
					fillSegment(segment);
				}

				if (bag.rushItinerary != null) {
					for (FmbBaggageResponseModel.Segment segment : bag.rushItinerary) {
						fillSegment(segment);
					}
				}

				if (bag.lastStatus != null && bag.lastStatus.lastTrackingStation != null) {
					LtcAirport__c lts = getAirport(bag.lastStatus.lastTrackingStation.code);
					bag.lastStatus.lastTrackingStation.name = translations.get(lts.airport_Name__c);
					bag.lastStatus.lastTrackingStation.city.name = translations.get(lts.city__c);
					bag.lastStatus.lastTrackingStation.city.code = lts.city_Code__c;

					Bag_Airport__c bagLts = getBagAirport(bag.lastStatus.lastTrackingStation.code);
					bag.lastStatus.lastTrackingStation.inCrisis = bagLts.in_Crisis__c;
					bag.lastStatus.lastTrackingStation.eligibleForBaggageTracking = bagLts.eligible_For_Baggage_Tracking__c;
				}
			}

			// status CheckedIn when bagtag available and in BeforeDeparture time window
			for (FmbBaggageResponseModel.Baggage bag : baggageResponse.bagTagNumber.bags) {
				if (bag.lastStatus == null && bag.departureArrivalTimeWindow === FmbBaggage.TimeWindow.BeforeDeparture) {
					bag.lastStatus = new FmbBaggageResponseModel.BaggageTrackingStatus();
					bag.lastStatus.trackingStatus = FmbBaggageResponseModel.TrackingStatus.CheckedIn;
					bag.lastStatus.trackingDateTime = nowString;
					if (bag.originalItinerary != null && bag.originalItinerary.size() > 0) {
						bag.lastStatus.lastTrackingStation = bag.originalItinerary.get(0).origin;
					}
				}
			}

			// status Delayed when rushed flight(s) and / or missed status is rushed
			for (FmbBaggageResponseModel.Baggage bag : baggageResponse.bagTagNumber.bags) {
				if ((bag.rushItinerary != null && bag.rushItinerary.size() > 0)
						|| bag.statusMessage != null) {
					if (bag.lastStatus == null) {
						FmbBaggageResponseModel.BaggageTrackingStatus stat = new FmbBaggageResponseModel.BaggageTrackingStatus();
						bag.lastStatus = stat;
					}
					bag.lastStatus.trackingStatus = FmbBaggageResponseModel.TrackingStatus.Delayed;
					bag.lastStatus.trackingDateTime = nowString;
				}
			}


		} else { // mblBaggageResponse.status = NOK
			System.debug('NOK mblBaggageResponse=' + mblBaggageResponse);
			baggageResponse = new FmbBaggageResponseModel();
			if ('NO_BAG_FOUND'.equals(mblBaggageResponse.statusKey)
					|| 'baggage_not_found'.equals(mblBaggageResponse.statusKey)) {
				FmbBaggageResponseModel.Error error = new FmbBaggageResponseModel.Error();
				baggageResponse.error = error;
				error.code = '404';
				error.description = 'Mismatch';
			} else {
				baggageResponse.message = 'NotAvailable';
			}
			System.debug('NOK baggageResponse=' + baggageResponse);
		}

		return baggageResponse;
	}

	// detemine if the FMB application should respond to client requests or is 'switched off'
	private boolean isGeneralSwitchOff() {
		boolean isOff = false;
		if (FmbSettings__c.getInstance('default') != null) {
        	isOff =  FmbSettings__c.getInstance('default').General_Switch_Off__c;
         }
		return isOff;
	}
	
	private void fillSegment(FmbBaggageResponseModel.Segment segment) {

		LtcAirport__c orig = getAirport(segment.origin.code);
		segment.origin.name = translations.get(orig.airport_Name__c);
		segment.origin.city.name = translations.get(orig.city__c);
		segment.origin.city.code = orig.city_Code__c;

		Bag_Airport__c bagOrig = getBagAirport(segment.origin.code);
		segment.origin.inCrisis = bagOrig.in_Crisis__c;
		segment.origin.eligibleForBaggageTracking = bagOrig.eligible_For_Baggage_Tracking__c;

		LtcAirport__c dest = getAirport(segment.destination.code);
		segment.destination.name = translations.get(dest.airport_Name__c);
		segment.destination.city.name = translations.get(dest.city__c);
		segment.destination.city.code = dest.city_Code__c;

		Bag_Airport__c bagDest = getBagAirport(segment.destination.code);
		segment.destination.inCrisis = bagDest.in_Crisis__c;
		segment.destination.eligibleForBaggageTracking = bagDest.eligible_For_Baggage_Tracking__c;

		Carrier__c carrier = getCarrier(segment.flight.carrier.code);
		segment.flight.carrier.eligibleForBaggageTracking = carrier.eligible_For_Baggage_Tracking__c;
	}

	/**
     * Retrieve the LTC airport object
     **/
    private LtcAirport__c getAirport(String iataCode) {
        LtcAirport__c ltcAirport = codeLtcAirportMap.get(iataCode);
        if (ltcAirport == null) {
        	ltcAirport = new LtcAirport__c();
        }
        if (ltcAirport.airport_Name__c == null) {
            ltcAirport.airport_Name__c = '';
        }
        if (ltcAirport.city__c == null) {
            ltcAirport.city__c = '';
        }        
        return ltcAirport;
    }

	/**
     * Retrieve the FMB airport object
     **/
    private Bag_Airport__c getBagAirport(String iataCode) {
        Bag_Airport__c airport = codeBagAirportMap.get(iataCode);

        if (airport == null) {
			airport = new Bag_Airport__c(); 
	        airport.name = iataCode;

	        // TODO optionally change default eligibility for E2E testing
	        airport.eligible_For_Baggage_Tracking__c = false;
	        // airport.eligible_For_Baggage_Tracking__c = true;
	        airport.in_Crisis__c = false;
        }
        return airport;
    }

	private void collectSegmentAirportCodes(FmbBaggageResponseModel.Segment segment) {
		airportCodes.add(segment.origin.code);
		airportCodes.add(segment.destination.code);
	}

	/**
	 * collect Ltc Airports and translate-able labels
	 **/
    private void collectLtcAirports() {

    	List<LtcAirport__c> airportList = [
        	select a.Name, a.Airport_Name__c, a.Airport_Code__c, a.City__c, a.City_Code__c
       		from LtcAirport__c a 
       		where a.Name in :airportCodes
    	];
        if (airportList != null) {
            for (LtcAirport__c airp : airportList) {
            	codeLtcAirportMap.put(airp.Airport_Code__c, airp);
            	translationCodes.add(airp.Airport_Name__c);
            	translationCodes.add(airp.City__c);
            	System.debug('add LTC airport=' + airp.name + ' - ' + airp);
            }
        }
    }
    
    /**
     * collect the FMB airport objects
     **/
    private void collectBagAirports() {
        List<Bag_Airport__c> airportList = [
            Select a.name, a.eligible_For_Baggage_Tracking__c, a.in_Crisis__c
            From Bag_Airport__c a 
            where a.name in :airportCodes
        ];

        if (airportList != null) {
            for (Bag_Airport__c airp : airportList) {
            	codeBagAirportMap.put(airp.name, airp);
            	System.debug('add FMB airport=' + airp.name + ' - ' + airp);
            }
        }
    }    
    /**
     * collect the Carrier__c objects
     **/
    private void collectCarriers() {
        List<Carrier__c> carrierList = [
            Select c.name, c.eligible_For_Baggage_Tracking__c
            From Carrier__c c 
        ];

        if (carrierList != null) {
            for (Carrier__c c : carrierList) {
            	codeCarrierMap.put(c.name, c);
            	System.debug('add carrier=' + c.name + ' - ' + c);
            }
        }
    }

  	/**
     * get translations, default en
     **/  
    private void collectTranslations(String acceptLanguage) {
		// default engels
		List<Translation__c> translationList = [
			select t.Translated_Text__c, Translation_Key__c
			from Translation__c t 
			where t.Translation_Key__c in :translationCodes
			and t.Language__c =: 'en'
		];
        if (translationList != null && !translationList.isEmpty()) {
        	for (Translation__c tr : translationList) {
        		translations.put(tr.Translation_Key__c, tr.Translated_Text__c);
        		System.debug('added en translation key=' + tr.Translation_Key__c + ' txt=' + tr.Translated_Text__c);
        	}
        }

		String language = LtcUtil.getLanguage(acceptLanguage);
		if (!'en'.equals(language)) {
			translationList = [
				select t.Translated_Text__c, Translation_Key__c
				from Translation__c t 
				where t.Translation_Key__c in :translationCodes
				and t.Language__c =: language
			];
	        if (translationList != null && !translationList.isEmpty()) {
	        	for (Translation__c tr : translationList) {
	        		translations.put(tr.Translation_Key__c, tr.Translated_Text__c);
	        		System.debug('added ' + language + ' translation key=' + tr.Translation_Key__c + ' txt=' + tr.Translated_Text__c);
	        	}
	        }
		}
    }

	/**
     * Retrieve the carrier object
     **/
    private Carrier__c getCarrier(String carrierCode) {
        Carrier__c carrier = codeCarrierMap.get(carrierCode);
        
        if (carrier == null) {
        	carrier = new Carrier__c();
        	
	        // TODO optionally change default eligibility for E2E testing
	        carrier.eligible_For_Baggage_Tracking__c = false;
	        //carrier.eligible_For_Baggage_Tracking__c = true;
	        carrier.Name = carrierCode;
        }
        return carrier;
    }

    public enum TimeWindow {
		BeforeDeparture,
		BetweenDepartureArrival,
		AtArrival,
		DelayedAllowedToShow
	}

	/**
	 * construct departure / arrival dates from the input parameters and use these to determine the departure and arrival time window
	 * all date / times in UTC
	 */
	public TimeWindow determineDepartureArrivalTimeWindow(Datetime now, String lastKnownDepDat, String lastKnownDepTim, String lastKnownArrDat, String lastKnownArrTim) {
		System.debug('determineDepartureArrivalTimeWindow ' + now.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss') + ' dep=' + lastKnownDepDat + ' ' + lastKnownDepTim + ' arr=' + lastKnownArrDat + ' ' +  lastKnownArrTim);
		Integer x_Minutes_Before_Off_Block = 5;
		Integer y_Minutes_After_On_Block = 5;
		Integer z_Minutes_Untill_Show_Delayed = 15;
		if (FmbSettings__c.getInstance('default') != null) {
                x_Minutes_Before_Off_Block =  FmbSettings__c.getInstance('default').x_Minutes_Before_Off_Block__c.intValue();
                y_Minutes_After_On_Block =  FmbSettings__c.getInstance('default').y_Minutes_After_On_Block__c.intValue();
                z_Minutes_Untill_Show_Delayed =  FmbSettings__c.getInstance('default').z_Minutes_Untill_Show_Delayed__c.intValue();
         }
		Datetime datetimeDep; Datetime datetimeArr;
		try {
			datetimeDep = getDateTime(lastKnownDepDat, lastKnownDepTim);
			datetimeArr = getDateTime(lastKnownArrDat, lastKnownArrTim);
			System.debug('determineDepartureArrivalTimeWindow datetimeDep=' + datetimeDep.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss') + ' datetimeArr=' + datetimeArr.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss'));
		} catch (Exception e) {
			System.debug(e);
			return null;
		}
		TimeWindow result = null;
		if (datetimeDep.addMinutes(x_Minutes_Before_Off_Block * -1) > now) {
			result = TimeWindow.BeforeDeparture;
		} 
		else if (datetimeDep.addMinutes(x_Minutes_Before_Off_Block * -1) <= now && datetimeArr.addMinutes(y_Minutes_After_On_Block) > now) {
			result = TimeWindow.BetweenDepartureArrival;
		}
		else if (datetimeArr.addMinutes(y_Minutes_After_On_Block) <= now && datetimeArr.addMinutes(z_Minutes_Untill_Show_Delayed) > now) {
			result = TimeWindow.AtArrival;
		}		
		else if (datetimeArr.addMinutes(z_Minutes_Untill_Show_Delayed).getTime() <= now.getTime()) {
			result = TimeWindow.DelayedAllowedToShow;
		} 
		System.debug('determineDepartureArrivalTimeWindow result=' + result);
		return result;
	}

   /**
    *    "date": "20150903",
    *    "time": "1845",
    */
	private Datetime getDateTime(String dateString, String timeString) {
		Integer y = Integer.valueOf(dateString.substring(0,4));
		Integer m = Integer.valueOf(dateString.substring(4,6));
		Integer d = Integer.valueOf(dateString.substring(6));
		Integer h = Integer.valueOf(timeString.substring(0,2));
		Integer min = Integer.valueOf(timeString.substring(2));		

		Datetime result = DateTime.newInstanceGmt(y, m, d, h, min, 0);
		return result;
	}
}