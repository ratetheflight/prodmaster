/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Rest Baggage response model
 * @log             : 20MAY2014: version 1.0
 *                  : 06JUL2015: version 2.0 changed Mal to Mbl
 *                  : 08SEP2015: version 2.1 added timewindow determination
 *                  : 16SEP2015: version 2.3 Response Model redesigned for bagg pilot october 2015
 */
global with sharing class FmbBaggageResponseModel {

	public IATATagReference bagTagNumber;
	public Error error;
	public String message;
	
	private Map<String, String> bagStatusFmbStatusMap = new Map<String, String>();
	
	public String getJson() {
		if (error != null) {
			return '{' +
	        	'"error": ' + error.toString() +
			'}';
		} else {
			System.debug('bagTagNumber=' + bagTagNumber);
	        return '{' +
	        	'"bagTagNumber": ' + (bagTagNumber == null ? null : bagTagNumber.toString()) +
			'}';
		}
	}

	public class IATATagReference {
		public String tagType;
		public String issuerCode2C;
		public String issuerCode3D;
		public String tagSequence;
		public List<Baggage> bags;

		public IATATagReference(String tagType, String issuerCode2C, String issuerCode3D, String tagSequence) {
			this.tagType = tagType;
			this.issuerCode2C = issuerCode2C;
			this.issuerCode3D = issuerCode3D;
			this.tagSequence = tagSequence;
			this.bags = new List<Baggage>();
		}

        public override String toString() { 
            return '{' +
        		'"issuerCode2C": "' + issuerCode2C + '", ' +
        		'"issuerCode3D": "' + issuerCode3D + '", ' +
        		'"tagSequence": "' + tagSequence + '", ' + 
        		'"tagType": "' + tagType + '", ' +
        		'"bags": ' + JSON.serialize(bags) + 
            '}';
        }
	}
	
	public class Baggage {
		public String id;
		public Boolean isDelayed = false;
		public Boolean notificationCheckBeltInfo = false;
		public Boolean notificationCheckMonitorInfo = false;
		public MissedStatus statusMessage;
		public Passenger passenger;
		public List<Segment> originalItinerary;
		public List<Segment> rushItinerary;
		public BaggageTrackingStatus lastStatus;
		public List<BaggageBelt> baggageBelts;
		public FmbBaggage.TimeWindow departureArrivalTimeWindow; 
	}

	public class Passenger {
		public String firstName = '';
		public String lastName = '';
	}

	public class Segment {
		public Flight flight;
		public Airport origin;
		public Airport destination;
	}

	public class Flight {
		public String flightDate = '';
		public String flightNumber = '';
		public Carrier carrier;
	}

	public class Carrier {
		public String code;
		public Boolean eligibleForBaggageTracking;
	}

	public class Airport {
		public String code = '';
		public String name = '';		
		public Boolean eligibleForBaggageTracking = false;
		public Boolean inCrisis = false;
		public City city; 
	}

	public class City {
		public String code = '';
		public String name = '';
	}

	public class BaggageBelt {
		public String beltNumber;
	}
	
	public class BaggageTrackingStatus {
		public String trackingDateTime;
		public TrackingStatus trackingStatus;
		public Airport lastTrackingStation;
	}

	public class Error {
		public String code;
		public String description;
	}

	private void collectStatusses() {
		List<Fmb_Tracking_status__c> statusList = [
			select Bag_status__c, Fmb_status__c
			from Fmb_Tracking_status__c
		];
		
		if (statusList != null && !statusList.isEmpty()) {
			for (Fmb_Tracking_Status__c bagstatus: statusList) {
				bagStatusFmbStatusMap.put(bagstatus.Bag_status__c, bagstatus.Fmb_status__c);
			}
		}
	}
	
	private void cleanupStatusses() {
		bagStatusFmbStatusMap = null;
	}
	
	public static FmbBaggageResponseModel parse(String json) {
		return (FmbBaggageResponseModel) System.JSON.deserialize(json, FmbBaggageResponseModel.class);
	}

	public FmbBaggageResponseModel() {
	}
	
	public FmbBaggageResponseModel(FmbMblGetBaggageInfoResponseModel mblBagInfo, 
			String tagType, String issuerCode2C, String issuerCode3D, String tagSequence, String language) {
		
		collectStatusses();
		
		this.bagTagNumber = new IATATagReference(tagType, issuerCode2C, issuerCode3D,tagSequence);
		for (FmbMblGetBaggageInfoResponseModel.PassengerBag passengerBag : mblBagInfo.passengerBags) {

			for (FmbMblGetBaggageInfoResponseModel.Bag bag : passengerBag.bags) {
				Baggage baggage = new Baggage();
				baggage.id = '' + bag.bobId;
				baggage.statusMessage = getMissedStatus(bag.missedStatus);
				this.bagTagNumber.bags.add(baggage);

				Passenger passenger = new Passenger();
				passenger.firstName = passengerBag.firstName;
				passenger.lastName = passengerBag.lastName;
				baggage.originalItinerary = new List<Segment>();
				baggage.passenger = passenger;

				if (bag.lastTracking != null) {
					BaggageTrackingStatus bts = new BaggageTrackingStatus();
					bts.trackingDateTime = bag.lastTracking.date_time;
					bts.trackingStatus = determineTrackingStatus(bag.lastTracking.status);
					baggage.lastStatus = bts;

					Airport lts = new Airport();
					lts.code = bag.lastTracking.station;
					City ltsCity = new City();
					lts.city = ltsCity;
					bts.lastTrackingStation = lts;
				}

				for (FmbMblGetBaggageInfoResponseModel.Flight mblFlight : bag.flights) {
					Segment segment = new Segment();

					Flight flight = new Flight();
					flight.flightDate = mblFlight.flightDate;
					flight.flightNumber = mblFlight.flightNumber;

					Carrier carrier = new Carrier();
					carrier.code = mblFlight.operatingAirline;
					flight.carrier = carrier;

					Airport origin = new Airport();
					origin.code = mblFlight.origin;
					City origCity = new City();
					origin.city = origCity;

					Airport destination = new Airport();
					destination.code = mblFlight.destination;
					City destCity = new City();
					destination.city = destCity;

					segment.flight = flight;
					segment.origin = origin;
					segment.destination = destination;

					baggage.originalItinerary.add(segment);
				}

				if (bag.flightRushes != null && bag.flightRushes.size() > 0) {
					baggage.rushItinerary = new List<Segment>();
					for (FmbMblGetBaggageInfoResponseModel.FlightRush mblFlight : bag.flightRushes) {
						Segment segment = new Segment();

						Flight flight = new Flight();
						flight.flightDate = mblFlight.flightDate;
						flight.flightNumber = mblFlight.flightNumber;

						Carrier carrier = new Carrier();
						carrier.code = mblFlight.operatingAirline;
						flight.carrier = carrier;

						Airport origin = new Airport();
						origin.code = mblFlight.origin;
						City origCity = new City();
						origin.city = origCity;

						Airport destination = new Airport();
						destination.code = mblFlight.destination;
						City destCity = new City();
						destination.city = destCity;

						segment.flight = flight;
						segment.origin = origin;
						segment.destination = destination;

						baggage.rushItinerary.add(segment);
					}
				}
			}
		}
		cleanupStatusses();
	}

	public enum MissedStatus {
		DD,
		Rush,
		NotOnBoardRush,
		NotOnBoard
	}

	private MissedStatus getMissedStatus(String status) {
		MissedStatus match;
		for (MissedStatus ms: MissedStatus.values()) {
		    if (ms.name().equalsIgnoreCase(status)) {
		        match = ms;
		        break;
		    }
	    }
	    return match;
	}

	public enum TrackingStatus {
		CheckedIn,
		InSorting,
		Delivery,
		Boarding,
		Boarded,
		OffLoaded,
		Delayed,
		Unknown
	}

	private TrackingStatus getTrackingStatus(String status) {
		TrackingStatus match = TrackingStatus.Unknown;
		for (TrackingStatus ts: TrackingStatus.values()) {
		    if (ts.name().equalsIgnoreCase(status)) {
		        match = ts;
		        break;
		    }
	    }
	    return match;
	}

	public TrackingStatus determineTrackingStatus(String status) {
		TrackingStatus result = TrackingStatus.Unknown;
		if (status != null && !status.equals('')) {
			String fmbTs = bagStatusFmbStatusMap.get(status);
			if (fmbTs != null) {
				result = getTrackingStatus(fmbTs);
			} else {
				if (Test.isRunningTest()) {
					result = getTrackingStatus(status);
				}
			}
		}
		
		System.debug('trackingstatus=' + status + ' leads to result=' + result );
		return result;
	}
}