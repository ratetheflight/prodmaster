/**
 * @author (s)      : Mees Witteman
 * @description     : Rest Baggage response model
 * @log             : 13OCT2015: version 2.3 Response Model redesigned for bagg pilot october 2015
**/
@isTest
private class FmbBaggageResponseModelTest {

	@isTest	static void testObjectCreation() {
		Datetime dt = Datetime.newInstanceGmt(2015, 1, 1, 12, 0, 0);
		FmbBaggageResponseModel rm = new FmbBaggageResponseModel();

		FmbBaggageResponseModel.Airport orig = new FmbBaggageResponseModel.Airport();
		orig.code = 'AMS';
		orig.name = 'Amsterdam';
		orig.eligibleForBaggageTracking = true;
		
		FmbBaggageResponseModel.Airport dest = new FmbBaggageResponseModel.Airport();
		dest.eligibleForBaggageTracking = true;
		dest.code = 'CDG';
		dest.name = 'Parijs';
		
		FmbBaggageResponseModel.Carrier carrier = new FmbBaggageResponseModel.Carrier();
		carrier.code = 'KL';
		carrier.eligibleForBaggageTracking = true;

		rm.bagTagNumber = new FmbBaggageResponseModel.IATATagReference('0', 'KL', '074', '321456');
		rm.bagTagNumber.bags = new List<FmbBaggageResponseModel.Baggage>();

		FmbBaggageResponseModel.Baggage bag = new FmbBaggageResponseModel.Baggage();
		bag.id = 'GEEN IDEE';
		rm.bagTagNumber.bags.add(bag);

		FmbBaggageResponseModel.Passenger pas = new FmbBaggageResponseModel.Passenger();
		pas.firstName = 'JAN';
		pas.lastName = 'JANSENSS';
		bag.passenger = pas;

		bag.originalItinerary = new List<FmbBaggageResponseModel.Segment>();
		//bag.rushItinerary = new List<FmbBaggageResponseModel.Segment>();

		bag.lastStatus = new FmbBaggageResponseModel.BaggageTrackingStatus();
		bag.lastStatus.trackingDateTime = '2015-01-01T08:00:00';
		bag.lastStatus.trackingStatus = FmbBaggageResponseModel.TrackingStatus.Boarded;
		bag.lastStatus.lastTrackingStation = orig;

		FmbBaggageResponseModel.Flight flight = new FmbBaggageResponseModel.Flight();
		flight.flightDate = '2015-01-01';
		flight.flightNumber = 'KL1001';
		flight.carrier = carrier;

		FmbBaggageResponseModel.Segment segment = new FmbBaggageResponseModel.Segment();
		segment.destination = dest;
		segment.origin = orig;
		segment.flight = flight;

		bag.originalItinerary.add(segment);

		String expected = '{"bagTagNumber": {"issuerCode2C": "KL", "issuerCode3D": "074", "tagSequence": "321456", "tagType": "0", "bags": [{"statusMessage":null,"rushItinerary":null,"passenger":{"lastName":"JANSENSS","firstName":"JAN"},"originalItinerary":[{"origin":{"name":"Amsterdam","inCrisis":false,"eligibleForBaggageTracking":true,"code":"AMS","city":null},"flight":{"flightNumber":"KL1001","flightDate":"2015-01-01","carrier":{"eligibleForBaggageTracking":true,"code":"KL"}},"destination":{"name":"Parijs","inCrisis":false,"eligibleForBaggageTracking":true,"code":"CDG","city":null}}],"notificationCheckMonitorInfo":false,"notificationCheckBeltInfo":false,"lastStatus":{"trackingStatus":"Boarded","trackingDateTime":"2015-01-01T08:00:00","lastTrackingStation":{"name":"Amsterdam","inCrisis":false,"eligibleForBaggageTracking":true,"code":"AMS","city":null}},"isDelayed":false,"id":"GEEN IDEE","departureArrivalTimeWindow":null,"baggageBelts":null}]}}';
		System.assertEquals(expected, rm.getJson());

		FmbBaggageResponseModel model = FmbBaggageResponseModel.parse(expected);

		System.assertEquals(expected, model.getJson());
	}

	@isTest	static void testLazyGetters() {
		FmbMblGetFlightInfoResponseModel rm = new FmbMblGetFlightInfoResponseModel();
		System.assertEquals('', rm.getLastKnownDepDat());
		System.assertEquals('', rm.getLastKnownDepTim());
		System.assertEquals('', rm.getLastKnownArrDat());
		System.assertEquals('', rm.getLastKnownArrTim());
	}
}