/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Rest API interface FMB for the retrieving of the baggage info
 * @log             :  7MAR2014: version 1.0
 * @log             : 13OCT2015: version 2.0 bagg pilot october 2015
 */
@RestResource(urlMapping='/Baggage/*')
global class FmbBaggageRestInterface {

	@HttpGet
    global static FmbBaggageResponseModel getBaggage() {
        LtcUtil.logMemoryUsage('start getBaggage rest call');
        String errorMsg = '';
        FmbBaggageResponseModel baggageResponseModel;
        try {
            String tagType = RestContext.request.params.get('tagType');
            String issuerCode2C = RestContext.request.params.get('issuerCode2C');
            String issuerCode3D = RestContext.request.params.get('issuerCode3D');
            String tagSequence = RestContext.request.params.get('tagSequence');            
            String familyName = RestContext.request.params.get('familyName');

            String language = FmbLocaleUtil.getlanguage(RestContext.request.headers.get('Accept-Language'));
            String country = FmbLocaleUtil.getCountry(RestContext.request.headers.get('Accept-Language'));
            System.debug('Accept-Language=' + RestContext.request.headers.get('Accept-Language'));

            baggageResponseModel = new FmbBaggage().getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, familyName, language, country);
            if (baggageResponseModel.error != null && '404'.equals(baggageResponseModel.error.code)) {
                RestResponse res = RestContext.response;
                res.statusCode = 404;
            } 

            // make bagtag response empty if message has been filled somewhere along filling the response
            if (baggageResponseModel.message != null) {
                System.debug('make bagtag empty because of message baggageResponseModel=' + baggageResponseModel);
                baggageResponseModel.bagTagNumber = null;
            }
        } catch (Exception ex) {
            baggageResponseModel = new FmbBaggageResponseModel();
            baggageResponseModel.message = 'NotAvailable';

            String msg = ex.getMessage() + ' ' + ex.getStackTraceString();
            System.debug(LoggingLevel.ERROR, 'msg=' + msg);
        }
        LtcUtil.logMemoryUsage('getBaggage just before return response to client'); 
        return baggageResponseModel;
    }
}