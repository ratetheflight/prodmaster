/**
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Mbl Baggage restinterface test class
 * @log             : 20MAY2014: version 1.0
 * @log             : 06JUL2015: version 2.0 changed Mal to Mbl
 * @log             : 13OCT2015: version 2.0 bagg pilot october 2015 
 */
@isTest 
private class FmbBaggageRestInterfaceTest {

    /**
     * Test automatic status change on the mock data
     **/
    static testMethod void baggageLoopTest() { 
        
        String tagType = '0';
        String issuerCode2C = 'KL';
        String issuerCode3D = '074';
        String tagSequence = '999999';
        FmbBaggage baggage = new FmbBaggage();

        FmbBaggageResponseModel result = baggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testb', 'en', 'GB');
        System.debug(LoggingLevel.INFO, result.toString());
        System.assert(result.toString().contains('CheckedIn'));
        
        result = baggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testb', 'en', 'GB');
        System.debug(LoggingLevel.INFO, result.toString());
        System.assert(result.toString().contains('InSorting'));
        
        result = baggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testb', 'en', 'GB');
        System.debug(LoggingLevel.INFO, result.toString());
        System.assert(result.toString().contains('Delivery'));
        
        result = baggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testb', 'en', 'GB');
        System.debug(LoggingLevel.INFO, result.toString());
        System.assert(result.toString().contains('Boarded'));
        
        result = baggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testb', 'en', 'GB');
        System.debug(LoggingLevel.INFO, result.toString());
        System.assert(result.toString().contains('InSorting'));

    }

    /** 
     * Test Baggage MblServiceresponse Translator
     **/
    static testMethod void baggageTest() {
        String tagType = '0';
        String issuerCode2C = 'KL';
        String issuerCode3D = '074';
        String tagSequence = '000001';
        FmbBaggage fmbBaggage = new FmbBaggage();
        FmbBaggageResponseModel responseModel = fmbBaggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testa Jansen', 'nl', 'NL');
        System.debug('responseModel.bagTagNumber.bags[0].toString()=' + responseModel.bagTagNumber.bags[0].toString());
        System.assert(responseModel.bagTagNumber.bags[0].toString().contains('CheckedIn'));

        responseModel = fmbBaggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testa Jansen', 'nl', 'NL');
        System.debug('responseModel.bagTagNumber.bags[0].toString()=' + responseModel.bagTagNumber.bags[0].toString());
        System.assert(responseModel.bagTagNumber.bags[0].toString().contains('InSorting'));

        responseModel = fmbBaggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'testa Jansen', 'nl', 'NL');
        System.debug('responseModel.bagTagNumber.bags[0].toString()=' + responseModel.bagTagNumber.bags[0].toString());
        System.assert(responseModel.bagTagNumber.bags[0].toString().contains('InSorting'));
    }

    static testMethod void baggageRealTest() {
        String tagType = '0';
        String issuerCode2C = 'KL';
        String issuerCode3D = '074';
        String tagSequence = '000001';
        FmbBaggage fmbBaggage = new FmbBaggage();
     
        try {
            FmbBaggageResponseModel responseModel = fmbBaggage.getBaggage(tagType, issuerCode2C, issuerCode3D, tagSequence, 'real thing failure', 'nl', 'NL');
            System.assert(false);
        } catch (Exception e) {
            System.debug(LoggingLevel.INFO, e);
            System.assert(e != null);
        }
    }

    /**
     * Code coverage tests for the dummy data
     **/
    static testMethod void testMblDummyData() {
        FmbMblDummyData fmbMblDummyData = new FmbMblDummyData();
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000001', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000002', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000003', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000004', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000005', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000006', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000007', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000008', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000009', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000010', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000011', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000012', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000013', 'xxxTest') != null);
        System.assert(fmbMblDummyData.getDummyResponseByBagTagNumber('000099', 'xxxTest') != null);
    }

    static testMethod void fourOoooooowFourTest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/Baggage';
        req.addParameter('tagType', '0');
        req.addParameter('issuerCode2C', 'KL');
        req.addParameter('issuerCode3D', '074');
        req.addParameter('tagSequence', '000001');
        req.addParameter('familyName', 'test404');

        req.addHeader('Accept-Language', 'nl-NL');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        FmbBaggageResponseModel responseModel = FmbBaggageRestInterface.getBaggage();
        
        System.assertEquals('404', responseModel.error.code);
        System.assertEquals('Mismatch', responseModel.error.description);
    }    
    
    /**
     * Test Mbl GetBaggage Service responsemodel
     **/
    static testMethod void testParseFmbMblGetBaggageInfoResponseModel() {
        String json = '{'+
        '\"status\": \"OK\",'+
        '\"statusMessage\": \"Request processed successfully\",'+
        '\"statusCode\": \"0\",'+
        '\"passengerBags\": ['+
            '{'+
                '\"firstName\": \"IZABELLAMRS\",'+
                '\"lastName\": \"BERGEL\",'+
                '\"frequentFlyerId\": \"AF1755979395\",'+
                    '\"bags\": ['+
                        '{'+
                            '\"bobId\": 9264,'+
                            '\"weight\": {'+
                                '\"value\": 14,'+
                                '\"weightUnit\": \"K\"'+
                            '},'+
                            '\"finalDestination\": \"CDG\",'+
                            '\"lastTracking\": {'+
                                '\"station\": \"AMS\",'+
                                '\"location\": \"PKGF\",'+
                                '\"infra\": \"F\",'+
                                '\"date_Time\": \"2013-09-14T22:51:17\"'+
                            '}'+
                        '}'+
                    ']'+
                '},'+
            '{'+
                '\"firstName\": \"IZABELLAMRS\",'+
                '\"lastName\": \"BERGEL\",'+
                '\"frequentFlyerId\": \"AF1755979395\",'+
                    '\"bags\": ['+
                        '{'+
                            '\"bobId\": 9264,'+
                            '\"weight\": {'+
                                '\"value\": 14,'+
                                '\"weightUnit\": \"K\"'+
                            '},'+
                            '\"finalDestination\": \"CDG\",'+
                            '\"lastTracking\": {'+
                                '\"station\": \"AMS\",'+
                                '\"location\": \"PKGF\",'+
                                '\"infra\": \"F\",'+
                                '\"date_Time\": \"2013-09-14T22:51:17\"'+
                            '},'+
                            '\"flights\": ['+
                                '{'+
                                    '\"operatingAirline\": "AF",'+
                                    '\"flightNumber\": "257",'+
                                    '\"origin\": "SIN",'+
                                    '\"destination\": "CDG",'+
                                    '\"originatingStationDateGMT\": "2014-07-09",'+
                                    '\"cancelled\": false,'+
                                    '\"authorityToLoad\": false,'+
                                    '\"screeningRequired\": false,'+
                                    '\"bagDeleted\": false,'+
                                    '\"loadingStatus\": false'+
                                '}'+
                            ']'+
                        '}'+
                    ']'+
                '}'+
            ']'+
        '}';
        FmbMblGetBaggageInfoResponseModel responseModel = FmbMblGetBaggageInfoResponseModel.parse(json);
        System.assertEquals('OK', responseModel.status);    
        System.assertEquals('Request processed successfully', responseModel.statusMessage);
        System.assertEquals('0', responseModel.statusCode); 
    }
    
    /**
     * Test Mbl GetBaggageInfo responsemodel
     **/    
    static testMethod void testParseFmbMblGetBaggageInfoResponseModelWithRush() {
        String json = '{'+
        '    \"status\": \"OK\",'+
        '    \"statusMessage\": \"Request processed successfully\",'+
        '    \"statusCode\": \"0\",'+
        '    \"passengerBags\": ['+
        '        {'+
        '            \"firstName\": \"FRANCESCO\",'+
        '            \"lastName\": \"COLLER\",'+
        '            \"bags\": ['+
        '                {'+
        '                    \"bobId\": 384416256,'+
        '                    \"weight\": {'+
        '                        \"weightUnit\": \"K\",'+
        '                        \"value\": 24'+
        '                    },'+
        '                    \"finalDestination\": \"VRN\",'+
        '                    \"lastTracking\": {'+
        '                        \"station\": \"CDG\",'+
        '                        \"location\": \"PKGG\",'+
        '                        \"infra\": \"G\",'+
        '                        \"dateTime\": \"2015-01-09T08:05:02\",'+
        '                        \"status\": \"ONBOARD\"'+
        '                    },'+
        '                    \"tag\": {'+
        '                        \"type\": \"0\",'+
        '                        \"issuer3d\": \"057\",'+
        '                        \"sequence\": \"550778\"'+
        '                    },'+
        '                    \"tagRush\": {'+
        '                        \"type\": \"0\",'+
        '                        \"issuer3d\": \"057\",'+
        '                        \"sequence\": \"550778\"'+
        '                    },'+
        '                    \"missedStatus\": \"NOTONBOARDRUSH\",'+
        '                    \"flights\": ['+
        '                        {'+
        '                            \"operatingAirline\": \"AF\",'+
        '                            \"flightNumber\": \"217\",'+
        '                            \"origin\": \"BOM\",'+
        '                            \"destination\": \"CDG\",'+
        '                            \"flightDate\": \"2015-01-08\",'+
        '                            \"cancelled\": false,'+
        '                            \"authorityToLoad\": true,'+
        '                            \"screeningRequired\": false,'+
        '                            \"bagDeleted\": false,'+
        '                            \"reconciliation\": {'+
        '                                \"passengerStatus\": \"B\",'+
        '                                \"classOfTravel\": \"Y\",'+
        '                                \"canceled\": false'+
        '                            },'+
        '                            \"loadingStatus\": false'+
        '                        },'+
        '                        {'+
        '                            \"operatingAirline\": \"AF\",'+
        '                            \"flightNumber\": \"1176\",'+
        '                            \"origin\": \"CDG\",'+
        '                            \"destination\": \"VRN\",'+
        '                            \"flightDate\": \"2015-01-08\",'+
        '                            \"cancelled\": false,'+
        '                            \"authorityToLoad\": true,'+
        '                            \"screeningRequired\": false,'+
        '                            \"bagDeleted\": false,'+
        '                            \"reconciliation\": {'+
        '                                \"passengerStatus\": \"B\",'+
        '                                \"classOfTravel\": \"Y\",'+
        '                                \"canceled\": false'+
        '                            },'+
        '                            \"loadingStatus\": false'+
        '                        }'+
        '                    ],'+
        '                    \"flightRushes\": ['+
        '                        {'+
        '                            \"operatingAirline\": \"AF\",'+
        '                            \"flightNumber\": \"1676\",'+
        '                            \"origin\": \"CDG\",'+
        '                            \"destination\": \"VRN\",'+
        '                            \"flightDate\": \"2015-01-09\",'+
        '                            \"cancelled\": false,'+
        '                            \"authorityToLoad\": true,'+
        '                            \"screeningRequired\": false,'+
        '                            \"bagDeleted\": false,'+
        '                            \"loadingStatus\": false'+
        '                        }'+
        '                    ],'+
        '                    \"inbound\": {'+
        '                        \"operatingAirline\": \"AF\",'+
        '                        \"flightNumber\": \"217\",'+
        '                        \"origin\": \"BOM\",'+
        '                        \"destination\": \"CDG\",'+
        '                        \"flightDateGmt\": \"2015-01-07\",'+
        '                        \"cancelled\": false,'+
        '                        \"authorityToLoad\": false,'+
        '                        \"screeningRequired\": false,'+
        '                        \"bagDeleted\": false,'+
        '                        \"loadingStatus\": false'+
        '                    }'+
        '                }'+
        '            ]'+
        '        }'+
        '    ]'+
        '}';
        FmbMblGetBaggageInfoResponseModel obj = FmbMblGetBaggageInfoResponseModel.parse(json);
        System.assert(obj != null);
    }
    
    static testMethod void testRetrieveFlightInfo() {
         FmbMblService service = new  FmbMblService();
         // AA triggers dummy record
         FmbMblGetFLightInfoResponseModel result = service.retrieveFlightInfo('nl', 'NL', 
                'AA', '9999', 'BLT',  '2015-07-07');
         System.assert(result.toString().contains('datTUheadFLT=2015-07-07'));
    }
    
    /**
     * Test Mbl FmbMblGetBaggageInfoResponseModel dummy
     **/
    static testMethod void testParseMblBaggResponseModel() {
        FmbMblGetBaggageInfoResponseModel responseModel = new FmbMblGetBaggageInfoResponseModel();
        responseModel.initializeDummy();
        
        System.assertEquals('OK', responseModel.status);    
        System.assertEquals('Request processed successfully', responseModel.statusMessage);
        System.assertEquals('0', responseModel.statusCode); 
    }
    
    /**
     * Test Mbl mock data through the rest interface.
     **/
    static testMethod void baggageRestInterfaceMblTest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/Baggage';
        req.addParameter('tagType', '0');
        req.addParameter('issuerCode2C', 'KL');
        req.addParameter('issuerCode3D', '074');
        req.addParameter('tagSequence', '000001');
        req.addParameter('familyName', 'xxx Jansen');

        req.addHeader('Accept-Language', 'nl-NL');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        FmbBaggageResponseModel model = FmbBaggageRestInterface.getBaggage();
        String now = Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss');
        for (FmbBaggageResponseModel.Baggage bag : model.bagTagNumber.bags) {
            bag.lastStatus.trackingDateTime = now;
        }
        String json = model.getJson();
        String expected = '{"bagTagNumber": {"issuerCode2C": "KL", "issuerCode3D": "074", "tagSequence": "000001", "tagType": "0", "bags": [{"statusMessage":"NotOnBoardRush","rushItinerary":[{"origin":{"name":null,"inCrisis":false,"eligibleForBaggageTracking":false,"code":"AMS","city":{"name":null,"code":null}},"flight":{"flightNumber":"1676","flightDate":"2014-07-09","carrier":{"eligibleForBaggageTracking":false,"code":"AF"}},"destination":{"name":null,"inCrisis":false,"eligibleForBaggageTracking":false,"code":"CDG","city":{"name":null,"code":null}}}],"passenger":{"lastName":"familyName","firstName":"FRANCESCO"},"originalItinerary":[{"origin":{"name":null,"inCrisis":false,"eligibleForBaggageTracking":false,"code":"AMS","city":{"name":null,"code":null}},"flight":{"flightNumber":"1234","flightDate":"2014-07-09","carrier":{"eligibleForBaggageTracking":false,"code":"KL"}},"destination":{"name":null,"inCrisis":false,"eligibleForBaggageTracking":false,"code":"CDG","city":{"name":null,"code":null}}}],"notificationCheckMonitorInfo":false,"notificationCheckBeltInfo":false,"lastStatus":{"trackingStatus":"Delayed","trackingDateTime":"' + now + '","lastTrackingStation":{"name":null,"inCrisis":false,"eligibleForBaggageTracking":false,"code":"AMS","city":{"name":null,"code":null}}},"isDelayed":false,"id":"384416256","departureArrivalTimeWindow":"DelayedAllowedToShow","baggageBelts":null}]}}';
        System.assertEquals(expected, json);
    }
    
    static testMethod void baggageRestInterfaceMblMultipleBagsTest() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/Baggage';
        req.addParameter('tagType', '0');
        req.addParameter('issuerCode2C', 'KL');
        req.addParameter('issuerCode3D', '074');
        req.addParameter('tagSequence', '000001');
        req.addParameter('familyName', 'big test');

        req.addHeader('Accept-Language', 'nl-NL');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        FmbBaggageResponseModel model = FmbBaggageRestInterface.getBaggage();
        System.debug('model=' + model.getJson());
        System.assertEquals(4, model.bagTagNumber.bags.size());


        for (FmbBaggageResponseModel.Baggage bag : model.bagTagNumber.bags) {
            //System.assertEquals(expected, json);
        }

    }

    /**
     * Test FmbMbl GetBaggageInfoResponseModel GetBaggageInfo dummy scenario call
     **/
    static testMethod void getFmbMblGetBaggageInfoResponseModelForScenarioTest() {
        FmbMblDummyData mdd = new FmbMblDummyData();
        FmbMblGetBaggageInfoResponseModel response =  mdd.getFmbMblGetBaggageInfoResponseModelForScenario('182817', 'testa');
        System.assertEquals('dummy firstName', response.passengerBags.get(0).firstName );
    }

    static testMethod void  testParseFmbMblGetFlightInfoResponseModel() {
        String json = '{'+
        '    \"status\": \"OK\",'+
        '    \"statusMessage\": \"Request processed successfully\",'+
        '    \"statusCode\": \"0\",'+
        '    \"flightId\": {'+
        '        \"airCod\": \"KL\",'+
        '        \"flightNumber\": \"706\",'+
        '        \"datTUhead\": \"20150608\",'+
        '        \"typDatTime\": \"L\",'+
        '        \"irgInfASM\": {'+
        '            \"strFLTFlightId\": {'+
        '                \"airCodFLT\": \"KL\",'+
        '                \"flightNumberFLT\": \"706\",'+
        '                \"datTUheadFLT\": \"20150608\"'+
        '            },'+
        '            \"flightCancelIndic\": \"N\"'+
        '        },'+
        '        \"strFlightInformation\": {'+
        '            \"flightType\": \"O\",'+
        '            \"haulType\": \"LC\",'+
        '            \"strItinary\": ['+
        '                {'+
        '                    \"station\": \"GIG\"'+
        '                },'+
        '                {'+
        '                    \"station\": \"AMS\"'+
        '                }'+
        '            ]'+
        '        },'+
        '        \"strLegFlightDate\": ['+
        '            {'+
        '                \"strLegInf\": {'+
        '                    \"operstatus\": \"S\",'+
        '                    \"servType\": \"J\"'+
        '                },'+
        '                \"strAircraftInf\": {'+
        '                    \"aircraftRegistration\": \"PHBQG\",'+
        '                    \"aircraftType\": \"772\",'+
        '                    \"ownerAirlineCode\": \"KL\",'+
        '                    \"physAircraftConfigVersion\": \"C035M283\",'+
        '                    \"physFreightVersion\": \"P006L000\",'+
        '                    \"operationalVersion\": \"C035M283\"'+
        '                },'+
        '                \"strDepInformation\": {'+
        '                    \"strFlightQuality\": {},'+
        '                    \"strFlightPosition\": {'+
        '                        \"iataairportCode\": \"GIG\"'+
        '                    },'+
        '                    \"strTimDep\": {'+
        '                        \"utcSchedDateDep\": \"20150608\",'+
        '                        \"utcSchedTimDep\": \"2345\",'+
        '                        \"utcLastKnownDateDep\": \"20150608\",'+
        '                        \"utcLastKnownTimDep\": \"2345\",'+
        '                        \"lastKnownType\": \"S\",'+
        '                        \"lastKnownOrigin\": \"S\",'+
        '                        \"ltschedDateDep\": \"20150608\",'+
        '                        \"ltschedTimDep\": \"2045\",'+
        '                        \"ltlastKnownDateDep\": \"20150608\",'+
        '                        \"ltlastKnownTimDep\": \"2045\"'+
        '                    }'+
        '                },'+
        '                \"strArrInformation\": {'+
        '                    \"strFlightQuality\": {},'+
        '                    \"strFlightPosition\": {'+
        '                        \"pierCode\": \"E\",'+
        '                        \"preJetwayDisembark\": \";;E\",'+
        '                        \"strGates\": ['+
        '                            {'+
        '                                \"gateArr\": \"E06\"'+
        '                            }'+
        '                        ],'+
        '                        \"strBagBelt\": ['+
        '                            {'+
        '                                \"bagBelt\": \"014\"'+
        '                            }'+
        '                        ],'+
        '                        \"iataairportCode\": \"AMS\"'+
        '                    },'+
        '                    \"strTimArr\": {'+
        '                        \"utcSchedDateArr\": \"20150609\",'+
        '                        \"utcSchedTimArr\": \"1115\",'+
        '                        \"utcLastKnownDateArr\": \"20150609\",'+
        '                        \"utcLastKnownTimArr\": \"1115\",'+
        '                        \"lastKnownType\": \"S\",'+
        '                        \"lastKnownOrigin\": \"S\",'+
        '                        \"ltschedDateArr\": \"20150609\",'+
        '                        \"ltschedTimArr\": \"1315\",'+
        '                        \"ltlastKnownDateArr\": \"20150609\",'+
        '                        \"ltlastKnownTimArr\": \"1315\"'+
        '                    }'+
        '                }'+
        '            }'+
        '        ],'+
        '        \"iataairportCode\": \"AMS\"'+
        '    }'+
        '}';
        FmbMblGetFlightInfoResponseModel obj = FmbMblGetFlightInfoResponseModel.parse(json);
        System.assert(obj != null);
    }


    static testMethod void testParseFmbResponseError() {
        String json = '{ '+
            '"error": {' +
                ' "description": "noBaggageFound",' +
                '"code": "404"' +
            '}, '+ 
            ' "bagTagNumber": null '  + 
        '}';
        
        FmbBaggageResponseModel obj = FmbBaggageResponseModel.parse(json);
        System.assert(obj != null);
    }

}