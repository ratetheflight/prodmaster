/**
 * @author (s)      : Mees Witteman
 * @log             : 13OCT2015: version 2.0 bagg pilot october 2015
 *
 * In this JUnit test the scenario from the specs is implemented:
 * utcLastKnownTimDep	13:00	Off Block
 * utcLastKnownTimArr	15:00	On Block
 * 		
 * 	UTC time	Which TimeWindow
 * 	12:00	BeforeDeparture
 * 	12:54	BeforeDeparture
 * 	12:55	BetweenDepartureArrival
 * 	15:00	BetweenDepartureArrival
 * 	15:04	BetweenDepartureArrival
 * 	15:05	AtArrival
 * 	15:14	AtArrival
 * 	15:15	DelayedAllowedToShow
**/
@isTest private class FmbBaggageTest {

	@isTest static void determineDepartureArrivalTimeWindow_BeforeDeparture_1() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 12, 0, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.BeforeDeparture, ts);
	}

	@isTest static void determineDepartureArrivalTimeWindow_BeforeDeparture_2() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 12, 54, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.BeforeDeparture, ts);
	}

	@isTest static void determineDepartureArrivalTimeWindow_BetweenDepartureArrival_1() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 12, 55, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.BetweenDepartureArrival, ts);
	}	

	@isTest static void determineDepartureArrivalTimeWindow_BetweenDepartureArrival_2() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 15, 0, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.BetweenDepartureArrival, ts);
	}
	@isTest static void determineDepartureArrivalTimeWindow_BetweenDepartureArrival_3() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 15, 4, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.BetweenDepartureArrival, ts);
	}

	@isTest static void determineDepartureArrivalTimeWindow_AtArrival_1() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 15, 5, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.AtArrival, ts);
	}

	@isTest static void determineDepartureArrivalTimeWindow_AtArrival_2() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 15, 14, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.AtArrival, ts);
	}


	@isTest static void determineDepartureArrivalTimeWindow_DelayedAllowedToShow() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 15, 15, 0);
		String lastKnownDepDat = '20150101'; String lastKnownDepTim = '1300'; String lastKnownArrDat = '20150101'; String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(FmbBaggage.TimeWindow.DelayedAllowedToShow, ts);
	}

		
	@isTest static void determineDepartureArrivalTimeWindow_Illegal_Date_1() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 12, 0, 0);
		String lastKnownDepDat = 'aaaaaaaa'; 
		String lastKnownDepTim = '1300';
		String lastKnownArrDat = '20150101';
		String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(null, ts);
	}
			
	@isTest static void determineDepartureArrivalTimeWindow_Illegal_Date_2() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 12, 0, 0);
		String lastKnownDepDat = '20150101'; 
		String lastKnownDepTim = 'aaaa';
		String lastKnownArrDat = '20150101';
		String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(null, ts);
	}			
	@isTest static void determineDepartureArrivalTimeWindow_Illegal_Date_3() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 12, 0, 0);
		String lastKnownDepDat = '20150101'; 
		String lastKnownDepTim = '1300';
		String lastKnownArrDat = '!@#$';
		String lastKnownArrTim = '1500';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(null, ts);
	}			
	@isTest static void determineDepartureArrivalTimeWindow_Illegal_Date_4() {
		Datetime now = Datetime.newInstanceGmt(2015, 1, 1, 12, 0, 0);
		String lastKnownDepDat = '20150101'; 
		String lastKnownDepTim = '';
		String lastKnownArrDat = '20150101';
		String lastKnownArrTim = '*&^%';
		FmbBaggage.TimeWindow ts = new FmbBaggage().determineDepartureArrivalTimeWindow(now, lastKnownDepDat, lastKnownDepTim, lastKnownArrDat, lastKnownArrTim);
		System.assertEquals(null, ts);
	}
	
}