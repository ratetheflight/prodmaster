/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Utility class for locale 
 * @log:    2JUN2014: version 1.0
 */
public with sharing class FmbLocaleUtil {
	
	// acceptLanguage param expected to start with 2 characters for the language and then 1 separator before the country
	public static String getCountry(String acceptLanguage) {
		String country = 'GB';
		if (acceptLanguage != null && acceptLanguage.length() >= 5) {
			country = acceptLanguage.substring(3, 5);
		}
		return country;
	}
	
	public static String getlanguage(String acceptLanguage) {
		String language = 'en'; // default to en
		if (acceptLanguage != null && acceptLanguage.length() >= 2) {
			language = acceptLanguage.substring(0, 2);
		}
		return language;
	}
}