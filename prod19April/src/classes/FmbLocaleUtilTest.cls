/**                     
 * @author (s)      : Mees Witteman
 * @description     : Testclass for FmbLocaleUtil
 * @log:    2JUN2014: version 1.0
 */
 @isTest
public with sharing class FmbLocaleUtilTest {
	
	static testMethod void determineCountryTest() {
        System.assertEquals('NL', FmbLocaleUtil.getCountry('nl-NL'));
        System.assertEquals('FR', FmbLocaleUtil.getCountry('fr-FR_EUR'));
        System.assertEquals('GB', FmbLocaleUtil.getCountry('fr-G'));
        System.assertEquals('GB', FmbLocaleUtil.getCountry(null));
    }
    
	static testMethod void determineLanguageTest() {
        System.assertEquals('nl', FmbLocaleUtil.getLanguage('nl-NL'));
        System.assertEquals('en', FmbLocaleUtil.getLanguage(''));
        System.assertEquals('en', FmbLocaleUtil.getLanguage('n'));
        System.assertEquals('en', FmbLocaleUtil.getLanguage(null));
    }
}