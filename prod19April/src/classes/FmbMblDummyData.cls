/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : MBL Baggage dummy data
 * @log             : 2JUN2014: version 1.0
 * @log             : 3JUL2015: version 2: Mal to Mbl
 */
public with sharing class FmbMblDummyData {
	public FmbMblDummyData() {
	}
	
	public FmbMblGetBaggageInfoResponseModel getFmbMblGetBaggageInfoResponseModelForScenario(String tagSequence, String lastName) {
		FmbMblGetBaggageInfoResponseModel rm =  new FmbMblGetBaggageInfoResponseModel();

		if (lastName.contains('404')) {
			rm.status = 'NOK';
			rm.statusKey = 'NO_BAG_FOUND';
		} else {
			rm.initializeDummy(); 
			rm.passengerBags.get(0).lastName = lastName;
			loopStatus(tagSequence, lastName, rm);
		}
		return rm;
	}

    /**
     * Loop all status values for the tagSequence and configure responseModel with dummy data
     **/
    private void loopStatus(String tagSequence, String lastName, FmbMblGetBaggageInfoResponseModel rm) {
        FmbDummyMalSeq__c fmbDummyMalSeq;
        List<FmbDummyMalSeq__c> fmbDummyMalSeqList = [select iataTagSequence__c, Status_Order__c from FmbDummyMalSeq__c where iataTagSequence__c =: tagSequence];
        if (fmbDummyMalSeqList != null && !fmbDummyMalSeqList.isEmpty()) {
            fmbDummyMalSeq = fmbDummyMalSeqList[0];
            if (++fmbDummyMalSeq.Status_Order__c > getNumberOfStatusses(lastName)) {
                fmbDummyMalSeq.Status_Order__c = 1;
            } 
        } else {
            fmbDummyMalSeq = new FmbDummyMalSeq__c(iataTagSequence__c=tagSequence, Status_Order__c=1); 
        }
        upsert fmbDummyMalSeq;

		if (lastname.startsWith('testa')) {
	        if (fmbDummyMalSeq.Status_Order__c==1) {
				rm.passengerBags[0].bags[0].lastTracking.status = 'CheckedIn';
	        } else if (fmbDummyMalSeq.Status_Order__c==2) {    
				rm.passengerBags[0].bags[0].lastTracking.status = 'InSorting';
	        } else if (fmbDummyMalSeq.Status_Order__c==3) {    
				rm.passengerBags[0].bags[0].lastTracking.status = 'InSorting';
	        } else if (fmbDummyMalSeq.Status_Order__c==4) {    
				rm.passengerBags[0].bags[0].lastTracking.status = 'InSorting';
	        } else if (fmbDummyMalSeq.Status_Order__c==5) {    
				rm.passengerBags[0].bags[0].lastTracking.status = 'Delayed';
	        }
		} else if (lastname.startsWith('testb')) {
			if (fmbDummyMalSeq.Status_Order__c==1) {
				rm.passengerBags[0].bags[0].lastTracking.status = 'CheckedIn';
	        } else if (fmbDummyMalSeq.Status_Order__c==2) {    
				rm.passengerBags[0].bags[0].lastTracking.status = 'InSorting';
	        } else if (fmbDummyMalSeq.Status_Order__c==3) {    
				rm.passengerBags[0].bags[0].lastTracking.status = 'Delivery';
	        } else if (fmbDummyMalSeq.Status_Order__c==4) {    
				rm.passengerBags[0].bags[0].lastTracking.status = 'Boarded';
				rm.passengerBags.get(0).firstName = 'true/ /' + rm.passengerBags.get(0).firstName; 
	        } else if (fmbDummyMalSeq.Status_Order__c==5) {
				// firstName starting with true will make the rest service fake the belt info: baggageBeltsAvailable = true 
				rm.passengerBags[0].bags[0].lastTracking.status = 'InSorting'; // actual beltstatus with belts
				rm.passengerBags.get(0).firstName = 'true/2/' + rm.passengerBags.get(0).firstName; 
				rm.passengerBags[0].bags[0].lastTracking.station = 'CDG';
	        }
		} else if (lastname.startsWith('testc')) {
			rm.passengerBags = null;
		} else if (lastname.startsWith('testd')) {
			rm.passengerBags[0].bags[0].lastTracking.status = 'SHDDSHLAHSH(IIWKRWJRWJWJRNRNDZNIU***&@)';
		}
    }
    
    private Integer getNumberOfStatusses(String lastName) {
    	if (lastname.startsWith('testc')) {
    		return 1;
    	} else if (lastname.startsWith('testd')) {
    		return 1;
    	} else {
    		return 5;
    	}
    }	
	
	/**
	 * Get a mocked response depending on the bagtagnumber
	 **/
	public String getDummyResponseByBagTagNumber(String tagSequence, String familyName) {
		String result = '';
		if ('000001'.equalsIgnoreCase(tagSequence)) {
			result = getCheckedIn('AMS', 'CDG', tagSequence, 'familyName', '2014-07-09T21:51:17', '2014-07-09', 'KL1234');
		} else if ('000002'.equalsIgnoreCase(tagSequence)) {
			result = getInSorting('AMS', 'CDG', tagSequence, familyName, '2014-07-09T21:55:16', '2014-07-09', 'KL1234');
		} else if ('000003'.equalsIgnoreCase(tagSequence)) {
			result = getSeen('AMS', 'CDG', tagSequence, familyName, '2014-07-09T21:59:43', '2014-07-09', 'KL1234');
		} else if ('000004'.equalsIgnoreCase(tagSequence)) {
			result = getSorted('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:10:27', '2014-07-09', 'KL1234');
		} else if ('000005'.equalsIgnoreCase(tagSequence)) {
			result = getInDelivery('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:12:33', '2014-07-09', 'KL1234');
		} else if ('000006'.equalsIgnoreCase(tagSequence)) {
			result = getDeliveredOnApron('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:15:14', '2014-07-09', 'KL1234');
		} else if ('000007'.equalsIgnoreCase(tagSequence)) {
			result = getOnBoard('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:17:25', '2014-07-09', 'KL1234');
		} else if ('000008'.equalsIgnoreCase(tagSequence)) {
			result = getBoarded('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:18:57', '2014-07-09', 'KL1234');
		} else if ('000009'.equalsIgnoreCase(tagSequence)) {
			result = getBoardedBelt('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:19:57', '2014-07-09', 'KL1234', '', false);
		} else if ('000010'.equalsIgnoreCase(tagSequence)) {
			result = getBoardedBelt('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:22:26', '2014-07-09', 'KL1234', '2', false);
		} else if ('000011'.equalsIgnoreCase(tagSequence)) {
			result = getBoardedBelt('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:43:54', '2014-07-09', 'KL1234', '2', true);
		} else if ('000012'.equalsIgnoreCase(tagSequence)) {
        	result = getNOK('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:57:44', '2014-07-09', 'KL1234');        	
		} else if ('000013'.equalsIgnoreCase(tagSequence)) {
			result = getUnknownStatus('AMS', 'CDG', tagSequence, familyName, '2014-07-09T22:59:44', '2014-07-09', 'KL1234');
		} else {
			result = loopStatus(tagSequence, familyName, 'nl' );
		}			
		return result;
	}
	
    /**
     * Loop all status values for the tagSequence
     **/
    private String loopStatus(String tagSequence, String familyName, String language) {
        String result = '';
        FmbDummyMalSeq__c fmbDummyMalSeq;
    
        List<FmbDummyMalSeq__c> fmbDummyMalSeqList = [select iataTagSequence__c, Status_Order__c from FmbDummyMalSeq__c where iataTagSequence__c =: tagSequence];
        if (fmbDummyMalSeqList!=null && !fmbDummyMalSeqList.isEmpty()) {
            fmbDummyMalSeq = fmbDummyMalSeqList[0];
            if (++fmbDummyMalSeq.Status_Order__c > 10) {
                fmbDummyMalSeq.Status_Order__c = 1;
            } 
        } else {
            fmbDummyMalSeq = new FmbDummyMalSeq__c(iataTagSequence__c=tagSequence, Status_Order__c=1); 
        }
        upsert fmbDummyMalSeq;

        if (fmbDummyMalSeq.Status_Order__c==1) {
			result = getCheckedIn('AMS', 'CDG', tagSequence, familyName, '2014-07-09T21:51:17', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==2) {    
			result = getInSorting('AMS', 'CDG', tagSequence, familyname, '2014-07-09T21:55:16', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==3) {    
			result = getSeen('AMS', 'CDG', tagSequence, familyname, '2014-07-09T21:59:43', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==4) {    
			result = getSorted('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:10:27', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==5) {    
			result = getInDelivery('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:12:33', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==6) {    
			result = getDeliveredOnApron('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:15:14', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==7) {    
			result = getOnBoard('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:17:25', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==8) {    
			result = getBoarded('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:18:57', '2014-07-09', 'KL1234');
        } else if (fmbDummyMalSeq.Status_Order__c==9) {    
			result = getBoardedBelt('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:41:17', '2014-07-09', 'KL1234', '1', false);
        } else if (fmbDummyMalSeq.Status_Order__c==10) {    
			result = getBoardedBelt('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:56:17', '2014-07-09', 'KL1234', '1', true);
        } else {
        	result = getNOK('AMS', 'CDG', tagSequence, familyname, '2014-07-09T22:57:44', '2014-07-09', 'KL1234');        	
        }
        return result;
    }   
	
	/**
	 * CheckedIn response
	 **/
	private String getCheckedIn(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'CheckedIn', 'Baggage is checked in', lastTrackingDateTime, travelDate, flightNumber);	
	}

	/**
	 * response InSorting
	 **/
	private String getInSorting(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'InSorting', 'Baggage being sorted', lastTrackingDateTime, travelDate, flightNumber);	
	}
	
	/**
	 * response Seen
	 **/
	private String getSeen(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'InSorting', 'Baggage being sorted', lastTrackingDateTime, travelDate, flightNumber);	
	}
	
	/**
	 * response Sorted
	 **/
	private String getSorted(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'InSorting', 'Baggage on its way to flight', lastTrackingDateTime, travelDate, flightNumber);	
	}
	
	/**
	 * response In delivery
	 **/
	private String getInDelivery(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'In delivery', 'Baggage on its way to flight', lastTrackingDateTime, travelDate, flightNumber);	
	}
	
	/**
	 * response Delivered on apron
	 **/
	private String getDeliveredOnApron(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'InSorting', 'Baggage is being delivered on board', lastTrackingDateTime, travelDate, flightNumber);	
	}

	/**
	 * response On board
	 **/
	private String getOnBoard(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'On board', 'Baggage is being delivered on board', lastTrackingDateTime, travelDate, flightNumber);	
	}
	
	/**
	 * response Boarded
	 **/
	private String getBoarded(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'Boarded', 'Baggage is being delivered on board', lastTrackingDateTime, travelDate, flightNumber);	
	}

	/**
	 * response Boarded with Belt
	 **/
	private String getBoardedBelt(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber, String belt, boolean flag) {
		return getDummyStatus(origin, destination, destination, tagSequence, familyname, 'Boarded', 'Baggage is being delivered on board', lastTrackingDateTime, travelDate, flightNumber, belt, flag);	
	}

	/**
	 * response NOK
	 **/
	private String getNOK(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'NOK', 'Bag not found', lastTrackingDateTime, travelDate, flightNumber);	
	}

	/**
	 * response Unknown
	 **/
	private String getUnknownStatus(String origin, String destination, String tagSequence, String familyname, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, origin, tagSequence, familyname, 'ThisStatusIsUnknown', 'What happened', lastTrackingDateTime, travelDate, flightNumber);	
	}

	/**
	 * Json mock response of the MBL
	 **/
	private String getDummyStatus(String origin, String destination, String station, String tagSequence, String familyname, String status, String statusMessage, String lastTrackingDateTime, String travelDate, String flightNumber) {
		return getDummyStatus(origin, destination, station, tagSequence, familyname, status, statusMessage, lastTrackingDateTime, travelDate, flightNumber, null, false);
	}
		
	/**
	 * Json mock response of the MBL GetBaggageInfo, to be tweaked to our likings and as needed for demo and testing purposes
	 **/
	private String getDummyStatus(String origin, String destination, String station, String tagSequence, String familyname, 
			String status, String statusMessage, String lastTrackingDateTime, String travelDate, String flightNumber, 
			String belt, Boolean beltsAvailable) {
				
		Integer bagNr = Integer.valueOf(tagSequence);
		String jsonResponse = '{'+
		    '"status": "OK",'+
		    '"statusMessage": "' + statusMessage + '",'+
		    '"statusCode": "0",'+
		    '"passengerBags": ['+
		        '{'+
		            '"firstName": "FRANCESCO",'+
		            '"lastName": "' + familyname + '",'+
		            '"bags": ['+
		               ' {'+
		                    '"bobId": 384416256,'+
		                    '"weight": {'+
		                       ' "weightUnit": "K",'+
		                        '"value": 24'+
		                    '},'+
		                    '"finalDestination": "VRN",'+
		                    '"lastTracking": {'+
		                        '"station": "' + station + '",'+
		                        '"location": "PKGG",'+
		                        '"infra": "G",'+
		                        '"dateTime": "2015-01-09T09:30:44",'+
		                        '"status": "' + status + '"'+
		                    '},'+
		                    '"tag": {'+
		                        '"type": "0",'+
		                        '"issuer3d": "074",'+
		                        '"sequence": "' + tagSequence + '"'+
		                    '},'+
		                    '"tagRush": {'+
		                        '"type": "0",'+
		                        '"issuer3d": "057",'+
		                        '"sequence": "550778"'+
		                    '},'+
		                    '"missedStatus": "NOTONBOARDRUSH",'+
		                    '"flights": ['+
		                        '{'+
		                            '"operatingAirline": "KL",'+
		                            '"flightNumber": "1234",'+
		                            '"origin": "' + origin + '",'+
		                            '"destination": "' + destination + '",'+
		                            '"flightDate": "' + travelDate + '",'+
		                            '"cancelled": false,'+
		                            '"authorityToLoad": true,'+
		                            '"screeningRequired": false,'+
		                            '"bagDeleted": false,'+
		                            '"reconciliation": {'+
		                                '"passengerStatus": "B",'+
		                                '"classOfTravel": "Y",'+
		                                '"canceled": false'+
		                            '},'+
		                            '"loadingStatus": false'+
		                        '}'+
		                    '],'+
		                    '"flightRushes": ['+
		                        '{'+
		                            '"operatingAirline": "AF",'+
		                            '"flightNumber": "1676",'+
		                            '"origin": "' + origin + '",'+
		                            '"destination": "' + destination + '",'+
		                            '"flightDate": "' + travelDate + '",'+
		                            '"cancelled": false,'+
		                            '"authorityToLoad": true,'+
		                            '"screeningRequired": false,'+
		                            '"bagDeleted": false,'+
		                            '"loadingStatus": false'+
		                        '}'+
		                    '],'+
		                    '"inbound": {'+
		                        '"operatingAirline": "AA",'+
		                        '"flightNumber": "217",'+
		                        '"origin": "' + origin + '",'+
		                        '"destination": "' + destination + '",'+
		                        '"flightDateGmt": "2015-01-07",'+
		                        '"cancelled": false,'+
		                        '"authorityToLoad": false,'+
		                        '"screeningRequired": false,'+
		                        '"bagDeleted": false,'+
		                        '"loadingStatus": false'+
		                    '}'+
		                '}'+
		            ']'+
		        '}'+
		    ']'+
		'}';
		
		return jsonResponse;
	}

	public FmbMblGetFLightInfoResponseModel getFmbMblGetFlightInfoResponseModelForScenario(
			String airlineCode, String flightNumber, String iataAirportCode,  String datTUhead, Boolean needBeltInfo) {
		String json = '';
		if (needBeltInfo) {
			json = '{'+
		'    \"status\": \"OK\",'+
		'    \"statusMessage\": \"Request processed successfully\",'+
		'    \"statusCode\": \"0\",'+
		'    \"flightId\": {'+
		'        \"airCod\": \"'+ airlineCode +'\",'+
		'        \"flightNumber\": \"'+ flightNumber +'\",'+
		'        \"datTUhead\": \"'+ datTUhead +'\",'+
		'        \"typDatTime\": \"L\",'+
		'        \"irgInfASM\": {'+
		'            \"strFLTFlightId\": {'+
		'                \"airCodFLT\": \"'+ airlineCode +'\",'+
		'                \"flightNumberFLT\": \"'+ flightNumber +'\",'+
		'                \"datTUheadFLT\": \"'+ datTUhead +'\"'+
		'            },'+
		'            \"flightCancelIndic\": \"N\"'+
		'        },'+
		'        \"strFlightInformation\": {'+
		'            \"flightType\": \"O\",'+
		'            \"haulType\": \"LC\",'+
		'            \"strItinary\": ['+
		'                {'+
		'                    \"station\": \"GIG\"'+
		'                },'+
		'                {'+
		'                    \"station\": \"AMS\"'+
		'                }'+
		'            ]'+
		'        },'+
		'        \"strLegFlightDate\": ['+
		'            {'+
		'                \"strLegInf\": {'+
		'                    \"operstatus\": \"S\",'+
		'                    \"servType\": \"J\"'+
		'                },'+
		'                \"strAircraftInf\": {'+
		'                    \"aircraftRegistration\": \"PHBQG\",'+
		'                    \"aircraftType\": \"772\",'+
		'                    \"ownerAirlineCode\": \"KL\",'+
		'                    \"physAircraftConfigVersion\": \"C035M283\",'+
		'                    \"physFreightVersion\": \"P006L000\",'+
		'                    \"operationalVersion\": \"C035M283\"'+
		'                },'+
		'                \"strDepInformation\": {'+
		'                    \"strFlightQuality\": {},'+
		'                    \"strFlightPosition\": {'+
		'                        \"iataairportCode\": \"GIG\"'+
		'                    },'+
		'                    \"strTimDep\": {'+
		'                        \"utcSchedDateDep\": \"'+ datTUhead +'\",'+
		'                        \"utcSchedTimDep\": \"2345\",'+
		'                        \"utcLastKnownDateDep\": \"'+ datTUhead +'\",'+
		'                        \"utcLastKnownTimDep\": \"2345\",'+
		'                        \"lastKnownType\": \"S\",'+
		'                        \"lastKnownOrigin\": \"S\",'+
		'                        \"ltschedDateDep\": \"'+ datTUhead +'\",'+
		'                        \"ltschedTimDep\": \"2045\",'+
		'                        \"ltlastKnownDateDep\": \"'+ datTUhead +'\",'+
		'                        \"ltlastKnownTimDep\": \"2045\"'+
		'                    }'+
		'                },'+
		'                \"strArrInformation\": {'+
		'                    \"strFlightQuality\": {},'+
		'                    \"strFlightPosition\": {'+
		'                        \"pierCode\": \"E\",'+
		'                        \"preJetwayDisembark\": \";;E\",'+
		'                        \"strGates\": ['+
		'                            {'+
		'                                \"gateArr\": \"E06\"'+
		'                            }'+
		'                        ],'+
		'                        \"strBagBelt\": ['+
		'                            {'+
		'                                \"bagBelt\": \"014\"'+
		'                            }'+
		'                        ],'+
		'                        \"iataairportCode\": \"AMS\"'+
		'                    },'+
		'                    \"strTimArr\": {'+
		'                        \"utcSchedDateArr\": \"'+ datTUhead +'\",'+
		'                        \"utcSchedTimArr\": \"1115\",'+
		'                        \"utcLastKnownDateArr\": \"'+ datTUhead +'\",'+
		'                        \"utcLastKnownTimArr\": \"1115\",'+
		'                        \"lastKnownType\": \"A\",'+
		'                        \"lastKnownOrigin\": \"A\",'+
		'                        \"ltschedDateArr\": \"'+ datTUhead +'\",'+
		'                        \"ltschedTimArr\": \"1315\",'+
		'                        \"ltlastKnownDateArr\": \"'+ datTUhead +'\",'+
		'                        \"lttouchDownDate\": \"'+ datTUhead +'\",'+
		'                        \"lttouchDownTime\": \"1315\",'+
		'                        \"touchDownType\": \"A\",'+
		'                        \"utcTouchDownDate\": \"'+ datTUhead +'\",'+
		'                        \"utcTouchDownTime\": \"1315\"'+
		'                    }'+
		'                }'+
		'            }'+
		'        ],'+
		'        \"iataairportCode\": \"AMS\"'+
		'    }'+
		'}';
		} else {
			json = '{'+
			'    \"status\": \"OK\",'+
			'    \"statusMessage\": \"Request processed successfully\",'+
			'    \"statusCode\": \"0\",'+
			'    \"flightId\": {'+
			'        \"airCod\": \"'+ airlineCode +'\",'+
			'        \"flightNumber\": \"'+ flightNumber +'\",'+
			'        \"datTUhead\": \"'+ datTUhead +'\",'+
			'        \"typDatTime\": \"L\",'+
			'        \"irgInfASM\": {'+
			'            \"strFLTFlightId\": {'+
			'                \"airCodFLT\": \"'+ airlineCode +'\",'+
			'                \"flightNumberFLT\": \"'+ flightNumber +'\",'+
			'                \"datTUheadFLT\": \"'+ datTUhead +'\"'+
			'            },'+
			'            \"flightCancelIndic\": \"N\"'+
			'        },'+
			'        \"strFlightInformation\": {'+
			'            \"flightType\": \"O\",'+
			'            \"haulType\": \"LC\",'+
			'            \"strItinary\": ['+
			'                {'+
			'                    \"station\": \"GIG\"'+
			'                },'+
			'                {'+
			'                    \"station\": \"AMS\"'+
			'                }'+
			'            ]'+
			'        },'+
			'        \"strLegFlightDate\": ['+
			'            {'+
			'                \"strLegInf\": {'+
			'                    \"operstatus\": \"S\",'+
			'                    \"servType\": \"J\"'+
			'                },'+
			'                \"strAircraftInf\": {'+
			'                    \"aircraftRegistration\": \"PHBQG\",'+
			'                    \"aircraftType\": \"772\",'+
			'                    \"ownerAirlineCode\": \"KL\",'+
			'                    \"physAircraftConfigVersion\": \"C035M283\",'+
			'                    \"physFreightVersion\": \"P006L000\",'+
			'                    \"operationalVersion\": \"C035M283\"'+
			'                },'+
			'                \"strDepInformation\": {'+
			'                    \"strFlightQuality\": {},'+
			'                    \"strFlightPosition\": {'+
			'                        \"iataairportCode\": \"GIG\"'+
			'                    },'+
			'                    \"strTimDep\": {'+
			'                        \"utcSchedDateDep\": \"'+ datTUhead +'\",'+
			'                        \"utcSchedTimDep\": \"1345\",'+
			'                        \"utcLastKnownDateDep\": \"'+ datTUhead +'\",'+
			'                        \"utcLastKnownTimDep\": \"1345\",'+
			'                        \"lastKnownType\": \"S\",'+
			'                        \"lastKnownOrigin\": \"S\",'+
			'                        \"ltschedDateDep\": \"'+ datTUhead +'\",'+
			'                        \"ltschedTimDep\": \"2045\",'+
			'                        \"ltlastKnownDateDep\": \"'+ datTUhead +'\",'+
			'                        \"ltlastKnownTimDep\": \"1045\"'+
			'                    }'+
			'                },'+
			'                \"strArrInformation\": {'+
			'                    \"strFlightQuality\": {},'+
			'                    \"strFlightPosition\": {'+
			'                        \"iataairportCode\": \"AMS\"'+
			'                    },'+
			'                    \"strTimArr\": {'+
			'                        \"utcSchedDateArr\": \"'+ datTUhead +'\",'+
			'                        \"utcSchedTimArr\": \"1615\",'+
			'                        \"utcLastKnownDateArr\": \"'+ datTUhead +'\",'+
			'                        \"utcLastKnownTimArr\": \"1615\",'+
			'                        \"lastKnownType\": \"S\",'+
			'                        \"lastKnownOrigin\": \"S\",'+
			'                        \"ltschedDateArr\": \"'+ datTUhead +'\",'+
			'                        \"ltschedTimArr\": \"1315\",'+
			'                        \"ltlastKnownDateArr\": \"'+ datTUhead +'\",'+
			'                        \"ltlastKnownTimArr\": \"1315\"'+
			'                    }'+
			'                }'+
			'            }'+
			'        ],'+
			'        \"iataairportCode\": \"AMS\"'+
			'    }'+
			'}';
		}
		System.debug(LoggingLevel.INFO, 'json=' + json);
		FmbMblGetFlightInfoResponseModel rm = FmbMblGetFlightInfoResponseModel.parse(json);
		return rm;
	}

}