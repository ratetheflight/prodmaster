/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : MBL GetBaggageInfo response model
 * @Log             : version 1.1 redesigned for bagg pilot october 2015
 */
public with sharing class FmbMblGetBaggageInfoResponseModel {

	public String status;
	public String statusMessage;
	public String statusCode;
	public String statusKey;
	public List<PassengerBag> passengerBags;

	public class PassengerBag {
		public String firstName;
		public String lastName;
		public List<Bag> bags;
	}

	public class Bag {
		public Integer bobId;
		public Weight weight;
		public String finalDestination;
		public LastTracking lastTracking;
		public Tag tag;
		public Tag tagRush;
		public String missedStatus;
		public List<Flight> flights;
		public List<FlightRush> flightRushes;
		public Inbound inbound;
	}

	public class Weight {
		public String weightUnit;
		public Integer value;
	}

	public class LastTracking {
		public String station;
		public String location;
		public String infra;
		public String date_time;
		public String status;
	}

	public class Tag {
		public String type;
		public String issuer3d;
		public String sequence;
	}
	public class Flight {
		public String operatingAirline;
		public String flightNumber;
		public String origin;
		public String destination;
		public String flightDate;
		public Boolean cancelled;
		public Boolean authorityToLoad;
		public Boolean screeningRequired;
		public Boolean bagDeleted;
		public Reconciliation reconciliation;
		public Boolean loadingStatus;
	}

	public class FlightRush {
		public String operatingAirline;
		public String flightNumber;
		public String origin;
		public String destination;
		public String flightDate;
		public Boolean cancelled;
		public Boolean authorityToLoad;
		public Boolean screeningRequired;
		public Boolean bagDeleted;
		public Boolean loadingStatus;
	}

	public class Inbound {
		public String operatingAirline;
		public String flightNumber;
		public String origin;
		public String destination;
		public String flightDateGmt;
		public Boolean cancelled;
		public Boolean authorityToLoad;
		public Boolean screeningRequired;
		public Boolean bagDeleted;
		public Boolean loadingStatus;
	}

	public class Reconciliation {
		public String passengerStatus;
		public String classOfTravel;
		public Boolean canceled;
	}

	
	public static FmbMblGetBaggageInfoResponseModel parse(String json) {
		String jsonResult = json.replaceAll('dateTime', 'date_Time');
		return (FmbMblGetBaggageInfoResponseModel) System.JSON.deserialize(jsonResult, FmbMblGetBaggageInfoResponseModel.class);
	}


	/**
	 * initialize an instance with a complete object graph and sample data, used for dummy scenario's.
	 */
	public void initializeDummy() {
		DateTime now = DateTime.now();
		status = 'OK'; // is used to determine the simplified status in the FmbBagageResponseModel
		statusMessage = 'Request processed successfully';
		statusCode = '0';
		passengerBags = new List<PassengerBag>();

		PassengerBag pb1 = new PassengerBag();
		pb1.firstName = 'dummy firstName';
		pb1.lastName = 'dummy lastName';
		pb1.bags = new List<Bag>();
		passengerBags.add(pb1);
		
		Bag bag = new Bag();
		bag.bobId = 34516271;
		bag.finalDestination = 'CDG';
		bag.flights = new List<Flight>();
		pb1.bags.add(bag);
		
		Weight weight = new Weight();
		weight.value = 23;
		weight.weightUnit = 'K';
		bag.weight = weight;
		
		LastTracking lt = new LastTracking();
		lt.date_Time = now.format('yyyy-MM-dd\'T\'HH:mm:ss', 'Europe/Brussels');
		lt.infra = 'K';
		lt.location = 'PKGK53';
		lt.station = 'AMS';
		bag.lastTracking = lt;
		
		Flight flight = new Flight();
		flight.authorityToLoad = false;
		flight.bagDeleted = false;
		flight.cancelled = false;
		flight.destination = 'CDG';
		flight.loadingStatus = false;
		flight.operatingAirline = 'KL';
		flight.origin = 'AMS';
		flight.flightDate = now.format('yyyy-MM-dd');
		flight.screeningRequired = false;

		bag.flights.add(flight);
	}
}