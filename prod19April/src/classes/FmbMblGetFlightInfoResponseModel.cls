/**                     
 * @author (s)      : Mees Witteman
 * @description     : MBL GetFlightInfo response model PAFI
 * @log             : 10JUL2015: version 1.0
 */
public class FmbMblGetFlightInfoResponseModel {
	public String status;
	public String statusMessage;
	public String statusCode;
	public FlightId flightId;

	public class FlightId {
		public String airCod;
	    public String flightNumber;
	    public String suffix;
	    public String datTUhead;
	    public String iataAirportCode;
	    public String typGMT;
	    public String dateAirport;
	    public String timeAirport;
	    public String typDatTime;
	    public String movType;
	    public IrgInfASM irgInfASM;
	    public StrFlightInformation strFlightInformation;
	    public List<StrLegFlightDate> strLegFlightDate;
	}

	public class IrgInfASM {
	    public StrFLTFlightId strFLTFlightId;
	    public String flightCancelIndic;
	    public String msgSeqRefCode;
	    public String msgSeqRefReason;
	    public String msgSeqRefDateUTC;
	    public String msgSeqRefDateLT;
	    public String msgSeqRefTimeLT;
	    public String irgCodeScheduleProg;
	    public String msgSeqRefTimeUTC;
	}

	public class StrGateArr {
		public String gateArr;
	}

	public class StrGateDep {
		public String gateDep;
	}

	public class StrFLTFlightId {
		public String airCodFLT;
	    public String flightNumberFLT;
	    public String suffixFLT;
	    public String datTUheadFLT;
	    public String typeFlightFlt;
	}

	public class StrArrInformation {
		public StrFlightQualityArr strFlightQuality;
		public StrFlightPositionArr strFlightPosition;
		public StrTimArr strTimArr;
	}
	public class StrFlightQualityArr {
	    public String flightContactDisembType;
	    public String busNumber;
	    public String priorityDisembIndicator;
	    public String indicatorMBI;
	}

	public class StrFlightQualityDep {
	    public String flightContactEmbType;
	    public String busNumber;
	    public String priorityEmbIndicator;
	}

	public class StrFlightPositionArr {
	    public String iataAirportCode;
	    public String iataTownCode;
	    public String aerogareCode;
	    public String terminalCode;
	    public String pierCode;
	    public String preJetwayDisembark;
	    public List<StrGateArr> strGates;
	    public String pkgStand;
	    public String pkgTerminal;
	    public String pkgCustomsStatus;
	    public String pkgConfirmationStatus;
	    public String pkgStandType;
	    public List<StrBagBeltArr> strBagBelt;
	    public String phase;
	}

	public class StrBagBeltArr {
		public String bagBelt;
	}

	public class StrFlightPositionDep {
	    public String iataAirportCode;
	    public String iataTownCode;
	    public String aerogareCode;
	    public String terminalCode;
	    public String pierCode;
	    public String preJetwayEmbark;
	    public List<StrGateDep> strGates;
	    public List<StrCheckDep> strCheckDep;
	    public String pkgStand;
	    public String pkgTerminal;
	    public String pkgCustomsStatus;
	    public String pkgConfirmationStatus;
	    public String pkgStandType;
	    public String phase;
	}

	public class StrAircraftInf {
	    public String aircraftRegistration;
	    public String aircraftType;
	    public String ownerAirlineCode;
	    public String physAircraftConfigVersion;
	    public String physFreightVersion;
	    public String operationalVersion;
	    public String saleableVers;
	}

	public class StrFlightInformation {
		public String flightCategory;
	    public String baseProvinceInd;
	    public String flightType;
	    public String franchiseCode;
	    public String haulType;
	    public List<StrItinary> strItinary;
	}

	public class StrLegInf {
		public String operstatus;
	    public String legCustomStatus;
	    public String servType;
	}

	public class StrTimDep {
	    public String utcSchedDateDep;
	    public String utcSchedTimDep;
	    public String ltSchedDateDep;
	    public String ltSchedTimDep;
	    public String utcModifiedDateDep;
	    public String utcModifiedTimDep;
	    public String ltModifiedDateDep;
	    public String ltModifiedTimDep;
	    public String utcLastKnownDateDep;
	    public String utcLastKnownTimDep;
	    public String ltLastKnownDateDep;
	    public String ltLastKnownTimDep;
	    public String lastKnownType;
	    public List<StrDelayDepInf> strDelayDepInf;
	    public String utcTakeoffDateDep;
	    public String utcTakeoffTimDep;
	    public String ltTakeoffDateDep;
	    public String ltTakeoffTimDep;
	    public String utcSlotDateDep;
	    public String utcSlotTimDep;
	    public String ltSlotDateDep;
	    public String ltSlotTimDep;
	    public String utcTsatDateDep;
	    public String utcTsatTimDep;
	    public String ltTsatDateDep;
	    public String ltTsatTimDep;
	    public String lastKnownOrigin;
	    public String lastKnownAcarsOrigin;
	    public String utcNextInfoTimDep;
	    public String utcNextInfoDateDep;
	    public String ltNextInfoDateDep;
	    public String ltNextInfoTimDep;
	    public List<StrRemark> strRemarksDep;
	    public String takeoffType;
	    public String utcTOBTDateDep;
	    public String lttobtDateDep;
	    public String lttobtTimDep;
	    public String utcTOBTTimDep;
	    public String utcAOPTimDep;
	    public String utcAOPDateDep;
	    public String ltaopDateDep;
	    public String ltaopTimDep;
	}

	public class StrCheckDep {
		public String checkDep;
	}

	public class StrDelayDepInf {
		public String codDelaydep;
	    public String codDlaDep;
	    public String durDelayDep;
	    public String delayReasInf;
	}

	public class StrBagBelt {
		public String bagBelt;
	}

	public class StrLegFlightDate {
		public StrLegInf strLegInf;
	    public StrAircraftInf strAircraftInf;
	    public StrDepInformation strDepInformation;
	    public StrArrInformation strArrInformation;
	    public List<StrPartnershipLeg> strPartnershipLeg;
	    public List<StrTransdLeg> strTransdLeg;
	}

	public class StrTransdLeg {
		public String typPartShip;
	    public String codeTransdAirline;
	    public String flightTransd;
	    public String suffixTransd;
	    public String dateTUHeadTransd;
	}

	public class StrPartnershipLeg {
		public String typPartShip;
	    public String codeShareType;
	    public String codeShareAirline;
	    public String flightShare;
	    public String suffixShare;
	    public String dateTUHeadShare;
	    public String saleableVersShare;
	}

	public class StrRemark {
		public String remarkSI;
	}

	public class StrTimArr {
	    public String utcSchedDateArr;
	    public String utcSchedTimArr;
	    public String ltSchedDateArr;
	    public String ltSchedTimArr;
	    public String utcModifiedDateArr;
	    public String utcModifiedTimArr;
	    public String ltModifiedDateArr;
	    public String ltModifiedTimArr;
	    public String utcLastKnownDateArr;
	    public String utcLastKnownTimArr;
	    public String ltLastKnownDateArr;
	    public String ltLastKnownTimArr;
	    public String touchDownType;
	    public String utcTouchDownDate;
	    public String utcTouchDownTime;
	    public String ltTouchDownDate;
	    public String ltTouchDownTime;
	    public String lastKnownType;
	    public String lastKnownOrigin;
	    public String lastKnownAcarsOrigin;
	    public List<StrRemark> strRemarksArr;
	}

	public class StrDepInformation {
		public StrFlightQuality strFlightQuality;
		public StrFlightPosition strFlightPosition;
		public StrTimDep strTimDep;
	}

	public class StrFlightPosition {
		public String iataairportCode;
	}


	public class StrItinary {
		public String station;
	}

	public class StrFlightQuality {
	}
	
	public String getLastKnownDepDat() {
		try {
			return this.flightId.strLegFlightDate[0].strDepInformation.strTimDep.utcLastKnownDateDep;
		} catch (Exception e) {
			return '';
		}
	}
	public String getLastKnownDepTim() {
		try {
			return this.flightId.strLegFlightDate[0].strDepInformation.strTimDep.utcLastKnownTimDep;
		} catch (Exception e) {
			return '';
		}
	}
	
	public String getLastKnownArrDat() {
		try {
			return this.flightId.strLegFlightDate[0].strArrInformation.strTimArr.utcLastKnownDateArr;
		} catch (Exception e) {
			return '';
		}
	}
	public String getLastKnownArrTim() {
		try {
			return this.flightId.strLegFlightDate[0].strArrInformation.strTImArr.utcLastKnownTimArr;
		} catch (Exception e) {
			return '';
		}
	}
	public static FmbMblGetFlightInfoResponseModel parse(String json) {
		return (FmbMblGetFlightInfoResponseModel) System.JSON.deserialize(json, FmbMblGetFlightInfoResponseModel.class);
	}
}