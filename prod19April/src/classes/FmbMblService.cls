/**
 * @author (s)    :	David van 't Hooft, Mees Witteman
 * @description   : Service class to retrieve MBL Baggage information
 * @log           : 13OCT2015: connected to bpap Live endpoint
 */
public with sharing class FmbMblService {
    private String result;
    
 	public FmbMblService() {
 	}
 	
 	/**
 	 * Dummy call for the test Page
 	 **/
 	public void retrieveBaggageInfo() {
 		retrieveBaggageInfo('0', 'KL', '074', '009264', 'BERGEL', 'nl', 'NL');
 	}
 	 
 	/**
 	 * country: NL
 	 * language: nl
 	 * station: CDG
 	 * iataTagSequence: 009264
 	 * lastName: BERGEL
 	 **/                           
    public FmbMblGetBaggageInfoResponseModel retrieveBaggageInfo(String tagType, String issuerCode2C, String issuerCode3D, String tagSequence, String lastName, String language, String country) {
    	return retrieveBaggageInfo('GoMobile', '3.0.0', 'KL', 'getBaggageInfo', 'ANDROID', 'Some detail string',
    		tagType, issuerCode2C, issuerCode3D, tagSequence, lastName, language, country );
	}
	
 	/**
 	 * country: NL
 	 * language: nl
 	 * station: CDG
 	 * iataTagSequence: 009264
 	 * lastName: BERGEL
 	 **/
    public FmbMblGetBaggageInfoResponseModel retrieveBaggageInfo(String application, String version, String requestor, String request, String deviceType, String deviceDetails,
    		String tagType, String issuerCode2C, String issuerCode3D, String tagSequence, String lastName, String language, String country) {
		FmbMblGetBaggageInfoResponseModel responseModel;
		
		if (lastName.startsWith('test')) {
			responseModel = new FmbMblDummyData().getFmbMblGetBaggageInfoResponseModelForScenario(tagSequence, lastName);		
		} else if (lastName.startsWith('big test')) {
			 result = '{"status":"OK","statusMessage":"Request processed successfully","statusCode":"0","passengerBags":[{"firstName":"SEBASTIEN","lastName":"GUILLAUME","bags":[{"bobId":432732298,"weight":{"weightUnit":"K","value":12},"finalDestination":"JFK","lastTracking":{"station":"CDG","location":"PKGE22","infra":"E","date_Time":"2015-10-21T09:59:20","status":"ONBOARD"},"tag":{"type":"0","issuer3d":"057","sequence":"692952"},"flights":[{"operatingAirline":"AA","flightNumber":"012","origin":"AMS","destination":"CDG","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"JFK","destination":"LHR","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"LHR","destination":"BCN","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"BCN","destination":"MLT","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false}],"inbound":{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDateGmt":"2015-10-21","cancelled":false,"authorityToLoad":false,"screeningRequired":false,"bagDeleted":false,"loadingStatus":false}}]},{"firstName":"SEBASTIEN","lastName":"GUILLAUME","bags":[{"bobId":432732298,"weight":{"weightUnit":"K","value":12},"finalDestination":"JFK","lastTracking":{"station":"CDG","location":"PKGE22","infra":"E","date_Time":"2015-10-21T09:59:20","status":"ONBOARD"},"tag":{"type":"0","issuer3d":"057","sequence":"692952"},"flights":[{"operatingAirline":"AA","flightNumber":"012","origin":"AMS","destination":"CDG","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"JFK","destination":"LHR","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"LHR","destination":"BCN","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"BCN","destination":"MLT","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false}],"inbound":{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDateGmt":"2015-10-21","cancelled":false,"authorityToLoad":false,"screeningRequired":false,"bagDeleted":false,"loadingStatus":false}}]},{"firstName":"SEBASTIEN","lastName":"GUILLAUME","bags":[{"bobId":432732298,"weight":{"weightUnit":"K","value":12},"finalDestination":"JFK","lastTracking":{"station":"CDG","location":"PKGE22","infra":"E","date_Time":"2015-10-21T09:59:20","status":"ONBOARD"},"tag":{"type":"0","issuer3d":"057","sequence":"692952"},"flights":[{"operatingAirline":"AA","flightNumber":"012","origin":"AMS","destination":"CDG","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"JFK","destination":"LHR","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"LHR","destination":"BCN","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"BCN","destination":"MLT","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false}],"inbound":{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDateGmt":"2015-10-21","cancelled":false,"authorityToLoad":false,"screeningRequired":false,"bagDeleted":false,"loadingStatus":false}}]},{"firstName":"SEBASTIEN","lastName":"GUILLAUME","bags":[{"bobId":432732298,"weight":{"weightUnit":"K","value":12},"finalDestination":"JFK","lastTracking":{"station":"CDG","location":"PKGE22","infra":"E","date_Time":"2015-10-21T09:59:20","status":"ONBOARD"},"tag":{"type":"0","issuer3d":"057","sequence":"692952"},"flights":[{"operatingAirline":"AA","flightNumber":"012","origin":"AMS","destination":"CDG","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"JFK","destination":"LHR","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"LHR","destination":"BCN","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false},{"operatingAirline":"AA","flightNumber":"012","origin":"BCN","destination":"MLT","flightDate":"2015-10-21","cancelled":false,"authorityToLoad":true,"screeningRequired":false,"bagDeleted":false,"reconciliation":{"passengerStatus":"B","classOfTravel":"Y","canceled":false},"loadingStatus":false}],"inbound":{"operatingAirline":"AA","flightNumber":"012","origin":"CDG","destination":"JFK","flightDateGmt":"2015-10-21","cancelled":false,"authorityToLoad":false,"screeningRequired":false,"bagDeleted":false,"loadingStatus":false}}]}]}';
			 responseModel = FmbMblGetBaggageInfoResponseModel.parse(result); 
		} else if (lastName.startsWith('xxx')) {    
        	// Rest call is not performed during test, so send back mock data
	        result = new FmbMblDummyData().getDummyResponseByBagTagNumber(tagSequence, lastName);
	        result = result.replaceAll('dateTime', 'date_Time');
	        responseModel = FmbMblGetBaggageInfoResponseModel.parse(result);    	
        } else {       		
			// The real thing
			Http h = new Http();
	        Httprequest req = new Httprequest();
	        req.setEndpoint('https://www.klm.com/bpap/getBaggageInfo');
	        //req.setEndpoint('https://www.klm.com/passage/mal-gm/getBaggageInfo');
	        //req.setEndpoint('https://www.ite1.klm.com/passage/mbl/getBaggageInfo');
	        req.setMethod('POST');
	        
	        req.setBody('{' +
				'\"application\": \"' + application +'\",' + 			// FollowMyBaggage oid
				'\"version\": \"' + version + '\",' +
				'\"requestor\": \"' + requestor + '\",' +
				'\"request\": \"' + request + '\",' +
				'\"deviceType\": \"' + deviceType + '\",' + 			// To analyse, so let's align on a standard
				'\"deviceDetails\": \"' + deviceDetails + '\",' +		// user agent oid
			    '\"country\": \"' + country + '\",' +
			    '\"language\": \"' + language + '\",' +
			    '\"station\": \"' + 'AMS' + '\",' +
			    '\"issuerCode2C\": \"' + issuerCode2C + '\",' +
			    '\"issuerCode3D\": \"' + issuerCode3D + '\",' +
			    '\"tagType\": \"' + tagType + '\",' +
			    '\"tagSequence\":\"' + tagSequence + '\",' +			// regexp ([0-9]{6})
			    '\"lastName\": \"' + lastName + '\"' + 					// mandatory, will be matched against the baggageInfo returned for requested TAG, 
			    														//    if there is no match no bags are returned.
		    '}');        
	        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
	        req.setTimeout(60000);
	        System.debug(req.getBody());
			httpresponse res = h.send(req);
			result = res.getBody();
			result = result.replaceAll('dateTime', 'date_Time');
			System.debug('BPAP result=' + result);
	        responseModel = FmbMblGetBaggageInfoResponseModel.parse(result);
        }
        return responseModel;
    }

    public FmbMblGetFLightInfoResponseModel retrieveFlightInfo(String country, String language, 
     			String airlineCode, String flightNumber, String iataAirportCode,  String datTUhead) {
    	return retrieveFlightInfo('GoMobile', '3.0.0', 'KL', 'getFlightInfo', 'ANDROID', 'Some detail string',
    		country, language, airlineCode, flightNumber, iataAirportCode, datTUhead);
	}

    public FmbMblGetFLightInfoResponseModel retrieveFlightInfo(String application, String version, String requestor, String request, 
     			String deviceType, String deviceDetails, String country, String language, 
     			String airlineCode, String flightNumber, String iataAirportCode,  String datTUhead) {
		FmbMblGetFlightInfoResponseModel responseModel;
		if (airlineCode.equals('AA') || Test.isRunningTest()) {
			if (iataAirportCode.equals('BLT')) {
				responseModel = new FmbMblDummyData().getFmbMblGetFlightInfoResponseModelForScenario(airlineCode, flightNumber, iataAirportCode, datTUhead, true);
			} else {
				responseModel = new FmbMblDummyData().getFmbMblGetFlightInfoResponseModelForScenario(airlineCode, flightNumber, iataAirportCode, datTUhead, false);
			}
        } else {       		
			// The real thing
			Http h = new Http();
	        Httprequest req = new Httprequest();
	        //req.setEndpoint('https://www.klm.com/passage/mal-gm/getFlightInfo');
	        //req.setEndpoint('https://www.ute1.klm.com/passage/mbl/getFlightInfo');
	        req.setEndpoint('https://www.klm.com/bpap/getFlightInfo');
	        req.setMethod('POST');
	        
	        req.setBody('{' +
				'\"application\": \"' + application +'\",' + 
				'\"version\": \"' + version + '\",' +
				'\"requestor\": \"' + requestor + '\",' +
				'\"request\": \"' + request + '\",' +
				'\"deviceType\": \"' + deviceType + '\",' + 
				'\"deviceDetails\": \"' + deviceDetails + '\",' +
			    '\"country\": \"' + country + '\",' +
			    '\"language\": \"' + language + '\",' +
			    '\"airlineCode\": \"' + airlineCode + '\",' +
			    '\"flightNumber\": \"' + flightNumber + '\",' + 	
			    '\"iataAirportCode\": \"' + iataAirportCode + '\",' + 	
			    '\"datTUhead\": \"' + datTUhead  + '\"' + 								
		    '}');        

	        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
	        req.setTimeout(60000);
	        System.debug('req.getBody()=' + req.getBody());
			httpresponse res = h.send(req);
			result = res.getBody();
			result = result.replaceAll('dateTime', 'date_Time');
			System.debug('result=' + result);
	        responseModel = FmbMblGetFlightInfoResponseModel.parse(result);
        }
        return responseModel;     	
     }

 	/**
 	 * Get Result for the visual force page
 	 **/
    public string getResult() {
        return this.result;
    }
    
    /**
 	 * Set Result for the Test class
 	 **/
    public void setResult(String result) {
        this.result = result;
    } 
}