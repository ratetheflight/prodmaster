/*************************************************************************************************
* File Name     :   FutureMethodRecordProcessing
* Description   :   Common Class to hold Future Methods List
* @author       :   Manuel Conde
* Modification Log
===================================================================================================
* Ver.    Date            Author          Modification
*--------------------------------------------------------------------------------------------------
* 1.0     11/02/2016      Manuel Conde    Initial version requested by FB Messenger Project
* 2.0     28/04/2016      Manuel Conde    Added retrieveUpdateKloutScore for AF Segmentation
* 3.0     12/06/2016      Ata             Added code to by pass KloutId callout if itt is already present
  4.0     03/04/2017      Prasanna        Updated basic value for klout score from 45 to 50 
 ****************************************************************************************************/

global class FutureMethodRecordProcessing {

    @future(callout=true)
    public static void sendCasesStatusUpdates2Heroku(String jsonContent){
        boolean isError = false;

        if(jsonContent == null && jsonContent == '') return;

        System.debug('jsonContent: ' + jsonContent);

        //if(!Test.isRunningTest()){

            HttpRequest request = null;

            try{
                AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('Settings');

                if(custSettings == null) return;

                String endPointConfig = custSettings.SCS_HerokuRestEndpoint__c;
                String userName = custSettings.SCS_HerokuRestApiUser__c;
                String pw = custSettings.SCS_HerokuRestApiPw__c;
                Blob headerValue = Blob.valueOf(userName + ':' + pw);

                request =new HttpRequest();                                                     
                request.setEndpoint(endPointConfig);
                request.setMethod('PUT'); 
                String authorizationHeader = 'Basic ' +
                EncodingUtil.base64Encode(headerValue);
                request.setHeader('Authorization', authorizationHeader);
                request.setHeader('Content-Type' ,'application/json');
                request.setBody(jsonContent);        


            } catch(Exception e) {
                system.debug('The following exception has occurred building request to Heroku: ' + e.getMessage());
                isError = true;
            }

            try {
                if(!isError){
                    Http http = new Http();
                    System.debug('Request to Heroku: ' + request.getBody());
                    HTTPResponse response = http.send(request);
                    System.debug('Response from Heroku: ' + request.getBody());
                }

            } catch(Exception e) {
                system.debug('The following exception has occurred sending request to Heroku: ' + e.getMessage());
            }

        //} 
        
       
    }

    @future(callout=true)
    public static void retrieveUpdateKloutScore(Set<Id> listOfAccounts, List<String> listOfPostsForKloutScoreUpd){

        if(listOfAccounts <> null && !listOfAccounts.isEmpty()){

            List<Account> accToUpdate = new List<Account>();
            Map<id,Integer> accMap = new Map<id,Integer>();
            Set<Id> accIds = new Set<Id>();

            //Accounts only 
            List<Account> accounts = [select id,KloutId__c,name,Klout_score__c,sf4twitter__Twitter_Username__pc, sf4twitter__Twitter_User_Id__pc,Influencer__c from Account where id in :listOfAccounts and (sf4twitter__Twitter_User_Id__pc <> null OR sf4twitter__Twitter_Username__pc <> null)];

            for(Account a : accounts){
                // populating kloutDetails object
                kloutDetails kloutInfo = FutureMethodRecordProcessing.getKloutScore(a);
                // If condition added by Ata
                if(a.Klout_score__c != kloutInfo.kloutScore ){ 
                    Account c = new Account(id=a.id);                   
                    c.Klout_score__c = kloutInfo.kloutScore;
                    c.Klout_Score_Last_Update_Time__c =  System.Now();
                    //Added By Ata
                    c.KloutId__c = kloutInfo.kloutId;
                    
                    if(kloutInfo.kloutScore >= 60 && a.Influencer__c == false){
                        c.Influencer__c = true;
                    }

                    accToUpdate.add(c);
                    accMap.put(c.id, kloutInfo.kloutScore);
                    accIds.add(c.id);
                }
            }
            if(!accToUpdate.isEmpty()){

                update accToUpdate;
            
                //Update twitter social posts
                List<SocialPost> allPostsToUpdate = [select id, whoId,provider, Klout_score__c from SocialPost where whoId <> null and whoId in :accIds and provider = 'Twitter' and Klout_score__c = null and createdDate IN(TODAY,YESTERDAY)];
                List<SocialPost> postsToUpdate = new List<SocialPost> ();

                if(!allPostsToUpdate.isEmpty()){

                    for(SocialPost a : allPostsToUpdate){
                        //If condition added by Ata
                        if(accMap.containskey(a.whoID)){                        
                            Integer score = (Integer) accMap.get(a.whoId);
                            SocialPost b = new SocialPost(id=a.id, Klout_score__c=score);
                            postsToUpdate.add(b);
                        }
                    }
                
                    if(!postsToUpdate.isEmpty())
                        update postsToUpdate;
                }

            }           

        }

        if(listOfPostsForKloutScoreUpd <> null && !listOfPostsForKloutScoreUpd.isEmpty()){

            List<SocialPost> posts2Upd = new List<SocialPost>();

            List<SocialPost> posts = [select id,name,Klout_score__c,Influencer_Score__c,handle,persona.followers from SocialPost where handle in :listOfPostsForKloutScoreUpd and Klout_score__c = null and createdDate IN(TODAY,YESTERDAY)];

            for(SocialPost a : posts){
                System.debug('Post: ' + a.handle + ' Score Before: ' + a.Klout_score__c);
                
                Account f = new Account(sf4twitter__Twitter_Username__pc=a.handle);

                kloutDetails kloutInfo = FutureMethodRecordProcessing.getKloutScore(f);
                
                if(a.Klout_score__c != kloutInfo.kloutScore){ 
                    SocialPost c = new SocialPost(id=a.id);
                    c.Klout_score__c = kloutInfo.kloutScore;

                    if(kloutInfo.kloutScore >= 60){
                        c.Influencers_SP__c = true;
                    }
                    
                    //added by ata to calculate influencer on SP
                    if((c.Klout_Score__c >=70 && c.Klout_Score__c <= 100) || (c.persona.followers > 100000)){
                        c.Influencer_Score__c = 4;
                        system.debug('m in 1st');
                    }    
                    else if((c.Klout_Score__c >=60 && c.Klout_Score__c <= 69) || (c.persona.followers > 50000 && c.persona.followers <= 100000)){   
                        c.Influencer_Score__c = 3;
                        system.debug('m in 2nd');
                    }    
                    else if((c.Klout_Score__c >=50 && c.Klout_Score__c <= 59) || (c.persona.followers > 15000 && c.persona.followers <= 50000)){
                        c.Influencer_Score__c = 2;
                        system.debug('m in 3rd');
                    }    
                    else{
                        c.Influencer_Score__c = 1;
                        system.debug('m in 4th');
                    }    
                    //Ends here
                    posts2Upd.add(c);
                }
            }

            if(!posts2Upd.isEmpty()){
                update posts2Upd;
            }

        }

    }
    

    //Get Klout Score externally
    public static kloutDetails getKloutScore(Account acc) {
        Integer kloutScore = 0;
        String kloutId = '';
        //declaring klout object
        kloutDetails kloutInfo = new kloutDetails();
        
        try {
            
            //added if else ladder to by pass KloutID callout
            if(acc.KloutId__c == Null){
                if(acc.sf4twitter__Twitter_User_Id__pc != null) {
                    kloutId = (String) FutureMethodRecordProcessing.kloutObjects('http://api.klout.com/v2/identity.json/tw/'+acc.sf4twitter__Twitter_User_Id__pc+'?key=us4k68rumkgwwnqkk797hsm4').get('id');
                    
                } else if(acc.sf4twitter__Twitter_Username__pc != null) {
                    kloutId = (String) FutureMethodRecordProcessing.kloutObjects('http://api.klout.com/v2/identity.json/twitter?screenName='+acc.sf4twitter__Twitter_Username__pc+'&key=us4k68rumkgwwnqkk797hsm4').get('id');
                }
            }   
            else{
                kloutId = acc.KloutId__c;
            }
                        
            if(!''.equals(kloutId)) {
                Decimal kloutDecimal = Decimal.valueOf(Double.valueOf(kloutObjects('http://api.klout.com/v2/user.json/'+kloutId+'/score?key=us4k68rumkgwwnqkk797hsm4').get('score')));
                kloutScore = Integer.valueOf(kloutDecimal.divide(1, 0, System.RoundingMode.UP));
            }
            
            //preparing the klout object to return
            kloutInfo.kloutId = kloutId;
            kloutInfo.kloutScore = kloutScore;
            
        } catch(Exception e) {            
            System.Debug(e.getMessage());
        }

        return kloutInfo;
    }
    
    //Wrapper class for klout score and Klout ID by Ata
    public class kloutDetails{
        
        public string kloutId;
        public Integer kloutScore;
        
    }
    
    public static Map<String, Object> kloutObjects(String endpoint) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('GET');
        
        HttpResponse res = h.send(req);
        System.debug('Res ' + res.getbody());
        return (Map<String, Object>) JSON.deserializeUntyped(res.getbody());
    } 
}