/*************************************************************************************************
* File Name     :   HerokuWebServiceMockImpl
* Description   :   Test Class to test callout to Heroku
* @author       :   Manuel Conde
* Modification Log
===================================================================================================
* Ver.    Date            Author          Modification
*--------------------------------------------------------------------------------------------------
* 1.0     11/02/2016      Manuel Conde    Initial version requested by FB Messenger Project

****************************************************************************************************/
@isTest
global class HerokuWebServiceMockImpl implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {
        if('PUT'.equals(req.getMethod())) {
            System.assertEquals('PUT', req.getMethod());
        }

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
        res.setBody(
                '<?xml version="1.0" encoding="utf-8"?>'+
                    '<heroku>'+
                        '<errorCode>200</errorCode>'+
                        '<results/>'+
                        '<statusCode/>'+
                    '</heroku>');
        

        res.setStatusCode(200);
        return res;
    }
}