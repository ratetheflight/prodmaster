/*************************************************************************************************
* File Name     :   KloutWebServiceMockImpl
* Description   :   Test Class to test callout to Klout
* @author       :   Manuel Conde
* Modification Log
===================================================================================================
* Ver.    Date            Author          Modification
*--------------------------------------------------------------------------------------------------
* 1.0     02/05/2016      Manuel Conde    Initial version

****************************************************************************************************/
@isTest
global class KloutWebServiceMockImpl implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();

        //Print end point
        System.debug('EndPoint: ' + req.getEndpoint());

        if(req.getEndpoint().contains('identity')){
            res.setBody('{"id":"170855332391893399","network":"ks"}');

        }else{
            res.setBody('{"score":74.491935829065426,"scoreDelta":{"dayChange":-0.007004570884895145,"weekChange":-0.6419733315219034,"monthChange":3.2127200934567384},"bucket":"20-29"}');            
        }


        res.setStatusCode(200);
        return res;
    }
}