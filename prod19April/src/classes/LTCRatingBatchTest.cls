/**
 * @author (s)    : Ata
 * @Date          : 21stNOV2016
 * @description   : This is a test class for LtcRatingOriginDestinationCountryBatch 
 *                  batch class.
 *                  
 * @log           : 21st NOV 2016: Developed
*/
@isTest
public with sharing class LTCRatingBatchTest {
    static testmethod void test() {
       //create test flight Info Record
       Flight_Date__c fI= new Flight_Date__c();
       insert fI;
       //create passenger record
       Passenger__c p = new  Passenger__c();
       p.First_Name__c = 'ABC';
       p.Family_Name__c = 'AKJD';
       insert p;
             
       //insert flight information
       Flight__c fly = new Flight__c();
       fly.Flight_Number__c = 'KL1001';
       fly.Scheduled_Departure_Date__c  = Date.Today();
       insert fly;
       
       Leg__c leg = new Leg__c();
       //leg.Leg_Name__c = 'Name';
       leg.origin__c = 'AMS';
       leg.destination__c = 'AUH';
       leg.departuredate__c = Date.Today();
       leg.arrivalDate__c = Date.Today();
       leg.Flight__c = fly.id;
       insert leg;
       //create ratings list
       List<Rating__c> rt = new List<Rating__c>();
       for (Integer i=0;i<20;i++) {
           Rating__c m = new Rating__c(
               Positive_comments__c= 'Merchandise',
               Negative_comments__c= 'Some description',
               Origin__c= 'AMS',
               Destination__c= 'AUH',
               //First_Name__c= 'ATAULL',
               Flight_Date__c= fI.id,
               Flight_Info__c= fly.id,
               Passenger__c = p.id);
           rt.add(m);
       }
       insert rt;
       //actual test start here.
       Test.startTest();
       LtcRatingOriginDestinationCountryBatch c = new LtcRatingOriginDestinationCountryBatch();
       Database.executeBatch(c);
       Test.stopTest();

    }
}