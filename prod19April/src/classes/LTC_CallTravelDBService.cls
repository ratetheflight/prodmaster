/**                     
 * @author (s)      : Ata
 * @description     : For retrieving the IFE information
 * @log:   28DEC2016: version 1.0
 *         
 */
public with sharing class LTC_CallTravelDBService {
    /**
     * sample input Parameter: Base 64 encoded encrypted text
     * Output:            {  
                           "itinerary":[  
                              {  
                                 "flightNumber":"LO334",
                                 "travelDate":"2017-01-07T07:30:00.000",
                                 "origin":"CDG",
                                 "destination":"WAW",
                                 "seqNo":1
                              },
                              {  
                                 "flightNumber":"LO2001",
                                 "travelDate":"2017-01-07T13:00:00.000",
                                 "origin":"WAW",
                                 "destination":"DXB",
                                 "seqNo":2
                              }
                           ],
                           "passenger":{  
                              "name":"LEVI",
                              "surName":"RUECKER",
                              "email":"AU@TAJAWAL.COM"
                           }
                        }
     */
    public LtcTravelDBResponseModel getPassengerData(String encodedData) {
        String decryptedData ;
        LtcTravelDBResponseModel travelDBResp;
        Boolean flag = True;
        String pnr;
        String firstName;
        String lastName;
        String dated;
        List<String> dataChunks = new List<String>();
        if (encodedData != null) {
           try{
             decryptedData = LTC_FORCETRACK.LTC_WHM_Operations.decryptData(encodedData,1);
             dataChunks = decryptedData.split('#');
            } catch(Exception ex){
                try{
                  if(Test.isRunningTest()) {
                        dataChunks = 'PNR=3LL5UN#LASTNAME=GALT#FIRSTNAME=John Mr#DATE=20170127'.split('#');
                  } else{
                  		decryptedData = LTC_FORCETRACK.LTC_WHM_Operations.decryptData(encodedData,2);
                  		dataChunks = decryptedData.split('#');  
                  }
                  } catch(Exception decrypFailed){
                    flag = false;
                    System.Debug('Decryption failed:'+decrypFailed.getMessage());
                    return new LtcTravelDBResponseModel('Decryption failed');
                }
            }
           if (flag) {
               for(string s: dataChunks) {
                   if(s.split('=')[0] == 'PNR'){
                       pnr = s.split('=')[1];
                   }
                   else if(s.split('=')[0] == 'LASTNAME'){
                       lastName = s.split('=')[1];
                   }
                   else if(s.split('=')[0] == 'FIRSTNAME'){
                       firstName = s.split('=')[1];
                   }
                   else if(s.split('=')[0] == 'DATE'){
                       dated = s.split('=')[1];
                   }
               }  
               //call the TravelDB with this Data and get response.  
               LTC_TravelDbhttpCall callTDb = new LTC_TravelDbhttpCall();
               travelDBResp = callTDb.getCalloutResponseContents(pnr,firstName,lastName);
           }
        }  
        return travelDBResp;
    } 
}