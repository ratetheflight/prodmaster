/**                     
 * @author (s)      : Ata
 * @description     : For retrieving the IFE information
 * @log:   28DEC2016: version 1.0
 *         
 */
public without sharing class LTC_FetchIFERecords {
    public static List<IFEData> getIFErecords(){
        List<IFEData> IFEList = new List<IFEData>();
        //get attachment from specified account record
        List<Account> acctIFE = [Select id from Account where name like '%IFE Attachment%' Limit 1];     
        //if account found
        if (acctIFE != null && acctIFE.size() > 0) {
            ID accID = acctIFE[0].id;
            //search for latest attachment
            List<Attachment> latestAtt = [Select id,parentid,body,name from Attachment where parentid = :accID order by createddate desc limit 1];
            if (latestAtt != null && latestAtt.size() > 0) {
                String nameFile = latestAtt[0].body.toString();
                List<string> filelines = nameFile.split('\n');
                //Iterate through every line and create a Account record for each row
                IFEList= new List<IFEData>();       
                for (Integer i=1;i<filelines.size();i++) {
                    String[] inputvalues = new String[]{};
                    inputvalues = filelines[i].split(',');
                    IFEData iData = new IFEData(Integer.Valueof(inputvalues[0]), inputvalues[1], inputvalues[2], inputvalues[3]);
                    IFEList.add(iData);
                }
            }
        }
        return IFEList;
    }
    //inner class blueprint of data to be sent to the requester
    public class IFEData{
        public String name {get;set;}
        public String url {get;set;}
        public Integer srlNo {get;set;}
        public String month {get;set;}
        public IFEData() {
            
        }
        public IFEData(Integer srlNo, String name, String url,String month){
            this.name = name;
            this.url = url;
            this.srlNo = srlNo;
            this.month = month;
        }
        public String makeString(List<IFEData> ifeList) {
            String ifeString = null;
            if (ifeList != null && !ifeList.isEmpty()) {
                ifeString = '[';
                for (IFEData iData : ifeList) {
                    //ifeString = ifeString + '{'+ '"name": "' + iData.name +  '", "url": "' + iData.url +'", '+'"srlNo": "' + iData.srlNo +  '" ' + '},';
                    ifeString = ifeString + '{' + '"srlNo": "'+ iData.srlNo + '", ' +'"name": "' + iData.name + '", ' + '"url": "'+ iData.url+ '", '+ '"month": "'+ iData.month+ '" },';
                }            
                ifeString = ifeString.substring(0, ifeString.length() - 1);
                ifeString = ifeString + ']';
            }
            ifeString = ifeString.replace('\r\n', '');
            ifeString = ifeString.replace('\r', '');
            ifeString = ifeString.replace('\n', '');
            return '{' +
                '"ifeList": ' + ifeString +
            '}';
        } 
    }
}