/**                     
 * @author (s)      : Ata
 * @description     : Class to handle inbound email services for IFE
 * @log:   28DEC2016: version 1.0
 * 
 */
global class LTC_IFE_EmailHandler implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Account account;
        // Look for account whose name is the subject and create it if necessary
        try{
            String subject = '%IFE Attachment%';
            if ([Select COUNT() from Account where Name like :subject] == 0) {
              account = new Account();
              account.Name = 'IFE Attachment';
              insert account;
            } else {
              account = [select Id from Account where Name like :subject];
            }       
            // Save attachments, if any
            for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                Attachment attachment = new Attachment();    
                attachment.Name = bAttachment.fileName;
                attachment.Body = bAttachment.body;
                //String nameFile =bAttachment.body.toString();
                //System.debug('Name file atachment CSV:::'+nameFile);
                attachment.ParentId = account.Id;
                insert attachment;
            }
            result.success = true;
        } catch (Exception e) { 
            System.debug('Email service failed::::'+e);
            result.success = false;
            result.message = 'Oops, I failed.';
        }
        return result;
    }
}