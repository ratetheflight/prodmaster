/**
 * @author (s)      : Ata
 * @description     : Test class to test the LTC_IFE_EmailHandler. 
 *                    
 * @log             : 13JAN2017: version 1.0
 */
@isTest
private class LTC_IFE_EmailHandlerTest {
    //Test Method for main class       
    static testMethod void TestinBoundEmail() {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
        email.subject = 'IFE Attachment';
        email.fromAddress = 'ataullah@klm.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        
        // add an Binary attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

        // add an Text atatchment
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };

        // call the email service class and test it with the data in the testMethod
        LTC_IFE_EmailHandler   testInbound=new LTC_IFE_EmailHandler();
        testInbound.handleInboundEmail(email, env);
    }
    //Negative Test
    static testMethod void TestinBoundEmailNeg() {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
        email.subject = 'IFE Attachment';
        email.fromAddress = 'ataullah@abcd.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        
        // call the email service class and test it with the data in the testMethod
        LTC_IFE_EmailHandler   testInbound=new LTC_IFE_EmailHandler();
        testInbound.handleInboundEmail(email, env);
    }
}