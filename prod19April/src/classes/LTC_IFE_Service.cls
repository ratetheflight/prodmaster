/**                     
 * @author (s)      : Ata
 * @description     : Rest API interface for retrieving the IFE information
 * @log:    28DEC2016: version 1.0
 *         
 */
@RestResource(urlMapping='/IFE_Service/*')
global class LTC_IFE_Service {
    /**
     * Retrieve stored IFE image links from Salesforce
     * Expected Url params: N/A
     **/
    @HttpGet
    global static void getIFEData() {
        String response = null;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        try {
            List<LTC_FetchIFERecords.IFEData> ifeList = LTC_FetchIFERecords.getIFErecords();
            if (ifeList == null && ifeList.size() == 0) {
                res.statusCode = 404;
                response = 'No Data to be displayed';
                res.responseBody = Blob.valueOf(response);
            } else {
                res.statusCode = 200;
                LTC_FetchIFERecords.IFEData ifeD = new LTC_FetchIFERecords.IFEData();
                response = ifeD.makeString(ifeList);
                res.responseBody = Blob.valueOf(response);
            }
        } catch (Exception ex) {
            if (res != null)  {
                res.statusCode = 500;
            }
            if (LtcUtil.isDevOrg() || Test.isRunningTest()) {
                response = ex.getMessage() + ' ' + ex.getStackTraceString();
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
    }   
}