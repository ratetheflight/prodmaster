/**                     
 * @author (s)      : Ata
 * @description     : Rest API interface for retrieving the passenger information from TravelDB
 * @log:    19JAn2017: version 1.0
 *         
 */
@RestResource(urlMapping='/TravelDB_Info/*')
global class LTC_TravelDB_Info {
    /**
     * Retrieve passenger info. based on PNR and last name provided in the URI
     * from TravelDB
     * Expected Url params: 64 bit encoded encrypted string
     *   SALESFORCE_URL?code=8VJ8g4QePlHrAI+qalakO0+4JH24dxLsSe9q405y082VZ7Z8Vc9FOcQUqAi5HFbdPytDWjQNiK5YXJ9RtNBBWA==  
     **/
    @HttpGet
    global static void getPassengerInfo() {
        String response = null;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        try{
            LtcTravelDBResponseModel responseModel;
            String encodedString = RestContext.request.params.get('code');
            LTC_CallTravelDBService serviceData = new LTC_CallTravelDBService();
            responseModel = serviceData.getPassengerData(encodedString.replaceall('\\s','+'));            
            if (responseModel.errorResponse == 'Decryption failed') {
                res.statusCode = 400;
                response = responseModel.errorResponse;
                res.responseBody = Blob.valueOf(response);
            } else if(responseModel.errorResponse == 'PNR not found in database.'){
                res.statusCode = 404;
                response = responseModel.errorResponse;
                res.responseBody = Blob.valueOf(response); 
            } else {
                response = responseModel.toString();
                res.responseBody = Blob.valueOf(response);
            }
        } catch (Exception ex) {
            if (res != null)  {
                res.statusCode = 500;
            }
            if (LtcUtil.isDevOrg() || Test.isRunningTest()) {
                response = ex.getMessage() + ' ' + ex.getStackTraceString();
            }
            System.debug('Exception:'+ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
    }    
}