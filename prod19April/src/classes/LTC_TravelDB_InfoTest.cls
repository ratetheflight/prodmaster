/* 
*******************************************************************************************************************************************
* File Name     :   LTC_TravelDB_InfoTest
* Description   :   Class to test TravelDB Services 
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    08/02/2017    Ata        Created the class.
******************************************************************************************************************************************* 
*/
@isTest
private class LTC_TravelDB_InfoTest{
    static testmethod void travelDBTest() {
        LTC_FORCETRACK.LTC_WHM_Operations.addSecrets('CURRENTKEY','cIuuKh2roZWEpnv#','abS9*y@8I6*iBt&H');
        LTC_FORCETRACK.LTC_WHM_Operations.addSecrets('PREVIOUSKEY','cIuuKh2roZWEpnv#','abS9*y@8I6*iBt&H');
        LTC_FORCETRACK.LTC_WHM_Operations.addTravelDbCreds('TRAVELDB','W73974394','pwseews');
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl()+'services/apexrest/TravelDB_Info?code=uCXpSrT/m3ngeroDcdM9h0AR4r6VKuVW8MI+h04AC1UsYxxPgr3GsSoTdDWL64QHB2Et0iISoSoylPmCFLE80Q==';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        RestContext.request.params.put('code','uCXpSrT/m3ngeroDcdM9h0AR4r6VKuVW8MI+h04AC1UsYxxPgr3GsSoTdDWL64QHB2Et0iISoSoylPmCFLE80Q==');
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new TravelDBHttpCalloutMock1());
        LTC_TravelDB_Info.getPassengerInfo();
        Test.StopTest();
    }
    static testmethod void travelDBTest2() {
        LTC_FORCETRACK.LTC_WHM_Operations.addSecrets('CURRENTKEY','cIuuKh2roZWEpnv#','abS9*y@8I6*iBt&H');
        LTC_FORCETRACK.LTC_WHM_Operations.addSecrets('PREVIOUSKEY','cIuuKh2roZWEpnv#','abS9*y@8I6*iBt&H');
        LTC_FORCETRACK.LTC_WHM_Operations.addTravelDbCreds('TRAVELDB','W73974394','pwseews');
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl()+'services/apexrest/TravelDB_Info';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        RestContext.request.params.put('code','uCXpSrT/m3ngeroDcdM9h0AR4r6VKuVW8MI+h04AC1UsYxxPgr3GsSoTdDWL64QHB2Et0iISoSoylPmCFLE80Q==');
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new TravelDBHttpCalloutMock2());
        LTC_TravelDB_Info.getPassengerInfo();
        Test.StopTest();
    }
    static testmethod void travelDBTest3() {
        LTC_FORCETRACK.LTC_WHM_Operations.addSecrets('CURRENTKEY','cIuuKh2roZWEpnv#','abS9*y@8I6*iBt&H');
        LTC_FORCETRACK.LTC_WHM_Operations.addSecrets('PREVIOUSKEY','cIuuKh2roZWEpnv#','abS9*y@8I6*iBt&H');
        LTC_FORCETRACK.LTC_WHM_Operations.addTravelDbCreds('TRAVELDB','W73974394','pwseews');
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl()+'services/apexrest/TravelDB_Info';  
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        RestContext.request.params.put('code','uCXpSrT/m3ngeroDcdM9h0AR4r6VKuVW8MI+h04AC1UsYxxPgr3GsSoTdDWL64QHB2Et0iISoSoylPmCFLE80Q==');
        Test.StartTest();
        //Test.setMock(HttpCalloutMock.class, new TravelDBHttpCalloutMock2());
        LTC_TravelDB_Info.getPassengerInfo();
        Test.StopTest();
    }
}