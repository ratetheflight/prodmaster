/**                     
 * @author (s)      : Ataullah Khan(Ata)
 * @description     : class to call TravelDb to fetch customer Data into salesforce 
 *                    based on PNR number and other parameters.
 * @log             : 27Jan2017: version 1.0
 * @log             :  
 *         
 */
public class LTC_TravelDbhttpCall {

    /** Input parameter 
    *   String PNR data
    *   Output : 
                        {  
                           "itinerary":[  
                              {  
                                 "flightNumber":"LO334",
                                 "travelDate":"2017-01-07T07:30:00.000",
                                 "origin":"CDG",
                                 "destination":"WAW",
                                 "seqNo":1
                              },
                              {  
                                 "flightNumber":"LO2001",
                                 "travelDate":"2017-01-07T13:00:00.000",
                                 "origin":"WAW",
                                 "destination":"DXB",
                                 "seqNo":2
                              }
                           ],
                           "passenger":{  
                              "name":"LEVI",
                              "surName":"RUECKER",
                              "email":"AU@TAJAWAL.COM"
                           }
                        }
    **/
    private Boolean callAgain = false;
    private String pnrData;
    public LtcTravelDBResponseModel getCalloutResponseContents(String pnr,String paxFirstName, String paxLastName) {
        System.debug('F name::'+paxFirstName);
        System.debug('L Name:::'+paxLastName);
        System.debug('PNR::'+pnr);
        Boolean archived = false;
        pnrData = pnr;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        List<String> credList = new List<String>();
        String cred =LTC_FORCETRACK.LTC_WHM_Operations.getTravelDbCreds();
        if(cred != null){
            credList = cred .split(':');    
        } else {
            return new LtcTravelDBResponseModel('Credentials missing');
        }
        if (callAgain == true){
            archived = true;
        }
        String textBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:afk0="http://www.af-klm.com/services/passenger/ProvideTravelDetailedInformationParam-v5/xsd"><soapenv:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" soapenv:mustUnderstand="1"><wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="UsernameToken-8"><wsse:Username>'+credList[0]+'</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+credList[1]+'</wsse:Password></wsse:UsernameToken></wsse:Security><To xmlns="http://www.w3.org/2005/08/addressing">https://ws-qvi-rc2.airfrance.fr/passenger/distribmgmt/000590v05</To><Action xmlns="http://www.w3.org/2005/08/addressing">http://www.af-klm.com/services/passenger/ProvideTravelDetailedInformation-v5/provideTravelDetailedInformation</Action><ReplyTo xmlns="http://www.w3.org/2005/08/addressing"><Address>http://www.w3.org/2005/08/addressing/anonymous</Address></ReplyTo><MessageID xmlns="http://www.w3.org/2005/08/addressing">7a28df9a-3020-4c93-8b21-ad4e3539f045</MessageID><trackingMessageHeader xmlns="http://www.af-klm.com/soa/xsd/MessageHeader-V1_0"><consumerRef><userID>X084784</userID><partyID>AF</partyID><consumerID>'+credList[0]+'</consumerID><consumerLocation>QVI</consumerLocation><consumerType>A</consumerType><consumerTime>'+Datetime.now().formatGmt('yyyy-MM-dd\'T\'hh:mm:ss\'.00+00:00\'')+'</consumerTime></consumerRef></trackingMessageHeader><RelatesTo xmlns="http://www.w3.org/2005/08/addressing" RelationshipType="InitiatedBy">7a28df9a-3020-4c93-8b21-ad4e3539f045</RelatesTo></soapenv:Header><soapenv:Body><afk0:DataInElement><pnrCriteria><reclocAMD>'+pnr+'</reclocAMD><archived>'+archived+'</archived><passengerLastName>'+paxLastName+'</passengerLastName></pnrCriteria><passengersList><firstName>'+paxFirstName+'</firstName><lastName>'+paxLastName+'</lastName></passengersList><displayFilter><reservationDisplay><display>true</display><displayContacts>true</displayContacts></reservationDisplay><ticketingDisplay><display>true</display><displayTicketingDetails>true</displayTicketingDetails></ticketingDisplay></displayFilter></afk0:DataInElement></soapenv:Body></soapenv:Envelope>';
       // if(URL.getSalesforceBaseUrl().getHost().mid(0,2) == 'cs'){
       //     req.setEndpoint('https://wscert-rct.airfrance.fr/passenger/distribmgmt/000590v05');
       // }else{
            req.setEndpoint('https://wscert.airfrance.fr/passenger/distribmgmt/000590v05'); 
        //}
        //system.debug( 'timestamp::'+Datetime.now().formatGmt('yyyy-MM-dd\'T\'hh:mm:ss\'.00+00:00\''));    
        req.setMethod('POST');
        req.setBody(textBody.trim());
        //set SSL certificate 
        req.setClientCertificateName('traveldb1');
        req.setHeader('Content-Type', 'application/xml');
        req.setHeader('SOAPAction', '"http://www.af-klm.com/services/passenger/ProvideTravelDetailedInformation-v5/provideTravelDetailedInformation"');
        req.setTimeout(120000);
        req.setHeader('Authorization', LTC_FORCETRACK.LTC_WHM_Operations.encodeTravelDbCreds());
        try {
            res = http.send(req);
            System.debug('response:raw::'+res.getbody());
            LtcTravelDBResponseModel responseModelTDB = getResponseObject(res, paxFirstName, paxLastName);
            System.debug('this is the response:::'+responseModelTDB);
            return responseModelTDB;            
            //return good response
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            return new LtcTravelDBResponseModel('Callout failed');
            //return some bas response
        }
    }
    public LtcTravelDBResponseModel getResponseObject(HttpResponse res,String paxFirstName,String paxLastName){
        Boolean emailFlag = false;
        LtcTravelDBResponseModel responseData = new LtcTravelDBResponseModel();
        List<LtcTravelDBResponseModel.Itinerary> itineraryList = new List<LtcTravelDBResponseModel.Itinerary>();
        LtcTravelDBResponseModel.Passenger passenger ;
        Dom.Document doc = res.getBodyDocument();
        Dom.XMLNode address = doc.getRootElement();
        List<Dom.XMLNode> abc = address.getChildElements();
        List<Dom.XMLNode> z = abc[1].getChildElements()[0].getChildren();
        for(Dom.XMLNode xn1 : abc[1].getChildElements()[0].getChildren()){
            System.debug('xn1 ::'+xn1 );
            if(xn1.getname() == 'detail'){
                String errorstring = xn1.getChildElements()[0].getChildElement('FaultDescription',null).getText();
                if(errorstring == 'PNR not found in database.' && callAgain == false){
                    callAgain = true;
                    responseData = getCalloutResponseContents(pnrData,paxFirstName,paxLastName);              
                    break;
                }    
            }
        }
        for(Dom.XMLNode xn : z[0].getChildren()){
            System.debug('xn:::'+xn);
            if(xn.getname() == 'detail'){
                String errorstring = xn.getChildElement('FaultDescription',null).getText();
                responseData = new LtcTravelDBResponseModel(errorstring);
                break;
            }
            if(xn.getname() == 'flightSegments') {
                String flightSeq = xn.getChildElement('tattoo',null).getText();
                String flightPre = xn.getChildElement('operatingFlight',null).getChildElement('airlineCode',null).getText();
                String flightPost = xn.getChildElement('operatingFlight',null).getChildElement('flightNumber',null).getText();
                String flightOrigin = xn.getChildElement('origin',null).getText();
                String flightDest = xn.getChildElement('destination',null).getText();
                String flightTravelDt = xn.getChildElement('departureDate',null).getText();
                itineraryList.add(new LtcTravelDBResponseModel.Itinerary(flightPre+flightPost, flightTravelDt, flightOrigin, flightDest, Integer.valueof(flightSeq)));
            }
            if(xn.getname() == 'contacts') {
                for(Dom.XMLNode node: xn.getChildElements()) {
                    if(node.getName() == 'decodedType') {
                       if(node.getText().toUpperCase() == 'EMAIL'){  
                           emailFlag = true;
                           String paxEmail = xn.getChildElement('decodedValue',null).getText(); 
                           passenger = new LtcTravelDBResponseModel.Passenger(paxFirstName, paxLastName, paxEmail); 
                       }
                    }
                }
                if(! emailFlag){
                    passenger = new LtcTravelDBResponseModel.Passenger(paxFirstName, paxLastName, ''); 
                }
            }
        }
        System.debug('data all::'+itineraryList + passenger );
        if(itineraryList != null && itineraryList.size() > 0 && passenger != null) {
            itineraryList.sort();
            responseData = new LtcTravelDBResponseModel(LtcTravelDBResponseModel.checkItinerary(itineraryList), passenger);
            System.debug('responseData:LAST:'+responseData);
        }
        return responseData;
    }
}