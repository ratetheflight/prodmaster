/*************************************************************************************************
* File Name     :   LiveChatTranscriptTriggerHandler  
* Description   :   Contains methods called from Live Chat Transcript trigger
* @author       :   Prasanna kumar
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     02/03/2017      Prasanna         Created the class (ST-1658)


*/
public class LiveChatTranscriptTriggerHandler {
    
    
    public void updatecaseinformation(list<LiveChatTranscript>chatT)
    {
    List <LiveChatTranscript> toUpdate=new List<LiveChatTranscript>();
    Set <Id> caseids=new Set<Id>();
    List <Case> tobeUpdated= new List<Case>();
    Map<ID,LiveChatTranscript> chatMap= new Map<Id,LiveChatTranscript>();
    for (LiveChatTranscript lchat: chatT){
    
    caseids.add(lchat.caseid);
    // if(lchat.accountid==null){
   // lchat.accountid=lchat.Case_Account__c;
  //  toUpdate.add(lchat);
    //}
    }
    // if(toUpdate.size() > 0){
   // update toUpdate;
   // }
   List <LiveChatTranscript> livechatlist=new List <LiveChatTranscript>();
   List<Case>  caselist=[select id,accountid from case where id in:caseids];
   for(LiveChatTranscript lc: chatT){
   chatMap.put(lc.caseid,lc); 
   }
   if(caselist.size() > 0){
  // LiveChatTranscript tempChat=new LiveChatTranscript();
   for(Case cs: caselist){
  LiveChatTranscript tempChat=chatMap.get(cs.id);
   system.debug('Pras:tempChat'+tempChat);
   if(tempChat!=null){
   system.debug('Pras:Inside chatMap:'+chatMap.get(cs.id));
   cs.chat_message__c=tempChat.body;
   cs.chat_key__c=tempChat.chatKey;
   cs.status='Closed';
   tobeUpdated.add(cs);
   //tempChat.clear();
   }
   }
   
   }
   if(tobeUpdated.size() > 0){
   try{
   update tobeUpdated;
   }
   catch(exception e){}
   }
   
        
}

}