/**
* @author (s)    : Satheeshkumar Subramani
* @description   : Apex Batch class to change old link to new link for FlightGuide in SMH
* @log           : 02 December 2015
* 
* Call anonymous: Id batjobId = Database.executeBatch(new LtcChangeLinkSMHBatch(), 200);
*/
global class LtcChangeLinkSMHBatch implements Database.Batchable<Case> {
    
    global Iterable<Case> start(Database.BatchableContext bc){
        RecordType rt = [
        	SELECT Id, Name 
        	FROM RecordType 
        	WHERE Name 
        	LIKE 'Rating Case' 
        	AND SobjectType = 'Case' 
        	LIMIT 1
        ];
        System.debug('*** Retrieved record type: ' + rt.Name);
        
        String likeVariable = 'https://full-klm.cs17.force.com/NgFlightGuide/%';
        if (!getInstanceUrl().equals('https://flightguide-fe-acc.herokuapp.com')) {
        	likeVariable = 'https://klm.secure.force.com/NgFlightGuide/%';
        }        
        List <Case> cl = [
        	SELECT id, klm_com_link__c 
    		FROM Case 
    		WHERE  RecordTypeId = :rt.Id 
    		AND klm_com_link__c 
    		LIKE :likeVariable 
    		AND Origin = 'Rate the flight' 
    		LIMIT 5000
        ];
        return cl;      
    }
    
    global void execute(Database.BatchableContext bc, LIST<Case> cl) {
        if (getInstanceUrl().equals('https://flightguide-fe-acc.herokuapp.com')) {
            for (Case c:cl) {
                c.klm_com_link__c = c.klm_com_link__c.replace('https://full-klm.cs17.force.com/NgFlightGuide','https://flightguide-fe-acc.herokuapp.com');
            } 
        } else {
            for (Case c:cl) {
                c.klm_com_link__c = c.klm_com_link__c.replace('https://klm.secure.force.com/NgFlightGuide','https://flightguide.klm.com');
            } 
        }  
        update cl;         
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
    private String getInstanceUrl(){
        RatingService__c ratingCustomSettings = RatingService__c.getOrgDefaults();
        System.debug('ratingCustomSettings.LTC_Base_Url__c=' +  ratingCustomSettings.LTC_Base_Url__c);
        return ratingCustomSettings.LTC_Base_Url__c;
    }
}