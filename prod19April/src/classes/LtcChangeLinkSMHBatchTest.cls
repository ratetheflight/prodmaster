/**
 * @author (s)    :	Satheeshkumar Subramani
 * @description   : Apex Batch class to change old link to new link for FlightGuide in SMH
 * @log           : 02 December 2015
 */
@isTest(SeeAllData=true)
private class LtcChangeLinkSMHBatchTest {

    static testMethod void test0_AllCases() {      
        Test.startTest();
		RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Rating Case' AND SobjectType = 'Case' LIMIT 1];
		
		RatingService__c ratingCustomSettings = RatingService__c.getOrgDefaults();
        ratingCustomSettings.LTC_Base_Url__c = 'https://flightguide-fe-acc.herokuapp.com';
        update ratingCustomSettings;
		        
        insertTestObject('test01','Rate the flight', rt, 'https://full-klm.cs17.force.com/NgFlightGuide/#/nl/nl/KL0706/2014-11-30/2014-11-30/2014-11-30/KL0706/ratingoverview');
        insertTestObject('test02','Rate the flight', rt, 'https://full-klm.cs17.force.com/NgFlightGuide/#/gb/en/KL1001/2015-04-30/2015-04-30/2015-04-30/KL1001/ratingoverview');
        
        Id batchjobId = Database.executeBatch(new LtcChangeLinkSMHBatch(), 200);
        System.assert(batchjobId != null);        
        Test.stopTest();

        Case testObj1 = [select klm_com_link__c from Case WHERE Reference__c = 'test01'];     
        System.assertEquals('https://flightguide-fe-acc.herokuapp.com/#/nl/nl/KL0706/2014-11-30/2014-11-30/2014-11-30/KL0706/ratingoverview', testObj1.klm_com_link__c);
    }
    
    private static void insertTestObject(String reference, String origin, RecordType rt, String klmComLink){
       Case c = new Case(
       		Reference__c = reference,
            Origin = origin,
	        RecordTypeId = rt.Id,
	        klm_com_link__c = klmComLink
        );
        insert c;
    }
}