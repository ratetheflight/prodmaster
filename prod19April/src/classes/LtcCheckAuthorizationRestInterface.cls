/**                     
 * Rest API interface for checking if the caller is authorized 
 * without putting any processing penalty on the server. 
 * 
 * @author       : Mees Witteman
 * @log          : OKT2016: version 1.0
 */
@RestResource(urlMapping='/CheckAuthorization/*')
global class LtcCheckAuthorizationRestInterface { 
	
	/**
     * Implicit token check end-point
     **/
    @HttpGet
    global static void checkAuthorization() {}
    
}