/**
 * @author (s)      : Mees Witteman
 * @description     : Test class to test the LtcCheckAuthorizationRestInterface class.
 *                    The class does not contain any business logic but is just a placeholder
 *                    to provide an endpoint to test the Authorization header. 
 */
@isTest
public with sharing class LtcCheckAuthorizationRestInterfaceTest {
	
	static testMethod void callCheckAuthorizationMethod() {
		LtcCheckAuthorizationRestInterface.checkAuthorization();
	}

}