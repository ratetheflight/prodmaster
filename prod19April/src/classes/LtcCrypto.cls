/**                     
 * @author (s)      : David van 't Hooft
 * @description     : class to obfuscate text, changing the key means that links will not work anymore.
 * @log             :   2MAY2014: version 1.0
 * @log             :  17JUN2016: version 2.0 move keys to static for JSON object mapper
 *         
 */
public with sharing class LtcCrypto {
	private static String hulpKey = 'L@.(,;<::*_><&~T-A=aZ/z4{]^f$#5C'; 
	private static Blob key = Blob.valueOf(hulpKey); //Needs to be 32 charachters long
	
	/**
	 * Default constructor
	 **/
	public LtcCrypto() {
	}
	
	/**
	 * Encode the the text 
	 **/
	public String encodeText(String text) {
		String result = text;
		try {
		    Blob cipherText = Crypto.encryptWithManagedIV('AES256', key, Blob.valueOf(text));
		    String tmpStr = EncodingUtil.base64Encode(cipherText);
		    String encodedStr = EncodingUtil.urlEncode(tmpStr, 'UTF-8');
		    cipherText = Blob.valueOf(encodedStr);
	    	result = EncodingUtil.base64Encode(cipherText); 		
		} catch (Exception ex) {
			//If this fails we just do not do it for LTC
		}
		return result;
	}
	
	/**
	 * Decode the text
	 **/
	public String decodeText(String text) {
		String result = text;
		try {
		    Blob encodedEncryptedBlob = EncodingUtil.base64Decode(text);	    
		    String decodedStr = EncodingUtil.urlDecode(encodedEncryptedBlob.toString(), 'UTF-8');
		    encodedEncryptedBlob = EncodingUtil.base64Decode(decodedStr);	    
		    Blob decryptedBlob = Crypto.decryptWithManagedIV('AES256', key, encodedEncryptedBlob);
	    	return decryptedBlob.toString();
		} catch (Exception ex) {
			//If this failes we just do not do it for LTC
		}
		return result;
	}
}