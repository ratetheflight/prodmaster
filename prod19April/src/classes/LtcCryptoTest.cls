/**
 * @author (s)      : David van 't Hooft
 * @description     : Test class to test the LtCCrypto class
 * @log             :  2MAY2014: version 1.0
 */
@isTest
private class LtcCryptoTest {

	/**
	 * Check encode and decode function
	 **/
    static testMethod void ltcCryptoUnitTest() {
        LtcCrypto ltcc = new LtcCrypto();
        String text1 = 'MySecretText';

        System.debug(LoggingLevel.DEBUG, 'start text: ' + text1);
        String text2 = ltcc.encodeText(text1);
        System.debug('encrypted text: ' + text2);
        System.assertNotEquals(text1, text2, 'Should be different');
        String text3 = ltcc.decodeText(text2);
        System.debug('end text: ' + text3);
        System.assertEquals(text1, text3, 'After encode and decode it should be the same');
    }
}