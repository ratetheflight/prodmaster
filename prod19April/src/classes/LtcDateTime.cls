/**                     
 * @author (s)      : Mees Witteman
 * @description     : Date time data holder
 * @log             : 15 FEB 2016 version 1.0
 */
 public with sharing class LtcDateTime {
 	
 	private Date d;
 	private String t;
 	private DateTime dt;
 	
 	public LtcDateTime(String ddMMMyy, String HHmm, String utcOffset) {
 		dt = LtcUtil.getDateTime(ddMMMyy, HHmm, utcOffset);
 		d = dt.dateGMT();
 		t = dt.format('HH:mm', '+00:00'); 	
 	}
 	
 	public LtcDateTime(Date day, String HHmm, String utcOffset) {
 		dt = LtcUtil.getDateTime(day, HHmm, utcOffset);
 		d = dt.dateGMT();
 		t = dt.format('HH:mm', '+00:00'); 	
 	}
 	
 	public DateTime getDateTime() {
 		return dt;
 	}
 	
 	public Date getDate() {
 		return d;
 	}
 	
 	public String getTime() {
 		return t;
 	}
    
}