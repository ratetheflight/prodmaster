/**
 * @author (s)      : David van 't Hooft
 * @description     : Date formatter Test class
 * @log:   8MAY2014: version 1.0
 */
@isTest
private class LtcDateTimeFormatterTest {

    /**
     * Test the date time formatter by locale
     **/
    static testMethod void dateTimeFormatterUnitTest() {
        LtcDateTimeFormatter ltcDateTimeFormatter = new LtcDateTimeFormatter();

        DateTime dt = DateTime.newInstance(2014, 8, 5, 12, 30, 10);
        String formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, 'en-GB');
        system.assertEquals('05/08/2014 12:30', formattedDateTime);
        formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, 'nl-NL');
        system.assertEquals('5-8-2014 12:30', formattedDateTime);
        formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, 'lt');
        system.assertEquals('2014.8.5 12.30', formattedDateTime);
        formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, '');
        system.assertEquals('05/08/2014 12:30', formattedDateTime);
    }
    
    /**
     * Test the date formatter by locale
     **/
    static testMethod void dateFormatterUnitTest() {
        LtcDateTimeFormatter ltcDateTimeFormatter = new LtcDateTimeFormatter();

        Date d = Date.newInstance(2014, 8, 5);
        String formattedDate = ltcDateTimeFormatter.formatDate(d, 'en-GB');
        system.assertEquals('05/08/2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, 'nl-NL');
        system.assertEquals('5-8-2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, 'nl-UNKNOWN'); //default to nl
        system.assertEquals('5-8-2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, 'zz-UNKNOWN'); // default to en
        system.assertEquals('05/08/2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, null);
        system.assertEquals('05/08/2014', formattedDate);
    }

}