/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman, Satheeshkumar Subramani,Ata
 * @description     : class to process the flight info
 * @log             : 06JUN2014 version  1
 * @log             : 2014/2015 version  2 Flightguide improvements
 * @log             : March 2016 version 3 Added iata-code of ac-type to select if registration-code is missing from the flight
 * @log             : 2nd Dec 2016 version 4 Change in getFlightinfo method blueprint to include Leg origin. Related to deeplink. 
 * @log             : 7th DEC 2016 version 5 Changes in weather method to show forecast data
 * @log             : 21st DEC 2016 version 6 Changes related to 
 */
public with sharing class LtcFlightInfo {
    public Date tempTravelDate;
    public LtcFlightResponseModel getFlightInfo(String flightNumberInput, String travelDate, String acceptLanguage, String legOrigin, String uid) {  
        tempTravelDate = Date.Valueof(travelDate);
        String response = '';   
        String flightNumber = LtcUtil.cleanFlightNumber(flightNumberInput);
        Monitor_Flight__c monFlight = getMonitorFlight(flightNumber);
        if (monFlight == null) {
            return new LtcFlightResponseModel('noMonitorFlight'); 
        }
        
        Flight__c flight = getFlight(flightNumber, travelDate, legOrigin);
        if (flight == null) {
            return new LtcFlightResponseModel('flightNotFound'); 
        } else {
            System.debug('loaded flight=' + flight);
        }

        // gather the weather for destination(s) 
        LtcFlightResponseModel.Weather weather;
        Map<String, LtcFlightResponseModel.Weather> weatherMap = new Map<String, LtcFlightResponseModel.Weather>();
        for (Leg__c leg : flight.legs__r) {
            LtcAirport__c destination = getAirportInfo(leg.destination__c);
            try {
                weather = getWeather(destination.city_code__c);
                weatherMap.put(destination.city_code__c, weather);
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, 'get weather failed for ' + leg.destination__c + ' ' + e);
            }
        }
        
        if (reloadFromFoxNeeded(flight)) {
            System.debug('reload flight ' + flightNumber + ' on ' + traveldate);
            reloadFlightFromFox(flight);
            flight = getFlight(flightNumber, travelDate, legOrigin);
        }

        if (weather != null && weather.newAccessToken != null) {
            LtcLocationsApiSettings__c setting = LtcLocationsApiSettings__c.getOrgDefaults();
            setting.oAuthAccessToken__c = weather.newAccessToken;
            upsert setting;
        }
        
        LtcAircraftType__c acType = null;
        LtcAircraft__c aircraft = getAircraft(flight.registrationCode__c);
        if (aircraft != null) {
            acType = getAircraftType(aircraft); 
        } else {
            acType =  getAircraftType(flight.aircraftTypeCode__c);
            if (acType != null) {
                aircraft = new LtcAircraft__c();
            }
        }
 
        Date arrivalDate = determineArrivalDate(flight);
        String arrivalTime = determineArrivalTime(flight);
        Datetime arrivalDateTime = determineArrivalDateTime(arrivalDate, arrivalTime);
        Integer timeToArrival = (Integer) flight.legs__r[(Integer) flight.currentLeg__c].timeToArrival__c;
        String ratingStatus = LtcUtil.determineRatingStatus(arrivalDateTime, timeToArrival, uid);
        String crewLanguages = monFlight.Languages__c;
    
        LtcFlightResponseModel ltcFlightResponseModel = new LtcFlightResponseModel(flight, aircraft, acType, ratingStatus, crewLanguages, acceptLanguage);
                    
        for (Leg__c leg : flight.legs__r) {
            LtcAirport__c origin = getAirportInfo(leg.origin__c);         
            LtcFlightResponseModel.Airport orig = getAirport(origin, acceptLanguage, null);
            
            LtcAirport__c destination = getAirportInfo(leg.destination__c);         
            weather = weatherMap.get(destination.city_code__c);
            LtcFlightResponseModel.Airport dest = getAirport(destination, acceptLanguage, weather);
            
            LtcFlightResponseModel.Leg responseLeg = new LtcFlightResponseModel.Leg(leg, orig, dest);
            ltcFlightResponseModel.flight.addLeg(responseLeg);           
            addServiceItems(flight, responseLeg, leg, acType);
        }
        
        return ltcFlightResponseModel; 
    }

    private void addServiceItems(Flight__c flight, LtcFlightResponseModel.Leg responseLeg, Leg__c leg, LtcAircraftType__c acType) {
    
        List<CabinClass__c> classes = [
            select c.Label__c, c.Id, c.Description__c           
            From CabinClass__c c
            limit 1000
        ];
                
        LtcFlightResponseModel.CabinClass rmClass;
        List<LtcFlightResponseModel.ServiceItem> serviceItems;
        LtcFlightResponseModel.ServiceItem serviceItem;
        
        List<LegClassItem__c> lcItems = [
            select 
                l.ServiceItem__r.Type__c, 
                l.ServiceItem__r.Description__c, 
                l.ServiceItem__r.Name__c, 
                l.ServiceItem__c, 
                l.Order__c,
                l.LegClassPeriod__r.CabinClass__r.id
            from LegClassItem__c l
            where l.legClassPeriod__r.FlightNumber__c =: flight.flight_number__c
            and   l.legClassPeriod__r.LegNumber__c =: (Integer) leg.legnumber__c
            and   l.legClassPeriod__r.StartDate__c <=: leg.scheduledDepartureDate__c
            and   l.legClassPeriod__r.EndDate__c >=: leg.scheduledDepartureDate__c
            order by l.legClassPeriod__r.cabinClass__c, l.Order__c
        ];
        
        for (CabinClass__c cabinClass : classes) {
            serviceItems = new  List<LtcFlightResponseModel.ServiceItem>();
            for (LegClassItem__c lcItem : lcItems) {
                if (acType != null && (
                        'Embraer 190'.equals(acType.name) || 
                        'Embraer 175'.equals(acType.name) || 
                        'Fokker 70'.equals(acType.name))) {
                    if (lcItem.ServiceItem__r.name__c.contains('_klc_')) {
                        if (lcItem.legclassperiod__r.cabinclass__r.id.equals(cabinClass.id)) {
                            serviceItem = new LtcFlightResponseModel.ServiceItem(
                                'klmmj.' + lcItem.ServiceItem__r.name__c, 
                                'klmmj.' + lcItem.ServiceItem__r.description__c, 
                                (Integer) lcItem.order__c, 
                                lcItem.ServiceItem__r.type__c
                            );
                            serviceItems.add(serviceItem);
                        }
                    }
                } else {
                    if (!lcItem.ServiceItem__r.name__c.contains('_klc_')) {
                        if (lcItem.legclassperiod__r.cabinclass__r.id.equals(cabinClass.id)) {
                            System.debug('service item name=' + lcItem.ServiceItem__r.name__c);
                            serviceItem = new LtcFlightResponseModel.ServiceItem(
                                'klmmj.' + lcItem.ServiceItem__r.name__c, 
                                'klmmj.' + lcItem.ServiceItem__r.description__c, 
                                (Integer) lcItem.order__c, 
                                lcItem.ServiceItem__r.type__c
                            );
                            serviceItems.add(serviceItem);
                        }
                    }                    
                }
            }
            if (serviceItems.size() > 0) {
                System.debug('service item label-' + cabinClass.label__c + 'desc- ' + cabinClass.description__c);
                rmClass = new LtcFlightResponseModel.CabinClass('klmmj.' + cabinClass.label__c, 'klmmj.' + cabinClass.description__c, serviceItems); 
                responseLeg.addClass(rmClass);
            }
        }
    }
    
    /**
     * Get the flight, load from SF
     **/    
    private Flight__c getFlight(String flightNumber, String travelDate, String legOrigin) {
        Date trDate = Date.valueof(travelDate);
        Flight__c flight = loadFlight(flightNumber, trDate, legOrigin);

        return flight;                  
    }
    
    /**
     * Load the flight from SF
     **/
    private Flight__c loadFlight(String flightNumber, Date travelDate, String legOrigin) {
        Flight__c flight = null;        
        if (legOrigin != null && legOrigin != '') {
            List<Leg__c> tLeg = [
                 Select 
                    id,
                    departureDate__c,
                    scheduledDepartureDate__c ,
                    origin__c,
                    Flight__r.Scheduled_Departure_Date__c
                    from Leg__c
                    Where Origin__c =: legOrigin
                    AND scheduledDepartureDate__c =: travelDate
                    AND  Flight__r.Flight_Number__c =: flightNumber];        
            if (tLeg != null && tLeg.size() > 0){
                travelDate = tLeg[0].Flight__r.Scheduled_Departure_Date__c;
            } else {
                return flight;
            }          
        }            
        List<Flight__c> flightList = [
             select 
                f.currentLeg__c,
                f.Flight_Number__c, 
                f.registrationCode__c,
                f.aircraftTypeCode__c,
                f.Scheduled_Departure_Date__c, 
                f.LastModifiedDate,
                (select 
                    Leg__c.actualArrivalDate__c,
                    Leg__c.actualArrivalTime__c,
                    Leg__c.actualDepartureDate__c,
                    Leg__c.actualDepartureTime__c,
                    Leg__c.arrivalDate__c,
                    Leg__c.arrivalDelay__c,
                    Leg__c.departureDate__c,
                    Leg__c.departureDelay__c,
                    Leg__c.destination__c,
                    Leg__c.estimatedArrivalDate__c,
                    Leg__c.estimatedArrivalTime__c,
                    Leg__c.estimatedDepartureDate__c,
                    Leg__c.estimatedDepartureTime__c,
                    Leg__c.legNumber__c,
                    Leg__c.origin__c,
                    Leg__c.scheduledArrivalDate__c,
                    Leg__c.scheduledArrivalTime__c,
                    Leg__c.scheduledDepartureDate__c,
                    Leg__c.scheduledDepartureTime__c,
                    Leg__c.status__c,
                    Leg__c.timeToArrival__c,
                    Leg__c.timeToDeparture__c
                 from f.Legs__r
                 order by Leg__c.legNumber__c
                )
            from Flight__c f
            where f.Flight_Number__c =:flightNumber 
            and f.Scheduled_Departure_Date__c =: travelDate
        ];
        //Result should only be one or none!
        if (flightList != null && !flightList.isEmpty()) {
            flight = flightList[0];
        }
        return flight;
    }
    
    /**
     * Reload from fox if no actuals available and scheduled departure is less than 3 days ago
     */
    private Boolean reloadFromFoxNeeded(Flight__c flight) {
        Boolean result = false;
        if (hasLegWithoutActual(flight)
                && System.now().dateGmt().daysBetween(flight.scheduled_departure_date__c.addDays(3)) > 0) {
            result = true;
        }
        return result;
    }
    
    /**
     * return true when there is at least one leg that does not have actual flight data
     */
    private Boolean hasLegWithoutActual(Flight__c flight) {
        Boolean result = false;
        for (Leg__c leg : flight.legs__r) {
            if (leg.actualarrivaltime__c == null || ''.equals(leg.actualarrivaltime__c) || leg.timeToArrival__c > 0) {
                result = true;
            }
        }
        return result;
    }
    
    private void reloadFlightFromFox(Flight__c flight) {
        LtcFoxService ltcFoxService =  new LtcFoxService();
        String flightType = 'departure';
        String flightNumber = flight.flight_number__c;
        String travelDate = String.valueOf(flight.scheduled_departure_date__c);
        // collect updates / upserts to prevent governor limit errors
        List<Leg__c> legsToUpsert = new List<Leg__c>();
        List<Flight__c> flightsToUpdate = new List<Flight__c>();

        System.debug('Call fox service to receive flight info');
        LtcFoxResponseModel ltcFoxResponseModel = ltcFoxService.getFoxFlightInfo(flightType, flightNumber, travelDate);
        if (ltcFoxResponseModel != null && ltcFoxResponseModel.flights != null && !ltcFoxResponseModel.flights.isEmpty()) {
            LtcFoxResponseModel.Flight foxFlight = ltcFoxResponseModel.flights[0];
            
            // populate flight with FOX data but restore registrationcode from PISA
            String pisaReg = flight.registrationCode__c;
            foxFlight.populateFlight(flight);
            flight.registrationCode__c = pisaReg;
            
            flight.Flight_Number__c = flightNumber;
            
            for (Integer i = 0 ; i < foxFlight.legs.size(); i++) {
                LtcFoxResponseModel.Leg foxLeg = foxFlight.legs[i];
                Leg__c leg = LtcUtil.findLeg(flight, foxLeg.legNumber);
                if (leg == null) {
                    leg = new Leg__c();
                    leg.flight__c = flight.id;
                    flight.legs__r.add(leg);
                    System.debug('add leg ' + leg);
                }
                foxLeg.populateLeg(leg);
                legsToUpsert.add(leg);
            }
            flightsToUpdate.add(flight);
            System.debug('update flight ' + flight);
        } else {
            System.debug('flight ' + flightNumber + ' on ' + travelDate + ' not found in fox');
        }
        
        // no need to update in 200 item blocks; will remain well under that number
        upsert legsToUpsert;
        update flightsToUpdate;
    }
    
    private Date determineArrivalDate(Flight__c flight) {
        Leg__c leg = LtcUtil.currentLeg(flight);
        Date result = leg.actualArrivalDate__c;
        if (result == null) {
            result = leg.estimatedArrivalDate__c;
        }
        if (result == null) {
            result = leg.scheduledArrivalDate__c;
        }
        return result;
    }
    
    private String determineArrivalTime(Flight__c flight) {
        Leg__c leg = LtcUtil.currentLeg(flight);
        String result = leg.actualArrivalTime__c;
        if (result == null || ''.equals(result)) {
            result = leg.estimatedArrivalTime__c;
        }
        if (result == null || ''.equals(result)) {
            result = leg.scheduledArrivalTime__c;
        }
        return result;
    }
    
    private DateTime determineArrivalDateTime(Date arrivalDate, String arrivalTime) {
        DateTime arrivalDateTime;
        if (arrivalDate != null) {
            Integer hh = 00;
            Integer mm = 12;
            if (arrivalTime != null) {
                hh = Integer.valueOf(arrivalTime.substring(0,2));
                mm = Integer.valueOf(arrivalTime.substring(3,5));
            }
            Integer day = arrivalDate.day();
            Integer month = arrivalDate.month();
            Integer year = arrivalDate.year();
            arrivalDateTime = Datetime.newInstanceGmt(year, month, day, hh, mm, 0);
        }
        return arrivalDateTime;
    }
    
    /**
     * Get the monitor flight or null if no match is found
     **/    
    private Monitor_Flight__c getMonitorFlight(String flightNumber) {
        Monitor_Flight__c flight;
        List<Monitor_Flight__c> flightList = [
            select f.Languages__c, f.Flight_Number__c 
            from Monitor_Flight__c f 
            where f.Flight_Number__c =:flightNumber
        ];
        if (flightList!=null && !flightList.isEmpty()) {
            flight = flightList[0];
        }
        return flight;
    }

        
    /**
     * Get the aircraft by registration code
     **/    
    public LtcAircraft__c getAircraft(String registrationCode) {

        System.debug('registrationCode=' + registrationCode);
        LtcAircraft__c aircraft = null;
        List<LtcAircraft__c> aircraftList = [
            select 
                a.aircraft_type__c,
                a.name,
                a.aircraft_Name__c,
                a.Livery__c,
                a.Wi_Fi__c,
                a.Power_Plug__c
            from LtcAircraft__c a 
            where a.Name =:registrationCode
        ];
        if (aircraftList != null && !aircraftList.isEmpty()) {
            aircraft = aircraftList[0];
        }
        return aircraft;                
    }
    
    /**
     * Get the ac type of an airplane by aircraft
     **/    
    public LtcAircraftType__c getAircraftType(LtcAircraft__c aircraft) {
        LtcAircraftType__c aircraftType;
        String aircraftTypeId;
        if (aircraft != null && aircraft.Aircraft_type__c != null) {
            aircraftTypeId = aircraft.Aircraft_type__c;
            List<LtcAircraftType__c> aircraftTypeList = [
                select 
                    LtcAircraftType__c.name,
                    LtcAircraftType__c.WIFI__C,
                    LtcAircraftType__c.Power_Plug__c,   
                    LtcAircraftType__c.IATA_Code__c,
                    LtcAircraftType__c.amount_owned_by_KLM__c,
                    LtcAircraftType__c.Image_URL__c,
                    LtcAircraftType__c.length__c,
                    LtcAircraftType__c.length_unit__c,
                    LtcAircraftType__c.width__c,
                    LtcAircraftType__c.width_unit__c,
                    LtcAircraftType__c.max_weight__c,
                    LtcAircraftType__c.max_weight_unit__c,
                    LtcAircraftType__c.max_distance__c,
                    LtcAircraftType__c.max_distance_unit__c,
                    LtcAircraftType__c.cruising_speed__c,
                    LtcAircraftType__c.cruising_speed_unit__c,
                    LtcAircraftType__c.nr_of_flights_per_week__c,
                    LtcAircraftType__c.nr_of_cycle_hours_per_week__c,
                    LtcAircraftType__c.nr_of_seats__c,
                    LtcAircraftType__c.Senior_Pursers__c, 
                    LtcAircraftType__c.Pursers__c, 
                    LtcAircraftType__c.Economy_CA_s__c, 
                    LtcAircraftType__c.businessclass_CA_s__c,
                    (select 
                        LtcAircraftType_Fact__c.name, 
                        LtcAircraftType_Fact__c.content__c, 
                        LtcAircraftType_Fact__c.description__c
                     from LtcAircraftType__c.Aircraft_type_facts__r
                    )
                from LtcAircraftType__c
                where 
                    LtcAircraftType__c.Id =:aircraftTypeId
            ];
            if (aircraftTypeList != null && !aircraftTypeList.isEmpty()) {
                aircraftType = aircraftTypeList[0];
            }
        } 
        return aircraftType;                  
    }
    
    /**
     * Get the Aircrafttype object by IATA code of the AC type
     **/    
    public LtcAircraftType__c getAircraftType(String acTypeIataCode) {
        LtcAircraftType__c aircraftType;
        if (acTypeIataCode != null) {
           List<LtcAircraftType__c> aircraftTypeList = [
                select 
                    LtcAircraftType__c.name,
                    LtcAircraftType__c.WIFI__C,
                    LtcAircraftType__c.Power_Plug__c,
                    LtcAircraftType__c.IATA_Code__c,
                    LtcAircraftType__c.amount_owned_by_KLM__c,
                    LtcAircraftType__c.Image_URL__c,
                    LtcAircraftType__c.length__c,
                    LtcAircraftType__c.length_unit__c,
                    LtcAircraftType__c.width__c,
                    LtcAircraftType__c.width_unit__c,
                    LtcAircraftType__c.max_weight__c,
                    LtcAircraftType__c.max_weight_unit__c,
                    LtcAircraftType__c.max_distance__c,
                    LtcAircraftType__c.max_distance_unit__c,
                    LtcAircraftType__c.cruising_speed__c,
                    LtcAircraftType__c.cruising_speed_unit__c,
                    LtcAircraftType__c.nr_of_flights_per_week__c,
                    LtcAircraftType__c.nr_of_cycle_hours_per_week__c,
                    LtcAircraftType__c.nr_of_seats__c,
                    LtcAircraftType__c.Senior_Pursers__c, 
                    LtcAircraftType__c.Pursers__c, 
                    LtcAircraftType__c.Economy_CA_s__c, 
                    LtcAircraftType__c.businessclass_CA_s__c,
                    (select 
                        LtcAircraftType_Fact__c.name, 
                        LtcAircraftType_Fact__c.content__c, 
                        LtcAircraftType_Fact__c.description__c
                     from LtcAircraftType__c.Aircraft_type_facts__r
                    )
                from LtcAircraftType__c
                where LtcAircraftType__c.IATA_Code__c = :acTypeIataCode
            ];
            if (aircraftTypeList != null && !aircraftTypeList.isEmpty()) {
                aircraftType = aircraftTypeList[0];
            }
        }
        return aircraftType;                  
    }
    
    /**
     * Retrieve the airport object
     **/
    private LtcAirport__c getAirportInfo(String IATACode) {
        LtcAirport__c ltcAirport = new LtcAirport__c();
        List<LtcAirport__c> airportList = [
            select a.Name, a.Airport_Name__c, a.Airport_Code__c, a.City__c, UTC_Offset__c, a.Country__c, a.City_Page_Link__c, a.City_Code__c
            from LtcAirport__c a 
            where a.Name = :IATACode
        ];
        if (airportList != null && !airportList.isEmpty()) {
            ltcAirport = airportList[0];
        }
        if (ltcAirport.airport_Name__c == null) {
            ltcAirport.airport_Name__c = '';
        }
        if (ltcAirport.city__c == null) {
            ltcAirport.city__c = '';
        }        
        if (ltcAirport.city_Page_Link__c == null) {
            ltcAirport.city_Page_Link__c = '';
        }
        if (ltcAirport.country__c == null) {
            ltcAirport.country__c = '';
        }
        if (ltcAirport.utc_Offset__c == null) {
            ltcAirport.utc_Offset__c = 0;
        }
        return ltcAirport;
    }
    
    private LtcFlightResponseModel.Airport getAirport(LtcAirport__c ltcAirport, String acceptLanguage, LtcFlightResponseModel.Weather weather) {
        LtcFlightResponseModel.Airport airport = new LtcFlightResponseModel.Airport(
                ltcAirport.name,
                LtcUtil.translate(ltcAirport.airport_name__c, acceptLanguage), 
                LtcUtil.translate(ltcAirport.city__c, acceptLanguage),
                LtcUtil.translate(ltcAirport.country__c, acceptLanguage),
                weather, ltcAirport.city_Page_Link__c, ltcAirport.city_code__c);
        return airport;
    }
    
    private LtcFlightResponseModel.Weather getWeather(String cityCode) {
        LtcFlightResponseModel.Weather weather = null;
        if (cityCode != null) {
           LtcLocationsWeatherResponse wr = LtcLocationsApiService.getWeatherForCity(cityCode);                  
            if (wr != null) {
                integer index = 0;
                Integer day = Date.Today().daysBetween(tempTravelDate);
                if ( tempTravelDate > Date.Today() && (day <= 4 && day >= 0) ) {
                    for (integer i=0;i<= 4;i++) {
                        if (Date.valueof(wr.forecast[i].dater) == Date.valueof(tempTravelDate)){
                            index = i;
                        }
                    }
                    LtcFlightResponseModel.WeatherDescription weatherDescription = 
                        new LtcFlightResponseModel.WeatherDescription(wr.forecast[index].description.id, wr.forecast[index].description.icon, wr.forecast[index].description.value);
                
                    weather = new LtcFlightResponseModel.Weather('',wr.forecast[index].maxTemp,wr.forecast[index].minTemp, weatherDescription, wr.newAccessToken);              
                } else {
                    LtcFlightResponseModel.WeatherDescription weatherDescription = 
                        new LtcFlightResponseModel.WeatherDescription(wr.actual.description.id, wr.actual.description.icon, wr.actual.description.value);
                    weather = new LtcFlightResponseModel.Weather(wr.actual.temp,'','', weatherDescription, wr.newAccessToken);          
                }
            } 
        }
        return weather;
    }
}