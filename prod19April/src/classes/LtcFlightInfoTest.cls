/**
 * @description     : Test FlightInfo class
 * @log             : Mees Witteman 30JUN2016: version 1.0
 */
@isTest
private class LtcFlightInfoTest {

    static testMethod void noMonitor() {
        String flightNumber = 'KL1111';
        String travelDate = setup(flightNumber);
        String acceptLanguage = 'nl-NL';
        
        LtcFlightInfo info = new LtcFlightInfo();
        LtcFlightResponseModel model = info.getFlightInfo(flightNumber, travelDate, acceptLanguage,'',null);
        System.assert(model != null);  
        System.debug('noMonitor model=' + model);
    }


    static testMethod void flightNotFound() {
        String flightNumber = 'KL9999';
        String travelDate = setup(flightNumber);
        String acceptLanguage = 'nl-NL';
        
        LtcFlightInfo info = new LtcFlightInfo();
        LtcFlightResponseModel model = info.getFlightInfo(flightNumber, travelDate, acceptLanguage,'', null);
        System.assert(model != null);  
        System.debug('flightNotFound model=' + model);
    }
    
    static testMethod void happyPath() {
        String flightNumber = 'KL4321';
        String travelDate = setup(flightNumber);
        String acceptLanguage = 'nl-NL';
        
        LtcFlightInfo info = new LtcFlightInfo();
        LtcFlightResponseModel model = info.getFlightInfo(flightNumber, travelDate, acceptLanguage,'', null);
        System.assert(model != null);  
        System.debug('happyPath model=' + model);
    }
    
    private static String setup(String flightNumber) {
        Test.setMock(HttpCalloutMock.class, new LtcLocationsCalloutMock());

        Monitor_Flight__c mf = new Monitor_Flight__c();
        mf.Flight_Number__c = flightNumber;
        mf.Languages__c = '["en", "nl", "fr", "es", "it", "ko"]';
        insert mf;
        
        mf = new Monitor_Flight__c();
        mf.Flight_Number__c = 'KL9999';
        insert mf;
        
        LtcAirport__c airportOrigin = new LtcAirport__c();
        airportOrigin.Name = 'AMS';
        airportOrigin.Airport_Name__c = 'airport_AMS_airport_name';
        airportOrigin.City__c = 'airport_AMS_airport_name';
        airportOrigin.UTC_Offset__c = 120;
        insert airportOrigin;

        LtcAirport__c airportDestination = new LtcAirport__c();
        airportDestination.Name = 'CDG';
        airportDestination.Airport_Name__c = 'airport_CDG_airport_name';
        airportDestination.City__c = 'airport_CDG_airport_name';
        airportDestination.UTC_Offset__c = 60;
        insert airportDestination;
        
        Translation__c t = new Translation__c();
        t.translation_key__c = 'airport_AMS_airport_name';
        t.translated_text__c = 'Schiphol';
        t.language__c = 'nl';
        insert t;
        
        Translation__c t2 = new Translation__c();
        t2.translation_key__c = 'airport_CDG_airport_name';
        t2.translated_text__c = 'Charles de Gaule';
        t2.language__c = 'nl';
        insert t2;
        
        LtcAircraftType__c acType = new LtcAircraftType__c();
        acType.name = 'FOKKER-100';
        acType.amount_owned_by_KLM__c = 3; 
        acType.businessclass_CA_s__c = '2';
        acType.cruising_speed__c = 800;
        acType.cruising_speed_unit__c ='km/u';
        acType.Economy_CA_s__c = '1';
        acType.Image_URL__c = 'plane.jpg';
        acType.length__c = 100;
        acType.length_unit__c = 'm';
        acType.max_distance__c = 20000;
        acType.max_distance_unit__c = 'km';
        acType.max_weight__c = 10000;
        acType.max_weight_unit__c = 'kg';
        acType.nr_of_cycle_hours_per_week__c = 150;
        acType.nr_of_flights_per_week__c = 50;
        acType.nr_of_seats__c = 230;
        acType.Pursers__c = '2';
        acType.Senior_Pursers__c = '0';
        acType.width__c = 80;
        acType.width_unit__c = 'm';
        insert acType;
        
        LtcAircraft__c ac = new LtcAircraft__c();
        ac.name = 'PH-AOA';
        ac.aircraft_Name__c = 'Dam - Amsterdam';
        ac.aircraft_type__c = acType.id;
        ac.Livery__c = 'Sky';
        ac.Wi_Fi__c = True;
        insert ac;
        
        String travelDate = String.valueOf(DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt());
        String language = 'nl-NL';
        
        Flight__c tmpFlight = new Flight__c();
        tmpFlight.Flight_Number__c = flightNumber;
        tmpFlight.registrationCode__c = 'PH-AOA';
        tmpFlight.currentleg__c = 0;
        tmpFlight.scheduled_departure_date__c =  DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        insert tmpFlight;
        System.debug('the id of the just inserted flight=' + tmpFlight.id);
        System.debug('sched dep date of flight=' + tmpFlight.scheduled_departure_date__c);
        
        Leg__c leg = new Leg__c();
        leg.flight__c = tmpFlight.id;
        leg.legnumber__c = 0;
        leg.origin__c = 'AMS';
        leg.status__c = 'ON_SCHEDULE';          
        leg.destination__c = 'CDG';

        String sta = String.ValueOf(DateTime.now().timeGmt().addMinutes(45)).substring(0,5);    //add 45 minutes for inactive
        String std = String.ValueOf(DateTime.now().timeGmt().addMinutes(5)).substring(0,5); //add 5 minutes for inactive
        
        leg.scheduledDepartureDate__c = DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        leg.scheduleddeparturetime__c = std;

        leg.scheduledArrivalDate__c = DateTime.now().addMinutes(Integer.valueOf(airportOrigin.UTC_Offset__c)).dateGmt();
        leg.scheduledarrivaltime__c = sta;
        
        leg.estimatedDepartureDate__c = DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        leg.estimateddeparturetime__c = std;

        leg.estimatedArrivalDate__c = DateTime.now().addMinutes(Integer.valueOf(airportOrigin.UTC_Offset__c)).dateGmt();
        leg.estimatedarrivaltime__c = sta;
        
        leg.actualDepartureDate__c = DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        leg.actualdeparturetime__c = std;

        leg.actualArrivalDate__c = DateTime.now().addMinutes(Integer.valueOf(airportOrigin.UTC_Offset__c)).dateGmt();
        leg.actualArrivalTime__c = sta;

        insert leg;
        
        CabinClass__c cc = new CabinClass__c();
        cc.name = 'Business Class';
        cc.label__c = 'my label';
        insert cc;
        
        ServicePeriod__c sp = new ServicePeriod__c();
        sp.name__c = 'winter';
        insert sp;
        
        LegClassPeriod__c lcp = new LegClassPeriod__c();
        lcp.startdate__c = Date.today().addDays(-3000);
        lcp.enddate__c = Date.today().addDays(30000);
        lcp.flightnumber__c = flightNumber;
        lcp.legnumber__c = 0;
        lcp.cabinclass__c = cc.id;
        lcp.serviceperiod__c = sp.id;
        insert lcp;
        
        ServiceItem__c si = new ServiceItem__c();
        si.description__c = 'si description';
        si.name__c = 'si name';
        insert si;
        
        ServiceItem__c siklc = new ServiceItem__c();
        siklc.description__c = 'siklc description';
        siklc.name__c = 'si_klc_name';
        insert siklc;
        
        LegClassItem__c lci = new LegClassItem__c();
        lci.legclassperiod__c = lcp.id;
        lci.serviceitem__c = si.id;
        lci.order__c = 1;
        insert lci;
        
        lci = new LegClassItem__c();
        lci.legclassperiod__c = lcp.id;
        lci.serviceitem__c = siklc.id;
        lci.order__c = 2;
        insert lci;
        
        Test.startTest();
        
        return travelDate;
    }
}