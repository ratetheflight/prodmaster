/**                     
 * @author (s)      : David van 't Hooft,Ata
 * @description     : Rest API interface for retrieving the (mainly FOX) Flight information
 * @log:    6JUN2014: version 1.0
 * @log:    2DEC2016: version 1.1
 *          DESC    : Bug with flight status shown in the FG(Deeplink)
 */
@RestResource(urlMapping='/Flights/*')
global class LtcFlightRestInterface {
    /**
     * Retrieve stored flight for a specific flight number and date
     * Expected Url params: ?flightNumber=ZZ77771&travelDate=2014-05-05
     **/
    @HttpGet
    global static String getFlight() {
        String response = null;
       	RestResponse res = RestContext.response;
       	System.debug(LoggingLevel.INFO, 'res=' + res);
        try {
            String flightNumber = RestContext.request.params.get('flightNumber');
            String travelDate = RestContext.request.params.get('travelDate');
            String acceptLanguage = RestContext.request.headers.get('Accept-Language');
            //Added by Ata
			String legOrigin = RestContext.request.params.get('origin');
			//end
			System.debug('Request Param:::'+RestContext.request.params);
			System.debug('legOrigin in call service:'+legOrigin);
			LtcFlightInfo ltcFlightInfo = new LtcFlightInfo();
            LtcFlightResponseModel responseModel = ltcFlightInfo.getFlightInfo(flightNumber, travelDate, acceptLanguage, legOrigin, null);
             
            if (responseModel.isEmpty()) {
            	res.statusCode = 404;
            	response = responseModel.errorResponse;
            } else {
            	response = responseModel.toString();
            }
        } catch (Exception ex) {
        	if (res != null)  {
	            res.statusCode = 500;
        	}
            if (LtcUtil.isDevOrg() || Test.isRunningTest()) {
            	response = ex.getMessage() + ' ' + ex.getStackTraceString();
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return response;
    }   
}