/**
 * @author (s)      : David van 't Hooft
 * @description     : class to test all related classes for the FlightRestInterface
 * @log:   10JUN2014: version 1.0
 */
@isTest
private class LtcFlightRestInterfaceTest {

    /**
     * Test the Get of the interface
     **/
    static testMethod void flightRestInterfaceGetOkTest() {
        
        // set up the request object
        String flightNumber = 'KL1142';
        String travelDate = '2012-12-31';
        LtcRating ltcRating = new LtcRating();
        String id = ltcRating.setRating('2', 'Excellent feedback1', 'Negative feedback1', 'True', 'Kees', 'Baksteen1', 'pietje@puk.nl', flightNumber, '1A', 'C', travelDate, 'ORG', 'DST', 'nl_NL',null,'','','');

        LtcAirport__c airportOrigin = new LtcAirport__c();
        airportOrigin.Name = 'AMS';
        airportOrigin.Airport_Name__c = 'Schiphol';
        airportOrigin.City__c = 'Amsterdam';
        airportOrigin.UTC_Offset__c = 120;
        insert airportOrigin;
         
        //update flight with fox data for the test 
        Flight__c flight = new Flight__c();
        Date trDate = Date.valueof(travelDate);
        List<Flight__c> flightList = [
            Select f.Flight_Number__c
            From Flight__c f 
        ];
        //Result should only be one or none!
        if (flightList != null && !flightList.isEmpty()) {
            flight = flightList[0];
        }
        update flight;
        
        test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/Ratings';
        req.addParameter('flightNumber', flightNumber);
        req.addParameter('travelDate', travelDate);
        req.addHeader('Accept-Language', 'nl-NL');
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
        
        // Invoke the method directly
        String response = LtcFlightRestInterface.getFlight();
        test.stopTest();
        String expectedResponse = 'noMonitorFlight';

        System.debug('**>'+response);
        System.assertEquals(expectedResponse, response);
    }
    
    /**
     * Test the Get of the interface
     **/
    static testMethod void flightInfoDateStatusCheckTest() {
        LtcFlightInfo ltcFlightInfo = new LtcFlightInfo(); 
        String flightNumber = 'KL4321';
                
        System.debug('setup the monitor flight');
        Monitor_Flight__c mf = new Monitor_Flight__c();
        mf.Flight_Number__c = flightNumber;
        mf.Languages__c = '["en", "nl", "fr", "es", "it", "ko"]';
        insert mf;
        
        System.debug('setup orgin');
        LtcAirport__c airportOrigin = new LtcAirport__c();
        airportOrigin.Name = 'AMS';
        airportOrigin.Airport_Name__c = 'airport_AMS_airport_name';
        airportOrigin.City__c = 'airport_AMS_airport_name';
        airportOrigin.UTC_Offset__c = 120;
        insert airportOrigin;

        System.debug('setup destination');
        LtcAirport__c airportDestination = new LtcAirport__c();
        airportDestination.Name = 'CDG';
        airportDestination.Airport_Name__c = 'airport_CDG_airport_name';
        airportDestination.City__c = 'airport_CDG_airport_name';
        airportDestination.UTC_Offset__c = 60;
        insert airportDestination;
        
        Translation__c t = new Translation__c();
        t.translation_key__c = 'airport_AMS_airport_name';
        t.translated_text__c = 'Schiphol';
        t.language__c = 'nl';
        insert t;
        
        Translation__c t2 = new Translation__c();
        t2.translation_key__c = 'airport_CDG_airport_name';
        t2.translated_text__c = 'Charles de Gaule';
        t2.language__c = 'nl';
        insert t2;
        
        System.debug('setup ac type');
        LtcAircraftType__c acType = new LtcAircraftType__c();
        acType.name = 'FOKKER-100';
        acType.amount_owned_by_KLM__c = 3; 
        acType.businessclass_CA_s__c = '2';
        acType.cruising_speed__c = 800;
        acType.cruising_speed_unit__c ='km/u';
        acType.Economy_CA_s__c = '1';
        acType.Image_URL__c = 'plane.jpg';
        acType.length__c = 100;
        acType.length_unit__c = 'm';
        acType.max_distance__c = 20000;
        acType.max_distance_unit__c = 'km';
        acType.max_weight__c = 10000;
        acType.max_weight_unit__c = 'kg';
        acType.nr_of_cycle_hours_per_week__c = 150;
        acType.nr_of_flights_per_week__c = 50;
        acType.nr_of_seats__c = 230;
        acType.Pursers__c = '2';
        acType.Senior_Pursers__c = '0';
        acType.width__c = 80;
        acType.width_unit__c = 'm';
        insert acType;
        System.debug('the id of the just inserted aircraft type=' + acType.id);
        
        LtcAircraft__c ac = new LtcAircraft__c();
        ac.name = 'PH-AOA';
        ac.aircraft_Name__c = 'Dam - Amsterdam';
        ac.aircraft_type__c = acType.id;
        ac.Livery__c = 'Sky';
        ac.Wi_Fi__c = True;
        insert ac;
        
        String travelDate = String.valueOf(DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt());
        String language = 'nl-NL';
        
        Flight__c tmpFlight = new Flight__c();
        tmpFlight.Flight_Number__c = flightNumber;
        tmpFlight.registrationCode__c = 'PH-AOA';
        tmpFlight.currentleg__c = 0;
        tmpFlight.scheduled_departure_date__c =  DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        insert tmpFlight;
        System.debug('the id of the just inserted flight=' + tmpFlight.id);
        System.debug('sched dep date of flight=' + tmpFlight.scheduled_departure_date__c);
        
        Leg__c leg = new Leg__c();
        leg.flight__c = tmpFlight.id;
        leg.legnumber__c = 0;
        leg.origin__c = 'AMS';
        leg.status__c = 'ON_SCHEDULE';          
        leg.destination__c = 'CDG';

        String sta = String.ValueOf(DateTime.now().timeGmt().addMinutes(45)).substring(0,5);    //add 45 minutes for inactive
        String std = String.ValueOf(DateTime.now().timeGmt().addMinutes(5)).substring(0,5); //add 5 minutes for inactive
        
        leg.scheduledDepartureDate__c = DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        leg.scheduleddeparturetime__c = std;

        leg.scheduledArrivalDate__c = DateTime.now().addMinutes(Integer.valueOf(airportOrigin.UTC_Offset__c)).dateGmt();
        leg.scheduledarrivaltime__c = sta;
        
        leg.estimatedDepartureDate__c = DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        leg.estimateddeparturetime__c = std;

        leg.estimatedArrivalDate__c = DateTime.now().addMinutes(Integer.valueOf(airportOrigin.UTC_Offset__c)).dateGmt();
        leg.estimatedarrivaltime__c = sta;
        
        leg.actualDepartureDate__c = DateTime.now().addMinutes(Integer.valueOf(airportDestination.UTC_Offset__c)).dateGmt();
        leg.actualdeparturetime__c = std;

        leg.actualArrivalDate__c = DateTime.now().addMinutes(Integer.valueOf(airportOrigin.UTC_Offset__c)).dateGmt();
        leg.actualArrivalTime__c = sta;

        insert leg;
        
        CabinClass__c cc = new CabinClass__c();
        cc.name = 'Business Class';
        cc.label__c = 'my label';
        insert cc;
        
        ServicePeriod__c sp = new ServicePeriod__c();
        sp.name__c = 'winter';
        insert sp;
        
        LegClassPeriod__c lcp = new LegClassPeriod__c();
        lcp.startdate__c = Date.today().addDays(-3000);
        lcp.enddate__c = Date.today().addDays(30000);
        lcp.flightnumber__c = flightNumber;
        lcp.legnumber__c = 0;
        lcp.cabinclass__c = cc.id;
        lcp.serviceperiod__c = sp.id;
        insert lcp;
        
        ServiceItem__c si = new ServiceItem__c();
        si.description__c = 'si description';
        si.name__c = 'si name';
        insert si;
        
        LegClassItem__c lci = new LegClassItem__c();
        lci.legclassperiod__c = lcp.id;
        lci.serviceitem__c = si.id;
        lci.order__c = 1;
        insert lci;
        
        String response = ltcFlightInfo.getFlightInfo(flightNumber, travelDate, language,'', null).toString();
        System.debug('Flight info response ' + response);


        System.assert(response.contains('{"flight": {"flightNumber": "KL4321", "travelDate": "' + travelDate + '", "travelDateToLocale": '));
        //System.assert(response.contains('"currentLeg": 0, "aircraft": {"registrationCode": "PH-AOA", "name": "Dam - Amsterdam", "livery": "Sky", "WiFi": true, "aircraftType": {"name": "FOKKER-100", "amountOwnedByKLM": 3, "imageUrl": "plane.jpg", "length": 100.00, "lengthUnit": "m", "width": 80.00, "widthUnit": "m", "maxWeight": 10000, "maxWeightUnit": "kg", "maxDistance": 20000, "maxDistanceUnit": "km", "cruisingSpeed": 800, "cruisingSpeedUnit": "km/u", "nrOfFlightsPerWeek": 50, "nrOfSeats": 230, "funFacts": null}}, "ratingStatus": "Inactive", "crew": {"crewAmounts": {"seniorPursers": "0", "pursers": "2", "businessCAs": "2", "economyCAs": "1"}, "language": ["en", "nl", "fr", "es", "it", "ko"]}, "legs": [{"status": "ON_SCHEDULE","origin": {"IATACode": "AMS", "airportName": "Schiphol", "country": "", "locationName": "Schiphol", "cityPageLink": "", "cityCode": "null", "weather": null},"destination": {"IATACode": "CDG", "airportName": "Charles de Gaule", "country": "", "locationName": "Charles de Gaule", "cityPageLink": "", "cityCode": "null", "weather": null},"legNumber": 0,'));
        System.assert(response.contains('"classes" : [{"name": "klmmj.my label", "description": "klmmj.null","serviceItems": [{"name": "klmmj.si name", "description": "klmmj.si description", "order": 1, "type": "null"}]}]}]}, "miscellaneousFeatures" : {"mobileAppPromotionOn" : false}}'));
        System.assert(response.contains('"scheduledDepartureDate": "' + travelDate + '", "scheduledDepartureTime": "'+ std + '"'));
        System.assert(response.contains('"scheduledArrivalDate": "' + travelDate + '", "scheduledArrivalTime": "'+ sta + '"'));
        System.assert(response.contains('"estimatedDepartureDate": "' + travelDate + '","estimatedDepartureTime": "'+ std + '"'));
        System.assert(response.contains('"estimatedArrivalDate": "' + travelDate + '", "estimatedArrivalTime": "'+ sta + '"'));
        System.assert(response.contains('"actualDepartureDate": "' + travelDate + '", "actualDepartureTime": "'+ std + '"'));
        System.assert(response.contains('"actualArrivalDate": "' + travelDate + '", "actualArrivalTime": "'+ sta + '"'));

    }
    
    /** Test the Get of the interface */
    static testMethod void flightNotFound() {
        LtcFlightInfo ltcFlightInfo = new LtcFlightInfo(); 
        String travelDate = String.valueOf(DateTime.now().addMinutes(60).dateGmt());
        String language = 'nl-NL';
        String flightNumber = 'KL4321';
                
        System.debug('setup the monitor flight');
        Monitor_Flight__c mf = new Monitor_Flight__c();
        mf.Flight_Number__c = flightNumber;
        mf.Languages__c = '["en", "nl", "fr", "es", "it", "ko"]';
        insert mf;
        
        String response = ltcFlightInfo.getFlightInfo(flightNumber, travelDate, language,'',null).toString();
        System.debug('Flight info response flightNotFound ' + response);
        System.assertEquals('{"flight": null, "miscellaneousFeatures" : {"mobileAppPromotionOn" : false}}', response);
        
    }
    
    /**
     * Test error response
     **/
    static testMethod void errorResponse() {
        LtcFlightInfo ltcFlightInfo = new LtcFlightInfo(); 
        String travelDate = String.valueOf(DateTime.now().addMinutes(60).dateGmt());
        String language = 'nl-NL';
        String flightNumber = null;
        
        String response = LtcFlightRestInterface.getFlight();
        System.debug(LoggingLevel.INFO, 'response=' + response);
        
        System.assert(response.startsWith('Attempt to de-reference a null object'));
    }    
}