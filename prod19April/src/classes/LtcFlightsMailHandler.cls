global class LtcFlightsMailHandler implements Messaging.InboundEmailHandler {

	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
		Account account = getAccount();
        if (email.binaryAttachments != null && email.binaryAttachments.get(0) != null) {
    		System.debug(LoggingLevel.DEBUG, 'binary att found in ' + email.subject + ' ' + email);
    		removeAttachmentsFromPisaAccount(account);
    		storeZipAttachment(email.binaryAttachments.get(0), account); 
    		schedulePisaPrepareBatch();
        } else if (email.textAttachments != null && email.textAttachments.get(0) != null) {
    		System.debug(LoggingLevel.DEBUG, 'text att found in ' + email.subject + ' ' + email);
            removeAttachmentsFromPisaAccount(account);
            storeTxtAttachment(email.textAttachments.get(0), account);
            schedulePisaPrepareBatch();
        } else {	
        	new LtcFoxBatchCheckerSchedulable().sendMail(email.subject + ' processing failed in pisabatch', 'no valid attachment found in email: ' + email.subject);
        }
        
        return result;
    }
    
    global void schedulePisaPrepareBatch() {
        System.debug(LoggingLevel.INFO, 'attachment stored now schedule LtcPisaPrepareBatch');
        insert LtcPisaBatchQueItemFactory.createQueueItemForPisaPrepareBatch();
    }
    
    global void storeZipAttachment(Messaging.InboundEmail.BinaryAttachment att, Account account) {
		Attachment attachment = new Attachment();
		attachment.ParentId = account.Id;
		attachment.Name = 'EE10.PRD.PWC.FLTGUIDE.SCHED.ZIP';
		attachment.Body = att.body;
		insert attachment;
	}
	
	global void storeTxtAttachment(Messaging.InboundEmail.TextAttachment att, Account account) {
    	Attachment attachment = new Attachment();
		attachment.ParentId = account.Id;
		attachment.Name = 'EE10.PRD.PWC.FLTGUIDE.SCHED.TXT';
		attachment.Body =  Blob.valueOf(att.body);
		insert attachment;
	}

	private Account getAccount() {
		Account account;
		List<Account> accounts = [
    		select Id, Name from Account where Name = 'pisabatch'
    	];
    	
    	if (accounts != null && accounts.size() > 0) {
			account = accounts.get(0);
    	} else {
	   		account = new Account();
			account.Name = 'pisabatch';
			insert account;
			System.debug('====> inserted account ' + account.Id + ' ' + account.Name);
    	}
    	return account;
	}

	global void removeAttachmentsFromPisaAccount(Account account) {
		List<Attachment> atts = [
			select Id, Body 
			from Attachment 
			where parentId = :account.Id 
			and name like 'EE10.PRD.PWC.FLTGUIDE.SCHED%'
		];
		delete atts;
	}
}