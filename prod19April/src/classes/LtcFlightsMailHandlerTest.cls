/**
 * This class contains unit tests for LtcFlightsMailHandler
 */
@isTest
private class LtcFlightsMailHandlerTest {

    static testMethod void handleBinaryAttachment() {
        LtcFlightsMailHandler fmh = new LtcFlightsMailHandler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        
        Messaging.InboundEmail.BinaryAttachment att = new Messaging.InboundEmail.BinaryAttachment();
        att.body = Blob.valueOf('test');
        email.binaryAttachments = new List<Messaging.InboundEmail.BinaryAttachment>();
        email.binaryAttachments.add(att);
        
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        fmh.handleInboundEmail(email, envelope);
    }
    
    static testMethod void handleTextAttachment() {
        LtcFlightsMailHandler fmh = new LtcFlightsMailHandler();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        
        Messaging.InboundEmail.TextAttachment att = new Messaging.InboundEmail.TextAttachment();
        att.body = 'test';
        email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
        email.textAttachments.add(att);
        
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        fmh.handleInboundEmail(email, envelope);
    }
}