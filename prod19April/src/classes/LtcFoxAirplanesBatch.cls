/**
 * @author (s)    :	David van 't Hooft
 * @description   : Apex Batch class for retrieving fox Airplanes Data
 *
 * @log: 	12May2014: version 1.0
 * Call anonymous: Id batjobId = Database.executeBatch(new LtcFoxAirplanesBatch(), 200);
 */
public class LtcFoxAirplanesBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	public LtcFoxAirplanesBatch(){	
	}

	/**
	 * Since we cannot iterate through a custom list in the execute, we need to perform all actions into the start
	 **/	
    public database.querylocator start(Database.BatchableContext BC) {
    	List<LtcAircraft__c> ltcAircraftsToStore = new List<LtcAircraft__c>();
    	List<LtcAircraftType__c> ltcAircraftTypessToStore = new List<LtcAircraftType__c>();
    	Map<String,LtcAircraft__c> ltcAircraftsMap = new Map<String,LtcAircraft__c>();
    	Map<String,LtcAircraftType__c> ltcAircraftTypesMap = new Map<String,LtcAircraftType__c>();
    	LtcAircraft__c LtcAircraft;
    	LtcAircraftType__c ltcAircraftType;
    	
    	//collect all existing Airports and place them in a map by iatacode 
    	List<LtcAircraft__c> LtcAircrafts = [select Id, Name from LtcAircraft__c limit 5000];
    	if (LtcAircrafts!=null && !LtcAircrafts.isEmpty()) {
    		for (LtcAircraft__c LtcAp : LtcAircrafts) {
    			ltcAircraftsMap.put(LtcAp.Name, LtcAp);
    		}
    	}

		//collect all existing Airport types and place them in a map by Aircrafttype name
    	List<LtcAircraftType__c> LtcAircraftTypes = [select Id, Name from LtcAircraftType__c limit 1000];
    	if (LtcAircraftTypes!=null && !LtcAircraftTypes.isEmpty()) {
    		for (LtcAircraftType__c LtcAct : LtcAircraftTypes) {
    			ltcAircraftTypesMap.put(LtcAct.Name, LtcAct);
    		}
    	}
    	
    	//Call Fox to retrieve all latest Airport information
    	LtcFoxService ltcFoxService = new LtcFoxService();
    	List<LtcFoxAirplanesResponseModel> ltcFoxARMList = ltcFoxService.getFoxAirplanesInfo();

		//Create all missing Aircrafttypes and complete the map
    	for (LtcFoxAirplanesResponseModel ltcFoxARM : ltcFoxARMList) {
			if (!ltcAircraftTypesMap.containsKey(ltcFoxARM.type)) {
				ltcAircraftType = new LtcAircraftType__c(Name = ltcFoxARM.type, Image_URL__c = 'http://fox.klm.com' + ltcFoxARM.imageUrl);
	    		ltcAircraftTypessToStore.add(ltcAircraftType);
    			ltcAircraftTypesMap.put(ltcAircraftType.Name, ltcAircraftType);
			}
    		if (ltcAircraftTypessToStore.size()>=200) {
    			insert ltcAircraftTypessToStore;
	    		for (LtcAircraftType__c LtcActtt : ltcAircraftTypessToStore) {
	    			ltcAircraftTypesMap.put(LtcActtt.Name, LtcActtt);
	    		}
    			ltcAircraftTypessToStore.clear();
    		}
    	}
		if (!ltcAircraftTypessToStore.isEmpty()) {
			insert ltcAircraftTypessToStore;
    		for (LtcAircraftType__c LtcActtt2 : ltcAircraftTypessToStore) {
    			ltcAircraftTypesMap.put(LtcActtt2.Name, LtcActtt2);
    		}
			ltcAircraftTypessToStore.clear();
		}
    	    	 
    	//Create all Aircrafts from fox    	 
    	for (LtcFoxAirplanesResponseModel ltcFoxARM : ltcFoxARMList) {
    		LtcAircraft = new LtcAircraft__c();
    		LtcAircraft.Name = ltcFoxARM.registrationCode;
    		LtcAircraft.Aircraft_Name__c = ltcFoxARM.name;
    		if (ltcAircraftTypesMap.containsKey(ltcFoxARM.type)) {
    			LtcAircraft.Aircraft_type__c = ltcAircraftTypesMap.get(ltcFoxARM.type).Id;
    		} 
    		
    		//If already exist retrieve the Id of the record
    		if (ltcAircraftsMap.containsKey(LtcAircraft.Name)) {
    			LtcAircraft.Id = ltcAircraftsMap.get(LtcAircraft.Name).Id;
    		}
    		
    		ltcAircraftsToStore.add(LtcAircraft);
    		
    		if (ltcAircraftsToStore.size()>=200) {
    			upsert ltcAircraftsToStore;
    			ltcAircraftsToStore.clear();
    		}
    	}
	 	if(!ltcAircraftsToStore.isEmpty()){
	 		upsert ltcAircraftsToStore;
	 	}
	 	//Fake query locator to keep SFDC happy  	 	
        return Database.getQueryLocator('select ID from Flight__c limit 0');       
	}

	public void execute(Database.BatchableContext BC, List<sObject> scope){
	}

	public void finish(Database.BatchableContext BC){
	}

}