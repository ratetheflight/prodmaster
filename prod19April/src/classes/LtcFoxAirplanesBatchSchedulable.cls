/**
 * @author (s)    :	David van 't Hooft
 * @description   : Apex Batch scheduler for the class LtcFoxAirplanesBatch
 *
 * @log: 	12Jun2014: version 1.0
 */
	
global class LtcFoxAirplanesBatchSchedulable implements Schedulable {
    global void execute(SchedulableContext scMain) {
        LtcFoxAirplanesBatch ltcFoxAirplanesBatch = new LtcFoxAirplanesBatch();
        ID idBatch1 = Database.executeBatch(ltcFoxAirplanesBatch, 200);
    }
}