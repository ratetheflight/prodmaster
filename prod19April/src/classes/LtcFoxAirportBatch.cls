/**
 * @author (s)    :	David van 't Hooft
 * @description   : Apex Batch class for retrieving fox Airport Data
 *
 * @log: 	12May2014: version 1.0
 * Call anonymous: Id batjobId = Database.executeBatch(new LtcFoxAirportBatch(), 200);
 */
public class LtcFoxAirportBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	private Map<String, List<LtcFoxAirportResponseModel>> foxResponseMap = new Map<String, List<LtcFoxAirportResponseModel>>();
	
	public LtcFoxAirportBatch() {
	}
	
    public database.querylocator start(Database.BatchableContext BC) {
    	List<LtcLocale__c> locales = [select Language__c from LtcLocale__c limit 1000];
    	for (LtcLocale__c locale : locales) {
	    	//Call Fox to retrieve all latest Airport information
	    	LtcFoxService ltcFoxService = new LtcFoxService();
	    	List<LtcFoxAirportResponseModel> ltcFoxARMList = ltcFoxService.getFoxAirportInfo(locale.Language__c);
	    	foxResponseMap.put(locale.Language__c, ltcFoxARMList);
    	}
    	system.debug('after loading before batch foxResponsemap' + foxResponsemap);
    	system.debug('after loading before batch foxResponsemap size' + foxResponsemap.size());
        return Database.getQueryLocator('select Language__c from LtcLocale__c');       
	}

	public void execute(Database.BatchableContext BC, List<LtcLocale__c> scope) {
		system.debug('Scope number:' + scope.size());
		for (LtcLocale__c locale : scope) {
			system.debug('Start language:' + locale.Language__c);
			List<LtcAirport__c> ltcAirportsToStore = new List<LtcAirport__c>();
	    	Map<String,LtcAirport__c> ltcAirportsMap = new Map<String,LtcAirport__c>();
			List<Translation__c> translationsToStore = new List<Translation__c>();
	    	Map<String,Translation__c> translationsMap = new Map<String,translation__c>();
	    	LtcAirport__c ltcAirport;
	    	Translation__c translation_airport_name;
	    	Translation__c translation_label;
	    	Translation__c translation_country;
	    	Translation__c translation_city;
	    	
	    	//collect all existing Airports and place them in a map by iatacode 
	    	List<LtcAirport__c> ltcAirports = [select Id, Name from LtcAirport__c limit 5000];
	    	if (ltcAirports!=null && !ltcAirports.isEmpty()) {
	    		for (LtcAirport__c LtcAp : ltcAirports) {
	    			ltcAirportsMap.put(LtcAp.Name, LtcAp);
	    		}
	    	}
	    	
	    	//collect all existing Translations and place them in a map by Translation_Key__c, Language__c 
	    	List<Translation__c> translations = [select Id, Translation_Key__c, Language__c from Translation__c];
	    	if (translations != null && !translations.isEmpty()) {
	    		for (Translation__c translation : translations) {
	    			String trKey = translation.Translation_Key__c + translation.Language__c;
	    			translationsMap.put(trKey, translation);
	    		}
	    	}
	    	system.debug('foxResponsemap' + foxResponsemap);
	    	List<LtcFoxAirportResponseModel> ltcFoxARMList = foxResponseMap.get(locale.Language__c);
	    	 
	    	for (LtcFoxAirportResponseModel ltcFoxARM : ltcFoxARMList) {
	    		ltcAirport = new LtcAirport__c();
	    		translation_airport_name = new Translation__c();
	    		translation_airport_name.Language__c = locale.Language__c;
	    		translation_label = new Translation__c();
	    		translation_label.Language__c = locale.Language__c;
	    		translation_country = new Translation__c();
	    		translation_country.Language__c = locale.Language__c;
	    		translation_city = new Translation__c();
	    		translation_city.Language__c = locale.Language__c;
	    		
	    		String trKey = 'airport_' + ltcFoxARM.iataCode + '_airport_name';
	    		translation_airport_name.Translated_Text__c = ltcFoxARM.name;
	    		translation_airport_name.Translation_Key__c = trKey;
	    		ltcAirport.Airport_Name__c = trKey;
	    		if (translationsMap.containsKey(trKey + locale.Language__c)) {
	    			translation_airport_name.Id = translationsMap.get(trKey + locale.Language__c).Id;
	    		}
	    		translationsToStore.add(translation_airport_name);
	    		
	    		trkey = 'airport_' + ltcFoxARM.iataCode + '_country';
	    		translation_country.Translated_Text__c = ltcFoxARM.country;
	    		translation_country.Translation_Key__c = trKey;
	    		ltcAirport.Country__c = trKey;
	    		if (translationsMap.containsKey(trKey + locale.Language__c)) {
	    			translation_country.Id = translationsMap.get(trKey + locale.Language__c).Id;
	    		}
	    		translationsToStore.add(translation_country);
	    		
	    		trKey = 'airport_' + ltcFoxARM.iataCode + '_label';
	    		translation_label.Translated_Text__c = ltcFoxARM.label;
	    		translation_label.Translation_Key__c = trKey;
	    		ltcAirport.Label__c = trKey;
	    		if (translationsMap.containsKey(trKey + locale.Language__c)) {
	    			translation_label.Id = translationsMap.get(trKey + locale.Language__c).Id;
	    		}
	    		translationsToStore.add(translation_label);
	    		
	    		trKey = 'airport_' + ltcFoxARM.iataCode + '_city';
	    		translation_city.Translated_Text__c = ltcFoxARM.city;
	    		translation_city.Translation_Key__c = trKey;
	    		ltcAirport.City__c = trKey;
	    		if (translationsMap.containsKey(trKey + locale.Language__c)) {
	    			translation_city.Id = translationsMap.get(trKey + locale.Language__c).Id;
	    		}
	    		translationsToStore.add(translation_city);
	    		
	    		ltcAirport.Latitude__c = ltcFoxARM.latitude;
	    		ltcAirport.Longitude__c = ltcFoxARM.longitude;
	    		ltcAirport.Time_Zone_Id__c = ltcFoxARM.timeZoneId;
	    		ltcAirport.Name = ltcFoxARM.iataCode;
	    		ltcAirport.UTC_Offset__c = ltcFoxARM.utcOffset;
	    		
	    		//If already exist retrieve the Id of the record
	    		if (ltcAirportsMap.containsKey(ltcAirport.Name)) {
	    			ltcAirport.Id = ltcAirportsMap.get(ltcAirport.Name).Id;
	    		}
	    		ltcAirportsToStore.add(ltcAirport);
	    		
	    		if (ltcAirportsToStore.size() >= 200) {
	    			upsert ltcAirportsToStore;
	    			ltcAirportsToStore.clear();
	    		}
	    		if (translationsToStore.size() >= 200) {
	    			upsert translationsToStore;
	    			translationsToStore.clear();
	    		}
	    	}
		 	if(!ltcAirportsToStore.isEmpty()){
		 		upsert ltcAirportsToStore;
		 	}
		 	if(!translationsToStore.isEmpty()){
    			upsert translationsToStore;
		 	}
		}
	}

	public void finish(Database.BatchableContext BC){
	}

}