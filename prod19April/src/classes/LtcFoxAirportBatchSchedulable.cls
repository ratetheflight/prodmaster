/**
 * @author (s)    :	David van 't Hooft
 * @description   : Apex Batch scheduler for the class LtcFoxAirportBatch
 *
 * @log: 	10Jun2014: version 1.0
 */
	
global class LtcFoxAirportBatchSchedulable implements Schedulable {
    global void execute(SchedulableContext scMain) {
        LtcFoxAirportBatch ltcFoxAirportBatch = new LtcFoxAirportBatch();
        ID idBatch1 = Database.executeBatch(ltcFoxAirportBatch, 200);
    }
}