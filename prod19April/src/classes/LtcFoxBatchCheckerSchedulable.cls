/**
 * @author (s)    : Mees Witteman/Ashok Royal
 * @Date          : 14/June/2016
 * @description   : Schedulable implementation that will check the results of the Pisa batch and check New flights from Flight table,
                    compare it on Monitor Flight also send notification email with new flight report to LTC FAM. 
                    Find new destinations from leg object and insert the new destinations to airport table as well as send notification 
                    respective users to verify it.  
 * @Assumumption  : the night batch runs before this check program
 */
public class LtcFoxBatchCheckerSchedulable implements Schedulable { 
    
    public LtcFoxBatchCheckerSchedulable() {}
    
    public void execute(SchedulableContext scMain) {
        execute();
        checkNewDestinations();
        checkNewFlights();
    }
          
    public Integer execute() {
        Integer result = 0;
        result += checkFlightsDayPlus(7);
        result += checkActualsForDay(-6);
        result += checkActualsForDay(-7);
        return result;
    }
    
    public Set<String> flightNumbers = new Set<String>();
    public Set<String> airportCodes= new Set<String>();
    
    public void checkNewDestinations() {
        // Get IATA Code from Airport object 
        for (LtcAirport__c airports : [
            select Name
            from LtcAirport__c]) {
            airportCodes.add(airports.Name); 
        }                 
        // Get the destination from leg Table and compare it to airport Table.
        Set<String> newDestinations = new Set<String>();
        List<LtcAirport__c> newAirports = New List<LtcAirport__c>();
        List <Leg__c> legDestination= [
            select destination__c
            from Leg__c 
            where destination__c not in : airportCodes
            and createddate >= LAST_WEEK
         ];          
                          
        List <Leg__c> legOrigin = [
            select origin__c
            from Leg__c 
            where origin__c not in : airportCodes
            and createddate >= LAST_WEEK
         ]; 
                                         
        for (Leg__c legs : legDestination) {
              newDestinations.add(legs.destination__c);                              
        }
                    
        for (Leg__c legs : legOrigin) {
            newDestinations.add(legs.origin__c);              
        }
            
        System.debug('new Destinations size' + newDestinations.size());
        System.debug('new Destinations list' + newDestinations);   
          
        String mailText = 'New destinations found:';
        for (String destination : newDestinations) {
             mailText += '\n' + destination;                
        }
                                    
        //Insert New Flight Records to Monitor Table from New Flight Report.
        for (String airportCode :newDestinations) {
             LtcAirport__c airport = new LtcAirport__c();
             airport.Name = airportCode;
             airport.Airport_Name__c = 'airport_'+airportCode+'_airport_name';
             airport.Country__c = 'airport_'+airportCode+'_country';
             airport.Label__c = 'airport_'+airportCode+'_label';
             airport.City__c = 'airport_'+airportCode+'_city';
             airport.airport_code__c = airportCode;
             airport.City_Code__c = airportCode;
             newAirports.add(airport);
        }            
         System.debug('New Airports' + newAirports);
         insert newAirports; 
         
         //Extract Loacale records
         Set<String> languages = new Set<String>();
         for(LtcLocale__c locale : [select Language__c from LtcLocale__c]) {
         languages.add(locale.Language__c);
         } 
         
        //Insert new translation records from new destinations.
        List<Translation__c> translations = new List<Translation__c>();
        for (String airportCode : newDestinations) {
           for(String locale : languages ) {          
             //Adding airport name translation key
             Translation__c airportNametranslation = new Translation__c();
             airportNametranslation.Translation_Key__c = 'airport_'+airportCode+'_airport_name';
             airportNametranslation.Translated_Text__c = 'airport_'+airportCode+'_airport_name';           
             airportNametranslation.Language__c = locale;             
             translations.add(airportNametranslation);
             
             //Adding airport country translation key
             Translation__c countryTranslation = new Translation__c();
             countryTranslation.Translation_Key__c = 'airport_'+airportCode+'_country';
             countryTranslation.Translated_Text__c = 'airport_'+airportCode+'_country';             
             countryTranslation.Language__c = locale;             
             translations.add(countryTranslation);
             
             //Adding airport label translation key
             Translation__c labelTranslation = new Translation__c();
             labelTranslation.Translation_Key__c = 'airport_'+airportCode+'_label';
             labelTranslation.Translated_Text__c = 'airport_'+airportCode+'_label';             
             labelTranslation.Language__c = locale;             
             translations.add(labelTranslation);
             
             //Adding airport city translation key
             Translation__c cityTranslation = new Translation__c();
             cityTranslation.Translation_Key__c = 'airport_'+airportCode+'_city';
             cityTranslation.Translated_Text__c = 'airport_'+airportCode+'_city';             
             cityTranslation.Language__c = locale;             
             translations.add(cityTranslation);
           }              
        }           
        System.debug('translation list' + translations);
        insert translations;
                     
         // To send Email to Respective users if any new destination found on Legs Table.
        /*Commented JIRA task 3229
        if (newDestinations.size() > 0) {               
                sendMail('new destination report', 'There are ' + newDestinations.size() + ' new destinations found today' + ' on airport table   \n\n' + mailText);
        }  else {
                sendMail('new destination report', 'There are no new destination found today ');
           }*/                                  
  }
    
    public void checkNewFlights() {
        // Get the Flight Number from Monitor Flight Table. 
        for (Monitor_Flight__c mtrFlights : [
            select Flight_Number__c
            from Monitor_Flight__c]) {
            flightNumbers.add(mtrFlights.Flight_Number__c); 
        } 
        
        System.debug('monitored flightNumbers=' + flightNumbers.size());
        
        // Get the Flight Number from Flight Table and compare it to the Monitor Flight Table.
        Set<String> newFlightSet = new Set<String>();
        List<Monitor_Flight__c> mtrNewFlight = New List<Monitor_Flight__c>();
        Integer newFlightNrs = 0;
        String actualsNewFLightnrs = '';
        List <Flight__c> flights= [
            select Flight_Number__c
            from Flight__c 
            where Flight_Number__c not in : flightNumbers
            and CreatedDate >= LAST_WEEK
         ]; {
            
            System.debug(LoggingLevel.DEBUG, 'found flights=' + flights.size());
            for (Flight__c f : flights) {
                newFlightSet.add(f.Flight_Number__c);              
            }
            
            System.debug(LoggingLevel.DEBUG, 'newFlightSet.size=' + newFlightSet.size());
            
            String mailText = 'New flightnrs found:';
            for (String flightnr : newFlightSet) {
                mailText += '\n' + flightnr;                
            }
            
            //Insert New Flight Records to Monitor Flight Table from New Flight Report.
            for (String newFlightStr :newFlightSet) {
                Monitor_Flight__c objMtrFlight = new Monitor_Flight__c();
                objMtrFlight.Flight_Number__c = newFlightStr;
                objMtrFlight.Languages__c = '["en", "nl", "fr", "es", "de"]';
                mtrNewFlight.add(objMtrFlight);
            }
            
            System.debug('MTRFlight' + mtrNewFlight );
            insert mtrNewFlight ;
             
            // To send Email to Respective users if any new flights found on Flight Table.
            /*Commented JIRA task 3229
            if (newFlightSet.size() > 0) {               
                sendMail('new flights report', 'There are ' + newFlightSet.size() + ' new flights found today' + ' with missing flight numbers on monitor flight   \n\n' + mailText);
            } else {
                sendMail('new flights report', 'There are no new flights found today ');
            }*/
        }                                    
    }
    
    public Integer checkFlightsDayPlus(Integer x) {
        Date trDate = Date.toDay().addDays(x);
        List<Flight__c> flights = [
            select id, Scheduled_Departure_date__c 
            from Flight__c where Scheduled_Departure_date__c =: trDate
        ];
        
        if (flights.size() < 20) {
            sendMail(null, 'There are only ' + flights.size() + ' flights of day + ' + x + ' (' + trDate + ') ... has the PisaBatch been failing ?');
        }
        return 1;
    }
    
    public Integer checkActualsForDay(Integer dayFromNow) {
        Date trDate = Date.toDay().addDays(dayFromNow);
        List<Flight__c> flights = [
            select 
                f.Scheduled_Departure_Date__c,
                f.flight_number__c,
                (select 
                    Leg__c.actualArrivalDate__c
                    from f.Legs__r
                )
            from Flight__c f
            where  f.Scheduled_Departure_Date__c =: trDate
        ];
        
        Integer missingActuals = 0;
        System.debug('Missing Arrival Date' + flights);
        String missingActualsFLightnrs = '';
        for (Flight__c flight : flights) {
            System.debug('flight=' + flight );            
            System.debug('flight.legs__r.size()=' + (flight.Legs__r != null ? '' + flight.Legs__r.size() : 'legs__r == null '));           
            if (flight.Legs__r != null && flight.Legs__r.size() > 0 && flight.Legs__r.get(0).actualArrivalDate__c == null) {
                missingActuals = missingActuals + 1;
                missingActualsFLightnrs += flight.flight_number__c + '\n';
            }      
        }    
        
        /*Commented JIRA task 3229
        if (missingActuals > 0) {
            sendMail(null, 'There are ' + missingActuals + ' flights on day ' + trDate + ' with missing actual arrival date / time from fox \n\n' + missingActualsFLightnrs);
        } */
        return 2;
    }
        
    public void sendMail(String subject, String text) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSenderDisplayName('KL Fox Data Protector Service');
            mail.setUseSignature(false);
            mail.setBccSender(false);
            mail.setPlainTextBody(text);
            mail.setToAddresses(getMailAddresses());

            if (subject == null) {
                mail.setSubject('missing flight data: ' + (text.length() > 89 ? text.substring(0, 90) : text));
            } else {
                mail.setSubject(subject);
            }
    
            try {
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, 'send mail failed msg=' + e.getMessage());
            }
    }
    
    public List<String> getMailAddresses() {
        List<String> mailList = new List<String>();
        List<String> mailAddresses = new List<String>();
         
        Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'LTC_FAM'];
        for (GroupMember gm : g.groupMembers) {
            mailList.add(gm.userOrGroupId);
        }
        
        User[] usr = [SELECT email FROM user WHERE id IN :mailList];
        for (User u : usr) {
           mailAddresses.add(u.email);                                                               
        }
        return mailAddresses;
    }
}