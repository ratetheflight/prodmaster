/**
 * Tests for the LtcFoxBatchCheckerSchedulable class.
 */
@isTest
private class LtcFoxBatchCheckerSchedulableTest {

    static testMethod void execute() {
        LtcFoxBatchCheckerSchedulable sched = new LtcFoxBatchCheckerSchedulable();
        Integer result = sched.execute();
        System.assertEquals(5, result);
        
        Flight__c objflight = new Flight__c();
        objflight.Flight_Number__c = 'KL2000';
        objflight.currentLeg__c =1;
        objflight.registrationCode__c ='PH-AOA';
        objflight.aircraftTypeCode__c ='332';
        insert objflight;
         
        system.debug('showid' + objflight.id );       
        sched.checkNewFlights();        
        
        Monitor_Flight__c mtrFlights = [ select Flight_Number__c,Languages__c from Monitor_Flight__c where Flight_Number__c =:'KL2000'];
        System.assertEquals('KL2000', mtrFlights.Flight_Number__c);
        System.assertEquals('["en", "nl", "fr", "es", "de"]', mtrFlights.languages__c);
        
        //Insert Flight record
        Flight__c flight = new Flight__c();
        flight.Flight_Number__c = 'KL1563';
        flight.currentLeg__c =1;
        flight.registrationCode__c ='PH-BGM';
        flight.aircraftTypeCode__c ='73W';
        insert flight;
        
        //Insert leg record
        Leg__c leg = new Leg__c();
        leg.destination__c ='GOA';
        leg.Flight__c = flight.id; 
        insert leg;
        
        //insert locale records 'en' and 'nl'
        LtcLocale__c locale_en = new LtcLocale__c();
        locale_en.Language__c = 'en';
        insert locale_en;
        
        LtcLocale__c locale_nl = new LtcLocale__c();
        locale_nl.Language__c = 'nl';
        insert locale_nl;
        
        sched.checkNewDestinations(); 
        
        LtcAirport__c airport = [ select Name from LtcAirport__c where Name =:'GOA'];
        System.assertEquals('GOA', airport.Name);
        
        Translation__c nameTranslation = [select Translation_Key__c,Language__c from Translation__c where Language__c = 'en' AND  Translation_Key__c =: 'airport_GOA_airport_name'];
        System.assertEquals('airport_GOA_airport_name', nametranslation.Translation_Key__c);
        System.assertEquals('en', nametranslation.Language__c);                
    }
}