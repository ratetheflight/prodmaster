/**
 * @author (s)      : Ata
 * @description     : Test class to test the LTC_IFE_Service 
 *        
 * @log             : 13JAN2017
 */
@isTest
private class LtcIFEServiceTest { 
    
    public static void createData(){
        Account acc = new Account();
        acc.name = 'IFE Attachment';
        insert acc;
        
        Attachment att = new Attachment();
        att.Body = Blob.valueOf('hey,this,is,an attachment\n1,b,c,d');
        att.Name = 'ABC.csv';
        att.parentId = acc.id;
        insert att;
    }
    public static void createData2(){
        Account acc = new Account();
        acc.name = 'IFE Attachment';
        insert acc;
        
        Attachment att = new Attachment();
        att.Body = Blob.valueOf('hey,this,is,an attachment\\n  1,b,c,d');
        att.Name = 'ABC.csv';
        att.parentId = acc.id;
        insert att;
    }
    static testMethod void getIFEDataPos() {
         createData();
         RestRequest req = new RestRequest();
         RestResponse res = new RestResponse();
         req.requestURI = '/services/apexrest/IFE_Service';        
         req.httpMethod = 'GET';
         req.addHeader('Accept-Language', 'nl-nl');
         RestContext.request = req;
         RestContext.response = res;
         LTC_IFE_Service.getIFEData();    
         //System.assert(RestContext.response.statusCode == 200);
    }
    static testMethod void getIFEDataNeg() {
         createData2();
         RestRequest req = new RestRequest();
         RestResponse res = new RestResponse();
         req.requestURI = '/services/apexrest/IFE_Service';        
         req.httpMethod = 'GET';
         req.addHeader('Accept-Language', 'nl-nl');
         RestContext.request = req;
         RestContext.response = res;
         LTC_IFE_Service.getIFEData();    
         //System.assert(RestContext.response.statusCode == 500);
    }
    
}