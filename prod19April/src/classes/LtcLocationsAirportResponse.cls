public with sharing class LtcLocationsAirportResponse{
	public String code;	//BOS
	public String name;	//Logan International
	public String description;	//Boston - Logan International (BOS), USA
	public cls_coordinates coordinates;
	public cls_parent parent;
	public class cls_coordinates {
		public Double latitude;	//46.316
		public Double longitude;	//-105.46
	}
	public class cls_parent {
		public cls_parent parent;
		public String code;	//N_AMER
		public String name;	//North America
		public String description;	//North America (N_AMER)
		public cls_coordinates coordinates;
	}
	public static LtcLocationsAirportResponse parse(String json){
		return (LtcLocationsAirportResponse) System.JSON.deserialize(json, LtcLocationsAirportResponse.class);
	}

}