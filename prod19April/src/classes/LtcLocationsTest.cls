/**
 * Tests for the Locations API client classes
 */
@isTest
private class LtcLocationsTest {
    
    static testMethod void getWeatherForCity() {
        Test.setMock(HttpCalloutMock.class, new LtcLocationsCalloutMock());
        
        Test.startTest();
        LtcLocationsWeatherResponse response = LtcLocationsApiService.getWeatherForCity('AMS');
        Test.stopTest();
        
        System.assertEquals('6', response.actual.temp); 
    }
	
    static testMethod void reprocessAccessToken() {
        LtcLocationsApiSettings__c setting = LtcLocationsApiSettings__c.getOrgDefaults();
        setting.oAuthAccessToken__c = 'at';
        setting.authorization__c = 'aut';
        setting.endpoint__c = 'http://localhost';
        setting.name = 'default';
        insert setting;
        
        Test.setMock(HttpCalloutMock.class, new LtcLocationsCalloutMock());
        
        Test.startTest();			
        HttpResponse httpResponse  = LtcLocationsApiService.reprocessOAuthToken('AMS');
        Test.stopTest();
        System.assertEquals('6', LtcLocationsWeatherResponse.parse(httpResponse.getBody()).actual.temp);  		
    }    
    
    static testMethod void testParse() {
		String json= '{'+
		'    "actual": {'+
		'        "precipitation": "0.0",'+
		'        "pressure": "1027",'+
		'        "temp": "5",'+
		'        "windSpeed": "9",'+
		'        "windDirection": "10",'+
		'        "description": {'+
		'            "id": "0004",'+
		'            "icon": "&#xe642;",'+
		'            "value": "Overcast"'+
		'        }'+
		'    },'+
		'    "forecast": ['+
		'        {'+
		'            "date": "2015-03-20",'+
		'            "minTemp": "6",'+
		'            "maxTemp": "13",'+
		'            "description": {'+
		'                "id": "0001",'+
		'                "icon": "&#xe640;",'+
		'                "value": "Sunny"'+
		'            }'+
		'        },'+
		'        {'+
		'            "date": "2015-03-21",'+
		'            "minTemp": "2",'+
		'            "maxTemp": "9",'+
		'            "description": {'+
		'                "id": "0009",'+
		'                "icon": "&#xe647;",'+
		'                "value": "Patchy rain nearby"'+
		'            }'+
		'        },'+
		'        {'+
		'            "date": "2015-03-22",'+
		'            "minTemp": "1",'+
		'            "maxTemp": "8",'+
		'            "description": {'+
		'                "id": "0001",'+
		'                "icon": "&#xe640;",'+
		'                "value": "Sunny"'+
		'            }'+
		'        },'+
		'        {'+
		'            "date": "2015-03-23",'+
		'            "minTemp": "4",'+
		'            "maxTemp": "10",'+
		'            "description": {'+
		'                "id": "0002",'+
		'                "icon": "&#xe641;",'+
		'                "value": "Partly Cloudy"'+
		'            }'+
		'        },'+
		'        {'+
		'            "date": "2015-03-24",'+
		'            "minTemp": "5",'+
		'            "maxTemp": "9",'+
		'            "description": {'+
		'                "id": "0009",'+
		'                "icon": "&#xe647;",'+
		'                "value": "Patchy rain nearby"'+
		'            }'+
		'        }'+
		'    ],'+
		'    "averages": ['+
		'        {'+
		'            "month": "January",'+
		'            "absMaxTemp": "12.9",'+
		'            "absMinTemp": "-6.9",'+
		'            "avgMaxTemp": "6.8",'+
		'            "avgMinTemp": "3.4"'+
		'        },'+
		'        {'+
		'            "month": "February",'+
		'            "absMaxTemp": "15",'+
		'            "absMinTemp": "-11",'+
		'            "avgMaxTemp": "6.2",'+
		'            "avgMinTemp": "2.5"'+
		'        },'+
		'        {'+
		'            "month": "March",'+
		'            "absMaxTemp": "21.3",'+
		'            "absMinTemp": "-2.7",'+
		'            "avgMaxTemp": "9.5",'+
		'            "avgMinTemp": "5.5"'+
		'        },'+
		'        {'+
		'            "month": "April",'+
		'            "absMaxTemp": "25.9",'+
		'            "absMinTemp": "-0.2",'+
		'            "avgMaxTemp": "10.9",'+
		'            "avgMinTemp": "6.5"'+
		'        },'+
		'        {'+
		'            "month": "May",'+
		'            "absMaxTemp": "29.6",'+
		'            "absMinTemp": "5.1",'+
		'            "avgMaxTemp": "15.7",'+
		'            "avgMinTemp": "10.7"'+
		'        },'+
		'        {'+
		'            "month": "June",'+
		'            "absMaxTemp": "29.4",'+
		'            "absMinTemp": "7.2",'+
		'            "avgMaxTemp": "17",'+
		'            "avgMinTemp": "11"'+
		'        },'+
		'        {'+
		'            "month": "July",'+
		'            "absMaxTemp": "32.2",'+
		'            "absMinTemp": "7",'+
		'            "avgMaxTemp": "20.2",'+
		'            "avgMinTemp": "12"'+
		'        },'+
		'        {'+
		'            "month": "August",'+
		'            "absMaxTemp": "32.1",'+
		'            "absMinTemp": "8",'+
		'            "avgMaxTemp": "19.9",'+
		'            "avgMinTemp": "12"'+
		'        },'+
		'        {'+
		'            "month": "September",'+
		'            "absMaxTemp": "27.6",'+
		'            "absMinTemp": "6",'+
		'            "avgMaxTemp": "17.3",'+
		'            "avgMinTemp": "10"'+
		'        },'+
		'        {'+
		'            "month": "October",'+
		'            "absMaxTemp": "23.2",'+
		'            "absMinTemp": "1.9",'+
		'            "avgMaxTemp": "13.6",'+
		'            "avgMinTemp": "9.3"'+
		'        },'+
		'        {'+
		'            "month": "November",'+
		'            "absMaxTemp": "16.8",'+
		'            "absMinTemp": "-4.9",'+
		'            "avgMaxTemp": "9.3",'+
		'            "avgMinTemp": "5.6"'+
		'        },'+
		'        {'+
		'            "month": "December",'+
		'            "absMaxTemp": "13.3",'+
		'            "absMinTemp": "-3",'+
		'            "avgMaxTemp": "7.3",'+
		'            "avgMinTemp": "1"'+
		'        }'+
		'    ]'+
		'}';
		LtcLocationsWeatherResponse obj = LtcLocationsWeatherResponse.parse(json);
		System.assert(obj != null);
	}

	static testMethod void testParseLtcLocationsAirportResponse() {
		String json=		'{'+
		'    "code": "BOS",'+
		'    "name": "Logan International",'+
		'    "description": "Boston - Logan International (BOS), USA",'+
		'    "coordinates": {'+
		'        "latitude": 42.365,'+
		'        "longitude": -71.00528'+
		'    },'+
		'    "parent": {'+
		'        "code": "BOS",'+
		'        "name": "Boston",'+
		'        "description": "Boston (BOS)",'+
		'        "coordinates": {'+
		'            "latitude": 42.36444,'+
		'            "longitude": -71.00083'+
		'        },'+
		'        "parent": {'+
		'            "code": "US",'+
		'            "name": "USA",'+
		'            "description": "USA (US)",'+
		'            "coordinates": {'+
		'                "latitude": 36,'+
		'                "longitude": -100'+
		'            },'+
		'            "parent": {'+
		'                "code": "N_AMER",'+
		'                "name": "North America",'+
		'                "description": "North America (N_AMER)",'+
		'                "coordinates": {'+
		'                    "latitude": 46.316,'+
		'                    "longitude": -105.46'+
		'                }'+
		'            }'+
		'        }'+
		'    }'+
		'}';
		LtcLocationsAirportResponse obj = LtcLocationsAirportResponse.parse(json);
		System.assert(obj != null);
		System.assertEquals('BOS', obj.code);
	}
	
	
}