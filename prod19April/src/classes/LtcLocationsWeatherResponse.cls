//response object modified by Ata to include date
public class LtcLocationsWeatherResponse{
    public cls_actual actual;
    public cls_forecast[] forecast;
    public cls_averages[] averages;
    public String newAccessToken;
    public class cls_actual {
        public String precipitation;    //0.0
        public String pressure; //1027
        public String temp; //5
        public String windSpeed;    //9
        public String windDirection;    //10
        public cls_description description;
    }
    public class cls_description {
        public String id;   //0001
        public String icon; //&#xe640;
        public String value;    //Sunny
    }
    // date replaced by dater (date is a reserved word)
    public class cls_forecast {
        public String dater;    //2015-03-20
        public String minTemp;  //6
        public String maxTemp;  //13
        public cls_description description;
    }
    public class cls_averages {
        public String month;    //January
        public String absMaxTemp;   //12.9
        public String absMinTemp;   //-6.9
        public String avgMaxTemp;   //6.8
        public String avgMinTemp;   //3.4
    }
    
    public static LtcLocationsWeatherResponse parse(String json){
        List<String> dateList = new List<String>();
        Map<String, Object> tempStructure = (Map<String, Object>) System.JSON.deserializeUntyped(json);
        List<Object> forecastData = (List<Object>) tempStructure.get('forecast'); 
        for (Object obj: forecastData) {
            Map<String, Object> abc = (Map<String, Object>) obj;
            dateList.add((String)abc.get('date')); 
        }
        LtcLocationsWeatherResponse response = (LtcLocationsWeatherResponse) System.JSON.deserialize(json, LtcLocationsWeatherResponse.class);
        for (Integer i= 0; i < dateList.size(); i++) {
            response.forecast[i].dater = dateList[i];
        }
        //return (LtcLocationsWeatherResponse) System.JSON.deserialize(json, LtcLocationsWeatherResponse.class);
        return response;
    }


}