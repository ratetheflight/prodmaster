/**
 * @author (s)    : Mees Witteman
 * @description   : Apex Batch class for processing SSIM7 records (PISA flight data)
 * @log           : 2016-02-09 version 1.0
 */
public class LtcPisaBatch extends LtcQueueItemAbstractBatchable implements Database.AllowsCallouts {

    public override void initParams(String params) {} 
    
    public override Database.querylocator actualStart(Database.BatchableContext bc){
        return Database.getQueryLocator(
            ' SELECT Processed_At__c, Flightnumber__c, Email_date__c, Content__c ' + 
            ' FROM Ssim7_Record__c' +
            ' WHERE Processed_At__c = null ' +
            ' ORDER BY Email_date__c ASC, Flightnumber__c ASC' 
        );
    }
    
    public override void actualExecute(Database.BatchableContext bc, sObject[] scopeSObjects){
        Ssim7_Record__c[] records = (Ssim7_Record__c[]) scopeSObjects;
        
        List<Ssim7_Record__c> recordsToUpdate = new List<Ssim7_Record__c>();
        for (Ssim7_Record__c record : records) {

            String flightNumber = record.Flightnumber__c;
            String itineraryVI = '-1';
            String[] lines = record.Content__c.split('\n');
            
            // collect travel dates from type 3 records
            Set<Date> travelDates = new Set<Date>();
            for (String line: lines) {
                LineReader lr = new LineReader(line);
                if (lr.isRecordType3() && lr.getIVI() != itineraryVI) {
                    travelDates.addAll(lr.getDepartureDates());
                    
                    //move to new Itinerary Variation Indicator
                    itineraryVI = lr.getIVI();
                }
            }
            
            // collect flights that already exist in SF
            List<Flight__c> sfdcFlights = [
                select Id, Flight_Number__c, Scheduled_departure_date__c, CurrentLeg__c,
                (select Id, legNumber__c, 
                    actualArrivalDate__c, actualArrivalTime__c, actualDepartureDate__c, actualDepartureTime__c,  
                    scheduledArrivalDate__c, scheduledArrivalTime__c, scheduledDepartureDate__c, scheduledDepartureTime__c  
                    from Legs__r 
                    order by legNumber__c)
                from Flight__c f 
                where f.Flight_Number__c = :flightNumber 
                and f.scheduled_departure_date__c in :travelDates
                limit 1000
            ];
            
            // map on traveldate
            Map<Date, Flight__c> flightMap = new Map<Date, Flight__c>();
            for (Flight__c flight: sfdcFlights) {
                flightMap.put(flight.scheduled_departure_date__c, flight);              
            }
        
            // update in batch  
            List<Flight__c> flightsToUpdate = new List<Flight__c>();
            Set<String> flightUpdateSet = new Set<String>();
            List<Leg__c> legsToUpdate = new List<Leg__c>();
 
            System.debug(LoggingLevel.DEBUG,'second iteration over lines[] : fill and create flight / legs if needed from rectype 3');
            itineraryVI = '-1';     
            List<Date> travelDatesIVI;
            for (String line: lines) {
                LineReader lr = new LineReader(line);
                System.debug(LoggingLevel.DEBUG,'line=' + line);
                if (lr.isRecordType3() && !lr.hasOperationalSuffix() && lr.isServiceTypeJQ()) {
                    Boolean newIVI = itineraryVI != lr.getIVI();
                    if (newIVI) {
                        travelDatesIVI = lr.getDepartureDates();
                        itineraryVI = lr.getIVI();
                    }

                    List<Date> legDates = lr.getDepartureDates();
                    for (Integer i = 0; i < legDates.size() ; i++) {
                        Date travelDate = travelDatesIVI.get(i);
                        System.debug(LoggingLevel.DEBUG,'travelDate=' + travelDate);
                        Flight__c flight = flightMap.get(travelDate);
                        if (flight == null) {
                            flight = new Flight__c();
                            flight.Flight_Number__c = flightNumber;
                            flight.operatingFlightCode__c = flightNumber;
                            flight.Scheduled_Departure_Date__c = travelDate;
                            flight.currentLeg__c = 0;
                            flight.aircraftTypeCode__c = lr.getAircraftType();
                            // violates 'no DML in for loops apex rule but ... 
                            // - we need the generated ID for leg 
                            // - there are max 15 flights per SSIM7 record, so there is no issue with LIMITS here
                            insert flight;  // max 15 per ssim7
                            flightMap.put(flight.scheduled_departure_date__c, flight);
                            System.debug(LoggingLevel.DEBUG,'created new flight ' + flight);
                        }
                        
                        Integer legNo = Integer.valueOf(lr.getLegSequence()) - 1;
                        Leg__c leg = LtcUtil.findLeg(flight, legNo);
                        
                        if (leg != null) {
                            System.debug(LoggingLevel.DEBUG,'leg found ' + leg);
                        } else {
                            leg = new Leg__c();
                            leg.flight__c = flight.id;
                            leg.legNumber__c = legNo;
                            flight.legs__r.add(leg);
                            System.debug(LoggingLevel.DEBUG, 'new leg ' + leg + ' created for flight ' + flight);
                            
                            if (flight != null && !flightUpdateSet.contains(flight.Flight_Number__c + flight.Scheduled_Departure_Date__c)) {  
                                flightsToUpdate.add(flight);
                                flightUpdateSet.add(flight.Flight_Number__c + flight.Scheduled_Departure_Date__c);
                                System.debug(LoggingLevel.DEBUG,'added flight to flightsToUpdate=' + flight);
                            }
                        }
                        Date legDate = legDates.get(i);
                        LtcDateTime ldtDeparture = new LtcDateTime(legDate, lr.getDepartureTime(), lr.getDepartureTimeVariation());
                        leg.departureDate__c          = ldtDeparture.getDate();
                        leg.scheduledDepartureDate__c = ldtDeparture.getDate();
                        leg.scheduledDepartureTime__c = ldtDeparture.getTime();
                        
                        // check arrival date day transition
                        DateTime utcDepDate = new LtcDateTime(legDate, lr.getDepartureTime(), null).getDateTime();
                        DateTime utcArrDate = new LtcDateTime(legDate, lr.getArrivalTime(), null).getDateTime();
                        
                        Date legArrivalDate = legDate;
                        if (utcDepDate > utcArrDate) {
                            legArrivalDate = legDate.addDays(1);
                        }
                        
                        LtcDateTime ldtArrival = new LtcDateTime(legArrivalDate, lr.getArrivalTime(), lr.getArrivalTimeVariation());
                        leg.arrivalDate__c          = ldtArrival.getDate();
                        leg.scheduledArrivalDate__c = ldtArrival.getDate();
                        leg.scheduledArrivalTime__c = ldtArrival.getTime();
                        
                        Integer offSetInMinutes = LtcUtil.getOffSetInMinutes(lr.getDepartureTimeVariation());
                        calculateDepartureData(leg, offSetInMinutes);
                        
                        offSetInMinutes = LtcUtil.getOffSetInMinutes(lr.getArrivalTimeVariation());
                        calculateArrivalData(leg, offSetInMinutes);
                        
                        leg.origin__c = lr.getDepartureStation();
                        leg.destination__c = lr.getArrivalStation();
                        leg.status__c = 'ON_SCHEDULE';
                                                
                        legsToUpdate.add(leg);
                        System.debug(LoggingLevel.DEBUG,'added leg to legsToUpdate=' + leg);
                    }
                    
                    for (Flight__c f : flightsToUpdate) {
                        System.debug(LoggingLevel.DEBUG, 'f.currentLeg__c before ' + f.currentLeg__c );
                        f.currentLeg__c = LtcUtil.determineCurrentLeg(f);
                        System.debug(LoggingLevel.DEBUG, 'f.currentLeg__c after ' + f.currentLeg__c );
                    }
                    upsert flightsToUpdate;
                    flightsToUpdate.clear();
                    upsert legsToUpdate;
                    legsToUpdate.clear();
                    
                    
                }           
            }
 
            System.debug(LoggingLevel.FINE,'third iteration over lines[] : rectypes 4');      
            
            // --- re-load the updated flights
            sfdcFlights = [
                select Id, Flight_Number__c, Scheduled_departure_date__c, CurrentLeg__c,
                (select Id, legNumber__c, 
                    actualArrivalDate__c, actualArrivalTime__c,
                    actualDepartureDate__c, actualDepartureTime__c,
                    scheduledArrivalDate__c, scheduledArrivalTime__c,
                    scheduledDepartureDate__c, scheduledDepartureTime__c,
                    estimatedArrivalDate__c, estimatedArrivalTime__c,
                    estimatedDepartureDate__c, estimatedDepartureTime__c 
                    from Legs__r order by legNumber__c)
                from Flight__c f 
                where f.Flight_Number__c = :flightNumber 
                and f.scheduled_departure_date__c in :travelDates
                limit 1000
            ];
            
            // -- re-map on traveldate
            flightMap = new Map<Date, Flight__c>();
            for (Flight__c flight: sfdcFlights) {
                flightMap.put(flight.scheduled_departure_date__c, flight);              
            }
            // ---- end of reload ---            

            System.debug(LoggingLevel.DEBUG,'third iteration over lines[] : load actual data from rectypes 4: 958 and 901');
            Set<String> legUpdateSet = new Set<String>();
            flightUpdateSet.clear();
            for (String line: lines) {
                LineReader lr = new LineReader(line);
                if (!lr.hasOperationalSuffix() && lr.isServiceTypeJQ()) {
                    Flight__c flight;
                    if (lr.isRecordType958()) {
                        flight = flightMap.get(lr.getScheduledFlightDate());
                        System.debug(LoggingLevel.DEBUG,'process 958 line=' + line);
                        flight.registrationCode__c = lr.getRegistrationCode().substring(0, 2) + '-' + lr.getRegistrationCode().substring(2); 
                        System.debug(LoggingLevel.DEBUG,'958 add flight to upsert=' + flight);
                    } else if (lr.isRecordType901()) {
                        flight = flightMap.get(lr.getScheduledFlightDate());
                        System.debug(LoggingLevel.DEBUG,'process 901 line=' + line);
                        Integer legNo = Integer.valueOf(lr.getLegSequence()) - 1;
                        Leg__c leg = LtcUtil.findLeg(flight, legNo);
                        System.debug(LoggingLevel.DEBUG,'901 leg=' + leg);
                        
                        LtcDateTime ldtDeparture = new LtcDateTime(lr.getActualDepartureDate(), lr.getActualDepartureTime(), lr.getActualDepartureTimeVariation());
                        leg.departureDate__c = ldtDeparture.getDate();  
                        leg.actualDepartureDate__c = ldtDeparture.getDate();        
                        leg.actualDepartureTime__c = ldtDeparture.getTime();
                        
                        Integer offSetInMinutes = LtcUtil.getOffSetInMinutes(lr.getActualDepartureTimeVariation());
                        calculateDepartureData(leg, offSetInMinutes);
                        
                        LtcDateTime ldtArrival = new LtcDateTime(lr.getActualArrivalDate(), lr.getActualArrivalTime(), lr.getActualArrivalTimeVariation());
                        leg.arrivalDate__c = ldtArrival.getDate();
                        leg.actualArrivalDate__c = ldtArrival.getDate();
                        leg.actualArrivalTime__c = ldtArrival.getTime();
                        
                        offSetInMinutes = LtcUtil.getOffSetInMinutes(lr.getActualArrivalTimeVariation());                       
                        calculateArrivalData(leg, offSetInMinutes);
                        
                        leg.status__c = 'ARRIVED';
                        
                        if (!legUpdateSet.contains(leg.Id)) {
                            legsToUpdate.add(leg);
                            legUpdateSet.add(leg.Id);
                        }
                        System.debug(LoggingLevel.DEBUG,'added leg to legsToUpdate=' + leg);
                    }
                    if (flight != null && !flightUpdateSet.contains(flight.Flight_Number__c + flight.Scheduled_Departure_Date__c)) {
                        flightsToUpdate.add(flight);
                        flightUpdateSet.add(flight.Flight_Number__c + flight.Scheduled_Departure_Date__c);
                        System.debug(LoggingLevel.DEBUG,'added flight to flightsToUpdate=' + flight);
                    }
                } else {
                    System.debug(LoggingLevel.DEBUG, 'skipped line: ' + line);
                }     
            }
            
            for (Flight__c f : flightsToUpdate) {
                f.currentLeg__c = LtcUtil.determineCurrentLeg(f);
            }
            
            upsert flightsToUpdate;
            flightsToUpdate.clear();
            upsert legsToUpdate;
            legsToUpdate.clear();
            
            record.Processed_At__c = DateTime.now();
            recordsToUpdate.add(record);
        }
        update recordsToUpdate;
        recordsToUpdate.clear();
    }
       
    public override void actualFinish(Database.BatchableContext BC){
        new LtcFoxBatchCheckerSchedulable().sendMail('pisa batch report', 'PisaBatch has ended');
    }

    private void calculateDepartureData(Leg__c leg, Integer offSetInMinutes) {
        
        System.debug(LoggingLevel.DEBUG, 'calculateDepartureData leg=' + leg + ' offSetInMinutes=' + offSetInMinutes);
            
        // leg stores local flight date / time so negate offset for calculation back to UTC time
        Long schedDeparture = LtcUtil.getDateTimeGMT(leg.scheduledDepartureDate__c, leg.scheduledDepartureTime__c, offSetInMinutes * -1).getTime();
        Long actDeparture;
        
        Long delay = 0;
        if (leg.actualDepartureDate__c != null) {
            actDeparture = LtcUtil.getDateTimeGMT(leg.actualDepartureDate__c, leg.actualDepartureTime__c, offSetInMinutes * -1).getTime();
            delay = actDeparture - schedDeparture; 
        }
        leg.departureDelay__c = delay;
         
        Long departure = actDeparture == null ? schedDeparture : actDeparture;
        Long timeToDeparture = departure - DateTime.now().getTime();
        leg.timeToDeparture__c = timeToDeparture;

    }
    
    private void calculateArrivalData(Leg__c leg, Integer offSetInMinutes) {
        System.debug(LoggingLevel.DEBUG, 'calculateArrivalData leg=' + leg + ' offSetInMinutes=' + offSetInMinutes);

        Long schedArrival = LtcUtil.getDateTimeGMT(leg.scheduledArrivalDate__c, leg.scheduledArrivalTime__c,  offSetInMinutes * -1).getTime();
        Long actArrival;
        
        Long delay = 0;
        if (leg.actualArrivalDate__c != null) {
            actArrival = LtcUtil.getDateTimeGMT(leg.actualArrivalDate__c, leg.actualArrivalTime__c,  offSetInMinutes * -1).getTime();
            delay = actArrival - schedArrival; 
        }
        leg.ArrivalDelay__c = delay;
         
        Long arrival = actArrival == null ? schedArrival : actArrival;
        Long timeToArrival = arrival - DateTime.now().getTime();
        leg.timeToArrival__c = timeToArrival;

    }

    
    public class LineReader {
        private String line;
        
        public LineReader(String line) {
            this.line = line;   
        }

        // generic fields
        public String getRecordType() {
            return line.substring(0,1);
        }
        public String getOperationalSuffix() {
            return line.substring(1,2);
        }
        public Boolean hasOperationalSuffix() {
            return getOperationalSuffix().trim().length() != 0;
        }
        public String getFlightNumber() {
            return line.substring(2,4) + line.substring(5,9);
        }
        public String getIVI() {
            return getItineraryVariationIndicator();
        }
        public String getItineraryVariationIndicator() {
            return line.substring(9,11);
        }
        public String getLegSequence() {
            return line.substring(11,13);
        }
        public Integer getRecordNumber() {
            return Integer.valueOf(line.substring(195));
        }
        
        //get the service type to check J or Q
        public String getServiceTypeJQ() {
            return line.substring(13,14);
        }
        
        // record type 3 fields
        public Date getStartDatePOO() {
            return LtcUtil.getDate(line.substring(14,21));
        }
        public Date getEndDatePOO() {
            return LtcUtil.getDate(line.substring(21,28));
        }
        public String getDaysPOO() {
            return line.substring(28,35);
        }
        public String getDepartureStation() {
            return line.substring(36,39);
        }
        public String getDepartureTime() {
            return line.substring(39,43);
        }
        public String getDepartureTimeVariation() {
            return line.substring(47,52);
        }
        public String getArrivalStation() {
            return line.substring(54,57);
        }
        public String getArrivalTime() {
            return line.substring(61,65);
        }
        public String getArrivalTimeVariation() {
            return line.substring(65,70);
        }
        public String getAircraftType() {
            return line.substring(72,75);
        }
        public List<Date> getDepartureDates() {
            List<Date> result = new List<Date>();
            Date start = getStartDatePOO();
            Date endDate = getEndDatePOO();
            while (start.daysBetween(endDate) >= 0) {
                System.debug(LoggingLevel.FINE,'daysPOO=' + getDaysPOO() + ' date=' + start + ' dayOfweek=' + LtcUtil.getDayOfWeek(start));
                if (getDaysPOO().contains(LtcUtil.getDayOfWeek(start))) {
                    System.debug(LoggingLevel.FINE,'departure flight ' + getFlightNumber() + ' ' + start);
                    result.add(start);
                }
                start = start.addDays(1);
            }
            return result;  
        }
        
        // rectypes 958 and 901
        public String getRegistrationCode() {
            return line.substring(54,59);
        }
        public Date getScheduledFlightDate() {
            return LtcUtil.getDate(line.substring(39,46));
        }
        public Date getActualDepartureDate() {
            return LtcUtil.getDate(line.substring(49,56));
        }
        public String getActualDepartureTime() {
            return line.substring(56,60);
        }
        public String getActualDepartureTimeVariation() {
            return line.substring(60,65);
        }   
        public Date getActualArrivalDate() {
            return LtcUtil.getDate(line.substring(100,107));
        }
        public String getActualArrivalTime() {
            return line.substring(107,111);
        }
        public String getActualArrivalTimeVariation() {
            return line.substring(111,116);
        }   
        
        //record indicators             
        public Boolean isRecordType3() {
            return '3' == getRecordType();
        }
                    
        public Boolean isRecordType901() {
            return '4' == getRecordType() && '901' == line.substring(30,33);
        }
                
        public Boolean isRecordType958() {
            return '4' == getRecordType() && '958' == line.substring(30,33);
        }
        
        public Boolean isServiceTypeJQ(){
            return 'J' == getServiceTypeJQ() || 'Q'  == getServiceTypeJQ();
        }
        
    }
    


}