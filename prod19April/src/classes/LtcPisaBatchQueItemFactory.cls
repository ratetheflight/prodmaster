/**
 * Create Que Items for the Pis batch parts
 **/
global class LtcPisaBatchQueItemFactory {
	private static final Integer SCOPE_SIZE = 1;
	
    public static QueueItem__c createQueueItemForPisaBatch() {
        Time midnight = Time.newInstance(0, 0, 0, 0);
        Datetime tomorrow = Datetime.newInstance(Date.today().addDays(1), midnight);
        return new QueueItem__c(
            Class__c = 'LtcPisaBatch',
            ScopeSize__c = SCOPE_SIZE,            
            Run_Before__c = tomorrow
        );
    }
    
    public static QueueItem__c createQueueItemForPisaPrepareBatch() {
        Time midnight = Time.newInstance(0, 0, 0, 0);
        Datetime tomorrow = Datetime.newInstance(Date.today().addDays(1), midnight);
        return new QueueItem__c(
            Class__c = 'LtcPisaPrepareBatch',
            ScopeSize__c = SCOPE_SIZE,            
            Run_Before__c = tomorrow
        );
    }
}