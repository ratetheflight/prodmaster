/**
 * test the Pisa batch
 */
@isTest
public class LtcPisaBatchTest {

    public static testMethod void testBatchKL1001() {
		// setup a SSIM7 record
		Ssim7_Record__c record = new Ssim7_Record__c();
		record.FlightNumber__c = 'KL1001';
		record.Content__c = 
'3 KL 10010101J17FEB1617FEB16  3     AMS06150615+0100  LHR07400740+0000  73WC8M130                                               KL KL KL KL 1002                            C36M106               005023' +
'\n4 KL 10010101J              AB902AMSLHRE                                                                                                                                                          005024' +
'\n4 KL 10010101J              AB901AMSLHR17FEB16AMS17FEB160620+010017FEB160648+0100LHR17FEB160727+000017FEB160731+0000                                                                              005025' +
'\n4 KL 10010101J              AB958AMSLHR17FEB1617FEB16APHBGH                                                                                                                                       005026' +
'\n3 KL 10010201J18FEB1618FEB16   4    AMS06150615+0100  LHR07400740+0000  73WC16M118                                              KL KL KL KL 1002                            C36M106               005027' +
'\n4 KL 10010201J              AB902AMSLHRE                                                                                                                                                          005028' +
'\n4 KL 10010201J              AB901AMSLHR18FEB16AMS18FEB160620+010018FEB160658+0100LHR18FEB160738+000018FEB160743+0000                                                                              005029' +
'\n4 KL 10010201J              AB958AMSLHR18FEB1618FEB16APHBGP                                                                                                                                       005030' +
'\n3 KL 10010301J19FEB1619FEB16    5   AMS06150615+0100  LHR07400740+0000  73WC20M112                                              KL KL KL KL 1002                            C36M106               005031' +
'\n4 KL 10010301J              AB902AMSLHRE                                                                                                                                                          005032' +
'\n4 KL 10010301J              AB901AMSLHR19FEB16AMS19FEB160635+010019FEB160649+0100LHR19FEB160739+000019FEB160744+0000                                                                              005033' +
'\n4 KL 10010301J              AB958AMSLHR19FEB1619FEB16APHBGN                                                                                                                                       005034' +
'\n3 KL 10010401J20FEB1620FEB16     6  AMS06150615+0100  LHR07400740+0000  73WC20M112                                              KL KL KL KL 1002                            C36M106               005035' +
'\n4 KL 10010401J              AB902AMSLHRE                                                                                                                                                          005036' +
'\n4 KL 10010401J              AB901AMSLHR20FEB16AMS20FEB160615+010020FEB160627+0100LHR20FEB160720+000020FEB160723+0000                                                                              005037' +
'\n4 KL 10010401J              AB958AMSLHR20FEB1620FEB16APHBGI                                                                                                                                       005038' +
'\n3 KL 10010501J21FEB1621FEB16      7 AMS06150615+0100  LHR07400740+0000  73HC20M156                                              KL KL KL KL 1002                            C36M150               005039' +
'\n4 KL 10010501J              AB902AMSLHRF                                                                                                                                                          005040' +
'\n4 KL 10010501J              AB901AMSLHR21FEB16AMS21FEB160617+010021FEB160632+0100LHR21FEB160726+000021FEB160731+0000                                                                              005041' +
'\n4 KL 10010501J              AB958AMSLHR21FEB1621FEB16APHBXV                                                                                                                                       005042' +
'\n3 KL 10010601J22FEB1622FEB161       AMS06150615+0100  LHR07400740+0000  73HC20M150                                              KL KL KL KL 1002                            C36M144               005043' +
'\n4 KL 10010601J              AB902AMSLHRE                                                                                                                                                          005044' +
'\n4 KL 10010601J              AB901AMSLHR22FEB16AMS22FEB160639+010022FEB160649+0100LHR22FEB160749+000022FEB160800+0000                                                                              005045' +
'\n4 KL 10010601J              AB958AMSLHR22FEB1622FEB16APHBXL                                                                                                                                       005046' +
'\n3 KL 10010701J23FEB1623FEB16 2      AMS06150615+0100  LHR07400740+0000  73HC12M168                                              KL KL KL KL 1002                            C36M150               005047' +
'\n4 KL 10010701J              AB902AMSLHRF                                                                                                                                                          005048' +
'\n4 KL 10010701J              AB901AMSLHR23FEB16AMS23FEB160619+010023FEB160630+0100LHR23FEB160712+000023FEB160721+0000                                                                              005049' +
'\n4 KL 10010701J              AB958AMSLHR23FEB1623FEB16APHBXW                                                                                                                                       005050' +
'\n3 KL 10010801J24FEB1624FEB16  3     AMS06150615+0100  LHR07400740+0000  73WC12M124                                              KL KL KL KL 1002                            C36M106               005051' +
'\n4 KL 10010801J              AB902AMSLHRE                                                                                                                                                          005052' +
'\n4 KL 10010801J              AB901AMSLHR24FEB16AMS24FEB160623+010024FEB160631+0100LHR24FEB160722+000024FEB160730+0000                                                                              005053' +
'\n4 KL 10010801J              AB958AMSLHR24FEB1624FEB16APHBGF                                                                                                                                       005054' +
'\n3 KL 10010901J25FEB1625FEB16   4    AMS06150615+0100  LHR07400740+0000  73HC16M162                                              KL KL KL KL 1002                            C36M150               005055' +
'\n4 KL 10010901J              AB902AMSLHRF                                                                                                                                                          005056' +
'\n4 KL 10010901J              AB958AMSLHR25FEB1625FEB16SPHBXU                                                                                                                                       005057' +
'\n3 KL 10011001J26FEB1626FEB16    5   AMS06150615+0100  LHR07400740+0000  73WC16M118                                              KL KL KL KL 1002                            C36M106               005058' +
'\n4 KL 10011001J              AB902AMSLHRE                                                                                                                                                          005059' +
'\n3 KL 10011101J27FEB1627FEB16     6  AMS06150615+0100  LHR07400740+0000  73HC20M156                                              KL KL KL KL 1002                            C36M150               005060' +
'\n4 KL 10011101J              AB902AMSLHRF                                                                                                                                                          005061' +
'\n3 KL 10011201J28FEB1628FEB16      7 AMS06150615+0100  LHR07400740+0000  73HC20M150                                              KL KL KL KL 1002                            C36M144               005062' +
'\n4 KL 10011201J              AB902AMSLHRE                                                                                                                                                          005063' +
'\n3 KL 10011301J29FEB1629FEB161       AMS06150615+0100  LHR07400740+0000  73WC20M112                                              KL KL KL KL 1002                            C36M106               005064' +
'\n4 KL 10011301J              AB902AMSLHRE                                                                                                                                                          005065' +
'\n3 KL 10011401J01MAR1601MAR16 2      AMS06150615+0100  LHR07400740+0000  73JC28M150                                              KL KL KL KL 1002                            C56M132               005066' +
'\n4 KL 10011401J              AB902AMSLHRC                                                                                                                                                          005067' +
'\n3 KL 10011501J02MAR1602MAR16  3     AMS06150615+0100  LHR07400740+0000  73HC20M150                                              KL KL KL KL 1002                            C36M144               005068' +
'\n4 KL 10011501J              AB902AMSLHRE                                                                                                                                                          005069';
	
		insert record;
		
		Test.startTest();
			
		LtcPisaBatch pb = new LtcPisaBatch();
		Database.querylocator ql = pb.actualStart(null);
		System.assert(ql != null);
		SObject[] scope = Database.query(
			' SELECT Processed_At__c, Flightnumber__c,  Content__c ' + 
            ' FROM Ssim7_Record__c' +
            ' WHERE Processed_At__c = null ' +  
            ' ORDER BY  Flightnumber__c ASC');
		pb.actualExecute(null, scope);
		pb.actualFinish(null);
		
		//assert that a flight has been added
		
		List<Flight__c> flights = [
		    select Id
		    from Flight__c f 
		];
		System.assertEquals(15, flights.size());
		
		//assert that the record was marked as processed
		List<Ssim7_Record__c> records = [
			SELECT Processed_At__c, Flightnumber__c, Content__c 
            FROM Ssim7_Record__c
		];
		record = records.get(0);
		
		System.assert(record.Processed_At__c != null);
		

    }
    
    public static testMethod void testBatchKL0411() {
		// setup a SSIM7 record
		Ssim7_Record__c record = new Ssim7_Record__c();
		record.FlightNumber__c = 'KL0411';
		record.Content__c = 
  '3 KL 04110101J17FEB1617FEB16  3     AMS13251325+0100  DMM19251925+0300  333C30M262                                              KL KL KL                                    C30M262LL13PP7        000028' +
'\n4 KL 04110101J              AB902AMSDMMA                                                                                                                                                          000029' +
'\n4 KL 04110101J              AB911AMSDMMLL13 PP7                                                                                                                                                   000030' +
'\n4 KL 04110101J              AB901AMSDMM17FEB16AMS17FEB161331+010017FEB161343+0100DMM17FEB161912+030017FEB161922+0300                                                                              000031' +
'\n4 KL 04110101J              AB958AMSDMM17FEB1617FEB16APHAKF                                                                                                                                       000032' +
'\n3 KL 04110102J17FEB1617FEB16  3     DMM20152015+0300  KWI21202120+0300  333C30M262                                              KL KL KL                                    C30M262LL13PP7        000033' +
'\n4 KL 04110102J              BC902DMMKWIA                                                                                                                                                          000034' +
'\n4 KL 04110102J              BC911DMMKWILL13 PP7                                                                                                                                                   000035' +
'\n4 KL 04110102J              BC901DMMKWI17FEB16DMM17FEB162015+030017FEB162030+0300KWI17FEB162106+030017FEB162113+0300                                                                              000036' +
'\n4 KL 04110102J              BC958DMMKWI17FEB1617FEB16APHAKF                                                                                                                                       000037' +
'\n3 KL 04110103J17FEB1617FEB16  3     KWI22352235+0300  AMS05050505+0100  333C30M262                                              KL KL KL                                    C30M262LL13PP7        000038' +
'\n4 KL 04110103J              CD902KWIAMSA                                                                                                                                                          000039' +
'\n4 KL 04110103J              CD911KWIAMSLL13 PP7                                                                                                                                                   000040' +
'\n4 KL 04110103J              CD901KWIAMS17FEB16KWI17FEB162221+030017FEB162236+0300AMS18FEB160453+010018FEB160505+0100                                                                              000041' +
'\n4 KL 04110103J              CD958KWIAMS17FEB1617FEB16APHAKF                                                                                                                                       000042' +
'\n3 KL 04110201J22FEB1602MAR161 3     AMS13251325+0100  DMM19251925+0300  332C30M213                                              KL KL KL                                    C30M213LL9PP6         000043' +
'\n4 KL 04110201J              AB902AMSDMMB                                                                                                                                                          000044' +
'\n4 KL 04110201J              AB911AMSDMMLL9  PP6                                                                                                                                                   000045' +
'\n4 KL 04110201J              AB901AMSDMM22FEB16AMS22FEB161345+010022FEB161358+0100DMM22FEB161935+030022FEB161940+0300                                                                              000046' +
'\n4 KL 04110201J              AB958AMSDMM22FEB1622FEB16APHAOE                                                                                                                                       000047' +
'\n4 KL 04110201J              AB958AMSDMM24FEB1624FEB16APHAOM                                                                                                                                       000048' +
'\n3 KL 04110202J22FEB1602MAR161 3     DMM20152015+0300  KWI21202120+0300  332C30M213                                              KL KL KL                                    C30M213LL9PP6         000049' +
'\n4 KL 04110202J              BC902DMMKWIB                                                                                                                                                          000050' +
'\n4 KL 04110202J              BC911DMMKWILL9  PP6                                                                                                                                                   000051' +
'\n4 KL 04110202J              BC901DMMKWI22FEB16DMM22FEB162026+030022FEB162037+0300KWI22FEB162118+030022FEB162125+0300                                                                              000052' +
'\n4 KL 04110202J              BC958DMMKWI22FEB1622FEB16APHAOE                                                                                                                                       000053' +
'\n4 KL 04110202J              BC958DMMKWI24FEB1624FEB16APHAOM                                                                                                                                       000054' +
'\n3 KL 04110203J22FEB1602MAR161 3     KWI22352235+0300  AMS05050505+0100  332C30M213                                              KL KL KL                                    C30M213LL9PP6         000055' +
'\n4 KL 04110203J              CD902KWIAMSB                                                                                                                                                          000056' +
'\n4 KL 04110203J              CD911KWIAMSLL9  PP6                                                                                                                                                   000057' +
'\n4 KL 04110203J              CD901KWIAMS22FEB16KWI22FEB162233+030022FEB162247+0300AMS23FEB160450+010023FEB160501+0100                                                                              000058' +
'\n4 KL 04110203J              CD958KWIAMS22FEB1622FEB16APHAOE                                                                                                                                       000059' +
'\n4 KL 04110203J              CD958KWIAMS24FEB1624FEB16APHAOM                                                                                                                                       000060';
	
		insert record;
		
		Test.startTest();
			
		LtcPisaBatch pb = new LtcPisaBatch();
		Database.querylocator ql = pb.actualStart(null);
		System.assert(ql != null);
		SObject[] scope = Database.query(
			' SELECT Processed_At__c, Flightnumber__c, Content__c ' + 
            ' FROM Ssim7_Record__c' +
            ' WHERE Processed_At__c = null ' +  
            ' ORDER BY Flightnumber__c ASC');
		pb.actualExecute(null, scope);
		pb.actualFinish(null);
		
		//assert that flights have been added
		
		List<Flight__c> flights = [
		    select Id
		    from Flight__c f 
		];
		System.assertEquals(5, flights.size());
		
		//assert that the record was marked as processed
		List<Ssim7_Record__c> records = [
			SELECT Processed_At__c, Flightnumber__c,  Content__c 
            FROM Ssim7_Record__c
		];
		record = records.get(0);
		
		System.assert(record.Processed_At__c != null);
		

    }
}