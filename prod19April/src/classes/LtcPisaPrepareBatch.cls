public class LtcPisaPrepareBatch extends LtcQueueItemAbstractBatchable implements Database.AllowsCallouts {
    
    public override void initParams(String params) {} 
    
    public override Database.querylocator actualStart(Database.BatchableContext bc){
    	Account account;
		List<Account> accounts = [
    		select Id, Name from Account where Name = 'pisabatch'
    	];
    	
    	if (accounts != null && accounts.size() > 0) {
			account = accounts.get(0);
    	}
    	
    	List<Attachment> atts = [
			select id, body, name, parentId 
			from Attachment 
			where parentId = :account.Id 
			and name like 'EE10.PRD.PWC.FLTGUIDE.SCHED%'
		];
    	
    	Attachment attachment;
    	if (atts != null && atts.size() == 1) {
    		attachment = atts.get(0);
    		if (attachment.Name == 'EE10.PRD.PWC.FLTGUIDE.SCHED.ZIP') {
    			String unzipEndpoint = 'https://fg-push-notifications-test.herokuapp.com/unzip';
    	    	if (LtcSettings__c.getInstance('unzip endpoint') != null) {
            		unzipEndpoint =  LtcSettings__c.getInstance('unzip endpoint').value__c;
        		}
    			String ssim7File = getCalloutResponseBody(unzipEndpoint);
    			processSsim7File(ssim7File);
    		} else if (attachment.Name == 'EE10.PRD.PWC.FLTGUIDE.SCHED.TXT') {
				String ssim7File = attachment.Body.toString();
    			processSsim7File(ssim7File);
    		}
    		// prep done, now schedule next step
    		insert LtcPisaBatchQueItemFactory.createQueueItemForPisaBatch();
    	}
    	
  	 	//Fake query locator to keep SFDC happy  	 	
        return Database.getQueryLocator('select ID from Flight__c limit 0');  
    }
    
    private String getCalloutResponseBody(String url) {
	    HttpRequest req = new HttpRequest();
	    req.setEndpoint(url);
	    req.setMethod('GET');

	    HttpResponse res = new Http().send(req);
	    return res.getBody();
	}

    private void processSsim7File(String ssim7FileContent) {
        System.debug(LoggingLevel.INFO, 'start processing ssim7FileContent : Limits.getHeapSize()='  + Limits.getHeapSize() + ' Limits.getLimitHeapSize()=' + Limits.getLimitHeapSize());
        List<Ssim7_record__c> recordsToInsert = new List<Ssim7_record__c>();

        LtcRowIteratorUtility r = new LtcRowIteratorUtility(ssim7FileContent,'\n');
        ssim7FileContent = null;
        
        Ssim7_record__c record = new Ssim7_record__c();
        String row; Integer i = 0; 
        String prevFlightNumber = '';
        String flightNumber = '';
        String content = '';
        Integer rowsPerFLight = 0; 
        DateTime today = Datetime.now();
        while (r.hasNext()) {
            row = r.next();
            System.debug(LoggingLevel.DEBUG, 'process row: ' + row);
            flightNumber = row.substring(2, 9).replaceAll(' ', '');
            if (flightNumber == prevFlightNumber) {
        		content = content + '\n' + row ;
                rowsPerFLight++;
            } else {
            	record = new Ssim7_record__c();
                record.Flightnumber__c = prevFlightNumber; 
                record.Content__c = content;
                record.Email_date__c = today;
	
	            if (record.Content__c != null && record.Content__c.startsWith('3 KL')) {
		        	recordsToInsert.add(record);
		        	System.debug('added record ' + record);
	            }
	            if (recordsToInsert.size() >= 200) {
	            	insert recordsToInsert;
	                recordsToInsert.clear();
	            } 

                // new flight
                content = row;
                rowsPerFLight = 1;
                prevFlightNumber = row.substring(2, 9).replaceAll(' ', '');
			}
	    }
		insert recordsToInsert;
    }
        
    public override void actualExecute(Database.BatchableContext bc, sObject[] scopeSObjects){}
       
    public override void actualFinish(Database.BatchableContext BC){}
    
}