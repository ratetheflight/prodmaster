/**
 * This class contains unit tests for LtcPisaPrepareBatch
 */
@isTest
private class LtcPisaPrepareBatchTest {

    static testMethod void pisaPrepareBatch() {
        Account account = new Account();
		account.Name = 'pisabatch';
		insert account;
		
		Attachment attachment = new Attachment();
		attachment.ParentId = account.Id;
		attachment.Name = 'EE10.PRD.PWC.FLTGUIDE.SCHED.TXT';
		attachment.Body = Blob.valueOf('test text');
		insert attachment;

		LtcPisaPrepareBatch bat = new LtcPisaPrepareBatch();
		bat.actualStart(null);
		
		// validate that one QI is inserted ...		
		List<QueueItem__c> qis = [
			SELECT Id, Message__c, Status__c
			FROM QueueItem__c
		];
		
		System.assertEquals(1, qis.size());
       
    }
}