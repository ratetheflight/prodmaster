/**
 * @author (s)    :	m.lapere@gen25.com
 * @description   : Abstract class that can be extended by classes to make them batchable
 *
 * @log: 	2015/04/13: version 1.0 
 * Extend this class to make a class batchable, and executable by LtcQueueItemRunner.
 * You can then create QueueItem records with the name of the extending class in the Class__c field.
 * They will be picked up by LtcQueueItemRunner, which will make sure they get executed even if there are already 5 batches running.
 * NOTE: the extending class must be a top-level class for this to work. Inner classes will cause internal Salesforce exceptions to occur.
 */
public with sharing abstract class LtcQueueItemAbstractBatchable implements Database.Batchable<sObject>, Database.Stateful, LtcQueueItemRunner.LtcQueueItemRunnableInterface {

	// internal state stuff
	private Id queueItemId;
	private Boolean hasError = false;

	// these are the values for the Status__c field
	public enum status {
		QUEUED,
		STARTED,
		FINISHED,
		FAILED
	}

	// these methods must be implemented by the extending class
	public abstract void initParams(String params);
	public abstract Database.querylocator actualStart(Database.BatchableContext bc);
	public abstract void actualExecute(Database.BatchableContext bc, sObject[] scope);
	public abstract void actualFinish(Database.BatchableContext bc);

	public void initFromQueueItem(QueueItem__c qi) {
		this.queueItemId = qi.Id;
		this.initParams(qi.Parameters__c);
	}

	public Database.querylocator start(Database.BatchableContext bc){
        try {
        	return this.actualStart(bc);
        } catch (Exception e) {
        	this.handleException(e);
        	// return a mock query locator so the batch won't crash
        	// originally this returned null, which compiles but causes issues at runtime
        	return Database.getQueryLocator('SELECT Id FROM QueueItem__c LIMIT 1');
        }
	}

	public void execute(Database.BatchableContext bc, sObject[] scope){
		try {
        	this.actualExecute(bc, scope);
        } catch (Exception e) {
        	this.handleException(e);
        }
	}
	
	public void finish(Database.BatchableContext bc) {
		try {
        	this.actualFinish(bc);
        } catch (Exception e) {
        	this.handleException(e);
        	return;
        }
        if (!this.hasError) {
        	// mark the QueueItem as finished
			QueueItem__c qi = this.getQueueItem();
			qi.Status__c = LtcQueueItemAbstractBatchable.status.FINISHED.name();
			qi.Finished__c = Datetime.now();		
			update qi;
		}
	}

	private void handleException(Exception e) {
		// mark the QueueItem as failed, with a stack trace
		QueueItem__c qi = this.getQueueItem();
		qi.Status__c = LtcQueueItemAbstractBatchable.status.FAILED.name();
		qi.Message__c = e.getTypeName() + ': ' + e.getMessage() + '\n' + e.getStackTraceString();
		update qi;
		this.hasError = true;
	}

	private QueueItem__c getQueueItem() {
		// get the QueueItem record for this object
		return [
			SELECT Finished__c
			FROM QueueItem__c
			WHERE Id = :this.queueItemId
		][0];
	}

}