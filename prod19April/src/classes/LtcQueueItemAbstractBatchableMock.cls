/**
 * @author (s)    :	m.lapere@gen25.com
 * @description   : Test class implementation of LtcQueueItemAbstractBatchable
 *
 * @log: 	2015/04/13: version 1.0
 * Simple implementation of LtcQueueItemAbstractBatchable for use in LtcQueueItemAbstractBatchableTest
 * This could not be done as an inner class of LtcQueueItemAbstractBatchableTest
 * because Salesforce throws an internal error when an internal batch class gets executed
 */
@isTest
public with sharing class LtcQueueItemAbstractBatchableMock extends LtcQueueItemAbstractBatchable {
	
	private String params;

	public override void initParams(String params) {
		this.params = params;
		if (this.params == 'init') {
			throw new LtcQueueItemRunner.QueueItemException();
		}
	}

	public override Database.QueryLocator actualStart(Database.BatchableContext bc) {
		if (this.params == 'start') {
			throw new LtcQueueItemRunner.QueueItemException();
		}
		return Database.getQueryLocator('SELECT Id FROM QueueItem__c LIMIT 5');
	}

	public override void actualExecute(Database.BatchableContext bc, List<SObject> scope) {
		if (this.params == 'execute') {
			throw new LtcQueueItemRunner.QueueItemException();
		}
	}

	public override void actualFinish(Database.BatchableContext bc) {
		if (this.params == 'finish') {
			throw new LtcQueueItemRunner.QueueItemException();
		}
	}
}