/**
 * @author (s)    :	m.lapere@gen25.com
 * @description   : Test class for LtcQueueItemAbstractBatchable
 *
 * @log: 	2015/04/13: version 1.0
 */
@isTest
private class LtcQueueItemAbstractBatchableTest {

	public static testMethod void testLtcQueueItemAbstractBatchable() {

		// create some QueueItem records for the test class LtcQueueItemAbstractBatchableMock
		// this class is designed to throw an exception somewhere, depending on the Parameters__c
		
		QueueItem__c qi = new QueueItem__c(
			Class__c = 'LtcQueueItemAbstractBatchableMock',
			Status__c = LtcQueueItemAbstractBatchable.status.QUEUED.name()
		);

		QueueItem__c[] qis = new QueueItem__c[]{};
		
		qi = qi.clone();
		qi.Parameters__c = '';
		qis.add(qi);
		
		qi = qi.clone();
		qi.Parameters__c = 'init';
		qis.add(qi);

		qi = qi.clone();
		qi.Parameters__c = 'start';
		qis.add(qi);

		qi = qi.clone();
		qi.Parameters__c = 'execute';
		qis.add(qi);

		qi = qi.clone();
		qi.Parameters__c = 'finish';
		qis.add(qi);
		
		insert qis;

		(new LtcQueueItemRunner()).execute(null);
		Test.startTest();
		Test.stopTest();

		// the first one should have been executed normally
		qi = [SELECT BatchId__c, Status__c, Message__c, Started__c, Finished__c FROM QueueItem__c WHERE Id = :qis[0].Id];
		System.assertEquals(LtcQueueItemAbstractBatchable.status.FINISHED.name(), qi.Status__c);
		System.assertEquals(null, qi.Message__c);
		System.assertNotEquals(null, qi.BatchId__c);
		System.assertNotEquals(null, qi.Started__c);
		System.assertNotEquals(null, qi.Finished__c);

		// the second one should have failed without receiving a batch id or a started datetime
		qi = [SELECT BatchId__c, Status__c, Message__c, Started__c, Finished__c FROM QueueItem__c WHERE Id = :qis[1].Id][0];
		System.assertEquals(LtcQueueItemAbstractBatchable.status.FAILED.name(), qi.Status__c);
		System.assertNotEquals(null, qi.Message__c);
		System.assertEquals(null, qi.BatchId__c);
		System.assertEquals(null, qi.Started__c);
		System.assertEquals(null, qi.Finished__c);

		// the next three should have all failed, with a batch id and a started datetime
		qi = [SELECT BatchId__c, Status__c, Message__c, Started__c, Finished__c FROM QueueItem__c WHERE Id = :qis[2].Id][0];
		System.assertEquals(LtcQueueItemAbstractBatchable.status.FAILED.name(), qi.Status__c);
		System.assertNotEquals(null, qi.Message__c);
		System.assertNotEquals(null, qi.BatchId__c);
		System.assertNotEquals(null, qi.Started__c);
		System.assertEquals(null, qi.Finished__c);

		qi = [SELECT BatchId__c, Status__c, Message__c, Started__c, Finished__c FROM QueueItem__c WHERE Id = :qis[3].Id][0];
		System.assertEquals(LtcQueueItemAbstractBatchable.status.FAILED.name(), qi.Status__c);
		System.assertNotEquals(null, qi.Message__c);
		System.assertNotEquals(null, qi.BatchId__c);
		System.assertNotEquals(null, qi.Started__c);
		System.assertEquals(null, qi.Finished__c);

		qi = [SELECT BatchId__c, Status__c, Message__c, Started__c, Finished__c FROM QueueItem__c WHERE Id = :qis[4].Id][0];
		System.assertEquals(LtcQueueItemAbstractBatchable.status.FAILED.name(), qi.Status__c);
		System.assertNotEquals(null, qi.Message__c);
		System.assertNotEquals(null, qi.BatchId__c);
		System.assertNotEquals(null, qi.Started__c);
		System.assertEquals(null, qi.Finished__c);
	}
}