/**
 * @author (s)    :	m.lapere@gen25.com
 * @description   : Looks in the QueueItem__c table for batches to execute
 *
 * @log: 	2015/04/13: version 1.0
 * Notes:
 * Any batches that you want to run must extend LtcQueueItemAbstractBatchable
 * QUEUE_SIZE can be increased to 100 if Apex Flex Queue is enabled in the org
 *
 * This class can be scheduled every 10 minutes by running the following in an anonymous context:
 *
 *	LtcQueueItemRunner job = new LtcQueueItemRunner();
 *	for (Integer i = 0; i < 60; i += 10) {
 *		String jobName = 'LtcQueueItemRunner ' + i;
 *		String cronString = '0 ' + i + ' * * * ? *';
 *		System.schedule(jobName, cronString, job);
 *	}
 */
global with sharing class LtcQueueItemRunner implements Schedulable {

	private static final Integer DEFAULT_SCOPE_SIZE = 100;
	private static final Integer QUEUE_SIZE = 5;

	global void execute(SchedulableContext scMain) {

		// find any jobs that are already running
		Map<Id, AsyncApexJob> runningJobs = this.getRunningJobs();

		// find any QueueItems that are marked as running
		// but are not in the list of currently running jobs
		// and set their status to failed
		this.markNonRunningJobsAsFailed(runningJobs);

		// start more jobs (no more than QUEUE_SIZE, based on the number of running jobs)
		this.startMoreJobs(LtcQueueItemRunner.QUEUE_SIZE - runningJobs.size());
	}

	private Map<Id, AsyncApexJob> getRunningJobs() {
		// find any jobs that are already running
		return new Map<Id, AsyncApexJob>([
			SELECT Id
			FROM AsyncApexJob
			WHERE JobType = 'BatchApex'
			AND (Status = 'Queued' OR Status = 'Processing' OR Status = 'Preparing')
		]);
	}

	private void markNonRunningJobsAsFailed(Map<Id, AsyncApexJob> runningJobs) {
		// find any QueueItems that are marked as running
		// but are not in the map of currently running jobs
		// and set their status to failed
		for (QueueItem__c[] qis : [
			SELECT Id, Message__c, Status__c
			FROM QueueItem__c
			WHERE BatchId__c NOT IN :runningJobs.keySet()
			AND (Status__c = :LtcQueueItemAbstractBatchable.status.STARTED.name()
				OR (Started__c != null AND Finished__c = null))
		]) {
			for (QueueItem__c qi : qis) {				
				System.debug(LoggingLevel.DEBUG, 'qi.Message__c=' + qi.Message__c + ' qi.Status__c=' + qi.Status__c );
				qi.Status__c = LtcQueueItemAbstractBatchable.status.FAILED.name();
				if (qi.Message__c == null || qi.Message__c.trim() == '') {
					qi.Message__c = 'Job started but never finished';
				}
			}
			update qis;
		}
	}

	private void startMoreJobs(Integer nrOfJobs) {
		// this method will attemt to start <nrOfJobs> batches
		for (QueueItem__c[] qis : [
			SELECT Id, BatchId__c,
				Class__c, Parameters__c, ScopeSize__c,
				Status__c, Message__c,
				Priority__c, Started__c, Finished__c, Run_After__c, Run_Before__c
			FROM QueueItem__c
			WHERE Started__c = null
			AND Status__c = :LtcQueueItemAbstractBatchable.status.QUEUED.name()
			AND (Run_After__c = null
				OR Run_After__c > :System.now())
			ORDER BY
				Priority__c DESC NULLS LAST,
				Run_Before__c ASC NULLS LAST,
				Run_After__c ASC NULLS LAST
		]) {
			
			for (QueueItem__c qi : qis) {
				if (qi.Run_Before__c != null && qi.Run_Before__c < System.now()) {
					// this job wasn't executed on time
					qi.Status__c = LtcQueueItemAbstractBatchable.status.FAILED.name();
					qi.Message__c = 'Job didn\'t start on time';
				} else if (nrOfJobs > 0) {
					// there is still room for a new job
					Boolean success = this.startJob(qi);
					if (success) {
						nrOfJobs = nrOfJobs - 1;
					}
				} else {
					// number of concurrent jobs exceeded
					// don't do anything
					// these jobs will be picked up in the next run
				}
			}
			update qis;
		}
	}

	private Boolean startJob(QueueItem__c qi) {

		Boolean success = true;

		try {

			// instantiate the class from the QueueItem
			System.debug('Attempting to start batch, class name: ' + qi.Class__c);
			Type t = Type.forName(qi.Class__c);
			if (t == null) {
				t = Type.forName('', qi.Class__c);
			}
			if (t == null) {
				throw new QueueItemException('Unable to find class: ' + qi.Class__c);
			}			
			LtcQueueItemRunnableInterface qiRunner = (LtcQueueItemRunnableInterface) t.newInstance();

			// initialize the batch with any parameters found in the QueueItem record
			qiRunner.initFromQueueItem(qi);

			// cast it to Database.Batchable<sObject> so it can be executed by the system 
			Database.Batchable<sObject> batch = (Database.Batchable<sObject>) qiRunner;

			// find out what scope size to use
			Integer scopeSize = qi.ScopeSize__c != null
				? Integer.valueOf(qi.ScopeSize__c)
				: LtcQueueItemRunner.DEFAULT_SCOPE_SIZE;

			// schedule the batch and save the Id
			Id batchId = Database.executeBatch(batch, scopeSize);

			// update the QueueItem record
			qi.Started__c = Datetime.now();
			qi.Status__c = LtcQueueItemAbstractBatchable.status.STARTED.name();
			qi.BatchId__c = batchId;

		} catch (Exception e) {

			// update the QueueItem record
			success = false;		
			qi.Status__c = LtcQueueItemAbstractBatchable.status.FAILED.name();
			String msg = qi.Message__c = e.getTypeName() + ': ' + e.getMessage() + '\n' + e.getStackTraceString();
			if (qi.Message__c == null || qi.Message__c.trim() == '') {
				qi.Message__c = msg;
			} else {
				qi.Message__c += '\n\n' + msg;
			}

		}

		return success;		
	}

	public interface LtcQueueItemRunnableInterface {
		void initFromQueueItem(QueueItem__c qi);
	}

	public class QueueItemException extends Exception {}
}