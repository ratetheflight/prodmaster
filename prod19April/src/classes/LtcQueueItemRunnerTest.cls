/**
 * @author (s)    :	Maarten Lapère, Mees Witteman
 * @description   : Test class for LtcQueueItemRunner
 *
 * @log: 	2015/04/13: version 1.0
 * @log: 	2016/08/25: version 2.0 removed dependency on FoxServiceSchedulable
 */

@isTest
public with sharing class LtcQueueItemRunnerTest {

	public static testMethod void testLtcQueueItemRunner() {
		Time midnight = Time.newInstance(0, 0, 0, 0);
        Datetime tomorrow = Datetime.newInstance(Date.today().addDays(1), midnight);
		
		List<QueueItem__c> qis = new List<QueueItem__c>();
		qis.add(new QueueItem__c(
            Class__c = 'LtcPisaBatch',
            Parameters__c = 'prm',
            ScopeSize__c = 1,            
            Run_Before__c = tomorrow
        ));
		qis.add(new QueueItem__c(
            Class__c = 'LtcPisaBatch',
            Parameters__c = 'prm',
            ScopeSize__c = 1,            
            Run_Before__c = tomorrow
        ));
		qis.add(new QueueItem__c(
            Class__c = 'LtcPisaBatch',
            Parameters__c = 'prm',
            ScopeSize__c = 1,            
            Run_Before__c = tomorrow
        ));
		qis.add(new QueueItem__c(
            Class__c = 'LtcPisaBatch',
            Parameters__c = 'prm',
            ScopeSize__c = 1,            
            Run_Before__c = tomorrow
        ));
		qis.add(new QueueItem__c(
            Class__c = 'LtcPisaBatch',
            Parameters__c = 'prm',
            ScopeSize__c = 1,            
            Run_Before__c = tomorrow
        ));
		insert qis;

		QueueItem__c[] qItems = [
			SELECT Id, Status__c, Started__c, Run_Before__c, Class__c
			FROM QueueItem__c
		];

		// let's set one of them to 'STARTED' to fake a batch that never finished
		qItems[0].Status__c = LtcQueueItemAbstractBatchable.status.STARTED.name();
		qItems[0].Started__c = System.now();
		Id neverStarted = qItems[0].Id;

		// let's give one of them an expired run before to fake a batch that didn't execute on time
		qItems[1].Run_Before__c = System.now().addDays(-1);
		Id expired = qItems[1].Id;

		// let's give one of them an invalid class name
		qItems[2].Class__c = 'some invalid class name';
		Id invalidClass = qItems[2].Id;

		// the other ones should succeed
		Id successful = qItems[3].Id;
		
		update qItems;

		(new LtcQueueItemRunner()).execute(null);
		Test.startTest();
		Test.stopTest();

		// assert that all QueueItems have been updated correctly
		for (QueueItem__c qi : [SELECT Id, Status__c, Started__c, Finished__c FROM QueueItem__c]) {
			if (qi.Id == neverStarted || qi.Id == expired || qi.Id == invalidClass) {
				System.assertEquals(LtcQueueItemAbstractBatchable.status.FAILED.name(), qi.Status__c);
				System.assertEquals(null, qi.Finished__c);
				if (qi.Id == neverStarted) {
					System.assertNotEquals(null, qi.Started__c);
				} else {
					System.assertEquals(null, qi.Started__c);
				}
			} else {
				System.assertEquals(LtcQueueItemAbstractBatchable.status.FINISHED.name(), qi.Status__c);
				System.assertNotEquals(null, qi.Finished__c);
				System.assertNotEquals(null, qi.Started__c);
			}
		}
	}
}