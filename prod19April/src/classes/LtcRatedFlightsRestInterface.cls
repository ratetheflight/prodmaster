/**                     
 * @author (s)      : Mees Witteman
 * @updatedBy       : Ashok Royal 
 * @description     : Rest API interface: return a list of flightnumbers that might have been rated grouped by parts of the world.
                      Receiving error : "System.LimitException: Too many query rows : 50001" while try to query all rated flight numbers 
                      from ratings table because of more than 50000 records residing in rating table. Since we dont have option to
                      resolve it and its salesforce.com governor limit. We decided to place temporary solution to fix this issue 
                      by fetching all flightNumbers from Monitor Flight table since its unique table storing flightNumbes with languages.
 *@required Action  : Need to find out permanent solution to fix the "System.LimitException: Too many query rows : 50001" on ratings table.                     
 *@log:   17OCT2014 : version 1.0                     
 *@log:   27SEP2016 : version 2.0 updated with new method name (getContinentGroupedFlightNumbers) and referenced getMtrFlightNumbers method from LtcRating class.
 */
@RestResource(urlMapping='/Ratings/Flights/*')
global class LtcRatedFlightsRestInterface {
/* 
{"filters" :
    [{"label" : "klmmj.flights.europe",
    "allFlightLabel" : "klmmj.flights.alleuropeanflights",
    "groupValue" : "KL1",
    "flightNumbers" : [
                   "KL1111",
                   "KL1233",
                   "KL1234"]
    },
        
        ... more of the same kind ...
         
    {"label" : "klmmj.flights.all",
     "allFlightLabel" : "klmmj.flights.allflights",
     "groupValue" : "KL",
     "flightNumbers" : null}
  ]} */
    @HttpGet
    global static String getContinentGroupedFlightNumbers() {
        String response = '{"filters" : [';
        try {
            LtcRating ltcRating = new LtcRating();
            List<String> flights = new List<String>(ltcRating.getMtrFlightNumbers());
            flights.sort();
            response = response + createFilter('europe', 'alleuropeanflights', 'KL1', getFormattedFlightsList('KL1', flights)) + ',';
            response = response + createFilter('asia', 'allasianflights', 'KL08' , getFormattedFlightsList('KL08', flights)) + ',';
            response = response + createFilter('africa', 'allafricanflights', 'KL05', getFormattedFlightsList('KL05', flights)) + ',';
            response = response + createFilter('middleeast', 'allmiddleeasternflights', 'KL04', getFormattedFlightsList('KL04', flights)) + ',';
            response = response + createFilter('northamerica', 'allnorthamericanflights', 'KL06', getFormattedFlightsList('KL06', flights)) + ',';
            response = response + createFilter('centralsouthamerica', 'allcentralsouthamericanflights', 'KL07', getFormattedFlightsList('KL07', flights)) + ',';
            response = response + createFilter('all', 'allflights', 'KL', null);
        } catch (Exception ex) {
            if (LtcUtil.isDevOrg() || Test.isRunningTest()) {
                response = ex.getMessage() + ' ' + ex.getStackTraceString();    
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        response = response + ']}'; 
        return response;
    }
    //klmmj.flights.otherflights
    private static String createFilter(String label, String allLabel, String groupValue, String flights) {
        String result = '{"label" : "klmmj.flights.' + label +'",'
                + '"allFlightLabel" : "klmmj.flights.' + allLabel + '",'
                + '"groupValue" : "' + groupValue + '",'
                + '"flightNumbers" :'  + flights + '}';
        return result;
    }
    
    // example return '["KL1234", "KL1444", "KL1555"]';
    private static String getFormattedFlightsList(String flightPrefix, List<String> allFlightNumbers) {
        String result = null;
        List<String> flightNumbers = new List<String>();
        for (String fn : allFlightNumbers) {
            System.debug(LoggingLevel.INFO, 'flightPrefix=' + flightPrefix + ' fn=' + fn);
            if (fn != null) {
                if (flightPrefix != null && fn.startsWith(flightPrefix)
                        || (flightPrefix.startsWith('KL1') && fn.startsWith('KL09'))) {
                    flightNumbers.add(fn);
                }
            }
        }
        
        if (!flightNumbers.isEmpty()) {
            result = '['; 
            for (String flightNumber : flightNumbers) {
                result += '"' + flightNumber + '", ';
            }
            result = result.substring(0, result.length() - 2);
            result +=  ']';
        }
        return result;
    }

}