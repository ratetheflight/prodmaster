/**
 * @author (s)      : Mees Witteman
 * @updatedBy       : Ashok Royal
 * @description     : Test class to test the LtcRatedFlightsRestInterface class
 */
@isTest
public with sharing class LtcRatedFlightsRestInterfaceTest {

    static testMethod void ratedFlightsFoundTest() {
        Date yesterdate = Date.today().addDays(-1);
        Monitor_Flight__c monFlight = new Monitor_Flight__c();
        monFlight.Flight_Number__c = 'KL1234';
        insert monFlight;

        Flight__c fl = new Flight__c();
        fl.flight_number__c = 'KL1234';
        fl.scheduled_departure_date__c = yesterdate;
        fl.currentleg__c = 0;
        insert fl;
        
        Leg__c leg = new Leg__c();
        leg.flight__c = fl.id;
        leg.scheduleddeparturedate__c = yesterdate;
        leg.scheduledarrivaldate__c = yesterdate;
        insert leg;
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        RestContext.request = req;
        RestContext.response = res;

        String yy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        LtcRatingPostModel.Seat seat = new LtcRatingPostModel.Seat('1A');
        LtcRatingPostModel.Passenger passenger = new LtcRatingPostModel.Passenger('Jan','Jansen', seat,'C', 'pietje@puk.nl', 'true');
        LtcRatingPostModel.Flight flight = new LtcRatingPostModel.Flight('KL1234', yy, 'ORG', 'DST');
        
        LtcRating ltcRating = new LtcRating();
        ltcRating.setRating('4', 'p', 'nek', 'true', 'piet', 'puk', 'pietje@puk.nl', 'kl1234', '11a', 'C', yy,'ORG', 'DST', 'nl_NL', null, '', '' ,'');
        
        String result =  LtcRatedFlightsRestInterface.getContinentGroupedFlightNumbers(); 
        System.debug(LoggingLevel.INFO, 'getRatedFlightNumbers result=' + result);
        
        System.assertEquals('{"filters" : ['
            + '{"label" : "klmmj.flights.europe","allFlightLabel" : "klmmj.flights.alleuropeanflights","groupValue" : "KL1","flightNumbers" :["KL1234"]},'
            + '{"label" : "klmmj.flights.asia","allFlightLabel" : "klmmj.flights.allasianflights","groupValue" : "KL08","flightNumbers" :null},'
            + '{"label" : "klmmj.flights.africa","allFlightLabel" : "klmmj.flights.allafricanflights","groupValue" : "KL05","flightNumbers" :null},'
            + '{"label" : "klmmj.flights.middleeast","allFlightLabel" : "klmmj.flights.allmiddleeasternflights","groupValue" : "KL04","flightNumbers" :null},'
            + '{"label" : "klmmj.flights.northamerica","allFlightLabel" : "klmmj.flights.allnorthamericanflights","groupValue" : "KL06","flightNumbers" :null},'
            + '{"label" : "klmmj.flights.centralsouthamerica","allFlightLabel" : "klmmj.flights.allcentralsouthamericanflights","groupValue" : "KL07","flightNumbers" :null},'
            + '{"label" : "klmmj.flights.all","allFlightLabel" : "klmmj.flights.allflights","groupValue" : "KL","flightNumbers" :null}]}'
          , result);
        
    }
}