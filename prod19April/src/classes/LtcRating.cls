/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman, Satheeshkumar Subramani,Ataullah Khan
 * @description     : Frontend class to hold the business logic before storing it in the Rating object
 * @log: 13MAR2014  : version 1.0
 * @log: 27AUG2015  : version 1.1 added img url fields
 * @log: 12APR2016  : version 1.2 improved sort of flight statistics
 * @log: 27SEP2016  : version 2.0
 *                    created new method(getMtrFlightNumbers) to fetch all flight numbers from Monitor Flight table;
 *                    added search for org, dest, country and language
 * @log: 18NOV2016  : version 2.1 
 *                    added logic to populate origin and destination country to ratings table based on airport codes.
 */
global class LtcRating {
    LtcCrypto ltcc;

    public LtcRating() {
        ltcc = new LtcCrypto();
    }

    /**
     * set rating by Json Rating Post format
     **/
    public String setRating(LtcRatingPostModel.Rating rating, String acceptLanguage, String clientId, String host, String channel) {
        return setRating(rating.rating, rating.positiveComment, rating.negativeComment, rating.isPublished,
                rating.passenger.firstName, rating.passenger.familyName, rating.passenger.email, rating.flight.flightNumber, rating.passenger.seat.seatNumber,
                rating.passenger.cabinCode, rating.flight.travelDate, rating.flight.origin, rating.flight.destination, acceptLanguage, rating.correlationId, clientId, host, channel);
    }

    /**
     * Set rating direct by SFDC fields
     * @return the id of the new created Rating__c object
     **/
    public String setRating(String ratingNum, String positiveFeedback, String negativeFeedBack, String isPublished,
            String firstName, String familyName, String email, String flightNumber, String seatNumber, String cabinCode,
            String travelDate, String origin, String destination, String acceptLanguage, String corrId, String clientId, String host, String channel) {
        
        Double ratingNumber = Double.valueof(ratingNum);
        Date trDate = Date.valueof(travelDate);
        flightNumber = LtcUtil.cleanFlightNumber(flightNumber);
        Boolean pub = 'TRUE'.equalsIgnoreCase(isPublished);
        Id flightId;
        Id flightDateId;
        Flight__c flight;
        Flight_Date__c flight_Date;
        Map<String,String> airportToCountryMap = new Map<String,String>();
        Map<String,String> KeyToCountryMap = new Map<String,String>();
        String language = LtcUtil.getLanguage(acceptLanguage);
       
        if (seatNumber != null) {
            seatNumber = seatNumber.toLowerCase();
        }

        if (cabinCode != null) {
            cabinCode = cabinCode.toUpperCase();
        }
        //added by Ata to find the origin/destination country 
        if (origin != null && destination != null){
            List<String> airportList = new List<String>();
            airportList.add(origin);
            airportList.add(destination);
            List<String> countrycodes = new List<String>();
            //get airport records
            List<LtcAirport__c> airports = [Select id,country__c,airport_code__c from LtcAirport__c where airport_code__c IN:airportList];
            
            if (airports != null && airports.size() > 0){
                for (LtcAirport__c ap : airports){
                    countrycodes.add(ap.country__c);
                    airportToCountryMap.put(ap.airport_code__c,ap.country__c);                  
                }
                
                List<Translation__c> transList = [Select id,translation_Key__c,translated_text__c,language__c from Translation__c where translation_Key__c IN: countrycodes AND language__c =:language ];   
                 
                if (transList != null && transList.size() > 0)
                    for (Translation__c tr: transList){
                        KeyToCountryMap.put(tr.translation_Key__c,tr.translated_text__c);   
                    }   
            }
        }
        //Get or create flight information
        List<Flight__c> flights = [select f.Id, f.Flight_Number__c from Flight__c f where f.Flight_Number__c =: flightNumber and f.Scheduled_Departure_Date__c =: trDate];
        
        if (flights != null && !flights.isEmpty() && flights[0] != null) {
            flightId = flights[0].Id;
        } else {
            flight = new Flight__c(Flight_Number__c = flightNumber, Scheduled_Departure_Date__c = trDate);
            insert flight;
            flightId = flight.Id;
        }

        //Get or create flight date
        List<Flight_Date__c> flightDates = [select f.Id, f.Full_Date__c from Flight_Date__c f where f.Full_Date__c =: trDate];
        
        if (flightDates != null && !flightDates.isEmpty() && flightDates[0] != null) {
            flightDateId = flightDates[0].Id;
        } else {
            flight_Date = new Flight_Date__c(Full_Date__c = trDate);
            insert flight_Date;
            flightDateId = flight_Date.Id;
        }

        // Store the passenger
        Passenger__c passenger = new Passenger__c(
                First_Name__c = firstName,
                Family_Name__c = familyName,
                Email__c = email,
                AllowEmailContact__c = !String.isBlank(email)
        );
        insert passenger;

        // Determine language and country
        String country = LtcUtil.getCountry(acceptLanguage);
        //String language = LtcUtil.getLanguage(acceptLanguage);
        Boolean hasComments = (positiveFeedback != null && !''.equals(positiveFeedback))
                || (negativeFeedBack != null && !''.equals(negativeFeedBack));
        String originCountry = airportToCountryMap.isEmpty() || KeyToCountryMap.isempty() ? '':  KeyToCountryMap.get(airportToCountryMap.get(origin));
        String destinCountry = airportToCountryMap.isEmpty() || KeyToCountryMap.isempty()? '' : KeyToCountryMap.get(airportToCountryMap.get(destination));
        
        // Store the Rating with links to the passenger and the flight information
        Rating__c rating = new Rating__c(
                Flight_Info__c = flightId,
                Flight_Date__c = flightDateId,
                Origin_Country__c = originCountry,
                Destination_Country__c = destinCountry,
                Origin__c = origin,
                Destination__c = destination,
                Passenger__c = passenger.Id,
                Negative_comments__c = negativeFeedBack,
                Positive_comments__c = positiveFeedback,
                Has_Comments__c = hasComments,
                Publish__c = pub,
                Rating_Number__c = ratingNumber,
                Seat_Number__c = seatNumber,
                Cabin_Code__c = cabinCode,
                Country__c = country,
                Language__c = language,
				CorrelationId__c = corrId,
				ClientId__c = clientId,
				Host__c = host,
				Channel__c  = channel
        );
        insert rating;

        return ltcc.encodeText(rating.Id);
    }

    /**
     * Update the isPublished parameter by Rating Id
     **/
    public String updateRating(LtcRatingPutModel.Rating rating) {
        Boolean pub = 'TRUE'.equalsIgnoreCase(rating.isPublished);
        Rating__c rat = new Rating__c(Id = ltcc.decodeText(rating.ratingId), Publish__c = pub);
        update rat;
        return ltcc.encodeText(rat.Id);
    }

    /**
     * Delete a rating by Rating Id
     **/
    public Rating__c deleteRating(LtcRatingPutModel.Rating rating) {
        Rating__c rat = null;

        List<Rating__c> ratingList = [
                select r.Id, r.CaseId__c
                from Rating__c r
                where r.Id =: (ltcc.decodeText(rating.ratingId))
        ];
        if (ratingList != null && !ratingList.isEmpty()) {
            rat = ratingList[0];
            if (rat.CaseId__c != null) {
                Case c = new Case(Id = rat.CaseId__c);
                System.debug(LoggingLevel.INFO, 'about to delete case with id:' + c.Id);
                delete c;
            }

            System.debug(LoggingLevel.INFO, 'about to delete rating with id:' + rat.Id);
            delete rat;
        }
        return rat;
    }

    /**
     * Get all flight ratings in Flight Get format
     **/
    public LtcRatingGetModel.Result getRatings(String flightNumber, String startDate, String endDate,
            List<String> ratingIds, Integer limitTo, Integer offset, String host,Boolean crew) {

        // defaults for pagination
        if (limitTo == null) {
            limitTo = 15;
        }
        if (offset == null) {
            offset = 0;
        }

        Date start = Date.valueof(startDate);
        Date ending = Date.valueof(endDate);
        List<Rating__c> ratingList = getFlightRatings(flightNumber, start, ending,crew);

        return new LtcRatingGetModel.Result(flightNumber, startDate, endDate,
                ratingIds, ratingList, limitTo, offset, host);
    }

    /**
     * Get all flight ratings in SFDC format, string dates
     **/
    public List<Rating__c> getFlightRatings(String flightNumber, String startDate, String endDate,Boolean crew) {
        Date start = Date.valueof(startDate);
        Date ending = Date.valueof(endDate);
        return getFlightRatings(flightNumber, start, ending,crew);
    }

    /**
     * Get all flight ratings in SFDC format, real dates, flightNumbers may be partial filled and used for masks
     **/
    public List<Rating__c> getFlightRatings(String flightNumber, Date startDate, Date endDate,Boolean crew) {
        List<Rating__c> ratingList;
        List<Rating__c> tempRatings = new List<Rating__c>();
        if ('KL1'.equals(flightNumber)) {
            ratingList = [
                    select
                            r.Rating_Number__c,
                            r.Id,
                            r.Positive_comments__c,
                            r.Seat_Number__c,
                            r.Cabin_Code__c,
                            r.Flight_Date__r.Full_Date__c,
                            r.Flight_Date__c,
                            r.Flight_Info__r.Flight_Number__c,
                            r.Flight_Info__r.Scheduled_Departure_Date__c,
                            r.Flight_Info__c,
                            r.Passenger__r.First_Name__c,
                            r.Passenger__r.Family_Name__c,
                            r.Passenger__r.Hidden_Name__c,
                            r.Passenger__c,
                            r.KLM_Reply__c,
                            r.Negative_comments__c,
                            r.Publish__c,
                            r.Country__c,
                            r.Language__c,
                            r.Hidden_Comments__c,
                            r.Hidden_Seat_Number__c,
                            r.CreatedDate
                    from Rating__c r
                    where  (r.Flight_Info__r.Flight_Number__c like: 'KL1%'
                    or r.Flight_Info__r.Flight_Number__c like: 'KL09%')
                    and r.Flight_Info__r.Scheduled_Departure_Date__c >=: startDate
                    and r.Flight_Info__r.Scheduled_Departure_Date__c <=: endDate
                    order by r.CreatedDate desc
            ];
        }  else {
            ratingList = [
                    select
                            r.Rating_Number__c,
                            r.Id,
                            r.Positive_comments__c,
                            r.Seat_Number__c,
                            r.Cabin_Code__c,
                            r.Flight_Date__r.Full_Date__c,
                            r.Flight_Date__c,
                            r.Flight_Info__r.Flight_Number__c,
                            r.Flight_Info__r.Scheduled_Departure_Date__c,
                            r.Flight_Info__c,
                            r.Passenger__r.First_Name__c,
                            r.Passenger__r.Family_Name__c,
                            r.Passenger__r.Hidden_Name__c,
                            r.Passenger__c,
                            r.KLM_Reply__c,
                            r.Negative_comments__c,
                            r.Publish__c,
                            r.Country__c,
                            r.Language__c,
                            r.Hidden_Comments__c,
                            r.Hidden_Seat_Number__c,
                            r.CreatedDate
                    from Rating__c r
                    where  r.Flight_Info__r.Flight_Number__c like: flightNumber + '%'
                    and r.Flight_Info__r.Scheduled_Departure_Date__c >=: startDate
                    and r.Flight_Info__r.Scheduled_Departure_Date__c <=: endDate
                    order by r.CreatedDate desc
            ];
        }
        System.debug('1st rating:::'+ratingList);
        //Hide if needed and convert Null to ""
        for (Rating__c rat : ratingList) {
            cleanupRating(rat);
            if(crew != null && crew == true){
                String pos = rat.Positive_comments__c.toUpperCase();
                String neg = rat.Negative_comments__c.toUpperCase();
                if(pos.contains('CREW') || pos.contains('PURSER') || pos.contains('ATTENDANT') || pos.contains('STEWARDESS')
                    || neg.contains('CREW') || neg.contains('PURSER') || neg.contains('ATTENDANT') || neg.contains('STEWARDESS')){
                        tempRatings.add(rat);   
                }
            }       
        }
        if( crew == true){
            return tempRatings;
        } else{
            return ratingList;
        }
    }

    public static void cleanupRating(Rating__c rat) {
        if (rat.hidden_comments__c == true || rat.Positive_comments__c == null) {
            rat.Positive_comments__c = '';
        }
        if (rat.hidden_comments__c == true || rat.Negative_comments__c == null) {
            rat.Negative_comments__c = '';
        }
        if (rat.hidden_comments__c == true || rat.KLM_Reply__c == null) {
            rat.KLM_Reply__c = '';
        }
        if (rat.Passenger__r.First_Name__c == null) {
            rat.Passenger__r.First_Name__c = '';
        }
        if (rat.Passenger__r.Family_Name__c == null) {
            rat.Passenger__r.Family_Name__c = '';
        }
        if (rat.hidden_seat_number__c == true || rat.Seat_Number__c == null) {
            rat.Seat_Number__c = '';
        }
    }

    /**
     * Get flight ratings with statistics for multiple flight numbers
     **/
    public LtcRatingGetModel.Result getRatings(List<String> flightNumbers, String startDate, String endDate,
            List<String> ratingIds, Integer limitTo, Integer offset, String host,Boolean crew) {

        // defaults for pagination
        if (limitTo == null) {
            limitTo = 15;
        }
        if (offset == null) {
            offset = 0;
        }
        List<Rating__c> tempRatings = new List<Rating__c>();
        Date start = Date.valueof(startDate);
        Date ending = Date.valueof(endDate);

        List<Rating__c> ratingList = [
                select
                        r.Rating_Number__c,
                        r.Id,
                        r.Positive_comments__c,
                        r.Seat_Number__c,
                        r.Cabin_Code__c,
                        r.Flight_Date__r.Full_Date__c,
                        r.Flight_Date__c,
                        r.Flight_Info__r.Flight_Number__c,
                        r.Flight_Info__r.Scheduled_Departure_Date__c,
                        r.Flight_Info__c,
                        r.Passenger__r.First_Name__c,
                        r.Passenger__r.Family_Name__c,
                        r.Passenger__r.Hidden_Name__c,
                        r.Passenger__c,
                        r.KLM_Reply__c,
                        r.Negative_comments__c,
                        r.Publish__c,
                        r.Country__c,
                        r.Language__c,
                        r.Hidden_Comments__c,
                        r.Hidden_Seat_Number__c,
                        r.CreatedDate
                from Rating__c r
                where  r.Flight_Info__r.Flight_Number__c in : flightNumbers
                and r.Flight_Date__r.Full_Date__c >=: start
                and r.Flight_Date__r.Full_Date__c <=: ending
                order by r.CreatedDate desc
        ];
        System.debug('2nd rating:::'+ratingList);
        for (Rating__c rat : ratingList) {
            cleanupRating(rat);
            if(crew != null && crew == true){
                String pos = rat.Positive_comments__c.toUpperCase();
                String neg = rat.Negative_comments__c.toUpperCase();
                if(pos.contains('CREW') || pos.contains('PURSER') || pos.contains('ATTENDANT') || pos.contains('STEWARDESS')
                    || neg.contains('CREW') || neg.contains('PURSER') || neg.contains('ATTENDANT') || neg.contains('STEWARDESS')){
                        tempRatings.add(rat);   
                }
            }   
        }
        String flightNumber = '';
        if (ratingList.size() > 0) {
            flightNumber = ratingList.get(0).Flight_Info__r.Flight_Number__c;
        }
        if (crew == true){
            return new LtcRatingGetModel.Result(flightNumber, startDate, endDate, ratingIds, tempRatings, limitTo, offset, host);
        }else{
            return new LtcRatingGetModel.Result(flightNumber, startDate, endDate, ratingIds, ratingList, limitTo, offset, host);
        }
    }

    /**
     * Get ratings with statistics for org, dest, country, language
     **/
    public LtcRatingGetModel.Result getRatings(String origin, String destination, String country, String language,
            String startDate, String endDate, List<String> ratingIds, Integer limitTo, Integer offset, String host,Boolean crew) {

        // defaults for pagination
        if (limitTo == null) {
            limitTo = 15;
        }
        if (offset == null) {
            offset = 0;
        }
        List<Rating__c> tempRatings = new List<Rating__c>();
        Date start = Date.valueof(startDate);
        Date ending = Date.valueof(endDate);

        String query = 'select r.Rating_Number__c, ';
        query += ' r.Id, ';
        query += ' r.Positive_comments__c, ';
        query += ' r.Seat_Number__c, ';
        query += ' r.Cabin_Code__c, ';
        query += ' r.Flight_Date__r.Full_Date__c, ';
        query += ' r.Flight_Date__c, ';
        query += ' r.Flight_Info__r.Flight_Number__c, ';
        query += ' r.Flight_Info__r.Scheduled_Departure_Date__c, ';
        query += ' r.Flight_Info__c,  ';
        query += ' r.Passenger__r.First_Name__c, ';
        query += ' r.Passenger__r.Family_Name__c,  ';
        query += ' r.Passenger__r.Hidden_Name__c, ';
        query += ' r.Passenger__c,  ';
        query += ' r.KLM_Reply__c,  ';
        query += ' r.Negative_comments__c, ';
        query += ' r.Publish__c,  ';
        query += ' r.Country__c, ';
        query += ' r.Language__c,  ';
        query += ' r.Hidden_Comments__c, ';
        query += ' r.Hidden_Seat_Number__c,  ';
        query += ' r.CreatedDate  ';
        query += ' from Rating__c r ';
        query += ' where r.Flight_Date__r.Full_Date__c >= :start ';
        query += ' and r.Flight_Date__r.Full_Date__c <= :ending ';

        String usedCriteria = '';
        if (!String.isEmpty(origin)) {
            origin = String.escapeSingleQuotes(origin);
            query += ' and r.Origin__c = :origin ';
            usedCriteria += ' origin=' + origin;
        }
        if (!String.isEmpty(destination)) {
            destination = String.escapeSingleQuotes(destination);
            query += ' and r.Destination__c = :destination ';
            usedCriteria += ' destination=' + destination;
        }
        if (!String.isEmpty(country)) {
            country = String.escapeSingleQuotes(country);
            query += ' and r.Country__c = :country ';
            usedCriteria += ' country=' + country;
        }
        if (!String.isEmpty(language)) {
            language = String.escapeSingleQuotes(language);
            query += ' and r.Language__c = :language ';
            usedCriteria += ' language=' + language;
        }
        query += ' order by r.CreatedDate desc ';

        List<Rating__c> ratingList = Database.query(query);
        System.debug('3rd rating:::'+ratingList);
        for (Rating__c rat : ratingList) {
            cleanupRating(rat);
            if(crew != null && crew == true){
                String pos = rat.Positive_comments__c.toUpperCase();
                String neg = rat.Negative_comments__c.toUpperCase();
                if(pos.contains('CREW') || pos.contains('PURSER') || pos.contains('ATTENDANT') || pos.contains('STEWARDESS')
                    || neg.contains('CREW') || neg.contains('PURSER') || neg.contains('ATTENDANT') || neg.contains('STEWARDESS')){
                        tempRatings.add(rat);   
                }
            }   
        }
        if(crew == true){
            return new LtcRatingGetModel.Result(usedCriteria, startDate, endDate, ratingIds, tempRatings, limitTo, offset, host);
        }
        else{
            return new LtcRatingGetModel.Result(usedCriteria, startDate, endDate, ratingIds, ratingList, limitTo, offset, host);
        }
    }


    /**
     * Get all rated flightNumbers
    public Set<String> getRatedFlightNumbers() {
        List<Rating__c> ratingList = [
            select r.Flight_Number__c
            from Rating__c r
            order by r.Flight_Number__c
            asc
        ];
        System.debug(ratingList);
        Set<String> uniqueFlightNumbers = new Set<String>();
        for (Rating__c rating : ratingList) {
            uniqueFlightNumbers.add(rating.Flight_Number__c);
        }
        return uniqueFlightNumbers;
    }
     **/
    public Set<String> getRatedFlightNumbers() {
        Set<String> uniqueFlightNumbers = new Set<String>();
        for (Rating__c[] ratingList : [
                select r.Flight_Number__c
                from Rating__c r
        ]) {
            for (Rating__c rating : ratingList) {
                uniqueFlightNumbers.add(rating.Flight_Number__c);
            }
        }
        return uniqueFlightNumbers;
    }

    /**
     * Get all flightNumbers from Monitor Flight Table
     **/
    public Set<String> getMtrFlightNumbers() {
        Set<String> flightNumbers = new Set<String>();
        for (Monitor_Flight__c[] mtrFlights : [
                select Flight_Number__c
                from Monitor_Flight__c
        ])
            for (Monitor_Flight__c mtrFlight : mtrFlights) {
                flightNumbers.add(mtrFlight.Flight_Number__c);
            }
        System.debug('flightNumbers.size=' + flightNumbers.size());
        return flightNumbers;
    }


    /**
     * Get all flight ratings in SFDC format with matching flightnumber for 365 days from today
     * No sorting on language and country
     **/
    public List<Rating__c> getFlightRatingsForStarInterface(String flightNumber, Date travelDate) {
        Date fromDate = Date.today().addDays(-365);

        List<Rating__c> resultList = selectRatingsFromDate(flightNumber, fromDate);
        System.debug(LoggingLevel.INFO, 'resultList.size()=' + resultList.size());

        //Hide if needed and convert Null to ""
        for (Rating__c rat : resultList) {
            cleanupRating(rat);
        }
        return resultList;
    }

    private List<Rating__c> selectRatingsFromDate(String flightNumber, Date fromDate) {
        System.debug(LoggingLevel.DEBUG, 'flNo=' + flightNumber + ' fromDate=' + fromDate);
        return [
                select
                        r.Rating_Number__c,
                        r.Id,
                        r.Positive_comments__c,
                        r.Seat_Number__c,
                        r.Cabin_Code__c,
                        r.Flight_Date__r.Full_Date__c,
                        r.Flight_Date__c,
                        r.Flight_Info__r.Flight_Number__c,
                        r.Flight_Info__r.Scheduled_Departure_Date__c,
                        r.Flight_Info__c,
                        r.Passenger__r.First_Name__c,
                        r.Passenger__r.Family_Name__c,
                        r.Passenger__r.Hidden_Name__c,
                        r.Passenger__c,
                        r.KLM_Reply__c,
                        r.Negative_comments__c,
                        r.Publish__c,
                        r.Country__c,
                        r.Language__c,
                        r.Hidden_Comments__c,
                        r.Hidden_Seat_Number__c,
                        r.CreatedDate
                from Rating__c r
                where  r.Flight_Info__r.Flight_Number__c =: flightNumber
                and r.Flight_Date__r.Full_Date__c >=: fromDate
                order by r.CreatedDate desc
        ];
    }

    /**
     * Get all flight ratings in SFDC format with matching flightnumber for 365 days from today
     * Sorting on language, country, english and then ... the rest
     **/
    public List<Rating__c> getSortedFlightRatings(String flightNumber, Date travelDate, String language, String country) {
        Date fromDate = Date.today().addDays(-365);
        System.debug(LoggingLevel.DEBUG, 'flNo=' + flightNumber + ' fromDate=' + fromDate + ' lang=' + language + ' country=' + country);

        List<Rating__c> resultList = selectRatingsForLanguagePlusCountry(flightNumber, fromDate, language, country);
        resultList.addAll(selectRatingsForLanguage(flightNumber, fromDate, language, country));
        resultList.addAll(selectRemainingEnglishRatings(flightNumber, fromDate, language, country));
        resultList.addAll(selectRemainingNonEnglishRatings(flightNumber, fromDate, language, country));

        //Hide if needed and convert Null to ""
        for (Rating__c rat : resultList) {
            cleanupRating(rat);
        }
        return resultList;
    }

    private List<Rating__c> selectRatingsForLanguagePlusCountry(String flightNumber, Date fromDate, String language, String country) {
        List<Rating__c> result = [
                select
                        r.Rating_Number__c,
                        r.Id,
                        r.Positive_comments__c,
                        r.Seat_Number__c,
                        r.Cabin_Code__c,
                        r.Flight_Date__r.Full_Date__c,
                        r.Flight_Date__c,
                        r.Flight_Info__r.Flight_Number__c,
                        r.Flight_Info__r.Scheduled_Departure_Date__c,
                        r.Flight_Info__c,
                        r.Passenger__r.First_Name__c,
                        r.Passenger__r.Family_Name__c,
                        r.Passenger__r.Hidden_Name__c,
                        r.Passenger__c,
                        r.KLM_Reply__c,
                        r.Negative_comments__c,
                        r.Publish__c,
                        r.Country__c,
                        r.Language__c,
                        r.Hidden_Comments__c,
                        r.Hidden_Seat_Number__c,
                        r.CreatedDate
                from Rating__c r
                where  r.Flight_Info__r.Flight_Number__c = :flightNumber
                and r.Flight_Date__r.Full_Date__c >= :fromDate
                and r.Language__c = :language
                and r.Country__c = :country
                order by r.Travel_Date__c desc
        ];
        System.debug(LoggingLevel.DEBUG, 'selectRatingsForLanguagePlusCountry result size=' + result.size() + ' lang=' + language + ' country=' + country);
        return result;
    }
    private List<Rating__c> selectRatingsForLanguage(String flightNumber, Date fromDate, String language, String country) {
        List<Rating__c> result = [
                select
                        r.Rating_Number__c,
                        r.Id,
                        r.Positive_comments__c,
                        r.Seat_Number__c,
                        r.Cabin_Code__c,
                        r.Flight_Date__r.Full_Date__c,
                        r.Flight_Date__c,
                        r.Flight_Info__r.Flight_Number__c,
                        r.Flight_Info__r.Scheduled_Departure_Date__c,
                        r.Flight_Info__c,
                        r.Passenger__r.First_Name__c,
                        r.Passenger__r.Family_Name__c,
                        r.Passenger__r.Hidden_Name__c,
                        r.Passenger__c,
                        r.KLM_Reply__c,
                        r.Negative_comments__c,
                        r.Publish__c,
                        r.Country__c,
                        r.Language__c,
                        r.Hidden_Comments__c,
                        r.Hidden_Seat_Number__c,
                        r.CreatedDate
                from Rating__c r
                where  r.Flight_Info__r.Flight_Number__c = :flightNumber
                and r.Flight_Date__r.Full_Date__c >= :fromDate
                and r.Language__c = :language
                and r.Country__c <> :country
                order by r.Travel_Date__c desc
        ];
        System.debug(LoggingLevel.DEBUG, 'selectRatingsForLanguage result size=' + result.size()+ ' lang=' + language );
        return result;
    }


    private List<Rating__c> selectRemainingEnglishRatings(String flightNumber, Date fromDate, String language, String country) {
        List<Rating__c> result = [
                select
                        r.Rating_Number__c,
                        r.Id,
                        r.Positive_comments__c,
                        r.Seat_Number__c,
                        r.Cabin_Code__c,
                        r.Flight_Date__r.Full_Date__c,
                        r.Flight_Date__c,
                        r.Flight_Info__r.Flight_Number__c,
                        r.Flight_Info__r.Scheduled_Departure_Date__c,
                        r.Flight_Info__c,
                        r.Passenger__r.First_Name__c,
                        r.Passenger__r.Family_Name__c,
                        r.Passenger__r.Hidden_Name__c,
                        r.Passenger__c,
                        r.KLM_Reply__c,
                        r.Negative_comments__c,
                        r.Publish__c,
                        r.Country__c,
                        r.Language__c,
                        r.Hidden_Comments__c,
                        r.Hidden_Seat_Number__c,
                        r.CreatedDate
                from Rating__c r
                where  r.Flight_Info__r.Flight_Number__c = :flightNumber
                and r.Flight_Date__r.Full_Date__c >= :fromDate
                and r.Language__c <> :language
                and r.Language__c = 'en'
                order by r.Travel_Date__c desc
        ];
        System.debug(LoggingLevel.DEBUG, 'selectRemainingEnglishRatings result size=' + result.size()+ 'lang = en && lang<>' + language);
        return result;
    }

    private List<Rating__c> selectRemainingNonEnglishRatings(String flightNumber, Date fromDate, String language, String country) {
        List<Rating__c> result = [
                select
                        r.Rating_Number__c,
                        r.Id,
                        r.Positive_comments__c,
                        r.Seat_Number__c,
                        r.Cabin_Code__c,
                        r.Flight_Date__r.Full_Date__c,
                        r.Flight_Date__c,
                        r.Flight_Info__r.Flight_Number__c,
                        r.Flight_Info__r.Scheduled_Departure_Date__c,
                        r.Flight_Info__c,
                        r.Passenger__r.First_Name__c,
                        r.Passenger__r.Family_Name__c,
                        r.Passenger__r.Hidden_Name__c,
                        r.Passenger__c,
                        r.KLM_Reply__c,
                        r.Negative_comments__c,
                        r.Publish__c,
                        r.Country__c,
                        r.Language__c,
                        r.Hidden_Comments__c,
                        r.Hidden_Seat_Number__c,
                        r.CreatedDate
                from Rating__c r
                where r.Flight_Info__r.Flight_Number__c = :flightNumber
                and r.Flight_Date__r.Full_Date__c >= :fromDate
                and r.Language__c <> :language
                and r.Language__c <> 'en'
                order by r.Travel_Date__c desc
        ];
        System.debug(LoggingLevel.DEBUG, 'selectRemainingNonEnglishRatings result size=' + result.size()+ 'lang<>en lang<>' + language);
        return result;
    }

    public Integer countRatings(String flightNumber, Date travelDate) {
        Integer result = 0;
        List<AggregateResult> aggs = [
                SELECT count(Id) nrOfRatings
                FROM Rating__c
                GROUP BY Flight_Info__r.Flight_Number__c, Flight_Info__r.Scheduled_Departure_Date__c
                HAVING Flight_Info__r.Flight_Number__c =: flightNumber
                AND Flight_Info__r.Scheduled_Departure_Date__c =: travelDate
        ];
        for (AggregateResult ar : aggs) {
            result = (Integer) ar.get('nrOfRatings');
        }
        return result;
    }

    public Boolean hasDuplicateRatings(String flightNumber, String travelDate, String ratingNum, String positiveFeedback, String negativeFeedBack,
            String firstName, String familyName, String seatNumber, String cabinCode) {

        firstName = firstName != null ? firstName.trim() : '';
        familyName = familyName != null ? familyName.trim() : '';

        List<Rating__c> ratings = [
                SELECT id,Positive_comments__c,Negative_comments__c
                FROM Rating__c
                WHERE Flight_Info__r.Flight_Number__c =: flightNumber
                AND Flight_Info__r.Scheduled_Departure_Date__c =: Date.valueOf(travelDate)
                AND Rating_Number__c =: Double.valueof(ratingNum)
                AND Passenger__r.First_Name__c =: firstName
                AND Passenger__r.Family_Name__c =: familyName
                AND Seat_Number__C =: seatNumber
                AND Cabin_Code__c =: cabinCode
        ];

        positiveFeedback = positiveFeedback != null ? positiveFeedback.trim() : '';
        negativeFeedBack = negativeFeedBack != null ? negativeFeedBack.trim() : '';
        for (Rating__c rating : ratings) {
            rating.Positive_comments__c = rating.Positive_comments__c != null ? rating.Positive_comments__c.trim() : '';
            rating.Negative_comments__c = rating.Negative_comments__c != null ? rating.Negative_comments__c.trim() : '';

            if (rating.Positive_comments__c.equals(positiveFeedback) && rating.Negative_comments__c.equals(negativeFeedBack)) {
                return true;
            }
        }
        return false;
    }

}