/**                     
 * Rest API interface for retrieving ratings and rating statistic for the star pages on EBT.
 */
@RestResource(urlMapping='/Ratings/FlightStatistics/*')
global class LtcRatingFlightStatisticsRestInterface { 

	/**                     
	 * Get all ratings that are allowed to display for the specified flightnumber and traveldate.
	 */	
	@HttpGet
    global static String searchRatedFlight() {
    	String response = ''; 
		RestResponse res = RestContext.response;
		RestRequest req = RestContext.request;
		String language = 'nl';
		String country = 'NL';
		if (res != null) {
			res.addHeader('Access-Control-Allow-Origin', '*');
		}
        try {
	    	String flightNumber = RestContext.request.params.get('flightNumber');
            String travelDateString = RestContext.request.params.get('travelDate');
            Date travelDate = Date.valueof(travelDateString);
            
            Integer limitTo = 15;
            String limitString  = RestContext.request.params.get('limit');
            if (limitString != null && !''.equals(limitString)) {
            	limitTo = Integer.valueOf(limitString);
            }            
            
            Integer offset = 0;
            String offsetString = RestContext.request.params.get('offset'); 
            if (offsetString != null && !''.equals(offsetString)) {
            	offset = Integer.valueOf(offsetString);
            }

            String acceptLanguage = RestContext.request.headers.get('Accept-Language');
            if (acceptLanguage != null && !''.equals(acceptLanguage.trim())) {
				language = LtcUtil.getLanguage(acceptLanguage);
				country  = LtcUtil.getCountry(acceptLanguage);
            }

	    	LtcRating rating = new LtcRating();
	    	List<Rating__c> flightRatings = rating.getSortedFlightRatings(flightNumber, travelDate, language, country);
			Integer total = flightRatings.size();
	    	
	    	LtcRatingGetModel.Result result = new LtcRatingGetModel.Result(flightNumber, travelDateString, travelDateString, null, flightRatings, limitTo, offset, 'KL');
 			response = '{ "flight": ' + result.toString() + '}';
        } catch (Exception ex) {
 			if (LtcUtil.isDevOrg()) {
 				response = 'statusCode 500 ' + ex.getMessage() + ' ' + ex.getStackTraceString();    
        	}
 			System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
 			res.statusCode = 500;
        }
	   	return response;
    }
    

}