/**                     
 * Rest API interface for retrieving ratings and rating statistic for the star pages on EBT.
 */
@RestResource(urlMapping='/Ratings/FlightsStatistics/*')
global class LtcRatingFlightsStatisticsRestInterface { 

 	/** 
 	 * @Deprecated: 
 	 * this class may be removed when search is using the new WEB Service
 	 *
 	 * Accepts a url parameter called data with flightnumbers and traveldates:
 	 * data=KL1001!2015-02-21@KL1002!2015-02-22@KL1003!2015-02-23
 	 * 
	 * return a list with star rating statistics per flightnumber / traveldate in the result
	 */	   
	@HttpGet
    global static String searchRatedFlights() {
		Map<String, String> params = RestContext.request.params;
		System.debug('data=' + params.get('data'));
		
		String response = '';
	    RestResponse res = RestContext.response;
		if (res != null) {
			res.addHeader('Access-Control-Allow-Origin', '*');
		}
        try {
	    	LtcRatingFlightStatisticsResponseModel responseModel = new LtcRatingFlightStatisticsResponseModel();
			String data = params.get('data');
			System.assert(data != null, 'param data may not be null');
			List<String> flights = data.split('@');
			for (String flight : flights) {
				List<String> flightParts = flight.split('!');
				String flightNumber = flightParts.get(0);
		        String travelDateString = flightParts.get(1);
			    Date travelDate = Date.valueof(travelDateString);
			    
			    System.debug('flightNumber=' + flightNumber + ' travelDateString=' + travelDateString);
		    		
	    		List<Rating__c> flightRatings = null;
	    		if (flightNumber != null && flightNumber.startsWith('KL')) {
			    	LtcRating rating = new LtcRating();
	    			flightRatings = rating.getFlightRatingsForStarInterface(flightNumber, travelDate);
	    		} 
	    
	    		LtcRatingFlightStatisticsResponseModel.Flight responseFlight = 
	    				new LtcRatingFlightStatisticsResponseModel.Flight(flightNumber, travelDateString, flightRatings);
	    		responseModel.flights.add(responseFlight);
			}
			
	    	response = responseModel.toString();

        } catch (Exception ex) {
        	if (LtcUtil.isDevOrg()) {
 				response = ex.getMessage() + ' ' + ex.getStackTraceString();    
        	}
 			System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }

    	return response;
    }

}