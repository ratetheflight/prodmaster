/**
 * @author (s)    : Ata
 * @Date          : 21stNOV2016
 * @description   : This is a one time batch to populate the origin/destination 
 *                  country data in ratings table for all existing data.
 *                  It's a one time batch.
 * @log           : 21st NOV 2016: Developed
 * Call anonymous: Id batjobId = Database.executeBatch(new LtcRatingOriginDestinationCountryBatch(), 200);                 
 */
global class LtcRatingOriginDestinationCountryBatch implements Database.Batchable<sObject>,Database.Stateful {
    
    global List<LtcAirport__c> airportCodes;
    global List<Translation__c> translation;
    global Set<String> countryTranslationKeys;
    global Map<String,String> airportCodeToCountryCodeMap;
    global Map<String,String> translKeyToText;
    
    global LtcRatingOriginDestinationCountryBatch(){
        translation = new List<Translation__c>();
        airportCodes = new List<LtcAirport__c>();
        countryTranslationKeys = new Set<String>();
        airportCodeToCountryCodeMap = new Map<String,String>();
        translKeyToText = new Map<String,String>();
        
        // Get IATA Code from Airport object 
        airportCodes = [Select Name,
                               country__c,
                               airport_code__c 
                               from LtcAirport__c];

        for (LtcAirport__c airportCode :airportCodes) {
            countryTranslationKeys.add(airportCode.country__c);
            airportCodeToCountryCodeMap.put(airportCode.airport_code__c,airportCode.country__c);
        }    
        
        translation = [select id,
                              Translated_Text__c,
                              Translation_Key__c,
                              language__c
                              from Translation__c 
                              where 
                              Translation_Key__c in : countryTranslationKeys AND language__c =: 'en'];
                                       
        for (Translation__c cTrans : translation) { 
            translKeyToText.put(cTrans.Translation_Key__c, cTrans.Translated_Text__c);
        } 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select id, '+
                              'Flight_Number__c, '+
                              'Origin__c, '+
                              'Destination__c, '+
                              'Origin_Country__c, '+
                              'Destination_Country__c '+ 
                              'from Rating__c '+ 
                              'where '+
                              '( Origin_Country__c = NULL OR '+
                              'Destination_Country__c = NULL )'+
                              'AND (Origin__c != NULL OR Destination__c != NULL)';
                               
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Rating__c> allRatings) {
        List<Rating__c> ratingsToUpdate = new List<Rating__c>();
        
        try{
            for (Rating__c r: allRatings){
                Boolean flag = false;
                if (r.origin__c != null){
                    r.Origin_Country__c = translKeyToText.get(airportCodeToCountryCodeMap.get(r.origin__c));
                    flag = true;
                }
                if (r.destination__c != null){
                    r.Destination_Country__c = translKeyToText.get(airportCodeToCountryCodeMap.get(r.destination__c));
                    flag = true;
                }
                if(flag){
                    ratingsToUpdate.add(r);
                }
            }
            
            if (ratingsToUpdate.size() > 0){
                update ratingsToUpdate;
            }
        }
        catch(Exception e){
            System.Debug('Exception occured in batchjob::'+e);
        }       
    }

    global void finish(Database.BatchableContext BC) {
       
    }
    
 }