/**                   
 * @author (s)      : David van 't Hooft, Mees Witteman,Ata
 * @description     : Model class to contain Rating Post objects
 * @log             :  2MAY2014: version 1.0
 * @log             : 21JUL2015: version 1.1
 * @log             : 27AUG2015: version 1.2 added img url properties
 * @log             : 11NOV2016: version 1.3 added input validations(Modified by Ata)
 */ 
global class LtcRatingPostModel {
    public Rating rating { get; set; }
    global class Rating {
        public String rating { get; set; }
        public String positiveComment { get; set; }
        public String positiveImage { get; set; }
        public String positiveImageLarge { get; set; }
        public String negativeComment { get; set; }
        public String negativeImage { get; set; }
        public String negativeImageLarge { get; set; }        
        public String isPublished { get; set; }        
        public String language { get; set; }        
        public String country { get; set; }    
        public String ratingToken {get;set;}
        public String correlationId {get;set;}    
        public LtcRatingPostModel.Flight flight { get; set; }
        public LtcRatingPostModel.Passenger passenger { get; set; }
    
        public Rating(String rating, String positiveComment, String negativeComment, String isPublished, LtcRatingPostModel.Flight flight, LtcRatingPostModel.Passenger passenger, String ratingToken,String correlationId) {
            this.rating = rating;
            this.positiveComment = positiveComment;
            this.negativeComment = negativeComment;
            this.isPublished = isPublished;
            this.flight = flight;
            this.passenger = passenger;
            this.ratingToken = ratingToken;
            this.correlationId = correlationId ;
        }
        
        public Boolean isInValid() {
            if (passenger != null){     
                passenger.FirstName = removeExtraSymbols(passenger.FirstName);
                passenger.FamilyName = removeExtraSymbols(passenger.FamilyName);
            }
            
            Boolean invalid = false;
            if (!('5'.equals(rating)
                    || '4'.equals(rating)
                    || '3'.equals(rating) 
                    || '2'.equals(rating)
                    || '1'.equals(rating))){
                invalid = true; 
            }
                
            if (flight == null 
                    || flight.flightNumber == null
                    || ''.equals(flight.flightNumber)
                    || (flight.flightNumber.length() != 5 && flight.flightNumber.length() != 6)
                    || (flight.flightNumber.toUpperCase().mid(0,2) != 'KL' && flight.flightNumber.toUpperCase().mid(0,2) != 'AF')
                    || !(flight.flightNumber.mid(2,(flight.flightNumber.length()-2))).IsNumeric() )  {
                invalid = true;     
            }
               
            if (flight.travelDate == null
                    || ''.equals(flight.travelDate)){
                invalid = true;
            } else {
                    try{
                        flight.travelDate = string.valueof(Date.Valueof(flight.travelDate));
                    }  catch(Exception e){
                        invalid = true;
                    }
            }
                
            if (passenger == null 
                    || (passenger.cabinCode != null && passenger.cabinCode.length() > 1 || (passenger.cabinCode != null && validateCabinNseat(passenger.cabinCode)))
                    || (passenger.cabinCode != null && passenger.cabinCode.isNumeric() )
                    || (passenger.email != null && (passenger.email.length() > 0 &&  !passenger.email.contains('@')))
                    || (passenger.seat.seatNumber != null && (passenger.seat.seatNumber.isAlpha() || passenger.seat.seatNumber.isNumeric()))
                    || (validateCabinNseat(passenger.seat.seatNumber) )
                    || (passenger.cabinCode != null && (passenger.cabinCode.length() != 1)) 
                    || (! nameValidation(passenger.FirstName))
                    || (! nameValidation(passenger.familyName)) ){
                invalid = true;
            }
            return invalid;
        }
        
        public override String toString() {
            String str = 
                '{' +
                '"rating": "' + rating +  '", ' +
                '"positiveComment": "' + positiveComment +  '", ' +
                '"negativeComment": "' + negativeComment +  '", ' +
                '"ratingToken": "' + ratingToken +  '", ' +
                '"isPublished": "' + isPublished + '"';
                if (flight != null) {
                    str += ', "flight": ' + flight.toString();
                }  
                if (passenger != null) {
                    str += ', "passenger": ' + passenger.toString();  
                }  
                str += '}';
            return str;
        }
        
        // Method added to verify that a given name string is 
        // all alphabets along with some permissable special characters.
        private Boolean nameValidation(String name){
            //allowed special characters in name string
            List<String> specialCharList = new List<String>{' ','/',',','.','-'};
            String tempName;
            
            if (name != null && name != ''){
                tempName = name;
                for (String s: specialCharList){
                    tempName = tempName.replace(s,'');
                }   
            }   
            
            if (tempName == null || tempName == '' || tempName.isAlpha()){
                return true;
            }
            
            return false;
        }
        
        //cleaning name for repetitive special characters
        private String removeExtraSymbols(String name){
            String tempName = name;
            
            if (tempName != null){
                tempName = tempName.trim();
                if (tempName != null){
                    tempName = tempName.replaceAll('-+','-');
                    tempName = tempName.replaceAll('\\s+',' ');
                    tempName = tempName.replaceAll('\\.+','.');
                    tempName = tempName.replaceAll('/+','/');
                    tempName = tempName.replaceAll(',+',',');
                }   
            }
            return tempName;
        }
        
        //CabinCode and SeatNumber validation
        private Boolean validateCabinNseat(String code){
            if (code != Null && code != ''){
                String cleanStringCode = code.replaceAll('[^a-zA-Z0-9]', '');
                if (code.length() > cleanStringCode.length()){
                    return true;
                }
            }               
            return false;
        }
    }
    
    global class Passenger {
        public String firstName { get; set; }
        public String familyName { get; set; }
        public String email { get; set; }
        public String allowEmailContact { get; set; }
        public Seat seat { get; set; }
        public String cabinCode { get; set; }
    
        public Passenger(String firstName, String familyName, Seat seat, String cabinCode, String email, String allowEmailContact) {
            this.firstName = firstName;
            this.familyName = familyName;
            this.seat = seat;
            this.cabinCode = cabinCode;
            this.email = email;
            this.allowEmailContact = allowEmailContact;
        }
        
        public override String toString() { 
            String str = '{' +
                '"firstName": "' + firstName +  '", ' +
                '"familyName": "' + familyName +  '", ' +
                '"email": "' + email +  '", ' +
                '"allowEmailContact": "' + allowEmailContact +  '", ' + 
                '"cabinCode": "' + cabinCode + '" ';
                if (seat != null) {
                    str += ', "seat": ' + seat;
                }                                             
                str += '}';
            return str;
        }
    }
       
    global class Flight {
        public String flightNumber;
        public String travelDate { get; set; }
        public String origin { get; set; }
        public String destination { get; set; }
    
        public Flight(String flightNumber, String travelDate, string origin, string destination) {
            this.flightNumber = flightNumber;
            this.travelDate = travelDate;
            this.origin = origin;
            this.destination = destination;
        }
        
        public override String toString() { 
            return '{' +
                '"flightNumber": "' + flightNumber +  '", ' +
                '"travelDate": "' + travelDate + '"' +
            '}';
        }
    }
    
    global class Seat {
        public String seatNumber { get; set; }
    
        public Seat(string seatNumber) {
            this.seatNumber = seatNumber;
        }
        
        public override String toString() { 
            return '{' +
                '"seatNumber": "' + seatNumber + '"' + 
            '}';
        }
    }
}