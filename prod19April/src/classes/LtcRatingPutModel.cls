/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman 
 * @description     : Model class to contain Rating Put objects
 * @log             :  2MAY2014: version 1.0
 * @log             : 21JUL2015: version 1.1
 */
global class LtcRatingPutModel { 
    public Rating rating { get; set; }
    global class Rating {
        public String ratingId { get; set; }
        public String isPublished { get; set; }        
    
        public Rating(String ratingId, String isPublished) {
            this.ratingId = ratingId;
            this.isPublished = isPublished;
        }
        
        public override String toString() {
        	String str = 
        		'{"ratingId": "' + ratingId +  '", ' +
            	'"isPublished": "' + isPublished + '"}';
            return str;
        }
    }

}