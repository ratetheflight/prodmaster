/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman, Satheeshkumar Subramani
 * @description     : Rest API interface for the rating feedback of a flight
 * @log             : 13MAR2014: version 1.0
 * @log             : 21JUL2015: version 2.0 LTC-743 do JSON deserializing try catch block and prevent detailed error reporting to the clients
 * @log             : 27AUG2015: version 2.1 added image url's
 * @log             : 14SEP2015: version 2.2 added max nr of ratings per flight check
 */
@RestResource(urlMapping='/Ratings/*')
global class LtcRatingRestInterface { 
    public static Integer MAX_RATINGS_PER_FLIGHT = 200;

    /**
     * Create a rating
     * Expected Json object:
     * { "rating": {"rating": "2", "positiveComment": "Excellent feedback1", "negativeComment": "Negative feedback1",
     *      "positiveImage": "http://fotosite/posurl.jpg", 
     *      "positiveImageLarge" : "http://fotosite/poslargeurl.jpg", 
     *      "negativeImage" : "", "negativeImageLarge" : "",
     *      "isPublished": "True",
     *      "uid": "gdvyug78tguf7FY7878guigFG*&TG",         
     *      "flight": {"flightNumber": "KL1234", "travelDate": "2014-01-01"}, 
     *      "passenger": {"firstName": "Kees", "familyName": "Jansen", 
     *      "seat": {"seatNumber": "1A"}}}});     
     **/
    @HttpPost
    global static void doPost(){
        String result = '';
        RestResponse res = RestContext.response;
        String clientId;
        String host;
        String channel;
        res.addHeader('Content-Type', 'application/json');
        //try {
            String requestBody = RestContext.request.requestBody.toString();
            System.debug('RequestBody:::'+requestBody);
            LtcRatingPostModel ratingpm = (LtcRatingPostModel) System.JSON.deserialize(requestBody, LtcRatingPostModel.class);
            clientId = RestContext.request.headers.get('AFKL-TRAVEL-Client-Id');
            host = RestContext.request.headers.get('AFKL-TRAVEL-Host');
            channel = RestContext.request.headers.get('AFKL-TRAVEL-Channel');
            result = createRating(ratingpm.rating, clientId, host, channel);
        /*} catch (Exception ex) {
            res.statusCode = 500;
            if (LtcUtil.isDevOrg()) {
                 result = '{"error": "'+ ex.getMessage() + '   '  + ex.getStackTraceString() + '"}';
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }*/
        res.responseBody = Blob.valueOf(result);
    }   
   
    global static String createRating(LtcRatingPostModel.Rating rating, String clientId, String host, String channel) {
        System.debug('Ratings::::'+rating);
        String ratingId = '';  
        String response = '';
        RestResponse res = RestContext.response;
        
        LtcRating ltcRating = new LtcRating();
        
        if (rating.isInValid()) {
        //if (true == false) {
            res.statusCode = 400;
            System.debug(LoggingLevel.ERROR, 'Invalid rating ' + rating);   
        } else if (ltcRating.hasDuplicateRatings(rating.Flight.flightNumber, rating.Flight.travelDate, rating.rating, rating.positiveComment, rating.negativeComment, 
            rating.passenger.firstName, rating.passenger.familyName, rating.passenger.seat.seatNumber, rating.passenger.cabinCode)) {
            response = '{"ratingStatus": "Duplicate"}';
            res.statusCode = 403;
            System.debug(LoggingLevel.ERROR, 'Duplicate rating already exists for Flight' + rating.Flight.flightNumber + ' ' + rating.Flight.travelDate);   
        } else if (ltcRating.countRatings(rating.Flight.flightNumber, Date.valueOf(rating.Flight.travelDate)) > MAX_RATINGS_PER_FLIGHT) {
            response = '{"ratingStatus": "Closed"}';
            res.statusCode = 403;
            System.debug(LoggingLevel.ERROR, 'max nr of ratings reached for flight ' + rating.Flight.flightNumber + ' ' + rating.Flight.travelDate); 
        } else {
            String acceptLanguage = RestContext.request.headers.get('Accept-Language');
            if (rating.Flight.flightNumber.startsWith('AF')) {
                ratingId = ltcRating.setRating(rating, acceptLanguage, clientId, host, channel);
                response = '{"rating": {"ratingId":"' + ratingId + '"}}';
                res.statusCode = 201;
            } else {
                LtcFlightInfo ltcFlightInfo = new LtcFlightInfo();
                LtcFlightResponseModel responseModel = ltcFlightInfo.getFlightInfo(rating.Flight.flightNumber, rating.Flight.travelDate, acceptLanguage, '', rating.ratingToken); 
                if (responseModel.isEmpty()) {
                    res.statusCode = 404;
                } else if ('Inactive'.equalsIgnoreCase(responseModel.flight.ratingStatus)) {
                    response = '{"ratingStatus": "Inactive"}';
                    res.statusCode = 403;
                } else if ('Closed'.equalsIgnoreCase(responseModel.flight.ratingStatus)) {
                    response = '{"ratingStatus": "Closed"}';
                    res.statusCode = 403;
                }  else {
                    ratingId = ltcRating.setRating(rating, acceptLanguage, clientId, host, channel);
                    response = '{"rating": {"ratingId":"' + ratingId + '"}}';
                    res.statusCode = 201;
                }            
            }
        }
        return response;
    }   
   
    
    /**
     * Update the publish parameter for a specific rating id
     * Expected Json object:
     * { "rating": {"ratingId": "a10110000009a09==", "isPublished": "False"}}
     **/
    @HttpPut
    global static void doPut(){
        String result = '';
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        try{
            String requestBody = RestContext.request.requestBody.toString();
            System.debug('requestBody=' + requestBody);
            LtcRatingPutModel ratingPutModel = (LtcRatingPutModel) System.JSON.deserialize(requestBody, LtcRatingPutModel.class);
            result = updateRating(ratingPutModel.rating);
        } catch (Exception ex) {
            res.statusCode = 500;
            if (LtcUtil.isDevOrg()) {
                result = '{"error": "'+ ex.getMessage() + '   '  + ex.getStackTraceString() + '"}';
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        res.responseBody = Blob.valueOf(result);
    }

    public static String updateRating(LtcRatingPutModel.Rating rating){
        String ratingId = '';
        String result = '';  
        try {
            LtcRating ltcRating = new LtcRating();
            ratingId = ltcRating.updateRating(rating);
            result = '{"rating": {"ratingId":"' + ratingId + '"}}';
        } catch(Exception ex) {
            if (LtcUtil.isDevOrg()) {
                result = '{"error": "'+ ex.getMessage() + '   '  + ex.getStackTraceString() + '"}';
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return result;
    }
     
    /**
     * Delete a rating with a specified rating Id, Id in encrypted format
     * Expected Json object:
     * { "rating": {"ratingId": "a10110000009a09=="}}
     **/
    @HttpDelete
    global static void doDelete(){
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String result = '';
        try{
            String requestBody = RestContext.request.requestBody.toString();
            System.debug('requestBody=' + requestBody);
            LtcRatingPutModel ratingPutModel = (LtcRatingPutModel) System.JSON.deserialize(requestBody, LtcRatingPutModel.class);
            
            LtcRating ltcRating = new LtcRating();
            Rating__c rating = ltcRating.deleteRating(ratingPutModel.rating);
            if (rating == null) {
                res.statusCode = 404;
            }
        } catch (Exception ex) {
            res.statusCode = 500;
            if (LtcUtil.isDevOrg()) {
                result = '{"error": "'+ ex.getMessage() + '   '  + ex.getStackTraceString() + '"}';
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        res.responseBody = Blob.valueOf(result);
    }

}