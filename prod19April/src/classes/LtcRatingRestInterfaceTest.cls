/**
 * @author (s)      : David van 't Hooft, Mees Witteman, Satheeshkumar Subramani
 * @description     : Test class to test the LtcRatingInterface, the LtcRating, the LtcRatingPostModel, 
 *                    LtcPutModel and the LtcRatingModel classes
 * @log             : 2MAY2014: version 1.0
 * @log             : 1MAR2016: version 2.0
 */
@isTest
private class LtcRatingRestInterfaceTest { 
    
    private static Date setup(Integer addDays) {
        Date theDay = Date.today().addDays(addDays);
        Monitor_Flight__c monFlight = new Monitor_Flight__c();
        monFlight.Flight_Number__c = 'KL1234';
        insert monFlight;

        Flight__c fl = new Flight__c();
        fl.flight_number__c = 'KL1234';
        fl.scheduled_departure_date__c = theDay;
        fl.currentleg__c = 0;
        insert fl;
        
        Leg__c leg = new Leg__c();
        leg.flight__c = fl.id;
        leg.scheduledDepartureDate__c = theDay;
        leg.scheduledDepartureTime__c = '12:00';
        leg.scheduledArrivalDate__c = theDay;
        leg.scheduledArrivalTime__c = '12:00';
        leg.actualArrivalDate__c = theDay;
        leg.actualArrivalTime__c = '12:00';
        leg.legNumber__c = 0;
        leg.timeToArrival__c = 1 * addDays;
        insert leg;
        
        return theDay;
    }

    /**
     * Test a correct rating Post
     **/
    static testMethod void postRatingViaRestInterfaceOk() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        req.requestBody = Blob.valueof('{"rating":{"rating":4,"positiveComment":"laikie","negativeComment":"dislaikie","isPublished":true,"flight":{"flightNumber":"KL1234","travelDate":"' + yyyy +  '","origin":"AMS","destination":"LHR"},"passenger":{"firstName":"piet","familyName":"jansen","seat":{"seatNumber":"11a"},"email":"fre.coputea@dingetje.nl"}}}');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingRestInterface.doPost();    
        System.assert(res.responseBody.toString().indexOf('ratingId') != -1);
        System.assert(RestContext.response.statusCode == 201);
    }

    /**
     * Test error reporting only in DEV org.
     **/
    static testMethod void postRatingNoParseErrorReportingBecauseNoDevUrl() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        req.requestBody = Blob.valueof('{"rating":{"rating":"0","positiveComment":123,"negativeComment":1,"isPublished":true,"flight":{"flightNumber":"AF1234","travelDate":"' + yyyy +  '","origin":"AMS","destination":"LHR"},"passenger":{"firstName":"piet","familyName":"jansen","seat":{"seatNumber":"1A"},"email":"coputea@klm.com"}}}');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingRestInterface.doPost();

        //System.debug(LoggingLevel.INFO, 'tuut' + res.responseBody.toString());
        //System.debug(LoggingLevel.INFO, 'res.statusCode=' + res.statusCode);
        System.assertEquals(400, res.statusCode);
        System.assert((0 == res.responseBody.toString().length()), 'no error info in result expected because test runs on cs18 machine url and not on baggdev');
    }

    /**
     * Test error reporting only in DEV org.
     **/
    static testMethod void putRatingNoParseErrorReportingBecauseNoDevUrl() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        req.requestBody = Blob.valueof('{ "rating": {"ratingId": ___, "isPublished": "False"}});');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingRestInterface.doPut();   
        System.assert(RestContext.response.statusCode == 500);
        System.assert((0 == res.responseBody.toString().length()), 'no error info in result expected because test runs on cs18 machine url and not on baggdev');
    }
    
    /**
     * Test delete method
     **/
    static testMethod void deleteRating() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'DELETE';
        req.addHeader('Accept-Language', 'nl-nl');
        req.requestBody = Blob.valueof('{ "rating": {"ratingId": "cWM2TCUyQlRQaE1jTVlNRU1IJTJCZWJPd2d2NWs4M1J3aDdzbHhmRmZCODR2NEk1aHcxN3F0bzFmOXlnbUo0ME01T0Y="}});');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingRestInterface.doDelete();   
        System.assert(RestContext.response.statusCode == 404);
        System.debug(LoggingLevel.INFO, res.responseBody.toString());
    }
    
    /**
     * Test a correct rating Post
     **/
    static testMethod void createRatingViaRestInterfaceOk() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        System.debug('yyyy=' + yyyy);
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingPostModel.Seat seat = new LtcRatingPostModel.Seat('1A');
        LtcRatingPostModel.Passenger passenger = new LtcRatingPostModel.Passenger('Pietje','Puk', seat, 'A', 'pietje@puk.nl', 'true');
        LtcRatingPostModel.Flight flight = new LtcRatingPostModel.Flight('KL1234', yyyy, 'ORG', 'DST');
        LtcRatingPostModel.Rating rating = new LtcRatingPostModel.Rating('4', 'Positive', 'Negative', 'False', flight, passenger, '3gt45gt4y656h54hj54' , '2wfr34f32gf234g545');
        System.debug('Rating:' + rating.toString());
        String result = LtcRatingRestInterface.createRating(rating, '', '', '');
        System.debug(LoggingLevel.INFO, 'Result:' + result);
        System.assert((50 < result.length()), 'encoded id'); //Result needs to be longer than 50 chars        
    }

    /**
     * rating status closed
     **/
    static testMethod void ratingCLosed() {
        Date theDate  = setup(-50);
        String yyyy = DateTime.newInstance(theDate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingPostModel.Seat seat = new LtcRatingPostModel.Seat('1A');
        LtcRatingPostModel.Passenger passenger = new LtcRatingPostModel.Passenger('Pietje', 'Puk', seat, 'A', 'pietje@puk.nl', 'true');
        LtcRatingPostModel.Flight flight = new LtcRatingPostModel.Flight('KL1234', yyyy, 'ORG', 'DST');
        LtcRatingPostModel.Rating rating = new LtcRatingPostModel.Rating('4', 'Positive', 'Negative', 'False', flight, passenger, '3gt45gt4y656h54hj54' , '2wfr34f32gf234g545');
        System.debug('Rating:' + rating.toString());
        String result = LtcRatingRestInterface.createRating(rating, '', '', '');
        System.assertEquals('{"ratingStatus": "Closed"}', result);     
    }


    /**
     * rating status duplicate
     **/
    static testMethod void duplicateRating() {
        Date theDate  = setup(-1);
        String yyyy = DateTime.newInstance(theDate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        Integer duplicateRatings = 2;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingPostModel.Seat seat = new LtcRatingPostModel.Seat('1A');
        LtcRatingPostModel.Passenger passenger = new LtcRatingPostModel.Passenger('Pietje','Puk', seat, 'A', 'pietje@puk.nl', 'true');
        LtcRatingPostModel.Flight flight = new LtcRatingPostModel.Flight('KL1234', yyyy, 'ORG', 'DST');
        LtcRatingPostModel.Rating rating = new LtcRatingPostModel.Rating('4', 'Positive', 'Negative', 'False', flight, passenger, '3gt45gt4y656h54hj54' , '2wfr34f32gf234g545');
        String result = LtcRatingRestInterface.createRating(rating, '', '', '');

        for (Integer i = 0; i < duplicateRatings; i++) {
            result = LtcRatingRestInterface.createRating(rating, '', '', '');
        }
        System.assertEquals('{"ratingStatus": "Duplicate"}', result);     
    }
  /**
     * rating status closed
     **/
    static testMethod void maxNumberOfRatingsReached() {
        Date theDate  = setup(-1);
        String yyyy = DateTime.newInstance(theDate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        LtcRatingRestInterface.MAX_RATINGS_PER_FLIGHT = 2;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingPostModel.Seat seat = new LtcRatingPostModel.Seat('1A');
        LtcRatingPostModel.Passenger passenger = new LtcRatingPostModel.Passenger('Pietje','Puk', seat, 'A', 'pietje@puk.nl', 'true');
        LtcRatingPostModel.Flight flight = new LtcRatingPostModel.Flight('KL1234', yyyy, 'ORG', 'DST');
        
        String result;
        for (Integer i = 1; i <= 4; i++) {
            result = LtcRatingRestInterface.createRating(new LtcRatingPostModel.Rating(String.valueOf(i), 'Positive', 'Negative', 'False', flight, passenger, '3gt45gt4y656h54hj54' , '2wfr34f32gf234g545'), '', '', '');
        }
        System.assertEquals('{"ratingStatus": "Closed"}', result);     
    }
    /**
     * Test the incorrect date scenario
     **/
    static testMethod void ratingRestInterfacePostTestFailedDate() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        RestContext.request = req;
        RestContext.response = res;
        
        LtcRatingPostModel.Seat seat = new LtcRatingPostModel.Seat('1A');
        LtcRatingPostModel.Passenger passenger = new LtcRatingPostModel.Passenger('Jan','Jansen',seat, 'A', 'pietje@puk.nl', 'true');
        LtcRatingPostModel.Flight flight = new LtcRatingPostModel.Flight('AF1234','2014-32-13', 'ORG', 'DST');
        LtcRatingPostModel.Rating rating = new LtcRatingPostModel.Rating('4','Positive','Negative','False',flight,passenger, '3gt45gt4y656h54hj54' , '2wfr34f32gf234g545');
        
        String result = 'init';
        try {
            result = LtcRatingRestInterface.createRating(rating,'','','');
        } catch (Exception e)  {

        }
        System.debug('responseCode=' + res.statusCode);
        System.assertEquals(400, res.statusCode);        
    }
    
    /**
     * Test the publish update
     **/
    static testMethod void ratingRestInterfaceUpdateRatingOkTest() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        System.debug('yyyy=' + yyyy);

        LtcRating ltcRating = new LtcRating();
        String id = ltcRating.setRating('2', 'Excellent feedback1', 'Negative feedback1', 'True', 'Kees', 'Baksteen1', 'pietje@puk.nl', 'KL1234', '1A', 'A', yyyy,'ORG', 'DST',  'nl_NL', null, '', '', '');        
        System.debug('rating id=' + id);
        List<Rating__c> ratingList = [select r.Publish__c from Rating__c r];
        
        System.assertEquals(1, ratingList.size());
        for (Rating__c rat : ratingList) {
            System.assertEquals(True, rat.Publish__c, 'By default is published set to True');
        }
    
        LtcRatingPutModel.Rating rating = new LtcRatingPutModel.Rating(id,'False'); 
        System.assertEquals('{"ratingId": "' + id + '", "isPublished": "False"}', rating.toString());
               
        String result = LtcRatingRestInterface.updateRating(rating);
        
        ratingList = [select r.Publish__c from Rating__c r];
        System.assertEquals(1, ratingList.size());
        for (Rating__c rat : ratingList) {
            System.assertEquals(False, rat.Publish__c, 'Should now read False');
        }
    }
    /**
     * Test the publish update
     **/
    static testMethod void ratingRestInterfaceUpdateRatingFailsTest() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');

        LtcRating ltcRating = new LtcRating();
        String id = ltcRating.setRating('2', 'Excellent feedback1', 'Negative feedback1', 'True', 'Kees', 'Baksteen1', 'pietje@puk.nl', 'KL1234', '1A', 'A', yyyy,'ORG', 'DST',  'nl_NL', null, '', '', '');        
        System.debug('rating id=' + id);
        List<Rating__c> ratingList = [select r.Publish__c from Rating__c r];

        LtcRatingPutModel.Rating rating = new LtcRatingPutModel.Rating('nonexistingid','False'); 
        String result = LtcRatingRestInterface.updateRating(rating);
        System.assertEquals('', result);
    }
        
    /**
     * Test the rating calculations
     **/
    static testMethod void ratingCleanFlightNumber() {
        LtcRating ltcRating = new LtcRating();
        LtcCrypto ltcc = new LtcCrypto();
        String tmpId = ltcRating.setRating('2', 'Excellent feedback1', 'Negative feedback1', 'True', 'Kees', 'Baksteen1', 'pietje@puk.nl', 'kL12', '1A', 'A', '2014-01-01','ORG', 'DST',  'nl_NL', null, '', '', '');
        String id = ltcc.decodeText(tmpId);
        List<Rating__c> ratingList = [select r.Flight_Info__r.Flight_Number__c, r.Flight_Info__c from Rating__c r where  Id =: id];
        for (Rating__c rat : ratingList) {
            System.assertEquals('KL0012', rat.Flight_Info__r.Flight_Number__c, 'By default is published set to True');
        }       
    }
    
    
    /**
     * KL 404 AF not
     **/
    static testMethod void test_404_Error_Non_Existing_KL() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        req.requestBody = Blob.valueof('{"rating":{"rating":"1","positiveComment":123,"negativeComment":1,"isPublished":true,"flight":{"flightNumber":"KL9999","travelDate":"' + yyyy +  '","origin":"AMS","destination":"LHR"},"passenger":{"firstName":"piet","familyName":"jansen","seat":{"seatNumber":"2A"},"email":"hela@klm.com"}}}');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingRestInterface.doPost();   
        System.assert(RestContext.response.statusCode == 404);
    }
    
    /**
     * No 404 error non existing flight af
     **/
    static testMethod void test_No_404_Error_For_AF_Non_Existing_Flight() {
        Date yesterdate  = setup(-1);
        String yyyy = DateTime.newInstance(yesterdate, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Ratings';        
        req.httpMethod = 'POST';
        req.addHeader('Accept-Language', 'nl-nl');
        req.requestBody = Blob.valueof('{"rating":{"rating":"1","positiveComment":123,"negativeComment":1,"isPublished":true,"flight":{"flightNumber":"AF9999","travelDate":"' + yyyy +  '","origin":"AMS","destination":"LHR"},"passenger":{"firstName":"piet","familyName":"jansen","seat":{"seatNumber":"2A"},"email":"copulara@monster.org"}}}');
        RestContext.request = req;
        RestContext.response = res;
        LtcRatingRestInterface.doPost();   
        System.debug('RestContext.response.statusCode=' + RestContext.response.statusCode);
        System.assert(RestContext.response.statusCode == 201);
    }
}