/**
 * Represents Json payload:
 * {
 *  "flightNumber" : "KL1234",
 *  "flightNumbers": ["KL1001", "KL1002"],
 *  "startDate": "2014-05-05",
 *  "endDate"  : "2014-05-05",
 *  "limit"    : 15,
 *  "offset"   : 0,
 *  "ratingIds": [
 *       "kFzWWY1a3B2TEsycHdOU2hyJTJCRXozbU5uMVNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUh4OWliaGo",
 *      "bladiebladeeieurSSKDSJDS(SIDSJIJUUIsusdjsisjsosbladiebladeeieurSSKDSJDS(SIDSJIJUUIsusdjsisjsos8s87si",
 *      "VNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUh4OWliaGoVNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUs"
 *   ],
 *  "host"        : "FG" / "KL" / "AF" ,
 *  "origin"      : "CDG",
 *  "destination" : "AMS",
 *  "country"     : "nl",
 *  "language"    : "NL"
 * }
 * 
 **/
global class LtcRatingSearchPostModel {
    public String flightNumber { get; set; }
    public List<String> flightNumbers { get; set; }
    public String startDate { get; set; }
    public String endDate { get; set; }
    public List<String> ratingIds { get; set; }
    public Integer limitTo { get; set; }
    public Integer offset { get; set; }
    public String host { get; set; }
    public String origin { get; set; }
    public String destination { get; set; }
    public String country { get; set; }
    public String language { get; set; }
    public Boolean crew {get;set;}
    
    public LtcRatingSearchPostModel(String flightNumber, List<String> flightNumbers, 
            String startDate, String endDate, List<String> ratingIds, 
            Integer limitTo, Integer offset, String host,
            String origin, String destination, String country, String language, Boolean crew) {
        this.flightNumber = flightNumber;
        this.flightNumbers = flightNumbers;
        this.startDate = startDate;
        this.endDate = endDate;
        this.ratingIds = ratingIds;
        this.limitTo = limitTo;
        this.offset = offset;
        this.host = host;
        this.origin = origin;
        this.destination = destination;
        this.country = country;
        this.language = language;
        this.crew = crew;
    }
}