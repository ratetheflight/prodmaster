/**                     
 * @author (s)      : Mees Witteman
 * @description     : Rest API interface for the rating feedback of a flight
 * @log             : 25AUG2016 version 2.0 valid JSON responses on the endpoints
 * @log             : 28SEP2016 version 3.0 added host AF to always show seat number
 **/
@RestResource(urlMapping='/Ratings/Search/*')
global class LtcRatingSearchRestInterface {
    
    /**
     * Retrieve the ratings for a specific flight or flight collection within a date range
     *
     * A choice must be made between one ratingNumber and a collection of ratingNumbers, 
     * if both are passed then the collection is used, ignoring the single flightNumber.
     * 
     * If one or more flights are present in the request then the origin, desination, country and language are ignored!!   
     * 
     * Expected Json payload:
     * "searchModel" :  {
     *   "flightNumber": "KL1", // may contains partial flightNumber
     *   "flightNumbers": ["KL1001", "KL1002"],
     *   "startDate": "2014-05-05",
     *   "endDate": "2014-05-05",
     *   "origin" : "CDG",
     *   "destination" : "AMS",
     *   "country" : "nl",
     *   "language" :  "NL",
     *   "limit": 15,
     *   "offset": 0,
     *   "ratingIds": [
     *      "kFzWWY1a3B2TEsycHdOU2hyJTJCRlPbkdibGZ3RHlarrtetteMXpPalF2Uwh4OWliaGo",
     *      "bladiebladeeieurSSKDSbladeeieurSSKDSJDS(SIDSJIJUUIsusdjsisjsos8s87si",
     *      "VNodXJMJTJGN0gzOWliaGoVNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUs"
     *    ],
     *   "host" : "FG", // can be FG, KL, AF etc.
     *   "crew" : true/false 
     }
     * ratingId must be an encoded String
     **/
    @HttpPost
    global static void postRatingsSearch(LtcRatingSearchPostModel searchModel) {
        RestResponse res = RestContext.response;
        String result = postRatingsSearchAsString(searchModel);
        if (res != null) { 
            res.addHeader('Content-Type', 'application/json');
            res.responseBody = Blob.valueOf(result);
        }
    }
    
    public static String postRatingsSearchAsString(LtcRatingSearchPostModel searchModel) {
        String response = '';
        LtcRatingGetModel.Result result;
        try {

            checkDates(searchModel.host, searchModel.startDate, searchModel.endDate);
            
            LtcRating ltcRating = new LtcRating();
            if (searchModel.flightNumbers != null && searchModel.flightNumbers.size() > 0) {
                result = ltcRating.getRatings(
                    searchModel.flightNumbers, searchModel.startDate, searchModel.endDate, 
                    searchModel.ratingIds, searchModel.limitTo, searchModel.offset, searchModel.host,searchModel.crew); 
            } 
            else if (!String.isEmpty(searchModel.flightNumber)) {
                result = ltcRating.getRatings(
                    searchModel.flightNumber, searchModel.startDate, searchModel.endDate, 
                    searchModel.ratingIds, searchModel.limitTo, searchModel.offset, searchModel.host,searchModel.crew); 
            } 
            else {
                result = ltcRating.getRatings(
                    searchModel.origin, searchModel.destination, 
                    searchModel.country, searchModel.language,
                    searchModel.startDate, searchModel.endDate, 
                    searchModel.ratingIds, searchModel.limitTo, searchModel.offset, searchModel.host,searchModel.crew); 
            }
            
            response = '{ "flight": ' + result.toString() + '}';
        } catch (SearchException se) {
            if (!Test.isRunningTest()) {
                RestResponse res = RestContext.response;
                res.statusCode = 400;
            }
            if (LtcUtil.isDevOrg() || Test.isRunningTest()) {
                response = '{"error": "'+ se.getMessage() + '   '  + se.getStackTraceString() + '"}';
            }
            System.debug(LoggingLevel.ERROR, se.getMessage() + ' ' + se.getStackTraceString());
        } catch (Exception ex) {
            if (!Test.isRunningTest()) {
                RestResponse res = RestContext.response;
                res.statusCode = 500;
            }
            if (LtcUtil.isDevOrg() || Test.isRunningTest()) {
                response = '{"error": "'+ ex.getMessage() + '   '  + ex.getStackTraceString() + '"}';
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return response;
    }
        
    private static void checkDates(String host, String startDate, String endDate) {
        if (startDate == null || startDate.length() != 10) {
            throw new SearchException('invalid startDate');
        }
        if (endDate == null || endDate.length() != 10) {
            throw new SearchException('invalid endDate');
        }
        Date start;
        Date ending;
        try {
            start = Date.valueof(startDate);
            ending = Date.valueof(endDate);
        } catch (Exception e) {
            throw new SearchException('invalid date');
        }
        
        if (checkPeriodForHost(host)) {
            if (start.daysBetween(ending) < 0) { // Changed by ata Jira 3377 previous value was 4.
                throw new SearchException('period too small');
            }
        }
    } 
    
    private static boolean checkPeriodForHost(String host) {
        return 'FG' != host && 'AF' != host;
    }
}