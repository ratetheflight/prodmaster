/**
 * Tests for LtcRatingSearchRestInterface
 */
@isTest
private class LtcRatingSearchRestInterfaceTest {

    static testMethod void ratingSearchRestInterface() {
        // setup rest context
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest//Ratings/Search/';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        
        LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel(
            'KL1234', null, '2014-01-01', '2014-12-31', new List<String>(), 15, 0,  '', null, null, null, null, null);
        
        LtcRatingSearchRestInterface.postRatingsSearch(searchModel);
        String expected = '{ "flight": {"flightNumber": "KL1234", "startDate": "2014-01-01", "endDate": "2014-12-31", "total": "0", "ratingStatistics": {"numberOfRatings": "0", "averageRating": "", "ratingCategory": [{"level": "1", "count": "0"}, {"level": "2", "count": "0"}, {"level": "3", "count": "0"}, {"level": "4", "count": "0"}, {"level": "5", "count": "0"}]}, "ratings": []}}';

        System.assertEquals(expected, res.responseBody.toString());
    }
    
    
    static testMethod void ratingSearchNoRatings() {
        LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel(
            'KL1234', null, '2014-01-01', '2014-12-31', new List<String>(), 15, 0,  '', null, null, null, null, null);
        String response = LtcRatingSearchRestInterface.postRatingsSearchAsString(searchModel);
        String expected = '{ "flight": {"flightNumber": "KL1234", "startDate": "2014-01-01", "endDate": "2014-12-31", "total": "0", "ratingStatistics": {"numberOfRatings": "0", "averageRating": "", "ratingCategory": [{"level": "1", "count": "0"}, {"level": "2", "count": "0"}, {"level": "3", "count": "0"}, {"level": "4", "count": "0"}, {"level": "5", "count": "0"}]}, "ratings": []}}';
        System.assertEquals(expected, response);
    }
    
    static testMethod void ratingSearchRatingsByOrigin() {
        LtcRatingStatisticsTest.setup();
        
        LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel(
            null, null, '2014-01-01', '2018-12-31', new List<String>(), 15, 0,  '', 'AMS', null, null, null,null);
        String response = LtcRatingSearchRestInterface.postRatingsSearchAsString(searchModel);
        System.debug('response=' + response);
        String expected = '{ "flight": {"flightNumber": " origin=AMS", "startDate": "2014-01-01", "endDate": "2018-12-31", "total": "6", "ratingStatistics": {"numberOfRatings": "6", "averageRating": "2.83333333333333333333333333333333", "ratingCategory": [{"level": "1", "count": "1"}, {"level": "2", "count": "2"}, {"level": "3", "count": "1"}, {"level": "4", "count": "1"}, {"level": "5", "count": "1"}]}, "ratings": [{"ratingId": null, "rating": "5", "positiveComment": "pos", "negativeComment": "neg", "klmReply": "", "flightNumber": "KL1001", "travelDate": "2016-09-18", "country": "NL", "language": "nl", "isPublished": "true", "hiddenComments": "false", "passenger": {"firstName": "Kees", "hiddenName": "false", "seat": {"seatNumber": "", "hiddenSeatNumber": "false"}}}, {"ratingId": null, "rating": "2", "positiveComment": "pos", "negativeComment": "neg", "klmReply": "", "flightNumber": "KL1233", "travelDate": "2016-09-18", "country": "FR", "language": "fr", "isPublished": "true", "hiddenComments": "false", "passenger": {"firstName": "Kees", "hiddenName": "false", "seat": {"seatNumber": "", "hiddenSeatNumber": "false"}}}, {"ratingId": null, "rating": "1", "positiveComment": "pos", "negativeComment": "neg", "klmReply": "", "flightNumber": "KL1001", "travelDate": "2016-09-18", "country": "NL", "language": "nl", "isPublished": "true", "hiddenComments": "false", "passenger": {"firstName": "Kees", "hiddenName": "false", "seat": {"seatNumber": "", "hiddenSeatNumber": "false"}}}, {"ratingId": null, "rating": "2", "positiveComment": "pos", "negativeComment": "neg", "klmReply": "", "flightNumber": "KL1001", "travelDate": "2016-09-18", "country": "NL", "language": "nl", "isPublished": "true", "hiddenComments": "false", "passenger": {"firstName": "Kees", "hiddenName": "false", "seat": {"seatNumber": "", "hiddenSeatNumber": "false"}}}, {"ratingId": null, "rating": "3", "positiveComment": "pos", "negativeComment": "neg", "klmReply": "", "flightNumber": "KL1001", "travelDate": "2016-09-18", "country": "NL", "language": "nl", "isPublished": "true", "hiddenComments": "false", "passenger": {"firstName": "Kees", "hiddenName": "false", "seat": {"seatNumber": "", "hiddenSeatNumber": "false"}}}, {"ratingId": null, "rating": "4", "positiveComment": "pos", "negativeComment": "neg", "klmReply": "", "flightNumber": "KL1001", "travelDate": "2016-09-18", "country": "FR", "language": "fr", "isPublished": "true", "hiddenComments": "false", "passenger": {"firstName": "Kees", "hiddenName": "false", "seat": {"seatNumber": "", "hiddenSeatNumber": "false"}}}]}}';
        System.assert(response.contains('origin=AMS'));
        System.assert(response.contains('"total": "6"'));
    }
    
    static testMethod void ratingSearchMultipleFlightNumbers() {
        List<String> flnoos = new List<String>();
        flnoos.add('KL1234');
        flnoos.add('KL9999');
        
        LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel(
            null, flnoos, '2014-01-01', '2014-12-31', null, 15, 0,  '', null, null, null, null,null);
        String expected = '{ "flight": {"flightNumber": "", "startDate": "2014-01-01", "endDate": "2014-12-31", "total": "0", "ratingStatistics": {"numberOfRatings": "0", "averageRating": "", "ratingCategory": [{"level": "1", "count": "0"}, {"level": "2", "count": "0"}, {"level": "3", "count": "0"}, {"level": "4", "count": "0"}, {"level": "5", "count": "0"}]}, "ratings": []}}';
        //LtcRatingSearchRestInterface.postRatingsSearch(searchModel);
        String response = LtcRatingSearchRestInterface.postRatingsSearchAsString(searchModel);
        System.assertEquals(expected, response);
    }
    
    static testMethod void ratingSearchDatesInvalid() {
        LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel( 
            'KL1234', null, '2014-01-01', '2014-01-01', new List<String>(), 15, 0,  'RTF', null, null, null, null,null);
        String response = LtcRatingSearchRestInterface.postRatingsSearchAsString(searchModel);
        System.debug(LoggingLevel.INFO, 'response='+ response);
        //System.assert(response.indexOf('period too small') != -1);
    }
    
    static testMethod void ratingSearchDatesInvalidButWeDontCare() {
        LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel( 
            'KL1234', null, '2014-01-01', '2014-01-01', new List<String>(), 15, 0,  'FG', null, null, null, null,null);
        String response = LtcRatingSearchRestInterface.postRatingsSearchAsString(searchModel);
        String expected = '{ "flight": {"flightNumber": "KL1234", "startDate": "2014-01-01", "endDate": "2014-01-01", "total": "0", "ratingStatistics": {"numberOfRatings": "0", "averageRating": "", "ratingCategory": [{"level": "1", "count": "0"}, {"level": "2", "count": "0"}, {"level": "3", "count": "0"}, {"level": "4", "count": "0"}, {"level": "5", "count": "0"}]}, "ratings": []}}';
        System.assertEquals(expected, response);
    }
    
    static testMethod void ratingSearchFailure() {
        LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel(
            null, null, null, null, new List<String>(), 15, 0,  '', null, null, null, null,null);
        String response = LtcRatingSearchRestInterface.postRatingsSearchAsString(searchModel);
        System.assert(response.indexOf('invalid startDate') != -1);
    }
}