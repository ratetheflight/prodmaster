/**
 * Rating statistics: number, average and categories plus a collection
 * of classes to support the Rating Statistics Rest endpoint for Search / FlightOffer
 * @author       : Mees Witteman
 * @log          : JUN2016: version 1.0
 */
global class LtcRatingStatistics {
    public Integer numberOfRatings { get; set; }
    public Decimal averageRating { get; set; }
    public RatingCategoryContainer ratingCategories { get; set; }

    public LtcRatingStatistics(List<Rating__c> ratingList) {
        this.numberOfRatings = ratingList.size();
        this.calculateAverage(ratingList);
        this.createRatingCategories(ratingList);
    }
    
    private void calculateAverage(List<Rating__c> ratingList) {
        Decimal total = 0;
        if (ratingList != null && ratingList.size() > 0) {
            for (Rating__c rating: ratingList) {
                total += rating.Rating_Number__c;
            }
            averageRating = total / ratingList.size();
        } else {
            averageRating = null;
        }       
    }
    
    private void createRatingCategories(List<Rating__c> ratingList) {
        List<RatingCategory> ratingCategoryList = new List<RatingCategory>();
        ratingCategoryList.add(new RatingCategory(1)); // 'Horrible'
        ratingCategoryList.add(new RatingCategory(2)); // 'So-so'
        ratingCategoryList.add(new RatingCategory(3)); // 'Ok'
        ratingCategoryList.add(new RatingCategory(4)); // 'Good'
        ratingCategoryList.add(new RatingCategory(5)); // 'Excellent'
        
        // Now count all the ratings.
        if (ratingList != null) {
            for (Rating__c rating: ratingList) {
                if (rating.Rating_Number__c > 0 && rating.Rating_Number__c < 6) {
                    ratingCategoryList[Integer.valueOf(rating.Rating_Number__c) - 1].count++;
                }
            }
        }
        ratingCategories = new RatingCategoryContainer(ratingCategoryList);
    }
    
    /**
     * Criteria to select a group of ratings identified by a reference ID of the caller
     */
	global class SelectionGroup {
		
		// id for caller to correlate in list responses
		public String serviceReferenceIdByCaller { get; set; }
		
		// selection criteria, dates refer to travelDate a.k.a. schedDepartureDate 
		public String fromDate { get; set; }
		public String endDate { get; set; }
		public String flightNumber { get; set; }

		public String country { get; set; }
		public String language { get; set; }

		public String origin { get; set; }
		public String destination { get; set; }
		
		public String getWhereClause() {
			String result = ' Where r.Id <> null ';
			if (String.isNotEmpty(fromDate)) {
				result += ' And r.Flight_Info__r.Scheduled_Departure_Date__c >= ' + fromDate + '';
			}
			if (String.isNotEmpty(endDate)) {
				result += ' And r.Flight_Info__r.Scheduled_Departure_Date__c <= ' + endDate + '';
			}
			if (String.isNotEmpty(flightNumber)) {
				result += ' And r.Flight_Info__r.Flight_Number__c = \'' + flightNumber + '\'';
			}
			if (String.isNotEmpty(country)) {
				result += ' And r.country__c = \'' + country + '\'';
			}
			if (String.isNotEmpty(language)) {
				result += ' And r.language__c = \'' + language + '\'';
			}
			if (String.isNotEmpty(origin)) {
				result += ' And r.origin__c = \'' + origin + '\'';
			}
			if (String.isNotEmpty(destination)) {
				result += ' And r.destination__c = \'' + destination + '\'';
			}
			return result;
		}
	}    

    /**
     * Category of rating: 1,2,3,4, or 5 and the number of ratings for the category.
     */	
    global class RatingCategory {
        public Integer level { get; set; }
        public Integer count { get; set; }
    
        public RatingCategory(Integer level) {
            this.level = level;
            this.count = 0;
        }
    }
    
    /**
     * Container for Rating Category instances.
     */	    
    global class RatingCategoryContainer {
        public List<RatingCategory> itemList { get; set; }
    
        public RatingCategoryContainer(List<RatingCategory> itemList) {
            this.itemList = itemList;
        }
    }
    
    /**
     * ResponseModelRoot providing a root element for the json to XML mapper tools of the ESB team
     */    
	global class ResponseModelRoot {
		public ResponseModel root = new ResponseModel();
	}
    
    /**
     * ResponseModel providing a list of ResponseGroup instances and optional error info
     */    
	global class ResponseModel {
		public List<ResponseGroup> responseGroups = new List<ResponseGroup>();
		public String errorInfo;
	}
		
    /**
     * Provides ratingStatistics for a ResponseGroup identified by a reference ID for the caller.
     */ 	
	global class ResponseGroup {
        public String serviceReferenceIdByCaller;
        public LtcRatingStatistics ratingStatistics;
    
        public ResponseGroup(String serviceReferenceIdByCaller, List<Rating__c> ratingList) {
            this.serviceReferenceIdByCaller = serviceReferenceIdByCaller;
            if (ratingList != null && ratingList.size() > 0) { 
            	this.ratingStatistics = new LtcRatingStatistics(ratingList);
            }
        }
	}
}