/**                     
 * Rest API interface for retrieving rating statistics. 
 * First and only client application: klm.com/search a.k.a. FlightOffer (FO)
 * @author       : Mees Witteman
 * @log          : JUN2016: version 1.0
 */
@RestResource(urlMapping='/Ratings/Statistics/*')
global class LtcRatingStatisticsRestInterface { 

    /** 
     * Accept a List of LtcRatingStatistics.SelectionGroup objects
     *
     * Expected Json payload example:
     * {"groups" : [
	 *  { 
	 *     "serviceReferenceIdByCaller" : "no-criteria"
	 *  },
	 *  { 
	 *     "serviceReferenceIdByCaller" : "all-criteria",
	 *     "fromDate" : "2015-01-01",
	 *     "endDate" : "2016-09-01",
	 *     "flightNumber" : "KL1001",
	 *     "country" : "nl",
	 *     "language" : "en",
	 *     "origin" : "AMS",
	 *     "destination" : "LHR"
	 *   }
	 *  ]
	 * }
     * return a list with star rating statistics per LtcRatingStatistics.SelectionGroup in the result
     */    
    @HttpPost
    global static LtcRatingStatistics.ResponseModelRoot searchRatingStatistics(List<LtcRatingStatistics.SelectionGroup> groups) {
        LtcRatingStatistics.ResponseModelRoot result = new LtcRatingStatistics.ResponseModelRoot();
        LtcRatingStatistics.ResponseModel responseModel = result.root;
        
        RestResponse res = RestContext.response;
        if (!Test.isRunningTest()) {
            res.addHeader('Access-Control-Allow-Origin', '*');
            res.addHeader('Content-Type', 'application/json');
        }
        try {
            for (LtcRatingStatistics.SelectionGroup ratingGroup : groups) {
                List<Rating__c> ratings = getRatings(ratingGroup);
                LtcRatingStatistics.ResponseGroup responseGroup = 
                        new LtcRatingStatistics.ResponseGroup(ratingGroup.serviceReferenceIdByCaller, ratings);
                responseModel.responseGroups.add(responseGroup);
            }
        } catch (Exception ex) {
            res.statusCode = 500; 
            if (LtcUtil.isDevOrg()) {
                responseModel.errorInfo = ex.getMessage() + ' ' + ex.getStackTraceString();    
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());  
        }
        return result;
    }

    /**
     * Get all flight ratings in SFDC format with matching flightnumber for 365 days from today
     * No sorting on language and country
     **/
    private static List<Rating__c> getRatings(LtcRatingStatistics.SelectionGroup ratingGroup) {
        Date fromDate = Date.today().addDays(-365);
        
        List<Rating__c> resultList = selectRatingsFromDate(ratingGroup, fromDate);
        System.debug(LoggingLevel.DEBUG, 'resultList.size()=' + resultList.size());
        return resultList;
    }

    private static List<Rating__c> selectRatingsFromDate(LtcRatingStatistics.SelectionGroup ratingGroup, Date fromDate) {
        System.debug(LoggingLevel.DEBUG, 'ratingGroup=' + ratingGroup + ' fromDate=' + fromDate);
        String soqlString = 
            ' select  ' + 
                ' r.Rating_Number__c,'  + 
                ' r.Id,'  + 
                ' r.Positive_comments__c,'  + 
                ' r.Seat_Number__c,'  + 
                ' r.Cabin_Code__c, '  + 
                ' r.Flight_Date__r.Full_Date__c,'  + 
                ' r.Flight_Date__c,'  +  
                ' r.Flight_Info__r.Flight_Number__c,'  + 
                ' r.Flight_Info__r.Scheduled_Departure_Date__c,'  + 
                ' r.Flight_Info__c,'  + 
                ' r.Passenger__r.First_Name__c,'  + 
                ' r.Passenger__r.Family_Name__c,'  + 
                ' r.Passenger__r.Hidden_Name__c,'  + 
                ' r.Passenger__c,'  + 
                ' r.KLM_Reply__c,'  + 
                ' r.Negative_comments__c,'  +
                ' r.Publish__c,'  + 
                ' r.Country__c,'  + 
                ' r.Language__c,'  + 
                ' r.Hidden_Comments__c,'  + 
                ' r.Hidden_Seat_Number__c,'  + 
                ' r.CreatedDate  ' + 
            ' from Rating__c r ' + 
              ratingGroup.getWhereClause() +  
            ' and r.Flight_Date__r.Full_Date__c >= :fromDate ' + 
            ' order by r.CreatedDate desc ';
            
        System.debug(LoggingLevel.DEBUG, 'soqlString=' + soqlString);
        List<Rating__c> ratings =  Database.Query(soqlString);    
        return ratings;
    }
}