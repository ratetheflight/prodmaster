/**
 * Test class to test the LtcRatingStatistics classes and the /Ratings/Statistics rest endpoint
 * @author       : Mees Witteman
 * @log          : JUN2016: version 1.0
 */
@isTest
public class LtcRatingStatisticsTest { 
    
    @testSetup 
    public static void setup() {
        setupFlights();
        setupRatings();
    }

    static testMethod void searchRatingStatisticsNothingFound() {
        List<LtcRatingStatistics.SelectionGroup> groups = new List<LtcRatingStatistics.SelectionGroup>();
        LtcRatingStatistics.SelectionGroup group1 = new LtcRatingStatistics.SelectionGroup();
        group1.serviceReferenceIdByCaller = 'srID1';
        group1.flightNumber = 'KL9876'; 
        groups.add(group1);
        
        LtcRatingStatistics.ResponseModel model = LtcRatingStatisticsRestInterface.searchRatingStatistics(groups).root;
        System.assert(model.responseGroups.get(0).ratingStatistics == null);
        System.assertEquals(model.responseGroups.get(0).serviceReferenceIdByCaller, 'srID1');
    }

    static testMethod void searchRatingStatisticsCountableRatings() {
        List<LtcRatingStatistics.SelectionGroup> groups = new List<LtcRatingStatistics.SelectionGroup>();
        LtcRatingStatistics.SelectionGroup group1 = new LtcRatingStatistics.SelectionGroup();
        group1.serviceReferenceIdByCaller = 'srID1';
        group1.flightNumber = 'KL1001';
        groups.add(group1);
        
        LtcRatingStatistics.ResponseModel model = LtcRatingStatisticsRestInterface.searchRatingStatistics(groups).root;
        System.assert(model.responseGroups.size() == 1);
        System.assert(model.responseGroups.get(0).ratingStatistics.numberOfRatings == 5);
        System.assert(model.responseGroups.get(0).ratingStatistics.averageRating == 3);

        group1.flightNumber = 'KL1002';
        model = LtcRatingStatisticsRestInterface.searchRatingStatistics(groups).root;
        System.assert(model.responseGroups.get(0).ratingStatistics.numberOfRatings == 1);
        System.assert(model.responseGroups.get(0).ratingStatistics.averageRating == 4);
        
        group1.flightNumber = null;
        group1.country = 'fr';
        
        model = LtcRatingStatisticsRestInterface.searchRatingStatistics(groups).root;
        System.assert(model.responseGroups.get(0).ratingStatistics.numberOfRatings == 5);
        System.assert(model.responseGroups.get(0).ratingStatistics.averageRating > 3);
        System.assert(model.responseGroups.get(0).ratingStatistics.averageRating < 4);
    }
        
    private static void setupFlights() {
        for (Integer i = -5; i < 1; i ++) {
            addFlight(i, 'KL1001', 'AMS', 'LHR');
            addFlight(i, 'KL1002', 'LHR', 'AMS');
            addFlight(i, 'KL1233', 'AMS', 'UNK');
            addFlight(i, 'KL1234', 'UNK', 'AMS');
        }
    }
    
    private static void addFlight(Integer addDays, String flightNumber, String o, String d) {
        Date theDay = Date.today().addDays(addDays);

        Flight__c fl = new Flight__c();
        fl.flight_number__c = flightNumber;
        fl.scheduled_departure_date__c = theDay;
        fl.currentleg__c = 0;
        insert fl;
        
        Leg__c leg = new Leg__c();
        leg.flight__c = fl.id;
        leg.scheduledDepartureDate__c = theDay;
        leg.scheduledDepartureTime__c = '12:00';
        leg.scheduledArrivalDate__c = theDay;
        leg.scheduledArrivalTime__c = '12:00';
        leg.actualArrivalDate__c = theDay;
        leg.actualArrivalTime__c = '12:00';
        leg.legNumber__c = 0;
        leg.timeToArrival__c = 1 * addDays;
        leg.origin__c = o;
        leg.destination__c = d;
        insert leg;
    }
    
    private static void setupRatings() {
        Date theDay = Date.today().addDays(-3);
        String yyyymmdd = DateTime.newInstance(theDay, Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');

        LtcRating ltcRating = new LtcRating();
        ltcRating.setRating('1', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1001', '1A', 'C', yyyymmdd,'AMS', 'LHR',  'nl_NL', null,'','','');
        ltcRating.setRating('2', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1001', '1A', 'C', yyyymmdd,'AMS', 'LHR',  'nl_NL', null,'','','');
        ltcRating.setRating('3', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1001', '1A', 'C', yyyymmdd,'AMS', 'LHR',  'nl_NL', null,'','','');
        ltcRating.setRating('4', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1001', '1A', 'C', yyyymmdd,'AMS', 'LHR',  'fr_FR', null,'','','');
        ltcRating.setRating('5', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1001', '1A', 'C', yyyymmdd,'AMS', 'LHR',  'nl_NL', null,'','','');

        ltcRating.setRating('4', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1002', '1A', 'C', yyyymmdd,'LHR', 'AMS',  'fr_FR',null,'','','');

        ltcRating.setRating('2', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1233', '1A', 'C', yyyymmdd,'AMS', 'UNK',  'fr_FR', null,'','','');
        ltcRating.setRating('3', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1234', '1A', 'C', yyyymmdd,'UNK', 'AMS',  'fr_FR', null,'','','');
        ltcRating.setRating('3', 'pos', 'neg', 'True', 'Kees', 'Test', null, 'KL1234', '1A', 'C', yyyymmdd,'UNK', 'AMS',  'fr_FR', null,'','','');
    }
}