@isTest
public with sharing class LtcRatingTest {
    
    
    static testMethod void singleFlightCall() {
        Monitor_Flight__c monFlight = new Monitor_Flight__c();
        monFlight.Flight_Number__c = 'KL1234';
        monFlight.Languages__c = '["en", "nl", "fr", "es", "de"]';
        insert monFlight;
        
        DateTime aDate = DateTime.now();
        LtcRating rating = new LtcRating();
        System.assert(rating.getMtrFlightNumbers().contains('KL1234'),true);
        rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl', 'KL1234', '11a', 'C', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL',null,'','','');
        System.assert(LtcRatedFlightsRestInterface.getContinentGroupedFlightNumbers().contains('KL1234'));
        
        //setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/Ratings/FlightStatistics/';  
        req.addParameter('flightNumber', 'KL1234');
        req.addParameter('travelDate', aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        // do request
        String result = LtcRatingFlightStatisticsRestInterface.searchRatedFlight();
        System.debug(LoggingLevel.INFO, 'result=' + result);
        System.assert(result.contains('{"level": "3", "count": "1"}'));
    }
    
    static testMethod void oneRatedFlight() { 
        Monitor_Flight__c monFlight = new Monitor_Flight__c();
        monFlight.Flight_Number__c = 'KL1234';
        insert monFlight;
        
        DateTime aDate = DateTime.now();
        LtcRating rating = new LtcRating();
        System.assert(rating.getMtrFlightNumbers().contains('KL1234'),true);
        rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', 'C', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL', null,'','','');
        System.assert(LtcRatedFlightsRestInterface.getContinentGroupedFlightNumbers().contains('KL1234'));
        
        //setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Ratings/FlightsStatistics/';  
        req.addParameter('data', 'KL1234!' + aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        String result = LtcRatingFlightsStatisticsRestInterface.searchRatedFlights(); 
        System.debug(LoggingLevel.INFO, 'result=' + result);
        System.assert(result.contains('{"numberOfRatings": "1", "averageRating": "3"'));
        System.assert(result.contains('{"level": "3", "count": "1"}'));
    }
    
    static testMethod void oneWeekFlightsOneRatingADay() { 
        Monitor_Flight__c monFlight = new Monitor_Flight__c();
        monFlight.Flight_Number__c = 'KL1234';
        insert monFlight;
        
        DateTime aDate = DateTime.now();
        LtcRating rating = new LtcRating();
        
        rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', 'C', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL',null,'','','');
        for (integer x = 1; x < 6; x++) {
            aDate = aDate.addDays(-1);
            rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', 'C', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL', null,'','','');
        }
        
        //setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Ratings/FlightsStatistics/';  
        req.addParameter('data', 'KL1234!' + aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        String result = LtcRatingFlightsStatisticsRestInterface.searchRatedFlights();
        System.debug(LoggingLevel.INFO, 'result=' + result);
        System.assert(result.contains('{"numberOfRatings": "6", "averageRating": "3"'));
        System.assert(result.contains('{"level": "3", "count": "6"}'));
    }
    
    static testMethod void moreRatings() { 
        Monitor_Flight__c monFlight = new Monitor_Flight__c();
        monFlight.Flight_Number__c = 'KL1234';
        insert monFlight;
        
        DateTime aDate = DateTime.now();
        LtcRating rating = new LtcRating();
        rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', 'C', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL', null,'','','');
        for (integer x = 1; x < 10; x++) {
            aDate = aDate.addDays(-7);
            rating.setRating('4', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', 'C', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL', null,'','','');
        }
         
        //setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Ratings/FlightsStatistics/';  
        req.addParameter('data', 'KL1234!' + aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        
        String result = LtcRatingFlightsStatisticsRestInterface.searchRatedFlights();
        System.debug(LoggingLevel.INFO, 'result=' + result);
        System.assert(result.contains('{"numberOfRatings": "10", "averageRating": "3.9"'));
        System.assert(result.contains('{"level": "3", "count": "1"}'));
        System.assert(result.contains('{"level": "4", "count": "9"}'));
    }
   
    static testMethod void sortedRatings() { 
        Monitor_Flight__c monFlight = new Monitor_Flight__c();
        monFlight.Flight_Number__c = 'KL1002';
        insert monFlight;
        
        setupTestRatings('KL1002', 'fr_FR');
        setupTestRatings('KL1002', 'fr_NL');
        setupTestRatings('KL1002', 'fr_CA');
        setupTestRatings('KL1002', 'nl_NL');
        setupTestRatings('KL1002', 'en_NL');
        setupTestRatings('KL1002', 'en_GB');
        setupTestRatings('KL1002', 'en_US');
        setupTestRatings('KL1002', 'en_CA');
        setupTestRatings('KL1002', 'en_FR');
        
        List<Rating__c> rats = new LtcRating().getSortedFlightRatings('KL1002', Date.toDay(), 'fr', 'FR');
        System.assertEquals(27, rats.size());
        System.assertEquals('FR', rats.get(0).country__c);
        System.assertEquals('fr', rats.get(0).language__c);
        System.assertEquals('fr', rats.get(3).language__c);
        System.assert(!'fr'.equals(rats.get(10).language__c));
   }
   
    
    public static void setupTestRatings(String flight, String la_CO) {
        DateTime aDate = DateTime.now();
        LtcRating rating = new LtcRating();
        for (integer x = 2; x < 5; x++) {
            aDate = aDate.addDays(-7 - x);
            rating.setRating('' + x, la_CO, la_CO, 'true', 'fn', 'ln', 'pietje@puk.nl',  flight, '11a', 'C', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  la_CO, null,'','','');
        }
    }
}