@isTest
private class LtcRowIteratorUtilityTest {
	
	static testMethod void tesThreeLines() {
		
		String fileData = 'test1\ntest2\ntest3';
		LtcRowIteratorUtility rit = new LtcRowIteratorUtility(fileData);
		if (rit.hasNext()) {
			System.assertEquals('test1', rit.next());
			System.assertEquals('test2', rit.next());
			System.assertEquals('test3', rit.next());
			System.assertEquals(false, rit.hasNext());
		}
	}
    
}