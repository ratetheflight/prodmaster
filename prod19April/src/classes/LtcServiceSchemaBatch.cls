/**
 * @author (s)    : Mees Witteman
 * @description   : Apex Batch class for loading service menu items
 * @log           : 9 november 2015
                  : 3rd October 2016- Changed Start Date and End Date hardcoding.
                    Moved it to custom setting(LTCSettings) with name 
 *                  'Service Menu Start Date' and 'Service Menu End Date'
 *                  (Modified by Ata)
 *
 * Call anonymous: Id batjobId = Database.executeBatch(new LtcServiceSchemaBatch(), 200);
 */
public class LtcServiceSchemaBatch implements Database.Batchable<sObject>, Database.Stateful {
        
    // the period to handle
    public ServicePeriod__c servicePeriod;
    
    // existing items
    public Map<String, ServiceItem__c> serviceItems = new Map<String, ServiceItem__c>();
    public CabinClass__c economyClass;
    public CabinClass__c businessClass;
    
    public Service_Record__c previousRecord = new Service_Record__c();
    
    public List<LegClassPeriod__c> legClassPeriods= new List<LegClassPeriod__c>();
    public List<LegClassItem__c> legClassItems = new List<LegClassItem__c>();
    
    //public Date startDate = Date.valueOf('2016-05-01');
    //public Date endDate = Date.valueOf('2016-10-29');
    
    //Fetch 'Start Date' and 'End Date' data from cutom setting
    public Date startDate = Date.valueof(LtcSettings__c.getInstance('Service Menu Start Date').value__c);
    public Date endDate =   Date.valueof(LtcSettings__c.getInstance('Service Menu End Date').value__c);
    
    public String itemName;
    public String itemCode;
    public String itemType;
        
    public database.querylocator start(Database.BatchableContext BC) {
        initialize();
        String query = 
            ' select Id, Flight_No__c,Leg__c, Class__c, Index__c, Template__c, Description__c ' + 
            ' from Service_Record__c  order by Flight_No__c, Leg__c, Class__c, Index__c';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('scope.size()=' + scope.size());
        
        Service_Record__c startRecord = previousRecord;
        
        List<LegClassPeriod__c> lcpsToUpdate = new List<LegClassPeriod__c>();
        Set<ServiceItem__c> serviceItemsToUpdate = new Set<ServiceItem__c>();
        
        LegClassPeriod__c legClassPeriod;
        ServiceItem__c serviceItem;
        LegClassItem__c legClassItem;
        
        for (sObject scopeEntry : scope) {
            Service_Record__c record = (Service_Record__c) scopeEntry;
            System.debug(Logginglevel.INFO, 'process record ' + record);
            
            if (isNewLegClass(record)) {
                if ('C'.equals(record.Class__c)) {
                    legClassPeriod = createLegClassPeriod(record, businessClass);
                } else {
                    legClassPeriod  = createLegClassPeriod(record, economyClass);
                }               
                legClassPeriods.add(legClassPeriod);
                lcpsToUpdate.add(legClassPeriod);
                System.debug(LoggingLevel.DEBUG, 'new legClassPeriod=' + legClassPeriod);
            } else {
                System.debug(LoggingLevel.DEBUG, 'use existing legClassPeriod=' + legClassPeriod);
            }
            
            itemCode = stripCode(record.Template__c);
            itemName = stripItemName(record.Description__c);
            itemType = getTypeFromName(itemName);
            itemName = correctItemName(itemName, itemCode, itemType);
            itemType = correctItemType(itemName, itemType);
            if (isValidItem(itemName)) {
                serviceItem = createServiceItem(itemName, itemType);
                serviceItems.put(serviceItem.name__c, serviceItem);
                serviceItemsToUpdate.add(serviceItem);
            }
            
            if (serviceItemsToUpdate.size() > 10) {
                upsert new List<ServiceItem__c>(serviceItemsToUpdate);
                serviceItemsToUpdate = new Set<ServiceItem__c>();
            }
            
            if (lcpsToUpdate.size() > 100) {
                upsert lcpsToUpdate;
                lcpsToUpdate = new List<LegClassPeriod__c>();
            }
            previousRecord = record;
        }
        upsert serviceItems.values();
        upsert lcpsToUpdate;
            
        System.debug(LoggingLevel.DEBUG, 'second iteration for leg class items');
        previousRecord = new Service_Record__c();
        for (sObject scopeEntry : scope) {
            Service_Record__c record = (Service_Record__c) scopeEntry;
            System.debug(Logginglevel.INFO, 'process record ' + record);
            
            if (isNewLegClass(record)) {
                if ('C'.equals(record.Class__c)) {
                    legClassPeriod = createLegClassPeriod(record, businessClass);
                } else {
                    legClassPeriod  = createLegClassPeriod(record, economyClass);
                }
            }
            
            itemCode = stripCode(record.Template__c);
            itemName = stripItemName(record.Description__c);
            itemType = getTypeFromName(itemName);
            itemName = correctItemName(itemName, itemCode, itemType);
            itemType = correctItemType(itemName, itemType);
            if (isValidItem(itemName)) {
                legClassItem = createLegClassItem(itemName, legClassPeriod, (Integer) record.Index__c);
                legClassItems.add(legClassItem);
                System.debug(Logginglevel.DEBUG, 'added LegClassItem=' + legClassItem);
            }

            if (legClassItems.size() > 100) {
                upsert legClassItems;
                legClassItems = new List<LegClassItem__c>();
            }
            
            previousRecord = record;
        }
        upsert legClassItems;

    }

    private Boolean isNewLegClass(Service_Record__c record) {
        return record.flight_no__c != previousRecord.flight_no__c 
            || record.Leg__c != previousRecord.Leg__c
            || record.Class__c != previousRecord.Class__c;
    }
    
    public void finish(Database.BatchableContext BC){
    }
    
    // ---------------------UTIL METHODS ---------------------------------------------------------
    
    private Boolean isValidItem(String itemName) {
        Boolean result = true;
        if (itemName == null) {
            result = false;
        } 
        else if (''.equals(itemName)) {
            result = false;
        }
        else if (itemName.toUppercase().contains('GARNITURE')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('NOSERVICE')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('WINESERVICE')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('BONBONS')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('FRIANDIS')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('VIENNOIS')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('HOTROLL')) {
            result = false;
        }
        else if (itemName.toUppercase().equals('BULK')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('COFFEE') && !itemName.toUppercase().contains('DRINKSERVICE')) {
            result = false;
        }
        else if (itemName.toUppercase().startsWith('DESSERT')) {
            result = false;
        }
        else if (itemName.toUppercase().startsWith('BAGS')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('E210')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('E295')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('E296')) {
            result = false;
        }
        else if (itemName.toUppercase().contains('E297')) {
            result = false;
        }
        return result;
    }
    
    private LegClassPeriod__c createLegClassPeriod(Service_Record__c record, CabinClass__c cabinClass) {
        String flightNumber = 'KL';
        if (record.flight_No__c < 1000) {
            flightNumber += '0' + (Integer) record.flight_No__c;
        } else {
            flightNumber += (Integer) record.flight_No__c;
        }
        
        system.debug('startDate::'+startDate);
        Integer legNr = (Integer) record.leg__c - 1;
        LegClassPeriod__c lcp = findLegClassPeriod(flightNumber, legNr, cabinClass);
        if (lcp == null) {
            lcp = new LegClassPeriod__c();
            lcp.CabinClass__c = cabinClass.id;
            lcp.FlightNumber__c = flightNumber;
            lcp.legNumber__c = legNr;
            lcp.StartDate__c = startDate;
            lcp.EndDate__c = endDate;
            lcp.ServicePeriod__c = servicePeriod.id;
            System.debug(Logginglevel.DEBUG, 'new leg/class/period flight=' + lcp.flightnumber__c + ' leg=' + lcp.legnumber__c + ' class=' + cabinClass.name + ' start=' + lcp.startDate__c  + ' end=' + lcp.endDate__c);
        } else {
            System.debug(Logginglevel.DEBUG, 'existing leg/class/period=' + lcp);
        }
        return lcp;
    }
    
    private LegClassPeriod__c findLegClassPeriod(String flightNumber, Integer legNr, CabinClass__c cabinClass) {
        System.debug(Logginglevel.DEBUG, 'findLegClassPeriod ' + flightNumber + ' ' + legNr + ' ' + cabinClass);
        for (LegClassPeriod__c lcp : legClassPeriods) {
            if (lcp.cabinClass__c.equals(cabinClass.id)
                    && lcp.flightNumber__c.equals(flightNumber)
                    && lcp.legNumber__c == legNr
                    && lcp.StartDate__c.isSameDay(startDate)
                    && lcp.EndDate__c.isSameDay(endDate)) {
                return lcp;
            }
        }
        return null;
    }       
    
    private ServiceItem__c createServiceItem(String serviceItemName, String itemType) {
        ServiceItem__c si = findServiceItem(serviceItemName);
        if (si == null) {
            si = new ServiceItem__c();
            si.name__c = 'ServiceItem_' + serviceItemName + '_name';
            si.description__c = 'ServiceItem_' + serviceItemName + '_description';
            // log the label translation sheet entries for new created service items
            System.debug(Logginglevel.INFO, 'si:\t\t{');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.name__c + '",');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
            System.debug(Logginglevel.INFO, 'si:\t\t},');
            System.debug(Logginglevel.INFO, 'si:\t\t{');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.description__c + '",');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
            System.debug(Logginglevel.INFO, 'si:\t\t},');
            
        } 
        si.type__c = itemType;
        return si;
    }
    
    private ServiceItem__c findServiceItem(String serviceItemName) {
        String siName = 'ServiceItem_' + serviceItemName + '_name';
        ServiceItem__c si = serviceItems.get(siName);
        return si;
    }
        
    private LegClassItem__c createLegClassItem(String serviceItemName, LegClassPeriod__c legClassPeriod, Integer order) {
        ServiceItem__c si = findServiceItem(serviceItemName);
        System.debug('service item si=' + si + ' serviceItemName=' + serviceItemName + ' legClassPeriod=' + legClassPeriod + ' order=' + order);
        LegClassItem__c lci = new LegClassItem__c();
        lci.LegClassPeriod__c = legClassPeriod.id;
        lci.serviceItem__c = si.id; 
        lci.order__c = order;
        return lci;
    }
    
    private String correctItemName(String itemName, String itemCode, String itemType) {
        String result = itemName;
        
        if ('Box'.equalsIgnoreCase(itemName)) {
            result = itemCode;
        }
        /** the `stef` replacements */
        else if ('e222f'.equals(itemCode)) {
            result = 'e222';
        }
        else if ('e517t'.equals(itemCode)) {
            result = 'e517';
        }
        else if ('e518'.equals(itemCode)) {
            result = 'e517';
        }
        else if ('e522'.equals(itemCode)) {
            result = 'e521';
        }
        else if ('e526'.equals(itemCode)) {
            result = 'e525';
        }
        else if ('e534'.equals(itemCode)) {
            result = 'e533';
        }
        else if ('e540'.equals(itemCode)) {
            result = 'e539';
        }
        else if ('e540t'.equals(itemCode)) {
            result = 'e539';
        }
        else if ('drinkservice-coffee-tea'.equals(itemName)) {
            result = 'drinkservice';
        }
        /** meal default to code */
        else if ('meal'.equals(itemType) && !''.equals(itemCode)) {
            result = itemCode; 
        }
        return result;
    }
    
    private String correctItemType(String itemName, String itemType) {
        String result = itemType;
        if ('e653'.equals(itemName) 
                || 'e654'.equals(itemName) 
                || 'e655'.equals(itemName)) {
            result = 'meal';
        } 
        else if ('e651'.equals(itemName) 
                || 'e652'.equals(itemName) )  {
            result = 'snack';
        }
        return result;
    }

    private String stripItemName(String input) {
        if (input == null) return '';
        String output = input.trim();
        if (output.contains('-')) {
            output = output.substring(output.indexOf('-') + 1).trim();
        }
        output = output.replaceAll(' ', '');
        output = output.replaceAll('/', '-');
        output = output.replaceAll('%', '');
        output = output.toLowerCase();
        return output;
    }
    
    private String stripCode(String input) {
        if (input == null) return '';
        String output = input.trim();
        
        if (output.indexOf(' ') != -1) {
            output = output.subString(0, output.indexOf(' ') + 1).trim();
        }
        
        output = output.replaceAll(' ', '');
        if ('-'.equals(output)) {
            output = '';
        }
        if (output.indexOf('-') != -1) {
            output = output.subString(0, output.indexOf('-'));
        }
        output = output.toLowerCase(); 
        return output;
    }
    
    private String getTypeFromName(String serviceItemName) {
        String result = 'other';
        if (serviceItemName.contains('drink')) {
            result = 'drinks';
        } else if (serviceItemName.contains('drk')) {
            result = 'drinks';
        } else if (serviceItemName.contains('meal')) {
            result = 'meal';
        } else if (serviceItemName.contains('tidbi')) {
            result = 'tidbits';
        } else if (serviceItemName.contains('water')) {
            result = 'drinks';
        } else if (serviceItemName.contains('juice')) {
            result = 'drinks';
        } else if (serviceItemName.contains('bonbon')) {
            result = 'snack';
        } else if (serviceItemName.contains('mnml')) {
            result = 'meal';
        } else if (serviceItemName.contains('dinner')) {
            result = 'meal';
        } else if (serviceItemName.contains('lun')) {
            result = 'meal';
        } else if (serviceItemName.contains('wine')) {
            result = 'drinks';
        } else if (serviceItemName.contains('beverage')) {
            result = 'drinks';
        } else if (serviceItemName.contains('juice')) {
            result = 'drinks';
        } else if (serviceItemName.contains('coffee')) {
            result = 'drinks';
        } else if (serviceItemName.contains('towel')) {
            result = 'towel';
        } else if (serviceItemName.contains('icecream')) {
            result = 'icecream';
        } else if (serviceItemName.contains('ice cream')) {
            result = 'icecream';
        } else if (serviceItemName.contains('breakfast')) {
            result = 'meal';
        } else if (serviceItemName.contains('bkf')) {
            result = 'meal';
        } else if (serviceItemName.contains('dess')) {
            result = 'meal';
        } else if (serviceItemName.contains('noodle')) {
            result = 'meal';
        } else if (serviceItemName.contains('friandise')) {
            result = 'meal';
        } else if (serviceItemName.contains('garniture')) {
            result = 'meal';
        } else if (serviceItemName.contains('combi')) {
            result = 'meal';
        } else if (serviceItemName.contains('yours')) {
            result = 'meal';
        } else if (serviceItemName.contains('sand')) {
            result = 'snack';
        } else if (serviceItemName.contains('cookie')) {
            result = 'snack';
        } else if (serviceItemName.contains('snack')) {
            result = 'snack';
        } else if (serviceItemName.contains('sale')) {
            result = 'sales';
        } else {
            result = 'other';
        }
        System.debug(LoggingLevel.INFO, 'name=' + serviceItemName + ' type=' + result);
        return result;
    }
    
    
    // --------------------------- BEHIND THIS POINT INITS ONLY ---------------------------------------
    private void initialize() {
        initPeriodData();
        loadCurrentRecords();
    }
    
    private void initPeriodData() {
        
        string season =  String.Valueof(LtcSettings__c.getInstance('Season').value__c);
        List<ServicePeriod__c> sps = [
            Select Id, name__c
            From ServicePeriod__c
            where name__c =: season 
        ];
        
        if (sps != null && !sps.isEmpty()) {
            servicePeriod = sps[0];
        } 
        else {
            servicePeriod = new ServicePeriod__c();
            servicePeriod.name__c = season;
            insert servicePeriod;
        }
    }
    
    private void loadCurrentRecords() {
        System.debug('init loadCurrent Records');
        economyClass = [
            Select Name, Id, label__c
            From CabinClass__c  
            where name =: 'economy'
        ];
        businessClass= [
            Select Name, Id, label__c
            From CabinClass__c 
            where name =: 'business'
        ];
        
        System.debug('economy=' + economyClass + ' businessClass=' + businessClass);
        
        List<ServiceItem__c> sis = [select id, name__c, description__c from ServiceItem__c ];
        for (ServiceItem__c si : sis) {
            serviceItems.put(si.name__c, si);
        }
        
        legClassPeriods = [
            select id, CabinClass__c, EndDate__c, FlightNumber__c, LegNumber__c, ServicePeriod__c, StartDate__c
            from LegClassPeriod__c
            where ServicePeriod__c =: servicePeriod.id
        ];
    }
    
}