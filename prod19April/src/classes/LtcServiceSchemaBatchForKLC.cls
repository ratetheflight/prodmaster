/**
 * @author (s)    : Mees Witteman,Satheeshkumar Subramani
 * @description   : Apex Batch class for loading KLC service menu items
 * @log           : 23 november 2015
 *                : 19th October 2016 - Created Service menu Start date, end date and season
 *                  in custom setting(LTCSettings) instead of making hard code. 
 *                  (Modified by Ashok)                    
 * Call anonymous: Id batjobId = Database.executeBatch(new LtcServiceSchemaBatchForKLC(), 200);
 */
public class LtcServiceSchemaBatchForKLC implements Database.Batchable<sObject>, Database.Stateful {
        
    // the period to handle
    public ServicePeriod__c servicePeriod;
    
    // existing items
    public Map<String, ServiceItem__c> serviceItems = new Map<String, ServiceItem__c>();
    public CabinClass__c economyClass;
    public CabinClass__c businessClass;
    
    public Service_Record__c previousRecord = new Service_Record__c();
    
    public List<LegClassPeriod__c> legClassPeriods= new List<LegClassPeriod__c>();
    public List<LegClassItem__c> legClassItems = new List<LegClassItem__c>();   
       
     //Fetch 'Start Date' and 'End Date' data from cutom setting(LtcSettings)
    public Date startDate = Date.valueof(LtcSettings__c.getInstance('Service Menu Start Date').value__c);
    public Date endDate =   Date.valueof(LtcSettings__c.getInstance('Service Menu End Date').value__c);
    
    public String itemName;
    public String itemType;
        
    public database.querylocator start(Database.BatchableContext BC) {
        initialize();
        String query = 
            ' select Id, Flight_No__c,Leg__c, Class__c, Index__c, Template__c, Description__c ' + 
            ' from Service_Record__c  order by Flight_No__c, Leg__c, Class__c, Index__c';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('scope.size()=' + scope.size());
        
        Service_Record__c startRecord = previousRecord;
        
        List<LegClassPeriod__c> lcpsToUpdate = new List<LegClassPeriod__c>();
        Set<ServiceItem__c> serviceItemsToUpdate = new Set<ServiceItem__c>();
        
        LegClassPeriod__c legClassPeriod;
        ServiceItem__c serviceItem;
        LegClassItem__c legClassItem;
        
        for (sObject scopeEntry : scope) {
            Service_Record__c record = (Service_Record__c) scopeEntry;
            System.debug(Logginglevel.INFO, 'process record ' + record);
            
            if (isNewLegClass(record)) {
                if ('C'.equals(record.Class__c)) {
                    legClassPeriod = createLegClassPeriod(record, businessClass);
                } else {
                    legClassPeriod  = createLegClassPeriod(record, economyClass);
                }               
                legClassPeriods.add(legClassPeriod);
                lcpsToUpdate.add(legClassPeriod);
                System.debug('new legClassPeriod=' + legClassPeriod);
            } else {
                System.debug('no new legClassPeriod=' + legClassPeriod);
            }
            
            itemName = stripItemName(record.Description__c);
            itemName = correctItemName(itemName);
            itemType = getTypeFromName(itemName);       
            serviceItem = createServiceItem(itemName, itemType);
            serviceItems.put(serviceItem.name__c, serviceItem);
            serviceItemsToUpdate.add(serviceItem);
            
            
            if (serviceItemsToUpdate.size() > 10) {
                upsert new List<ServiceItem__c>(serviceItemsToUpdate);
                serviceItemsToUpdate = new Set<ServiceItem__c>();
            }
            
            if (lcpsToUpdate.size() > 100) {
                upsert lcpsToUpdate;
                lcpsToUpdate = new List<LegClassPeriod__c>();
            }
            previousRecord = record;
        }
        upsert serviceItems.values();
        upsert lcpsToUpdate;
        
        System.debug(LoggingLevel.DEBUG, 'second iteration for leg class items');
        previousRecord = new Service_Record__c();
        for (sObject scopeEntry : scope) {
            Service_Record__c record = (Service_Record__c) scopeEntry;
            System.debug(Logginglevel.INFO, 'process record ' + record);
            
            if (isNewLegClass(record)) {
                if ('C'.equals(record.Class__c)) {
                    legClassPeriod = createLegClassPeriod(record, businessClass);
                } else {
                    legClassPeriod  = createLegClassPeriod(record, economyClass);
                }               
            }
            
            itemName = stripItemName(record.Description__c);
            itemName = correctItemName(itemName);
            itemType = getTypeFromName(itemName);   
            legClassItem = createLegClassItem(itemName, legClassPeriod, (Integer) record.Index__c);
            legClassItems.add(legClassItem);
        

            if (legClassItems.size() > 100) {
                upsert legClassItems;
                legClassItems = new List<LegClassItem__c>();
            }
            
            previousRecord = record;
        }
        upsert legClassItems;

    }

    private Boolean isNewLegClass(Service_Record__c record) {
        return record.flight_no__c != previousRecord.flight_no__c 
            || record.Leg__c != previousRecord.Leg__c
            || record.Class__c != previousRecord.Class__c;
    }
    
    public void finish(Database.BatchableContext BC){
    }
    
    // ---------------------UTIL METHODS ---------------------------------------------------------
    
    
    
    private LegClassPeriod__c createLegClassPeriod(Service_Record__c record, CabinClass__c cabinClass) {
        String flightNumber = 'KL';
        if (record.flight_No__c < 1000) {
            flightNumber += '0' + (Integer) record.flight_No__c;
        } else {
            flightNumber += (Integer) record.flight_No__c;
        }
        
        Integer legNr = (Integer) record.leg__c - 1;
        LegClassPeriod__c lcp = findLegClassPeriod(flightNumber, legNr, cabinClass);
        if (lcp == null) {
            lcp = new LegClassPeriod__c();
            lcp.CabinClass__c = cabinClass.id;
            lcp.FlightNumber__c = flightNumber;
            lcp.legNumber__c = legNr;
            lcp.StartDate__c = startDate;
            lcp.EndDate__c = endDate;
            lcp.ServicePeriod__c = servicePeriod.id;
            System.debug('new leg/class/period flight=' + lcp.flightnumber__c + ' leg=' + lcp.legnumber__c + ' class=' + cabinClass.name + ' start=' + lcp.startDate__c  + ' end=' + lcp.endDate__c);
        } else {
            System.debug('existing leg/class/period=' + lcp);
        }
        return lcp;
    }
    
    private LegClassPeriod__c findLegClassPeriod(String flightNumber, Integer legNr, CabinClass__c cabinClass) {
        System.debug('findLegClassPeriod ' + flightNumber + ' ' + legNr + ' ' + cabinClass);
        for (LegClassPeriod__c lcp : legClassPeriods) {
            if (lcp.cabinClass__c.equals(cabinClass.id)
                    && lcp.flightNumber__c.equals(flightNumber)
                    && lcp.legNumber__c == legNr
                    && lcp.StartDate__c.isSameDay(startDate)
                    && lcp.EndDate__c.isSameDay(endDate)) {
                return lcp;
            }
        }
        return null;
    }       
    
    private ServiceItem__c createServiceItem(String serviceItemName, String itemType) {
        ServiceItem__c si = findServiceItem(serviceItemName);
        if (si == null) {
            si = new ServiceItem__c();
            si.name__c = 'ServiceItem_klc_' + serviceItemName + '_name';
            si.description__c = 'ServiceItem_klc_' + serviceItemName + '_description';
            // log the label translation sheet entries for new created service items
            System.debug(Logginglevel.INFO, 'si:\t\t{');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.name__c + '",');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
            System.debug(Logginglevel.INFO, 'si:\t\t},');
            System.debug(Logginglevel.INFO, 'si:\t\t{');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.description__c + '",');
            System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
            System.debug(Logginglevel.INFO, 'si:\t\t},');
            
        } 
        si.type__c = itemType;
        return si;
    }
    
    private ServiceItem__c findServiceItem(String serviceItemName) {
        String siName = 'ServiceItem_klc_' + serviceItemName + '_name';
        ServiceItem__c si = serviceItems.get(siName);
        return si;
    }
        
    private LegClassItem__c createLegClassItem(String serviceItemName, LegClassPeriod__c legClassPeriod, Integer order) {
        ServiceItem__c si = findServiceItem(serviceItemName);
        System.debug('service item si=' + si + ' serviceItemName=' + serviceItemName + ' legClassPeriod=' + legClassPeriod + ' order=' + order);
        LegClassItem__c lci = new LegClassItem__c();
        lci.LegClassPeriod__c = legClassPeriod.id;
        lci.serviceItem__c = si.id; 
        lci.order__c = order;
        return lci;
    }
    
    
    private String stripItemName(String input) {
        if (input == null) return '';
        String output = input.trim();
        if (output.contains('-')) {
            output = output.substring(output.indexOf('-') + 1).trim();
        }
        output = output.replaceAll(' ', '');
        output = output.replaceAll('/', '-');
        output = output.replaceAll('\\*', '');
        output = output.toLowerCase();
        return output;
    }
    
    private String correctItemName(String itemName) {
        String result = itemName;
        
        if ('Choice3'.equalsIgnoreCase(itemName)) {
            result = 'choice1';
        }
        else if ('Choice4'.equalsIgnoreCase(itemName)) {
            result = 'choice1';
        }
        else if ('Choice5'.equalsIgnoreCase(itemName)) {
            result = 'choice1';
        }
        
        return result;
    }
    
    private String getTypeFromName(String serviceItemName) {
        String result = 'other';
        if (serviceItemName.contains('water')) {
            result = 'drinks';
        } else if (serviceItemName.contains('juice')) {
            result = 'drinks';
        }  else if (serviceItemName.contains('choice')) {
            result = 'drinks';
        }  else if (serviceItemName.contains('lun')) {
            result = 'meal';
        }  else if (serviceItemName.contains('dinner')) {
            result = 'meal';
        }  else if (serviceItemName.contains('breakfast')) {
            result = 'meal';
        }  else if (serviceItemName.contains('chips')) {
            result = 'snack';
        }  else if (serviceItemName.contains('fresh')) {
            result = 'snack';
        }  else if (serviceItemName.contains('snack')) {
            result = 'snack';
        } 
         else {
            result = 'other';
        }
        System.debug(LoggingLevel.INFO, 'name=' + serviceItemName + ' type=' + result);
        return result;
    }
    
    
    // --------------------------- BEHIND THIS POINT INITS ONLY ---------------------------------------
    private void initialize() {
        initPeriodData();
        loadCurrentRecords();
    }
    
    private void initPeriodData() {
        string season =  String.Valueof(LtcSettings__c.getInstance('Season').value__c);
        List<ServicePeriod__c> sps = [
            Select Id, name__c
            From ServicePeriod__c
            where name__c =: season
        ];
        
        if (sps != null && !sps.isEmpty()) {
            servicePeriod = sps[0];
        } 
        else {
            servicePeriod = new ServicePeriod__c();
            servicePeriod.name__c = season;
            insert servicePeriod;
        }
    }
    
    private void loadCurrentRecords() {
        System.debug('init loadCurrent Records');
        economyClass = [
            Select Name, Id, label__c
            From CabinClass__c  
            where name =: 'economy'
        ];
        businessClass= [
            Select Name, Id, label__c
            From CabinClass__c 
            where name =: 'business'
        ];
        
        System.debug('economy=' + economyClass + ' businessClass=' + businessClass);
        
        List<ServiceItem__c> sis = [select id, name__c, description__c from ServiceItem__c ];
        for (ServiceItem__c si : sis) {
            serviceItems.put(si.name__c, si);
        }
        
        legClassPeriods = [
            select id, CabinClass__c, EndDate__c, FlightNumber__c, LegNumber__c, ServicePeriod__c, StartDate__c
            from LegClassPeriod__c
            where ServicePeriod__c =: servicePeriod.id
        ];
    }
    
}