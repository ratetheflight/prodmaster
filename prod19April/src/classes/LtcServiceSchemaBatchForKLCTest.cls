/**
 * LtcServiceSchemaBatchForKLC Test class
 */
@isTest
private class LtcServiceSchemaBatchForKLCTest {
              
    static testMethod void startTest() {
        initTest();
        LtcServiceSchemaBatchForKLC batch = new LtcServiceSchemaBatchForKLC();
        batch.start(null);
    }
    
    static testMethod void executeTest() {
        initTest();
        Id batchjobId = Database.executeBatch(new LtcServiceSchemaBatchForKLC(), 200);
        System.assert(batchjobId != null);
    }
    
    static testMethod void finishTest() {
        initTest();
        LtcServiceSchemaBatchForKLC batch = new LtcServiceSchemaBatchForKLC();
        batch.finish(null);
    }   
    
    static void initTest() {
        
        setupData();
                   
        CabinClass__c economyClass = new CabinClass__c();
        economyClass.name = 'economy';
        economyClass.label__c = 'economy';
        insert economyClass;
        
        CabinClass__c businessClass = new CabinClass__c();
        businessClass.name = 'business';
        businessClass.label__c = 'business';
        insert businessClass;
        
        Service_Record__c sr = new Service_Record__c();
        sr.Class__c = 'M';
        sr.Description__c = 'Fresh sandwich';
        sr.Flight_No__c = 1880;
        sr.Leg__c = 1;
        sr.Template__c = '';
        sr.Index__c = 1;
        insert sr;
        
        sr = new Service_Record__c();
        sr.Class__c = 'C';
        sr.Description__c = 'Lunch*';
        sr.Flight_No__c = 1641;
        sr.Leg__c = 1;
        sr.Template__c = '';
        sr.Index__c = 1;
        insert sr;
        
        sr = new Service_Record__c();
        sr.Class__c = 'C';
        sr.Description__c = 'Choice 1*';
        sr.Flight_No__c = 1641;
        sr.Leg__c = 1;
        sr.Template__c = '';
        sr.Index__c = 1;
        insert sr;
        
        Test.startTest();
    }
    
    @TestSetup static void setupData() { 
        List<LtcSettings__c> ltcSetting = new List<LtcSettings__c>();
         
        LtcSettings__c csLtcFieldSeason = new LtcSettings__c();
        csLtcFieldSeason.Name = 'Season';
        csLtcFieldSeason.name__c = 'Season';
        csLtcFieldSeason.value__c = 'Winter 16';
        ltcSetting.add(csLtcFieldSeason);
    
        LtcSettings__c csLtcFieldServiceStartDate = new LtcSettings__c();
        csLtcFieldServiceStartDate.Name = 'Service Menu Start Date';
        csLtcFieldServiceStartDate.name__c = 'StartDate';
        csLtcFieldServiceStartDate.value__c = '2016-10-30';
        ltcSetting.add(csLtcFieldServiceStartDate);
        
        LtcSettings__c csLtcFieldServiceEndtDate = new LtcSettings__c();
        csLtcFieldServiceEndtDate.Name = 'Service Menu End Date';
        csLtcFieldServiceEndtDate.name__c = 'EndDate';
        csLtcFieldServiceEndtDate.value__c = '2017-03-25';
        ltcSetting.add(csLtcFieldServiceEndtDate);
        
        insert ltcSetting;
        }
}