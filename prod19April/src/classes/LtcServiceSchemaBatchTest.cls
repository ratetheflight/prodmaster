/**
 * LtcServiceSchemaBatch Test class
 */
@isTest
private class LtcServiceSchemaBatchTest {

    static testMethod void startTest() {
        initTest();
        LtcServiceSchemaBatch batch = new LtcServiceSchemaBatch();
        batch.start(null);
    }
    
    static testMethod void executeTest() {
        initTest();
        Id batchjobId = Database.executeBatch(new LtcServiceSchemaBatch(), 200);
        System.assert(batchjobId != null);
    }
    
    static testMethod void finishTest() {
        initTest();
        LtcServiceSchemaBatch batch = new LtcServiceSchemaBatch();
        batch.finish(null);
    }
    
    static void initTest() {
        List<LtcSettings__c> Lsetting = new List<LtcSettings__c>();
        
        LtcSettings__c seasonStart = new LtcSettings__c();
        seasonStart.name = 'Service Menu Start Date';
        seasonStart.name__c = 'StartDate';
        seasonStart.Value__c = '2016-10-30';
        Lsetting.add(seasonStart);
        
        LtcSettings__c seasonEnd = new LtcSettings__c();
        seasonEnd.name = 'Service Menu End Date';
        seasonEnd.name__c = 'EndDate';
        seasonEnd.Value__c = '2017-03-25';
        Lsetting.add(seasonEnd);
        
        LtcSettings__c currentSeason = new LtcSettings__c();
        currentSeason.name = 'Season';
        currentSeason.name__c = 'Season';
        currentSeason.Value__c = 'Winter 16';
        Lsetting.add(currentSeason);
        
        insert Lsetting;
        
        CabinClass__c economyClass = new CabinClass__c();
        economyClass.name = 'economy';
        economyClass.label__c = 'economy';
        insert economyClass;
        
        CabinClass__c businessClass = new CabinClass__c();
        businessClass.name = 'business';
        businessClass.label__c = 'business';
        insert businessClass;
        
        Service_Record__c sr = new Service_Record__c();
        sr.Class__c = 'M';
        sr.Description__c = 'Main meal Korean';
        sr.Flight_No__c = 123;
        sr.Leg__c = 1;
        sr.Template__c = 'WhroeHaha';
        sr.Index__c = 1;
        insert sr;
        
        sr = new Service_Record__c();
        sr.Class__c = 'C';
        sr.Description__c = 'E653';
        sr.Flight_No__c = 9999;
        sr.Leg__c = 1;
        sr.Template__c = 'WhroeHaha';
        sr.Index__c = 1;
        insert sr;
        
        sr = new Service_Record__c();
        sr.Class__c = 'C';
        sr.Description__c = 'E297 #12';
        sr.Flight_No__c = 9999;
        sr.Leg__c = 1;
        sr.Template__c = 'WhroeHaha';
        sr.Index__c = 1;
        insert sr;
        
        Test.startTest();
    }
}