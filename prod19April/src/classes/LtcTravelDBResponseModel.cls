/**                     
 * @author (s)      : Ata
 * @description     : Model Class to contain travelDB info
 * @log:   20JAN2017: version 1.0
 * 
 */
 global class LtcTravelDBResponseModel {
     public List<Itinerary> itinerary;
     public String errorResponse;
     public Passenger passengerData;
     public LtcTravelDBResponseModel(String errorResponse) {
        this.errorResponse = errorResponse;
     }
     public LtcTravelDBResponseModel(){
     }
     public LtcTravelDBResponseModel(List<Itinerary> itinerary, Passenger passengerData){
         this.itinerary = itinerary;
         this.passengerData = passengerData;
     }
     public override String toString() {
        String itineraryString = null;
        if (itinerary != null) {
            itineraryString = '[';
            for (Itinerary itn : this.itinerary) {
                itineraryString = itineraryString + itn.toString() + ',';
            }            
            itineraryString = itineraryString.substring(0, itineraryString.length() - 1);
            itineraryString = itineraryString + ']';
        }else {
            return errorResponse = 'PNR not found in database.';           
        }        
        return '{' +
                '"itinerary": ' + itineraryString  +  ', ' +
                '"passenger": ' + passengerData.toString() +       
            '}';
     }
     public boolean isEmpty() {
        return (itinerary == null);
    }   
     global class Itinerary implements Comparable{
         public String flightNumber;
         public String travelDate;
         public String origin;
         public String destination;
         public Integer seqNo;
         public String uuid;
         
         public Itinerary(String fNo, String tDate, String orgn, String dest, Integer seq ){
             flightNumber =  fNo;  
             travelDate = tDate;
             origin = orgn;
             destination = dest;
             seqNo = seq;           
         }
         
         public override String toString() {
             if(uuid == null){
                 return '{' +
                '"flightNumber": "' + flightNumber +  '", ' +
                '"travelDate": "' + travelDate +  '", ' +
                '"origin": "' + origin +  '", ' +
                '"destination": "' + destination +  '", ' +
                '"ratingToken": ' + uuid +  ', ' +
                '"seqNo": ' + String.valueOf(seqNo) +
            '}';    
             }else{
             return '{' +
                '"flightNumber": "' + flightNumber +  '", ' +
                '"travelDate": "' + travelDate +  '", ' +
                '"origin": "' + origin +  '", ' +
                '"destination": "' + destination +  '", ' +
                '"ratingToken": "' + uuid +  '", ' +
                '"seqNo": ' + String.valueOf(seqNo) +
            '}';
            }
         }
         public DateTime getTimeStamp(){
             String dateString = travelDate;
             List<String> dList =  dateString.split('T');
             List<String> dateLiterals = dList[0].split('-');
             List<String> timeLiterals = dList[1].split(':');
             DateTime dt = DateTime.newInstance(Integer.valueof(dateLiterals[0]),Integer.valueof(dateLiterals[1]),Integer.valueof(dateLiterals[2]),Integer.valueof(timeLiterals[0]),Integer.valueof(timeLiterals[1]),0);         
             return dt ;
         }   
         public Integer compareTo(Object obj) {
            LtcTravelDBResponseModel.Itinerary itn = (LtcTravelDBResponseModel.Itinerary)(obj);
            SYstem.debug('comparing:::'+this.getTimeStamp()+'#########'+itn.getTimeStamp());
            if (this.getTimeStamp().getTime() > itn.getTimeStamp().getTime()) {
                return 1;
            }

            if (this.getTimeStamp().getTime() == itn.getTimeStamp().getTime()) {
                return 0;
            }

            return -1;
        }
     }     
     global class Passenger {
         public String name;
         public String surName;
         public String email; 
         
         public Passenger(String fName, String familyName, String mail) {
            name = removePrefix(fName);
            surName = familyName;
            email = mail;  
         }
         public override String toString() {
            return '{' +
                '"name": "' + name +  '", ' +
                '"surname": "' + surName +  '", ' +
                '"email": "' + email + '"'+
            '}';
         } 
         public String removePrefix(String name){
             Set<String> prefix = new Set<String>{'MISS','MR','MS','MRS'};
             String refinedName = '';
             name = name.toUpperCase();
             for(String s: prefix){
                if(name.endsWith(s)){
                    name = name.removeEnd(s);                   
                }
             }   
             //name = name.toLowerCase().Capitalize();
             List<String> nameLiterals = name.split(' ');
             for(String n: nameLiterals){
                 if(! prefix.contains(n.toUpperCase())){
                     refinedName = refinedName + n.toLowerCase().Capitalize()+ ' '; 
                 }
             }
             return refinedName.trim() ;
         }      
     }
     public static List<Itinerary> checkItinerary(List<Itinerary> itn){
            List<Itinerary> tempItinerary = new List<Itinerary>();
            Boolean flag = false;
            if(itn != null && itn.size() > 0){
                Long dt1Long = itn[itn.size() - 1].getTimeStamp().getTime();
                Long dt2Long = DateTime.now().getTime();
                Long milliseconds = dt2Long - dt1Long;
                Long seconds = milliseconds / 1000;
                Long minutes = seconds / 60;
                Long hours = minutes / 60;
                Long days = hours / 24;
                if(days <=35 ){
                    for(Itinerary i: itn){
                        i.uuid = LtcUtil.generateUUI();
                        String uniqueId = i.uuid;
                        tempItinerary.add(i);
                        //System.Debug('I am in uuid part'+uniqueId);
                        LtcRatings_Cache__c cache = new LtcRatings_Cache__c();
                        cache.UUID__c = uniqueId;
                        insert cache;
                    }
                    flag = true;
                }
            }
            if(flag)
                return tempItinerary;
            else
                return itn; 
         }
 }