public with sharing class LtcUnzipService {
    
    public LtcUnzipService() {
        //List<Attachment> atts = [select Id, Body from Attachment where name='EE10.PRD.PWC.FLTGUIDE.SCHED.ZIP' limit 1];
        List<Attachment> atts = [select Id, Body, ParentId from Attachment where ParentId in (select Id from Account where Name = 'pisabatch')];
        if (atts != null && atts.size() > 0) {
	        contentAsText = EncodingUtil.base64Encode(atts[0].Body);
        } else {
        	contentAsText = '';
        }
    }
    
    public String contentAsText { get; set; }
}