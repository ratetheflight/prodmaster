/**
 * This class contains unit tests for LtcUnzipService
 */
@isTest
private class LtcUnzipServiceTest {
	
	static testMethod void getContent() {
		LtcUnzipService uzs = new LtcUnzipService();
		System.assertEquals('',  uzs.contentAsText);
	}
}