/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Class to hold all static LTC general used Util functions
 * @log             : 13MAR2014: version 1.0
 * @log             :   FEB2016: version 2.0 added date utility methods
 */ 
public with sharing class LtcUtil {
    
    /**
     * Returns a leg with number legNo or null if not found in the legs__r collection
     */
    public static Leg__c findLeg(Flight__c flight, Integer legNo) {
        Leg__c result = null;
        Leg__c leg;
        System.debug('legNo=' + legNo);
        System.debug('flight.legs__r=' + flight.legs__r);
        if (flight != null && flight.legs__r != null) {
            for(Integer i = 0; i < flight.legs__r.size(); i++) {
                System.debug('flight.legs__r.size()=' + flight.legs__r.size());
                leg = flight.legs__r[i];
                if (leg.legNumber__c == legNo) {
                    result = leg;
                    break;
                }
            }
        }
        return result;
    }
    
    public static Leg__c currentLeg(Flight__c flight) {
        Integer currentLeg = flight.currentLeg__c == null ? 0 : (Integer) flight.currentLeg__c;
        return findLeg(flight, currentLeg);
    }
        
    /**
     * Returns a number indicating the day of the week 1= monday, 2 = tuesday etc
     * or -1 if the day can not be determined
     */
    public static String getDayOfWeek(Date d) {
        Datetime myDate = datetime.newInstance(d.year(), d.month(), d.day());  
        String day = myDate.format('EEEE');
        String result = '-1';       
        if (day == 'Monday') {
            result = '1';
        } else if (day == 'Tuesday') {
            result = '2';
        } else if (day == 'Wednesday') {
            result = '3';
        } else if (day == 'Thursday') {
            result = '4';
        } else if (day == 'Friday') {
            result = '5';
        } else if (day == 'Saturday') {
            result = '6';
        } else if (day == 'Sunday') {
            result = '7';
        }
        return result;
    }
    
    /**
     * Returns a date object created from a formatted string : ddMMMyy
     * example 03NOV15 => 03-11-2015
     */
    public static Date getDate(String ddMMMyy) {
        Integer day = Integer.valueOf(ddMMMyy.substring(0,2));
        Integer month = Integer.valueOf(getMonthNumber(ddMMMyy.substring(2,5)));
        Integer year = Integer.valueOf(ddMMMyy.substring(5)) + 2000;
        
        return Date.newInstance(year, month, day);
    }
    
    /**
     * Returns a date time object created from a formatted date string : ddMMMyy and a time HHmm
     * example 03NOV15 2130 -08:00 => 03-11-2015 13:30 (local time) 21:30 (UTC)
     */
    public static DateTime getDateTime(String ddMMMyy, String HHmm, String utcOffset) {
        Date day = getDate(ddMMMyy);
        return LtcUtil.getDateTime(day, HHmm, utcOffset);
    }
    
    /**
     * Returns a date time object created from a date and a formatted string time HHmm and utc offset
     * example 03NOV15 2130 -08:00 => 03-11-2015 13:30 (local time) 21:30 (UTC)
     */
    public static DateTime getDateTime(Date day, String HHmm, String utcOffset) {
        Integer hours = Integer.valueOf(HHmm.substring(0,2));
        Integer minutes = Integer.valueOf(HHmm.substring(2));
        Time newTime = Time.newInstance(hours, minutes, 0, 0);
        return DateTime.newInstanceGmt(day, newTime).addHours(getOffsetHours(utcOffset)).addMinutes(getOffsetMinutes(utcOffset));
    }
    
    public static Integer getOffsetHours(String sHHmm) {
        Integer offsetHours = 0;
        if (sHHmm != null) {
            offsetHours = Integer.valueOf(sHHmm.subString(0,3));
        }
        return offsetHours;
    }
    public static Integer getOffsetMinutes(String sHHmm) {
        Integer offsetMinutes = 0;
        if (sHHmm != null) {
            offsetMinutes = Integer.valueOf(sHHmm.subString(0,1) + sHHmm.subString(4));
        }
        return offsetMinutes;
    }
    
    public static Integer getOffsetInMinutes(String timeVariation) {
        Integer offSetInMinutes = 60 * LtcUtil.getOffsetHours(timeVariation) + LtcUtil.getOffsetMinutes(timeVariation);
        return offsetInMinutes;
    }

    /**
     * Returns a DateTime object created from a Date and a formatted string time HH:mm and a utc offset in minutes
     * example: 3 nov 2016 21:30 -120 => 03-11-2016 21:30 (local time) 19:30 (UTC)
     */
    public static DateTime getDateTimeGMT(Date day, String HHmm, Integer utcOffsetMinutes) {
        System.debug(LoggingLevel.DEBUG, 'getDateTimeGMT parms=' + day + ' ' + HHmm + ' ' + utcOffsetMinutes);
        System.debug(LoggingLevel.DEBUG, 'getDateTimeGMT DateTime.now()=' + DateTime.now());
        Integer hours = Integer.valueOf(HHmm.substring(0,2));
        Integer minutes = Integer.valueOf(HHmm.substring(3));
        Time newTime = Time.newInstance(hours, minutes, 0, 0);
        
        System.debug('DateTime.newInstanceGmt(day, newTime)=' + DateTime.newInstanceGmt(day, newTime)); 
        System.debug('DateTime.newInstanceGmt(day, newTime).addMinutes(utcOffsetMinutes)=' + DateTime.newInstanceGmt(day, newTime).addMinutes(utcOffsetMinutes)); 

        return DateTime.newInstanceGmt(day, newTime).addMinutes(utcOffsetMinutes);
    }
    
    /**
     * Returns the number of the month (1 - 12) or -1 if no match is found
     */
    public static Integer getMonthNumber(String monthName) {
        Integer result = -1;
        if (monthName == 'JAN') {
             result = 1;
        } else if (monthName == 'FEB') {
             result = 2;
        } else if (monthName == 'MAR') {
             result = 3;
        } else if (monthName == 'APR') {
             result = 4;
        } else if (monthName == 'MAY') {
             result = 5;
        } else if (monthName == 'JUN') {
             result = 6;
        } else if (monthName == 'JUL') {
             result = 7;
        } else if (monthName == 'AUG') {
             result = 8;
        } else if (monthName == 'SEP') {
             result = 9;
        } else if (monthName == 'OCT') {
             result = 10;
        } else if (monthName == 'NOV') {
             result = 11;
        } else if (monthName == 'DEC') {
             result = 12;
        }
        return result;
    }
    
    public static Integer determineCurrentLeg(Flight__c flight) {
        Integer legNo = -1;
        Integer result = 0;
        for (Leg__c leg : flight.legs__r) {
            System.debug(LoggingLevel.DEBUG, 'see if leg has actuals ' + leg + ' legNo=' + legNo);
            if (leg.actualArrivalDate__c != null && leg.legNumber__c > legNo) {
                result = (Integer) leg.legNumber__c;
                legNo  = (Integer) leg.legNumber__c;
            }
        }
        System.debug(LoggingLevel.DEBUG, 'determineCurrentLeg result=' + result);
        return result;
    }

    /**
     * Returns a list containg the elements from allRatings from index offset untill ofset + limitTo
     */
    public static List<Rating__c> getLimitedList(List<Rating__c> allRatings, Integer limitTo, Integer offset) {
        List<Rating__c> result = new List<Rating__c>();
        Integer fromRecord = offset;
        Integer untillRecord = offset + limitTo;
        
        System.debug('offset=' + offset);
        System.debug('limitTo=' + limitTo);
        System.debug('allRatings.size=' + allRatings.size());
        System.debug('untillRecord=' + untillRecord);
        
        for (integer i = fromRecord; i < untillRecord && i < allRatings.size(); i++) {
            result.add(allRatings.get(i));
        }
        
        System.debug('result.size=' +result.size()); 
        
        return result;
    }
    
    /**
     * Clean flightNumber to a format AA0000
     * Expected format AA?...? or aa?...? or Aa?...? or aA?...?
     */
    static public String cleanFlightNumber(String flightNumber) {
        //Clean flightNumber
        if (flightNumber != null) {         
            flightNumber = flightNumber.toUpperCase();
            String num = String.valueOf(Integer.valueOf(flightNumber.substring(2,flightNumber.length())));          
            flightNumber = flightNumber.substring(0, 2);
            if (!flightNumber.isAlpha() || !num.isNumeric() || num.length() > 4) {
                throw new LtcUtilException('Invalid flight number');
            }
            for (Integer x = num.length(); x < 4; x++) {
                flightNumber += '0';
            }
            flightNumber += num;
        }
        return flightNumber;
    }
    
    /**
     * returns the language part of the acceptLanguage header attribute, default en
     */
    public static String getLanguage(String acceptLanguage) {
        String language = 'en';
        if (acceptLanguage != null) {
            if (acceptLanguage.length() == 2) {
                language = acceptLanguage;
            } else if (acceptLanguage.length() > 2) {
                language = acceptLanguage.substring(0, 2);
            }
        }
        return language.toLowerCase();
    }

    /**
     * returns the country part of the acceptLanguage header attribute, default NL
     */
    public static String getCountry(String acceptLanguage) {
        String country = 'NL';
        if (acceptLanguage != null && acceptLanguage.length() >= 5) {
            country = acceptLanguage.substring(3,5);
        }
        return country.toUpperCase();
    }
    
    public static String translate(String translationKey, String acceptLanguage) {
        String language = getLanguage(acceptLanguage);
        Translation__c translation;
        String translatedText = '';
        List<Translation__c> translationList = [
            select t.Translated_Text__c 
            from Translation__c t 
            where t.Translation_Key__c =:translationKey 
            and t.Language__c =: language
        ];

        //Result should only be one or none!
        if (translationList != null && !translationList.isEmpty()) {
            translation = translationList[0];
            translatedText = translation.Translated_Text__c;
        } else if (!'en'.equalsIgnoreCase(language)) {
            // not found, now try english
            translationList = [
                select t.Translated_Text__c 
                from Translation__c t 
                where t.Translation_Key__c =:translationKey 
                and t.Language__c =: 'en'
            ];
            if (translationList != null && !translationList.isEmpty()) {
                translation = translationList[0];
                translatedText = translation.Translated_Text__c;
            } 
        }
        System.debug('trlk=' + translationKey + ' acclng=' + acceptLanguage + ' translation=' + translatedText);
        return translatedText;
    }
    
    public static String determineRatingStatus(Datetime arrivalDateTime, Integer timeToArrival, String uid) { 
        System.debug(LoggingLevel.DEBUG, 'determineRatingStatus arrivalDateTime=' + arrivalDateTime + ' timeToArrival=' + timeToArrival);
        String ratingStatus = 'Inactive';
        Boolean flag = false;
        // Active if now after timeToArrival <= 0
        if (timeToArrival < 0) {
            ratingStatus = 'Active';
        }
        if(uid != null){
            // Get a cached value
            List<LtcRatings_Cache__c> cache = [Select id,name,UUID__c from LtcRatings_Cache__c where UUID__c =: uid];
            if(cache.size() > 0){
                flag = (cache[0] != null) ? true : false;
                if(flag){
                    delete cache;
                }
             }
        }
        // Closed from 35 days after landing
        Date now = Date.today();
        Integer daysBetween = arrivalDateTime.date().daysBetween(now);
        if (daysBetween > 35 && !flag) {
            ratingStatus = 'Closed';
        } else if (daysBetween > 1) {
            // fallback for missing FOX info
            ratingStatus = 'Active';
        }
        return ratingStatus;
    }

    public static Boolean showBaggageInformation(Datetime arrivalDateTime, Integer utcOffSet) {
        // Default 
        Boolean ratingStatus = false;
        
        // Active if now after arrivalDateTime
        arrivalDateTime = arrivalDateTime.addMinutes(utcOffSet * -1);
        // Allowed to show 15 min before landing
        arrivalDateTime = arrivalDateTime.addMinutes(15);
        if (System.now().getTime() > arrivalDateTime.getTime()) {
            ratingStatus = true;
        }
        return ratingStatus;
    }

    public static void logMemoryUsage() {
        System.debug('current heap size=' + Limits.getHeapSize()+ ' max heap size=' + Limits.getLimitHeapSize()); 
    }

    public static void logMemoryUsage(String message) {
        System.debug('' + message + ' current heap size=' + Limits.getHeapSize()+ ' max heap size=' + Limits.getLimitHeapSize()); 
    }
    
    /**
     * Used for logging debuginfo to the client: 
     * http://baggdev-klm.cs18.force.com  ==>  true
     */
    public static Boolean isDevOrg () {
        return URL.getSalesforceBaseUrl().toExternalForm().contains('dev');
    }

    public class LtcUtilException extends Exception {

    }     
    
    public static string doAesEncryption(String thisIV, String thisKey, String plainText) {
        Blob providedIv = Blob.valueOf(thisIV); //16 bit/16 characters
        Blob key = Blob.valueof(thisKey); //16 bit/16 characters
        Blob data = Blob.valueOf(plainText);
        Blob encrypted = Crypto.encrypt('AES128', key, providedIv, data);
        String textEncodedData = EncodingUtil.base64Encode(encrypted);
        return textEncodedData;
    }   
    
    public static string doAesDecryption(String thisIV, String thisKey, String encodedText) {
        Blob providedIv = Blob.valueOf(thisIV);//16 bit/16 characters
        Blob key = Blob.valueof(thisKey); //16 bit/16 characters
        Blob decrypted = Crypto.decrypt('AES128', key, providedIv, EncodingUtil.base64Decode(encodedText));
        //Blob decrypted = Crypto.decrypt('AES128', key, providedIv, encodedText);
        String decryptedString = decrypted.toString();
        return decryptedString;
    }
    public static String generateUUI(){
        Blob aes = Crypto.generateAesKey(128);
        String hex = EncodingUtil.convertToHex(aes);
        return hex;
    } 
}