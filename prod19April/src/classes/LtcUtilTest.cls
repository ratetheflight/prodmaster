/**
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Test class to test the LtcUtil class
 * @log             : 14MAY2014: version 1.0 
 * @log             : 11FEB2014: version 2.0 added date utility methods
 * */
@isTest
private class LtcUtilTest {

	static testMethod void testDayOfWeek() {
		System.assertEquals('7', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2,  7)));
		System.assertEquals('1', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2,  8)));
		System.assertEquals('2', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2,  9)));
		System.assertEquals('3', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2, 10)));
		System.assertEquals('4', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2, 11)));
		System.assertEquals('5', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2, 12)));
		System.assertEquals('6', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2, 13)));
		System.assertEquals('7', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2, 14)));
		System.assertEquals('1', LtcUtil.getDayOfWeek(Date.newInstance(2016, 2, 15)));
	}
	
	static testMethod void testDateConversion() {
		System.assertEquals(Date.newInstance(2015,  1, 1), LtcUtil.getDate('01JAN15'));
		System.assertEquals(Date.newInstance(2016, 11, 1), LtcUtil.getDate('01NOV16'));
	}
	
	static testMethod void testDateTimeConversion() {
		System.assertEquals(DateTime.newInstanceGmt(2015, 1, 1, 18, 33, 0), LtcUtil.getDateTime('01JAN15', '1032', '+08:01'));
		System.assertEquals(DateTime.newInstanceGmt(2016, 11, 1, 2, 32, 0), LtcUtil.getDateTime('01NOV16', '1033', '-08:01'));
	}

	static testMethod void testLtcDateTime() {
		LtcDateTime ldt = new LtcDateTime('10JAN16', '1145', '-02:11');
		System.assertEquals(Date.newInstance(2016, 1, 10), ldt.getDate());
		System.assertEquals('09:34', ldt.getTime());
	}

    static testMethod void cleanFlightNumberTest() {
        System.assertEquals('KL0012', LtcUtil.cleanFlightNumber('KL012'));
        System.assertEquals('KL0012', LtcUtil.cleanFlightNumber('kl000012'));
        System.assertEquals('KL1234', LtcUtil.cleanFlightNumber('KL1234'));
    }
    
    static testMethod void testLimitedList() {
   		List<Rating__c> bigList = new List<Rating__c>();
   		for (integer i = 0; i < 35; i++) {
   			Rating__c rat = new Rating__c();
   			bigList.add(rat);
   		}
   		List<Rating__c> limitedList = LtcUtil.getLimitedList(bigList, 10, 0);
   		System.assertEquals(10, limitedList.size());
   		
   		limitedList = LtcUtil.getLimitedList(bigList, 10, 30);
   		System.assertEquals(5, limitedList.size());
    }
    
    static testMethod void testLimitedListSmall() {
   		List<Rating__c> bigList = new List<Rating__c>();
   		for (integer i = 0; i < 5; i++) {
   			Rating__c rat = new Rating__c();
   			bigList.add(rat);
   		}
   		List<Rating__c> limitedList = LtcUtil.getLimitedList(bigList, 10, 0);
   		System.assertEquals(5, limitedList.size());
   		
   		limitedList = LtcUtil.getLimitedList(bigList, 10, 1);
   		System.assertEquals(4, limitedList.size());
    }
   
    	
    	
}