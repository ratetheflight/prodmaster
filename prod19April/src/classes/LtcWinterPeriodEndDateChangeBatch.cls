/**
 * @author (s)    :	Satheeshkumar Subramani
 * @description   : Apex Batch class for changing end date for winter period for old service menu
 * @log           : 17 Dec 2015
 * 
 * Call anonymous: Id batjobId = Database.executeBatch(new LtcWinterPeriodEndDateChangeBatch(), 200);
 *
 * also the next script will do in a developers console:
 
for (LegClassPeriod__c[] legClassPeriods : [
    SELECT id, endDate__c
    FROM LegClassPeriod__c
    WHERE  EndDate__c =: Date.valueOf('2016-03-26')
]) {
    for (LegClassPeriod__c lcp : legClassPeriods) {				
        lcp.EndDate__c = Date.valueOf('2016-05-01');
    }
    update legClassPeriods;
}

 */
global class LtcWinterPeriodEndDateChangeBatch implements Database.Batchable<LegClassPeriod__c> {
	
	// the period to handle
	public ServicePeriod__c servicePeriod;
	
	public Iterable<LegClassPeriod__c> start(Database.BatchableContext BC) {
		initPeriodData();
				       	
		List<LegClassPeriod__c> legClassPeriods= new List<LegClassPeriod__c>();		
       	legClassPeriods = [
			SELECT id
			FROM LegClassPeriod__c
			WHERE ServicePeriod__c =: servicePeriod.id
			AND StartDate__c =: Date.valueOf('2015-10-26')
			AND EndDate__c =: Date.valueOf('2016-03-26')
		];
		return legClassPeriods;	
	}

	public void execute(Database.BatchableContext BC, LIST<LegClassPeriod__c> lcps){
		for (LegClassPeriod__c lcp: lcps) {
            lcp.EndDate__c = Date.valueOf('2015-12-09');
        }      
        update lcps;
	}
	
	public void finish(Database.BatchableContext BC){
	}
	
	private void initPeriodData() {
		List<ServicePeriod__c> sps = [
			SELECT Id, name__c
			FROM ServicePeriod__c
			WHERE name__c = 'Winter 15'
		];
		
		if (sps != null && !sps.isEmpty()) {
			servicePeriod = sps[0];
        }         
	}	
}