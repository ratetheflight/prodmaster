@isTest
private class LtcWinterPeriodEndDateChangeBatchTest {

    static testMethod void test_LtcWinterPeriodEndDateChangeBatch() {
    	ServicePeriod__c servicePeriod = new ServicePeriod__c();
		servicePeriod.name__c = 'Winter 15';
		insert servicePeriod;
		
		CabinClass__c cabinClass = new CabinClass__c();
		cabinClass.name = 'economy';
		cabinClass.label__C = 'cabinClass_economy_label';
		insert cabinClass;
					
        insertTestObject(cabinClass,'KL1221', 0, '2015-10-26', '2016-03-26', servicePeriod);
        insertTestObject(cabinClass,'KL1221', 1, '2015-10-26', '2016-03-26', servicePeriod);
        insertTestObject(cabinClass,'KL1001', 0, '2015-10-26', '2016-03-26', servicePeriod);
        insertTestObject(cabinClass,'KL1001', 1, '2015-10-26', '2016-03-26', servicePeriod);
        
        Test.startTest();       
        Database.executeBatch(new LtcWinterPeriodEndDateChangeBatch(), 3000);     
        Test.stopTest();
           
 		LegClassPeriod__c testObj1 = [select EndDate__c from LegClassPeriod__c WHERE FlightNumber__c = 'KL1221' AND LegNumber__c = 0];  
 		LegClassPeriod__c testObj2 = [select EndDate__c from LegClassPeriod__c WHERE FlightNumber__c = 'KL1001' AND LegNumber__c = 0];   
        System.assertEquals(Date.valueOf('2015-12-09'), testObj1.EndDate__c);
        System.assertEquals(Date.valueOf('2015-12-09'), testObj2.EndDate__c);
    }
    
    private static void insertTestObject(CabinClass__c cabinClass, String flightNumber, Integer legNumber, String startDate, String endDate, ServicePeriod__c servicePeriod){
        LegClassPeriod__c lcp = new LegClassPeriod__c(
        	CabinClass__c = cabinClass.id,
        	FlightNumber__c = flightNumber,
        	LegNumber__c =  legNumber,
            StartDate__c = Date.valueOf(startDate),
	        EndDate__c = Date.valueOf(endDate),
	        ServicePeriod__c = servicePeriod.id
	    );
        insert lcp;
    }
}