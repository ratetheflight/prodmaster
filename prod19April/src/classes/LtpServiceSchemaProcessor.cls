/**							
 * @author      : Mees Witteman
 * @description : Class to process the static resource LtcServiceSchema (service schema summer / winter)
 *				  and create a ServicePeriod with ServiceItems, LegClassPeriods and LegClassItems
 *
 *                LtpServiceSchemaProcessor processor = new LtpServiceSchemaProcessor();
 *                processor.processServiceSchema();
 */
public with sharing class LtpServiceSchemaProcessor {
	
	// the period to handle
	public ServicePeriod__c servicePeriod;
	
	// existing items
	public Map<String, ServiceItem__c> serviceItems = new Map<String, ServiceItem__c>();
	public CabinClass__c economyClass;
	public CabinClass__c businessClass;
	
	public List<LegClassPeriod__c> legClassPeriods = new List<LegClassPeriod__c>();
	public List<LegClassItem__c> legClassItems = new List<LegClassItem__c>();

	public void processServiceSchema() {
		initialize();
		
		loadCurrentRecords();

		String[] lines = getLinesFromResource();
		String[] record;
		String[] nextRecord;
		LegClassPeriod__c economyClassPeriod;
		LegClassPeriod__c businessClassPeriod;
		ServiceItem__c serviceItem;
		LegClassItem__c legClassItem;
		Integer sortOrder = 0;
		Boolean reachedServiceItems = false; // only after line starting with I the si's will follow
		// first iteration to insert all ServiceItems and LegClassPeriods
		for (Integer i = 3; i < lines.size(); i++) {
			record = lines[i].split(',');
			
			if (record[0].startswith('KL')) {
				System.debug(Logginglevel.INFO, 'KL line: new flight / leg' + lines[i]);
				// new leg, nextline contains the dates for the leg period
				i = i + 1;
				nextRecord = lines[i].split(',');
				businessClassPeriod = createLegClassPeriod(record, nextRecord, businessClass);
				legClassPeriods.add(businessClassPeriod);
				economyClassPeriod = createLegClassPeriod(record, nextRecord, economyClass);
				legClassPeriods.add(economyClassPeriod);
				reachedServiceItems = false;
			} 
			else if (record[0].startsWith('I')) {
				System.debug(Logginglevel.INFO, 'I record, start service items ' + lines[i]);
				// after this line si's will follow
				reachedServiceItems = true;
			}
			else if (reachedServiceItems && isServiceLine(record)) {
				System.debug(Logginglevel.INFO, 'process service record ' + lines[i]);
				String itemName = stripItemName(record[1]);
				if (isValidItem(itemName)) {
					serviceItem = createServiceItem(itemName);
					serviceItems.put(serviceItem.name__c, serviceItem);
				}
				itemName = stripItemName(record[3]);
				if (isValidItem(itemName)) {
					serviceItem = createServiceItem(itemName);
					serviceItems.put(serviceItem.name__c, serviceItem);
				}
			} else {
				System.debug(Logginglevel.INFO, 'skipped line ' + lines[i]);
			}
			if (legClassPeriods.size() > 40) {
				upsert legClassPeriods;
				legClassPeriods = new List<LegClassPeriod__c>();
			}
		}
		upsert serviceItems.values();
		upsert legClassPeriods;
			
		loadCurrentRecords();
		
		reachedServiceItems = false;
		
		// second iteration to insert all LegClassItem objects
		for (Integer i = 3; i < lines.size(); i++) {
			System.debug(Logginglevel.INFO, 'process line' + lines[i]);
			record = lines[i].split(',');
			if (record[0].startswith('KL')) {
				sortOrder = 0;
				// new leg, nextline contains the dates for the period
				i++;
				nextRecord = lines[i].split(',');
				economyClassPeriod = findLegClassPeriod(record, nextRecord, economyClass);
				businessClassPeriod = findLegClassPeriod(record, nextRecord, businessClass);
				reachedServiceItems = false;
	
			} 
			else if (record[0].startsWith('I')) {
				reachedServiceItems = true;
			}
			else if (reachedServiceItems && isServiceLine(record)) {
				String itemName = stripItemName(record[1]);
				if (isValidItem(itemName)) {
					legClassItem = createLegClassItem(itemName, businessClassPeriod, sortOrder);
					legClassItems.add(legClassItem);
				}
				itemName = stripItemName(record[3]);
				if (isValidItem(itemName)) {
					legClassItem = createLegClassItem(itemName, economyClassPeriod, sortOrder);
					legClassItems.add(legClassItem);
				}
				sortOrder = sortOrder + 1;
			} 
			if (legClassItems.size() > 40) {
				upsert legClassItems;
				legClassItems = new List<LegClassItem__c>();
			}
		}
		upsert legClassItems;
	}
	
	private Boolean isServiceLine(String[] record) {
		Boolean result = false;
		if (record[0].startsWith('E') 
			|| record[0].trim().startsWith('-')
			|| record[2].startsWith('E')
			|| record[2].trim().startsWith('-'))	{
			result = true;
		}
		return result;
			
	}
	private Boolean isValidItem(String itemName) {
		Boolean result = true;
		if (itemName == null) {
			result = false;
		} 
		else if (''.equals(itemName)) {
			result = false;
		}
		else if (itemName.toUppercase().contains('GARNITURE')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('WINESERVICE')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('BONBONS')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('COFFEE')) {
			result = false;
		}
		else if (itemName.toUppercase().startsWith('DESSERT')) {
			result = false;
		}
		return result;
	}
	
	private LegClassPeriod__c createLegClassPeriod(String[] record, String[] nextRecord, CabinClass__c cabinClass) {
		LegClassPeriod__c lcp = findLegClassPeriod(record, nextRecord, cabinClass);
		if (lcp == null) {
			lcp = new LegClassPeriod__c();
			lcp.CabinClass__c = cabinClass.id;
			lcp.FlightNumber__c = record[0].replaceAll(' ', '');
			lcp.legNumber__c = Integer.valueOf(record[1].replaceAll('leg:', '').trim()) - 1;
			lcp.StartDate__c = parseCsvDate(nextRecord[1]);
			lcp.EndDate__c = parseCsvDate(nextRecord[2]);
			lcp.ServicePeriod__c = servicePeriod.id;
		}
		System.debug(Logginglevel.INFO, 'leg/class/period ' + lcp.flightnumber__c + ' ' + lcp.legnumber__c + ' ' + cabinClass.name);
		return lcp;
	}
	
	private LegClassPeriod__c findLegClassPeriod(String[] record, String[] nextRecord, CabinClass__c cabinClass) {
		for (LegClassPeriod__c lcp : legClassPeriods) {
			if (lcp.cabinClass__c.equals(cabinClass.id)
				&& lcp.flightNumber__c.equals(record[0].replaceAll(' ', ''))
				&& lcp.legNumber__c == Integer.valueOf(record[1].replaceAll('leg:', '').trim()) - 1
				&& lcp.StartDate__c.isSameDay(parseCsvDate(nextRecord[1]))
				&& lcp.EndDate__c.isSameDay(parseCsvDate(nextRecord[2]))) {
				return lcp;
			}
		}
		return null;
	}		
	
	private ServiceItem__c createServiceItem(String serviceItemName) {
		ServiceItem__c si = findServiceItem(serviceItemName);
		if (si == null) {
			si = new ServiceItem__c();
			si.name__c = 'ServiceItem_' + serviceItemName + '_name';
			si.description__c = 'ServiceItem_' + serviceItemName + '_description';
			// log the label translation sheet entries for new created service items
//			System.debug(Logginglevel.INFO, 'si:\t\t{');
//			System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.name__c + '",');
//			System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
//			System.debug(Logginglevel.INFO, 'si:\t\t},');
//			System.debug(Logginglevel.INFO, 'si:\t\t{');
//			System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.description__c + '",');
//			System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
//			System.debug(Logginglevel.INFO, 'si:\t\t},');
			
		} 
		si.type__c = getTypeFromName(si.name__c);
		return si;
	}
	
	private ServiceItem__c findServiceItem(String serviceItemName) {
		String siName = 'ServiceItem_' + serviceItemName + '_name';
		ServiceItem__c si = serviceItems.get(siName);
		return si;
	}
		
	private LegClassItem__c createLegClassItem(String serviceItemName, LegClassPeriod__c legClassPeriod, Integer order) {
		ServiceItem__c si = findServiceItem(serviceItemName);
		LegClassItem__c lci = new LegClassItem__c();
		lci.LegClassPeriod__c = legClassPeriod.id;
		lci.serviceItem__c = si.id;
		lci.order__c = order;
		return lci;
	}

 	private String stripItemName(String input) {
 		if (input == null) return '';
 		String output = input.trim();
 		if (output.contains('-')) {
 			output = output.substring(output.indexOf('-') + 1).trim();
 		}
 		output = output.replaceAll(' ', '');
 		output = output.replaceAll('/', '-');
 		output = output.replaceAll('%', '');
 		output = output.toLowerCase();
 		return output;
 	}
 	
 	private String getTypeFromName(String serviceItemName) {
 		String result = 'other';
 		if (serviceItemName.contains('drink')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('tidbi')) {
 			result = 'tidbits';
 		} else if (serviceItemName.contains('water')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('bonbon')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('snack')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('mnml')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('wine')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('beverage')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('juice')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('coffee')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('towel')) {
 			result = 'towel';
 		} else if (serviceItemName.contains('breakfast')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('dess')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('noodle')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('friandise')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('garniture')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('meal')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('combi')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('yours')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('icecream')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('sand')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('sale')) {
 			result = 'sales';
 		} else {
 			result = 'other';
 		}
 		return result;
 
 	}
 	
	// convert dd-MMM-yyyy to yyyy-MM-dd HH:mm:ss format
	private Date parseCsvDate(String inputDate) {
		inputDate = inputDate.trim();
		String outputDate = inputDate.substring(7,11);
		outputDate = outputDate + '-';
		outputDate = outputDate + getMonthNumber(inputDate.substring(3,6));
		outputDate = outputDate + '-';
		outputDate = outputDate + inputDate.substring(0,2);
		outputDate = outputDate + ' 12:00:00';
		return Date.valueOf(outputDate);
	}
	
	private String getMonthNumber(String monthName) {
		String result = '01';
		if ('Feb'.equals(monthName)) {
			result = '02';
		} else if ('Mar'.equals(monthName)) {
			result = '03';
		} else if ('Apr'.equals(monthName)) {
			result = '04';
		} else if ('May'.equals(monthName)) {
			result = '05';
		} else if ('Jun'.equals(monthName)) {
			result = '06';
		} else if ('Jul'.equals(monthName)) {
			result = '07';
		} else if ('Aug'.equals(monthName)) {
			result = '08';
		} else if ('Sep'.equals(monthName)) {
			result = '09';
		} else if ('Oct'.equals(monthName)) {
			result = '10';
		} else if ('Nov'.equals(monthName)) {
			result = '11';
		} else if ('Dec'.equals(monthName)) {
			result = '12';
		}
		return result;
	}

  	private String[] getLinesFromResource() {
  		StaticResource sr = [
			Select  s.Name, s.Id, s.Body 
			From StaticResource s  
			where name =: 'LTCServiceSchedule'
		];
		blob tempBlob = sr.Body;
		String tempString = tempBlob.toString();
		tempString = tempSTring.replace('"', '');
		String[] lines = tempString.split('\n');
		return lines;
  	}
  	
	private void loadCurrentRecords() {
		economyClass = [
			Select Name, Id, label__c
			From CabinClass__c	
			where name =: 'economy'
		];
		businessClass= [
			Select Name, Id, label__c
			From CabinClass__c 
			where name =: 'business'
		];
		
		List<ServiceItem__c> sis = [select id, name__c, description__c from ServiceItem__c];
		for (ServiceItem__c si : sis) {
			serviceItems.put(si.name__c, si);
		}
		
		legClassPeriods = [
			select id, CabinClass__c, EndDate__c, FlightNumber__c, LegNumber__c, ServicePeriod__c, StartDate__c
			from LegClassPeriod__c
			where ServicePeriod__c =: servicePeriod.id
		];

	}
	
	public void initialize() {
		StaticResource sr = [
			Select Name, Id, Description 
			From StaticResource 
			where name =: 'LTCServiceSchedule'
		];
		
		List<ServicePeriod__c> sps = [
			Select Id, name__c
			From ServicePeriod__c
			where name__c =: sr.description
		];
		if (sps != null && !sps.isEmpty()) {
			ServicePeriod__c sp = sps[0];
			System.debug(Logginglevel.INFO, 'delete service period ' + sp.name__c);
			delete sp;
        }
		
		servicePeriod = new ServicePeriod__c();
		servicePeriod.name__c = sr.description;
		insert servicePeriod;
		
		ServicePeriod__c sp = [
			Select Id, name__c
			From ServicePeriod__c
			where name__c =: sr.description
		];
		
	}
}