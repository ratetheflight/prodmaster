/**********************************************************************
 Name:  MyFlight.cls
======================================================
Purpose: Class to hold detail flight data to transfer to the page:
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van 't Hooft  27/09/2014      Initial development
***********************************************************************/
global class MyFlight {	
	//public Integer nrOfInfants;			// Nr of infant results
	//public Integer nrOfInfantsOnSeat;	// Nr of infants on seat results	
	//public Integer nrOfChildsOnSeat;	// Nr of childs on seat results	
	public String airportOri;
	public String airportDes;
	public List<AFKLM_WS_Manager.paxEntry> paxEntry;
	
	/**
	public Map<String, AFKLM_WS_Manager.paxEntry> mapOfKeyToPax {
		get{
			if (mapOfKeyToPax == null){
				mapOfKeyToPax = new Map<String, AFKLM_WS_Manager.paxEntry>();
			} 
			return mapOfKeyToPax;
		}
		set;
	}
	**/
	
}