@RestResource(urlMapping='/feedback/*')
global class RESTFeedbackController {

@HttpGet
  global static List<RESTFeedbackController.FeedbackStat> getFeedbackStats() {
    List<Feedback__c> feedbacks = [SELECT airportCode__c, area__c, feedbackDate__c, subArea__c, keyword__c, thumbsUp__c FROM Feedback__c WHERE feedbackDate__c > YESTERDAY order by feedbackDate__c desc];
    List<RESTFeedbackController.FeedbackStat> stats = new List<RESTFeedbackController.FeedbackStat>();
    Map<String, FeedbackStat > feedbackMap = new Map<String,FeedbackStat >();
    Map<String, Keyword> keywordsMap = new Map<String, Keyword>();

    for (Feedback__c fb: feedbacks) {
        FeedbackStat stat = new FeedbackStat();
        stat.airportCode = fb.airportCode__c;
        stat.feedbackDate = fb.feedbackDate__c;
        stat.area = fb.area__c;
        stat.subArea = fb.subArea__c;
        Keyword kw = new Keyword();
        kw.keyword = fb.keyword__c;
        kw.thumbsUp = fb.thumbsUp__c;
        kw.numberOfVotes = 1;
        if (fb.thumbsUp__c) {
            stat.positiveKeywords.add(kw);
        } else {
            stat.negativeKeywords.add(kw);        
        }

        if (fb.thumbsUp__c) {
            stat.thumbsUp = 1;
            stat.thumbsDown = 0;
        } else {
            stat.thumbsUp = 0;
            stat.thumbsDown = 1;
        }

        String keyword = fb.airportCode__c + fb.area__c + fb.subArea__c;
        FeedbackStat exists = feedbackMap.get(keyword);
        String kwKeyword = keyword + fb.keyword__c + fb.thumbsUp__c;
            
        if (exists == null) {
            feedbackMap.put(keyword, stat);
            stats.add(stat);
            keywordsMap.put(kwKeyword, kw);
        } else {
            if (fb.thumbsUp__c) {
                exists.thumbsUp = exists.thumbsUp + 1;
            } else {
                exists.thumbsDown = exists.thumbsDown + 1;            
            }

            Keyword existingKeyword = keywordsMap.get(kwKeyword );

            if (existingKeyword == null) {
                keywordsMap.put(kwKeyword, kw);
                if (kw.thumbsUp) {
                    exists.positiveKeywords.add(kw);
                } else {
                    exists.negativeKeywords.add(kw);
                }
            } else {
                existingKeyword.numberOfVotes = existingKeyword.numberOfVotes+1 ;
            }
        }
    }

    return stats;
  }
    @HttpPost
    global static String persistFeedback(String airportCode, String area, String subArea, Boolean thumbsUp, String deviceDetails, String keyword, String comment, String fbNumber, String fbTierLevel, DateTime feedbackDate){
        DateTime now = DateTime.now();
        if (now.getTime() < feedbackDate.getTime()) {
            feedBackDate = now;
        }

        Feedback__c fb = new Feedback__c(airportCode__c = airportCode,
                                       area__c = area,
                                       subArea__c = subArea,
                                       thumbsUp__c = thumbsUp,
                                       deviceDetails__c = deviceDetails,
                                       keyword__c = keyword,
                                       comment__c = comment,
                                       fbNumber__c = fbNumber,
                                       fbTierLevel__c = fbTierLevel,
                                       feedbackDate__c = feedbackDate);
        insert fb;
        return fb.Id;
    }
    
    global class FeedbackStat {
        public String airportCode;
        public String area;
        public String subArea;
        public List<Keyword> positiveKeywords = new List<Keyword>();
        public List<Keyword> negativeKeywords = new List<Keyword>();
        public Integer thumbsUp;
        public Integer thumbsDown;
        public DateTime feedbackDate;
    }
    global class Keyword {
        public String keyword;
        public Integer numberOfVotes;
        public boolean thumbsUp;
    }
}