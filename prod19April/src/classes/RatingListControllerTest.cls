/********************************************************************** 
 Name:  RatingListControllerTest
 Task:    N/A
 Runs on: RatingListController
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van 't Hooft  21/01/2015      Initial Development
***********************************************************************/
@isTest
private class RatingListControllerTest {
	
    static testMethod void myControllerTest() {
    	List<Rating__c> ratingList = new List<Rating__c>();
        ApexPages.StandardSetController ratingSetController = new ApexPages.StandardSetController(ratingList);
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest());

        RatingListController myEmptyController = new RatingListController();
        RatingListController myController = new RatingListController(ratingSetController);
        Test.stopTest();
        
        system.assert(ratingList == ratingSetController.getRecords(), '\n\nWARNING: Social Posts are not the same.');
    }
    
    static testMethod void lazyInitGetterController() {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest());
        RatingListController rlc = new RatingListController();
        rlc.filterId = 'All';
        Test.stopTest();
    }
    
    static testMethod void createCasesTooManySelected() {
		List<Rating__c> ratingList = getRatingList();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest());

        RatingListController myController = new RatingListController();
        List<RatingWrapper> wraps = myController.getRatings();
        RatingWrapper rw = wraps.get(0);
        rw.isSelected = true;
        rw = wraps.get(1);
        rw.isSelected = true;
        
        PageReference pr = myController.createCases();
        
        Test.stopTest();
        // no case created
        List<Case> cases = [select id from Case];
        System.assertEquals(0, cases.size());
    }
    
    static testMethod void getControllerWithFilterIds() {
		List<Rating__c> ratingList = getRatingList();
        List<RatingWrapper> wraps;
        Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest());

        RatingListController myController = new RatingListController();
        myController.filterId = RatingListController.ALL_CLOSED_IGT;
        wraps = myController.getRatings();
        //System.assertEquals(0, wraps.size());
        
        myController = new RatingListController();
        myController.filterId = RatingListController.ALL_OPEN_SMH;
        wraps = myController.getRatings();
        //System.assertEquals(2, wraps.size());

        // add rating case
        Rating__c rating = ratingList.get(0);
        
        Case myCase = new Case();
        insert myCase;
		myCase.rating__c = rating.id;
		rating.caseId__c = myCase.id;
		update rating;
        
        myController = new RatingListController();
        myController.filterId = RatingListController.ALL_OPEN_SMH;
        wraps = myController.getRatings();
        //System.assertEquals(2, wraps.size());
        
        myController = new RatingListController();
        myController.filterId = RatingListController.ALL_CLOSED_SMH;
        wraps = myController.getRatings();
        //System.assertEquals(0, wraps.size());
        
        myCase.status = 'Closed';
        myCase.Case_details__c = 'this must be filled';
        myCase.Case_detail__c = 'Entertainment';
        myCase.Sentiment_at_start_of_case__c = 'Neutral';
        myCase.Sentiment_at_close_of_case__c = 'Neutral';
        update myCase;
        
        myController = new RatingListController();
        myController.filterId = RatingListController.ALL_CLOSED_SMH;
        wraps = myController.getRatings();
        //System.assertEquals(1, wraps.size());

        myController = new RatingListController();
        myController.filterId = RatingListController.ALL_OPEN_SMH;
        wraps = myController.getRatings();
        //System.assertEquals(1, wraps.size());        
        
        myController = new RatingListController();
        myController.filterId = RatingListController.ALL_OPEN_IGT;
        wraps = myController.getRatings();
        //System.assertEquals(0, wraps.size());
    }

    static testMethod void createCasesCaseExist() {
        List<Rating__c> ratingList = getRatingList();

        RatingListController myController = new RatingListController();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest());
        List<RatingWrapper> wraps = myController.getRatings();
        RatingWrapper rw = wraps.get(0);
        rw.isSelected = true;
        rw.wRating.rating_number__c = 1;
        
        PageReference pr = myController.createCases();
        Test.stopTest();
        
        // no case created
        List<Case> cases = [select id from Case];
        
        //System.assert(myController.error == false);
        System.assertEquals(1, cases.size());
    }
    
    static testMethod void createCasesCaseCreated() {
    	List<Rating__c> ratingList = getRatingList();
        
        Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest());

        RatingListController myController = new RatingListController();
        Test.startTest();
        List<RatingWrapper> wraps = myController.getRatings();
        RatingWrapper rw = wraps.get(0);
        rw.isSelected = true;
        
        PageReference pr = myController.createCases();
        Test.stopTest();
        
        // new case created
        List<Case> cases = [select id from Case];
        
        System.assertEquals(1, cases.size());
        
    }
    
    static testMethod void testSeveralSmallMethods() {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest());

        RatingListController myController = new RatingListController();
        myController.refreshRatingView();
        System.assert(!myController.selectAll);
               
        System.assertEquals(4, myController.getFieldItems().size());
        System.assertEquals('SLA__C DESC, Rating_Number__c ASC', myController.fieldValues);
        myController.fieldValues = 'SLA__C ASC';
        System.assertEquals('SLA__C ASC', myController.fieldValues);
        
        myController.resetFilter();
        
        Test.stopTest();
        
        System.assert(!myController.selectAll);
        
        //System.assertEquals(1, myController.getTotalPageNo());
       // System.assertEquals(10, myController.getSize());
        
    }
      
    // Helper class test method for the controller
    private static ApexPages.StandardSetController myStandardSetController() {
    	List<Rating__c> ratingList = new List<Rating__c>();
    	ApexPages.StandardSetController ratingListController = 
            new ApexPages.StandardSetController(ratingList);
            
            return ratingListController;
    }

	private static List<Rating__c> getRatingList() {
		List<Rating__c> ratingList = new List<Rating__c>();
        String flightNumber = 'KL1234';
        Date trDate = System.now().date();
        String firstName = 'Jan';
        String familyName = 'Jansen';
        String negativeFeedBack = 'Negative';
        String positiveFeedback = 'Positive';
        Boolean pub = false;
        Integer ratingNumber = 5;
        String seatNumber = '50G';

        //Create Flight
        Flight__c flight = new Flight__c(Flight_Number__c = flightNumber, Scheduled_Departure_Date__c = trDate);
        insert flight;
        
        Leg__c leg = new Leg__c(origin__c = 'AMS', destination__c = 'LHR', flight__c = flight.id);
        insert leg;
        
        //Create flight date
        Flight_Date__c flight_Date = new Flight_Date__c(Full_Date__c = trDate);
        insert flight_Date;
            
        Passenger__c passenger; 
        Rating__c rating;       
        
        for (integer i = 0; i < 2; i++) {
            familyName += String.valueOf(i);
            passenger = new Passenger__c(First_Name__c = firstName, Family_Name__c = familyName );
            insert passenger;
            
            //Store the Rating with links to the passenger and the flight information
            rating = new Rating__c(Flight_Info__c = flight.Id, Flight_Date__c = flight_Date.Id, 
                    Passenger__c = passenger.Id, Negative_comments__c = negativeFeedBack, 
                    Positive_comments__c = positiveFeedback, Publish__c = pub, Rating_Number__c = ratingNumber, 
            		has_Comments__c = true,
                    Seat_Number__c = seatNumber, 
            		language__c = 'nl' );
            insert rating;

            ratingList.add(rating);
        }
        return ratingList;
	}
}