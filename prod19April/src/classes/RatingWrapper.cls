/********************************************************************** 
 Name:  RatingWrapper
 Task:    N/A
 Runs on: RatingController
====================================================== 
Purpose: 
    Rating Wrapper to order the Rating Post by date and verify if the Rating has been selected.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van 't Hooft  19/01/2014      Initial Development
***********************************************************************/
global class RatingWrapper implements Comparable {
    public transient Boolean isSelected {get;set;}
    public transient Rating__c wRating {get;set;}
    public ApexPages.StandardSetController sRatingController {get;set;}
    public String shortenPos {get;set;}
    public String shortenNeg {get;set;}
    public String shortenKlmReply {get;set;}

    /**
    * Standard Constructor. 
    */
    public RatingWrapper(RatingListController ratingController) {}
    
    /**
    * Overloading Constructor. 
    * To wrap the SocialPost with selected checkbox value.
    */
    public RatingWrapper(Rating__c wRating, ApexPages.StandardSetController sRatingController){
        this.wRating = wRating;
        isSelected = false;
        shortenPos = '';
        shortenNeg = '';
          
        // Shorten the Positive / Negative content for display
        if(wRating.Positive_comments__c != null){
            if(wRating.Positive_comments__c.length() > 100) {
                shortenPos = wRating.Positive_comments__c.substring(0,100) + '...';
            } else {
                shortenPos = wRating.Positive_comments__c;
            }
        }
        if(wRating.Negative_comments__c != null) {
            if(wRating.Negative_comments__c.length() > 100) {
                shortenNeg = wRating.Negative_comments__c.substring(0,100) + '...';
            } else {
                shortenNeg = wRating.Negative_comments__c;
            }
        }          
        if(wRating.Klm_Reply__c != null) {
            if(wRating.Klm_Reply__c.length() > 100) {
                shortenKlmReply = wRating.Klm_Reply__c.substring(0, 100) + '...';
            } else {
                shortenKlmReply = wRating.Klm_Reply__c;
            }
        }          
        this.sRatingController = sRatingController;
    }

    /**
    * Order the Rating using Comparable RatingWrapper.sort() this method will be used 
    *
    * @param    compareTo Object and field which needed to be sorted
    * @return   Integer return positive or negative value to verify if the posted date is higher than the previous record
    */
    global Integer compareTo(Object compareTo) {
        // Cast argument to Rating Wrapper
        RatingWrapper compareToRating = (RatingWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (wRating.CreatedDate > compareToRating.wRating.CreatedDate) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (wRating.CreatedDate < compareToRating.wRating.CreatedDate) {
            // Set return value to a negative value.
            returnValue = -1;
        }        
        return returnValue;       
    }          
}