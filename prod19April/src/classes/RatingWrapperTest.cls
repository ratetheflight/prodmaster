/********************************************************************** 
 Name:  RatingWrapperTest
 History                                                            
 -------                                                            
 VERSION     AUTHOR              DATE            DETAIL                                 
     1.0     David van 't Hooft  03/03/2015      Initial Development
***********************************************************************/
@isTest
private class RatingWrapperTest {
	
	static testMethod void myControllerTest() {
		Test.startTest();
		RatingWrapper tstRatingWrapper = new RatingWrapper(null);
		Test.stopTest();
		
		System.assertEquals(tstRatingWrapper.shortenPos, null);
		System.assertEquals(tstRatingWrapper.shortenNeg, null);
		System.assertEquals(tstRatingWrapper.shortenKlmReply, null);
		System.assertEquals(tstRatingWrapper.isSelected, null);
		System.assertEquals(tstRatingWrapper.wRating, null);
	}
	
	static testMethod void testTheLongOne() {
		String longText = 'abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890';
		String shortText = 'abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890abcdefghij1234567890...';
		Rating__c rating = new Rating__c();
		rating.positive_comments__c = longText;
		rating.negative_comments__c = longText;
		rating.Klm_Reply__c = longText;
		RatingWrapper tstRatingWrapper = new RatingWrapper(rating, null);
		
		System.assertEquals(shortText, tstRatingWrapper.shortenPos);
		System.assertEquals(shortText, tstRatingWrapper.shortenNeg);
		System.assertEquals(shortText, tstRatingWrapper.shortenKlmReply);
	}
	
	static testMethod void testShorty() {
		String longText = 'abcdefghij1234567890';
		String shortText = 'abcdefghij1234567890';
		Rating__c rating = new Rating__c();
		rating.positive_comments__c = longText;
		rating.negative_comments__c = longText;
		rating.Klm_Reply__c = longText;
		RatingWrapper tstRatingWrapper = new RatingWrapper(rating, null);
		
		System.assertEquals(shortText, tstRatingWrapper.shortenPos);
		System.assertEquals(shortText, tstRatingWrapper.shortenNeg);
		System.assertEquals(shortText, tstRatingWrapper.shortenKlmReply);
	}
	

	
}