/**
 * @author (s)      : David van 't Hooft
 * @name			: RestApiService
 * @description     : Rest call handling for GET, POST methods, retries on connection based on Twilio.
 * @log:   16MARCH2015: version 1.0
 */
/*
Copyright (c) 2012 Twilio, Inc.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
public with sharing class RestApiService {
    private Integer numRetries = 3; // Number of retries to connect to endpoint
    private string endpointUrl = 'https://api.klm.com';
	private String accessToken;
     
    /**
     * Gets the num retries.
     * 
     * @return the num retries
     */
    public Integer getNumRetries() {
        return numRetries;
    }

    /**
     * Sets the num retries.
     * 
     * @param numRetries
     *            the new num retries
     */
    public void setNumRetries(Integer numRetries) {
        this.numRetries = numRetries;
    }
    
    /**
     * Make a request, handles retries + back-off for server/network errors
     * 
     * @param path
     *            the URL (absolute w.r.t. the endpoint URL - i.e.
     *            https://api.ute1.klm.com/testcustomerid/?api_key=<API KEY>&flying-blue-number=<FB number>
     *            or https://api.ute1.klm.com/testcustomers/<Customer ID>?api_key=<API Key>
     * @param method
     *            the HTTP method to use, defaults to GET
     * @param vars
     *            for POST or PUT, a map of data to send, for GET will be
     *            appended to the URL as querystring params
     * @param jsonBody is used if not NULL as body instead of the params map 
     * @return The response
     */
    public RestApiResponse safeRequest(String path, String method,
            Map<String, String> vars, String jsonBody) {
                
        RestApiResponse response = null;
        for (Integer retry = 0; retry < this.numRetries; retry++) {
            response = request(path, method, vars, jsonBody);
            if (response.isClientError()) {
            	// Handle the 401 error [Status=Unauthorized, StatusCode=401] when AccessToken is expired 
                if(response.getHttpStatus() == 401) {
                	//@TODO ApexPages does not work here!
            		// Get a new AccesToken using Client ID and Secret from Custom Settings.
            		System.debug('Rest API Token has probably expired and gives HTTP 401 status: '+response.getResponseText());
		    		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Rest API Access token has expired please reload the page');
            		ApexPages.addMessage(myMsg);
                } else if (response.getHttpStatus() == 404) {

                    System.debug('Error: 404 Not Found');
                } else {
                	throw new RestApiException(response);
                }
            } else if (response.isServerError()) {
                // TODO handle backoff scenario
                //try {
                //  Thread.sleep(100 * retry); // Backoff on our sleep
                //} catch (Exception e) {
                //}
                continue;
            }

            return response;
        }

        Integer errorCode = response == null ? -1 : response.getHttpStatus();
        throw new RestApiException('Cannot fetch: ' + method + ' ' + path,
                errorCode);
    }
    
    /**
     * sendRequst Sends a REST Request to the REST API.
     * 
     * @param path
     *            the URL (absolute w.r.t. the endpoint URL - i.e.
     *            https://api.ute1.klm.com/testcustomerid/?api_key=<API KEY>&flying-blue-number=<FB number>
     *            or https://api.ute1.klm.com/testcustomers/<Customer ID>?api_key=<API Key>
     * @param method
     *            the HTTP method to use, defaults to GET
     * @param vars
     *            for POST or PUT, a map of data to send, for GET will be
     *            appended to the URL as querystring params
     * 
     * @param jsonBody is used if not NULL as body instead of the params map 
     * @return the REST API response
     */
    public RestApiResponse request(String path, String method,
            Map<String, String> vars, String jsonBody) {
		
        HttpRequest request = setupRequest(path, method, vars, jsonBody);
        String responseBody;
        Integer statusCode;
        String contentType;

        if (Test.isRunningTest()) {
	        // TODO Can't execute HTTP requests during test execution and use a Mock class
            AFKLM_SCS_HttpCalloutMock hcm = new AFKLM_SCS_HttpCalloutMock();
            // HttpRequest req = new HttpRequest();
	        request.setEndpoint(path);
	        request.setMethod(method);
            HttpResponse res = hcm.respond(request);
            responseBody = res.getBody();
            statusCode = res.getStatusCode();
            contentType = res.getHeader('Content-Type');
        } else {
            // execute the real API HTTP request
            HttpResponse response = (new Http()).send(request);
            responseBody = (response.getBody()==null)?'':response.getBody();
            statusCode = response.getStatusCode();
            contentType = response.getHeader('Content-Type');
        } 

        RestApiResponse restResponse = new RestApiResponse(request.getEndpoint(),
                     responseBody, statusCode);
        
        restResponse.setContentType(contentType);

        return restResponse;
    }
    
    /**
     * Setup request.
     * 
     * @param path
     *            the path
     * @param method
     *            the method
     * @param vars
     *            the vars
     * @param jsonBody is used if not NULL as body instead of the params map 
     * @return the http uri request
     */
    private HttpRequest setupRequest(String path, String method,
            Map<String, String> params, String jsonBody) {

        String normalizedPath = path.toLowerCase();
        String sb = '';

        // If we've given a fully qualified uri then skip building the endpoint
        if (normalizedPath.startsWith('http://') || normalizedPath.startsWith('https://')) {
            sb += path;
        } else {
			//Scheung do not do anything if Salesforce 'Named Credentials' is used
			if(!isNamedCredentials(path)) {
	            sb += this.getEndpoint();
	
	            if (!normalizedPath.startsWith('/')) {
	                sb += '/';
	            }
			}
            sb += path;
        }

        path = sb;
        
        HttpRequest request = buildMethod(method, path, params, jsonBody);
		//Scheung is not stored Salesforce 'Named Credentials' check the accessToken
		if(!isNamedCredentials(path)) {
			if(accessToken == null || ''.equals(accessToken) ) {
				request.setHeader('Authorization', 'Bearer ' + getAccessToken());
			} else {
				request.setHeader('Authorization', 'Bearer ' + accessToken);
			}
		}
		
        request.setHeader('Content-Type','application/x-www-form-urlencoded');
	    request.setHeader('Content-Type', 'application/json');
        
        // TODO: Add generated certificate information. 
        // For production use the signed by Verisign or other approved authorities.
        // request.setHeader('Authorization',
        //             'Basic '+EncodingUtil.base64Encode(Blob.valueOf(this.accountSid + ':' + this.authToken)));
        
        return request;
    }
    
    /**
     * Get the current endpoint this client is pointed at.
     * 
     * @return String the api endpoint
     */
    public void setEndpoint(String endpointUrl) {
    	this.endpointUrl = endpointUrl;
    }
    
    /**
     * Get the current endpoint this client is pointed at.
     * 
     * @return String the api endpoint
     */
    public String getEndpoint() {
        return this.endpointUrl;
    }
    
    // TODO: In the future we need to update API data. The below method should be added.
    /**
     * Builds the method.
     * 
     * @param method
     *            the method
     * @param path
     *            the path
     * @param params
     *            the params
     * @param jsonBody is used if not NULL as body instead of the params map 
     * @return the http uri request
     */
    private HttpRequest buildMethod(String method, String path,
            Map<String,String> params, String jsonBody) {
        
        if (method.equalsIgnoreCase('GET')) {
            return generateGetRequest(path, params);
        } else if (method.equalsIgnoreCase('POST')) {
            return generatePostRequest(path, params, jsonBody);
        /*} else if (method.equalsIgnoreCase('PUT')) {
            return generatePutRequest(path, params);
        } else if (method.equalsIgnoreCase('DELETE')) {
            return generateDeleteRequest(path, params); */
        } else {
            throw new RestApiException('Unknown Method: ' + method);
        }
    }
    
    // TODO: In the future we need to update API data. The below methods should be created for POST, PUT and DELETE.
    /**
     * Generate get request.
     * 
     * @param path
     *            the path
     * @param params
     *            the params
     * @return the http get
     */
    private HttpRequest generateGetRequest(String path, Map<String,String> params) {
        HttpRequest req = new HttpRequest();
                
		// Need it for Salesforce 'Named Credentials' mechanism
        if(isNamedCredentials(path)) {
        	req.setEndpoint(path);
        } else {	
        	URL uri = buildUri(path, params);
	        req.setEndpoint(uri.toExternalForm());
        }

        req.setMethod('GET');
        
        return req;
    }
    
	/**
     * Generate post request. (Currently only used for the OAuth 2.0 token)
     * 
     * @param path
     *            the path
     * @param params
     *            the params
     * @param jsonBody is used if not NULL as body instead of the params map 
     * @return the http post
     */
    private HttpRequest generatePostRequest(String path, Map<String,String> params, String jsonBody) {
        HttpRequest req = new HttpRequest();
		
		// Need it for Salesforce 'Named Credentials' mechanism
        if(isNamedCredentials(path)) {
        	 req.setEndpoint(path);
    	} else {
        	URL uri = buildUri(path);
        	req.setEndpoint(uri.toExternalForm());
        }
        
		//DvtH modification on the twillio Rest to be able to send json body data
		if (jsonBody==null) {
        	jsonBody = buildEntityBody(params);
		}
        
        req.setMethod('POST');
        req.setBody(jsonBody);

        return req;
    }
    
    /**
     * Builds the uri.
     * 
     * @param path
     *            the path
     * @param queryStringParams
     *            the query string params
     * @return the uRI
     */
    private URL buildUri(String path, Map<String,String> queryStringParams) {
        String sb = path;

        if (queryStringParams != null && queryStringParams.size() > 0) {
            /* sb += '&';
            sb += buildEntityBody(queryStringParams);
            */
            
            if(sb.contains('customer')) {
            	sb += buildEntityBody(queryStringParams);
            } else {
            	// Use API search service /customer to find a Customer on FB#, E-mail, Social Network/Handle 
            } 
        }

        URL uri;

        try {
            uri = new URL(sb);
        } catch (Exception e) {
            throw new RestApiException('Invalid uri: '+sb, e);
        }
		
        return uri;
    }

    /**
     * Builds the uri.
     * 
     * @param path
     *            the path
     * @return the uRI
     */
    private URL buildUri(String path) {
        return buildUri(path, new Map<String,String>());
    }

    /**
     * Builds the entity body.
     * 
     * @param params
     *            the params
     * @return the url encoded form entity
     */
    private String buildEntityBody(Map<String,String> params) {
        String entityBody='';
        if (params != null) {
            for (String key : params.keySet()) {
                try {
                    entityBody += (entityBody=='' ? '' : '&')
                                    + key + '=' + EncodingUtil.urlEncode(params.get(key), 'UTF-8');
                } catch (Exception e) {
                    System.debug('*** ERROR: ' + e);
                }
            }
        }
        return entityBody;
    }

	/**
	 * Gets the Access Token from Custom Settings. Else refresh the token
	 * @depricated
	 **/
	public String getAccessToken() {
        AFKLM_SocialCustomerService__c custSetting = AFKLM_SocialCustomerService__c.getValues('CustomerAPI');
		return EncodingUtil.base64Decode( custSetting.SCS_CustomerAPIOAuthAccessToken__c ).toString();
	}
	
	//Scheung Verify if SF Named Credentials is used
	private Boolean isNamedCredentials(String path) {
		if(path.contains('callout:')) {
			return true;
		} else {
			return false;
		}
	}
}