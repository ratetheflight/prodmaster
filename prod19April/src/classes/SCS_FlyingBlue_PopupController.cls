/*************************************************************************************************
* File Name     :  	SCS_FlyingBlue_PopupController  
* Description   :   class for Flying Blue Popup Functionality
* @author       :   Sathish
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     01/02/2017      Sathish         Created the class (ST-1548)

*/
public class SCS_FlyingBlue_PopupController {
    
    public ApexPages.StandardController sc;
    public Case  Caserecord {get;set;}
    public boolean showFlyingblue {get; set;}
    public boolean showsocialmedia {get; set;}
    public Id recid{get;set;}
   
    public SCS_FlyingBlue_PopupController(ApexPages.StandardController sc) {
        
        Sobject obj=sc.getrecord();
        if(obj!=null)
		 {
		 	 Caserecord = (case)obj;
		 }
		 else
		 {
		 	 Caserecord = new Case();
		 }
        showFlyingblue=False;
        showsocialmedia=False;
        String Profileid=(userinfo.getProfileId()).substring(0,(userinfo.getProfileId().length())-3);
        //string flyingblueprofile=Label.Flying_Blue_Profile;
        string[] flyingblueprofile=(Label.Flying_Blue_Profile).split(',');
        system.debug('flyingblueprofile '+flyingblueprofile[0]+'--'+flyingblueprofile[1]);
        system.debug('userinfo.getProfileId() '+(userinfo.getProfileId()).substring(0,(userinfo.getProfileId().length())-3));
        
      if(((Profileid==flyingblueprofile[0] || Profileid==flyingblueprofile[1]) && caserecord.Is_Flying_Blue__c==FALSE))
     {
         	showFlyingblue=FALSE;
          	showsocialmedia=TRUE;
     }
      if(Profileid!=flyingblueprofile[0] && Profileid!=flyingblueprofile[1] && caserecord.Is_Flying_Blue__c==TRUE)
      {
          showFlyingblue=TRUE;
          showsocialmedia=FALSE;
      }
    

    }
    
    public pagereference CheckandSave()
	{
        pagereference prpage = new pagereference('/' + caserecord.id);
        Database.DMLOptions dmo = new Database.DMLOptions();
		if(caserecord.Case_Phase__c=='' || caserecord.Case_Phase__c==NULL)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Fill the Case Phase'));  
            
        }
        else if(caserecord.Case_Topic__c=='' || caserecord.Case_Topic__c==NULL)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Fill the Case Topic'));            
        }
        else if(caserecord.Case_Detail__c=='' || caserecord.Case_Detail__c==NULL)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Fill the Case Detail'));            
        }
        else
        {
            system.debug('caserecord.Is_Flying_Blue__c '+caserecord.Is_Flying_Blue__c);
            if(caserecord.Is_Flying_Blue__c==TRUE)
            {
                if(caserecord.Group__c!=NULL && caserecord.Group__c!='CX')
                {
                    if(caserecord.Group__c=='IGT')
                    {
                       caserecord.Status='Waiting for Internal Answer IGT';
                       dmo.assignmentRuleHeader.useDefaultRule = true;
                    }
                    if(caserecord.Group__c=='Cygnific')
                    {
                        caserecord.Status='Waiting for Internal Answer Cygnific';                         
                        dmo.assignmentRuleHeader.useDefaultRule = true;                        
                    }
                   
                }
                else
                {
                   caserecord.Status='Waiting for Answer Social Media Hub'; 
                   
                }
              	caserecord.Is_Flying_Blue__c=FALSE;
                system.debug('inside first loop');
            }
            else
            {
                caserecord.Status='Waiting for Internal answer Flying Blue';
                caserecord.Is_Flying_Blue__c=TRUE;
                caserecord.OwnerId=Label.Flying_Blue_Queue;
                dmo.assignmentRuleHeader.useDefaultRule = false;                
                system.debug('inside second loop');
            }
          	Database.update(caserecord, dmo);
           //update Caserecord;
           return prpage;
        }
        return null;
	}

}