/*************************************************************************************************
* File Name     :   SCS_FlyingBlue_PopupController_Test  
* Runs On       :   SCS_FlyingBlue_PopupController,SocialPostTriggerHandler
* @author       :   Sathish
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     01/02/2017      Sathish         Created the class (ST-1548)

*/

@isTest
public class SCS_FlyingBlue_PopupController_Test {
    
        static testmethod void testcheckandsave()
        {
            AFKLM_TestDataFactory.loadCustomSettings();
            User usr = AFKLM_TestDataFactory.createUser('Test','CaseUser','System Administrator');
        insert usr;
        //Insert Cases
        System.runAs(usr){
        List<Case> caseList= new List<Case>();
        caseList=AFKLM_TestDataFactory.insertKLMCase(1,'In Progress');
        insert caseList;
        Case cs=new case();
        cs=caselist[0];
        ApexPages.StandardController sc = new ApexPages.StandardController(cs);     
        SCS_FlyingBlue_PopupController FB = new SCS_FlyingBlue_PopupController(sc);
        Test.startTest();
        FB.CheckandSave();
        cs.Case_Phase__c='Orientation';
        update cs;
        ApexPages.StandardController sc1 = new ApexPages.StandardController(cs);        
        SCS_FlyingBlue_PopupController FB1 = new SCS_FlyingBlue_PopupController(sc1);
        FB1.CheckandSave();     
        cs.Case_Topic__c='Health & Wellness (OR)';
        update cs;
        ApexPages.StandardController sc2 = new ApexPages.StandardController(cs);        
        SCS_FlyingBlue_PopupController FB2 = new SCS_FlyingBlue_PopupController(sc2);
        FB2.CheckandSave();        
        cs.Case_Detail__c='Vaccination & Travel Clinic';
        update cs;
        ApexPages.StandardController sc3 = new ApexPages.StandardController(cs);        
        SCS_FlyingBlue_PopupController FB3 = new SCS_FlyingBlue_PopupController(sc3);
        FB3.CheckandSave(); 
        cs.Group__c='IGT';
        cs.Is_Flying_Blue__c=TRUE;
        cs.Language__c='English';
        update cs;
        ApexPages.StandardController sc4 = new ApexPages.StandardController(cs);        
        SCS_FlyingBlue_PopupController FB4 = new SCS_FlyingBlue_PopupController(sc4);
        FB4.CheckandSave();
        Test.stopTest();        
        cs.Group__c='';
        update cs;
        ApexPages.StandardController sc5 = new ApexPages.StandardController(cs);        
        SCS_FlyingBlue_PopupController FB5 = new SCS_FlyingBlue_PopupController(sc5);
        FB5.CheckandSave();
        cs.Group__c='Cygnific';
        update cs;
        ApexPages.StandardController sc6 = new ApexPages.StandardController(cs);        
        SCS_FlyingBlue_PopupController FB6 = new SCS_FlyingBlue_PopupController(sc6);
        FB6.CheckandSave(); 
        }
            
        }

}