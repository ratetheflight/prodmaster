/*************************************************************************************************
* File Name     :   SCS_HerokuOutboundHandler
* Description   :   Class to interface with Heroku (Nexmo) for outbound logic in Cases
* @author       :   Manuel Conde
* Modification Log
===================================================================================================
* Ver.    Date            Author          Modification
*--------------------------------------------------------------------------------------------------
* 1.0     11/02/2016      Manuel Conde    Initial version requested by FB Messenger Project
* 1.1     21/11/2016      Nagavi Babu     ST-324: Added callout to delete PM from iframes specific to Facebook

****************************************************************************************************/
public with sharing class SCS_HerokuOutboundHandler {
    
    public static boolean onAfterUpdate_FirstRun = true;


    public SCS_HerokuOutboundHandler() {
        
    }

    //
    public void sendCasesOnUpdateToHeroku(List<Case> oldCases, List<Case> objectsToUpdate,
                                     Map<Id,Case> oldCasesMap,Map<Id,Case> objectsToUpdateMap){

        if(onAfterUpdate_FirstRun){

            onAfterUpdate_FirstRun = false;

            //RecordType servicingRecType = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Servicing'];

            List<Case> casesClosed2BeSent = new List<Case>();
            List<Case> casesOpen2BeSent = new List<Case>();
                   
            for(Case cs:objectsToUpdate) {
                
                if((objectsToUpdateMap.get(cs.Id).isClosed!=oldCasesMap.get(cs.Id).isClosed 
                        && objectsToUpdateMap.get(cs.Id).isClosed==True) 
                        && (objectsToUpdateMap.get(cs.Id).chatHash__c != null && !objectsToUpdateMap.get(cs.Id).chatHash__c.equals('') 
                        && (objectsToUpdateMap.get(cs.Id).Origin != null && objectsToUpdateMap.get(cs.Id).Origin.equals('Facebook')))){
                        casesClosed2BeSent.add(cs);     
                }                        
                else if((objectsToUpdateMap.get(cs.Id).isClosed!=oldCasesMap.get(cs.Id).isClosed 
                        && objectsToUpdateMap.get(cs.Id).isClosed==false)
                        && (objectsToUpdateMap.get(cs.Id).chatHash__c != null && !objectsToUpdateMap.get(cs.Id).chatHash__c.equals('') 
                        && (objectsToUpdateMap.get(cs.Id).Origin != null && objectsToUpdateMap.get(cs.Id).Origin.equals('Facebook')))){
                        casesOpen2BeSent.add(cs);   
                }                
                
            }

            //Send them!
            sendCases2Heroku(casesClosed2BeSent, casesOpen2BeSent);

        }

    }

    //Private utility to Prepare and Send Cases 
    public void sendCases2Heroku(List<Case> casesClosed2BeSent, List<Case> casesOpen2BeSent){

        if(casesClosed2BeSent.isEmpty() && casesOpen2BeSent.isEmpty()) return;

        JSONGenerator jsonGenerator = JSON.createGenerator(true);
        jsonGenerator.writeStartArray();

        for(Case caseObj : casesClosed2BeSent){
            System.debug('Closed Case: ' + caseObj.chatHash__c);
            boolean trueFlag = true;
            jsonGenerator.writeStartObject();            
            jsonGenerator.writeStringField('chat_hash',caseObj.chatHash__c);
            jsonGenerator.writeBooleanField('closed',trueFlag);
            jsonGenerator.writeEndObject();
        }

        for(Case caseObj : casesOpen2BeSent){
            boolean falseFlag = false;
            jsonGenerator.writeStartObject();
            System.debug('Open Case: ' + caseObj.chatHash__c);
            jsonGenerator.writeStringField('chat_hash',caseObj.chatHash__c);
            jsonGenerator.writeBooleanField('closed', falseFlag);
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();

        String responseJSONString = jsonGenerator.getAsString();

        System.debug('responseJSONString: ' + responseJSONString);

        if(!System.isBatch()){
            System.debug('Non Batch processing: ' + responseJSONString);            
            FutureMethodRecordProcessing.sendCasesStatusUpdates2Heroku(responseJSONString);
        }
        else{
            System.debug('Batch processing: ' + responseJSONString);            
            sendCasesStatusUpdates2Heroku(responseJSONString);    
        }
    }
 
    private void sendCasesStatusUpdates2Heroku(String jsonContent){
        boolean isError = false;

        if(jsonContent == null && jsonContent == '') return;

        System.debug('jsonContent: ' + jsonContent);

        //if(!Test.isRunningTest()){

            HttpRequest request = null;

            try{
                AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('Settings');

                if(custSettings == null) return;

                String endPointConfig = custSettings.SCS_HerokuRestEndpoint__c;
                String userName = custSettings.SCS_HerokuRestApiUser__c;
                String pw = custSettings.SCS_HerokuRestApiPw__c;
                Blob headerValue = Blob.valueOf(userName + ':' + pw);

                request =new HttpRequest();                                                     
                request.setEndpoint(endPointConfig);
                request.setMethod('PUT'); 
                String authorizationHeader = 'Basic ' +
                EncodingUtil.base64Encode(headerValue);
                request.setHeader('Authorization', authorizationHeader);
                request.setHeader('Content-Type' ,'application/json');
                request.setBody(jsonContent);
                system.debug('Heroku Request endpoint'+request.getEndpoint());
                system.debug('Heroku Request body'+request.getBody());        


            } catch(Exception e) {
                system.debug('The following exception has occurred building request to Heroku: ' + e.getMessage());
                isError = true;
            }

            try {
                if(!isError){
                    Http http = new Http();
                    System.debug('Request to Heroku: ' + request.getBody());
                    HTTPResponse response = http.send(request);
                    System.debug('Response from Heroku: ' + response.getBody());
                }

            } catch(Exception e) {
                system.debug('The following exception has occurred sending request to Heroku: ' + e.getMessage());
            }

        //} 
        
       
    }
    
    //Public utility to Prepare and delete social posts ST-324: Nagavi
    public static Boolean preparePostsToHeroku(List<String> pmsList){

        if(pmsList.isEmpty()) return false;

        JSONGenerator jsonGenerator = JSON.createGenerator(true);
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField('message_ids', pmsList);
        jsonGenerator.writeEndObject();

        String responseJSONString = jsonGenerator.getAsString();

        System.debug('responseJSONString: ' + responseJSONString);

        System.debug('Batch processing: ' + responseJSONString);            
        Boolean status=false;
        status=deletePostsInHeroku(responseJSONString); 
        
        return status;           
    }
    
    //Method to initiate the actual callout to remove details from iframe ST-324: Nagavi
    private static Boolean deletePostsInHeroku(String jsonContent){
        boolean isError = false;

        if(jsonContent == null && jsonContent == '') return false;

        System.debug('jsonContent: ' + jsonContent);

        //if(!Test.isRunningTest()){

            HttpRequest request = null;

            try{
                AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('Settings');

                if(custSettings == null) return false;

                String endPointConfig = custSettings.SCS_HerokuRestDeleteEndpoint__c;
                String userName = custSettings.SCS_HerokuRestApiUser__c;
                String pw = custSettings.SCS_HerokuRestApiPw__c;
                Blob headerValue = Blob.valueOf(userName + ':' + pw);
                
                request =new HttpRequest();                                                     
                request.setEndpoint(endPointConfig);
                request.setMethod('DELETE'); 
                String authorizationHeader = 'Basic ' +
                EncodingUtil.base64Encode(headerValue);
                request.setHeader('Authorization', authorizationHeader);
                request.setHeader('Content-Type' ,'application/json');
                request.setBody(jsonContent);        


            } catch(Exception e) {
                system.debug('The following exception has occurred building delete request to Heroku: ' + e.getMessage());
                isError = true;
                return false;
            }

            try {
                if(!isError){
                    Http http = new Http();
                    System.debug('Request to Heroku: ' + request.getBody());
                    HTTPResponse response = http.send(request);
                    System.debug('Response from Heroku: ' + response.getBody());
                    
                    String calloutStatus= response.getStatus();
                    System.debug('calloutStatus' + calloutStatus);
                    if(calloutStatus=='OK')
                        return true;
                    else
                        return false;
                }
                else
                    return false;

            } catch(Exception e) {
                system.debug('The following exception has occurred sending delete request to Heroku: ' + e.getMessage());
                return false;
            }

        //} 
        
       
    }
    
    
 
}