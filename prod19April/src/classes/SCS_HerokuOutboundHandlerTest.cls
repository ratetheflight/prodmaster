/*************************************************************************************************
* File Name     :   SCS_HerokuOutboundHandlerTest
* Description   :   Class to Test SCS_HerokuOutboundHandler Class
* @author       :   Manuel Conde
* Modification Log
===================================================================================================
* Ver.    Date            Author          Modification
*--------------------------------------------------------------------------------------------------
* 1.0     11/02/2016      Manuel Conde    Initial version requested by FB Messenger Project
* 1.1     11/21/2016      Nagavi          Added code coverage for methods related to ST-324

****************************************************************************************************/

@isTest
private class SCS_HerokuOutboundHandlerTest
{

    private static void insertTestObject(String reference, String status, String origin, String ssent, DateTime stagedt, Id rtID){
        Case c = new Case(
            Reference__c = reference,
            Status = status,
            Origin = origin,
            Case_stage_datetime__c = stagedt,
            RecordTypeId = rtID,
            chatHash__c = Datetime.now() + reference);

        insert c;
    }

 
    @isTest
    static void TestSingleOpen2Close()
    {

        AFKLM_SocialCustomerService__c st = new AFKLM_SocialCustomerService__c();
        st.name='Settings';
        st.SCS_HerokuRestEndpoint__c ='https:/test.test';
        st.SCS_HerokuRestApiUser__c = 'user';
        st.SCS_HerokuRestApiPw__c = 'pw';
        insert st;

        RecordType servicingRecType = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Servicing'];
 

        Id rtID = servicingRecType.id;

        insertTestObject('MCONDEtest01', 'New', 'Facebook', 'Negative', Datetime.now().addDays(-4), rtID);
        insertTestObject('MCONDEtest02', 'New', 'Facebook', 'Negative', Datetime.now().addDays(-4), rtID);

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new HerokuWebServiceMockImpl());

        Case testObj1 = [select Status, isclosed,chatHash__c from Case WHERE Reference__c = 'MCONDEtest01'];
        Case testObj2 = [select Status, isclosed,chatHash__c from Case WHERE Reference__c = 'MCONDEtest02'];

        System.assertEquals(testObj1.Status, 'New', 'Status for test 01 is not equal to New');

        testObj1.status = 'Closed - No Response';

        update testObj1;

        System.assertEquals(testObj1.Status, 'Closed - No Response', 'Status for test 01 is not equal to Closed - No Response');        

        Test.stopTest();
    }

    @isTest
    static void TestSingleClose2Open()
    {

        AFKLM_SocialCustomerService__c st = new AFKLM_SocialCustomerService__c();
        st.name='Settings';
        st.SCS_HerokuRestEndpoint__c = 'https:/test.test';
        st.SCS_HerokuRestApiUser__c = 'user';
        st.SCS_HerokuRestApiPw__c = 'pw';
        insert st;

        RecordType servicingRecType = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Servicing'];


        Id rtID = servicingRecType.id;

        insertTestObject('MCONDEtest01', 'New', 'Facebook', 'Negative', Datetime.now().addDays(-4), rtID);

        Test.startTest();
 
        Test.setMock(HttpCalloutMock.class, new HerokuWebServiceMockImpl());

        Case testObj1 = [select Status, isclosed,chatHash__c from Case WHERE Reference__c = 'MCONDEtest01'];

        System.assertEquals(testObj1.Status, 'New', 'Status for test 01 is not equal to New');

        testObj1.status = 'Closed - No Response';

        update testObj1;

        testObj1 = [select Status, isclosed,chatHash__c from Case WHERE Reference__c = 'MCONDEtest01'];
        
        testObj1.status = 'New';

        update testObj1;

        testObj1 = [select Status, isclosed,chatHash__c from Case WHERE Reference__c = 'MCONDEtest01'];

        System.assertEquals(testObj1.Status, 'New', 'Status for test 01 is not equal to New');      

        Test.stopTest();
    }

    @isTest
    static void TestSingleCallout()
    {

        AFKLM_SocialCustomerService__c st = new AFKLM_SocialCustomerService__c();
        st.name='Settings';
        st.SCS_HerokuRestEndpoint__c = 'https:/test.test';
        st.SCS_HerokuRestApiUser__c = 'user';
        st.SCS_HerokuRestApiPw__c = 'pw';
        insert st;

        RecordType servicingRecType = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Servicing'];


        Id rtID = servicingRecType.id;

        insertTestObject('MCONDEtest01', 'New', 'Facebook', 'Negative', Datetime.now().addDays(-4), rtID);

        
 
        List<Case> testObj1 = [select Status, isclosed,chatHash__c from Case WHERE Reference__c = 'MCONDEtest01'];

        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new HerokuWebServiceMockImpl());

        SCS_HerokuOutboundHandler handler = new SCS_HerokuOutboundHandler();
        handler.sendCases2Heroku(testObj1,testObj1);

        Test.stopTest();
    }

    static testMethod void testBatchCaseClose() {
        // insert new AFKLM_SCS_CaseAutoClose__c(Number_of_Days__c = -1.0);
        AFKLM_SCS_CaseAutoClose__c ac = new AFKLM_SCS_CaseAutoClose__c();
        ac.Number_of_Days__c = 1;
        insert ac;

        AFKLM_SocialCustomerService__c st = new AFKLM_SocialCustomerService__c();
        st.name='Settings';
        st.SCS_HerokuRestEndpoint__c = 'https:/test.test';
        st.SCS_HerokuRestApiUser__c = 'user';
        st.SCS_HerokuRestApiPw__c = 'pw';
        insert st;        
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Servicing' AND SobjectType = 'Case' LIMIT 1];
                
        insertTestBatchObject('test01B', 'Waiting for Client Answer', 'Facebook', 'Negative', Datetime.now().addDays(-4), rt);
        insertTestBatchObject('test02B', 'Waiting for Client Answer', 'Facebook', 'Negative', Datetime.now().addDays(-3), rt);
        insertTestBatchObject('test03B', 'Waiting for Client Answer', 'Facebook', 'Negative', Datetime.now().addDays(-1), rt);
        insertTestBatchObject('test04B', 'New', 'Facebook', 'Negative', Datetime.now().addDays(-3), rt);
        insertTestBatchObject('test05B', 'Reopened', 'Facebook', 'Negative', Datetime.now().addDays(-2), rt);
        insertTestBatchObject('test06B', 'Closed - No Response', 'Facebook', 'Negative', Datetime.now().addDays(-1), rt);

        Case testObj1 = [select Status from Case WHERE Reference__c = 'test01B'];     
        Case testObj2 = [select Status from Case WHERE Reference__c = 'test02B']; 
        Case testObj3 = [select Status from Case WHERE Reference__c = 'test03B']; 
        Case testObj4 = [select Status from Case WHERE Reference__c = 'test04B']; 
        Case testObj5 = [select Status from Case WHERE Reference__c = 'test05B']; 
        Case testObj6 = [select Status from Case WHERE Reference__c = 'test06B']; 

        Test.startTest();       
        Database.executeBatch(new AFKLM_CaseCloseWaitOnCustomerBatch(), 3000);     
        Test.stopTest();
        
        System.assertEquals(testObj1.Status, 'Waiting for Client Answer', 'Status for test 01 is not equal to Waiting for client answer');
        System.assertEquals(testObj2.Status, 'Waiting for Client Answer', 'Status for test 02 is not equal to Waiting for client answer');
        System.assertEquals(testObj3.Status, 'Waiting for Client Answer', 'Status for test 03 is not equal to Waiting for Client Answer');
        System.assertEquals(testObj4.Status, 'New', 'Status for test 04 is not equal to New');
        System.assertEquals(testObj5.Status, 'Reopened', 'Status for test 05 is not equal to Reopened');
        System.assertEquals(testObj6.Status, 'Closed - No Response', 'Status for test 06 is not equal to Closed');
    }
    
    private static void insertTestBatchObject(String reference, String status, String origin, String ssent, DateTime stagedt, RecordType rt){
        Case c = new Case(
            Reference__c = reference,
            Status = status,
            Origin = origin,
            chatHash__c = reference,
            Case_stage_datetime__c = stagedt,
            RecordTypeId = rt.Id
        );

        insert c;
    }
    
    //Test method to check delete social post callouts ST-324
    @isTest
    static void TestDeleteSPCallout()
    {

        AFKLM_SocialCustomerService__c st = new AFKLM_SocialCustomerService__c();
        st.name='Settings';
        st.SCS_HerokuRestDeleteEndpoint__c = 'https:/testdelete.test';
        st.SCS_HerokuRestApiUser__c = 'user';
        st.SCS_HerokuRestApiPw__c = 'pw';
        insert st;
        
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78956788');
        insert newPerson;
        
        //Insert persona
        List<SocialPersona> personaList=new List<SocialPersona>();
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList;
        
        //Insert for SocialPost
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,5);
       
        //Get the Message Ids
        List<string> msgList=new List<string>();
        for(SocialPost sp:tempSocialPostList){
            msgList.add(sp.ExternalPostId__c);    
        }
        Test.startTest();

        Test.setMock(HttpCalloutMock.class, new HerokuWebServiceMockImpl());

        SCS_HerokuOutboundHandler.preparePostsToHeroku(msgList);

        Test.stopTest();
    }
}