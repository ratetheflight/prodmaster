/**
 * @author (s)    	: David van 't Hooft
 * @requirement id  : 
 * @description   	: Test class to test the SCS_WeChatCreateSocialPostPersona trigger
 * @log:            : 06OCT2015 v1.0 
 */
@isTest
private class SCS_WeChatCreateSocialPostPersonaTest {

    static testMethod void outboundHappyFlowTest() {
    	Account acc = new Account(FirstName='Test', Lastname='Account', sf4twitter__Fcbk_User_Id__pc='FB_123456789');
     	insert acc;
     	
		AFKLM_SCS_WeChatSettings__c custSettings = new AFKLM_SCS_WeChatSettings__c(
			Name = 'Settings', 
			SCS_SysAccountName_del__c = 'SFDC WeChat', 
			SCS_SysAccountId__c = acc.Id,
			SCS_SysPersonaExternalId__c = 'WECHAT123456789',
			SCS_SysPersonaId__c = '0SP20000000wHLK',
			SCS_SysPersonaName__c = 'WECHAT123456789',
			SCS_TemporaryPersonAccount__c = '001g000000VM85q');
    	
    	insert custSettings;
    	
		Integer i = 50;
		List<Nexmo_Post__c> nexPost = new List<Nexmo_Post__c>();

		for(Integer j=0;j<i;j++){
			Nexmo_Post__c np = new Nexmo_Post__c(
				From_User_Id__c ='1234567'+j,
				Chat_Hash__c='1234567chatHash'+j,
				Message_Id__c='1234567messsageId'+j,
				Send_Time__c=System.now(),
				Source__c='Outbound',
				Text__c='This is testing content on NexmoPost object '+j,
				To_Id__c='1234567toId',
				Type__c='text',
				User_Real_Name__c='TestingRealName '+j,
				Social_Network__c='WeChat'
				);

			nexPost.add(np);
		}

		
		test.startTest();
		insert nexPost;
		test.stopTest();		
		
        List<SocialPersona> personaList = [SELECT Id, ParentId, ExternalId FROM SocialPersona where Provider = 'WeChat' LIMIT 1000];

		System.assertEquals(1, personaList.size(), 'Needs to be 1 As the Outbound is always the KLM Social Persona' );
        
        List<SocialPost> socialPost = [SELECT Id, MediaType, Name, Language__c, OwnerId, Provider, MessageType FROM SocialPost LIMIT 1000];
		System.assertEquals(i, socialPost.size(), 'Needs to be as much as the created nextmo records' );
    
        List <SocialPersona> spList = [Select s.ParentId, s.Id, (Select Id, CreatedDate From Posts order by CreatedDate limit 1) From SocialPersona s limit 1000];
		for(SocialPersona spersona : spList) {
			System.assertNotEquals(spersona.Posts[0].Id, spersona.ParentId, 'Id\'s need to differ' );
		}
    
    }


    static testMethod void inboundHappyFlowTest() {
		AFKLM_SCS_WeChatSettings__c custSettings = new AFKLM_SCS_WeChatSettings__c(
			Name = 'Settings', 
			SCS_SysAccountName_del__c = 'SFDC WeChat', 
			SCS_SysAccountId__c = '0012000001R4Yzf',
			SCS_SysPersonaExternalId__c = 'WECHAT123456789',
			SCS_SysPersonaId__c = '0SP20000000wHLK',
			SCS_SysPersonaName__c = 'WECHAT123456789',
			SCS_TemporaryPersonAccount__c = '00120000016N8l1');
    	
    	insert custSettings;
    	
		Integer i = 50;
		List<Nexmo_Post__c> nexPost = new List<Nexmo_Post__c>();

		for(Integer j=0;j<i;j++){
			Nexmo_Post__c np = new Nexmo_Post__c(
				From_User_Id__c ='1234567'+j,
				Chat_Hash__c='1234567chatHash'+j,
				Message_Id__c='1234567messsageId'+j,
				Send_Time__c=System.now(),
				Source__c='Inbound',
				Text__c='This is testing content on NexmoPost object '+j,
				To_Id__c='1234567toId',
				Type__c='text',
				User_Real_Name__c='TestingRealName '+j,
				Social_Network__c='WeChat'
				);

			nexPost.add(np);
		}

		
		test.startTest();
		insert nexPost;
		test.stopTest();		
		
        List<SocialPersona> personaList = [SELECT Id, ParentId, ExternalId FROM SocialPersona where Provider = 'WeChat' LIMIT 1000];

		System.assertEquals(i, personaList.size(), 'Needs to be as much as the created nextmo records' );
        
        List<SocialPost> socialPost = [SELECT Id, MediaType, Name, Language__c, OwnerId, Provider, MessageType FROM SocialPost LIMIT 1000];
		System.assertEquals(i, socialPost.size(), 'Needs to be as much as the created nextmo records' );
       
    
        List <SocialPersona> spList = [Select s.ParentId, s.Id, (Select Id, CreatedDate From Posts order by CreatedDate limit 1) From SocialPersona s limit 1000];
		for(SocialPersona spersona : spList) {
			System.assertEquals(spersona.Posts[0].Id, spersona.ParentId, 'Id\'s needs to be the same' );
		}
    
    }
    
}