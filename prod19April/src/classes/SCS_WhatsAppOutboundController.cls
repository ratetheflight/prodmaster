public with sharing class SCS_WhatsAppOutboundController {

	public Case cs;
	public String accId{get;set;}

	public String manAccount {
		get{ 
			return manAccount = 'WhatsApp outbound service';
		} 
		set{}
	}
	public String newMessage{get;set;}
	public String inReplyToMessage{get;set;}
	
	public List<SocialPost> spost {get;set;}

	private static final String WHATSAPP_API = 'https://eazy.im/api/1.0/outbound';

	public SCS_WhatsAppOutboundController(ApexPages.StandardController caseController) {

		if(!Test.isRunningTest()){
           //caseController.addFields(new List<String>{'AccountId'});
        }

		this.cs = (Case) caseController.getRecord();
		this.accId = cs.AccountId;
		getSocialPost();
		
	}

	public List<SocialPost> getSocialPost(){
		spost = new List<SocialPost>();
		spost = [SELECT id, Content, Handle, Provider, Posted, PersonaId, AttachmentURL, TopicProfileName FROM SocialPost WHERE parentId =: cs.id AND IsOutbound = false AND Provider = 'WhatsApp' ORDER BY Posted DESC LIMIT 5];

		if(!spost.isEmpty()){
			return spost;
		} 

		return spost;
	}

	public PageReference sendMessage(){
		SocialPost sp = new SocialPost(
			Content = newMessage,
			ParentId = cs.id,
			Name = 'Outbound message to: '+spost[0].Handle,
			IsOutbound = true,
			Provider = 'WhatsApp',
			Handle = 'KLM',
			MessageType = 'Private',
			ReplyToId = spost[0].id, 
			Posted = System.now(),
			Status = 'Sent'

			);
		
			invokeWebService();
			insert sp;

			refreshMessagePanel();
				
		return null;

	}

	public String getKLMid(){
		String KLMid = [SELECT id FROM Account WHERE Name=:'KLM' LIMIT 1].id;

		return KLMid;
	}

	public Account getCaseAccountId(){
		Account acc = [SELECT id FROM Account WHERE id=:accid];
		return acc;
	}
	
	public void invokeWebService(){
		
		SocialPersona persona;
		
		String reportId = spost[0].id;
		String apiKey = spost[0].TopicProfileName;
		String personaId = spost[0].PersonaId;

		if(personaId != null){
			persona = [SELECT id, ExternalId FROM SocialPersona WHERE id=:personaId LIMIT 1];
		}

		String endpoint = '';

		//if(UserInfo.getOrganizationId() == '00Dg0000003LWXn'){
			//endpoint = 'https://non.existingWhatsApp/api/1.0/outbound';
		//} else {
			endpoint = 'https://eazy.im/api/1.0/outbound';
		//}
		
		String customerWhatsAppNumber = persona.ExternalId;
		
		//String contentToSend = '{"content":{"text": "'+EncodingUtil.urlEncode(newMessage, 'UTF-8')+'"},"key": "'+apiKey+'","reportId": "'+reportId+'","to":"'+customerWhatsAppNumber+'@s.whatsapp.net","type": "text"}';
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeFieldName('content');

			gen.writeStartObject();
			gen.writeObjectField('text',newMessage);
			gen.writeEndObject();
		
		gen.writeStringField('key', apiKey); 
		gen.writeStringField('reportId', reportId);
		gen.writeStringField('to', customerWhatsAppNumber+'@s.whatsapp.net');
		gen.writeStringField('type', 'text');

//'{  
//	"content": {"text": "'+EncodingUtil.urlEncode(newMessage, 'UTF-8')+'"},
//	"key": "'+apiKey+'",
//  "reportId": "'+reportId+'",
//  "to":"'+customerWhatsAppNumber+'@s.whatsapp.net",
//  "type": "text"
//}';

		gen.writeEndObject();
		String contentToSend = gen.getAsString();

		//**********************************************************************
        RestApiResponse res;
        RestApiService restApiClient = new RestApiService();

        if(!Test.isRunningTest()){
            restApiClient.setEndpoint('https://eazy.im/api/1.0/outbound');
        } else {
            restApiClient.setEndpoint(WHATSAPP_API);
        }

        try {

            res = restApiClient.safeRequest(endpoint, 'POST', new Map<String,String>(), contentToSend);

        } catch(Exception e) {
            system.debug('The following exception has occurred: ' + e.getMessage());
        }
	}
		
	private void refreshMessagePanel(){
		newMessage = '';
	}

}