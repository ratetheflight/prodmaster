@isTest
private class SCS_WhatsAppOutboundControllerTest {

	
	@isTest 
	static void testSCS_WhatsAppOutboundController() {
		Case tstCase = caseController();
		//*****init controller test*****
		ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(tstCase);
		SCS_WhatsAppOutboundController outboundController = new SCS_WhatsAppOutboundController(stdController);
		System.assertEquals(tstCase, outboundController.cs);

		

		//*****testgetKLMId*****
		Account klmAccount = new Account(
			LastName = 'KLM');
		insert klmAccount;

		String returnedId = outboundController.getKLMid();

		System.assertEquals(klmAccount.id, returnedId);

		

		//*****testgetCaseAccountId*****
		Account getAccountId = outboundController.getCaseAccountId();

		Account accToCheck = [SELECT id FROM Account WHERE id=:tstCase.accountId LIMIT 1];

		System.assertEquals(accToCheck, getAccountId);


		//*****testSendMessage*****
		outboundController.newMessage = 'This is new test message';
		outboundController.sendMessage();

		System.assertEquals('',outboundController.newMessage);
	}
	
	private static Case caseController(){
		Account acc = new Account(LastName='Testing Account');
		insert acc;

		SocialPersona persona = new SocialPersona(
			Name = 'Testing Persona',
			ParentId = acc.Id,
			Provider = 'Whatsapp'
			);
		insert persona;

		Case cs = new Case(
			AccountId = acc.id,
			Status = 'New'

			);
		insert cs;

		SocialPost sp = new SocialPost(
			Name = 'Post from Testing User',
			Content = 'Content of social post',
			ParentId = cs.id,
			Handle = 'Testing User', 
			Provider = 'Whatsapp', 
			Posted = System.now(), 
			PersonaId = persona.id, 
			AttachmentURL = 'url', 
			TopicProfileName = 'abc123xyz'
			);
		insert sp;


		return cs;
	}
	
}