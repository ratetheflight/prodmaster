/**********************************************************************
 Name:  SMHLiveDashboardCntrl
 Task:    N/A
 Runs on: SMHLiveDashboardCntrl
======================================================
Purpose: 
   Controller for SMH Live Dashboard
======================================================
History                                                            
-------                                                            
VERSION         AUTHOR            DATE             DETAIL                                 
    1.0         Pushkar           03/27/2017       ST-1971 - SMH Live Dashboard
    1.1         Sai Choudhry      04/06/2017       ST-1887 - Adding more components to the dahsboard.
***********************************************************************/
public class SMHLiveDashboardCntrl{

    public Integer countPMFB {get;set;}
    public Integer countPostFB {get;set;}
    public Integer countWeChat {get;set;}
    public Integer countTweets {get;set;}
    public Integer countTweetsDM {get;set;}
    
    public Integer countPMFBNorm {get;set;}
    public Integer countPMFBUrgent {get;set;}
    public Integer countPMFBFB {get;set;}
    public Integer countPostFBNorm {get;set;}
    public Integer countPostFBUrgent {get;set;}
    public Integer countPostFBFB {get;set;}
    public Integer countTweetsNorm {get;set;}
    public Integer countTweetsUrgent {get;set;}
    public Integer countTweetsFB {get;set;}
    public Integer countTweetsDMNorm {get;set;}
    public Integer countTweetsDMUrgent {get;set;}
    public Integer countTweetsDMFB {get;set;}
    //public Integer countReTweets {get;set;}
    public Integer countWeChatNorm {get;set;}
    public Integer countWeChatUrgent {get;set;}
    public Integer countWeChatFB {get;set;}
    
    public Integer avgWaitTimePMFB {get;set;}
    public Integer avgWaitTimePostFB {get;set;}
    public Integer avgWaitTimeTweet {get;set;}
    public Integer avgWaitTimeTweetDM {get;set;}
    //public Integer avgWaitTimeReTweet {get;set;}
    public Integer avgWaitTimeWeChat {get;set;}
    public Integer avgWaitTimePMFBYest {get;set;}
    public Integer avgWaitTimePostFBYest {get;set;}
    public Integer avgWaitTimeTweetYest {get;set;}
    public Integer avgWaitTimeTweetDMYest {get;set;}
    public Integer avgWaitTimeWeChatYest {get;set;}
    
    public Integer avgWaitTimeEngl {get;set;}
    public Integer avgWaitTimeDutch {get;set;}
    public Integer avgWaitTimeFrench {get;set;}
    public Integer avgWaitTimePortuguese {get;set;}
    public Integer avgWaitTimeJpn {get;set;}
    public Integer avgWaitTimeSpn {get;set;}
    public Integer avgWaitTimeGerman {get;set;}
    public Integer avgWaitTimeItalian {get;set;}
    public Integer avgWaitTimeKorean {get;set;}
    public Integer avgWaitTimeChinese {get;set;}
    public Integer avgWaitTimeEnglYest {get;set;}
    public Integer avgWaitTimeDutchYest {get;set;}
    public Integer avgWaitTimeFrenchYest {get;set;}
    public Integer avgWaitTimePortugueseYest {get;set;}
    public Integer avgWaitTimeJpnYest {get;set;}
    public Integer avgWaitTimeSpnYest {get;set;}
    public Integer avgWaitTimeGermanYest {get;set;}
    public Integer avgWaitTimeItalianYest {get;set;}
    public Integer avgWaitTimeKoreanYest {get;set;}
    public Integer avgWaitTimeChineseYest {get;set;}
    
    //Added by sai. ST-1887
    //Start
    public Integer fbPublicOldest{get; set;}
    public Integer fbPmOldest{get; set;}
    public Integer twPublicOldest{get; set;}
    public Integer twDmOldest{get; set;}
    public Integer weChatOldest{get; set;}
    
    public Integer englishOldest{get; set;}
    public Integer dutchOldest{get; set;}
    public Integer frenchOldest{get; set;}
    public Integer portugueseOldest{get; set;}
    public Integer japaneseOldest{get; set;}
    public Integer spanishOldest{get; set;}
    public Integer germanOldest{get; set;}
    public Integer italianOldest{get; set;}
    public Integer koreanOldest{get; set;}
    public Integer ChineseOldest{get; set;}
    //End
    
    public List<ChartDataStck> postUnanswered {get;set;}
    public List<ChartDataStck> postAvgWaitTime {get;set;}
    public List<ChartDataStck> postAvgWaitTimeLang {get;set;}
    public List<ChartDataStck> postOldestChannel {get;set;}
    public List<ChartDataStck> postOldestLanguage {get;set;}
    
    public SMHLiveDashboardCntrl(){
        incrementCounter();
    }

    public PageReference incrementCounter() {
    
        avgWaitTimeEngl = 0;
        avgWaitTimeDutch = 0;
        avgWaitTimeFrench = 0;
        avgWaitTimePortuguese = 0;
        avgWaitTimeJpn = 0;
        avgWaitTimeSpn = 0;
        avgWaitTimeGerman = 0;
        avgWaitTimeItalian = 0;
        avgWaitTimeKorean = 0;
        avgWaitTimeChinese = 0;
        avgWaitTimeEnglYest = 0;
        avgWaitTimeDutchYest = 0;
        avgWaitTimeFrenchYest = 0;
        avgWaitTimePortugueseYest = 0;
        avgWaitTimeJpnYest = 0;
        avgWaitTimeSpnYest = 0;
        avgWaitTimeGermanYest = 0;
        avgWaitTimeItalianYest = 0;
        avgWaitTimeKoreanYest = 0;
        avgWaitTimeChineseYest = 0;
        
        avgWaitTimePMFB = 0;
        avgWaitTimePostFB  = 0;
        avgWaitTimeTweet = 0;
        avgWaitTimeTweetDM  = 0;
        //avgWaitTimeReTweet = 0;
        avgWaitTimeWeChat = 0;
        avgWaitTimePMFBYest = 0;
        avgWaitTimePostFBYest  = 0;
        avgWaitTimeTweetYest = 0;
        avgWaitTimeTweetDMYest  = 0;
        avgWaitTimeWeChatYest = 0;
        
        Set<SocialPost> PMFBSet = new Set<SocialPost>();
        Set<SocialPost> PMFBSetUrg = new Set<SocialPost>();
        Set<SocialPost> PMFBSetFB = new Set<SocialPost>();
        Set<SocialPost> PostFBSet = new Set<SocialPost>();
        Set<SocialPost> PostFBSetUrg = new Set<SocialPost>();
        Set<SocialPost> PostFBSetFB = new Set<SocialPost>();
        Set<SocialPost> TweetsDMSet = new Set<SocialPost>();
        Set<SocialPost> TweetsDMSetUrg = new Set<SocialPost>();
        Set<SocialPost> TweetsDMSetFB = new Set<SocialPost>();
        Set<SocialPost> TweetsSet = new Set<SocialPost>();
        Set<SocialPost> TweetsSetUrg = new Set<SocialPost>();
        Set<SocialPost> TweetsSetFB = new Set<SocialPost>();
        Set<SocialPost> WeChatSet = new Set<SocialPost>();
        Set<SocialPost> WeChatSetUrg = new Set<SocialPost>();
        Set<SocialPost> WeChatSetFB = new Set<SocialPost>();
        
        //Added by Sai. ST-1887.
        //Start
        fbPublicOldest = 0;
        fbPmOldest = 0;
        twPublicOldest = 0;
        twDmOldest = 0;
        weChatOldest = 0;
        
        englishOldest = 0;
        dutchOldest = 0;
        frenchOldest = 0;
        portugueseOldest = 0;
        japaneseOldest = 0;
        spanishOldest = 0;
        germanOldest = 0;
        italianOldest = 0;
        koreanOldest = 0;
        ChineseOldest = 0;
        
        List<SocialPost> PMFBList = new List<SocialPost>();
        List<SocialPost> PostFBList = new List<SocialPost>();
        List<SocialPost> TweetsDMList = new List<SocialPost>();
        List<SocialPost> TweetsList = new List<SocialPost>();
        List<SocialPost> WeChatList = new List<SocialPost>();
        
        List<SocialPost> englishList = new List<SocialPost>();
        List<SocialPost> dutchList = new List<SocialPost>();
        List<SocialPost> frenchList = new List<SocialPost>();
        List<SocialPost> portugueseList = new List<SocialPost>();
        List<SocialPost> japansesList = new List<SocialPost>();
        List<SocialPost> spanishList = new List<SocialPost>();
        List<SocialPost> germanList = new List<SocialPost>();
        List<SocialPost> italianList = new List<SocialPost>();
        List<SocialPost> koreanList = new List<SocialPost>();
        List<SocialPost> chineseList = new List<SocialPost>();
        
        List<SocialPost> socialPostList = [SELECT id,MessageType,Provider,Language__c,Reply_speed__c,TopicProfileName,Company__c,Is_Urgent__c,Proposed_Urgent__c,FlyingBlue__c FROM SocialPost WHERE SCS_Status__c NOT IN ('Actioned','Mark as reviewed','Mark As Reviewed Auto','Engaged') AND Scs_MarkAsReviewed__c = False AND Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') AND MessageType != 'ReTweet' AND SCS_IsOutbound__c=false AND (TopicProfileName like '%KLM%' OR Company__c like '%KLM%') AND CreatedDate IN (THIS_MONTH,LAST_MONTH) LIMIT 50000];
        //End.
        
        for(SocialPost sp : socialPostList ){
        
            if(sp.MessageType == 'Private' && sp.Provider=='Facebook' && !sp.FlyingBlue__c)
                PMFBSet.add(sp);
            if(sp.MessageType == 'Private' && sp.Provider=='Facebook' && sp.FlyingBlue__c)
                PMFBSetFB.add(sp);
            if(sp.MessageType == 'Private' && sp.Provider=='Facebook' && (sp.Is_Urgent__c || sp.Proposed_Urgent__c) && !sp.FlyingBlue__c)
                PMFBSetUrg.add(sp); 
            if((sp.MessageType == 'Post' || sp.MessageType == 'Comment' || sp.MessageType == 'Reply') && sp.Provider=='Facebook' && !sp.FlyingBlue__c)
                PostFBSet.add(sp);  
            if((sp.MessageType == 'Post' || sp.MessageType == 'Comment' || sp.MessageType == 'Reply') && sp.Provider=='Facebook' && sp.FlyingBlue__c)
                PostFBSetFB.add(sp);
            if((sp.MessageType == 'Post' || sp.MessageType == 'Comment' || sp.MessageType == 'Reply') && sp.Provider=='Facebook' && (sp.Is_Urgent__c || sp.Proposed_Urgent__c) && !sp.FlyingBlue__c)
                PostFBSetUrg.add(sp);
            if(sp.MessageType == 'Direct' && sp.Provider=='Twitter' && !sp.FlyingBlue__c)
                TweetsDMSet.add(sp);
            if(sp.MessageType == 'Direct' && sp.Provider=='Twitter' && sp.FlyingBlue__c)
                TweetsDMSetFB.add(sp);
            if(sp.MessageType == 'Direct' && sp.Provider=='Twitter' && (sp.Is_Urgent__c || sp.Proposed_Urgent__c) && !sp.FlyingBlue__c)
                TweetsDMSetUrg.add(sp);
            if((sp.MessageType == 'Tweet' || sp.MessageType == 'Reply') && sp.Provider=='Twitter' && !sp.FlyingBlue__c)
                TweetsSet.add(sp);
            if((sp.MessageType == 'Tweet' || sp.MessageType == 'Reply') && sp.Provider=='Twitter' && sp.FlyingBlue__c)
                TweetsSetFB.add(sp);
            if((sp.MessageType == 'Tweet' || sp.MessageType == 'Reply') && sp.Provider=='Twitter' && (sp.Is_Urgent__c || sp.Proposed_Urgent__c) && !sp.FlyingBlue__c)
                TweetsSetUrg.add(sp);
            if(sp.Provider=='WeChat' && !sp.FlyingBlue__c)
                WeChatSet.add(sp);
            if(sp.Provider=='WeChat' && sp.FlyingBlue__c)
                WeChatSetFB.add(sp);
            if(sp.Provider=='WeChat' && (sp.Is_Urgent__c || sp.Proposed_Urgent__c) && !sp.FlyingBlue__c)
                WeChatSetUrg.add(sp);
        
        }
        
        //Added by Sai. ST-1887.
        //Start
        for(SocialPost SPO : socialPostList ){
            if(SPO.MessageType == 'Private' && SPO.Provider=='Facebook' && PMFBList.isempty()){
                PMFBList.add(SPO);
                fbPmOldest=PMFBList[0].Reply_speed__c.intValue();
                }
            if((SPO.MessageType == 'Post' || SPO.MessageType == 'Comment' || SPO.MessageType == 'Reply') && SPO.Provider=='Facebook' && PostFBList.isempty()){
                PostFBList.add(SPO);
                fbPublicOldest=PostFBList[0].Reply_speed__c.intValue();
                }
            if(SPO.MessageType == 'Direct' && SPO.Provider=='Twitter' && TweetsDMList.isempty()){
                TweetsDMList.add(SPO);
                twDmOldest=TweetsDMList[0].Reply_speed__c.intValue();
                }
            if((SPO.MessageType == 'Tweet' || SPO.MessageType == 'Reply') && SPO.Provider=='Twitter' && TweetsList.isempty()) {
                TweetsList.add(SPO);  
                twPublicOldest=TweetsList[0].Reply_speed__c.intValue();
                }              
            if(SPO.Provider=='WeChat' && WeChatList.isempty()){
                WeChatList.add(SPO);  
                weChatOldest=WeChatList[0].Reply_speed__c.intValue();
                } 
        }
        
        for(SocialPost spl : socialPostList )
        {
            if(englishList.isempty() || dutchList.isempty() || frenchList.isempty() || portugueseList.isempty() || japansesList.isempty() || spanishList.isempty() || germanList.isempty() || italianList.isempty() || koreanList.isempty() || chineseList.isempty())
            {
                if(spl.Language__c=='English' && englishList.isempty()){
                    englishList.add(spl);
                    englishOldest=englishList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='Dutch' && dutchList.isempty()){
                    dutchList.add(spl);
                    dutchOldest=dutchList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='French' && frenchList.isempty()){
                    frenchList.add(spl);
                    frenchOldest=frenchList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='Portuguese' && portugueseList.isempty()){
                    portugueseList.add(spl);
                    portugueseOldest=portugueseList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='Japanese' && japansesList.isempty()){
                    japansesList.add(spl);
                    japaneseOldest=japansesList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='Spanish' && spanishList.isempty()){
                    spanishList.add(spl);
                    spanishOldest=spanishList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='German' && germanList.isempty()){
                    germanList.add(spl);
                    germanOldest=germanList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='Italian' && italianList.isempty()){
                    italianList.add(spl);
                    italianOldest=italianList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='Korean' && koreanList.isempty()){
                    koreanList.add(spl);
                    koreanOldest=koreanList[0].reply_speed__c.intValue();
                    }
                if(spl.Language__c=='Chinese' && chineseList.isempty()){
                    chineseList.add(spl);
                    chineseOldest=chineseList[0].reply_speed__c.intValue();
                    }
            }
        }        
        //End
         
        countPMFBNorm = PMFBSet.size() - PMFBSetUrg.size();
        countPMFBUrgent = PMFBSetUrg.size();
        countPMFBFB = PMFBSetFB.size();
        countPostFBNorm = PostFBSet.size() - PostFBSetUrg.size();
        countPostFBUrgent = PostFBSetUrg.size();
        countPostFBFB = PostFBSetFB.size();
        countTweetsDMNorm = TweetsDMSet.size() - TweetsDMSetUrg.size();
        countTweetsDMUrgent = TweetsDMSetUrg.size();
        countTweetsDMFB = TweetsDMSetFB.size();
        countTweetsNorm = TweetsSet.size() - TweetsSetUrg.size();
        countTweetsUrgent = TweetsSetUrg.size();
        countTweetsFB = TweetsSetFB.size();
        countWeChatNorm = WeChatSet.size() - WeChatSetUrg.size();
        countWeChatUrgent = WeChatSetUrg.size(); 
        countWeChatFB = WeChatSetFB.size(); 
        
        //Added by Sai.ST-1887.
        //Start.
        postOldestChannel = new List<ChartDataStck >();
        postOldestChannel.add(new ChartDataStck ('FB PMs',fbPmOldest,0,0));
        postOldestChannel.add(new ChartDataStck ('FB Public',fbPublicOldest,0,0));
        postOldestChannel.add(new ChartDataStck ('Twt DM',twDmOldest,0,0));
        postOldestChannel.add(new ChartDataStck ('Twt Public',twPublicOldest,0,0));
        postOldestChannel.add(new ChartDataStck ('WeChat',weChatOldest,0,0));   
        
        postOldestLanguage = new List<ChartDataStck >();
        postOldestLanguage.add(new ChartDataStck('English',englishOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('Dutch',dutchOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('French',frenchOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('Portuguese',portugueseOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('Japanese',japaneseOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('Spanish',spanishOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('German',germanOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('Italian',italianOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('Korean',koreanOldest,0,0));
        postOldestLanguage.add(new ChartDataStck('Chinese',ChineseOldest,0,0));   
        //End.
        
        postUnanswered = new List<ChartDataStck >();
        postUnanswered.add(new ChartDataStck ('FB PMs',countPMFBUrgent,countPMFBNorm,countPMFBFB));
        postUnanswered.add(new ChartDataStck ('FB Public',countPostFBUrgent,countPostFBNorm,countPostFBFB));
        postUnanswered.add(new ChartDataStck ('Twt DM',countTweetsDMUrgent,countTweetsDMNorm,countTweetsDMFB));
        postUnanswered.add(new ChartDataStck ('Twt Public',countTweetsUrgent,countTweetsNorm,countTweetsFB));
        //postUnanswered.add(new ChartDataStck ('Twt ReTweets',countReTweets));
        postUnanswered.add(new ChartDataStck ('WeChat',countWeChatUrgent,countWeChatNorm,countWeChatFB));
        
        AggregateResult[] groupedResultPM = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE MessageType='Private' and Provider='Facebook' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=TODAY];
        avgWaitTimePMFB = (groupedResultPM[0].get('aver') != null)?((decimal)groupedResultPM[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultPMYest = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE MessageType='Private' and Provider='Facebook' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=YESTERDAY];
        avgWaitTimePMFBYest = (groupedResultPMYest[0].get('aver') != null)?((decimal)groupedResultPMYest[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultPost = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE MessageType IN ('Post','Comment','Reply') and Provider='Facebook' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=TODAY];
        avgWaitTimePostFB = (groupedResultPost[0].get('aver') != null)?((decimal)groupedResultPost[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultPostYest = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE MessageType IN ('Post','Comment','Reply') and Provider='Facebook' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=YESTERDAY];
        avgWaitTimePostFBYest = (groupedResultPostYest[0].get('aver') != null)?((decimal)groupedResultPostYest[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultTweetDM = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE Provider='Twitter' and MessageType='Direct' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=TODAY];
        avgWaitTimeTweetDM = (groupedResultTweetDM[0].get('aver') != null)?((decimal)groupedResultTweetDM[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultTweetDMYest = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE Provider='Twitter' and MessageType='Direct' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=YESTERDAY];
        avgWaitTimeTweetDMYest = (groupedResultTweetDMYest[0].get('aver') != null)?((decimal)groupedResultTweetDMYest[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultTweet = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE Provider='Twitter' and MessageType IN ('Tweet','Reply') and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=TODAY];
        avgWaitTimeTweet = (groupedResultTweet[0].get('aver') != null)?((decimal)groupedResultTweet[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultTweetYest = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE Provider='Twitter' and MessageType IN ('Tweet','Reply') and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=YESTERDAY];
        avgWaitTimeTweetYest = (groupedResultTweetYest[0].get('aver') != null)?((decimal)groupedResultTweetYest[0].get('aver')).intValue():0;
        //AggregateResult[] groupedResultReTweet = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE Provider='Twitter' and MessageType='ReTweet' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and SCS_IsOutbound__c=false and CreatedDate IN (THIS_MONTH,LAST_MONTH)];
        //avgWaitTimeReTweet = ((decimal)groupedResultReTweet[0].get('aver')).intValue();
        AggregateResult[] groupedResultWeChat = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE Provider='WeChat' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and Company__c like '%KLM%' and CreatedDate=TODAY];
        avgWaitTimeWeChat = (groupedResultWeChat[0].get('aver') != null)?((decimal)groupedResultWeChat[0].get('aver')).intValue():0;
        AggregateResult[] groupedResultWeChatYest = [SELECT AVG(Reply_speed__c) aver FROM SocialPost WHERE Provider='WeChat' and (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and Company__c like '%KLM%' and CreatedDate=TODAY];
        avgWaitTimeWeChatYest = (groupedResultWeChatYest[0].get('aver') != null)?((decimal)groupedResultWeChatYest[0].get('aver')).intValue():0;
        
        postAvgWaitTime = new List<ChartDataStck>();
        postAvgWaitTime.add(new ChartDataStck('FB PMs',avgWaitTimePMFBYest,avgWaitTimePMFB,0));
        postAvgWaitTime.add(new ChartDataStck('FB Public',avgWaitTimePostFBYest,avgWaitTimePostFB,0));
        postAvgWaitTime.add(new ChartDataStck('Twt DM',avgWaitTimeTweetDMYest,avgWaitTimeTweetDM,0));
        postAvgWaitTime.add(new ChartDataStck('Twt Public',avgWaitTimeTweetYest,avgWaitTimeTweet,0));
        //postAvgWaitTime.add(new ChartData('Twt ReTweets',avgWaitTimeReTweet));
        postAvgWaitTime.add(new ChartDataStck('WeChat',avgWaitTimeWeChatYest,avgWaitTimeWeChat,0));
        
        AggregateResult[] groupedResultPMLang = [SELECT Language__c,AVG(Reply_speed__c) aver FROM SocialPost WHERE (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and MessageType!='ReTweet' and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=TODAY GROUP BY Language__c LIMIT 50000];
        for (AggregateResult ar : groupedResultPMLang)  {
            if(ar.get('Language__c') == 'English')
               avgWaitTimeEngl = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Dutch')
               avgWaitTimeDutch = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'French')
               avgWaitTimeFrench = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Portuguese')
               avgWaitTimePortuguese = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Japanese')
               avgWaitTimeJpn = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Spanish')
               avgWaitTimeSpn = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'German')
               avgWaitTimeGerman = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Italian')
               avgWaitTimeItalian = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Korean')
               avgWaitTimeKorean = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Chinese')
               avgWaitTimeChinese = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
        }
        
        AggregateResult[] groupedResultPMLangYest = [SELECT Language__c,AVG(Reply_speed__c) aver FROM SocialPost WHERE (SCS_Status__c NOT IN ('Engaged','Mark As Reviewed','Mark As Reviewed Auto','Actioned') or Status = 'Replied') and MessageType!='ReTweet' and Language__c IN ('English','Dutch','French','Portuguese','Japanese','Spanish','German','Italian','Korean','Chinese') and SCS_IsOutbound__c=false and CreatedDate=YESTERDAY GROUP BY Language__c LIMIT 50000];
        for (AggregateResult ar : groupedResultPMLangYest)  {
            if(ar.get('Language__c') == 'English')
               avgWaitTimeEnglYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Dutch')
               avgWaitTimeDutchYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'French')
               avgWaitTimeFrenchYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Portuguese')
               avgWaitTimePortugueseYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Japanese')
               avgWaitTimeJpnYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Spanish')
               avgWaitTimeSpnYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'German')
               avgWaitTimeGermanYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Italian')
               avgWaitTimeItalianYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Korean')
               avgWaitTimeKoreanYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
            if(ar.get('Language__c') == 'Chinese')
               avgWaitTimeChineseYest = (ar.get('aver') != null)?((decimal)ar.get('aver')).intValue():0;
        }
        

        postAvgWaitTimeLang = new List<ChartDataStck>();
        postAvgWaitTimeLang.add(new ChartDataStck('English',avgWaitTimeEnglYest,avgWaitTimeEngl,0));
        postAvgWaitTimeLang.add(new ChartDataStck('Dutch',avgWaitTimeDutchYest,avgWaitTimeDutch,0));
        postAvgWaitTimeLang.add(new ChartDataStck('French',avgWaitTimeFrenchYest,avgWaitTimeFrench,0));
        postAvgWaitTimeLang.add(new ChartDataStck('Portuguese',avgWaitTimePortugueseYest,avgWaitTimePortuguese,0));
        postAvgWaitTimeLang.add(new ChartDataStck('Japanese',avgWaitTimeJpnYest,avgWaitTimeJpn,0));
        postAvgWaitTimeLang.add(new ChartDataStck('Spanish',avgWaitTimeSpnYest,avgWaitTimeSpn,0));
        postAvgWaitTimeLang.add(new ChartDataStck('German',avgWaitTimeGermanYest,avgWaitTimeGerman,0));
        postAvgWaitTimeLang.add(new ChartDataStck('Italian',avgWaitTimeItalianYest,avgWaitTimeItalian,0));
        postAvgWaitTimeLang.add(new ChartDataStck('Korean',avgWaitTimeKoreanYest,avgWaitTimeKorean,0));
        postAvgWaitTimeLang.add(new ChartDataStck('Chinese',avgWaitTimeChineseYest,avgWaitTimeChinese,0));
        
        return null;

    }

   
    // Wrapper class
    public class ChartDataStck {
        public String name { get; set; }
        public Integer data{ get; set; }
        public Integer data2{ get; set; }
        public Integer data3{ get; set; }
        public ChartDataStck(String name, Integer data, Integer data2, Integer data3) {
            this.name = name;
            this.data = data;
            this.data2 = data2;
            this.data3 = data3;
        }
    }   



}