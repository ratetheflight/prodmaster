@isTest
private class SMHStatisticsManagerTest {
    static testMethod void testSMHStatisticsCalculatorCalculate() {
        SMHStatisticsManager.SMHStatisticsCalculator calculator = new SMHStatisticsManager.SMHStatisticsResponseTimesCalculator(2, null);
        calculator.calculate();
        
        SMHStatistics__c record = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c
            from SMHStatistics__c
            order by CreatedDate desc
            limit 1
        ];
        
        System.assertNotEquals(null, record);
        System.assertEquals(2, record.Period_Hours__c);
        System.assert(record.Period_End_Date__c <= DateTime.now());
        System.assert(record.Period_End_Date__c > DateTime.now().addSeconds(-5) );
        System.assertEquals(2 * 60 * 60 * 1000, record.Period_End_Date__c.getTime() - record.Period_Start_Date__c.getTime());
    }
    
    static testMethod void testSMHStatisticsResponseTimesCalculatorCalculate() {
        loadTestData2();
        
        SMHStatisticsManager.SMHStatisticsCalculator calculator = new SMHStatisticsManager.SMHStatisticsResponseTimesCalculator(2, null);
        calculator.calculate();
        
        //DvtH24JUL2014
        SMHStatistics__c record = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c, Type__c, Amount__c,
                   Total_Minutes__c, Top_50_Longest_Minutes__c, Top_60_Longest_Minutes__c,
                   Top_70_Longest_Minutes__c, Top_80_Longest_Minutes__c, Top_90_Longest_Minutes__c,
                   Top_50_Average_Minutes__c,Top_60_Average_Minutes__c,Top_70_Average_Minutes__c,
                   Top_80_Average_Minutes__c,Top_90_Average_Minutes__c
            from SMHStatistics__c
            order by CreatedDate desc
            limit 1
        ];
        
        System.assertEquals('Response', record.Type__c);
        
     //   System.assertEquals(10, record.Amount__c);
        
        
       // system.assertEquals(1 + 11 + 12 + 13 + 14 + 15 + 16 + 17 + 18 + 19, record.Total_Minutes__c);
        
       // System.assertEquals(14, record.Top_50_Longest_Minutes__c);
        //System.assertEquals(15, record.Top_60_Longest_Minutes__c);  
        //System.assertEquals(16, record.Top_70_Longest_Minutes__c);
        //System.assertEquals(17, record.Top_80_Longest_Minutes__c);  
        //System.assertEquals(18, record.Top_90_Longest_Minutes__c);

        //DvtH24JUL2014
        //System.debug('DvtH 60'+record.Top_60_Average_Minutes__c);   
        //System.debug('DvtH 70'+record.Top_70_Average_Minutes__c);
        //System.debug('DvtH 80'+record.Top_80_Average_Minutes__c);   
        //System.debug('DvtH 90'+record.Top_90_Average_Minutes__c);
        
        //System.assertEquals(10.20, record.Top_50_Average_Minutes__c);
        //System.assertEquals(11, record.Top_60_Average_Minutes__c);
        //System.assertEquals(11.714285714285714, record.Top_70_Average_Minutes__c);
        //System.assertEquals(12.375, record.Top_80_Average_Minutes__c);  
        //System.assertEquals(13, record.Top_90_Average_Minutes__c);
    }
    
    static testMethod void testSMHStatisticsResponseTimesCalculatorOnlyFacebook() {
        loadTestData2();
        
        SMHStatisticsManager.SMHStatisticsCalculator calculator = new SMHStatisticsManager.SMHStatisticsResponseTimesCalculator(2, SMHStatisticsManager.FACEBOOK);
        calculator.calculate();
        
        SMHStatistics__c record = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c, Type__c, Amount__c,
                   Total_Minutes__c, Top_50_Longest_Minutes__c, Top_60_Longest_Minutes__c,
                   Top_70_Longest_Minutes__c, Top_80_Longest_Minutes__c, Top_90_Longest_Minutes__c
            from SMHStatistics__c
            order by CreatedDate desc
            limit 1
        ];
        
       // System.assertEquals(2, record.Amount__c);   
    }   
    
    static testMethod void testSMHStatisticsResponseTimesCalculatorOnlyTwitter() {
        loadTestData2();
        
        SMHStatisticsManager.SMHStatisticsCalculator calculator = new SMHStatisticsManager.SMHStatisticsResponseTimesCalculator(2, SMHStatisticsManager.TWITTER);
        calculator.calculate();
        
        SMHStatistics__c record = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c, Type__c, Amount__c,
                   Total_Minutes__c, Top_50_Longest_Minutes__c, Top_60_Longest_Minutes__c,
                   Top_70_Longest_Minutes__c, Top_80_Longest_Minutes__c, Top_90_Longest_Minutes__c
            from SMHStatistics__c
            order by CreatedDate desc
            limit 1
        ];
        
        //System.assertEquals(8, record.Amount__c);   
    }
            
    static testMethod void testSMHStatisticsResolutionTimesCalculatorCalculate() {
        loadTestData2();
        
        SMHStatisticsManager.SMHStatisticsCalculator calculator = new SMHStatisticsManager.SMHStatisticsResolutionTimesCalculator(2, null);
        calculator.calculate();
        
        SMHStatistics__c record = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c, Type__c, Amount__c,
                   Total_Minutes__c, Top_50_Longest_Minutes__c, Top_60_Longest_Minutes__c,
                   Top_70_Longest_Minutes__c, Top_80_Longest_Minutes__c, Top_90_Longest_Minutes__c
            from SMHStatistics__c
            order by CreatedDate desc
            limit 1
        ];
        
        System.assertEquals('Resolution', record.Type__c);      
        
        System.assertEquals(6, record.Amount__c);
        System.assertEquals(0, record.Total_Minutes__c);
        System.assertEquals(0, record.Top_50_Longest_Minutes__c);
        System.assertEquals(0, record.Top_60_Longest_Minutes__c);
        System.assertEquals(0, record.Top_70_Longest_Minutes__c);
        System.assertEquals(0, record.Top_80_Longest_Minutes__c);
        System.assertEquals(0, record.Top_90_Longest_Minutes__c);
    }
    
    static testMethod void testSMHStatisticsResolutionTimesCalculatorOnlyTwitter() {
        loadTestData2();
        
        SMHStatisticsManager.SMHStatisticsCalculator calculator = new SMHStatisticsManager.SMHStatisticsResolutionTimesCalculator(2, SMHStatisticsManager.TWITTER);
        calculator.calculate();
        
        SMHStatistics__c record = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c, Type__c, Amount__c,
                   Total_Minutes__c, Top_50_Longest_Minutes__c, Top_60_Longest_Minutes__c,
                   Top_70_Longest_Minutes__c, Top_80_Longest_Minutes__c, Top_90_Longest_Minutes__c
            from SMHStatistics__c
            order by CreatedDate desc
            limit 1
        ];
        
        System.assertEquals(3, record.Amount__c);   
    }   
    
    static testMethod void testSMHStatisticsResolutionTimesCalculatorOnlyFacebook() {
        loadTestData2();
        
        SMHStatisticsManager.SMHStatisticsCalculator calculator = new SMHStatisticsManager.SMHStatisticsResolutionTimesCalculator(2, SMHStatisticsManager.FACEBOOK);
        calculator.calculate();
        
        SMHStatistics__c record = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c, Type__c, Amount__c,
                   Total_Minutes__c, Top_50_Longest_Minutes__c, Top_60_Longest_Minutes__c,
                   Top_70_Longest_Minutes__c, Top_80_Longest_Minutes__c, Top_90_Longest_Minutes__c
            from SMHStatistics__c
            order by CreatedDate desc
            limit 1
        ];
        
        System.assertEquals(2, record.Amount__c);   
    }               
    
    static testMethod void testExecute() {
        SMHStatisticsManager mgr = new SMHStatisticsManager(1);
        mgr.execute(null);
        
        List<SMHStatistics__c> records = 
        [
            select Period_Hours__c, Period_Start_Date__c, Period_End_Date__c, Type__c, Amount__c,
                   Total_Minutes__c, Top_50_Longest_Minutes__c, Top_60_Longest_Minutes__c,
                   Top_70_Longest_Minutes__c, Top_80_Longest_Minutes__c, Top_90_Longest_Minutes__c
            from SMHStatistics__c
        ];
                
        //System.assertEquals(6, records.size());     
    }
    
    //DvtH24JUL2014
    private static void loadTestData2() {
        List<SocialPost> convsToInsert = new List<SocialPost>();
        
        DateTime now = DateTime.now();
        
        // Conv1 -> Should BE in query result
        SocialPost conv1 = new SocialPost();
        conv1.Name = 'SocialPost1';
        conv1.ExternalPostId = '111111111111';
        conv1.SCS_Status__c = 'Actioned';
        conv1.Provider = 'Twitter';
        conv1.Company__c = 'KLM';
        conv1.Posted = now.addMinutes(-1);
        conv1.Ignore_for_SLA__c = false;            
        conv1.IncludeResponseTime__c = true;
        conv1.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv1);

        
        
        // Conv2 -> Should NOT BE in query result (actioned date too far in the past)
        SocialPost conv2 = new SocialPost();
        conv2.Name = 'SocialPost2';
        conv2.ExternalPostId = '111111111112';
        conv2.SCS_Status__c = 'Actioned';
        conv2.Provider = 'Twitter';
        conv2.Company__c = 'KLM';
        conv2.Ignore_for_SLA__c = false;            
        conv2.IncludeResponseTime__c = true;
        conv2.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv2);   
        
        // Conv3 -> Should NOT BE in query result (actioned date too far in the past)
        SocialPost conv3 = new SocialPost();
        conv3.Name = 'SocialPost3';
        conv3.ExternalPostId = '111111111113';
        conv3.SCS_Status__c = 'Actioned';
        conv3.Provider = 'Twitter';
        conv3.Company__c = 'KLM';
        conv3.Ignore_for_SLA__c = false;            
        conv3.IncludeResponseTime__c = true;
        conv3.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv3);       
        
        // Conv4 -> Should NOT BE in query result (Not state Actioned)
        SocialPost conv4 = new SocialPost();
        conv4.Name = 'SocialPost4';
        conv4.ExternalPostId = '111111111114';
        conv4.SCS_Status__c = 'New';
        conv4.Provider = 'Twitter';
        conv4.Company__c = 'KLM';
        conv4.Replied_date__c = DateTime.now().addHours(-1);
        conv4.Ignore_for_SLA__c = false;            
        conv4.IncludeResponseTime__c = true;
        conv4.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv4);       
        
        // Conv11 -> Should BE in query result
        SocialPost conv11 = new SocialPost();
        conv11.Name = 'SocialPost11';
        conv11.ExternalPostId = '1111111111111';
        conv11.SCS_Status__c = 'Actioned';
        conv11.Provider = 'Twitter';
        conv11.Company__c = 'KLM';
        conv11.Posted = now.addMinutes(-11);
        conv11.Ignore_for_SLA__c = false;           
        conv11.IncludeResponseTime__c = true;
        conv11.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv11);
        
        // Conv12 -> Should BE in query result
        SocialPost conv12 = new SocialPost();
        conv12.Name = 'SocialPost12';
        conv12.ExternalPostId = '1111111111112';
        conv12.SCS_Status__c = 'Actioned';
        conv12.Provider = 'Twitter';
        conv12.Company__c = 'KLM';
        conv12.Posted = now.addMinutes(-12);
        conv12.Ignore_for_SLA__c = false;           
        conv12.IncludeResponseTime__c = true;
        conv12.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv12);  
        
        // Conv13 -> Should BE in query result
        SocialPost conv13 = new SocialPost();
        conv13.Name = 'SocialPost19';
        conv13.ExternalPostId = '1111111111113';
        conv13.SCS_Status__c = 'Actioned';
        conv13.Provider = 'Twitter';
        conv13.Company__c = 'KLM';
        conv13.Posted = now.addMinutes(-13);
        conv13.Ignore_for_SLA__c = false;           
        conv13.IncludeResponseTime__c = true;
        conv13.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv13);  
        
        // Conv14 -> Should BE in query result
        SocialPost conv14 = new SocialPost();
        conv14.Name = 'SocialPost19';
        conv14.ExternalPostId = '1111111111114';
        conv14.SCS_Status__c = 'Actioned';
        conv14.Provider = 'Twitter';
        conv14.Company__c = 'KLM';
        conv14.Posted = now.addMinutes(-14);
        conv14.Ignore_for_SLA__c = false;           
        conv14.IncludeResponseTime__c = true;
        conv14.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv14);
        
        // Conv15 -> Should BE in query result
        SocialPost conv15 = new SocialPost();
        conv15.Name = 'SocialPost19';
        conv15.ExternalPostId = '1111111111115';
        conv15.SCS_Status__c = 'Actioned';
        conv15.Provider = 'Twitter';
        conv15.Company__c = 'KLM';
        conv15.Posted = now.addMinutes(-15);
        conv15.Ignore_for_SLA__c = false;           
        conv15.IncludeResponseTime__c = true;
        conv15.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv15);
        
        // Conv16 -> Should BE in query result
        SocialPost conv16 = new SocialPost();
        conv16.Name = 'SocialPost19';
        conv16.ExternalPostId = '1111111111116';
        conv16.SCS_Status__c = 'Actioned';
        conv16.Provider = 'Twitter';
        conv16.Company__c = 'KLM';
        conv16.Posted = now.addMinutes(-16);
        conv16.Ignore_for_SLA__c = false;           
        conv16.IncludeResponseTime__c = true;
        conv16.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv16);  
        
        // Conv17 -> Should BE in query result
        SocialPost conv17 = new SocialPost();
        conv17.Name = 'SocialPost19';
        conv17.ExternalPostId = '1111111111117';
        conv17.SCS_Status__c = 'Actioned';
        conv17.Provider = 'Twitter';
        conv17.Company__c = 'KLM';
        conv17.Posted = now.addMinutes(-17);
        conv17.Ignore_for_SLA__c = false;           
        conv17.IncludeResponseTime__c = true;
        conv17.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv17);                                                              
        
        // Conv18 -> Should BE in query result
        SocialPost conv18 = new SocialPost();
        conv18.Name = 'SocialPost19';
        conv18.ExternalPostId = '1111111111118';
        conv18.SCS_Status__c = 'Actioned';
        conv18.Provider = 'Facebook';
        conv18.Company__c = 'KLM';
        conv18.Posted = now.addMinutes(-18);
        conv18.Ignore_for_SLA__c = false;           
        conv18.IncludeResponseTime__c = true;
        conv18.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv18);  
        
        // Conv19 -> Should BE in query result
        SocialPost conv19 = new SocialPost();
        conv19.Name = 'SocialPost19';
        conv19.ExternalPostId = '1111111111119';
        conv19.SCS_Status__c = 'Actioned';
        conv19.Provider = 'Facebook';
        conv19.Company__c = 'KLM';
        conv19.Posted = now.addMinutes(-19);
        conv19.Ignore_for_SLA__c = false;           
        conv19.IncludeResponseTime__c = true;
        conv19.Replied_date__c = DateTime.now().addMinutes(-30);
        convsToInsert.add(conv19);          
        
        insert convsToInsert;
        
        SocialPost conv2_2 = 
            [select id from SocialPost where ExternalPostId = :conv2.ExternalPostId limit 1];
        conv2_2.Replied_date__c = DateTime.now().addHours(-3);
        update conv2_2;
        
        SocialPost conv3_2 = 
            [select id from SocialPost where ExternalPostId = :conv3.ExternalPostId limit 1];
        conv3_2.Replied_date__c = DateTime.now().addHours(1);
        update conv3_2; 
        
        List<Case> casesToInsert = new List<Case>();
        
        // Case1 -> Should BE in query result
        Case case1 = new Case();
        //case1.
        case1.status = 'Closed';
        case1.Case_Topic__c = 'Baggage (PT)';
        case1.Case_Detail__c = 'Stolen Baggage';
        case1.Sentiment_at_start_of_case__c = 'Positive';
        case1.Sentiment_at_close_of_case__c = 'Positive';
        case1.Origin = 'Twitter';
        case1.RecordTypeId = SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        casesToInsert.add(case1);

        // Case2 -> Should NOT BE in query result (status not closed)
        Case case2 = new Case();
        case2.status = 'New';
        case2.Sentiment_at_start_of_case__c = 'Positive';
        //case2.Sentiment_at_close_of_case__c = 'Positive';
        case2.Origin = 'Twitter';
        case2.RecordTypeId = SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        casesToInsert.add(case2);
        
        // Case3 -> Should BE in query result
        Case case3 = new Case();
        case3.status = 'Closed';
        case3.Case_Topic__c = 'Baggage (PT)';
        case3.Case_Detail__c = 'Stolen Baggage';
        case3.Sentiment_at_start_of_case__c = 'Positive';
        case3.Sentiment_at_close_of_case__c = 'Positive';
        case3.Origin = 'Twitter';
        case3.RecordTypeId = SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        casesToInsert.add(case3);
        
        // Case4 -> Should BE in query result
        Case case4 = new Case();
        case4.status = 'Closed';
        case4.Case_Topic__c = 'Baggage (PT)';
        case4.Case_Detail__c = 'Stolen Baggage';
        case4.Sentiment_at_start_of_case__c = 'Positive';
        case4.Sentiment_at_close_of_case__c = 'Positive';
        case4.Origin = 'Twitter';
        case4.RecordTypeId = SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        casesToInsert.add(case4);
        
        // Case5 -> Should BE in query result
        Case case5 = new Case();
        case5.status = 'Closed';
        case5.Case_Topic__c = 'Baggage (PT)';
        case5.Case_Detail__c = 'Stolen Baggage';
        case5.Sentiment_at_start_of_case__c = 'Positive';
        case5.Sentiment_at_close_of_case__c = 'Positive';
        case5.Origin = 'Facebook';
        case5.RecordTypeId =SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        casesToInsert.add(case5);
    
        // Case6 -> Should BE in query result
        Case case6 = new Case();
        case6.status = 'Closed';
        case6.Case_Topic__c = 'Baggage (PT)';
        case6.Case_Detail__c = 'Stolen Baggage';
        case6.Sentiment_at_start_of_case__c = 'Positive';
        case6.Sentiment_at_close_of_case__c = 'Positive';
        case6.Origin = 'Facebook';
        case6.RecordTypeId = SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        casesToInsert.add(case6);
        
        // Case7 -> Should BE in query result
        Case case7 = new Case();
        case7.status = 'Closed';
        case7.Case_Topic__c = 'Baggage (PT)';
        case7.Case_Detail__c = 'Stolen Baggage';
        case7.Sentiment_at_start_of_case__c = 'Positive';
        case7.Sentiment_at_close_of_case__c = 'Positive';
        case7.Origin = 'Hyves';
        case7.RecordTypeId = SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        casesToInsert.add(case7);   
        
        // Case8 -> Should NOT BE in query result (wrong record type)
        Case case8 = new Case();
        case8.status = 'Closed';
        case8.Case_Topic__c = 'Baggage (PT)';
        case8.Case_Detail__c = 'Stolen Baggage';
        case8.Sentiment_at_start_of_case__c = 'Positive';
        case8.Sentiment_at_close_of_case__c = 'Positive';
        case8.Origin = 'Hyves'; 
        case8.RecordTypeId = [select Id from RecordType where Name = 'WebShop' and sObjectType='Case' limit 1].Id;
        casesToInsert.add(case8);
        
        // Case9 -> Should NOT BE in query result (wrong sub reason)
        Case case9 = new Case();
        case9.status = 'Closed';
        case9.Case_Topic__c = 'Baggage (PT)';
        case9.Case_Detail__c = 'Stolen Baggage';
        case9.Sentiment_at_start_of_case__c = 'Positive';
        case9.Sentiment_at_close_of_case__c = 'Positive';
        case9.Origin = 'Hyves';
        case9.RecordTypeId = SMHStatisticsManager.SERVICING_RECORD_TYPE_ID;
        case9.Sub_Reason__c = 'Ticket to campaign';
        casesToInsert.add(case9);   
                                
        insert casesToInsert;
    }
    
    static testMethod void testExecute2() {
        
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78956788');
        insert newPerson;
        
        //Insert persona
        List<SocialPersona> personaList=new List<SocialPersona>();
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList;
        
        //Insert for SocialPost
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,5);
        tempSocialPostList[0].Automated_PAX_notification__c='Test';
        tempSocialPostList[1].Is_Campaign__c=True;
        
        List<SocialPost> tempSocialPostList2= new List<SocialPost>();
        tempSocialPostList2=AFKLM_TestDataFactory.insertSocialPost(personaList,newPerson.Id,5);
        tempSocialPostList2[0].MessageType='Direct';
        tempSocialPostList2[0].ReplyToId=null;
              
        tempSocialPostList.addAll(tempSocialPostList2);
        
        insert tempSocialPostList;
        SMHStatisticsManager.SMHStatisticsOutboundPostCountCalculator  smhcalc=new SMHStatisticsManager.SMHStatisticsOutboundPostCountCalculator();           
        smhcalc.getPostsCount();
        
        for(SocialPost sPosts:tempSocialPostList){
            smhcalc.checkIfValid(sPosts);    
        }   
    }

}