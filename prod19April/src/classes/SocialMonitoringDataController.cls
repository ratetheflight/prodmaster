/**********************************************************************
 Name:  SocialMonitoringDataController.cls
=======================================================================
Purpose: Controller for the SocialMonitoringData page
=======================================================================
History                                                            
-------                                                            
VERSION     AUTHOR                DATE            DETAIL                                 
    1.0     Aard-Jan van Kesteren 26/09/2013      Initial development
***********************************************************************/ 
public with sharing class SocialMonitoringDataController {

	public String getJsonData() {		

		Map<String, Map<String, String>> result = new Map<String, Map<String, String>>();
		
		result.put('Response_1_All', getSMHStatisticsRecord('Response', 1, null));
		result.put('Response_1_Facebook', getSMHStatisticsRecord('Response', 1, 'Facebook'));
		result.put('Response_1_Twitter', getSMHStatisticsRecord('Response', 1, 'Twitter'));
		
		result.put('Resolution_1_All', getSMHStatisticsRecord('Resolution', 1, null));
		result.put('Resolution_1_Facebook', getSMHStatisticsRecord('Resolution', 1, 'Facebook'));
		result.put('Resolution_1_Twitter', getSMHStatisticsRecord('Resolution', 1, 'Twitter'));		
		
		result.put('Response_24_All', getSMHStatisticsRecord('Response', 24, null));
		result.put('Response_24_Facebook', getSMHStatisticsRecord('Response', 24, 'Facebook'));
		result.put('Response_24_Twitter', getSMHStatisticsRecord('Response', 24, 'Twitter'));
		
		result.put('Resolution_24_All', getSMHStatisticsRecord('Resolution', 24, null));
		result.put('Resolution_24_Facebook', getSMHStatisticsRecord('Resolution', 24, 'Facebook'));
		result.put('Resolution_24_Twitter', getSMHStatisticsRecord('Resolution', 24, 'Twitter'));		
		
		result.put('Response_168_All', getSMHStatisticsRecord('Response', 168, null));
		result.put('Response_168_Facebook', getSMHStatisticsRecord('Response', 168, 'Facebook'));
		result.put('Response_168_Twitter', getSMHStatisticsRecord('Response', 168, 'Twitter'));
		
		result.put('Resolution_168_All', getSMHStatisticsRecord('Resolution', 168, null));
		result.put('Resolution_168_Facebook', getSMHStatisticsRecord('Resolution', 168, 'Facebook'));
		result.put('Resolution_168_Twitter', getSMHStatisticsRecord('Resolution', 168, 'Twitter'));					
		
		return JSON.serialize(result);
	}
	
	private Map<String, String> getSMHStatisticsRecord(String tpe, Integer periodHours, String origin) {
		
		Map<String, String> jsonRecord = new Map<String, String>();
		
		List<SMHStatistics__c> records = new List<SMHStatistics__c>();
		if (origin == null) {
		 	records = [
				select Calculated_Minutes__c
				,      Average_Minutes__c
				,      Period_Start_Date__c
				,      Period_End_Date__c
				from SMHStatistics__c
				where Type__c = :tpe 
				and   Period_Hours__c = :periodHours
				order by CreatedDate desc
				limit 1 
			];
		} else {
		 	records = [
				select Calculated_Minutes__c
				,      Average_Minutes__c
				,      Period_Start_Date__c
				,      Period_End_Date__c
				from SMHStatistics__c
				where Type__c = :tpe 
				and   Origin__c = :origin
				and   Period_Hours__c = :periodHours
				order by CreatedDate desc
				limit 1 
			];						
		}
		
		if (records.size() > 0) {
			jsonRecord.put('averageMinutes', records[0].Average_Minutes__c + '');
			jsonRecord.put('calculatedMinutes', records[0].Calculated_Minutes__c + '');
			jsonRecord.put('periodStartDate', records[0].Period_Start_Date__c + '');
			jsonRecord.put('periodEndDate', records[0].Period_End_Date__c + '');
			return jsonRecord;
		} else {
			return null;
		}
	}



}