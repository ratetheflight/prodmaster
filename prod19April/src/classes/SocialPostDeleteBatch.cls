/*************************************************************************************************
* File Name     :   SocialPostDeleteBatch 
* Description   :   Batch to delete all the SocialPost records that are marked to be deleted with the Delete Social Post field. 
* @author       :   David van 't Hooft
* Modification Log
===================================================================================================
* Ver.    Date            Author              Modification
*--------------------------------------------------------------------------------------------------
* 1.0     20/07/2015      David van't Hooft   Initial version 
* 1.1     21/11/2016      Nagavi              ST-324 : Included logic to remove the posts from iframe and nexmo post object for private messages of FB

****************************************************************************************************/
global class SocialPostDeleteBatch implements Database.Batchable<SocialPost>,Database.AllowsCallouts,Database.Stateful {
    
    //global string query;
    global Iterable<SocialPost> start(Database.BatchableContext bc){
        return getDataSet();
    }

    global LIST<SocialPost> getDataSet() {
        //To make the query more selective , filter has been added to get records belonging to current month
        List <SocialPost> spList = [Select id, Delete_Social_Post__c,MessageType,Provider,ExternalPostId__c, CreatedDate From SocialPost where Delete_Social_Post__c = true and SystemModStamp = THIS_MONTH order by CreatedDate asc limit 50000];
        return spList;         
    }
       
    global void execute(Database.BatchableContext bc, List<SocialPost> spList) {
        List<SocialPost> postsToBedeleted=new List<SocialPost>();
        List<SocialPost> pmsList=new List<SocialPost>();
        List<string> messageIds=new List<string>();
        List<Nexmo_Post__c> nexmoPostsList=new List<Nexmo_Post__c>();
        
        // Logic to detect the Facebook PM among the incoming social posts - ST-324 by Nagavi
        if(!spList.isEmpty()){
            for(SocialPost sp:spList){
                if(sp.MessageType=='Private' && sp.Provider=='Facebook'){
                    pmsList.add(sp);  
                    messageIds.add(sp.ExternalPostId__c);  
                }
                else
                    postsToBedeleted.add(sp);
            }
        }
        
                        
        if(!pmsList.IsEmpty()){
        
            //Savepoint sp = Database.setSavepoint();
            try{
                // To remove the private message from iframe - ST-324 by Nagavi
                Boolean calloutSuccess=SCS_HerokuOutboundHandler.preparePostsToHeroku(messageIds);
                
                //If the PM is successfully removed from iframe, its corresponding nexmo post and social post will be deleted 
                if(calloutSuccess || Test.isRunningTest()){
                    nexmoPostsList=[Select id,Message_Id__c from Nexmo_Post__c where Message_Id__c IN:messageIds]; 
                    if(!nexmoPostsList.isEmpty()){
                        delete nexmoPostsList;
                    }
                    
                    delete pmsList;    
                }           
            } 
            catch(exception e){
                system.debug('Error has occured'+e.getMessage());
                //Database.rollback(sp);
            }
                  
        }
        
        //To delete the normal posts, comments , tweets etc - ST-324
        if(!postsToBedeleted.isEmpty())
            delete postsToBedeleted;
        
        
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}