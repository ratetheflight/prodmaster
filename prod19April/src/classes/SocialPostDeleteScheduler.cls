/*************************************************************************************************
* File Name     :   SocialPostDeleteScheduler 
* Description   :   Scheduler class for the SocialPostDeletebatch. 
* @author       :   David van 't Hooft
* Modification Log
===================================================================================================
* Ver.    Date            Author              Modification
*--------------------------------------------------------------------------------------------------
* 1.0     20/07/2015      David van't Hooft   Initial version 
* 1.1     21/11/2016      Nagavi              ST-324 : Changed the batch size from 100 to 50

****************************************************************************************************/
global class SocialPostDeleteScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    SocialPostDeleteBatch socialPostDeleteBatchTmp = new SocialPostDeleteBatch();
    Database.executeBatch(socialPostDeleteBatchTmp, 50);
  }
}