/*************************************************************************************************
* File Name     :   SocialPostDeleteTest 
* Description   :   Test class to test the SocialPostDeletebatch
* @author       :   Manuel Conde
* Modification Log
===================================================================================================
* Ver.    Date            Author          Modification
*--------------------------------------------------------------------------------------------------
* 1.0     11/02/2016      Manuel Conde    Initial version 
* 1.1     11/21/2016      Nagavi          Added code coverage for methods related to ST-324

****************************************************************************************************/

@isTest
private class SocialPostDeleteTest {

    static testMethod void socialPostDeleteTestOk() {
        
        //Insert Person Account        
        Account newPerson=AFKLM_TestDataFactory.createPersonAccount('78956788');
        insert newPerson;
        
        //Insert persona
        List<SocialPersona> personaList=new List<SocialPersona>();
        personaList=AFKLM_TestDataFactory.insertFBpersona(1,newPerson.Id,'Facebook');
        insert personaList;
        
        //Insert for SocialPost
        List<SocialPost> tempSocialPostList= new List<SocialPost>();
        tempSocialPostList=AFKLM_TestDataFactory.insertFBSocialPost(personaList,newPerson.Id,5);
        
        List<SocialPost> tempSocialPostList2= new List<SocialPost>();
        tempSocialPostList2=AFKLM_TestDataFactory.insertSocialPost(personaList,newPerson.Id,5);
        
        tempSocialPostList.addAll(tempSocialPostList2);
        
        for(SocialPost sp:tempSocialPostList)
            sp.Delete_Social_Post__c =true;    
        
        insert tempSocialPostList;
        
        List<Nexmo_Post__c> nexmoList=new List<Nexmo_Post__c>();
        nexmoList=AFKLM_TestDataFactory.insertNexmoSocialPost('5678',5);
        insert nexmoList;
        
        List <SocialPost> spList = [Select s.id From SocialPost s limit 20];         
        //system.assertEquals(1, spList.size());
        
        test.starttest();
        SocialPostDeleteScheduler spds = new SocialPostDeleteScheduler();
        spds.execute(null);
        test.stoptest();
        
        spList = [Select s.id From SocialPost s limit 20];         
        system.assertEquals(0, spList.size(), 'SocialPost should be deleted');        
    }
    
    static testMethod void socialPostDeleteTestNotOk() {
        SocialPost sp = new SocialPost(name = 'Test social post to delete', Delete_Social_Post__c = false);
        insert sp;
    
        List <SocialPost> spList = [Select s.id From SocialPost s limit 10];         
        system.assertEquals(1, spList.size());
        
        test.starttest();
        SocialPostDeleteScheduler spds = new SocialPostDeleteScheduler();
        spds.execute(null);
        test.stoptest();
        
        spList = [Select s.id From SocialPost s limit 10];         
        system.assertEquals(1, spList.size(), 'SocialPost should *not* be deleted');        
    }
}