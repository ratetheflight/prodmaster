/*************************************************************************************************
* File Name     :   SocialPostTriggerHandler  
* Description   :   Contains methods called from social post trigger
* @author       :   Sathish
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
---------------------------------------------------------------------------------------------------
* 1.0     09/02/2017      Sathish         Created the class (ST-1548)
* 1.1     22/02/2017      Aruna           Show multiple new posts in the view (ST-1766)
* 1.2     07/03/2017      Aruna           Agent Status - Flying Blue (ST-1878) , Update language in outbound socialposts (ST-1752)
* 1.3     16/03/2017      Nagavi          ST-005338 -- issues in Envelope sign
* 1.1     21/03/2017      Sai Choudhry   Agent Status on flyingblue bug fix. JIRA ST-1996.

****************************************************************************************************/
public class SocialPostTriggerHandler {
    
    
    public void updatecasestatusforflyingblue(list<SocialPost>sprecords)
    {
       try
       {
        set<Id>Caseid=new set<Id>();
        list<case>Caserec=new list<case>();
        for(SocialPost sp:sprecords)
        {
           if(sp.ParentId!=NULL && sp.FlyingBlue__c==TRUE)
           {
               caseid.add(sp.ParentId);
           }
        }
        
        if(caseid.size()>0)
        {
           caserec=[select id,status from Case where Id IN:caseid AND status='Waiting for Client Answer']; 
        }
        if(caserec.size()>0)
        {
           for(Case cs:caserec)
           {
               cs.status='Waiting for Internal answer Flying Blue';
           }
            update caserec;
        }
    }
    catch(Exception e)
    {
        system.debug('Error message '+e.getLineNumber()+'--'+e.getMessage());
    }
  }
    
    public void updatenewpostspercase(Set<Id> caseids){
    
    List<SocialPost> socialPostList = new List<SocialPost>();
    List<Case> toBeCheckedCases = new List<Case>();
    List<Case> toBeUpdatedCases = new List<Case>();
    //Modified the query filter to eliminate the Bug : ST-005338 -- issues in Envelope sign. Added by Nagavi on 20/03/2017
    socialPostList=[select id,parentid from socialpost where parentid IN :caseids and isoutbound = False and SCS_MarkAsReviewed__c=FALSE and (Scs_status__C !='Actioned' and Scs_status__C !='Mark As Reviewed' and Scs_status__C !='Mark As Reviewed Auto' and Scs_status__C !='Engaged')];
    //system.debug('Aruna:socialpostlist'+socialPostList+'--'+socialPostList.size());
    toBeCheckedCases=[select id,Total_New_Posts__c from Case where id IN :caseids ];
    //system.debug('Aruna:tobecheckedcases'+toBeCheckedCases);
    Map<ID,List<Socialpost>> PostMap = new Map<ID,List<Socialpost>>();
    List<socialpost> tempList;    
        for(SocialPost sp:socialPostList)
        {
           if(postMap.containskey(sp.parentid))
           {
               postMap.get(sp.parentid).add(sp);
           }
           else{
            postMap.put(sp.parentid, new List<SocialPost>{sp});
               }
        }
        //system.debug('Aruna:postmap'+postMap);
        //Added null checks to eliminate bugs - Nagavi
          if(!toBeCheckedCases.IsEmpty())
           {
             
             for(Case cs:toBeCheckedCases )
             {
              tempList = new List<socialpost>();
              if(postMap.containsKey(cs.id))
                  tempList=postMap.get(cs.id);
              
              if(!tempList.isempty() && tempList.Size() >= 1)
                {
                 cs.Total_New_Posts__c =tempList.Size();
                 toBeUpdatedCases.add(cs);
                }
              else{
                 cs.Total_New_Posts__c =0;
                 toBeUpdatedCases.add(cs);
              }
              //system.debug('tempList.Size()'+tempList.Size());
              //system.debug('Aruna:tobeupdatedcases'+toBeUpdatedCases);
               }
            }
            
         if(!toBeUpdatedCases.IsEmpty())
          {
           update toBeUpdatedCases;
          }
    }

    public void updatespstatus(Map<Id,Id> parentMap){
        Map <ID,List<Socialpost>> tobeupdatedmap = new Map<ID,List<SocialPost>>();
        List<SocialPost> socialPostList = new List<SocialPost>();
        List<SocialPost> toBeUpdatedList = new List<SocialPost>();
        socialPostList=[select id,parentid,Scs_status__C,MessageType from socialpost where parentid IN :parentMap.keyset() and isoutbound = False and SCS_MarkAsReviewed__c=FALSE and ( Scs_status__C ='New' OR Scs_status__c ='') and FlyingBlue__c=True order by createddate desc ];
        for (SocialPost sp:socialPostList)
        {
            if(!tobeupdatedmap.containskey(sp.parentid))
            {
                tobeupdatedmap.put(sp.parentid,new List<SocialPost>{sp});
            }
            else
            {
                tobeupdatedmap.get(sp.parentid).add(sp);
            }
        }
        //Modified By Sai. JIRA ST-1996.
        //Start
        for (ID csid : tobeupdatedmap.keyset())
        {
            List<SocialPost> tempList = new List<SocialPost>();
            //If(!tobeupdatedmap.get(csid).isEmpty())
            //{
                templist = tobeupdatedmap.get(csid);
                if(templist[0].MessageType == 'Private')
                {
                templist[0].Scs_status__C='Actioned';
                tempList[0].Replied_date__c= DateTime.now();
                }
                for (Socialpost sp : templist)
                {
                    if(sp.Scs_status__C !='Actioned')
                    {
                        sp.Scs_status__C='Mark As Reviewed Auto';
                        sp.SCS_MarkAsReviewed__c = True;
                        sp.SCS_MarkAsReviewedBy__c = '';
                    }
                    sp.Ignore_for_SLA__c = false;
                    sp.ownerid=parentMap.get(csid);
                    toBeUpdatedList.add(sp);
                }
           // }
        }
        //system.debug('To be Updated'+toBeUpdatedList);
        if(!toBeUpdatedList.IsEmpty())
        {
            update toBeUpdatedList;
        }
        //End
    }
    
    //1.2 language on outbound posts
     public void updateoutboundlanguage(Map<Id,List<Socialpost>> outboundMap){
        
        Map<ID,SocialPost> inboundPostMap = new Map<ID,SocialPost>();
        Set<SocialPost> inboundsocialPostSet= new Set<SocialPost>([select id, parentid, Language__c , Created_date_time__c from socialpost where parentid IN :outboundMap.keyset() and isoutbound = False  and Scs_status__C !='New' order by Created_date_time__c desc]);
        for(socialpost obsp : inboundsocialPostSet)
        {
            if(!inboundPostMap.containskey(obsp.parentid))
            {
                inboundPostMap.put(obsp.parentid,obsp);
            }
        }
        if(!inboundPostMap.isempty() && !outboundMap.isempty())
        {   
            List<Socialpost> tempList = new List<SocialPost>();
            for(socialpost insp: inboundPostMap.values()) 
            {
                tempList = outboundMap.get(insp.parentid);
                for(socialpost sp : tempList)
                {
                    sp.Language__c= insp.Language__c;
                }
            }
        }   
    }
    
}