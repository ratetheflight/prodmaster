/**********************************************************************
 Name:  SocialPostTriggerHandler_Test.cls
======================================================
Purpose: Test class for SocialPostTriggerHandler
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR            DATE            DETAIL                                 
    1.0     Sai Choudhry    24/03/2017      Initial development
***********************************************************************/
@IsTest
public class SocialPostTriggerHandler_Test {
    
    Static TestMethod Void updatespstatus()
    {
        List<Case> cs= AFKLM_TestDataFactory.insertKLMCase(1, 'new');
        Account ac=AFKLM_TestDataFactory.createPersonAccount('123456789');
        cs[0].accountid= ac.id;
        cs[0].Is_Flying_Blue__c = True;
        cs[0].status='Waiting for Client Answer';
        insert ac;
        insert cs;
        List<socialpersona> persona=AFKLM_TestDataFactory.insertpersona(1, ac.Id);
        insert persona;
        List<socialpost> inboundPost = AFKLM_TestDataFactory.insertFBSocialPost(persona, ac.Id, 2);
        inboundPost[0].ParentId =cs[0].id;
        inboundPost[1].ParentId =cs[0].id;
        insert inboundPost;
        List<socialpost> outboundPost = AFKLM_TestDataFactory.insertFBSocialPost(persona, ac.Id, 1);
        outboundPost[0].isoutbound=true;
        outboundPost[0].ParentId =cs[0].id;
        outboundPost[0].ownerId=UserInfo.getUserId();
        insert outboundPost;
    }
    
}