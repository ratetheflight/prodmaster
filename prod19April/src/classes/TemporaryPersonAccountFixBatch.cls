/*************************************************************************************************
* File Name     :   TemporaryPersonAccountFixBatch 
* Description   :   Batch to relink al SocialPersona's connected to the TemporaryPerson Account
* @author       :   David van 't Hooft
* Modification Log
===================================================================================================
* Ver.    Date            Author             Modification
*--------------------------------------------------------------------------------------------------
* 1.0     22/09/2015      David van 't Hooft Created the class
* 1.1     05/04/2016      Nagavi             Modified code to include database.querylocator and removed hardcoded values
****************************************************************************************************/
   
global class TemporaryPersonAccountFixBatch implements Database.Batchable<Sobject> {
    
    public String relinkId = '';
    global string query;
    
    global TemporaryPersonAccountFixBatch() {
          
        if (Test.isRunningTest()) {
            //Should have been manually set
        }
        else
           relinkId =Label.TemporaryPersonAccountId; 
                
        system.debug('**'+relinkId);
                   
        this.query='Select s.ParentId, s.Id, (Select Id, CreatedDate From Posts order by CreatedDate limit 1) From SocialPersona s where ParentId =\''+relinkId +'\' '+ ' AND ProcessedBy__c = null Limit 100000';
        system.debug('**query'+query);        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
                 
        return Database.getQueryLocator(query);  
    }
    
    global void execute(Database.BatchableContext bc, List<SocialPersona> scope) {
        system.debug('**** Start **** Social Personas relink to the first Social Post ****');
        List <SocialPersona> toUpdateSpList = new List<SocialPersona>();
        system.debug('**scopesize'+scope.size());
        for (SocialPersona sp : scope) {
            if (sp.Posts != null && !sp.Posts.isEmpty()) {
                sp.ParentId = sp.Posts[0].Id;   
                
            }
            else {
                sp.ProcessedBy__c='TPAFixBatch';
            }   
            toUpdateSpList.add(sp);
                
        }
        
        if (!toUpdateSpList.isEmpty()) {
            update toUpdateSpList;
        }
        system.debug('**** Ended **** Social Personas relinked to the first Social Post ****');
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}