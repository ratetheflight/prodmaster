/**
 * @author (s)      : David van 't Hooft
 * @requirement id  : 
 * @description     : Test class to test the TemporaryPersonAccountFixBatch and TemporaryPersonAccountFixScheduler
 * @log:            : 21JUL2015 v1.0 
 */
@isTest
private class TemporaryPersonAccountFixBatchTest {
    static testMethod void caseDeleteTestOk() {
        Account acc = new Account(LastName='TemporaryPersonAccount');
        insert acc;
        SocialPersona sp = new SocialPersona(name='Test Persona', ParentId = acc.Id, provider = 'Facebook', ProfileURL='http://www.testingurlfortestuser.com/id=12345678', ExternalId='12345678');
        insert sp;
        SocialPost spost = new SocialPost(name='Post from: Test Account', PersonaId=sp.Id , Posted=System.now(), Handle='KLM', Provider='Facebook', Is_campaign__c=false, WhoId=acc.id, MessageType='Comment');
        insert spost;
        //seperate inserts to get some delay!
        SocialPost spost2 = new SocialPost(name='Post2 from: Test Account', PersonaId=sp.Id , Posted=System.now(), Handle='KLM', Provider='Facebook', Is_campaign__c=false, WhoId=acc.id, MessageType='Comment');
        insert spost2;
                
        test.startTest();
        //code coverage test
        TemporaryPersonAccountFixBatch tpaf = new TemporaryPersonAccountFixBatch();
        tpaf.query = 'Select s.ParentId, s.Id, (Select Id, CreatedDate From Posts order by CreatedDate limit 1) From SocialPersona s where ParentId =\''+acc.Id+'\' ';
        Database.executeBatch(tpaf);
        
        TemporaryPersonAccountPostFixBatch tpapostf = new TemporaryPersonAccountPostFixBatch ();
        tpapostf.query = 'Select Id, WhoID From SocialPost where WhoId =\''+acc.Id+'\' ';
        Database.executeBatch(tpapostf);
        
        //tpafs.execute(null);
        TemporaryPersonAccountPostFixScheduler tpapostfs = new TemporaryPersonAccountPostFixScheduler();
        tpapostfs.execute(null);
        
        TemporaryPersonAccountFixScheduler tpafs = new TemporaryPersonAccountFixScheduler();
        tpafs.execute(null);
        
        //real test
        /*TemporaryPersonAccountFixBatch tpafb = new TemporaryPersonAccountFixBatch();
        tpafb.relinkId = acc.Id;
        tpafb.execute(null, tpafb.getDataSet());
        
        TemporaryPersonAccountPostFixBatch tpapfb = new TemporaryPersonAccountPostFixBatch();
        tpapfb.relinkId = acc.Id;
        tpapfb.execute(null, tpapfb.getDataSet());  */      
                
        test.stopTest();
        
        List <SocialPersona> spList = [Select s.ParentId, s.Id from SocialPersona s where Id = :sp.Id];
        
        system.assertEquals(spost.Id, spList[0].parentId, 'Parent should be replaced by the first post');
                
        List <SocialPost> sPostList = [Select s.WhoId, s.Id from SocialPost s where Id = :spost.Id];
        system.assertEquals(null, sPostList[0].WhoId, 'WhoId should be cleared');
                
        sPostList = [Select s.WhoId, s.Id from SocialPost s where Id = :spost2.Id];
        system.assertEquals(null, sPostList[0].WhoId, 'WhoId should be cleared');        
    }    
}