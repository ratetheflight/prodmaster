/**
 * @author (s)    	: David van 't Hooft
 * @requirement id  : 
 * @description   	: Scheduler class for the TemporaryPersonAccountFixBatch
 * @log:            : 22SEP2015 v1.0 
 */
global class TemporaryPersonAccountFixScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    TemporaryPersonAccountFixBatch temporaryPersonAccountFixBatchTmp = new TemporaryPersonAccountFixBatch();
    Database.executeBatch(temporaryPersonAccountFixBatchTmp, 200);
  }
}