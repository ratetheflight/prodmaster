/*************************************************************************************************
* File Name     :   TemporaryPersonAccountPostFixBatch 
* Description   :   Batch to remove the link to the TemporaryPerson Account in all social posts
* @author       :   David van 't Hooft
* Modification Log
===================================================================================================
* Ver.    Date            Author             Modification
*--------------------------------------------------------------------------------------------------
* 1.0     22/09/2015      David van 't Hooft Created the class
* 1.1     05/04/2016      Nagavi             Modified code to include database.querylocator 
******************************************************************************************************/ 
global class TemporaryPersonAccountPostFixBatch implements Database.Batchable<Sobject> {
        
    public String relinkId = '';
    global string query;
    

    global TemporaryPersonAccountPostFixBatch () {
        if (Test.isRunningTest()) {
            //Should have been manually set
        } 
        else
             relinkId =Label.TemporaryPersonAccountId; 
             
                
        system.debug('**'+relinkId);           
        this.query='Select Id, WhoID From SocialPost where WhoId =\''+relinkId +'\' ';
        system.debug('**query'+query);                   
                 
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, LIST<SocialPost> spList) {
        system.debug('**** Start **** Social Post remove link to the Temporary Person Account ****');
        List <SocialPost> toUpdateSpList = new List<SocialPost>();
        
        for (SocialPost sp : spList) {
            sp.whoId = null;    
            toUpdateSpList.add(sp);
        }
        
        if (!toUpdateSpList.isEmpty()) {
            update toUpdateSpList;
        }
        system.debug('**** Ended **** Social Post WhoId has bean cleared ****');
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}