/**
 * @author (s)    	: David van 't Hooft
 * @requirement id  : 
 * @description   	: Scheduler class for the TemporaryPersonAccountFixPostBatch
 * @log:            : 22SEP2015 v1.0 
 */
global class TemporaryPersonAccountPostFixScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    TemporaryPersonAccountPostFixBatch temporaryPersonAccountPostFixBatchTmp = new TemporaryPersonAccountPostFixBatch();
    Database.executeBatch(temporaryPersonAccountPostFixBatchTmp, 200);
  }
}