/* 
*******************************************************************************************************************************************
* File Name     :   TravelDBHttpCalloutMock1 
* Description   :   Class to test TravelDB Services 
* @author       :   TCS
* Modification Log
===================================================================================================
* Ver    Date          Author        Modification
---------------------------------------------------------------------------------------------------
* 1.0    08/02/2017    Ata        Created the class.
******************************************************************************************************************************************* 
*/
@isTest
global class TravelDBHttpCalloutMock1 implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        //LtcTravelDBResponseModel resp = new LtcTravelDBResponseModel();
        res.setBody('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Header><trackingMessageHeader xmlns="http://www.af-klm.com/soa/xsd/MessageHeader-V1_0"><consumerRef><userID>W90000590</userID><partyID>unidentified</partyID><consumerID>unidentified</consumerID><consumerLocation>unidentified</consumerLocation><consumerType>unidentified</consumerType><consumerTime>2017-02-09T11:31:30Z</consumerTime></consumerRef></trackingMessageHeader><MessageID xmlns="http://www.w3.org/2005/08/addressing">uuid:9e91f833-8fe4-47ef-bfbb-603643057fc2</MessageID><RelatesTo RelationshipType="http://www.af-klm.com/soa/tracking/ReplyTo" xmlns="http://www.w3.org/2005/08/addressing">uuid:esb6749eba9-aa33-47f3-9123-915dd082d10d</RelatesTo><RelatesTo RelationshipType="http://www.af-klm.com/soa/tracking/InitiatedBy" xmlns="http://www.w3.org/2005/08/addressing">uuid:esb6749eba9-aa33-47f3-9123-915dd082d10d</RelatesTo></S:Header><S:Body><S:Fault xmlns:ns4="http://www.w3.org/2003/05/soap-envelope"><faultcode>S:Server</faultcode><faultstring>Business error occurred</faultstring><faultactor>URN:AFKL:CAE:QVI:PROV:qvirnapplx10.france.airfrance.fr:traveldb-ws-rct.airfrance.fr</faultactor><detail><ns3:ProvideTravelDetailedInformationFaultBusinessFaultElement xmlns:ns5="http://www.af-klm.com/services/passenger/Identifier-v1/xsd" xmlns:ns4="http://www.af-klm.com/services/passenger/Common-v1/xsd" xmlns:ns2="http://www.af-klm.com/services/common/SystemFault-v1/xsd" xmlns:ns3="http://www.af-klm.com/services/passenger/ProvideTravelDetailedInformationParam-v5/xsd"><ErrorCode>PNR_NOT_FOUND</ErrorCode><FaultDescription>PNR not found in database.</FaultDescription></ns3:ProvideTravelDetailedInformationFaultBusinessFaultElement></detail></S:Fault></S:Body></S:Envelope>');
        res.setStatusCode(404);
        return res;
    }
}