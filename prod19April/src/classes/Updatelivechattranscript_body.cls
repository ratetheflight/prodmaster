/*************************************************************************************************
* File Name     :   Updatelivechattranscript_body
* @author       :   Prasanna
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     11/04/2017      Prasanna      Email body for transcript email

****************************************************************************************************/
public class Updatelivechattranscript_body
{
    @InvocableMethod
    public static void updatebody(List<Id> TranscriptIds)
    {
    List<Livechattranscript> lchat=[select body,Populate_OwnerName__c,Populate_Accountname__c from livechattranscript where id in:TranscriptIds];
  LIst<Livechattranscript> toUpdate=new List<Livechattranscript>();
  for(LiveChatTranscript lchat1: lchat){
String visitorinfo='<span style="color:#8A8288;"><b>'+lchat1.Populate_Accountname__c+':</b></span>'; 
String ownernamereplace='<span style="color:blue;"><b>'+lchat1.Populate_OwnerName__c+':'+'</b></span>';
          String chatbody=lchat1.body;
system.debug('initial body:'+chatbody);
       string owner=lchat1.Populate_OwnerName__c+':';
        String replaced=chatbody.substringafter('( ');
        String replaced1='( '+replaced;
        //string replaced3=replaced1.replace('<br>','<br></span>');
        String replacevisitor=replaced1.replace('Visitor:',visitorinfo);
        string replaceowner=replacevisitor.replace(owner,ownernamereplace);
    string regexp='\\(.*?\\)';
string replaced2=replaceowner.replaceall(regexp,'');
       if(replaced2!=null) {
        lchat1.body_email__c=replaced2;}
        else
        lchat1.body_email__c=lchat1.body;
        toUpdate.add(lchat1);
system.debug('final body:'+replaced2);
   }
   if(toUpdate.size()>0){
   update toUpdate;
   }
  }
}