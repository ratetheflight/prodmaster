/**********************************************************************
 Name:  WannagivesIntegrationManager
======================================================
Purpose: 

Manages all integration for the Wannagives project

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		AJ van Kesteren		06/09/2013		Initial Development
	1.1		Patrick Brinksma	08/11/2013		Added LinkedIn Logic
***********************************************************************/	
public with sharing class WannagivesIntegrationManager {

	private static final Id PERSON_ACCOUNT_RECORD_TYPE_ID = [select Id from RecordType where SobjectType = 'Account' and Name = 'Person Account' limit 1].Id;

	private static final String NS_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/';
	private static final String NS_MAGENTO = 'urn:Magento';

	private static final String NOT_OK_STATUS = 'not_ok';
	private static final String OK_STATUS = 'ok';

	public static final String INTEGRATION_STATUS_PENDING = 'Pending';
	public static final String INTEGRATION_STATUS_SUCCESS = 'Success';
	public static final String INTEGRATION_STATUS_FAULT = 'Fault';

	private static final String SETTINGS_NAME = 'Default';

 	// Handle a list of new Wannagives cases.
	public void handleNewCases(List<Case> cases) {
		List<Account> accountsToInsert = new List<Account>();
		List<Account> accountsToUpdate = new List<Account>();

		// Get all accounts with the same FB number as in the case 
		List<String> fbNumbers = new List<String>();
		for (Case c : cases) {
			if (c.Flying_Blue_Number__c == '0'){
				c.Flying_Blue_Number__c = null;
			}	
			if (c.Flying_Blue_Number__c != null) {
				// Lets trim the fb number in case, to make sure it's nice and tidy.
				c.Flying_Blue_Number__c = c.Flying_Blue_Number__c.trim(); 
				fbNumbers.add(c.Flying_Blue_Number__c);
			}
		}
		Map<String, Account> fbNumberAccounts = new Map<String, Account>();
		System.debug('fbNumbers: ' + fbNumbers);
		for (Account account : [
			select Id, Flying_Blue_Number__c, sf4twitter__Fcbk_User_Id__pc, LinkedIn_User_Id__pc
			from Account
			where Flying_Blue_Number__c in :fbNumbers]) {
			fbNumberAccounts.put(account.Flying_Blue_Number__c, account);
		}
		
		// Get all accounts with the same Facebook User Id as in the case 
		Set<String> facebookUserIds = new Set<String>();
		Set<String> linkedinUserIds = new Set<String>();
		for (Case c : cases) {

			if (c.Requestor_Social_Id__c != null) {
				// Lets trim the user id just in case, to make sure it's nice and tidy.
				c.Requestor_Social_Id__c = c.Requestor_Social_Id__c.trim(); 
				if (c.Requestor_Social_Id__c.left(2).containsIgnoreCase('FB')){
                    c.Requestor_Social_Id__c = c.Requestor_Social_Id__c.left(2).toUpperCase() + c.Requestor_Social_Id__c.substring(2);
					facebookUserIds.add(c.Requestor_Social_Id__c);
				} else if (c.Requestor_Social_Id__c.left(2).containsIgnoreCase('LI')){
					c.Requestor_Social_Id__c = c.Requestor_Social_Id__c.left(2).toUpperCase() + c.Requestor_Social_Id__c.substring(2);
                    linkedinUserIds.add(c.Requestor_Social_Id__c);
				}
			}
		}
		// Retrieve Accounts with FB id and LinkedIn Id
		Map<String, Account> facebookUserIdAccounts = new Map<String, Account>();
		Map<String, Account> linkedinUserIdAccounts = new Map<String, Account>();
		List<Account> listOfAcc = new List<Account>();
		// Use SOSL for performance reasons
        String sFB = '';
        for (String s : facebookUserIds){
            sFB += s + ' OR ';
        }
        String sLI = '';
        for (String s : linkedinUserIds){
            sLI += s + ' OR ';
        }
        if (!facebookUserIds.isEmpty()){
            sFB = sFB.substring(0, sFB.length() - 4);
            List<List<SObject>> searchList = [FIND :sFB IN ALL FIELDS RETURNING Account (Id, sf4twitter__Fcbk_User_Id__pc, LinkedIn_User_Id__pc, Flying_Blue_Number__c)];
			listOfAcc.addAll((List<Account>)searchList[0]);
		}
		if (!linkedinUserIds.isEmpty()){
            sLI = sLI.substring(0, sLI.length() - 4);
			List<List<SObject>> searchList = [FIND :sLI IN ALL FIELDS RETURNING Account (Id, sf4twitter__Fcbk_User_Id__pc, LinkedIn_User_Id__pc, Flying_Blue_Number__c)];
            listOfAcc.addAll((List<Account>)searchList[0]);
		}
				
		for (Account account : listOfAcc) {
			if (account.sf4twitter__Fcbk_User_Id__pc != null && facebookUserIds.contains(account.sf4twitter__Fcbk_User_Id__pc)){
				facebookUserIdAccounts.put(account.sf4twitter__Fcbk_User_Id__pc, account);
			}
			if (account.LinkedIn_User_Id__pc != null && linkedinUserIds.contains(account.LinkedIn_User_Id__pc)){
				linkedinUserIdAccounts.put(account.LinkedIn_User_Id__pc, account);
			}
		}		
		
		for (Case c : cases) {
			// First check if we can find an existing account based on FB number
			Account a = fbNumberAccounts.get(c.Flying_Blue_Number__c);
			
			if (a == null && c.Requestor_Social_Id__c != null) {
				// If not check if we can find an existing account based on Facebook user id
				a = facebookUserIdAccounts.get(c.Requestor_Social_Id__c);
				if (a == null){
					a = linkedinUserIdAccounts.get(c.Requestor_Social_Id__c);	
				}
			}
			
			if (a == null) {
				// Apperently we weren't able to find the account based on fb number, so lets create a new one.
				a = new Account();
				a.RecordTypeId = PERSON_ACCOUNT_RECORD_TYPE_ID;
				a.FirstName = c.First_Name__c;
				a.LastName = c.Last_Name__c;
				a.Flying_Blue_Number__c = c.Flying_Blue_Number__c;
				a.Phone = c.Telephone_Number__c;
				a.PersonEmail = c.Email__c;
				if (c.Requestor_Social_Id__c != null) {
					// Add facebook or linkedin social id to account
					if (c.Requestor_Social_Id__c.left(2) == 'FB'){
						a.sf4twitter__Fcbk_User_Id__pc = c.Requestor_Social_Id__c;
					} else if (c.Requestor_Social_Id__c.left(2) == 'LI'){
						a.LinkedIn_User_Id__pc = c.Requestor_Social_Id__c;
					}				
				}					 
				accountsToInsert.add(a);
			} else {
				// Update account with social id's and FB Number
				if (a.Flying_Blue_Number__c == null){
					a.Flying_Blue_Number__c = c.Flying_Blue_number__c;
				}
				if (a.sf4twitter__Fcbk_User_Id__pc == null && c.Requestor_Social_Id__c!= null && c.Requestor_Social_Id__c.left(2) == 'FB'){
					a.sf4twitter__Fcbk_User_Id__pc = c.Receiver_Social_Id__c;
				}
				if (a.sf4twitter__Fcbk_User_Id__pc == null && c.Requestor_Social_Id__c!= null && c.Requestor_Social_Id__c.left(2) == 'LI'){
					a.LinkedIn_User_Id__pc = c.Receiver_Social_Id__c;
				}
				accountsToUpdate.add(a);
			}
			c.Account = a;
			
			// Update the flight number
			if (c.Flight_Number__c != null) {
				c.Flight_Prefix__c = c.Flight_Number__c.substring(0, 2);
				c.Flight_Number__c = c.Flight_Number__c.substring(2);
			}
		}
		if (!accountsToInsert.isEmpty()){
			insert accountsToInsert;
		}
		if (!accountsToUpdate.isEmpty()){
			update accountsToUpdate;
		}
		
		for (Case c : cases) {
			// Make sure that all AccountId values are set correctly. This is necesarry for just created accounts. 
			c.AccountId = c.Account.Id;
		}
	}

	// Handle a Wannagives case that went to status OK
	@future(callout=true)
	public static void handleOkClosedCase(Id caseId) {
		WannagivesIntegrationManager mgr = new WannagivesIntegrationManager();
		mgr.handleClosedCase(caseId, OK_STATUS);
	}
	
	// Handle a Wannagives case that went to status NOT OK
	@future(callout=true)
	public static void handleNotOkClosedCase(Id caseId) {
		WannagivesIntegrationManager mgr = new WannagivesIntegrationManager();
		mgr.handleClosedCase(caseId, NOT_OK_STATUS);
	}	
	
	// Handle a Wannagives case that has been closed
	public void handleClosedCase(Id caseId, String status) {

		Case c = [
			select  Id, Wannagives_Order_Number__c, Flight_Prefix__c, Flight_Number__c, 
				Flight_Suffix__c, PNR__c, Flight_Destination__c, Last_Name__c, 
				First_Name__c, Wannagives_Not_Ok_Reason__c,
				Receiver_First_Name__c, Reciever_Last_Name__c
			from Case
			where Id = :caseId
			limit 1
		];	
		
		String log = '';
		try {
			
			String loginMessageBody = createLoginMessageBody();
			log += 'Login Message Body: \n';
			log += loginMessageBody + '\n\n';
			
			Dom.Document loginResponse = callWS(loginMessageBody);
			log += 'Login Response: \n';
			log += loginResponse.toXmlString() + '\n\n';
			
			String sessionId = getSessionIdFromLoginResponse(loginResponse);
			log += 'Session id: ' + sessionId + '\n\n';
			
			String confirmOrderMessageBody = createSalesOrderConfirmOrderMessageBody(sessionId, status, c);
			log += 'Sales Order Confirm Order Message Body:\n';
			log += confirmOrderMessageBody + '\n\n';
			
			Dom.Document confirmOrderResponse = callWS(confirmOrderMessageBody);
			log += 'Sales Order Confirm Order Response:\n';
			log += confirmOrderResponse.toXmlString() + '\n\n';
			
			Boolean isSuccess = getIsSuccessFromConfirmOrderResponse(confirmOrderResponse, c);
			log += 'Is Success: ' + isSuccess + '\n\n';
			
			c.Wannagives_Integration_Status__c = isSuccess ? INTEGRATION_STATUS_SUCCESS : INTEGRATION_STATUS_FAULT;
		} catch(Exception e) {
			c.Wannagives_Integration_Status__c = INTEGRATION_STATUS_FAULT;
			c.Wannagives_Integration_Faultcode__c = '';
			c.Wannagives_Integration_Faultstring__c = e.getMessage();
			log += 'An exception occured:\n';
			log += e.getMessage() + '\n';
			log += e.getStackTraceString();
		} finally {
			c.Wannagives_Integration_Log__c = log;
			update c;
		}
	}

	public Dom.Document callWS(String body){
		Http http = new Http();
		Httprequest request = new HttpRequest();
		request.setBody(body);
		request.setEndpoint(Wannagives_Integration_Settings__c.getInstance(SETTINGS_NAME).Endpoint__c);
		request.setMethod('GET');
		request.setHeader('Content-Type', 'text/xml; charset=UTF-8' );
		request.setTimeout(60000);		
		request.setHeader('Accept', 'text/xml');
		request.setHeader('Connection','keep-alive');
		Httpresponse response = http.send(request);
		return response.getBodyDocument();
	}
	
	public String getSessionIdFromLoginResponse(Dom.Document response) {
			Dom.XmlNode root = response.getRootElement();
			Dom.XmlNode body = root.getChildElement('Body', NS_SOAP);
			Dom.XmlNode loginResponse = body.getChildElement('loginResponse', NS_MAGENTO);
			Dom.XmlNode loginReturn = loginResponse.getChildElement('loginReturn', null);
			return loginReturn.getText();
	} 

	public String createLoginMessageBody() {
		Dom.Document doc = new Dom.Document();
		doc.createRootElement('Envelope', NS_SOAP, null);
		Dom.Xmlnode root = doc.getRootElement();
		Dom.XmlNode bodyNode = root.addChildElement('Body', NS_SOAP, null);
		Dom.XmlNode loginNode = bodyNode.addChildElement('login', NS_MAGENTO, null);
		loginNode.addChildElement('username', null, null).addTextNode(Wannagives_Integration_Settings__c.getInstance(SETTINGS_NAME).Username__c);
		loginNode.addChildElement('apiKey', null, null).addTextNode(Wannagives_Integration_Settings__c.getInstance(SETTINGS_NAME).API_Key__c);
		return doc.toXmlString();		
	}

	public String createSalesOrderConfirmOrderMessageBody(String sessionId, String status, Case c) {
		Dom.Document doc = new Dom.Document();
		doc.createRootElement('Envelope', NS_SOAP, null);
		Dom.Xmlnode root = doc.getRootElement();
		Dom.XmlNode headerNode = root.addChildElement('Header', NS_SOAP, 's');
		Dom.XmlNode bodyNode = root.addChildElement('Body', NS_SOAP, 's');
		
		Dom.XmlNode confirmOrderNode = bodyNode.addChildElement('salesOrderConfirmOrder', NS_MAGENTO, null);
		
		confirmOrderNode.addChildElement('sessionId', null, null).addTextNode(sessionId);
		confirmOrderNode.addChildElement('orderIncrementId', null, null).addTextNode(emptyStringIfNull(c.Wannagives_Order_Number__c));
		confirmOrderNode.addChildElement('flightPrefix', null, null).addTextNode(emptyStringIfNull(c.Flight_Prefix__c));
		confirmOrderNode.addChildElement('flightNumber', null, null).addTextNode(emptyStringIfNull(c.Flight_Number__c));
		confirmOrderNode.addChildElement('flightSuffix', null, null).addTextNode(emptyStringIfNull(c.Flight_Suffix__c));
		confirmOrderNode.addChildElement('pnr', null, null).addTextNode(emptyStringIfNull(c.PNR__c));
		confirmOrderNode.addChildElement('flightDestination', null, null).addTextNode(emptyStringIfNull(c.Flight_Destination__c));
		confirmOrderNode.addChildElement('lastName', null, null).addTextNode(emptyStringIfNull(c.Reciever_Last_Name__c));
		confirmOrderNode.addChildElement('firstName', null, null).addTextNode(emptyStringIfNull(c.Receiver_First_Name__c));
		confirmOrderNode.addChildElement('status', null, null).addTextNode(emptyStringIfNull(status));
		if (status == NOT_OK_STATUS) {
			confirmOrderNode.addChildElement('message', null, null).addTextNode(emptyStringIfNull(c.Wannagives_Not_Ok_Reason__c));
		}
		return doc.toXmlString();			
	}
	
	public Boolean getIsSuccessFromConfirmOrderResponse(Dom.Document confirmOrderResponse, Case c) {
			Dom.XmlNode root = confirmOrderResponse.getRootElement();
			Dom.XmlNode body = root.getChildElement('Body', NS_SOAP);
			Dom.XmlNode fault = body.getChildElement('Fault', NS_SOAP);
			if (fault != null) {
				// A fault tag found, so return false;
				c.Wannagives_Integration_Faultcode__c = fault.getChildElement('faultcode', null).getText();
				c.Wannagives_Integration_Faultstring__c = fault.getChildElement('faultstring', null).getText();
				return false;
			} else {
				return true;
			}
	}
	
	private String emptyStringIfNull(string s) {
		return s == null ? '' : s;
	}

}