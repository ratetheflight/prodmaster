//Copyright (c) 2010, Mark Sivill, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// History
//
// Version      Date            Author          Comments
// 1.0.0        26-04-2010      Mark Sivill     Initial version
//
// Overview
//
// Process incoming Yahoo! Weather feed 
//
public with sharing class WeatherChatter {

    //
    // get rss feed for weather
    //   woeid - used by Yahoo to set weather location
    //   temperatureScale - c or f
    //
    public static Dom.Document getFeed(Integer woeid, String temperatureScale  )
    {
        HttpResponse res = null;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        Dom.Document doc;
                
        // Yahoo! Waether URL endpoint, need to ensure it is in Remote Site Settings        
        req.setEndpoint('http://weather.yahooapis.com/forecastrss?w=' + woeid + '&u=' + temperatureScale);
        req.setMethod('GET');
        if (!Test.isRunningTest()) {  //DvtH28Oct2015 Added for the change in behaviour on call-outs in the winter 16 release               
            res = h.send(req);
        }
        
        //System.Debug('YahooWeather.getFeed return status' + res.getStatus());
        //System.Debug('YahooWeather.getFeed body' + res.getBody())

        try
        {
            doc = res.getBodyDocument();
        }
        catch (Exception e)
        {
            doc = new Dom.Document(); //DvtH28Oct2015 Missing code. Added for the change in behaviour on call-outs in the winter 16 release               
            doc.load('<error>Error parsing please contact your administrator</error>');         
        }
                
        return doc;
    }
    
    //
    // get summary from
    //
    public static String getWeatherSummary(Dom.Document doc)
    {
        String weatherSummary = '';
        
        try
        {
            
            Dom.XMLNode channel = doc.getRootElement().getChildElement('channel', null);

            Dom.XMLNode units = channel.getChildElement('units', 'http://xml.weather.yahoo.com/ns/rss/1.0');
            Dom.XMLNode astronomy = channel.getChildElement('astronomy', 'http://xml.weather.yahoo.com/ns/rss/1.0');
            Dom.XMLNode item = channel.getChildElement('item', null);

            Dom.XMLNode condition = item.getChildElement('condition', 'http://xml.weather.yahoo.com/ns/rss/1.0');

            String temperatureScale = units.getAttributeValue('temperature',null);
            Temperature temp = new Temperature( Integer.valueOf(condition.getAttributeValue('temp',null)),temperatureScale);
            Temperature tempConverted = Temperature.convert(temp);
                
            weatherSummary = item.getChildElement('title', null).getText()
                + ', '
                + condition.getAttributeValue('text',null)
                + ', '
                + temp.temperature
                + temp.scale
                + ' ('
                + tempConverted.temperature
                + tempConverted.scale
                + ')'
                + '\nSunrise: '
                + astronomy.getAttributeValue('sunrise',null)           
                + ' Sunset: '
                + astronomy.getAttributeValue('sunset',null)            
                + '\n';

            // get child elements for forecasts
            for(Dom.XMLNode child : item.getChildElements())
            {
        
                Temperature high, low, highConverted, lowConverted;
            
                if (child.getName() == 'forecast')
                {

                    high = new Temperature( Integer.valueOf(child.getAttributeValue('high',null)),temperatureScale);
                    highConverted = Temperature.convert(high);
                    low = new Temperature( Integer.valueOf(child.getAttributeValue('low',null)),temperatureScale);
                    lowConverted = Temperature.convert(low);

                    weatherSummary = weatherSummary + child.getAttributeValue('day',null)
                        + ': '
                        + child.getAttributeValue('text',null)
                        + ' - High: '
                        +  high.temperature + high.scale + ' (' + highConverted.temperature + highConverted.scale + ')'
                        + ' - Low: '
                        +  low.temperature + low.scale + ' (' + lowConverted.temperature + lowConverted.scale + ')'
                        + '\n';             
                }

            }       
            
        }
        catch (NullPointerException npe)
        {
            weatherSummary = 'Unable to find data in feed please contact your administrator and/or check "Where on Earth Identifier" for record- ' + npe.getMessage();          
        }

        return weatherSummary;
    }

    //
    // get url link back to Yahoo! Weather
    //
    public static String getURL(Dom.Document doc)
    {
        String result;
        try
        {
            result = doc.getRootElement().getChildElement('channel', null).getChildElement('link', null).getText();
        }
        catch (NullPointerException npe)
        {
            result = 'http://salesforce.com'; 
        }
        return result; 
    }

    //
    // get title for Yahoo! Weather
    //
    public static String getTitle(Dom.Document doc)
    {
        String result;
        try
        {
            result = doc.getRootElement().getChildElement('channel', null).getChildElement('title', null).getText();
        }
        catch (NullPointerException npe)
        {
            result = 'Unable to find data please check Yahoo! Weather WOEID'; 
        }
        return result; 
    }

}