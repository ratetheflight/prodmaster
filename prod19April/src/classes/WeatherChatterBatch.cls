//Copyright (c) 2010, Mark Sivill, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// History
//
// Version      Date            Author          Comments
// 1.0.0        26-04-2010      Mark Sivill     Initial version
//
// Overview
//
// Create chatter feeds in batch for all Weather Chatter locations (default is create feeds where Chatter Active is checked) 
//
global with sharing class WeatherChatterBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    global final String queryString;
    
    // process all records where Chatter Active is checked
    global WeatherChatterBatch()
    {
    	queryString = 'SELECT Id, Where_on_Earth_Identifier__c, Temperature_Scale__c FROM Weather_Chatter__c WHERE Chatter_Active__c=true';
    }
    
    global WeatherChatterBatch(String s)
    {
    	queryString = s;
    }
    
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(queryString);
	}
    
	global void finish(Database.BatchableContext BC){}

	global void execute(Database.BatchableContext BC, List<sObject> scope){

		List<FeedPost> feedposts = new List<FeedPost>();
		for(SObject s : scope) {

			// create chatter feed			
			FeedPost feedpost = new FeedPost();
			feedpost.parentid = s.id;

			// get Yahoo! Weather then process it
			Dom.Document doc = WeatherChatter.getFeed( Integer.valueOf((String)s.get('Where_on_Earth_Identifier__c')), (String)s.get('Temperature_Scale__c'));

			// add further information						
			feedpost.body = WeatherChatter.getWeatherSummary(doc);
			feedpost.linkUrl = WeatherChatter.getURL(doc);
	        feedpost.title = WeatherChatter.getTitle(doc);
			
			feedposts.add(feedpost);
		}

		// create chatter feeds
		insert feedposts;

    }
}