//Copyright (c) 2010, Mark Sivill, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// History
//
// Version      Date            Author          Comments
// 1.0.0        26-04-2010      Mark Sivill     Initial version
//
// Overview
//
// Manually allow chatter feeds to be created from the record via a button 
//
public with sharing class WeatherChatterExtension {

	private final Weather_Chatter__c theRecord;

    public WeatherChatterExtension (ApexPages.StandardController stdController)
    {
        theRecord = (Weather_Chatter__c) stdController.getRecord();
    }
    
    //
    // create chatter feed is record is markered as active
    //
    public PageReference chatterWeather()
	{
		PageReference pageref;

		// only create feed is record has been markered "chatter is active"
		if (theRecord.Chatter_Active__c == true)
		{
			// create chatter feed			
			FeedPost feedpost = new FeedPost();
			feedpost.parentid = theRecord.id;

			// get Yahoo! Weather then process it
			Dom.Document doc = WeatherChatter.getFeed( Integer.valueOf(theRecord.Where_on_Earth_Identifier__c), theRecord.Temperature_Scale__c);

			// add further details						
			feedpost.body = WeatherChatter.getWeatherSummary(doc);
			feedpost.linkUrl = WeatherChatter.getURL(doc);
	        feedpost.title = WeatherChatter.getTitle(doc);
	        
			insert feedpost;
			pageRef = new PageReference ('/' + theRecord.Id);
		}
		else
		{
			// do nothing
			pageRef = null;
		}
		
		return pageRef;
	}

}