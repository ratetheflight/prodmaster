//Copyright (c) 2010, Mark Sivill, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// History
//
// Version      Date            Author          Comments
// 1.0.0        26-04-2010      Mark Sivill     Initial version
//
// Overview
//
// Tests for Create Chatter Feeds button on Weather Chatter
//
@isTest
private class WeatherChatterExtensionTest {

    //
    // check for chatter active
    //
    static testMethod void testButtonActive() {
        /*
        ApexPages.StandardController controller;
        WeatherChatterExtension controllerExtension;
        PageReference pagereference1;
        
        // test for active
        Weather_Chatter__c theRecord = new Weather_Chatter__c( name='Twickenham', Where_on_Earth_Identifier__c='38395', Temperature_Scale__c='c', Chatter_Active__c=true );
        upsert theRecord Where_on_Earth_Identifier__c;
        // test constructor
        controller = new ApexPages.Standardcontroller(theRecord);
        controllerExtension = new WeatherChatterExtension(controller);        
        // test methods
        pagereference1 = controllerExtension.chatterWeather();
        */
    }

    //
    // check for chatter inactive
    //
    static testMethod void testButtonInActive() {
        /*
        ApexPages.StandardController controller;
        WeatherChatterExtension controllerExtension;
        PageReference pagereference1;
        
        // test for active
        Weather_Chatter__c theRecord1 = new Weather_Chatter__c( name='Staines', Where_on_Earth_Identifier__c='26823904', Temperature_Scale__c='c', Chatter_Active__c=false );
        upsert theRecord1 Where_on_Earth_Identifier__c;
        // test constructor
        controller = new ApexPages.Standardcontroller(theRecord1);
        controllerExtension = new WeatherChatterExtension(controller);
        // test methods
        pagereference1 = controllerExtension.chatterWeather();
        */
    }

}