//Copyright (c) 2010, Mark Sivill, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// History
//
// Version      Date            Author          Comments
// 1.0.0        26-04-2010      Mark Sivill     Initial version
//
// Overview
//
// Setup page for Salesforce Weather Chatter using Yahoo! Weather 
//
public with sharing class WeatherChatterSetupController {

	// determine if seed data has been run
	public Boolean dataCreated {get; private set;}

	// determine if chatter feeds have been batched
	public Boolean batchCreated {get; private set;}
	
	public WeatherChatterSetupController() {
		dataCreated = false;
		batchCreated = false;
	}
	
    //
    // seed Salesforce Weather Chatter with some data
    // to allow it to be demoed quickly
    //
    // NOTE seed data should not exceed 499 records in order to all test classes to run correctly
    //
    //
    public pageReference seedData() {
        
        // woeid information found on http://sigizmund.info/woeidinfo/
        List<Weather_Chatter__c> yahooWeather = new List<Weather_Chatter__c>
            { 
                new Weather_Chatter__c( Name='Auckland', Where_on_Earth_Identifier__c='2348079', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+12'),
                new Weather_Chatter__c( Name='Suva', Where_on_Earth_Identifier__c='1064103', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+12'),
                new Weather_Chatter__c( Name='Brisbane', Where_on_Earth_Identifier__c='1100661', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+10'),
                new Weather_Chatter__c( Name='Sydney', Where_on_Earth_Identifier__c='1105779', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+10'),
                new Weather_Chatter__c( Name='Adelaide', Where_on_Earth_Identifier__c='1099805', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+09:30'),
                new Weather_Chatter__c( Name='Darwin', Where_on_Earth_Identifier__c='1101597', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+09:30'),
                new Weather_Chatter__c( Name='Seoul', Where_on_Earth_Identifier__c='1132599', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+09'),
                new Weather_Chatter__c( Name='Tokyo', Where_on_Earth_Identifier__c='1118370', Temperature_Scale__c='c', Chatter_Active__c=true, timezone__c='+09'),
                new Weather_Chatter__c( Name='Kuala Lumpur', Where_on_Earth_Identifier__c='1154781', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Manila', Where_on_Earth_Identifier__c='1199477', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Shanghai', Where_on_Earth_Identifier__c='2151849', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Singapore', Where_on_Earth_Identifier__c='1062617', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Taipei City', Where_on_Earth_Identifier__c='2306179', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Perth', Where_on_Earth_Identifier__c='1098081', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Beijing', Where_on_Earth_Identifier__c='2151330', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Hong Kong', Where_on_Earth_Identifier__c='2165352', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+08'),
                new Weather_Chatter__c( Name='Bangkok', Where_on_Earth_Identifier__c='1225448', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+07'),
                new Weather_Chatter__c( Name='Jakarta', Where_on_Earth_Identifier__c='1047378', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+07'),
                new Weather_Chatter__c( Name='Ho Chi Minh City', Where_on_Earth_Identifier__c='1252431', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+07'),
                new Weather_Chatter__c( Name='Yangon', Where_on_Earth_Identifier__c='1015662', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+06:30'),
                new Weather_Chatter__c( Name='Dhaka', Where_on_Earth_Identifier__c='1915035', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+06'),
                new Weather_Chatter__c( Name='Katmandu', Where_on_Earth_Identifier__c='2269179', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+05:45'),
                new Weather_Chatter__c( Name='Kolkata', Where_on_Earth_Identifier__c='2295386', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+05:30'),
                new Weather_Chatter__c( Name='Colombo', Where_on_Earth_Identifier__c='2189783', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+05:30'),
                new Weather_Chatter__c( Name='Bangalore', Where_on_Earth_Identifier__c='2295420', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+05:30'),
                new Weather_Chatter__c( Name='Gurgaon', Where_on_Earth_Identifier__c='2295020', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+05:30'),
                new Weather_Chatter__c( Name='Karachi', Where_on_Earth_Identifier__c='2211096', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+05'),
                new Weather_Chatter__c( Name='Tashkent', Where_on_Earth_Identifier__c='2272113', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+05'),
                new Weather_Chatter__c( Name='Kabul', Where_on_Earth_Identifier__c='1922738', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+04:30'),
                new Weather_Chatter__c( Name='Dubai', Where_on_Earth_Identifier__c='1940345', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+04'),
                new Weather_Chatter__c( Name='Tbilisi', Where_on_Earth_Identifier__c='1965878', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+04'),
                new Weather_Chatter__c( Name='Tehran', Where_on_Earth_Identifier__c='2251945', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+03:30'),
                new Weather_Chatter__c( Name='Nairobi', Where_on_Earth_Identifier__c='1528488', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+03'),
                new Weather_Chatter__c( Name='Baghdad', Where_on_Earth_Identifier__c='1979455', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+03'),
                new Weather_Chatter__c( Name='Kuwait City', Where_on_Earth_Identifier__c='1940631', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+03'),
                new Weather_Chatter__c( Name='Riyadh', Where_on_Earth_Identifier__c='1939753', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+03'),
                new Weather_Chatter__c( Name='Moscow', Where_on_Earth_Identifier__c='2122265', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+03'),
                new Weather_Chatter__c( Name='Cairo', Where_on_Earth_Identifier__c='1521894', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Johannesburg', Where_on_Earth_Identifier__c='1582504', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Jerusalem', Where_on_Earth_Identifier__c='1968222', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Athens', Where_on_Earth_Identifier__c='946738', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Bucharest', Where_on_Earth_Identifier__c='868274', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Helsinki', Where_on_Earth_Identifier__c='565346', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Istanbul', Where_on_Earth_Identifier__c='2344116', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Minsk', Where_on_Earth_Identifier__c='834463', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Espoo', Where_on_Earth_Identifier__c='564617', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+02'),
                new Weather_Chatter__c( Name='Amsterdam', Where_on_Earth_Identifier__c='727232', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Berlin', Where_on_Earth_Identifier__c='638242', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Bruxelles', Where_on_Earth_Identifier__c='968019', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Paris', Where_on_Earth_Identifier__c='615702', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Prague', Where_on_Earth_Identifier__c='796597', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Rome', Where_on_Earth_Identifier__c='721943', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Morges', Where_on_Earth_Identifier__c='783404', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='München', Where_on_Earth_Identifier__c='676757', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Milano', Where_on_Earth_Identifier__c='718345', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Stockholm', Where_on_Earth_Identifier__c='906057', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Diegem', Where_on_Earth_Identifier__c='968950', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Utrecht', Where_on_Earth_Identifier__c='734047', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Hellerup', Where_on_Earth_Identifier__c='553971', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Madrid', Where_on_Earth_Identifier__c='766273', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='+01'),
                new Weather_Chatter__c( Name='Dublin', Where_on_Earth_Identifier__c='560743', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='0'),
                new Weather_Chatter__c( Name='Lisbon', Where_on_Earth_Identifier__c='742676', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='0'),
                new Weather_Chatter__c( Name='London', Where_on_Earth_Identifier__c='44418', Temperature_Scale__c='c', Chatter_Active__c=true, timezone__c='0'),
                new Weather_Chatter__c( Name='Twickenham', Where_on_Earth_Identifier__c='38395', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='0'),
                new Weather_Chatter__c( Name='Staines', Where_on_Earth_Identifier__c='26823904', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='0'),
                new Weather_Chatter__c( Name='Buenos Aires', Where_on_Earth_Identifier__c='468739', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-03'),
                new Weather_Chatter__c( Name='Sao Paulo', Where_on_Earth_Identifier__c='455827', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-03'),
                new Weather_Chatter__c( Name='Santiago', Where_on_Earth_Identifier__c='349859', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-04'),
                new Weather_Chatter__c( Name='Hamilton', Where_on_Earth_Identifier__c='56793', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-04'),
                new Weather_Chatter__c( Name='Caracas', Where_on_Earth_Identifier__c='395269', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-04:30'),
                new Weather_Chatter__c( Name='Bogotá', Where_on_Earth_Identifier__c='368148', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-05'),
                new Weather_Chatter__c( Name='Toronto', Where_on_Earth_Identifier__c='4118', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-05'),
                new Weather_Chatter__c( Name='Indianapolis', Where_on_Earth_Identifier__c='2427032', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-05'),
                new Weather_Chatter__c( Name='Lima', Where_on_Earth_Identifier__c='418440', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-05'),
                new Weather_Chatter__c( Name='New York', Where_on_Earth_Identifier__c='2459115', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-05'),
                new Weather_Chatter__c( Name='Panama City', Where_on_Earth_Identifier__c='159386', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-05'),
                new Weather_Chatter__c( Name='Chicago', Where_on_Earth_Identifier__c='2379574', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-06'),
                new Weather_Chatter__c( Name='San Salvador', Where_on_Earth_Identifier__c='79825', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-06'),
                new Weather_Chatter__c( Name='Mexico City', Where_on_Earth_Identifier__c='116545', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-06'),
                new Weather_Chatter__c( Name='Denver', Where_on_Earth_Identifier__c='2391279', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-07'),
                new Weather_Chatter__c( Name='Phoenix', Where_on_Earth_Identifier__c='2471390', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-07'),
                new Weather_Chatter__c( Name='Los Angeles', Where_on_Earth_Identifier__c='2442047', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-08'),
                new Weather_Chatter__c( Name='Tijuana', Where_on_Earth_Identifier__c='149361', Temperature_Scale__c='c', Chatter_Active__c=false, timezone__c='-08'),
                new Weather_Chatter__c( Name='San Francisco', Where_on_Earth_Identifier__c='2487956', Temperature_Scale__c='f', Chatter_Active__c=true, timezone__c='-08'),
                new Weather_Chatter__c( Name='San Mateo', Where_on_Earth_Identifier__c='2488142', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-08'),
                new Weather_Chatter__c( Name='Santa Monica', Where_on_Earth_Identifier__c='2488892', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-08'),
                new Weather_Chatter__c( Name='Anchorage', Where_on_Earth_Identifier__c='2354490', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-09'),
                new Weather_Chatter__c( Name='Honolulu', Where_on_Earth_Identifier__c='2423945', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-10'),
                new Weather_Chatter__c( Name='Pago Pago', Where_on_Earth_Identifier__c='1062841', Temperature_Scale__c='f', Chatter_Active__c=false, timezone__c='-11')
            };
        upsert yahooWeather Where_on_Earth_Identifier__c;       
		dataCreated = true;
        return null;
//		return Page.WeatherChatterSetup;
    }
    
    //
    // manually start batch process
    //
    public pageReference runAllActive() {

        WeatherChatterBatch b = new WeatherChatterBatch();
        // need to have batch size of one to avoid governor limits for HTTP callouts
        database.executebatch(b,1);
		batchCreated = true;
		return null;
//		return Page.WeatherChatterSetup;

    }
}