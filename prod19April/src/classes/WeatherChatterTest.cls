//Copyright (c) 2010, Mark Sivill, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// History
//
// Version      Date            Author          Comments
// 1.0.0        26-04-2010      Mark Sivill     Initial version
//
// Overview
//
// Test Weather Chatter feed 
//
@isTest
private class WeatherChatterTest {

	//
	// test feed
	//
    static testMethod void getFeedTest()
    {

		// test for London
		Dom.Document doc = WeatherChatter.getFeed(44418, 'c');		
System.Debug('WeatherChatterTest.getFeedTest document - ' + doc);
		
    }
 
 	//
	// test summary processing
	//
    static testMethod void getSummaryTest()
    {

		Dom.Document doc;
		String xml, summary;
		
		doc = new Dom.Document();
		xml = '<rss version="2.0" xmlns:yweather="http://xml.weather.yahoo.com/ns/rss/1.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#"><channel><title>Yahoo! Weather - London, GB</title><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><description>Yahoo! Weather for London, GB</description><language>en-us</language><lastBuildDate>Mon, 26 Apr 2010 4:20 pm BST</lastBuildDate><ttl>60</ttl><yweather:location city="London" region=""   country="United Kingdom"/><yweather:units temperature="C" distance="km" pressure="mb" speed="km/h"/><yweather:wind chill="17"   direction="270"   speed="11.27" /><yweather:atmosphere humidity="55"  visibility="9.99"  pressure="1015.92"  rising="0" /><yweather:astronomy sunrise="5:42 am"   sunset="8:14 pm"/><image><title>Yahoo! Weather</title><width>142</width><height>18</height><link>http://weather.yahoo.com</link><url>http://l.yimg.com/a/i/us/nws/th/main_142b.gif</url></image><item><title>Conditions for London, GB at 4:20 pm BST</title><geo:lat>51.51</geo:lat><geo:long>-0.13</geo:long><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><pubDate>Mon, 26 Apr 2010 4:20 pm BST</pubDate><yweather:condition  text="Mostly Cloudy"  code="28"  temp="17"  date="Mon, 26 Apr 2010 4:20 pm BST" /><description><![CDATA[<img src="http://l.yimg.com/a/i/us/we/52/28.gif"/><br /><b>Current Conditions:</b><br />Mostly Cloudy, 17 C<BR /><BR /><b>Forecast:</b><BR />Mon - Partly Cloudy. High: 17 Low: 8<br />Tue - Partly Cloudy. High: 21 Low: 10<br /><br /><a href="http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html">Full Forecast at Yahoo! Weather</a><BR/><BR/>(provided by <a href="http://www.weather.com" >The Weather Channel</a>)<br/>]]></description><yweather:forecast day="Mon" date="26 Apr 2010" low="8" high="17" text="Partly Cloudy" code="29" /><yweather:forecast day="Tue" date="27 Apr 2010" low="10" high="21" text="Partly Cloudy" code="30" /><guid isPermaLink="false">UKXX0085_2010_04_26_16_20_BST</guid></item></channel></rss>';
		doc.load(xml);
		summary = WeatherChatter.getWeatherSummary(doc);
System.Debug('WeatherChatterTest.getWeatherSummary summary - ' + summary);

		// valid xml missing title tag
		doc = new Dom.Document();
		xml = '<rss version="2.0" xmlns:yweather="http://xml.weather.yahoo.com/ns/rss/1.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#"><channel><title>Yahoo! Weather - London, GB</title><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><description>Yahoo! Weather for London, GB</description><language>en-us</language><lastBuildDate>Mon, 26 Apr 2010 4:20 pm BST</lastBuildDate><ttl>60</ttl><yweather:location city="London" region=""   country="United Kingdom"/><yweather:units temperature="C" distance="km" pressure="mb" speed="km/h"/><yweather:wind chill="17"   direction="270"   speed="11.27" /><yweather:atmosphere humidity="55"  visibility="9.99"  pressure="1015.92"  rising="0" /><yweather:astronomy sunrise="5:42 am"   sunset="8:14 pm"/><image><title>Yahoo! Weather</title><width>142</width><height>18</height><link>http://weather.yahoo.com</link><url>http://l.yimg.com/a/i/us/nws/th/main_142b.gif</url></image><item><geo:lat>51.51</geo:lat><geo:long>-0.13</geo:long><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><pubDate>Mon, 26 Apr 2010 4:20 pm BST</pubDate><yweather:condition  text="Mostly Cloudy"  code="28"  temp="17"  date="Mon, 26 Apr 2010 4:20 pm BST" /><description><![CDATA[<img src="http://l.yimg.com/a/i/us/we/52/28.gif"/><br /><b>Current Conditions:</b><br />Mostly Cloudy, 17 C<BR /><BR /><b>Forecast:</b><BR />Mon - Partly Cloudy. High: 17 Low: 8<br />Tue - Partly Cloudy. High: 21 Low: 10<br /><br /><a href="http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html">Full Forecast at Yahoo! Weather</a><BR/><BR/>(provided by <a href="http://www.weather.com" >The Weather Channel</a>)<br/>]]></description><yweather:forecast day="Mon" date="26 Apr 2010" low="8" high="17" text="Partly Cloudy" code="29" /><yweather:forecast day="Tue" date="27 Apr 2010" low="10" high="21" text="Partly Cloudy" code="30" /><guid isPermaLink="false">UKXX0085_2010_04_26_16_20_BST</guid></item></channel></rss>';
		doc.load(xml);
		summary = WeatherChatter.getWeatherSummary(doc);
System.Debug('WeatherChatterTest.getWeatherSummary summary - ' + summary);
		
    }  

 	//
	// test getting URL
	//
    static testMethod void getURLTest()
    {

		Dom.Document doc;
		String xml, url;
		
		doc = new Dom.Document();
		xml = '<rss version="2.0" xmlns:yweather="http://xml.weather.yahoo.com/ns/rss/1.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#"><channel><title>Yahoo! Weather - London, GB</title><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><description>Yahoo! Weather for London, GB</description><language>en-us</language><lastBuildDate>Mon, 26 Apr 2010 4:20 pm BST</lastBuildDate><ttl>60</ttl><yweather:location city="London" region=""   country="United Kingdom"/><yweather:units temperature="C" distance="km" pressure="mb" speed="km/h"/><yweather:wind chill="17"   direction="270"   speed="11.27" /><yweather:atmosphere humidity="55"  visibility="9.99"  pressure="1015.92"  rising="0" /><yweather:astronomy sunrise="5:42 am"   sunset="8:14 pm"/><image><title>Yahoo! Weather</title><width>142</width><height>18</height><link>http://weather.yahoo.com</link><url>http://l.yimg.com/a/i/us/nws/th/main_142b.gif</url></image><item><title>Conditions for London, GB at 4:20 pm BST</title><geo:lat>51.51</geo:lat><geo:long>-0.13</geo:long><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><pubDate>Mon, 26 Apr 2010 4:20 pm BST</pubDate><yweather:condition  text="Mostly Cloudy"  code="28"  temp="17"  date="Mon, 26 Apr 2010 4:20 pm BST" /><description><![CDATA[<img src="http://l.yimg.com/a/i/us/we/52/28.gif"/><br /><b>Current Conditions:</b><br />Mostly Cloudy, 17 C<BR /><BR /><b>Forecast:</b><BR />Mon - Partly Cloudy. High: 17 Low: 8<br />Tue - Partly Cloudy. High: 21 Low: 10<br /><br /><a href="http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html">Full Forecast at Yahoo! Weather</a><BR/><BR/>(provided by <a href="http://www.weather.com" >The Weather Channel</a>)<br/>]]></description><yweather:forecast day="Mon" date="26 Apr 2010" low="8" high="17" text="Partly Cloudy" code="29" /><yweather:forecast day="Tue" date="27 Apr 2010" low="10" high="21" text="Partly Cloudy" code="30" /><guid isPermaLink="false">UKXX0085_2010_04_26_16_20_BST</guid></item></channel></rss>';
		doc.load(xml);
		url = WeatherChatter.getURL(doc);
System.Debug('WeatherChatterTest.getWeatherSummary url - ' + url);

		// test different xml		
		doc = new Dom.Document();
		xml = '<error>Error parsing please contact your administrator</error>';
		doc.load(xml);
		url = WeatherChatter.getURL(doc);
System.Debug('WeatherChatterTest.getWeatherSummary url - ' + url);
		
    }  

 	//
	// test getting Title
	//
    static testMethod void getTitleTest()
    {

		Dom.Document doc;
		String xml, title;
		
		doc = new Dom.Document();
		xml = '<rss version="2.0" xmlns:yweather="http://xml.weather.yahoo.com/ns/rss/1.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#"><channel><title>Yahoo! Weather - London, GB</title><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><description>Yahoo! Weather for London, GB</description><language>en-us</language><lastBuildDate>Mon, 26 Apr 2010 4:20 pm BST</lastBuildDate><ttl>60</ttl><yweather:location city="London" region=""   country="United Kingdom"/><yweather:units temperature="C" distance="km" pressure="mb" speed="km/h"/><yweather:wind chill="17"   direction="270"   speed="11.27" /><yweather:atmosphere humidity="55"  visibility="9.99"  pressure="1015.92"  rising="0" /><yweather:astronomy sunrise="5:42 am"   sunset="8:14 pm"/><image><title>Yahoo! Weather</title><width>142</width><height>18</height><link>http://weather.yahoo.com</link><url>http://l.yimg.com/a/i/us/nws/th/main_142b.gif</url></image><item><title>Conditions for London, GB at 4:20 pm BST</title><geo:lat>51.51</geo:lat><geo:long>-0.13</geo:long><link>http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html</link><pubDate>Mon, 26 Apr 2010 4:20 pm BST</pubDate><yweather:condition  text="Mostly Cloudy"  code="28"  temp="17"  date="Mon, 26 Apr 2010 4:20 pm BST" /><description><![CDATA[<img src="http://l.yimg.com/a/i/us/we/52/28.gif"/><br /><b>Current Conditions:</b><br />Mostly Cloudy, 17 C<BR /><BR /><b>Forecast:</b><BR />Mon - Partly Cloudy. High: 17 Low: 8<br />Tue - Partly Cloudy. High: 21 Low: 10<br /><br /><a href="http://us.rd.yahoo.com/dailynews/rss/weather/London__GB/*http://weather.yahoo.com/forecast/UKXX0085_c.html">Full Forecast at Yahoo! Weather</a><BR/><BR/>(provided by <a href="http://www.weather.com" >The Weather Channel</a>)<br/>]]></description><yweather:forecast day="Mon" date="26 Apr 2010" low="8" high="17" text="Partly Cloudy" code="29" /><yweather:forecast day="Tue" date="27 Apr 2010" low="10" high="21" text="Partly Cloudy" code="30" /><guid isPermaLink="false">UKXX0085_2010_04_26_16_20_BST</guid></item></channel></rss>';
		doc.load(xml);
		title = WeatherChatter.getTitle(doc);
System.Debug('WeatherChatterTest.getWeatherSummary title - ' + title);		

		doc = new Dom.Document();
		xml = '<error>Error parsing please contact your administrator</error>';
		doc.load(xml);
		title = WeatherChatter.getTitle(doc);
System.Debug('WeatherChatterTest.getWeatherSummary title - ' + title);		
		
    }  
    
}