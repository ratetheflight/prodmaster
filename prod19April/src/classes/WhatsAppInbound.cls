@RestResource(urlMapping='/whatsappInbound/*')
global without sharing class WhatsAppInbound {

    @HttpGet
    global static string doGet() {

        return 'Please use POST method to save data';
    }

    @HttpPost
    global static string doPost() {

        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        return postHelper(req, res);
    }
 
    global static String postHelper(RestRequest req, RestResponse res){

        try {
            
            System.debug('Body: ' + (req.requestBody != null ? req.requestBody.toString() : ''));

            if(req.requestBody != null){

                String rBody = req.requestBody.toString().replace('"from":', '"from_x":');
                rBody = rBody.replace('"time":', '"time_x":');
                rBody = rBody.replace('"type":', '"type_x":');

                WhatsAppMessageInfo waMsg = (WhatsAppMessageInfo) JSON.deserialize(rBody, WhatsAppMessageInfo.class);
                system.debug('--+ waMsg: '+waMsg);

                system.debug('--+ content: '+waMsg.content);
                system.debug('--+ to: '+waMsg.to);

                // Invalid json request
                if(waMsg.content == null && waMsg.to == null) {
                    res.statusCode = 500;
                    return 'Something went wrong';
                }

                WhatsappPost__c waPost = new WhatsappPost__c();
                                
                if(!String.IsBlank(waMsg.Content.text)) {
                    waPost.Content__c = waMsg.Content.text;
                }

                if(waMsg.Content.Image != null) {

                    if(waMsg.Content.Image.width!=null && waMsg.Content.Image.height!=null){
                        waPost.Height__c = waMsg.Content.Image.height;
                        waPost.Width__c = waMsg.Content.Image.width;
                    }

                    if(!String.isBlank(waMsg.Content.Image.caption)) waPost.Caption__c = waMsg.Content.Image.caption;
                    if(!String.IsBlank(waMsg.Content.Image.mimeType)) waPost.mimeType__c = waMsg.Content.Image.mimeType;
                    if(!String.IsBlank(waMsg.Content.Image.url)) waPost.URL__c = waMsg.Content.Image.url;

                } else if (waMsg.Content.Video != null) {

                    if(waMsg.Content.Video.width!=null && waMsg.Content.Video.height!=null){
                        waPost.Height__c = waMsg.Content.Video.height;
                        waPost.Width__c = waMsg.Content.Video.width;
                    }

                    if(!String.isBlank(waMsg.Content.Video.caption)) waPost.Caption__c = waMsg.Content.Video.caption;
                    if(!String.IsBlank(waMsg.Content.Video.url)) waPost.URL__c = waMsg.Content.Video.url;
                    if(!String.IsBlank(waMsg.Content.Video.mimeType)) waPost.mimeType__c = waMsg.Content.Video.mimeType;

                } else if (waMsg.Content.Audio != null) {

                    if(!String.IsBlank(waMsg.Content.Audio.mimeType)) waPost.mimeType__c = waMsg.Content.Audio.mimeType;
                    if(!String.IsBlank(waMsg.Content.Audio.url)) waPost.URL__c = waMsg.Content.Audio.url;

                } else if(waMsg.Content.Location != null && 
                    waMsg.Content.Location.latitude != null && waMsg.Content.Location.longitude != null){

                    waPost.Latitude__c = waMsg.Content.Location.latitude;
                    waPost.Longitude__c = waMsg.Content.Location.longitude;
                    waPost.Location_Name__c = waMsg.Content.Location.name;
                }

                waPost.From__c = waMsg.from_x;
                waPost.Full_Name__c = waMsg.name;               
                waPost.MessageId__c = waMsg.messageId;
                
                DateTime dateInstance = datetime.newInstanceGmt(1970, 1, 1, 0, 0, 0);
                waPost.Posted__c = dateInstance.addSeconds(waMsg.time_x);
                
                // waPost.ReportId__c = 
                waPost.To__c = waMsg.to;
                waPost.Type__c = waMsg.type_x;
               
                system.debug('--+ waPost : '+waPost);
                insert waPost;
            }

        } catch (Exception e) {

            res.statusCode = 500;
            system.debug('--+ error: '+e.getStackTraceString());
            return e.getStackTraceString();
        }

        res.statusCode = 200;

        return 'Success';
    }
}