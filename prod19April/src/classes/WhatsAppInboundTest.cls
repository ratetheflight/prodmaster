@isTest
private class WhatsAppInboundTest {

    /**
     * Test a correct whatsapp GET
     **/
    static testMethod void whatsAPGetNotOk() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'GET';
        req.requestBody = Blob.valueof('{"content":{"text":"Test message"},"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"to":"31613020560@s.whatsapp.net","type":"text"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doGet();
        System.assertEquals('Please use POST method to save data', result);
    }

    /**
     * Test a correct whatsapp Text POST
     **/
    static testMethod void whatsAPPostTextOk() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"content":{"text":"Test message"},"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"to":"31613020560@s.whatsapp.net","type":"text"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doPost();
        System.assertEquals('Success', result);
    }

    /**
     * Test a correct whatsapp Video POST
     **/
    static testMethod void whatsAPPostVideoOk() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"content":{"video":{"caption":"The caption text","mimeType":"video/mp4","height":360,"width":480,"url":"https://eazy.im/api/1.0/content/aaa"}},"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"to":"31613020560@s.whatsapp.net","type":"video"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doPost();
        System.assertEquals('Success', result);
    }

    /**
     * Test a correct whatsapp Audio POST
     **/
    static testMethod void whatsAPPostAudioOk() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"content":{"audio":{"mimeType":"audio/aac","url":"https://eazy.im/api/1.0/content/aaa"}},"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"to":"31613020560@s.whatsapp.net","type":"audio"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doPost();
        System.assertEquals('Success', result);
    }

    /**
     * Test a correct whatsapp Image POST
     **/
    static testMethod void whatsAPPostImageOk() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"content":{"image":{"caption":"The caption text","mimeType":"image/jpeg","height":1290,"width":719,"url":"https://eazy.im/api/1.0/content/9999"}},"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"to":"31613020560@s.whatsapp.net","type":"image"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doPost();
        System.assertEquals('Success', result);
    }

    /**
     * Test a correct whatsapp Location POST
     **/
    static testMethod void whatsAPPostLocationOk() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"content":{"location":{"latitude":52.090435,"longitude":5.148338,"name":"Headquarters"}},"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"to":"31613020560@s.whatsapp.net","type":"location"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doPost();
        System.assertEquals('Success', result);
    }

    /**
     * Test a correct whatsapp Location POST
     **/
    static testMethod void whatsAPPostMailformedJson() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"content":{"location":{"latitude":52.090435,"longitude":5.148338,"name":"Headquarters"},"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"to":"31613020560@s.whatsapp.net","type":"location"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doPost();
        //System.assertEquals('Success', result);
    }

    /**
     * Test a incorrect whatsapp POST
     **/
    static testMethod void whatsAPPostNoContent() {
    
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/whatsapp/services/apexrest/whatsappInbound';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof('{"from":"31621803412@s.whatsapp.net","messageId":"227fecf87cdc2ce619fd561a7741c302957be7670d","name":"Bas","time":1442414558,"type":"text"}');

        RestContext.request = req;
        RestContext.response = res;
        
        String result = WhatsAppInbound.doPost();
        System.assertEquals('Something went wrong', result);
    }    
}