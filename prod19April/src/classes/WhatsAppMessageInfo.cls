public with sharing class WhatsAppMessageInfo {

    public Content content;
    public String from_x;
    public String messageId;
    public String name;
    public Integer time_x;
    public String to;
    public String type_x;

    public class Content {
        public String text;
        public Audio audio;
        public Video video;
        public Location location;
        public Image image;
    }

    public class Audio {
        public String mimeType;
        public String url;
    }

    public class Video {
        public String caption;
        public String mimeType;
        public Integer height;
        public Integer width;
        public String url;
    }

    public class Location {
        public String latitude;
        public String longitude;
        public String name;
    }

    public class Image {
        public String caption;
        public String mimeType;
        public Integer height;
        public Integer width;
        public String url;
    }
    
    public static WhatsAppMessageInfo parse(String json) {
        return (WhatsAppMessageInfo ) System.JSON.deserialize(json, WhatsAppMessageInfo .class);
    }    
}