@isTest
public with sharing class WhatsAppMessageInfoTest {

    @isTest
    static void testParseText() {

        String json = '{'+
        '    \"content\": {'+
        '        \"text\": \"This is a test message from John\"'+
        '    },'+
        '    \"from\": \"31622222222@s.whatsapp.net\",'+
        '    \"messageId\": \"b212895dcc91cb1a5f0dbf54bba0789f3d7adb3f\",'+
        '    \"name\": \"John\",'+
        '    \"time\": 1428482233,'+
        '    \"to\": \"31611111111@s.whatsapp.net\",'+
        '    \"type\": \"text\"'+
        '}';
        WhatsAppMessageInfo r = WhatsAppMessageInfo.parse(json);
        System.assert(r != null);

    }

    @isTest
    static void testParseVideo() {

        String json = '{'+
        '    \"content\": {'+
        '        \"video\": {'+
        '            \"caption\": \"The caption text\",'+
        '            \"mimeType\": \"video/mp4\",'+
        '            \"height\": 360,'+
        '            \"width\": 480,'+
        '            \"url\": \"https://eazy.im/api/1.0/content/<unique id>\"'+
        '        }'+
        '    },'+
        '    \"from\": \"31622222222@s.whatsapp.net\",'+
        '    \"messageId\": \"b212895dcc91cb1a5f0dbf54bba0789f3d7adb3f\",'+
        '    \"name\": \"John\",'+
        '    \"time\": 1428482233,'+
        '    \"to\": \"31611111111@s.whatsapp.net\",'+
        '    \"type\": \"video\"'+
        '}';
        WhatsAppMessageInfo r = WhatsAppMessageInfo.parse(json);
        System.assert(r != null);

    }


    @isTest
    static void testParseAudio() {

        String json = '{'+
        '    \"content\": {'+
        '        \"audio\": {'+
        '            \"mimeType\": \"audio/aac\",'+
        '            \"url\": \"https://eazy.im/api/1.0/content/<unique id>\"'+
        '        }'+
        '    },'+
        '    \"from\": \"31622222222@s.whatsapp.net\",'+
        '    \"messageId\": \"b212895dcc91cb1a5f0dbf54bba0789f3d7adb3f\",'+
        '    \"name\": \"John\",'+
        '    \"time\": 1428482233,'+
        '    \"to\": \"31611111111@s.whatsapp.net\",'+
        '    \"type\": \"audio\"'+
        '}';
        WhatsAppMessageInfo r = WhatsAppMessageInfo.parse(json);
        System.assert(r != null);

    } 

    @isTest
    static void testParseLocation() {

        String json = '{'+
        '    \"content\": {'+
        '        \"location\": {'+
        '            \"latitude\": 52.090435,'+
        '            \"longitude\": 5.148338,'+
        '            \"name\": \"Headquarters\"'+
        '        }'+
        '    },'+
        '    \"from\": \"31622222222@s.whatsapp.net\",'+
        '    \"messageId\": \"b212895dcc91cb1a5f0dbf54bba0789f3d7adb3f\",'+
        '    \"name\": \"John\",'+
        '    \"time\": 1428482233,'+
        '    \"to\": \"31611111111@s.whatsapp.net\",'+
        '    \"type\": \"location\"'+
        '}';
        WhatsAppMessageInfo r = WhatsAppMessageInfo.parse(json);
        System.assert(r != null);

    }

}