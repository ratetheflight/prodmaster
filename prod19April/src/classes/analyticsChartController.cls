/*************************************************************************************************
* File Name     :   analyticsChartController 
* Description   : This controller fetches analytics chart configuration data and feeds it to the fronEnd    
                  JS to draw google charts
* @author       :   Ataullah
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     21/03/2016      Ataullah         Created the class
****************************************************************************************************/
public class analyticsChartController {
    
    public integer size {get;set;}
    public string jsonData {get;set;}
    public class DashboardException extends Exception {}

    //Default constructor
    public analyticsChartController(){
        
        try{
            List<ACustomSetting> data = new List<ACustomSetting>();
            List<Analytic_Report__c > AnalyticSettings = new List<Analytic_Report__c >();
            
            List<Analytic_Report__c> repIdList = Analytic_Report__c.getall().values();
            AnalyticSettings =  sortList(repIdList);
            string userDivision = [Select id, Division from User where ID=: userinfo.getUserId()].division;
            
            for(Analytic_Report__c lVar: AnalyticSettings){
                if(lVar.On_Off__c == True && lVar.Division__c  == userDivision )
                    data.add( new ACustomSetting(lVar));     
            }
            
            //Serialize before sending to frontEnd
            jsonData = JSON.serialize(data);
            
            //Get size of the analytic custom setting's list
            size = data.size();
            if(size == 0)
                throw new DashboardException('There is no dashboard for your Division');
        }
        catch(DashboardException ex){
            ApexPages.addMessages(ex);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
    }
    
    //Sort the list of custom Setting data based on Name field. Name field is supposed to have integer values
    // that will help us in arranging on the screen
    private list<Analytic_Report__c> sortList(list<Analytic_Report__c> repIdList ){
    
        list<Integer> sortedList = new list<Integer>();
        List<Analytic_Report__c> SortedCSList = new List<Analytic_Report__c>();
        
        for(Analytic_Report__c cSetting: repIdList ){
            
            sortedList.add(Integer.Valueof(cSetting.name));              
        }
        
        sortedList.sort();
        Analytic_Report__c tempAnalyticStg;
        
        for(Integer i: sortedList){            
              tempAnalyticStg =  Analytic_Report__c.getValues(String.valueof(i));
          SortedCSList.add(tempAnalyticStg);
        }
        
      return SortedCSList;
    }
    
    // SubClass to pass to the JS method in the front end to do calculations
    public Class ACustomSetting {
        
        public Integer serialNo {get; set;}
        public string reportType {get;set;}
        public string reportId {get;set;}
        
        
        public ACustomSetting(Analytic_Report__c aData){
            
            serialNo = Integer.valueof(aData.name);
            reportType = aData.ReportType__c;
            reportId  = aData.Report_Id__c;        
        }
    }
}