/*************************************************************************************************
* File Name     :   analyticsChartControllerTest 
* Description   : Test Class for analyticChartController
* @author       :   Ataullah
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     21/04/2016      Ataullah         Created the class
****************************************************************************************************/
@isTest
public class analyticsChartControllerTest {

    public static testMethod void testMyController() {
        
        List<Analytic_Report__c> CSettingList = new List<Analytic_Report__c>();
        
        //set test page
        PageReference pageRef = Page.AFKLM_SCS_Analytic_Chart;
        Test.setCurrentPage(pageRef);
        
        //Create Test Data
        Analytic_Report__c aReportdata = new Analytic_Report__c();
        aReportdata.name = '1';
        aReportdata.Division__c = '';
        aReportdata.On_Off__c = true;
        aReportdata.Report_Id__c = '00Og0000000UbK4';
        aReportdata.ReportType__c = 'Bar';
        CSettingList.add(aReportdata);
        
        Analytic_Report__c aReportdata1 = new Analytic_Report__c();
        aReportdata1.name = '2';
        aReportdata1.Division__c = 'KLM';
        aReportdata1.On_Off__c = true;
        aReportdata1.Report_Id__c = '00Og0000000UbK4';
        aReportdata1.ReportType__c = 'Bar';
        CSettingList.add(aReportdata1);
        
        Analytic_Report__c aReportdata2 = new Analytic_Report__c();
        aReportdata2.name = '3';
        aReportdata2.Division__c = 'CX';
        aReportdata2.On_Off__c = true;
        aReportdata2.Report_Id__c = '00Og0000000UbK4';
        aReportdata2.ReportType__c = 'Bar';
        CSettingList.add(aReportdata2);
        
        Analytic_Report__c aReportdata3 = new Analytic_Report__c();
        aReportdata3.name = '4';
        aReportdata3.Division__c = 'Cygnific';
        aReportdata3.On_Off__c = true;
        aReportdata3.Report_Id__c = '00Og0000000UbK4';
        aReportdata3.ReportType__c = 'Bar';
        CSettingList.add(aReportdata3);
        
        insert CSettingList;       
               
        Test.StartTest();
          analyticsChartController controller = new analyticsChartController();
        Test.StopTest();
        
    }
    
    public static testMethod void testMyController2() {
        
        List<Analytic_Report__c> CSettingList = new List<Analytic_Report__c>();
        
        //set test page
        PageReference pageRef = Page.AFKLM_SCS_Analytic_Chart;
        Test.setCurrentPage(pageRef);
        
        //Create Test Data
        Analytic_Report__c aReportdata = new Analytic_Report__c();
        aReportdata.name = '1';
        aReportdata.Division__c = '';
        aReportdata.On_Off__c = false;
        aReportdata.Report_Id__c = '00Og0000000UbK4';
        aReportdata.ReportType__c = 'Bar';
        CSettingList.add(aReportdata);
        
        insert CSettingList;       
               
        Test.StartTest();
          analyticsChartController controller = new analyticsChartController();
        Test.StopTest();
        
    }
    
    
}