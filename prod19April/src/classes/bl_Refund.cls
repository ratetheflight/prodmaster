/**********************************************************************
 Name:  bl_Refund
======================================================
Purpose: Business Logic for Refund
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    30/06/2014      Initial development
***********************************************************************/  
public class bl_Refund {

    /*
     * Method to Issue Refund Requests to CSR backoffice and update Refund records with CSR Ref Number
     * Input: List<Id> listOfRefundId - List Of Refund Ids to be processed
     * Output: void
     */    
    public static void issueRefundRequests(List<Id> listOfRefundId){
        if (!listOfRefundId.isEmpty()){
            // Query for Refunds
            Map<Id, Refund__c> mapOfIdToRefund = new Map<Id, Refund__c>([select 
                                                                            Id, 
                                                                            Case__c,
                                                                         	CSR_Refund_Request_Id__c,
                                                                            Error_Message__c,
                                                                            Email__c,
                                                                            First_Name__c,
                                                                            Flight_Date__c,
                                                                            Flight_Number__c,
                                                                            Flying_Blue_Number__c,
                                                                            Language__c,
                                                                            Last_Name__c,
                                                                            Paid_Service__c,
                                                                            PNR__c,
                                                                            Remarks__c,
                                                                            Seat_Number__c,
                                                                            Status__c
                                                                        from Refund__c
                                                                        where Id in :listOfRefundId
                                                                        and (status__c in ('New', 'Submitted', 'Failed'))
                                                                            ]);
            // Call Web Service
            Map<Id, String> mapOfIdToCSR = AFKLM_WS_Manager.issueRefundRequest(mapOfIdToRefund.values());
            if (!mapOfIdToCSR.isEmpty()){
                // Map response
                for (Id thisId : mapOfIdToCSR.keySet()){
                    Refund__c thisRefund = mapOfIdToRefund.get(thisId);
                    if (mapOfIdToCSR.get(thisId).startsWith('Reason')){
                        thisRefund.Error_Message__c = mapOfIdToCSR.get(thisId); 
                        thisRefund.Status__c = 'Failed';
                    } else {
                        thisRefund.CSR_Refund_Request_Id__c = mapOfIdToCSR.get(thisId); 
                        thisRefund.Error_Message__c = null; 
                        thisRefund.Status__c = 'Requested';
                    }
                }
                
                // Prevent trigger on Refund to execute
                AFKLM_Utility.hasRunRefundTrigger = true;
                
                // Update Refunds with CSR Ref Numbers
                update mapOfIdToRefund.values();
            }
        }
    }

    /*
     * Method to call issueRefundRequest method asynchronoulys
     */
    @future(callout=true)
    public static void issueRefundRequestsAsynch(List<Id> listOfRefundId){
        issueRefundRequests(listOfRefundId);
    }

    /*
     * Method to create Refund requests from Case record
     * Input: List<Id> listOfCaseId - List Cases Ids to create Refund requests for
     * Output: Map<Id, Refund__c> - Map of Case Id to Refund 
     */
    public static List<Refund__c> createRefundFromCase(List<Id> listOfCaseId){
        List<Refund__c> listOfRefund = new List<Refund__c>();
        // Retrieve Case details
        List<Case> listOfCase = [
            SELECT 
                    Id,
                    Flight_Date__c,
                    Flight_Prefix__c,
                    Flight_Suffix__c,
                    Flight_Number__c,
                    AccountId,
                    Account.FirstName,
                    Account.LastName,
                    Account.PersonEmail,
                    Account.Flying_Blue_Number__c,
                    SuppliedEmail,
                    Seat_Nr__c,
                    PNR__c
            FROM
                Case
            WHERE
                Id in :listOfCaseId
            ];

        // For each Case create a Refund record
        for (Case thisCase : listOfCase){
            Refund__c thisRefund = new Refund__c(
                Case__c = thisCase.Id,
                Flight_Date__c = thisCase.Flight_Date__c,
                Flight_Number__c = thisCase.Flight_Number__c,
                Account__c = thisCase.AccountId,
                Email__c = thisCase.Account.PersonEmail == null ? thisCase.SuppliedEmail : thisCase.Account.PersonEmail,
                PNR__c = thisCase.PNR__c,
                Seat_Number__c = thisCase.Seat_Nr__c,
                Status__c = 'New'
                );
            listOfRefund.add(thisRefund);
        }

        return listOfRefund;
    }

    /*
     * Method to validate if all fields are provided required for CSR Service
     * Input: thisRefund - instance of Refund
     * Output: Boolean - indicate if validation failed
     */
    public static Boolean validateRefundForRequest(Refund__c thisRefund){
        if ( thisRefund.Email__c == null ||
             thisRefund.First_Name__c == null ||
             thisRefund.Flight_Date__c == null ||
             thisRefund.Flight_Number__c == null ||
             thisRefund.Language__c == null ||
             thisRefund.Last_Name__c == null ||
             thisRefund.Paid_Service__c == null ||
             thisRefund.PNR__c == null ||
             thisRefund.Seat_Number__c == null
            ){
            return false;
        }
        return true;
    }
    
}