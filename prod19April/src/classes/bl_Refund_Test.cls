/**********************************************************************
 Name:  bl_Refund_Test.cls
======================================================
Purpose: Test class for:
1. bl_Refund
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    21 Jul 2014		Initial development
***********************************************************************/  
@isTest
private class bl_Refund_Test {

	/*
	 * Test method of validateRefundForRequest of bl_Refund
	 * Test.startTest() and Test.stopTest() are not used because
	 * of using multiple updates to test the same method
	 */ 	
	@isTest
	static void validateRefundForRequestTest(){
		Account tstAccnt = AFKLM_WS_TestUtil.createAccount();
		Case tstCase = new Case();
		insert tstCase;
		Refund__c tstRefund = new Refund__c();
		tstRefund.Account__c = tstAccnt.Id;
		insert tstRefund;
		tstRefund = [
			select 
                Id, 
                Case__c, 
             	CSR_Refund_Request_Id__c,
                Error_Message__c,
                Email__c,
                First_Name__c,
                Flight_Date__c,
                Flight_Number__c,
                Flying_Blue_Number__c,
                Language__c,
                Last_Name__c,
                Paid_Service__c,
                PNR__c,
                Remarks__c,
                Seat_Number__c,
                Status__c
            from Refund__c
            where Id = :tstRefund.Id
		];
		assertRefund(tstRefund, false);
		tstRefund.Case__c = tstCase.Id;
		update tstRefund;
		assertRefund(tstRefund, false);
		tstRefund.Email__c = 'test@test.nl';
		update tstRefund;
		assertRefund(tstRefund, false);
		tstRefund.Paid_Service__c = 'PAID MEAL';
		update tstRefund;
		assertRefund(tstRefund, false);
		tstRefund.Flight_Date__c = System.today();
		update tstRefund;
		assertRefund(tstRefund, false);
		tstRefund.Flight_Number__c = 'KL1001';
		update tstRefund;
		assertRefund(tstRefund, false);
		tstRefund.Language__c = 'EN';
		update tstRefund;
		assertRefund(tstRefund, false);
		tstRefund.Seat_Number__c = '1A';
		update tstRefund;
		assertRefund(tstRefund, false);
		tstRefund.PNR__c = 'PNRPNR';
		update tstRefund;
		assertRefund(tstRefund, true);
	}

	/*
	 * Test method of createRefundFromCase of bl_Refund
	 */ 	
	@isTest
	static void createRefundFromCaseTest(){
		Account tstAccnt = AFKLM_WS_TestUtil.createAccount();
		List<Case> listOfCase = new List<Case>();
		for (Integer i=0; i<200; i++){
			Case tstCase = AFKLM_WS_TestUtil.createCase(tstAccnt, false);
			listOfCase.add(tstCase);
		}
		insert listOfCase;
		List<Id> listOfCaseId = new List<Id>();
		for (Case thisCase : listOfCase){
			listOfCaseId.add(thisCase.Id);
		}
		Test.startTest();

		List<Refund__c> listOfRefund = bl_Refund.createRefundFromCase(listOfCaseId);

		Test.stopTest();

		System.assertEquals(listOfCase.size(), listOfRefund.size());
	}

	/*
	 * Test method of issueRefundRequestsAsynch and issueRefundRequests of bl_Refund
	 */ 	
	@isTest
	static void issueRefundRequestsAsynchTest(){

		AFKLM_WS_TestUtil.createWSSetting('IssueRefundRequest-v1');

		Account tstAccnt = AFKLM_WS_TestUtil.createAccount();
		List<Case> listOfCase = new List<Case>();
		for (Integer i=0; i<200; i++){
			Case tstCase = AFKLM_WS_TestUtil.createCase(tstAccnt, false);
			listOfCase.add(tstCase);
		}
		insert listOfCase;
		List<Id> listOfCaseId = new List<Id>();
		for (Case thisCase : listOfCase){
			listOfCaseId.add(thisCase.Id);
		}

		List<Refund__c> listOfRefund = bl_Refund.createRefundFromCase(listOfCaseId);
		insert listOfRefund;
		List<Id> listOfRefundId = new List<Id>();
		for (Refund__c thisRefund : listOfRefund){
			listOfRefundId.add(thisRefund.Id);
		}

		AFKLM_WS_MockTest tstWSMock = new AFKLM_WS_MockTest();
		tstWSMock.listOfRefundId = listOfRefundId;
		Test.setMock(WebServiceMock.class, tstWSMock);

		Test.startTest();

		bl_Refund.issueRefundRequestsAsynch(listOfRefundId);

		Test.stopTest();

		listOfRefund = [select Id, CSR_Refund_Request_Id__c, Status__c, Error_Message__c from Refund__c where Id in :listOfRefundId];
		for (Refund__c thisRefund : listOfRefund){
			if (thisRefund.Error_Message__c == null){
				System.assertEquals('Requested', thisRefund.Status__c);
				System.assertEquals('1234567890', thisRefund.CSR_Refund_Request_Id__c);
			} else {
				System.assertEquals(null, thisRefund.CSR_Refund_Request_Id__c);
				System.assertEquals('Failed', thisRefund.Status__c);
				System.assertEquals('Reason - some exception', thisRefund.Error_Message__c);
			}
		}
	}	

	/*
	 * Assert Refund
	 */
	private static void assertRefund(Refund__c tstRefund, Boolean result){
		Boolean validateRefund = bl_Refund.validateRefundForRequest(tstRefund);
		System.assertEquals(result, validateRefund);		
	}
}