/*************************************************************************************************
* File Name     :   pnrWrapper 
* Description   :   SORTING PNRENTRY
* @author       :   
* Modification Log
===================================================================================================
* Ver.    Date            Author         Modification
*--------------------------------------------------------------------------------------------------
* 1.0     25/07/2016      Ata           Created this class
****************************************************************************************************/
global class pnrWrapper implements Comparable {

    public AFKLM_WS_Manager.pnrEntry PE;
    
    // Constructor
    public pnrWrapper (AFKLM_WS_Manager.pnrEntry p) {
        PE = p;
    }
    
    // Compare PNR entry based on the PNR date field.
    global Integer compareTo(Object compareTo) {
        // Cast argument to PNRWrapper
        pnrWrapper compareToPE = (pnrWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        
        string TDate = PE.theDate.split(' ')[0];
        List<string> dtComp = TDate.split('-');
        //creating date instance from booking date field that came thro' integration
        Date apiDate = Date.newinstance(Integer.valueof(dtComp[2]),Integer.valueof(dtComp[1]),Integer.valueof(dtComp[0])); 
        //PE.theDate = apiDate;
        
        string TDate1 = compareToPE.PE.theDate.split(' ')[0];
        List<string> dtComp1 = TDate1.split('-');
        //creating date instance from booking date field that came thro' integration
        Date apiDate1 = Date.newinstance(Integer.valueof(dtComp1[2]),Integer.valueof(dtComp1[1]),Integer.valueof(dtComp1[0])); 
        //compareToPE.PE.theDate = apiDate1;
        
        if (apiDate > apiDate1) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (apiDate < apiDate1) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}