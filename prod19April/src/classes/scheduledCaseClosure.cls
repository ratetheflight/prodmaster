global class scheduledCaseClosure implements Schedulable {
   global void execute(SchedulableContext sc) {

      AFKLM_CaseClosureBatch b = new AFKLM_CaseClosureBatch();
      database.executebatch(b,50);
        
      AFKLM_CaseClosureBatch closebatch = new AFKLM_CaseClosureBatch('In Progress');
      database.executebatch(closebatch ,50);
   }
}