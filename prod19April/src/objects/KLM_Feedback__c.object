<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to capture feedback about the customer from internal KLM resources</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Feedback</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Case_Created__c</fullName>
        <description>Patrick Brinksma - 08 Jan 2014: Ability to create a new case from a feedback record in a list view</description>
        <externalId>false</externalId>
        <formula>IF( ISBLANK( Case__c ), HYPERLINK( &quot;/apex/createCaseFromFeedback?feedbackId=&quot; + Id, IMAGE( &quot;/img/feeds/follow12.png&quot;, &quot;New Case&quot; ), &quot;_self&quot; ), HYPERLINK(&quot;/&quot; + Case__c,  Case__r.CaseNumber, &quot;_self&quot; ) )</formula>
        <label>Case Created</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case_Status__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(  Case__r.Status )</formula>
        <label>Case Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipName>Feedback</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Category__c</fullName>
        <externalId>false</externalId>
        <label>Category</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pre-Flight</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>In-Flight</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Post-Flight</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Non-Flight</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Class__c</fullName>
        <description>Defines which class a customer had. Economy Class (EC) or Business Class (BC)</description>
        <externalId>false</externalId>
        <label>Class</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Business Class</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Economy Comfort</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Contact_Medium__c</fullName>
        <description>Medium by which the customer was contacted.</description>
        <externalId>false</externalId>
        <inlineHelpText>Medium by which the customer was contacted.</inlineHelpText>
        <label>Contact Medium</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Email</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>In person</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Ipad on Board</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Phone</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Postal mail</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Social Media</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Contact_Reason__c</fullName>
        <description>Reason to contact customer.</description>
        <externalId>false</externalId>
        <inlineHelpText>Reason to contact customer.</inlineHelpText>
        <label>Contact Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Call program</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Customer care</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Customer panel</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Customer’s initiative</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>During Flight</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Duty/Leisure – AF/KL Staff</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Event</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Satisfaction Survey</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Department__c</fullName>
        <description>Patrick Brinksma - 20 Nov 2012 - Department of user who created the record</description>
        <externalId>false</externalId>
        <formula>TEXT( CreatedBy.KLM_Department__c )</formula>
        <label>Department</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Donation__c</fullName>
        <description>Patrick Brinksma - 26 June 2014 - Number of miles that a FB Member donates</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of Flying Blue Miles donated</inlineHelpText>
        <label>Donation</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Feedback__c</fullName>
        <externalId>false</externalId>
        <label>Feedback</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Flight_Date__c</fullName>
        <externalId>false</externalId>
        <label>Flight Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Flight_Number__c</fullName>
        <externalId>false</externalId>
        <label>Flight Number</label>
        <length>8</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Flying_Blue_Details__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Flying_Blue_Details__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Flying Blue Details</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Nr_Of_Pax__c</fullName>
        <description>Patrick Brinksma - 20 Jan 2014: Number of passengers in context of miles upgrade</description>
        <externalId>false</externalId>
        <label>Nr Of Pax</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Subcategory__c</fullName>
        <externalId>false</externalId>
        <label>Subcategory</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Category__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>AF/KLM Direct Sales Offices</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>AF/KLM Website</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Airport Check in / Baggage drop off</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Airport Ticket Office</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Boarding</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Cancellation</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Catering</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Delay</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Denied Boarding</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Downgrade</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Flying Blue</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Inflight Entertainment</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Inflight Sales</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Internet / Mobile Check-in</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Lounge</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Luggage</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Luggage Damage</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Luggage Delay</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Luggage Lost</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Missed Connection Due To Delay</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Seat</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Staff</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Ticket fare</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Transfer / connection information</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Upgrade</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Miles Upgrade on Board</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Donation FB miles</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>AF/KLM Direct Sales Offices</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>AF/KLM Website</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Airport Check in / Baggage drop off</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Airport Ticket Office</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Boarding</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Cancellation</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Delay</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Denied Boarding</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Downgrade</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Internet / Mobile Check-in</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Lounge</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Luggage</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Missed Connection Due To Delay</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <controllingFieldValue>In-Flight</controllingFieldValue>
                <valueName>Seat</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <controllingFieldValue>In-Flight</controllingFieldValue>
                <controllingFieldValue>Post-Flight</controllingFieldValue>
                <valueName>Staff</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Ticket fare</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <valueName>Upgrade</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pre-Flight</controllingFieldValue>
                <controllingFieldValue>In-Flight</controllingFieldValue>
                <controllingFieldValue>Post-Flight</controllingFieldValue>
                <controllingFieldValue>Non-Flight</controllingFieldValue>
                <valueName>Other</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>In-Flight</controllingFieldValue>
                <valueName>Catering</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>In-Flight</controllingFieldValue>
                <valueName>Inflight Entertainment</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>In-Flight</controllingFieldValue>
                <valueName>Inflight Sales</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>In-Flight</controllingFieldValue>
                <valueName>Miles Upgrade on Board</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Post-Flight</controllingFieldValue>
                <valueName>Luggage Damage</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Post-Flight</controllingFieldValue>
                <valueName>Luggage Delay</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Post-Flight</controllingFieldValue>
                <valueName>Luggage Lost</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Post-Flight</controllingFieldValue>
                <valueName>Transfer / connection information</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Non-Flight</controllingFieldValue>
                <valueName>Flying Blue</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Non-Flight</controllingFieldValue>
                <valueName>Donation FB miles</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>UUID__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>Patrick Brinksma - 13 Jan 2014: Unique identifier for integration with iPad on Board.</description>
        <externalId>true</externalId>
        <label>UUID</label>
        <length>32</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Feedback</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CREATED_DATE</columns>
        <columns>Department__c</columns>
        <columns>Account__c</columns>
        <columns>Category__c</columns>
        <columns>Subcategory__c</columns>
        <columns>Contact_Medium__c</columns>
        <columns>Contact_Reason__c</columns>
        <columns>Flight_Date__c</columns>
        <columns>Flight_Number__c</columns>
        <columns>Feedback__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>{0000000}</displayFormat>
        <label>Feedback Name</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Feedback</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Category__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Subcategory__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Flight_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Flight_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Department__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Feedback__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Flightdate_and_Nr_Required</fullName>
        <active>true</active>
        <description>Flight date and nr required if category is not Non-Flight</description>
        <errorConditionFormula>AND( TEXT (  Category__c ) &lt;&gt; &apos;Non-Flight&apos;, OR ( ISBLANK(  Flight_Date__c ), ISBLANK(  Flight_Number__c ) ) )</errorConditionFormula>
        <errorMessage>Flight Date and Flight Number are required for category other then &apos;Non-Flight&apos;</errorMessage>
    </validationRules>
</CustomObject>
