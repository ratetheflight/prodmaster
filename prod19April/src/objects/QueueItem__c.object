<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Keeps track of the status of batches that have been placed in the queue.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>BatchId__c</fullName>
        <description>Id of the batch placed in the queue.</description>
        <externalId>false</externalId>
        <label>BatchId</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Class__c</fullName>
        <description>The name of a batchable class to run.</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of a batchable class to run.</inlineHelpText>
        <label>Class</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Finished__c</fullName>
        <description>When this job was finsihed.</description>
        <externalId>false</externalId>
        <inlineHelpText>When this job was finsihed.</inlineHelpText>
        <label>Finished</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Message__c</fullName>
        <externalId>false</externalId>
        <label>Message</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Parameters__c</fullName>
        <description>Use this text area to keep track of a batch&apos;s parameters. This is not restricted to a particular format. E.g.: &apos;dateVal=2015-03-13&amp;intVal=5&apos;  or &apos;x=2; y=6&apos;</description>
        <externalId>false</externalId>
        <label>Parameters</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Priority__c</fullName>
        <description>The priority with which to execute this job. Higher number = higher priority.</description>
        <externalId>false</externalId>
        <label>Priority</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Run_After__c</fullName>
        <description>The job will only be run in between the Run After and Run Before datetimes.</description>
        <externalId>false</externalId>
        <label>Run After</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Run_Before__c</fullName>
        <description>The job will only be run in between the Run After and Run Before datetimes.</description>
        <externalId>false</externalId>
        <label>Run Before</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ScopeSize__c</fullName>
        <description>How many items to process per batch iteration.</description>
        <externalId>false</externalId>
        <inlineHelpText>How many items to process per batch iteration.</inlineHelpText>
        <label>ScopeSize</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Started__c</fullName>
        <description>When this job was started.</description>
        <externalId>false</externalId>
        <inlineHelpText>When this job was started.</inlineHelpText>
        <label>Started</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <defaultValue>&quot;QUEUED&quot;</defaultValue>
        <description>The status of the batch.</description>
        <externalId>false</externalId>
        <label>Status</label>
        <length>32</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>QueueItem</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Class__c</columns>
        <columns>BatchId__c</columns>
        <columns>Status__c</columns>
        <columns>Message__c</columns>
        <columns>Parameters__c</columns>
        <columns>ScopeSize__c</columns>
        <columns>Priority__c</columns>
        <columns>Run_After__c</columns>
        <columns>Run_Before__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Started__c</columns>
        <columns>Finished__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>QI-{000000000}</displayFormat>
        <label>QueueItem Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>QueueItems</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
