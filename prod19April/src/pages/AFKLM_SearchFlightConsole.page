<!-- ********************************************************************** 
 Name:  AFKLM_SearchFlightConsole
 Task:    N/A
 Runs on: ctrl_searchGateway
====================================================== 
Purpose: 
    Search Flights functionality in Console
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1    Narmatha            28/03/2017       Initial Development- ST-2100
   
 *********************************************************************** -->

<apex:page docType="html-5.0" id="searchGateWay" standardController="Account" extensions="ctrl_searchGateway" tabStyle="Search_Customer_Flight__tab" action="{!initPage}">
    <!-- Page Styling -->
    <apex:stylesheet value="{!URLFOR($Resource.CRMTool,'css/jquery-ui.min-theme-start-1.10.3.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.CRMTool,'css/crmtool.css')}" />
    
    <!-- Include jQuery for DOM manipulation -->
    <apex:includeScript value="{!URLFOR($Resource.CRMTool,'js/jquery-1.10.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.CRMTool,'js/jquery-ui.min-1.10.3.js')}" />

    <!-- JavaScript functions -->
    <apex:includeScript value="{!URLFOR($Resource.CRMTool,'js/searchGateway.js')}" />

    <!-- Console Integration -->
    <apex:includeScript value="/support/console/30.0/integration.js"/>
   <script language="JavaScript1.2" src="/js/functions.js"></script>
    <script src="/soap/ajax/9.0/connection.js" type="text/javascript"></script>
    <script id="clientEventHandlersJS" language="javascript">
       
       //Variables
        var accountid;
        var fbnumber;
        var gin;
        var $j = jQuery.noConflict();
        $j(function() {
            sforce.console.setTabTitle('Account 360 View');
        });
        
        //Open existing Person Account in a seperate Primary Tab
        function toOpenAccountPage(id){
            accountid=id;
            sforce.console.getPrimaryTabIds(openSubtab );
        }
        var openSubtab = function openSubtab(result) {
            var primaryTabId = result.id;
            var URLValue='/'+accountid;
            sforce.console.openPrimaryTab(null,URLValue, true);
        };      
       
       
       //Opens the Flight Search results as a Primary tab  
       function toOpenFlightSubtab(){
            if (sforce.console.isInConsole()) {
                sforce.console.getPrimaryTabIds(openSubtab2);
            }
           
        }
        
        var openSubtab2 = function openSubtab2(result) {
            var airline=jQuery('[id$=airLine]').val();
            var flightnumber=jQuery('[id$=flightNumber]').val();
            var flightdate=jQuery('[id$=flightDateString]').val();
            var URLValue='/apex/AFKLM_SearchFlightConsole?param1='+airline+'&param2='+flightnumber+'&param3='+flightdate;
            
            sforce.console.openPrimaryTab(null,URLValue, true, 'Account',null, openSuccess, 'NewCaseSubtab');
                
        };
        
        var openSuccess = function openSuccess(result) {
            //if (result.success == true) {        
            // alert('subtab successfully opened');
            //} else {
            // alert('subtab cannot be opened');
            //}
        };
        
        //Create and Open a PersonAccount page for Flying Blue Passenger
        function searchCustomerOnFB2(fbNum){
            if (sforce.console.isInConsole()) {
                fbnumber=fbNum;
                jQuery('[id$=myHiddenValue]').val('True');
                $j('input[id*="theflyingBlueNumber"]').val(fbNum);
                var flightdate=jQuery('[id$=theflyingBlueNumber]').val();
                searchFBnumber();
            }
        }
    
        function FBSearch(){
            sforce.connection.sessionId = '{!$Api.Session_ID}';
            var accList=sforce.connection.query( "SELECT id from Account Where flying_blue_number__c=\'" +fbnumber+ "\' limit 1");
            var acc=accList.getArray("records");
            accountid=acc[0].Id;
            sforce.console.getPrimaryTabIds(openSubtab );
            location.reload();

        }
        
         //Create and Open a PersonAccount page for non Flying Blue Passenger
        function openNonFBCustomer(ginnumber){
         if (sforce.console.isInConsole()) {
          gin=ginnumber;
         jQuery('[id$=myHiddenValue]').val('True');
         var flightdate=jQuery('[id$=myHiddenValue]').val();
         //createnonFBAcc();
         }
     }
     function openNonFBPrimaryTab(){
      if (sforce.console.isInConsole()) {
         sforce.connection.sessionId = '{!$Api.Session_ID}';
         var acc=sforce.connection.query( "SELECT id from Account Where GIN__c=\'" +gin+ "\' limit 1");
         var account=acc.getArray("records");
         accountid=account[0].Id;
         var URLValue='/'+accountid;
        sforce.console.openPrimaryTab(null,URLValue, true);
       location.reload();
       }

     }
     
       // Execute search if Account id is given and matched the controller Account Id
        var fbNum = $j('input[id*="flyingBlueNumber"]').val();
        if (fbNum != null && fbNum != ''){
            if ('{!thisAccount.Id}'.length >= 15 && '{!thisAccount.Id}'.substring(0, 15) == '{!$CurrentPage.Parameters.Id}'){
                searchCustomerOnFB(fbNum);
            }
        }   
    </script>

    <div style="height:1200px;margin-top:10px;">
    <apex:form id="searchForm">
    
        <!-- Placeholder for messages -->
        <apex:pageMessages id="topMessages"/>
    
        <!-- Dialog model window for loading status -->
        <apex:actionStatus id="dialogStatusCustomerSearch" onStart="showOverlayWithTitle('Searching for customer');" onstop="hideSearch();hideOverlay();" />
        <apex:actionStatus id="dialogStatusFlightSearch" onStart="showOverlayWithTitle('Searching for flight');" onstop="hideSearchForFlight();hideOverlay();" />   
        <apex:actionStatus id="dialogStatusCreateNewAccount" onStart="showOverlayWithTitle('Creating new Account');" onstop="hideSearch();hideOverlay();" />    
        <apex:actionFunction action="{!searchFlight}" name="Searchflights" rerender="searchform"/>
        <apex:actionFunction name="searchFBnumber" action="{!searchFB}" rerender="searchform" oncomplete="FBSearch();" status="dialogStatusCreateNewAccount" />
        
        <div id="searchDiv">

                <!-- Start: Block for search on Flight -->
        <div style="float:left; width: 360px; margin-right: 20px;">
    
            <apex:pageBlock id="searchBlockFlight" title="Search for Flight" mode="detail" rendered="{!isOpenFlightdetails==false}" >
                                            
                <apex:pageBlockButtons id="searchBlockFlightButtons" location="bottom">
                
                    <apex:commandButton id="searchBlockFlightButtonsSearch" value="Search"  status="dialogStatusFlightSearch" rerender="searchForm" onclick="toOpenFlightSubtab()"/>
                    
                </apex:pageBlockButtons>
            
                <apex:pageBlockSection id="searchBlockCustomerSection" collapsible="false">
            
                    <apex:outputLabel id="airLineLabel" styleClass="labelCol" value="Airline" for="airLine"/>
                    
                    <apex:selectList id="airLine" value="{!searchInput.airLine}" size="1">
                        <apex:selectOptions value="{!Airlines}" />
                    </apex:selectList>                  
                    
                    <apex:outputLabel id="flightNumberLabel" styleClass="labelCol" value="Flight Number" for="flightNumber"/>
                    
                    <apex:inputText id="flightNumber" value="{!searchInput.flightNumber}" onkeypress="return noenter(event,'searchBlockFlightButtonsSearch')"/>                 

                    <apex:outputLabel id="flightDateStringLabel" styleClass="labelCol" value="Flight Date" for="flightDateString"/>
                    
                    <apex:inputText id="flightDateString" value="{!searchInput.flightDateString}" onfocus="DatePicker.pickDate(false, document.getElementById('{!$Component.flightDateString}'), false);" onkeypress="return noenter(event,'searchBlockFlightButtonsSearch')"/>     
                    
                </apex:pageBlockSection>
                
            </apex:pageBlock>
            
        </div>
        <!-- End: Block for search on Flight -->
        
       
        </div>
        
       

        <!-- Start: Block for Flight search results -->
        <apex:outputPanel style="display: block; width: 100%;" layout="block" id="customerFlightResults">

            <apex:pageblock id="searchFlightResultsBlock" title="{!nrOfPaxEntries} Search results for {!searchInput.flightDate} {!searchInput.airline}{!searchInput.flightNumber} ({!airportOri} - {!airportDes})" rendered="{!nrOfPaxEntries > 0}">

                <apex:pageBlockButtons >
                
                    <apex:commandButton id="searchFlightResultsBlockHideSearch" value="Hide Search" onclick="event.preventDefault();hideSearch();showHideButtons('searchFlightResultsBlockHideSearch', 'none');" style="display:none;"/>                
                    
                    <apex:commandButton id="searchFlightResultsBlockNewSearch" value="New Search" onclick="event.preventDefault();showSearch('flight');showHideButtons('searchFlightResultsBlockHideSearch', 'inline');"/>
                    
                    <apex:commandButton value="Create Contact List" action="{!createContactList}"/>
                
                </apex:pageBlockButtons>
                <!-- Field to capture selected point of sales -->
                <apex:inputText id="paxCountry" value="{!paxCountry}" style="display:none;"/>
                
                <apex:inputHidden value="{!searchinput.flyingBlueNumber}" id="theflyingBlueNumber"/> 
                <apex:inputHidden value="{!isConsole}" id="myHiddenValue"/>
                
                <table class="list" id="customerFlightResultsBlockTable" border="0" cellpadding="0" cellspacing="0">
                    <caption class="" style="text-align:left;margin-bottom:5px;">
                        <span style="margin-right:15px;">&nbsp;</span>
                        Filter 1: Point of Sales: <select id="aFilter1" onchange="$j('input[id*=paxCountry]').val(this.options[this.selectedIndex].value);filterTable()" size="1"></select> 
                        AND TierLevel: <select id="aFilter2" onchange="filterTable()" size="1"></select> 
                        AND Booking Class:<select id="aFilter3" onchange="filterTable()" size="1"></select>
                        AND IATA Code:<select id="aFilter4" onchange="filterTable()" size="1"></select>
                        AND Leg:<select id="aFilter5" onchange="filterTable()" size="1"></select><br />
                        OR Filter 2: Point of Sales: <select id="bFilter1" onchange="filterTable()" size="1"></select> 
                        AND TierLevel: <select id="bFilter2" onchange="filterTable()" size="1"></select> 
                        AND Booking Class:<select id="bFilter3" onchange="filterTable()" size="1"></select>
                        AND IATA Code:<select id="bFilter4" onchange="filterTable()" size="1"></select>&nbsp;
                        <a href="javascript:resetFilters()">reset filters</a>       
                    </caption>
                    <colgroup span="16"></colgroup>
                    <thead class="rich-table-thead">
                        <tr class="headerRow">
                            <th class="headerRow" scope="col" colspan="1" id="fSelectheader">
                                <input id="fCheckAll" type="checkbox" onclick="checkUnCheckAll(this)" title="Click to select/deselect all"/>
                            </th>
                            <th class="headerRow" scope="col" colspan="1"></th>
                            <th class="headerRow" scope="col" colspan="1">Tier Level</th>
                            <th class="headerRow" scope="col" colspan="1"></th>
                            <th class="headerRow" scope="col" colspan="1">Flying Blue Number</th>
                            <th class="headerRow" scope="col" colspan="1">Name</th>
                            <th class="headerRow" scope="col" colspan="1">Corporate</th>
                            <th class="headerRow" scope="col" colspan="1">Award /<br/>Upgrade</th>
                            <th class="headerRow" scope="col" colspan="1">Point<br/>Of Sales</th>
                            <th class="headerRow" scope="col" colspan="1">Booked</th>
                            <th class="headerRow" scope="col" colspan="1">Flown</th>
                            <th class="headerRow" scope="col" colspan="1">Status</th>
                            <th class="headerRow" scope="col" colspan="1">Seat</th>
                            <th class="headerRow" scope="col" colspan="1"></th>
                            <th class="headerRow" scope="col" colspan="1">Legs</th>
                            <th class="headerRow" scope="col" colspan="1">PNR</th>
                            <th class="headerRow" scope="col" colspan="1">IATA</th>
                        </tr>
                    </thead>
                    <tbody>
                    <apex:repeat value="{!mapOfKeyToPax}" var="k">
                        <tr class="dataRow even first" onmouseover="if (window.hiOn){hiOn(this);}" onmouseout="if (window.hiOff){hiOff(this);}" onblur="if (window.hiOff){hiOff(this);}" onfocus="if (window.hiOn){hiOn(this);}">
                            <td class="dataCell" id="fSelect">
                                <apex:inputCheckbox id="fCheck" onclick="$j('input.fCheckAll').prop('checked', false)" value="{!mapOfKeyToPax[k].selected}" title="Click to select passenger to add to a Contact List" />
                            </td>
                            <td class="dataCell" style="text-align:center;">
                                <apex:commandLink action="{!createCustomerWithUniqueId}" rendered="{!mapOfKeyToPax[k].theAccntId == null && mapOfKeyToPax[k].theFlyingBlueNumber == ''}" onclick="openNonFBCustomer('{!mapOfKeyToPax[k].theUniqueId}');" oncomplete="openNonFBPrimaryTab();" status="dialogStatusCreateNewAccount">
                                    <apex:param name="uniqueId" value="{!mapOfKeyToPax[k].theUniqueId}" assignTo="{!uniqueId}"/>
                                    <apex:image id="caGIN" value="/img/feeds/follow12.png" style="width: 12px; height: 12px; cursor: pointer; margin-left: 4px;" title="Click to create an Account" />
                                </apex:commandLink>
                                <apex:image id="caFB" value="/img/feeds/follow12.png" style="width: 12px; height: 12px; cursor: pointer; margin-left: 4px;" rendered="{!mapOfKeyToPax[k].theAccntId == null && mapOfKeyToPax[k].theFlyingBlueNumber != ''}" title="Click to open detail page" onclick="searchCustomerOnFB2('{!mapOfKeyToPax[k].theFlyingBlueNumber}')" />
                            </td>                           
                            <td class="dataCell" id="{!k}:fTierLevel">{!mapOfKeyToPax[k].theTierLevel}</td>
                            <td class="dataCell"><apex:image rendered="{!!ISNULL(mapOfKeyToPax[k].theFlyingBlueNumber)}" value="/img/msg_icons/info16.png" width="12" height="12" style="cursor: pointer;" title="Click to quickly view Flying Blue Member details" onclick="fbMemberPopup(this, '{!mapOfKeyToPax[k].theFlyingBlueNumber}');" /></td>
                            <td class="dataCell">
                                <apex:outputText rendered="{!mapOfKeyToPax[k].theFlyingBlueNumber != ''}">{!mapOfKeyToPax[k].theFlyingBlueNumber}</apex:outputText>
                                <apex:outputText rendered="{!mapOfKeyToPax[k].theFlyingBlueNumber == ''}">{!mapOfKeyToPax[k].theFreqFlyerNumber}</apex:outputText>
                            </td>
                            <!-- Open a seperate tab when we click on the Account Name ST-1680 -->
                            <apex:outputText rendered="{!mapOfKeyToPax[k].theAccntId != null}">
                            <td class="dataCell"><a href="#" onclick="toOpenAccountPage('{!mapOfKeyToPax[k].theAccntId}' );return false">{!mapOfKeyToPax[k].theLastname} {!mapOfKeyToPax[k].theFirstname}<apex:outputPanel title="{!mapOfKeyToPax[k].theInfantName}" rendered="{!mapOfKeyToPax[k].theHasInfant == true}"> (INF)<apex:outputPanel rendered="{!mapOfKeyToPax[k].thePaxType <> 'ADT'}"> ({!mapOfKeyToPax[k].thePaxType})</apex:outputPanel></apex:outputPanel></a></td>
                            </apex:outputText>
                            <apex:outputText rendered="{!mapOfKeyToPax[k].theAccntId == null}">
                            <td class="dataCell">{!mapOfKeyToPax[k].theLastname} {!mapOfKeyToPax[k].theFirstname}<apex:outputPanel title="{!mapOfKeyToPax[k].theInfantName}" rendered="{!mapOfKeyToPax[k].theHasInfant == true}"> (INF)</apex:outputPanel><apex:outputPanel rendered="{!mapOfKeyToPax[k].thePaxType <> 'ADT'}"> ({!mapOfKeyToPax[k].thePaxType})</apex:outputPanel></td>
                            </apex:outputText>
                            <td class="dataCell">{!mapOfKeyToPax[k].theLevel}</td>
                            <td class="dataCell"><span title="{!mapOfKeyToPax[k].theFlyingBlueNumber}">{!mapOfKeyToPax[k].theAwardOrUpgrade}</span></td>
                            <td class="dataCell" id="{!k}:fPOS">{!mapOfKeyToPax[k].theCountry}</td>
                            <td class="dataCell" id="{!k}:fBookingClass">{!mapOfKeyToPax[k].theBookingClass}</td>
                            <td class="dataCell">{!mapOfKeyToPax[k].theClassFlown}</td>
                            <td class="dataCell">
                            <apex:outputText rendered="{!( mapOfKeyToPax[k].theBoardingStatus == 'BOARDED' || ( mapOfKeyToPax[k].theBoardingStatus == null && mapOfKeyToPax[k].theCheckedInPaxStatus == 'CONFIRMED' ) ) && mapOfKeyToPax[k].theCheckedInPaxStatus != 'SEAT_AVAILABLE'}">
                                <img src="/img/msg_icons/confirm16.png" width="12" height="12" title="Booking status: {!mapOfKeyToPax[k].theCheckedInPaxStatus} - Boarding status: {!mapOfKeyToPax[k].theBoardingStatus}" style="cursor:pointer;" onclick="showInfoPanel(this, 'statusInfoPane');"/>
                            </apex:outputText>
                            <apex:outputText rendered="{!mapOfKeyToPax[k].theCheckedInPaxStatus == 'SEAT_AVAILABLE'}">
                                <img src="/img/msg_icons/warning16.png" width="12" height="12" title="Booking status: {!mapOfKeyToPax[k].theCheckedInPaxStatus} - Boarding status: {!mapOfKeyToPax[k].theBoardingStatus}" style="cursor:pointer;" onclick="showInfoPanel(this, 'statusInfoPane');"/>
                            </apex:outputText>
                            <apex:outputText rendered="{!mapOfKeyToPax[k].theBoardingStatus != null && mapOfKeyToPax[k].theBoardingStatus != 'BOARDED' && mapOfKeyToPax[k].theCheckedInPaxStatus != 'SEAT_AVAILABLE'}">
                                <img src="/img/msg_icons/error16.png" width="12" height="12" title="Booking status: {!mapOfKeyToPax[k].theCheckedInPaxStatus} - Boarding status: {!mapOfKeyToPax[k].theBoardingStatus}" style="cursor:pointer;" onclick="showInfoPanel(this, 'statusInfoPane');"/>
                            </apex:outputText>
                                <span id="statusInfoPane" style="display:none;">
                                    <div class="apexp"><div class="individualPalette"><div class="accountBlock">
                                    <div class="bPageBlock brandSecondaryBrd bDetailBlock secondaryPalette">
                                        <div class="pbBody">
                                            <table class="list" border="0" cellpadding="0" cellspacing="0">
                                            <caption class="" style="text-align:left;margin-left:8px;font-weight:bold;">Name: {!mapOfKeyToPax[k].theLastname} {!mapOfKeyToPax[k].theFirstname}</caption>
                                            <colgroup span="6"></colgroup>
                                            <thead class="rich-table-thead">
                                            <tr class="headerRow">
                                                <th class="headerRow">Type</th>
                                                <th class="headerRow">Value</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="dataRow">
                                                    <td class="dataCell">Booked Status</td>
                                                    <td class="dataCell">{!mapOfKeyToPax[k].theCheckedInPaxStatus}</td>
                                                </tr>
                                                <tr class="dataRow">
                                                    <td class="dataCell">Boarding Status</td>
                                                    <td class="dataCell">{!mapOfKeyToPax[k].theBoardingStatus}</td>
                                                </tr>
                                                <tr class="dataRow">
                                                    <td class="dataCell">Acceptance Status</td>
                                                    <td class="dataCell">{!mapOfKeyToPax[k].theAcceptanceStatus}</td>
                                                </tr>
                                                <tr class="dataRow">
                                                    <td class="dataCell">Cancellation Code</td>
                                                    <td class="dataCell">{!mapOfKeyToPax[k].theAcceptanceCancellationCode}</td>
                                                </tr>
                                                <tr class="dataRow">
                                                    <td class="dataCell">Regrade Status</td>
                                                    <td class="dataCell">{!mapOfKeyToPax[k].theRegradeStatus}</td>
                                                </tr>
                                                <tr class="dataRow">
                                                    <td class="dataCell">Regrade Code</td>
                                                    <td class="dataCell">{!mapOfKeyToPax[k].theRegradeCode}</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                            <p style="text-align:right;font-size:10px;cursor:pointer;">(click to close...)</p>
                                        </div>
                                    </div>
                                    </div></div></div>
                                </span>                 
                            </td>
                            <td class="dataCell">{!mapOfKeyToPax[k].theSeat}</td>
                            <td class="dataCell">
                                <apex:outputPanel rendered="{!mapOfKeyToPax[k].listOfFlights.size > 0}">
                                <apex:outputPanel rendered="{!mapOfKeyToPax[k].theConnection == 'i'}" style="cursor:pointer;font-family:sans-serif;font-size:20px;" onclick="showInfoPanel(this, 'flightInfoPane');" title="Click to view connecting flight details">⇩</apex:outputPanel>
                                <apex:outputPanel rendered="{!mapOfKeyToPax[k].theConnection == 'o'}" style="cursor:pointer;font-family:sans-serif;font-size:20px;" onclick="showInfoPanel(this, 'flightInfoPane');" title="Click to view connecting flight details">⇧</apex:outputPanel>
                                <apex:outputPanel rendered="{!mapOfKeyToPax[k].theConnection == 'io'}" style="cursor:pointer;font-family:sans-serif;font-size:20px;" onclick="showInfoPanel(this, 'flightInfoPane');" title="Click to view connecting flight details">⇩⇧</apex:outputPanel>
                                
                                <span id="flightInfoPane" style="display:none;">
                                    <div class="apexp"><div class="individualPalette"><div class="accountBlock">
                                    <div class="bPageBlock brandSecondaryBrd bDetailBlock secondaryPalette">
                                        <div class="pbBody">
                                            <table class="list" border="0" cellpadding="0" cellspacing="0">
                                            <caption class="" style="text-align:left;margin-left:8px;font-weight:bold;">Name: {!mapOfKeyToPax[k].theLastname} {!mapOfKeyToPax[k].theFirstname}</caption>
                                            <colgroup span="6"></colgroup>
                                            <thead class="rich-table-thead">
                                            <tr class="headerRow">
                                                <th class="headerRow">Type</th>
                                                <th class="headerRow">Flight</th>
                                                <th class="headerRow">Departure</th>
                                                <th class="headerRow">Arrival</th>
                                                <th class="headerRow">From</th>
                                                <th class="headerRow">To</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <apex:repeat value="{!mapOfKeyToPax[k].listOfFlights}" var="f">
                                                <tr class="dataRow">
                                                    <td class="dataCell" style="{!IF(LEFT(f.theType, 10) = 'Transfered', 'font-weight:bold;color:#ff0000;', '')}">{!f.theType}</td>
                                                    <td class="dataCell">{!f.theFlightNumber}{!IF(f.theOperatingFlightNumber != '' && f.theOperatingFlightNumber != f.theFlightNumber, ' (' + f.theOperatingFlightNumber + ')', '')}</td>
                                                    <td class="dataCell">{!f.theDep}</td>
                                                    <td class="dataCell">{!f.theArr}</td>
                                                    <td class="dataCell">{!f.theOri}</td>
                                                    <td class="dataCell">{!f.theDes}</td>
                                                </tr>
                                            </apex:repeat>
                                            </tbody>
                                            </table>
                                            <p style="text-align:right;font-size:10px;cursor:pointer;">(click to close...)</p>
                                        </div>
                                    </div>
                                    </div></div></div>
                                </span>
                                </apex:outputPanel>
                            </td>
                            <td class="dataCell" id="{!k}:fLegs">{!mapOfKeyToPax[k].theLegOri} - {!mapOfKeyToPax[k].theLegDes}</td>
                            <td class="dataCell">{!mapOfKeyToPax[k].thePNRNumber}</td>
                            <td class="dataCell" id="{!k}:fIATA" colspan="1">{!mapOfKeyToPax[k].theIataCode}</td>
                        </tr>
                    </apex:repeat>
                    </tbody>
                </table>
            
            </apex:pageblock>
            
            <script type="text/javascript">
                // This script will fire on Load, but then the pageBlockTable is emtpy
                // After the submit of the searchFlight the fCheckAll will be found
                
                if ($j('input[id*="fCheckAll"]').length == 1){
                    // Give each TR a filter element
                    setFilterData();
                    // Get filter options for Point of Sales
                    setFilterOptions('fPOS', 'aFilter1', 'bFilter1');
                    setFilterOptions('fTierLevel', 'aFilter2', 'bFilter2');
                    setFilterOptions('fBookingClass', 'aFilter3', 'bFilter3');
                    setFilterOptions('fIATA', 'aFilter4', 'bFilter4');
                    setLegFilterOption('fLegs', 'aFilter5');
                }
                
            </script>
            
        </apex:outputPanel>
        
        <!-- Load additional fields for controller constructor -->
        <div style="display:none;">{!Account.Flying_Blue_Number__c} {!Account.PersonContactId}</div>
        <!--
        <apex:outputPanel id="debugInfo" styleClass="debugInfo" layout="block" rendered="{!debugInfo != '' && $Profile.Name == 'System Administrator'}">
            
            <p>Debug information:</p>
            
            <apex:outputText value="{!debugInfo}" escape="false"/>
            
        </apex:outputPanel>
        -->
    </apex:form>
    </div>
    
    <!-- Loading dialog driven by jQuery UI -->
    <div id="loadingDialog">
        <div class="innerDialog">Busy retrieving backend data... </div>
    </div>

    <!-- info dialog driven by jQuery UI -->
    <div id="infoDialog" class="bPageBlock" style="margin-top: 3px;"></div>
        
      
</apex:page>