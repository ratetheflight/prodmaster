/*************************************************************************************************
* File Name     :   AFKLM_AccountBeforeTrigger
* Description   :   
* @author       :   Prasanna
* Modification Log
===================================================================================================
* Ver.    Date            Author    Modification
*--------------------------------------------------------------------------------------------------
* 1.0     21/10/2016      Prasanna    Created the trigger

****************************************************************************************************/
trigger AFKLM_AccountBeforeTrigger on Account (before insert, before update) {

    AFKLM_AccountTriggerHandler handler=new AFKLM_AccountTriggerHandler();
        
    // Trigger Calls to the handler
        
       
        if(Trigger.isBefore && Trigger.isUpdate)
            handler.onBeforeInsertUpdate(trigger.new,trigger.oldMap);
      
}