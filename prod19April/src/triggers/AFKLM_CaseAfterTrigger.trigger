/*************************************************************************************************
* File Name     :   AFKLM_CaseAfterTrigger
* Description   :   
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author    Modification
*--------------------------------------------------------------------------------------------------
* 1.0     02/10/2016      Nagavi    Created the trigger

****************************************************************************************************/
trigger AFKLM_CaseAfterTrigger on Case (after insert, after update,after delete) {

    AFKLM_CaseAfterTriggerHandler handler=new AFKLM_CaseAfterTriggerHandler();
        
    // Trigger Calls to the handler
        if(trigger.isAfter && trigger.isInsert) 
            handler.onAfterInsert(trigger.new);     
    
        if(Trigger.isAfter && Trigger.isUpdate)
            handler.onAfterUpdate(trigger.old, trigger.new,trigger.oldMap,trigger.newMap);
          
        if(Trigger.isAfter && Trigger.isDelete) 
          handler.onAfterDelete(trigger.old); 
}