/*************************************************************************************************
* File Name     :   KLM_DGCaseCompanionTrigger 
* Description   :   
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author    Modification
*--------------------------------------------------------------------------------------------------
* 1.0     30/01/2017      Nagavi    Created the trigger

****************************************************************************************************/
trigger AFKLM_DGCaseCompanionTrigger on dgai__DG_Companion__c(after insert) {
     
     AFKLM_DGCaseCompanionHandler handler=new AFKLM_DGCaseCompanionHandler();
        
    // Trigger Calls to the handler
        if(trigger.isAfter && trigger.isInsert) 
          handler.onAfterInsert(trigger.new);     

}