/*************************************************************************************************
* File Name     :   AFKLM_SocialPostBeforeTrigger
* Description   :   
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author    Modification
*--------------------------------------------------------------------------------------------------
* 1.0     23/08/2016      Nagavi    Created the trigger
* 1.1     25/01/2017      Sathish   Updated trigger to execute keyword search based on language
* 1.2     07/02/2017      Sai       Updating the Last outbound message related to the post. JIRA ST-1199
* 1.3     18/02/2017      Nagavi    Added logic to open up auto urgency for Facebook :ST - ST-1755
* 1.4     22/02/2017      Sai       Opened up urgency for Twitter. JIRA ST-1752.
* 1.5     07/03/2017      Aruna     Update language in outbound socialposts (ST-1725)
****************************************************************************************************/
trigger AFKLM_SocialPostBeforeTrigger on SocialPost(before insert, before update,before delete) {

    AFKLM_SocialPostBeforeTriggerHandler beforehandler=new AFKLM_SocialPostBeforeTriggerHandler ();
    SocialPostTriggerHandler trghandler=new SocialPostTriggerHandler();
    Map<ID,List<Socialpost>> updateLanaguegMap = new Map<ID,List<Socialpost>>();
        
    Boolean triggerexecution=FALSE;
    if(!test.isRunningTest())    
    {
       AFKLM_Trigger_Execution__c trgexecution=AFKLM_Trigger_Execution__c.getValues('Social Post');
     if(trgexecution!=NULL)
        triggerexecution=trgexecution.Run_Old_Trigger__c;
    }
    else
    {
       triggerexecution=TRUE; 
    }
    
  if(triggerexecution){ 
    // Trigger Calls to the handler
        if(trigger.isBefore && trigger.isInsert) 
        {
               //1.1 starts
             list<socialpost> newpostlist=new list<socialpost>();         
              for(SocialPost newpost:trigger.new)
              {  
                //Modified by Sai. Added Twitter to the condition. JIRA ST-1752.
                if(newpost.content!=null && newpost.IsOutbound!=true  && newpost.TopicProfileName!=null && (newpost.TopicProfileName.contains('KLM') || newpost.TopicProfileName=='KLM') && (newpost.Provider=='Facebook' || ( newpost.Provider == 'Twitter' && newpost.MessageType.contains('Direct'))))
                {
                   if(newpost.PostTags!=NULL)
                  {
                     newpostlist.addAll(trigger.new);
                     
                  }
                }
                  //1.5 changes starts
                if(newpost.isoutbound && newpost.parentid!=null)
                {
                    if(updateLanaguegMap.containskey(newpost.parentid))
                    {
                        updateLanaguegMap.get(newpost.parentid).add(newpost);
                    }
                    else
                    {
                        updateLanaguegMap.put(newpost.parentid,new List<SocialPost>{newpost});
                    }
                }
                  //1.5 changes starts
              }
            if(!newpostlist.isEmpty())
            {
              beforehandler.onBeforeInsert(newpostlist);  
            }
            if(!updateLanaguegMap.isEmpty())
            {
                trghandler.updateoutboundlanguage(updateLanaguegMap);
            }
            //1.1 ends
           //beforehandler.onBeforeInsert(trigger.new); 
        }
             
        
        if(trigger.isBefore && trigger.isUpdate)             
        {
           
           beforehandler.onBeforeUpdate(trigger.old, trigger.new,trigger.oldMap,trigger.newMap); 
        }
        //added by sai.Updating the Last outbound message related to the post. JIRA ST-1199
        //Start
        if(trigger.isUpdate && trigger.isBefore)
        {
            List<SocialPost> socialPostList = new List<SocialPost>();
            for(socialpost sp:Trigger.new)
            {
                if(sp.TopicProfileName != null)
                {
                    if(sp.parentid!=null && sp.Last_OutBound_Message__C == null && sp.Isoutbound == False && (sp.TopicProfileName.contains('KLM') || sp.TopicProfileName.contains('Airline')))
                    {                    
                        socialPostList.add(sp);
                    }
                }
                else
                {
                    if(sp.parentid!=null && sp.Last_OutBound_Message__C == null && sp.Isoutbound == False && sp.provider == 'wechat' && (sp.company__c == null || sp.company__c !='airfrance') )
                    {
                        socialPostList.add(sp);
                    }
                }
                
            }
            beforehandler.findLastOutBound(socialPostList);
        }
        //end
    }
}