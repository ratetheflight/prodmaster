/*************************************************************************************************
* File Name     :   AFKLM_SocialPostTrigger 
* Description   :   
* @author       :   Nagavi
* Modification Log
===================================================================================================
* Ver.    Date            Author    Modification
*--------------------------------------------------------------------------------------------------
* 1.0     03/02/2017      Nagavi    Created the trigger

****************************************************************************************************/
trigger AFKLM_SocialPostTrigger on SocialPost (after insert, after update,after delete,before insert, before update, before delete){

    AFKLM_SocialPostTriggerHandler handler=new AFKLM_SocialPostTriggerHandler();
    Boolean triggerexecution=FALSE;
    if(!test.isRunningTest())    
    {
       AFKLM_Trigger_Execution__c trgexecution=AFKLM_Trigger_Execution__c.getValues('Social Post');
	   if(trgexecution!=NULL)
        triggerexecution=trgexecution.Run_New_Trigger__c;
    }
    else
    {
       triggerexecution=TRUE; 
    }
    
  if(triggerexecution){    
    if (Trigger.IsBefore){
        if (Trigger.IsInsert)
            handler.onBeforeInsert(trigger.new);
        if (Trigger.IsUpdate)
            handler.onBeforeUpdate(trigger.old,trigger.new,trigger.oldMap,trigger.newMap);
        if (Trigger.IsDelete)
            handler.onBeforeDelete(trigger.old);
    }
    
    if (Trigger.IsAfter){
        if (Trigger.IsInsert)
            handler.onAfterInsert(trigger.new);
        if (Trigger.IsUpdate)
            handler.onAfterUpdate(trigger.old,trigger.new,trigger.oldMap,trigger.newMap);
        if (Trigger.IsDelete)
            handler.onAfterDelete(trigger.old);
    	}
	}
}