/*************************************************************************************************
* File Name     :   AFKLM_SupportTicketAfterTrigger
* Description   :   
* @author       :   Prasanna kumar
* Modification Log
===================================================================================================
* Ver.    Date            Author    Modification
*--------------------------------------------------------------------------------------------------
* 1.0     29/12/2016      Prasanna    Created the trigger

****************************************************************************************************/
trigger AFKLM_SupportTicketAfterTrigger on Support_ticket__c(after insert, after update) {
    
  if(system.isFuture()) return;
 
    List<Id> tktid=new List<Id>();
    // Trigger Calls to the handler
        if(trigger.isUpdate || trigger.isInsert) {
        for(Support_ticket__c tkt: Trigger.new){
        if(tkt.severity__c=='High (SOCIAL ONLY)'){
        tktid.add(tkt.id);
        }
        }
        if(tktid.size()>0)
         AFKLM_SupportTicketTriggerHandler.onAfterInsert(tktid);     
    }
    /*    if(Trigger.isAfter && Trigger.isUpdate)
            handler.onAfterUpdate(trigger.old, trigger.new,trigger.oldMap,trigger.newMap);
          
        if(Trigger.isAfter && Trigger.isDelete) 
          handler.onAfterDelete(trigger.old); */
}