/**********************************************************************
 Name:  Contact_List_Account_BeforeInsertUpdate
======================================================
Purpose: 

Populate the ContactId from Person Account

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		AJ van Kesteren		30/01/2014		INITIAL DEVELOPMENT
***********************************************************************/	
trigger Contact_List_Account_BeforeInsertUpdate on Contact_List_Account__c (before insert) {

	Set<Id> setOfAccntId = new Set<Id>();

	for ( Contact_List_Account__c thisCLA : Trigger.new ) {
		if ( thisCLA.Contact__c == null ) {
			setOfAccntId.add( thisCLA.Account__c );
		}
	}

	if ( !setOfAccntId.isEmpty() ) {
		Map<Id, Account> mapOfIdToAcc = new Map<Id, Account>([
											select
													Id,
													PersonContactId
											from
													Account
											where
													Id in :setOfAccntId
										]);
		for ( Contact_List_Account__c thisCLA : Trigger.new ) {
			if ( thisCLA.Contact__c == null ) {
				if ( mapOfIdToAcc.get( thisCLA.Account__c) != null ) {
					thisCLA.Contact__c = mapOfIdToAcc.get( thisCLA.Account__c).PersonContactId;
				}
			}
		}
	}

}