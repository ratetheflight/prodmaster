trigger DG_CaseTrigger2 on Case (after insert, before update, after update) {

    if ((Trigger.isAfter && Trigger.isInsert) ||
        (Trigger.isAfter && Trigger.isUpdate)){
        // The subset of Cases to perform predictions on.
        List<Case> casesToPredict = new List<Case>();

        // If a Case record has just been upserted, the DG_Fill_Status__c is set to 'Pending' and the
        // Data_for_Prefill_By_DG__c has changed then perform a fresh prediction.
        for (Case thisCase : Trigger.new){
            if (!String.isEmpty(thisCase.dgAI2__DG_Fill_Status__c) &&
                thisCase.dgAI2__DG_Fill_Status__c.equals('Pending') &&
                !String.isEmpty(thisCase.Data_for_Prefill_By_DG__c)){
        
                if (Trigger.isInsert ||
                    !thisCase.dgAI2__DG_Fill_Status__c.equals(Trigger.oldMap.get(thisCase.Id).dgAI2__DG_Fill_Status__c) ||
                    !thisCase.Data_for_Prefill_By_DG__c.equals(Trigger.oldMap.get(thisCase.Id).Data_for_Prefill_By_DG__c)){

                    // Add this Case to the list of Cases to perform predictions on.
                    casesToPredict.add(thisCase);
                }
            }
        }
        
        // Perform predictions.
        if (casesToPredict.size() > 0){
            dgAI2.DG_PredictionTriggerHandler.doPrediction(casesToPredict);
        }
    }

    // DigitalGenius: If a Case is being closed, call doFeedback, which will check if the DG Capture Analytics flag has
    // changed from False to True and if so will send pre-filled field values to the AI Platform for continuous learning
    // and analytics.
    if (Trigger.isBefore && Trigger.isUpdate){
        // Get the Id of the Servicing record type.
        Id servicingRTId = [SELECT Id FROM RecordType WHERE Name = 'Servicing' LIMIT 1].Id;

        // Get a list of Ids of the Cases passed to the trigger handler.
        List<Id> caseIds = new List<Id>(Trigger.newMap.keySet());

        // For each Case passed to the trigger handler, this structure has a list of its associated predictions.
        // A Case can have zero or more predictions.
        // To access a prediction index: Integer predictionIndex = predictionIndices[caseIndex][predictionNumber];
        // The predictionIndex can then be used to index into 'predictions' to access a prediction for the Case Id.
        // The order of Case Ids in 'predictionIndices' is the same as in 'caseIds'.
        List<List<Integer>> predictionIndices = new List<List<Integer>>();

        // A map of Case Ids to their indices in the 'caseIds' and 'predictionIndices' lists for efficient lookup.
        Map<Id, Integer> caseIdToCaseIndexMap = new Map<Id, Integer>();

        // Build 'predictionIndices' and 'caseIdToCaseIndexMap'.
        for (Integer caseIndex = 0; caseIndex < caseIds.size(); caseIndex++){
            predictionIndices.add(new List<Integer>());
            caseIdToCaseIndexMap.put(caseIds[caseIndex], caseIndex);
        }

        // Query for all predictions for all Cases passed to the trigger handler.
        List<dgAI2__DG_Prediction__c> predictions =
            [SELECT Id, dgAI2__Record_ID__c,
                 (SELECT Id, dgAI2__Integration_Status__c
                  FROM dgAI2__DG_Analytics__r
                  ORDER BY CreatedDate
                 )
             FROM dgAI2__DG_Prediction__c
             WHERE dgAI2__Record_ID__c IN :caseIds
             ORDER BY CreatedDate];

        // Update 'predictionIndices' with the index of each prediction for a Case. This allows a Case's child
        // predictions to be accessed. Needed because there is no Master-Detail or Lookup relationship between
        // Case and predictions as predictions can be attached to any Salesforce standard or custom object.
        for (Integer predictionIndex = 0; predictionIndex < predictions.size(); predictionIndex++){
            Integer caseIndex = caseIdToCaseIndexMap.get(predictions[predictionIndex].dgAI2__Record_ID__c);
            predictionIndices[caseIndex].add(predictionIndex);
        }

        // The Cases for which analytics will be sent.
        List<Case> casesToFeedback = new List<Case>();

        // Only send analytics for Cases that have a record type of 'Servicing', that are being closed and that have at
        // least one predicton and where the most recent prediction has not had analytics sent yet.
        for (Case thisCase : Trigger.new){
            // Check the Case Record Type and if the Case is being closed.
            if ((thisCase.RecordTypeId == servicingRTId) &&
                !String.isEmpty(thisCase.Status) &&
                (thisCase.Status.equals('Closed') || thisCase.Status.equals('Closed By KLM Social Administrator') || thisCase.Status.equals('Closed - No Response')) &&
                !thisCase.Status.equals(Trigger.oldMap.get(thisCase.Id).Status)){
                
                // Get the Case's index into the 'caseId' and 'predictionIndices' structures.
                Integer caseIndex = caseIdToCaseIndexMap.get(thisCase.Id);
    
                // How many predictions does this Case have?
                Integer numPredictions = predictionIndices[caseIndex].size();
    
                // Does this Case have any predictions?
                if (numPredictions > 0){

                    // Get the most recent prediction index.
                    Integer predictionIndex = predictionIndices[caseIndex][numPredictions - 1];
        
                    // Get the number of anayltics records for this prediction. Note that this value should be zero
                    // or one!
                    Integer numAnalytics = predictions[predictionIndex].dgAI2__DG_Analytics__r.size();
        
                    // If there are no analytics records for this prediction, no analytics have been sent for
                    // this prediction yet. Set the Capture Analytics flag and add the Case to the Cases to
                    // perform analytics on.
                    if (numAnalytics == 0){
                        thisCase.dgAI2__DG_Capture_Analytics__c = true;
                        casesToFeedback.add(thisCase);
                    }
                }
            }
        }

        // Perform feedback.
        if (casesToFeedback.size() > 0){
            dgAI2.DG_PredictionTriggerHandler.doFeedback(casesToFeedback);
        }
    }
}