/**********************************************************************
 Name:  FeedbackBeforeInsertUpdate
======================================================
Purpose: 

1. Trigger to populate Station_Manager_Email_Address__c field 
from the Station_Manager__c object using the Airport Code

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		Patrick Brinksma	25/09/2012		INITIAL DEVELOPMENT
	1.1		Patrick Brinksma	27/02/2013		Added Area and Subarea as criteria
***********************************************************************/	
trigger FeedbackBeforeInsertUpdate on feedback__c (before insert, before update) {
	
	// Retrieve a set of airport codes to retrieve email address station manager
	Set<String> airportCodeSet = new Set<String>();	
	for (feedback__c f : trigger.new){
		if (f.airportCode__c != null) airportCodeSet.add(f.airportCode__c);
	}
	// Retrieve all email addresses of station managers and put them into maps to quickly identify the correct one
	if (!airportCodeSet.isEmpty()){
		Map<String, String> airportCodeToEmailMap = new Map<String, String>();
		Map<String, String> airportCodeAreaToEmailMap = new Map<String, String>();
		Map<String, String> airportCodeAreaSubAreaToEmailMap = new Map<String, String>();
		List<Station_Manager__c> statManList = [SELECT Name, Area__c, Subarea__c, Station_Manager_Email_Address__c from Station_Manager__c WHERE Name IN :airportCodeSet];
		for (Station_Manager__c s : statManList){
			if (s.Name != null && s.Area__c == null && s.Subarea__c == null){
				airportCodeToEmailMap.put(s.Name, s.Station_Manager_Email_Address__c);
			} else if (s.Name != null && s.Area__c != null && s.Subarea__c == null){
				airportCodeAreaToEmailMap.put(s.Name + s.Area__c, s.Station_Manager_Email_Address__c);
			} else if (s.Name != null && s.Area__c != null && s.Subarea__c != null){			
				airportCodeAreaSubAreaToEmailMap.put(s.Name + s.Area__c + s.Subarea__c, s.Station_Manager_Email_Address__c);
			}
		}
		// Now we have all data, update feedback records
		for (feedback__c f : trigger.new){
			if (f.airportCode__c != null){
				// First try matching on Airport Code + Area + Subarea
				if (airportCodeAreaSubAreaToEmailMap.get(f.airportCode__c + f.area__c + f.subArea__c) != null){
					f.Station_Manager_Email_Address__c = airportCodeAreaSubAreaToEmailMap.get(f.airportCode__c + f.area__c + f.subArea__c);
				// otherwise match on Airport Code + Area
				} else if (airportCodeAreaToEmailMap.get(f.airportCode__c + f.area__c) != null){
					f.Station_Manager_Email_Address__c = airportCodeAreaToEmailMap.get(f.airportCode__c + f.area__c);
				// otherwise match on Airport Code
				} else if (airportCodeToEmailMap.get(f.airportCode__c) != null){
					f.Station_Manager_Email_Address__c = airportCodeToEmailMap.get(f.airportCode__c);
				// otherwise empty address
				} else {
					f.Station_Manager_Email_Address__c = null;
				}
			}
		}			
	}	
}