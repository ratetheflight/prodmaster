/**********************************************************************
 Name:  FeedbackToolCaseAfterInsertUpdate
======================================================
Purpose: 

Handle various status changes for a Feedback Tool Case

======================================================
History                                                            
-------                                                            
VERSION    		AUTHOR        	DATE      				DETAIL                                 
  1.0    	AJ van Kesteren    22/03/2013    	INITIAL DEVELOPMENT
  1.1		Sathish			   03/02/2017		Added conditions for too many SOQL issues
***********************************************************************/  
trigger FeedbackToolCaseAfterInsertUpdate on Case (after insert, after update) {
  System.debug('@@@ FeedbackToolCaseAfterInsertUpdate trigger started');  
  //1.1 Added for hot fix.ST-1549
  ID rt = Schema.SObjectType.case.getRecordTypeInfosByName().get('Customer Feedback Tool Case').getRecordTypeId();
  //System.debug('@@@ Retrieved record type: ' + rt.Name);
  //1.1 Added for hot fix.ST-1549
  	set<id>caserecid=new set<id>();
        for(case cs:Trigger.new)
        {
            if(rt!=NULL)
            {
              if(cs.RecordTypeId==rt)
              {
                caserecid.add(cs.id);
              }  
            }
            
        }
    //1.1 Added for hot fix.ST-1549
   if(caserecid.size()>0)
	{	
  		List<Case> cases = [select Id, Status, CaseNumber, RecordTypeId, PNR__c, Description, Owner.Email, Owner.Name,
                			Account.Id, Account.Flying_Blue_Number__c, Account.Official_First_Name__c, Account.Official_Last_Name__c
            				from Case 
            				where Id in :Trigger.newMap.keySet()];
  
  for (Case c : cases) {
    if (c.RecordTypeId == rt) {
      String oldStatus = null;
      if (Trigger.oldMap != null && Trigger.oldMap.get(c.Id) != null) {
        oldStatus = Trigger.oldMap.get(c.Id).Status;
      }
      String newStatus = c.Status;
        
      if (!newStatus.equals(oldStatus)) {
        if (newStatus == 'Sent to Customer Care') {
          AFKLM_CFT_CustomerCareEmailSender.sendEmail(c.Id);
        }
        
        if (newStatus == 'Waiting for Answer Social Media Hub') {
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setToAddresses(new List<String>{AFKLM_CFT_Emails__c.getValues('smh').email__c});
          mail.setSubject('Customer Feedback Tool: Please have a look at this case.');
          String s = 'Dear Social Media Hub,<br/><br/>';
          s += 'Please have a look at the case with number: ' + c.CaseNumber + '.<br/><br/>';
          s += 'Thank you very much!<br/><br/>';
          s += 'Kind regards,<br/>';
          s += 'The Customer Feedback Tool Team';
          mail.setHtmlBody(s);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        if (newStatus == 'Processed by Social Media Hub') {
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setToAddresses(new List<String>{c.Owner.Email});
          mail.setSubject('Customer Feedback Tool: Social Media Hub processed your Case.');
          String s = 'Dear Case Owner,<br/><br/>';
          s += 'Social Media Hub just processed case with number: ' + c.CaseNumber + '.<br/><br/>';
          s += 'Kind regards,<br/>';
          s += 'The Customer Feedback Tool Team';
          mail.setHtmlBody(s);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });          
        }
        
        if (newStatus == 'Waiting for Internal answer Flying Blue') {
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setToAddresses(new List<String>{AFKLM_CFT_Emails__c.getValues('fb').email__c});
          mail.setSubject('Customer Feedback Tool: Please have a look at this case.');
          String s = 'Dear Flying Blue Team,<br/><br/>';
          s += 'Please have a look at the case with number: ' + c.CaseNumber + '.<br/><br/>';
          s += 'Thank you very much!<br/><br/>';
          s += 'Kind regards,<br/>';
          s += 'The Customer Feedback Tool Team';
          mail.setHtmlBody(s);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        if (newStatus == 'Processed by Flying Blue Team') {
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setToAddresses(new List<String>{c.Owner.Email});
          mail.setSubject('Customer Feedback Tool: The Flying Bleu Team processed your Case.');
          String s = 'Dear Case Owner,<br/><br/>';
          s += 'The Flying Bleu Team just processed case with number: ' + c.CaseNumber + '.<br/><br/>';
          s += 'Kind regards,<br/>';
          s += 'The Customer Feedback Tool Team';
          mail.setHtmlBody(s);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });          
        }  
        
        if (newStatus == 'Sent to SIN Airport Management') {
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          mail.setToAddresses(new List<String>{AFKLM_CFT_Emails__c.getValues('airport_sin').email__c});
          mail.setSubject('Customer Feedback Tool: Please have a look at this case.');
          String s = 'Dear SIN Airport Manager,<br/><br/>';
          s += 'Please have a look at the case with number: ' + c.CaseNumber + '.<br/><br/>';
          s += 'Thank you very much!<br/><br/>';
          s += 'Kind regards,<br/>';
          s += c.Owner.Name;
          mail.setHtmlBody(s);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         }    
       }    
     }          
   }  
 } 
  System.debug('@@@ FeedbackToolCaseAfterInsertUpdate trigger completed'); 
}