/**********************************************************************
 Name:  RefundTrigger
======================================================
Purpose: Trigger on Refund to invoke Refund Request (future)
This should only be triggered if not in user interface
The status value 'Submitted' can only be set via the API
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    30/06/2014      Initial development
***********************************************************************/  
trigger RefundTrigger on Refund__c (after insert, after update) {

    if (!AFKLM_Utility.hasRunRefundTrigger){
        List<Id> listOfRefundId = new List<Id>();
        for (Refund__c thisRefund : Trigger.new){
            if (thisRefund.Status__c == 'Submitted'){
                listOfRefundId.add(thisRefund.Id);
            }
        }
		
        if (!listOfRefundId.isEmpty()){
            // Call future method to execute Refund Request
            bl_Refund.issueRefundRequestsAsynch(listOfRefundId);
        }
        
        // Prevent recursive trigger
        AFKLM_Utility.hasRunRefundTrigger = true;
    }
}