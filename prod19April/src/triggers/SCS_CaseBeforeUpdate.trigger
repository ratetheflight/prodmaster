/********************************************************************** 
 Name:  SCS_CaseBeforeUpdate
====================================================== 
Purpose: Updates the stage datetime when the Status has been changed.

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      24/01/2014      Initial Development
    1.1     Stevano Cheung      11/02/2014      Added recordtype for SCS servicing
    1.2     Sai Choudhry        02/03/2017      Removing the SOQL query for Record Type, JIRA ST-1868.
	1.3	 	Sathish         	21/03/2017		ST-1970 Workflow Process builder Changes
***********************************************************************/
trigger SCS_CaseBeforeUpdate on Case (before update) {
    //Removing the SOQL query for Record Type, JIRA ST-1868.
    //Start
    //RecordType rt = [select Id, Name from RecordType where Name = 'Servicing' and SobjectType = 'Case' limit 1];
    //RecordType rtAF = [select Id, Name from RecordType where Name = 'AF Servicing' and SobjectType = 'Case' limit 1];
    //AFKLM_SCS_CaseRecordTypeSingleton crt = AFKLM_SCS_CaseRecordTypeSingleton.getInstance();
    Id klmRtId= Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
    Id afRtId = Schema.SObjectType.case.getRecordTypeInfosByName().get('AF Servicing').getRecordTypeId();
    //End
   // System.debug('@@@ Retrieved record type: ' + crt.klmRt.Name + ' AirFrance: '+crt.afRt.Name);
   
    
    for(Case cs: Trigger.new) {
        if (cs.RecordTypeId == klmRtId || cs.RecordTypeId == afRtId) {
            System.debug('@@@ Current case is servicing case');
            cs.Case_stage_datetime__c = Datetime.now();
            //fix for twitter survey sent for status Closed No response
        if ((cs.Origin == 'Twitter') && (cs.Status == 'Closed - No Response' || cs.Status == 'Closed By KLM Social Administrator') ){
               //  System.debug('@@@ Case is closed no response' +cs.Twitter_Survey_Stopper_On_Batch_Close__c);
            cs.Twitter_Survey_Stopper_On_Batch_Close__c = 'YES';
            }
             if ((cs.Origin == 'Twitter') && (cs.Twitter_Survey_Stopper_On_Batch_Close__c == 'YES') && (cs.Status == 'Closed') ){
               //  System.debug('@@@ Case is closed no response' +cs.Twitter_Survey_Stopper_On_Batch_Close__c);
            cs.Twitter_Survey_Stopper_On_Batch_Close__c = '';
            }
            // Added by Sathish for ST-1970 -Starts
            Case oldcaserecord=trigger.oldmap.get(cs.id);
            if(cs.Status!=oldcaserecord.Status && (oldcaserecord.Status=='Closed' || oldcaserecord.Status=='Closed - No Response' || oldcaserecord.Status=='Sent to Customer Care'))
            { 
                cs.Date_Reopened__c=system.now();
            }
            if(cs.Urgency_Tracking__c==FALSE && (cs.Is_Urgent__c || cs.Proposed_Urgent__c) &&(cs.RecordTypeId==klmRtId && cs.Status!='Closed' && cs.Status!='Closed - No Response' && cs.Status!='Closed By KLM Social Administrator'))
            {
                cs.Urgency_Tracking__c=TRUE;
            }
            if(cs.RecordTypeId==klmRtId && (cs.Is_Urgent__c || cs.Proposed_Urgent__c) &&( cs.Status=='Closed' || cs.Status=='Closed - No Response' || cs.Status=='Closed By KLM Social Administrator'))
            {
                cs.Proposed_Urgent__c=FALSE;
                cs.Is_Urgent__c=FALSE;
            }
            //Ends
            
       }
   }
   
}