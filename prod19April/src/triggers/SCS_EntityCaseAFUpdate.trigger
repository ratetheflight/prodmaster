/*************************************************************************************************
* File Name     :   SCS_EntityCaseAFUpdat
* Description   :   
* @author       :   
* Modification Log
===================================================================================================
* Ver.    Date            Author                      Modification
*--------------------------------------------------------------------------------------------------
* 1.0    25/2/2015         Robert Stankiewicz         Created the trigger
* 1.1    15/12/2016        Sai Choudhry               Modified to bypass outbound messages. JIRA ST-1311.

****************************************************************************************************/

trigger SCS_EntityCaseAFUpdate on SocialPost (after update) {
    Public Static boolean onAfterUpdate_FirstRun = True;
    Boolean triggerexecution=FALSE;
    if(!test.isRunningTest())    
    {
       AFKLM_Trigger_Execution__c trgexecution=AFKLM_Trigger_Execution__c.getValues('Social Post');
     if(trgexecution!=NULL)
        triggerexecution=trgexecution.Run_Old_Trigger__c;
    }
    else
    {
       triggerexecution=TRUE; 
    }
    
  if(triggerexecution){ 
    If(onAfterUpdate_FirstRun )
    {
    onAfterUpdate_FirstRun = False;
    Map<Id, String> parentCaseIds = new Map<Id, String>();
    //Added by Sai.
    List<id> vipPostParentId = new List<id>();
    List<case> casesToBeUpdated = new List<case>();

    for(SocialPost sp : Trigger.new) {
        if (sp.ParentId != null) {
            //Added additional if condition to bypass outbound messages.JIRA ST-1311.
            if('AirFrance'.equals(sp.Company__c) && sp.IsOutbound == False) {
                parentCaseIds.put(sp.ParentId, sp.Entity__c);
            }
        }
    }
    
    if (parentCaseIds.size() > 0) {
        List<Case> parentCases = [Select c.Id, c.Entity__c
                                    From Case c
                                    Where c.Id in :parentCaseIds.keySet()];
        List<Case> parentCasesToUpdate = new List<Case>();

        for (Case parent: parentCases) {
        //Added an if condtion to avoid recurrsive updates to entity field. JIRA ST-1311.
            if(parent.Entity__c !=parentCaseIds.get(parent.Id))
            {
                parent.Entity__c = parentCaseIds.get(parent.Id);
                parentCasesToUpdate.add(parent);  
            }
        }
        
        //Bulk update
        if (parentCasesToUpdate.size()>0) {
            update parentCasesToUpdate;
        }
    }
    
    //Added by Sai.
    //Logic to Mark the social post from VIP and Ambassador as Urgent. ST-853.
    for(SocialPost newPost: Trigger.new)
    {
        
        if(!newPost.Is_Urgent__c && !newPost.Proposed_Urgent__c && newPost.TopicProfileName == 'KLM' && (newPost.Language__c == 'English' || newPost.Language__c == 'Dutch'))
        {
            if((newPost.VIP__c || newPost.Ambassador__c) && !(newPost.IsOutbound) && (newPost.MessageType == 'Private') && (newPost.Provider == 'Facebook'))
            {
                vipPostParentId.add(newPost.parentId);
            }
        }
    }
    
    if(!vipPostParentId.isEmpty())
    {
        for(Case cs:[Select id,is_urgent__C from case where id IN :vipPostParentId])
        {
            cs.is_urgent__c = True;
            casesToBeUpdated.add(cs);
        }
    }
    if(!casesToBeUpdated.isEmpty())
    {
        Database.SaveResult[] results =Database.Update(casesToBeUpdated,false);
        
        for(Integer i=0;i<results.size();i++)
        {
            if (!results.get(i).isSuccess())
            {
                Database.Error err = results.get(i).getErrors().get(0);
                System.debug('Error - '+err.getMessage() + '\nStatus Code : '+err.getStatusCode()+'\n Fields : '+err.getFields());
            }
        }
        casesToBeUpdated.clear();
    }
    //end
    }
  }
}