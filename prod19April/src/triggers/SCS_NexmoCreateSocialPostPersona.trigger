/********************************************************************** 
 Name:    SCS_NexmoCreateSocialPostPersona
 Task:    N/A
 Runs on: SCS_PreviousCases
====================================================== 
Purpose: 
    Trigger to handle messages from Heroku platform, both Messenger and WeChat
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta
            Robert Stankiewicz  10/10/2015      Initial Development, WeChat
    1.1     Ivan Botta
            Robert Stankiewicz  22/12/2015      Messenger logic included
    1.2     Nagavi Babu         09/05/2016      Assign owner to the social post - Added by Nagavi 
                                                on 09/05/2016 as part of ticket - ST -002742
    1.3    Sai Choudhry         01/08/2016      Mapping the new field "Automated_Message_Type__c". Jira ST-563
    1.4    Sai Choudhry         19/08/2016      Added logic for Inbound and Outbound AF WeChat Messages. JIRA ST-438
    1.5    Sathish              06/02/2017      Added condition to limit the execution of trigger by user level.ST-1713
    1.6    Pushkar              06/02/2017      Changes for 'Mark as Reviewed Auto' for Customer 'Thanks' messages -- ST-1649
    1.7    Nagavi               22/03/2017      ST-1638:Development - Agent Status on Post - when replied agent status should be replied
***********************************************************************/
trigger SCS_NexmoCreateSocialPostPersona on Nexmo_Post__c (after insert) {
    
    //1.5 
    //AFKLM_SCS_UserSkipValidationSingleton Usr = AFKLM_SCS_UserSkipValidationSingleton.getInstance();
    private SocialPost newSocialPost;
    private SocialPersona persona;

    //**********************WeChat lists*******************************
    private List<SocialPersona> weChatPersonas {
        get {

            if(weChatPersonas == NULL) {
                weChatPersonas = new List<SocialPersona>();
            }

            return weChatPersonas;
        } 
        set;}

    private List<SocialPost> inboundWeChatPosts {
        get {

            if(inboundWeChatPosts == NULL) {
                inboundWeChatPosts = new List<SocialPost>();
            }

            return inboundWeChatPosts;
        }
        set;}

    private List<SocialPost> outboundWeChatPosts {
        get {

            if(outboundWeChatPosts == NULL) {
                outboundWeChatPosts = new List<SocialPost>();
            }

            return outboundWeChatPosts;
        }
        set;}

    //******************Facebook Messenger lists*************************
    private List<SocialPersona> messengerPersonas {
        get {

            if(messengerPersonas == NULL) {
                messengerPersonas = new List<SocialPersona>();
            }

            return messengerPersonas;
        }
        set;}

    private List<SocialPost> inboundMessengerPosts {
        get {

            if(inboundMessengerPosts == NULL) {
                inboundMessengerPosts = new List<SocialPost>();
            }

            return inboundMessengerPosts;
        }
        set;}

    private List<SocialPost> outboundMessengerPosts {
        get {

            if(outboundMessengerPosts == NULL) {
                outboundMessengerPosts = new List<SocialPost>();
            }

            return outboundMessengerPosts;
        }
        set;}
     
    private List<String> externalIdsList {
        get {

            if(externalIdsList == NULL) {
                externalIdsList = new List<String>();
            }

            return externalIdsList;
        } 
        set;}

    private Set<String> topicFBSet = new Set<String>();

    // this may be not needed anymore - depends if wechat always provides a right picture
    private String picUrl = '/resource/SocialCustomerServiceResources/SocialCustomerServiceResources/img/noface.gif';

    AFKLM_SCS_WeChatSettings__c custSettings = AFKLM_SCS_WeChatSettings__c.getValues('Settings');

    // to remove duplicates from the list
    Set<SocialPersona> weChatPersonasTmpSet = new Set<SocialPersona>();
    List<SocialPersona> weChatPersonasTmpList = new List<SocialPersona>();

    //FB Messenger
    Set<SocialPersona> messengerPersonasTmpSet = new Set<SocialPersona>();
    List<SocialPersona> messengerPersonasTmpList = new List<SocialPersona>();
    //ST-1649
    Customer_Response_Case_Open_Settings__mdt cmdt = [select id,Customer_Response__c from Customer_Response_Case_Open_Settings__mdt where DeveloperName = 'Customer_Response'];

//1.5
  //if(Usr.getUsr.size() > 0) {
   //if(Usr.getUsr[0].Run_Nexmopost__c) {
    for(Nexmo_Post__c nexmoPost : Trigger.new){

        //System.debug('--+ nexmo post: '+nexmoPost);

        // don't create anything for events
        if(!'event'.equals(nexmoPost.Type__c)) {

            // Create Social Post - Outbound path
            if('Outbound'.equals(nexmoPost.Source__c)) {

                //Added the status='Sent' and removed the agent status field setting as part of ST-1638 - Nagavi
                // CREATE SOCIAL POST
                newSocialPost = new SocialPost(                    
                    Content = nexmoPost.Text__c,
                    Status = 'Sent',
                    Posted = nexmoPost.Send_Time__c,
                    IsOutbound = true,
                    Provider = nexmoPost.Social_Network__c,
                    chatHash__c = nexmoPost.Chat_Hash__c,
                    Automated_PAX_notification__c=nexmoPost.Automated_Message_Type__c
                    );
                    
                 if (nexmoPost.Chat_Medium__c != null)
                 {
                       newSocialPost.Chat_Medium__c = nexmoPost.Chat_Medium__c;
                 }
                          
                //The newly created field in Nexmopost and social Post objects are mapped in the above code.Jira ST-563

                //Assign owner to the social post - Added by Nagavi on 09/05/2016 as part of ticket - ST -002742
                    try{
                        Id ownerId;
                        if(nexmoPost.AgentId__c <> null && !nexmoPost.AgentId__c.equals('') && nexmoPost.AgentId__c.startswith('005')){
                            ownerId = Id.valueOf(nexmoPost.AgentId__c);
                        }else{
                            ownerId = UserInfo.getUserId();
                        }
                        newSocialPost.OwnerId  = ownerId;         
                    }catch(Exception s){
                        System.debug(s);
                    }
                // CREATE SOCIAL PERSONA
                //added by sai--Added logic for Inbound and Outbound AF WeChat Messages. JIRA ST-438
                //start
                
                persona = new SocialPersona(
                    ParentId = custSettings.SCS_SysAccountId__c,         
                    Provider = nexmoPost.Social_Network__c
                    );
                // UPDATE VALUES BASED ON SOCIAL NETWORK
                                                                          
                If(nexmoPost.Company__c=='AirFrance')
                {
                     persona.Name = custSettings.SCS_AFSysPersonaName__c;
                     persona.RealName = custSettings.SCS_AFSysPersonaName__c;
                     persona.ExternalId = custSettings.SCS_AFSysPersonaExternalId__c;
                    if('WeChat'.equals(nexmoPost.Social_Network__c))
                    {
                        // for WeChat externalPostId = Chat_Hash__c
                        newSocialPost.Name = 'Message from: '+custSettings.SCS_SysAccountName_del__c;
                        newSocialPost.ExternalPostId = nexmoPost.Chat_Hash__c;
                        newSocialPost.Handle = custSettings.SCS_AFSysPersonaName__c;
                        newSocialPost.WhoId = custSettings.SCS_SysAccountId__c;
                        //ST-002851
                        newSocialPost.Company__c = nexmoPost.Company__c;
                        //ST-002851
                        outboundWeChatPosts.add( newSocialPost );
                    }
                                                                                          
                } 
                //end                  
                else
                {
                    persona.Name = custSettings.SCS_SysPersonaName__c;
                    persona.RealName = custSettings.SCS_SysPersonaName__c;
                    persona.ExternalId = custSettings.SCS_SysPersonaExternalId__c;
                // UPDATE VALUES BASED ON SOCIAL NETWORK
                    if('WeChat'.equals(nexmoPost.Social_Network__c))
                    {

                    // for WeChat externalPostId = Chat_Hash__c
                    newSocialPost.Name = 'Message from: '+custSettings.SCS_SysAccountName_del__c;

                    newSocialPost.ExternalPostId = nexmoPost.Chat_Hash__c;
                    newSocialPost.Handle = custSettings.SCS_SysPersonaName__c;
                    newSocialPost.WhoId = custSettings.SCS_SysAccountId__c;


                    //ST-002851
                    //added by sai -Added logic for Inbound and Outbound AF WeChat Messages. JIRA ST-438
                    newSocialPost.Company__c = nexmoPost.Company__c;
                    //ST-002851
                    outboundWeChatPosts.add( newSocialPost );
                    }
                if('Facebook'.equals(nexmoPost.Social_Network__c)) {
                    //persona
                    persona.Name = persona.RealName = 'KLM';
                    // Messenger - real (Facebook) Messge ID
                    newSocialPost.Name = 'Message from: KLM';
                    newSocialPost.Language = nexmoPost.Language__c;                 
                    newSocialPost.Company__c = nexmoPost.Company__c;
                    newSocialPost.ExternalPostId = nexmoPost.Chat_Hash__c;
                    newSocialPost.ExternalPostId__c = nexmoPost.Message_Id__c;
                    newSocialPost.TopicProfileName = nexmoPost.TopicProfileName__c;
                    newSocialPost.MessageType = 'Private';
                    newSocialPost.Handle = 'KLM';
                    newSocialPost.Heroku_Message__c = TRUE;
                    

                    outboundMessengerPosts.add( newSocialPost );
                    System.debug('***'+outboundMessengerPosts);
                    /*try{
                        Id ownerId;
                        if(nexmoPost.AgentId__c <> null && !nexmoPost.AgentId__c.equals('') && nexmoPost.AgentId__c.startswith('005')){
                            ownerId = Id.valueOf(nexmoPost.AgentId__c);
                        }else{
                            ownerId = UserInfo.getUserId();
                        }
                        newSocialPost.OwnerId  = ownerId;

                        outboundMessengerPosts.add( newSocialPost );                 
                    }catch(Exception s){
                        System.debug(s);

                    }*/
                    
                }
                }
            } else {

                //Part for the Inbound Social Post
                // CREATE SOCIAL POST

                newSocialPost = new SocialPost(
                    Name = 'Message from: '+nexmoPost.User_Real_Name__c,
                    Handle = nexmoPost.User_Real_Name__c,
                    SCS_Status__c = 'New',
                    Posted = nexmoPost.Send_Time__c,
                    IsOutbound = false,
                    Provider = nexmoPost.Social_Network__c,
                    ExternalPostId = nexmoPost.Chat_Hash__c,
                    chatHash__c = nexmoPost.Chat_Hash__c);
                
                if (nexmoPost.Chat_Medium__c != null)
                 {
                       newSocialPost.Chat_Medium__c = nexmoPost.Chat_Medium__c;
                 }    
       
                // CREATE SOCIAL PERSONA
                persona = new SocialPersona(
                    Name = nexmoPost.User_Real_Name__c,
                    Provider = nexmoPost.Social_Network__c,
                    RealName = nexmoPost.User_Real_Name__c,
                    ExternalPictureURL = nexmoPost.ProfileIconUrl__c,
                    ExternalId = nexmoPost.From_User_Id__c);
                system.debug('nexmopost.From_User_Id__c'+nexmoPost.From_User_Id__c.replace('ott:fbmessenger:',''));
                // Handle different types of messages
                if('image'.equals(nexmoPost.Type__c)) {

                    newSocialPost.Content = 'Photo message from '+nexmoPost.User_Real_Name__c;
                    newSocialPost.AttachmentUrl = nexmoPost.Text__c;
                } else if ('audio'.equals(nexmoPost.Type__c) || 'document'.equals(nexmoPost.Type__c)) {

                    newSocialPost.Content = 'Message from '+nexmoPost.User_Real_Name__c+' with attachment(s)';
                    if (nexmoPost.Text__c.length() <= 255) {
                        
                        newSocialPost.AttachmentUrl = nexmoPost.Text__c;
                    } else {
                        newSocialPost.Content = newSocialPost.Content + ': ' + nexmoPost.Text__c;
                    } 
                } else {
                            
                    newSocialPost.Content = nexmoPost.Text__c;
                }
                System.debug('ST-1649'+newSocialPost.Content);
                //ST-1649 start
                if(!Test.isRunningTest()){
                try{
                if(nexmoPost.Type__c == 'text' && 'Facebook'.equals(nexmoPost.Social_Network__c)){
                    String message = newSocialPost.Content;
                    String regex = '[^a-zA-Z0-9]';
                    Pattern regexPattern = Pattern.compile(regex);
                    Matcher regexMatcher = regexPattern.matcher(message);
                    String replacedMessage;
                    String [] finalMsg = new String []{};
                    if(regexMatcher.find()) {
                       finalMsg = ((message.replaceAll( '\\s+', '')).replaceAll(regex, '|')).split('\\|',2) ;
                       replacedMessage = finalMsg[0];
                    }else{
                       replacedMessage =  message.replaceAll( '\\s+', '');
                       finalMsg.add(replacedMessage);
                       
                    }
                    System.debug('ST-1649'+replacedMessage);
                    System.debug('ST-1649'+nexmoPost.TopicProfileName__c);
 
                    if(replacedMessage !='' && nexmoPost.TopicProfileName__c != null){
                        if(finalMsg.size()>1){
                            if((cmdt.Customer_Response__c).contains(replacedMessage.toUpperCase()) && finalMsg[1].length()<=5 && nexmoPost.TopicProfileName__c.contains('KLM'))
                                newSocialPost.SCS_Status__c = 'Mark As Reviewed Auto';      
                        }else{ 
                            if((cmdt.Customer_Response__c).contains(replacedMessage.toUpperCase()) && nexmoPost.TopicProfileName__c.contains('KLM'))
                                newSocialPost.SCS_Status__c = 'Mark As Reviewed Auto';
                        }
                    }
                    System.debug('ST-1649'+nexmoPost.Type__c);
                }
                }catch(Exception e){}
                }
                //ST-1649 end 
                // UDPATE VALUES BASED ON SOCIAL NETWORK VALUE
                if('WeChat'.equals(nexmoPost.Social_Network__c)){

                    //newSocialPost.Content = nexmoPost.Text__c;
                    ////ST-002851
                    //added by sai--Added logic for Inbound and Outbound AF WeChat Messages. JIRA ST-438
                    newSocialPost.Company__c = nexmoPost.Company__c;
                    inboundWeChatPosts.add( newSocialPost );
                }        

                if('Facebook'.equals(nexmoPost.Social_Network__c)) {
                    //persona
                    persona.ProfileUrl = nexmoPost.ProfileUrl__c; 

                    newSocialPost.Language__c = nexmoPost.Language__c;
                    newSocialPost.PostTags = nexmoPost.Language__c;
                    newSocialPost.Company__c = nexmoPost.Company__c;
                    newSocialPost.ExternalPostId__c = nexmoPost.Message_Id__c;
                    newSocialPost.TopicProfileName = nexmoPost.TopicProfileName__c;
                    newSocialPost.Notified__c = nexmoPost.Notified__c; // automated message
                    newSocialPost.WA_Notified__c = nexmoPost.WA_Notified__c; // wheresapp
                    newSocialPost.MessageType = 'Private';
                    newSocialPost.Heroku_Message__c = TRUE;                    
                    inboundMessengerPosts.add( newSocialPost );

                    topicFBSet.add(nexmoPost.TopicProfileName__c);

                    // to check duplicates
                    externalIdsList.add( newSocialPost.ExternalPostId__c );
                }
            }

            //personas.add(persona);
            if('WeChat'.equals(nexmoPost.Social_Network__c)){
                
                weChatPersonasTmpList.add(persona);
            } 

            if('Facebook'.equals(nexmoPost.Social_Network__c)) {

                messengerPersonasTmpList.add(persona);
            }
        }
    }

    if(topicFBSet.size() > 0) {
        List<String> topicFBList = new List<String>(topicFBSet);
        //System.debug('--+ topicFBList: '+topicFBList);

        List<MessengerPostTag__c> postTags = [SELECT id, Name, Language__c, PostTag__c FROM MessengerPostTag__c WHERE Name IN: topicFBList];
        //System.debug('--+ postTags: '+postTags);

        for(SocialPost sp : inboundMessengerPosts) {

            for(MessengerPostTag__c pt : postTags) {

                if(sp.TopicProfileName == pt.Name &&
                    sp.Language__c == pt.Language__c) {

                    sp.postTags = pt.PostTag__c;
                    //sp.Language__c=''; making the Language field is empty for avoiding the recursive WF updates - ST-002996
                    break;
                    
                } else if (sp.TopicProfileName == pt.Name &&
                    sp.Language__c != pt.Language__c && (pt.Language__c=='' || pt.Language__c==NULL)) {

                    sp.postTags = pt.PostTag__c;
                    //sp.Language__c=''; making the Language field is empty for avoiding the recursive WF updates - ST-002996
                    break;
                }
            }
        }
    }

    //******************** WeChat *******************************
    // remove all duplicates
    //DvtH23JULY2015 TODO if pesona is directly set in the set instead of the list than it saves a copy from list to set!
    weChatPersonasTmpSet.addAll( weChatPersonasTmpList );
    weChatPersonas.addAll( weChatPersonasTmpSet );
    //System.debug('--+ wechatPersonas: '+weChatPersonas);

    if(!inboundWeChatPosts.isEmpty() || !outboundWeChatPosts.isEmpty()) {

        AFKLM_SCS_InboundWeChatHandlerImpl wechatHandler = new AFKLM_SCS_InboundWeChatHandlerImpl();
        if(!inboundWeChatPosts.isEmpty()){
            wechatHandler.handleInboundSocialPost(inboundWeChatPosts,weChatPersonas);
        }

        if(!outboundWeChatPosts.isEmpty()){
            wechatHandler.handleInboundSocialPost(outboundWeChatPosts,weChatPersonas);
        }
    }

    //************************** Facebook Messenger *******************************
    messengerPersonasTmpSet.addAll( messengerPersonasTmpList );
    messengerPersonas.addAll( messengerPersonasTmpSet );
    //System.debug('--+ messengerPeronas: '+messengerPersonas);

    if(!inboundMessengerPosts.isEmpty() || !outboundMessengerPosts.isEmpty()) {

        List<SocialPost> allMessengerPosts = new List<SocialPost>();


        AFKLM_SCS_InboundMessengerHandlerImpl messengerHandler = new AFKLM_SCS_InboundMessengerHandlerImpl();
        if(!inboundMessengerPosts.isEmpty()){    
            allMessengerPosts.addAll(inboundMessengerPosts);
            //messengerHandler.handleInboundSocialPost(inboundMessengerPosts,messengerPersonas,externalIdsList);
        } 

        if(!outboundMessengerPosts.isEmpty()){
            allMessengerPosts.addAll(outboundMessengerPosts);
            //messengerHandler.handleInboundSocialPost(outboundMessengerPosts,messengerPersonas,externalIdsList);
        }
        
        messengerHandler.handleInboundSocialPost(allMessengerPosts,messengerPersonas,externalIdsList);
    }
  //} 
 //}
}