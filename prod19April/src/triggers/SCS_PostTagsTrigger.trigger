trigger SCS_PostTagsTrigger on SocialPost (before insert) {
    
    Boolean triggerexecution=FALSE;
    if(!test.isRunningTest())    
    {
       AFKLM_Trigger_Execution__c trgexecution=AFKLM_Trigger_Execution__c.getValues('Social Post');
     if(trgexecution!=NULL)
        triggerexecution=trgexecution.Run_Old_Trigger__c;
    }
    else
    {
       triggerexecution=TRUE; 
    }
    
  if(triggerexecution){ 
    for(SocialPost sp : Trigger.new){
        sp.SCS_Post_Tags__c = sp.PostTags;
    }
  }
}