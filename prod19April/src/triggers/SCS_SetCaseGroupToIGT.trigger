/********************************************************************** 
 Name:  SCS_SetCaseGroupToIGT 
 Task:    N/A
 
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0                                         Initial Development
    1.1     Pushkar          30/01/2017         A new agent status 'Engaged' in place of 'Mark as reviewed'. JIRA ST-1263
    1.2     Sathish          09/02/2017         Update Case status for flying blue records when a inbound post created.JIRA ST-1548
***********************************************************************/
trigger SCS_SetCaseGroupToIGT on SocialPost (after update) {
    
    SocialPostTriggerHandler trghandler=new SocialPostTriggerHandler();
    Map<Id,Case> updateCases = new Map<Id,Case>();

    Map<Id,SocialPost> updateSocialPosts = new Map<Id,SocialPost>();
    List<SocialPost> spToUpdate = new List<SocialPost>();

    //List<String> topicProfileNames = new List<String>{'KLM Canada','KLM Ireland','KLM USA','KLM UK','KLM Malaysia','KLM Indonesia','KLM Singapore','KLM South Africa','KLM Hong Kong','KLM China','KLM Switzerland'};
    Map<String,SocialPost> socialPostWithCases = new Map<String,SocialPost>();
    List<Id> usersToCheck = new List<Id>();
    Boolean triggerexecution=FALSE;
    if(!test.isRunningTest())    
    {
       AFKLM_Trigger_Execution__c trgexecution=AFKLM_Trigger_Execution__c.getValues('Social Post');
     if(trgexecution!=NULL)
        triggerexecution=trgexecution.Run_Old_Trigger__c;
    }
    else
    {
       triggerexecution=TRUE; 
    }
    
  if(triggerexecution){ 
    for(SocialPost spost : Trigger.new){
    
        socialPostWithCases.put(spost.ParentId,spost);

    }

    List<Case> cases = [SELECT id, Group__c FROM Case WHERE Id IN: socialPostWithCases.keySet()];

    for(SocialPost sp : socialPostWithCases.values()){
        //IBO:For the future 'IGT Spanish' needs to be added to IF statement
        if(sp.ParentId != null && ('IGT English'.equals(sp.SCS_Post_Tags__c) || 'IGT Spanish'.equals(sp.SCS_Post_Tags__c))) {
            
            for(Case cs : cases){
                if(cs.id == sp.ParentId){
                    cs.Group__c = sp.Group__c;

                    if( !updateCases.containsKey(cs.id) ){
                        updateCases.put(cs.id,cs); 
                    }
                }
            }
            

            /*if( !cs.isEmpty() ){
                cs[0].Group__c = sp.Group__c;
                
                if( !updateCases.containsKey(cs[0].id) ){
                    updateCases.put(cs[0].id,cs[0]); 
                }
            }*/
        }

        if('Unsupported'.equals(sp.Language__c) && 
            ('Actioned'.equals(sp.SCS_Status__c) || 'Mark as reviewed'.equals(sp.SCS_Status__c) || 'Engaged'.equals(sp.SCS_Status__c)) &&
                (sp.Group__c == null || sp.Group__c == '')) {

            updateSocialPosts.put(sp.Id, sp);
            usersToCheck.add(sp.OwnerId);
        }
    }

    // fix for ST-001137
    if( !usersToCheck.isEmpty() ){

        Map<Id, User> owners = new Map<Id,User>([SELECT Id, Division FROM User WHERE id IN : usersToCheck]);

        if( !owners.isEmpty() ){

            for(SocialPost sptu : updateSocialPosts.values()) {

                if( owners.containsKey( sptu.OwnerId ) ) {

                    User tmpUser = owners.get(sptu.OwnerId);

                    // Only this values allowed: Cygnific, IGT, TWC
                    if( 'Cygnific'.equals(tmpUser.Division) || 
                        'IGT'.equals(tmpUser.Division) ||
                            'CX'.equals(tmpUser.Division)) {

                        SocialPost newsp = new SocialPost(Id = sptu.Id);

                        newsp.Group__c = tmpUser.Division;
                        spToUpdate.add(newsp);
                    }
                }
            }
        }
    }

    if( !spToUpdate.isEmpty() ){
        update spToUpdate;
    }
   
    if( !updateCases.isEmpty() ){
        update updateCases.values();
    }
    //1.2 Starts ST-1548
            list<socialpost> newpostrecs=new list<socialpost>();
            for(socialpost sp:trigger.new)
            {
                Socialpost oldpost=trigger.oldmap.get(sp.Id);
                if(sp.ParentId!=NULL && sp.ParentId!=oldpost.ParentId && sp.FlyingBlue__c==TRUE && sp.IsOutbound==FALSE)
                {
                    newpostrecs.add(sp);
                }
            }
            if(newpostrecs.size()>0)
            {
               trghandler.updatecasestatusforflyingblue(newpostrecs);
            }
    //1.2 ends
  }
}