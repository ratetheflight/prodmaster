/********************************************************************** 
 Name:  SCS_SetEliteValue 
====================================================== 
Purpose: 

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0                                         Initial Development
    1.1     Manuel                              Kloutscore
    1.2     Ata                9/06/2016        Copy Influencer score on social Post
    1.3     Ata                1/08/2016        filter out criteria added while creating SP list 
                                                before calling future method on social post
    1.4     Ata                08/08/2016      Added tier level data n booking class logic   
    1.5    Sai Choudhry        02/02/2017        Bulkification of tier level update.JIRA ST-1682      
***********************************************************************/
trigger SCS_SetEliteValue on SocialPost (before insert) {
    
    Map<Id, SocialPost> socialPostIds = new Map<Id, SocialPost>();

    //Manuel Conde: AF Segmentation
    Set<Id> listOfAccForKloutScoreUpd = new Set<Id>();
    List<String> listOfPostsForKloutScoreUpd = new List<String>();
    //added by Sai.Bulkification of tier level update.JIRA ST-1682 
    List<socialpost> toBeUpdatedPosts = new List<socialpost>();

    Boolean triggerexecution=FALSE;
    if(!test.isRunningTest())    
    {
       AFKLM_Trigger_Execution__c trgexecution=AFKLM_Trigger_Execution__c.getValues('Social Post');
     if(trgexecution!=NULL)
        triggerexecution=trgexecution.Run_Old_Trigger__c;
    }
    else
    {
       triggerexecution=TRUE; 
    }
    
  if(triggerexecution){ 
    for(SocialPost sp : Trigger.new) {
        
        if (sp.whoId != null) {
            socialPostIds.put(sp.whoId, sp);
            //added by Sai.Bulkification of tier level update.JIRA ST-1682 
            toBeUpdatedPosts.add(sp);
        }else{
                // filtering only AirFrance twitter posts here
              if(sp.provider != Null && sp.TopicProfileName != Null)  
                if(sp.provider.toLowerCase().contains('twitter') && sp.TopicProfileName.toLowerCase().contains('airfrance'))
                    listOfPostsForKloutScoreUpd.add(sp.handle);
        }
    }   

    if (socialPostIds.size() > 0) {
        List<Account> parentAccount = [Select Id, 
                                              Elite_non_elite__c, 
                                              Influencer__c, 
                                              VIP__pc, 
                                              Ambassador__c, 
                                              Klout_score__c,
                                              Influencer_Score__c,
                                              Tier_Level__c,
                                              Booking_Class__c
                                              From Account a
                                            Where a.Id in :socialPostIds.keySet()];
        
        for (Account accountToProcess: parentAccount) {
            for(socialpost sp: toBeUpdatedPosts) //added by Sai.Bulkification of tier level update.JIRA ST-1682 
                {
                    if(sp.whoid == accountToProcess.id) //added by Sai.Bulkification of tier level update.JIRA ST-1682 
                        {
                            sp.Elite__c = accountToProcess.Elite_non_elite__c; 
                            sp.Influencers_SP__c = accountToProcess.Influencer__c;
                            sp.Vip__c = accountToProcess.VIP__pc; 
                            sp.Ambassador__c = accountToProcess.Ambassador__c; 
                            //Added By Ata-ST-583,ST-580
                            sp.Tier_Level__c = accountToProcess.Tier_Level__c;
                            sp.Booking_Class__c = accountToProcess.Booking_Class__c;
                            sp.Influencer_Score__c = accountToProcess.Influencer_Score__c;
                            //Manuel Conde: AF Segmentation
                            if(accountToProcess.Klout_score__c <> null){ 
                                sp.Klout_score__c = accountToProcess.Klout_score__c; 
                                }
                            else{
                                listOfAccForKloutScoreUpd.add(accountToProcess.Id);
                                }
                        }
            }
        }
    }

    
    //Manuel Conde: AF Segmentation
    if(!listOfAccForKloutScoreUpd.isEmpty() || !listOfPostsForKloutScoreUpd.isEmpty()){
        FutureMethodRecordProcessing.retrieveUpdateKloutScore(listOfAccForKloutScoreUpd, listOfPostsForKloutScoreUpd);
    }
  }
}