trigger SCS_SetPostcardOnCase on FeedItem (after insert) {
    List<Case> queriedCases = new List<Case>();
    List<Case> casesToUpdate = new List<Case>();
    List<String> fiList = new List<String>();

    for(FeedItem fi : Trigger.new){
        System.debug('^^^contains postcard keyword??'+fi.Body);
        //if(fi.Body != null){
            if(fi.Body != null && fi.Body.containsIgnoreCase('#postcard') && fi.ContentSize > 0){
                //System.debug('^^^Inside if ');
                fiList.add(fi.ParentId);
                //System.debug('^^^fiList '+fiList);
            }
        //}
    }
    //Modified by sai.Removing the SOQL query for Record Type, JIRA ST-1868.
    //List<RecordType> caseRecordTypes = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND (Name = 'Servicing' OR Name = 'iPad on Board Case')];
    List<ID> caseRecordTypes = new List<ID>();
    caseRecordTypes.add(Schema.SObjectType.case.getRecordTypeInfosByName().get('iPad on Board Case').getRecordTypeId());
    caseRecordTypes.add(Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId());
    //System.debug('^^^Case record types '+caseRecordTypes);

    if(caseRecordTypes != null){    
        //System.debug('^^^Query '+[SELECT Id, Postcard_sent__c FROM Case WHERE Id IN: fiList AND RecordTypeId IN: caseRecordTypes]);
        queriedCases = [SELECT Id, Postcard_sent__c FROM Case WHERE Id IN: fiList AND RecordTypeId IN: caseRecordTypes];
        //System.debug('^^^Queried case '+queriedCases);
        for(Case cs : queriedCases){
            if(cs.Postcard_sent__c == false){
                cs.Postcard_sent__c = true;

                casesToUpdate.add(cs);
            }           
        }
    }
    //system.debug('^^^casestoupdate'+casesToUpdate);
    if(!casesToUpdate.isEmpty()){
        update casesToUpdate;
    }
}