/********************************************************************** 
 Name:  SCS_SocialPostReplyParentIdNotChanged
====================================================== 
Purpose: ParentId from SocialPost cannot be modified after auto creation of Case for Facebook Reply.

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      22/04/2014      Initial Development
    1.1     Ata                 09/06/2016      Upadate Influencer score on WhoId update
***********************************************************************/
trigger SCS_SocialPostReplyParentIdNotChanged on SocialPost (before update) {
    //List<User> Usr = [SELECT SkipValidation__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];
    AFKLM_SCS_UserSkipValidationSingleton Usr = AFKLM_SCS_UserSkipValidationSingleton.getInstance();
    
    //add by Ata
    set<Id> WhoIdset = new set<Id>();
    Boolean triggerexecution=FALSE;
    if(!test.isRunningTest())    
    {
       AFKLM_Trigger_Execution__c trgexecution=AFKLM_Trigger_Execution__c.getValues('Social Post');
     if(trgexecution!=NULL)
        triggerexecution=trgexecution.Run_Old_Trigger__c;
    }
    else
    {
       triggerexecution=TRUE; 
    }
    
  if(triggerexecution){ 
    if(Usr.getUsr.size() > 0) {
        if(!Usr.getUsr[0].SkipValidation__c) {
            for(SocialPost sp: Trigger.new) {
                //Taken from SCS_SocialPostWallPostParentIdNotChanged
                if ('Facebook'.equals(sp.Provider) && sp.ParentId == null && 'Post'.equals(sp.MessageType)) {
                      sp.ParentId.addError('The Wallpost from Facebook with Case cannot be changed');
                }

                if ('Facebook'.equals(sp.Provider) && sp.ParentId == null && 'Reply'.equals(sp.MessageType)) {
                      sp.ParentId.addError('Sorry, you are not allowed to create a case on Facebook Reply level.');
                }
              //added by ata
              WhoIdset.add(sp.whoId); 
              System.debug('I m here'); 
            }
        }
        
     //Added by Ata
     Map<id,Account> accMap = new Map<id,Account>([select id, Influencer_Score__c from Account where Id IN: WhoIdset]);
     //system.debug('ACCOUNT ID SET:::'+WhoIdset);    
     if(accMap != Null && !accMap.isEmpty())
         for(SocialPost sp: Trigger.new){
             if(sp.whoId != Null && accMap.containsKey(sp.WhoId) && sp.whoId != Trigger.oldMap.get(sp.Id).whoId){            
                 sp.Influencer_Score__c = (Integer)accMap.get(sp.whoId).Influencer_Score__c; 
                 
             }    
         }
    //end          
    }
  }
}