/********************************************************************** 
 Name:  SCS_StatusTrackerStatistic 
====================================================== 
Purpose: 
   
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE                DETAIL                                 
    1.0                                         Initial Development
    1.1     Ata                5/06/2016        populate Owner Employee Number on status tracker 
    1.2     Sathish           27/01/2017        Populate Agent process speed,Total agents and Total post when a case is closed
	1.3		Sathish			  03/02/2017		Added conditions for too many SOQL issues.ST-1549
***********************************************************************/
trigger SCS_StatusTrackerStatistic on Case (after insert, before update) {
    Boolean notUpdate=false;

    //RecordType servicingRecType = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND Name = 'Servicing'];
    //system.debug('Recordtypetest,'+servicingRecType);
    //1.3 Added for hot fix.ST-1549
    Id servicingRecType = Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId();
    //system.debug('Recordtypetest,'+servicingRecType );
    AFKLM_CaseBeforeTriggerHandler casebeforetgr=new AFKLM_CaseBeforeTriggerHandler();
    Map<String, String> caseIds = new Map<String,String>();
    List<Case> cases = new List<Case>();

    Set<Case> casesToUpdate = new Set<Case>();
    
    //end
    for(Case cs : Trigger.new){
        //SERVICING ONLY
        //1.3 Added for hot fix.ST-1549
        if(servicingRecType!=NULL)
        {
         if(trigger.isinsert ||(trigger.isupdate && cs.Status!=trigger.oldmap.get(cs.Id).status))
        {
            if(cs.RecordTypeId == servicingRecType){
                caseIds.put(cs.id, cs.Status);
                cases.add(cs);
        	}
        }
            
        }
        
    }
    
    //added by ata
    //List<user> Userlist = [select id,Name,EmployeeNumber from User where id =: UserInfo.getUserId()];          
    
    if(!caseIds.isEmpty() && caseIds.size() != null){
        //1.3 Added for hot fix.ST-1549
        List<user> Userlist = [select id,Name,EmployeeNumber from User where id =: UserInfo.getUserId()]; 
        //on insert: insert new case history
        if(Trigger.isInsert){
            
            List<Status_Tracker__c> statusTrackerItem = new List<Status_Tracker__c>();

            for(String csId : caseIds.keySet()){
                Status_Tracker__c cst = new Status_Tracker__c(
                    Case__c = csId,
                    Old_Status__c = caseIds.get(csId),
                    Start_time__c = Datetime.now()
                    );
                if('In Progress'.equals(caseIds.get(csId))){
                    cst.Type__c = 'Handled';
                } else {
                    cst.Type__c = 'Not Handled';
                }
                //added by Ata
                
                cst.Agent_Employee_Nr__c = Userlist[0].EmployeeNumber;
                //end
                statusTrackerItem.add(cst);
            }
            notUpdate = true;
            insert statusTrackerItem;
        }
        //on update: update all case history with value of new old status ..... insert new case history
        
        if(Trigger.isUpdate){
            if(!notUpdate){
                List<Status_Tracker__c> trackerToUpsert = new List<Status_Tracker__c>();
                List<Status_Tracker__c> trackersToUpdate = [SELECT Id,Agent_Employee_Nr__c, Case__c, Old_status__c, New_Status__c, Changed_By__c,Type__c, End_Time__c FROM Status_Tracker__c WHERE Case__c IN: caseIds.keySet() ORDER BY CreatedDate DESC];
                //update old
                for(String str : caseIds.keySet()){
                    if(!trackersToUpdate.isEmpty() && trackersToUpdate.size() > 0){
                        for(Status_Tracker__c statusTracker : trackersToUpdate){
                            if(statusTracker.New_Status__c == null && statusTracker.Case__c == str && statusTracker.Old_status__c != caseIds.get(str)){
                                statusTracker.New_Status__c = caseIds.get(str);
                                statusTracker.End_Time__c = Datetime.now();
                                statusTracker.Changed_By__c = UserInfo.getFirstName()+' '+UserInfo.getLastName();
                                //added by Ata
                                
                                statusTracker.Agent_Employee_Nr__c = Userlist[0].EmployeeNumber;
                                //end
                                trackerToUpsert.add(statusTracker);
                               

                                //create new
                                Status_Tracker__c trackerItem = new Status_Tracker__c(
                                    Case__c = str,
                                    Old_status__c = caseIds.get(str),
                                    Start_Time__c = Datetime.now()
                                    );

                                if('In Progress'.equals(caseIds.get(str))){
                                    trackerItem.Type__c = 'Handled';
                                    //update total handling time on case

                                } else {
                                    trackerItem.Type__c = 'Not Handled';
                                }
                                trackerItem.Agent_Employee_Nr__c = Userlist[0].EmployeeNumber;
                                trackerToUpsert.add(trackerItem);
                            }
                        }
                    } else {
                        //If there is no created, create new status tracker - fix for old cases before implementation
                        Status_Tracker__c trackerItem = new Status_Tracker__c(
                                    Case__c = str,
                                    Old_status__c = caseIds.get(str),
                                    Start_Time__c = Datetime.now(),
                                    //added by Ata
                                    Agent_Employee_Nr__c = Userlist[0].EmployeeNumber
                                    //end                                    
                                    );
                                     
                        if('In Progress'.equals(caseIds.get(str))){
                            trackerItem.Type__c = 'Handled';
                            //update total handling time on case

                        } else {
                            trackerItem.Type__c = 'Not Handled';
                            
                        }
                        trackerItem.Agent_Employee_Nr__c = Userlist[0].EmployeeNumber;
                        trackerToUpsert.add(trackerItem);
                    }
                system.debug('trackerToUpsert:::::'+trackerToUpsert);    
                upsert trackerToUpsert;
                }
            
                List<Status_Tracker__c> handledTrackers = [SELECT Handling_Time__c, Case__c FROM Status_Tracker__c WHERE Case__c IN: caseIds.keySet() AND Handling_Time__c != null AND Old_Status__c = 'In progress'];
                Integer totalHandlingTime;
                for(Case c : cases){
                    totalHandlingTime = 0;
                    for(Status_Tracker__c st : handledTrackers){
                        if(c.Id == st.Case__c){
                            totalHandlingTime = totalHandlingTime + (Integer)st.Handling_Time__c;
                        }
                    }
                    if(!casesToUpdate.contains(c)){
                        c.Total_Handling_Time__c = totalHandlingTime/60;
                        casesToUpdate.add(c);
                    }
                }
            }
                
        }
    }
    if(trigger.isupdate)
    {
        //V1.2 Execute the trigger when the case status contains 'Closed'
        casebeforetgr.onbeforeupdate(trigger.new);
    }
}