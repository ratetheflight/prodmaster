trigger SCS_UpdateCaseOwnerMessenger on Case (after insert) {
	Map<String,Case> caseIDs = new Map<String,Case>();
	
	for(Case cs : Trigger.new){
		if(cs.chatHash__c != null){
			caseIDs.put(cs.Id, cs);
			System.debug('^^^I am working with this case '+cs.id);
		}
	}

	if(!caseIDs.isEmpty()){

		AFKLM_SCS_CaseOwnerFutureMethod.setCaseOwner(caseIDs.keySet());
	
	}
}