trigger SCS_UpdateCasePostcardOnFeedComment on FeedComment (after insert) {
    
    List<Case> queriedCases = new List<Case>();
    List<Case> casesToUpdate = new List<Case>();
    List<String> fcList = new List<String>();

    for(FeedComment fc : Trigger.new){
        //System.debug('^^^contains postcard keyword?? '+fc.CommentBody);
            if(fc.CommentBody != null && (fc.CommentBody.containsIgnoreCase('#Postcard') || fc.CommentBody.containsIgnoreCase('#Post card'))){
                fcList.add(fc.ParentId);
            }
            //System.debug('^^^contains postcard keyword?? '+fcList);
    }
     //Modified by sai.Removing the SOQL query for Record Type, JIRA ST-1868.
    //List<RecordType> caseRecordTypes = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND (Name = 'Servicing' OR Name = 'iPad on Board Case')];
    List<ID> caseRecordTypes = new List<ID>();
    caseRecordTypes.add(Schema.SObjectType.case.getRecordTypeInfosByName().get('iPad on Board Case').getRecordTypeId());
    caseRecordTypes.add(Schema.SObjectType.case.getRecordTypeInfosByName().get('Servicing').getRecordTypeId());
    //System.debug('^^^contains postcard keyword?? '+caseRecordTypes);
    
    if(caseRecordTypes != null){    
        queriedCases = [SELECT Id, Postcard_sent__c FROM Case WHERE Id IN: fcList AND RecordTypeId IN: caseRecordTypes];
        if(!queriedCases.isEmpty()){
            for(Case cs : queriedCases){
                if(cs.Postcard_sent__c == false){
                    cs.Postcard_sent__c = true;

                    casesToUpdate.add(cs);
                }           
            }
        }
    }
    //System.debug('^^^contains postcard keyword?? '+casesToUpdate);
    if(!casesToUpdate.isEmpty()){
        update casesToUpdate;
    }

}