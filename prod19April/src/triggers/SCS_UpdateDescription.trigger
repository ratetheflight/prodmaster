/********************************************************************** 
 Name:  SCS_UpdateDescription 
====================================================== 
Purpose: 

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0                                         Initial Development
    1.2     Ata                9/06/2016        Copy Influencer score on social Post
    1.3     Prasanna          14/10/2016        Provided hot fix for additional description field
    1.4     Pushkar           11/04/2017        Account email update on live chat transcript
******************************************************************************/
trigger SCS_UpdateDescription on Account (after insert, after update) {
    List<SocialPersona> personasToUpdate = new List<SocialPersona>();
    Map<Id,Account> accMap = new Map<Id,Account>();
    List<LiveChatTranscript> chatList = new  List<LiveChatTranscript>();
    
    if(Trigger.isUpdate){
        for(Account acc : Trigger.New){
            
            if(acc.PersonEmail != Trigger.oldMap.get(acc.id).PersonEmail)
                accMap.put(acc.Id,acc);
        }
        
        for(LiveChatTranscript cht : [select id, Email__c,AccountId from LiveChatTranscript where AccountId IN : accMap.keySet()]){
            LiveChatTranscript chtInst = new LiveChatTranscript(Id = cht.id,Email__c = accMap.get(cht.AccountId).PersonEmail);
            chatList.add(chtInst);
            
        }
        
        if(chatList.size()>0)
            update chatList;
    }
    for(SocialPersona sp : [SELECT Id, ParentId, Additional_Description__c FROM SocialPersona
                      WHERE ParentId IN :Trigger.new]) {

        for(Account ac: Trigger.new) {
            //Author: Prasanna - Added extra filter to check whether old and new additional description field is same ST-1078
            if(sp.ParentId == ac.id && 
                ac.Additional_Description__c != NULL && ac.additional_description__c!=Trigger.oldMap.get(ac.id).additional_description__c) {

                sp.Additional_Description__c = ac.Additional_Description__c;
                personasToUpdate.add(sp);
            }  //Author: Prasanna - Added extra filter to check whether old and new additional description field is same ST-1078
            else if (sp.ParentId == ac.id && 
                ac.Additional_Description__c == NULL && ac.additional_description__c!=Trigger.oldMap.get(ac.id).additional_description__c) {

                sp.Additional_Description__c = NULL;
                personasToUpdate.add(sp);               
            }
        }
    }
    
    //Added by Ata
    List<socialPost> spToUpdate = new List<socialPost>();
    Set<ID> whoIdset = new set<ID>();
    Map<id,List<socialPost>> AccountPostMap = new Map<id,List<socialPost>>();
    
    if(Trigger.IsAfter && Trigger.isUpdate){
        
        List<Socialpost> spost = [Select id,Influencer_score__c,whoId from socialPost where (SCS_Status__c = 'New' OR SCS_Status__c = '') AND whoId IN: Trigger.new];
        
        if(spost != Null && spost.size() > 0){
            for(socialpost sp: spost){
                if(! whoIdset.contains(sp.whoId)){
                    whoIdset.add(sp.whoId);
                    AccountPostMap.put(sp.whoId, new List<SocialPost>{sp});
                }
                else{
                    List<Socialpost> spList = AccountPostMap.get(sp.whoId);
                    spList.add(sp);
                    AccountPostMap.put(sp.WhoId,spList);
                }
            }
            
            for(Account ac: Trigger.new){
                //check if influencer/Tier level/Booking class is changed
                if(ac.Influencer_score__c != Trigger.oldMap.get(ac.id).Influencer_score__c ||
                    ac.Tier_Level__c != Trigger.oldMap.get(ac.id).Tier_Level__c || ac.Booking_Class__c != Trigger.oldMap.get(ac.id).Booking_Class__c){
                  if(AccountPostMap.get(ac.id) != null)
                    for(socialpost sp: AccountPostMap.get(ac.id)){
                        sp.Influencer_Score__c = ac.Influencer_Score__c;
                        //Added tier level n booking class assignment-ST-580,ST-583
                        sp.Tier_Level__c = ac.Tier_Level__c;
                        sp.Booking_Class__c = ac.Booking_Class__c;
                        spToUpdate.add(sp);
                    }
                }
                //
            }
        }
        try{
            if(spToUpdate.size() > 0)
            update spToUpdate;
         }
         catch(Exception e){
             System.debug('Exception happened::'+e.getmessage());
         }
    }   
    //end
    
    if(personasToUpdate.size() > 0)
        upsert personasToUpdate;
}