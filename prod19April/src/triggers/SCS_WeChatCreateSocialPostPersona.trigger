//DvtH23JULY2015 TODO Header is missing
trigger SCS_WeChatCreateSocialPostPersona on Nexmo_Post__c (after insert) {
    
    /*private SocialPost newSocialPost;
    private SocialPersona persona;
    //**********************WeChat lists*******************************
    private List<SocialPersona> weChatPersonas {
        get {

            if(weChatPersonas == NULL) {
                weChatPersonas = new List<SocialPersona>();
            }

            return weChatPersonas;
        }
        set;}

    private List<SocialPost> inboundWeChatPosts {
        get {

            if(inboundWeChatPosts == NULL) {
                inboundWeChatPosts = new List<SocialPost>();
            }

            return inboundWeChatPosts;
        }
        set;}

    private List<SocialPost> outboundWeChatPosts {
        get {

            if(outboundWeChatPosts == NULL) {
                outboundWeChatPosts = new List<SocialPost>();
            }

            return outboundWeChatPosts;
        }
        set;}

    //******************Facebook Messenger lists*************************
    private List<SocialPersona> messengerPersonas {
        get {

            if(messengerPersonas == NULL) {
                messengerPersonas = new List<SocialPersona>();
            }

            return messengerPersonas;
        }
        set;}

    private List<SocialPost> inboundMessengerPosts {
        get {

            if(inboundMessengerPosts == NULL) {
                inboundMessengerPosts = new List<SocialPost>();
            }

            return inboundMessengerPosts;
        }
        set;}

    private List<SocialPost> outboundMessengerPosts {
        get {

            if(outboundMessengerPosts == NULL) {
                outboundMessengerPosts = new List<SocialPost>();
            }

            return outboundMessengerPosts;
        }
        set;}
    
    private String picUrl = '/resource/SocialCustomerServiceResources/SocialCustomerServiceResources/img/noface.gif';

    AFKLM_SCS_WeChatSettings__c custSettings = AFKLM_SCS_WeChatSettings__c.getValues('Settings');

    // to remove duplicates from the list
    Set<SocialPersona> weChatPersonasTmpSet = new Set<SocialPersona>();
    List<SocialPersona> weChatPersonasTmpList = new List<SocialPersona>();

    //FB Messenger
    Set<SocialPersona> messengerPersonasTmpSet = new Set<SocialPersona>();
    List<SocialPersona> messengerPersonasTmpList = new List<SocialPersona>();

    for(Nexmo_Post__c nexmoPost : Trigger.new){

        // Create Social Post - Outbound path
        if('Outbound'.equals(nexmoPost.Source__c)){
            
            try{
                newSocialPost = new SocialPost(
                    Name = 'Message from: '+custSettings.SCS_SysAccountName_del__c,
                    Content = nexmoPost.Text__c,
                    Handle = custSettings.SCS_SysPersonaName__c,
                    WhoId = custSettings.SCS_SysAccountId__c,
                    SCS_Status__c = 'New',
                    Posted = nexmoPost.Send_Time__c,
                    ExternalPostId = nexmoPost.Chat_Hash__c,
                    IsOutbound = true,
                    Provider = nexmoPost.Social_Network__c, 
                    Company__c = nexmoPost.Company__c
                    );
                if('WeChat'.equals(nexmoPost.Social_Network__c)){
                    outboundWeChatPosts.add( newSocialPost );
                } else {
                    newSocialPost.Provider = 'Other';
                    outboundMessengerPosts.add( newSocialPost );
                }

            } catch (Exception e){
                
                System.debug(e.getMessage());
            }

            try{
                persona = new SocialPersona(
                    Name = custSettings.SCS_SysPersonaName__c,
                    ParentId = custSettings.SCS_SysAccountId__c,         
                    Provider = nexmoPost.Social_Network__c,
                    ExternalPictureURL = picUrl,
                    RealName = custSettings.SCS_SysPersonaName__c,
                    ExternalId = custSettings.SCS_SysPersonaExternalId__c           
                );

            } catch(Exception e){

                System.debug('Error message '+e.getMessage());
            }
                
        } else {

            //Part for the Inbound Social Post
            try{
                newSocialPost = new SocialPost(
                    Name = 'Message from: '+nexmoPost.User_Real_Name__c,
                    Content = nexmoPost.Text__c,
                    Handle = nexmoPost.User_Real_Name__c,
                    SCS_Status__c = 'New',
                    Posted = nexmoPost.Send_Time__c,
                    ExternalPostId = nexmoPost.Chat_Hash__c,
                    IsOutbound = false,
                    Provider = nexmoPost.Social_Network__c, 
                    Company__c = nexmoPost.Company__c
                    //SP_Traffic__c = nexmoPost.Direction__c
                );

                if('WeChat'.equals(nexmoPost.Social_Network__c)){
                    inboundWeChatPosts.add( newSocialPost );
                } else {
                    newSocialPost.Provider = 'Other';
                    inboundMessengerPosts.add( newSocialPost );
                }
            } catch (Exception e){
                
                System.debug('--+ Error message: '+e.getMessage());
            }

            try{
                persona = new SocialPersona(
                    Name = nexmoPost.User_Real_Name__c,
                    //ParentId = custSettings.SCS_TemporaryPersonAccount__c,         //DvtH05OCT2019 TempAccountFix: -->
                    Provider = nexmoPost.Social_Network__c,
                    ExternalPictureURL = picUrl,
                    RealName = nexmoPost.User_Real_Name__c,
                    ExternalId = nexmoPost.From_User_Id__c
                    //WeChat_User_ID__c = nexmoPost.From_User_Id__c         
                );

            } catch(Exception e){

                System.debug('Error message '+e.getMessage());
            }

        }

        //personas.add(persona);
        if('WeChat'.equals(nexmoPost.Social_Network__c)){
            weChatPersonasTmpList.add(persona);
        } else {
            persona.Provider = 'Other';
            messengerPersonasTmpList.add(persona);
        }
    }

    //********************WECHAT*******************************
    // remove all duplicates
    System.debug('--+ CreatePostPersona - weChatPersonasTmpList: '+weChatPersonasTmpList);
    //DvtH23JULY2015 TODO if pesona is directly set in the set instead of the list than it saves a copy from list to set!
    weChatPersonasTmpSet.addAll( weChatPersonasTmpList );
    System.debug('--+ CreatePostPersona - weChatPersonasTmpSet: '+weChatPersonasTmpSet);
    weChatPersonas.addAll( weChatPersonasTmpSet );
    System.debug('--+ CreateTmpSet - personas: '+weChatPersonas);

    AFKLM_SCS_InboundWeChatHandlerImpl wechatHandler = new AFKLM_SCS_InboundWeChatHandlerImpl();
    if(!inboundWeChatPosts.isEmpty()){    
        system.debug('^^^^Personas '+weChatPersonas);
        wechatHandler.handleInboundSocialPost(inboundWeChatPosts,weChatPersonas);
    }

    if(!outboundWeChatPosts.isEmpty()){
        wechatHandler.handleInboundSocialPost(outboundWeChatPosts,weChatPersonas);
    }

    //**************************Facebook Messenger*******************************
    messengerPersonasTmpSet.addAll( messengerPersonasTmpList );
    messengerPersonas.addAll( messengerPersonasTmpSet );

    AFKLM_SCS_InboundMessengerHandlerImpl messengerHandler = new AFKLM_SCS_InboundMessengerHandlerImpl();
    if(!inboundMessengerPosts.isEmpty()){    
        messengerHandler.handleInboundSocialPost(inboundMessengerPosts,messengerPersonas);
    }

    if(!outboundMessengerPosts.isEmpty()){
        messengerHandler.handleInboundSocialPost(outboundMessengerPosts,messengerPersonas);
    }*/
}