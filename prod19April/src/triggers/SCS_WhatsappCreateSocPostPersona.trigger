trigger SCS_WhatsappCreateSocPostPersona on WhatsappPost__c (after insert) {
    //DvtH23JULY2015 TODO Header is missing
    
    private SocialPost newSocialPost;
    private SocialPersona persona;
    private String toApiKey;
    private String toNumber;
    private Map<String,String> apiKeys = new Map<String,String>();
    //**********************whatsapp lists*******************************
    private List<SocialPersona> whatsappPersonas {
        get {

            if(whatsappPersonas == NULL) {
                whatsappPersonas = new List<SocialPersona>();
            }

            return whatsappPersonas;
        }
        set;}

    private List<SocialPost> inboundwhatsappPosts {
        get {

            if(inboundwhatsappPosts == NULL) {
                inboundwhatsappPosts = new List<SocialPost>();
            }

            return inboundwhatsappPosts;
        }
        set;}

            
    //****************Fill apiKeys**************************
    apiKeys.put('31613020560', '9lTFTstno7mcw8zS29PMNf01MoCTQqbkt2T59TCo');
    apiKeys.put('31683809030', 'hLCJQJLM88hhJ2PmWRLt7vE5RndDuYUKLO3dYTBe');
    apiKeys.put('31683809123', 'u1QeU8s05AMU9RhFGCw0eKykZpTmTjSjyWPaDBwb');
    //SME WhatsApp number
    apiKeys.put('31683808006', 'RC6hBnR1c6e7JZ4GgepFmdVKcYs0ZWObi8QmybyQ');

    private String picUrl = '/resource/SocialCustomerServiceResources/SocialCustomerServiceResources/img/noface.gif';

    //AFKLM_SCS_WhatsappSettings__c custSettings = AFKLM_SCS_WhatsappSettings__c.getValues('Settings');

    // to remove duplicates from the list
    Set<SocialPersona> whatsappPersonasTmpSet = new Set<SocialPersona>();
    List<SocialPersona> whatsappPersonasTmpList = new List<SocialPersona>();

    

    for(WhatsappPost__c whatsappPost : Trigger.new){

        String modifiedContent = '';

    	toNumber = WhatsappPost.To__c.substring(0,WhatsappPost.To__c.lastindexof('@'));

    	if(apiKeys.containsKey(toNumber)){
    		toApiKey = apiKeys.get(toNumber);
    	}
        //if(WhatsappPost.Content__c != null){
        //    system.debug('--+ WhatsappPost.Content__c : '+WhatsappPost.Content__c);
        //    modifiedContent = WhatsappPost.Content__c;
        //    }

        /*if(modifiedContent.contains('&#39;')){
            modifiedContent = modifiedContent.replace('&#39;', '\'');
        }*/

            try{

                if('text'.equals(WhatsappPost.Type__c)){
                    
                    newSocialPost = new SocialPost(
                        Name = 'Message from: '+WhatsappPost.Full_Name__c,
                        Content = WhatsappPost.Content__c,
                        //Content = modifiedContent,
                        Handle = WhatsappPost.Full_Name__c,
                        SCS_Status__c = 'New',
                        Posted = WhatsappPost.Posted__c,
                        ExternalPostId = WhatsappPost.MessageId__c,
                        IsOutbound = false,
                        Provider = 'WhatsApp',
                        MessageType = 'Private', 
                        TopicProfileName = toApiKey,
                        Recipient = 'KLM',
                        RecipientsNumber__c = toNumber,
                        Company__c = 'KLM'
                        //SP_Traffic__c = WhatsappPost.Direction__c
                    );
                } else if('location'.equals(WhatsappPost.Type__c)){
                    newSocialPost = new SocialPost(
                        Content = WhatsappPost.Location_Name__c+': '+WhatsappPost.Latitude__c+' '+WhatsappPost.Longitude__c,
                        Name = 'Message from: '+WhatsappPost.Full_Name__c,
                        Handle = WhatsappPost.Full_Name__c,
                        SCS_Status__c = 'New',
                        Posted = WhatsappPost.Posted__c,
                        ExternalPostId = WhatsappPost.MessageId__c,
                        IsOutbound = false,
                        Provider = 'WhatsApp',
                        MessageType = 'Private',
                        TopicProfileName = toApiKey,
                        Recipient = 'KLM',
                        RecipientsNumber__c = toNumber, 
                        Company__c = 'KLM'
                    );
                } else {
                    newSocialPost = new SocialPost(
                        Content = WhatsappPost.Caption__c,
                        AttachmentUrl = WhatsappPost.URL__c,
                        Name = 'Message from: '+WhatsappPost.Full_Name__c,
                        Handle = WhatsappPost.Full_Name__c,
                        SCS_Status__c = 'New',
                        Posted = WhatsappPost.Posted__c,
                        ExternalPostId = WhatsappPost.MessageId__c,
                        IsOutbound = false,
                        Provider = 'WhatsApp',
                        MessageType = 'Private',
                        TopicProfileName = toApiKey,
                        Recipient = 'KLM',
                        RecipientsNumber__c = toNumber, 
                        Company__c = 'KLM'
                    );
                }

                    inboundwhatsappPosts.add( newSocialPost );
                
            } catch (Exception e){
                
                System.debug('--+ Error message: '+e.getMessage());
            }

            try{
                persona = new SocialPersona(
                    Name = WhatsappPost.From__c.substring(0,WhatsappPost.From__c.lastindexof('@')),
                    //ParentId = custSettings.SCS_TemporaryPersonAccount__c,	//DvtH05OCT2019 TempAccountFix: -->
                    Provider = 'WhatsApp',
                    ExternalPictureURL = picUrl,
                    RealName = WhatsappPost.Full_Name__c,
                    ExternalId = WhatsappPost.From__c.substring(0,WhatsappPost.From__c.lastindexof('@'))
                    //whatsapp_User_ID__c = WhatsappPost.From_User_Id__c            
                );

            } catch(Exception e){

                System.debug('Error message '+e.getMessage());
            }

        //personas.add(persona);
        whatsappPersonasTmpList.add(persona);
    }

    //********************whatsapp*******************************
    whatsappPersonasTmpSet.addAll( whatsappPersonasTmpList );
    whatsappPersonas.addAll( whatsappPersonasTmpSet );

    AFKLM_SCS_InboundWhatsappHandlerImpl whatsappHandler = new AFKLM_SCS_InboundWhatsappHandlerImpl();
    if(!inboundwhatsappPosts.isEmpty()){    
        whatsappHandler.handleInboundSocialPost(inboundwhatsappPosts[0],whatsappPersonas[0]);
        //DvtH08OCT2015 Warning message added just in case. Should not occure
        if(inboundwhatsappPosts.size()>1) {
        	System.debug(logginglevel.WARN, 'Multiple Social Posts and Persona\'s received! All dropped with exception of the first one!!');
        }
    }

    

}