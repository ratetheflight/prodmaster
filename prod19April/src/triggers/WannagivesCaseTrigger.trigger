/**********************************************************************
 Name:  WannagivesCaseTrigger
======================================================
Purpose: 

Handle various status changes for a Wannagives Case

======================================================
History                                                            
-------                                                            
VERSION      AUTHOR                DATE            DETAIL                                 
    1.0     AJ van Kesteren     06/09/2013      Initial Development
    1.1     Sathish             03/02/2017      Added conditions for too many SOQL issues.ST-1549
    1.2     Prasanna            21/03/2017      For Webchat Anonymous Account scenario ST-1738
    1.3     Banu                06/04/2017      ST-2115: Person account creation for webchat cases
***********************************************************************/    
trigger WannagivesCaseTrigger on Case (before insert, after update) {

Id RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Webchat').getRecordTypeId();
Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
Anonymous_Account__c anx=Anonymous_Account__c.getvalues('Anonymous');
List <Account> acc=new List<Account>();
List <Case> caselist=new List<Case>();
Set<Case> caseForPACreationSet=new Set<Case>();
Date mydate=Date.today();

if(Trigger.isInsert){
    for(Case ca: Trigger.new){
        if(ca.recordtypeid==RecordTypeId && (ca.Visitor_email__c!=null || ca.Visitor_Flying_Blue_number__c!=null) && ca.accountid==null){
           
            if(ca.Visitor_email__c=='default@email.com')
                ca.Visitor_email__c='';
            if(ca.Visitor_Flying_Blue_number__c=='FBNumber')
                ca.Visitor_Flying_Blue_number__c='';
            if(ca.web_bookingcode__c=='BookingCode')
                ca.web_bookingcode__c='';
        }
     }
}
/*
if(Trigger.isInsert){
    for(Case ca: Trigger.new){
        if(ca.recordtypeid==RecordTypeId && (ca.Visitor_email__c!=null || ca.Visitor_Flying_Blue_number__c!=null) && ca.accountid==null){
            acc=[select id,name from Account where flying_blue_number__c =:ca.Visitor_Flying_Blue_number__c or personemail=:ca.Visitor_email__c limit 1];

           
            if(ca.Visitor_email__c=='default@email.com')
                ca.Visitor_email__c='';
            if(ca.Visitor_Flying_Blue_number__c=='FBNumber')
                ca.Visitor_Flying_Blue_number__c='';
            if(ca.web_bookingcode__c=='BookingCode')
                ca.web_bookingcode__c='';

            if(acc.size() > 0){
                
                ca.accountid=acc[0].id;
            }
            else if((ca.Visitor_email__c != null && ca.Visitor_email__c != '') || (ca.Visitor_Flying_Blue_number__c != null && ca.Visitor_Flying_Blue_number__c != '')){
                
                caseForPACreationSet.add(ca);
                AFKLM_CaseBeforeTriggerHandler handler=new AFKLM_CaseBeforeTriggerHandler();
                handler.createPersonAccount(caseForPACreationSet);    
                System.debug('Banu testing : Account Mapped');
            }
            else{
                
                if(anx.transcriptcount__c > 499){
                    Account acch=[select id,firstname,lastname from account where id=:anx.accountid__c limit 1];
                    acch.firstname='AnonymousAccount';
                    acch.lastname='History'+mydate;
                    update acch;
                    Account acct=new Account();
                    acct.firstname='Anonymous';
                    acct.lastname='Account';
                    acct.recordtypeid=AccRecordTypeId;
                    insert acct;
                    anx.accountid__c=acct.id;
                    anx.transcriptcount__c=1;
                    update anx;
                }
                else{
                    anx.transcriptcount__c=anx.transcriptcount__c+1;
                    update anx;
                }
                ca.accountid=anx.accountid__c;
            }
        }
    }
}

*/


    //1.1.ST-1549
    private Id WANNAGIVES_RECORD_TYPE_ID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Wannagives').getRecordTypeId();


    // Filter all cases in scope of this trigger on record type id
    List<Case> wannagivesCases = new List<Case>();
    for (Case c : Trigger.new) {
        if(WANNAGIVES_RECORD_TYPE_ID!=NULL){
           if (c.RecordTypeId == WANNAGIVES_RECORD_TYPE_ID) {
            wannagivesCases.add(c);     
            }  
        }
          
    }
    
    // Only do something if there are actually Wannagives cases in scope
    if (wannagivesCases.size() > 0) {
        WannagivesIntegrationManager manager = new WannagivesIntegrationManager();
        if (Trigger.isInsert) {
            
            System.debug('@@@@ New Wannagives Cases Created');
            manager.handleNewCases(wannagivesCases);    
        
        } else if (Trigger.isUpdate) {

            List<Case> casesToUpdate = new List<Case>();            
            for (Case c : wannagivesCases) {
            
                // Do a toUpperCase on the old and new status, just to make the solution resilient to case configuration changes
                String oldStatus = Trigger.oldMap.get(c.Id).Status.toUpperCase();
                String newStatus = c.Status.toUpperCase();
                
                if (oldStatus == 'NEW' && newStatus == 'OK') {
                    System.debug('@@@@ Wannagives Case Status change: New -> Ok');  
                    Case c2 = [select Wannagives_Integration_Status__c from Case where Id = :c.Id limit 1];
                    c2.Wannagives_Integration_Status__c = WannagivesIntegrationManager.INTEGRATION_STATUS_PENDING;
                    casesToUpdate.add(c2);
                    WannagivesIntegrationManager.handleOkClosedCase(c.Id);
                } else if (oldstatus == 'NEW' && newStatus == 'NOT OK') {
                    System.debug('@@@@ Wannagives Case Status change: New -> Not Ok');
                    Case c2 = [select Wannagives_Integration_Status__c from Case where Id = :c.Id limit 1];
                    c2.Wannagives_Integration_Status__c = WannagivesIntegrationManager.INTEGRATION_STATUS_PENDING;
                    casesToUpdate.add(c2);
                    WannagivesIntegrationManager.handleNotOkClosedCase(c.Id); 
                }
            }
            update casesToUpdate;
            
        }
    }
}