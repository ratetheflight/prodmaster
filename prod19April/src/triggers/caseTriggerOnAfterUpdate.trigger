trigger caseTriggerOnAfterUpdate on Case (after update) {

    if(!System.isBatch()){
        if(Trigger.isAfter && Trigger.isUpdate){

            SCS_HerokuOutboundHandler herokuHandler = new SCS_HerokuOutboundHandler();
            herokuHandler.sendCasesOnUpdateToHeroku(trigger.old, trigger.new,trigger.oldMap,trigger.newMap);

        }
     }
}