<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>PSL_Survey_Request_EN</fullName>
        <description>PSL Survey Request - EN</description>
        <protected>false</protected>
        <recipients>
            <field>PSL_Survey_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.psl@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Platinum_Service_Line/Survey_Request_EN</template>
    </alerts>
    <alerts>
        <fullName>PSL_Survey_Request_FR</fullName>
        <description>PSL Survey Request - FR</description>
        <protected>false</protected>
        <recipients>
            <field>PSL_Survey_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.psl@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Platinum_Service_Line/Survey_Request_FR</template>
    </alerts>
    <alerts>
        <fullName>PSL_Survey_Request_NL</fullName>
        <description>PSL Survey Request - NL</description>
        <protected>false</protected>
        <recipients>
            <field>PSL_Survey_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply.psl@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Platinum_Service_Line/Survey_Request_NL</template>
    </alerts>
    <fieldUpdates>
        <fullName>PSL_Survey_Last_Sent</fullName>
        <field>PSL_Survey_Last_Sent__c</field>
        <formula>NOW()</formula>
        <name>PSL Survey Last Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Send_Email_Flag</fullName>
        <field>PSL_Survey_Send_Email_Flag__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PSL Survey EN</fullName>
        <actions>
            <name>PSL_Survey_Request_EN</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PSL_Survey_Last_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Send_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.PSL_Survey_Send_Email_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PSL_Survey_Language__c</field>
            <operation>equals</operation>
            <value>EN</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PSL Survey FR</fullName>
        <actions>
            <name>PSL_Survey_Request_FR</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PSL_Survey_Last_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Send_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.PSL_Survey_Send_Email_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PSL_Survey_Language__c</field>
            <operation>equals</operation>
            <value>FR</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PSL Survey NL</fullName>
        <actions>
            <name>PSL_Survey_Request_NL</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PSL_Survey_Last_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Send_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.PSL_Survey_Send_Email_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.PSL_Survey_Language__c</field>
            <operation>equals</operation>
            <value>NL</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
