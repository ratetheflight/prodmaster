<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Clientele_New_Heroku_Case_alert</fullName>
        <description>Clientele New Heroku Case alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>niels.jonker@klm.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Heroku_new_support_alert</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_IMO_Ancillary</fullName>
        <description>Clientele auto reply IMO Ancillary</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>imo.ancillary@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_IMO_E_Booking</fullName>
        <description>Clientele auto reply IMO E-Booking</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>imo.e-booking@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_IMO_Fam_E_Acquisition</fullName>
        <description>Clientele auto reply IMO Fam E-Acquisition</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>imo.fam-e-acquisition@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_IMO_Mobile</fullName>
        <description>Clientele auto reply IMO Mobile</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>imo.mobile@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_IMO_Support_Checkout_and_Payment</fullName>
        <description>Clientele auto reply IMO Support Checkout and Payment</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>grpsplxe200516@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_Medeam_1st_line_e_support</fullName>
        <description>Clientele auto reply Medeam 1st line e-support</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>1stline.esupport@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_Mobile</fullName>
        <description>Clientele auto reply Mobile</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>mobile.support@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_Shop_Management</fullName>
        <description>Clientele auto reply Shop Management</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>shopmanagement@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_TCS_1st_line_e_support</fullName>
        <description>Clientele auto reply TCS 1st line e-support</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>tcs-1stline.esupport@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>Clientele_auto_reply_Webshop</fullName>
        <description>Clientele auto reply Webshop</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>shopklmamssz@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Case_Auto_Reply</template>
    </alerts>
    <alerts>
        <fullName>New_Digital_Case</fullName>
        <description>New Digital Case</description>
        <protected>false</protected>
        <recipients>
            <recipient>ilija.prokopov@klm.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nalini.nauth@klm.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Digital_notification</template>
    </alerts>
    <alerts>
        <fullName>New_Ipad_on_Board_Case_Email</fullName>
        <ccEmails>socialmediahub@klm.com</ccEmails>
        <description>New Ipad on Board Case Email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Ipad_On_Board_Case_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Webshop_email</fullName>
        <ccEmails>shoppingserviceinflight@klm.com</ccEmails>
        <description>Webshop: New Webshop email</description>
        <protected>false</protected>
        <senderAddress>socialmediahub@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Notification_Webshop</template>
    </alerts>
    <alerts>
        <fullName>Sent_remider_to_CC_NL</fullName>
        <ccEmails>Luuk.vermaas@accenture.com</ccEmails>
        <description>Sent reminder to CC NL</description>
        <protected>false</protected>
        <senderAddress>socialmediahub@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>iPad_on_Board_Templates/Reminder_CC_NL</template>
    </alerts>
    <alerts>
        <fullName>Trigger_Email_Notification_to_new_owners</fullName>
        <description>Trigger Email Notification to new owners</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>iPad_on_Board_Templates/IPOB_case_Owner_change</template>
    </alerts>
    <alerts>
        <fullName>Webshop_email_notification</fullName>
        <ccEmails>socialmediahub@klm.com</ccEmails>
        <description>Webshop email notification</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORTNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>email_alert</fullName>
        <description>email alert</description>
        <protected>false</protected>
        <recipients>
            <field>In_progress_email_alert__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforce@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Email_alert_for_agents</template>
    </alerts>
    <alerts>
        <fullName>iPOB_Medical_cases_forwarded_to_frontlinestaff</fullName>
        <description>iPOB Medical cases forwarded to frontlinestaff</description>
        <protected>false</protected>
        <recipients>
            <field>iPOB_Medical_Case_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>info.crm@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>iPad_on_Board_Templates/Medical_iPOB_cases</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_WG_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Wannagives_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to WG queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Engaged_Date_Time_Set</fullName>
        <description>Set the Engaged Date Time on the Case</description>
        <field>Engaged_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Case Engaged Date Time Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner_PA_Desk</fullName>
        <description>Set Case Owner to PA-Desk</description>
        <field>OwnerId</field>
        <lookupValue>PA_Desk_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Case Owner PA-Desk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Data_Prefill_Field</fullName>
        <description>Clear the Data for Prefill by DG field</description>
        <field>Data_for_Prefill_By_DG__c</field>
        <name>Clear_Data_Prefill_Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cygnific_Group</fullName>
        <field>Group__c</field>
        <literalValue>Cygnific</literalValue>
        <name>Cygnific Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Suppor_IMO_Ancillary_Owner</fullName>
        <description>E-Support old name was (Clientele)  IMO Ancillary set Case Owner to the Clientele IMO Ancillary Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_IMO_Ancillary</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-SupporIMO Ancillary Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_IMO_E_Booking_Owner</fullName>
        <description>E-Support old name was (Clientele) IMO E-Booking Owner</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_IMO_E_Booking_team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support IMO E-Booking Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_IMO_Fam_E_Acquisition_Owner</fullName>
        <description>E-Support old name was (Clientele) IMO Fam E-Acquisition set Case Owner to the IMO Fam E-Acquisition Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_IMO_E_Acquisition</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support IMO Fam E-Acquisition Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_IMO_Heroku_Owner</fullName>
        <description>E-Support old name was (Clientele) IMO Heroku set case owner to the Clientele IMO Heroku Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_IMO_Heroku</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support IMO Heroku Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_IMO_Mobile_Owner</fullName>
        <description>E-Support old name was (Clientele) IMO Mobile set case owner to the Clientele IMO Mobile Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_IMO_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support IMO Mobile Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_IMO_Support_C_and_P_Owner</fullName>
        <description>E-Support old name was (Clientele) IMO Support Checkout and Payment set case owner to the IMO Support Checkout and Payment Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_IMO_PRODUCT_TEAM_CHECKOUT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support IMO Support C and P Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_Medeam_1st_Line_e_Supp_Owner</fullName>
        <description>E-Support old name was (Clientele) Medeam 1st Line e_Support set case owner to the Clientele Medeam 1st Line e_Support Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_Medeam_1st_Line_e_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support Medeam 1st Line e_Supp Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_Medeam_Mobile_Owner</fullName>
        <description>E-Support old name was (Clientele) Mobile set case owner to the Clientele Medeam Mobile Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_Medeam_Mobile</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support Medeam Mobile Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_Mobile_App_Owner</fullName>
        <description>E-Support old name was (Clientele) Mobile App set case owner to the Clientele Mobile App Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_Mobile_App</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support Mobile App Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_Shop_Management_Owner</fullName>
        <description>E-Support old name was (Clientele) Shop Management set case owner to the Clientele Shop Management Queue</description>
        <field>OwnerId</field>
        <lookupValue>MailBox_ShopManagement</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support Shop Management Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_TCS_1st_Line_e_Support_Owner</fullName>
        <description>E-Support old name was (Clientele) TCS 1st Line e_Support set Owner to the TCS 1st Line e_Support queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_TCS_1st_Line_e_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support TCS 1st Line e_Support Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>E_Support_Webshop_Owner</fullName>
        <description>E-Support old name was (Clientele) Webshop set case owner to the Clientele webshop Queue</description>
        <field>OwnerId</field>
        <lookupValue>Mailbox_Webshop</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>E-Support Webshop Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Group_to_Null</fullName>
        <description>Update the Group field to NULL when Queue name contains SCS - AF</description>
        <field>Group__c</field>
        <name>Group to Null </name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IGT_Group</fullName>
        <field>Group__c</field>
        <literalValue>IGT</literalValue>
        <name>IGT Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ipob_normal_priority_owner_assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>Ipob_Normal_Priority_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Ipob normal priority owner assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_as_FlyingBlue</fullName>
        <field>Is_Flying_Blue__c</field>
        <literalValue>1</literalValue>
        <name>Mark as FlyingBlue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Notify_Owner_checkbox_field_update</fullName>
        <field>Notify_New_Owner__c</field>
        <literalValue>1</literalValue>
        <name>Notify Owner checkbox field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PA_Desk_Case_Ownership_to_PA_queue</fullName>
        <description>If a case is logged for PA-Desk record type, PA-desk queue will become owner.</description>
        <field>OwnerId</field>
        <lookupValue>PA_Desk_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PA-Desk - Case Ownership to PA-queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PA_Desk_status_to_closed</fullName>
        <description>Set PA-Desk status to closed when due date is set on Today</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>PA-Desk status to closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Priority_VIP</fullName>
        <field>Priority</field>
        <literalValue>VIP</literalValue>
        <name>Priority = VIP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>REmove_default_email_add</fullName>
        <field>Visitor_Email__c</field>
        <name>REmove default email add</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reason_for_ipob_medical_cases</fullName>
        <field>Reason</field>
        <literalValue>In-Flight</literalValue>
        <name>Reason for ipob medical cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Restricted_Account_Case</fullName>
        <field>Restricted_Account_Case__c</field>
        <literalValue>1</literalValue>
        <name>Restricted Account Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetFlyingBlueToFalse</fullName>
        <description>To set the FlyingBlue to false</description>
        <field>Is_Flying_Blue__c</field>
        <literalValue>0</literalValue>
        <name>SetFlyingBlueToFalse</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetStatusToDelta</fullName>
        <description>Set Status To &quot;Waiting for internal answer Delta&quot;</description>
        <field>Status</field>
        <literalValue>Waiting for internal answer Delta</literalValue>
        <name>SetStatusToDelta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Date_Reopened_to_Now</fullName>
        <field>Date_Reopened__c</field>
        <formula>Now()</formula>
        <name>Set Date Reopened to Now</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Statuschange_to_IGT_B_O_to_TRUE</fullName>
        <description>Sets value &quot;Statuschange to IGT B.O.&quot; to TRUE</description>
        <field>Statuschange_to_IGT_B_O__c</field>
        <literalValue>1</literalValue>
        <name>Set Statuschange to IGT B.O. to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Subject</fullName>
        <field>Subject</field>
        <formula>TEXT(Case_Detail__c)</formula>
        <name>Set Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_as_Closed_customer_care_info</fullName>
        <field>Status</field>
        <literalValue>Closed Customer Care</literalValue>
        <name>Status as Closed customer care info</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Division_Update</fullName>
        <field>Sub_Division__c</field>
        <formula>TEXT($User.Sub_Division__c)</formula>
        <name>Sub Division Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_reason_for_ipob_medical_cases</fullName>
        <field>Sub_Reason__c</field>
        <literalValue>Medical / Health</literalValue>
        <name>Sub reason for ipob medical cases</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TWC_Group</fullName>
        <field>Group__c</field>
        <literalValue>CX</literalValue>
        <name>CX Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdatePNR</fullName>
        <field>PNR__c</field>
        <formula>IF((LEN(web_bookingcode__c)==6),web_bookingcode__c,&apos;&apos; )</formula>
        <name>UpdatePNR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Received_From</fullName>
        <field>Case_Received_From__c</field>
        <formula>TEXT(Origin)</formula>
        <name>Update Case Received From</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_FBNumber</fullName>
        <field>Visitor_Flying_Blue_number__c</field>
        <name>Update FBNumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_bookingcode</fullName>
        <field>web_bookingcode__c</field>
        <name>Update bookingcode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updateetkt</fullName>
        <field>E_ticket__c</field>
        <formula>If(((LEN(web_bookingcode__c)&lt;&gt;6) &amp;&amp; web_bookingcode__c &lt;&gt;&quot;BookingCode&quot;),web_bookingcode__c,&apos;&apos;)</formula>
        <name>Updateetkt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_Case_Subject_Update_FU</fullName>
        <field>Subject</field>
        <formula>TEXT(What_is_your_request__c)+&quot;&quot;+(IF(Which_Product_s__c != &apos;&apos;,(&quot; on &quot;+Which_Product_s__c+&quot;.&quot;),&quot;.&quot;))+(IF(What_is_your_Order_Number_if_available__c != &apos;&apos;,(&quot; Order No: &quot;+ What_is_your_Order_Number_if_available__c),&quot;&quot;))</formula>
        <name>Webshop Case Subject Update FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_Record_Type_Update_FU</fullName>
        <description>update the record type as Webshop for IPOB cases as well.</description>
        <field>RecordTypeId</field>
        <lookupValue>Webshop</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Webshop Record Type Update FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_Salutation_Update2_FU</fullName>
        <description>Update Salutation (Dutch) full-form so that it can be used in email templates</description>
        <field>Salutation__c</field>
        <literalValue>mevrouw</literalValue>
        <name>Webshop Salutation Update2 FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_Salutation_Update_FU</fullName>
        <description>Update Salutation (Dutch) full-form so that it can be used in email templates</description>
        <field>Salutation__c</field>
        <literalValue>meneer</literalValue>
        <name>Webshop Salutation Update FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_case_reason_to_Other</fullName>
        <description>Set webshop case reason to &apos;Other’</description>
        <field>Reason</field>
        <literalValue>Other</literalValue>
        <name>Webshop case reason to Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_case_sub_reason_to_Notification</fullName>
        <description>Set webshop case sub reason to &quot;Notification from Magento&quot;</description>
        <field>Sub_Reason__c</field>
        <literalValue>Notification from Magento</literalValue>
        <name>Webshop case sub reason to Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_queue</fullName>
        <description>Set the owner to Webshop Queue.</description>
        <field>OwnerId</field>
        <lookupValue>Webshop_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Webshop queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_set_status_to_Closed</fullName>
        <description>Webshop case set status to Closed when subject is Payment Transaction Failed Reminder</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Webshop set status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>dgAI2__DG_Capture_Analytics_Closed_Case_Update</fullName>
        <description>DG_Capture_Analytics__c checkbox should updated to true when Case Status equals Closed.</description>
        <field>dgAI2__DG_Capture_Analytics__c</field>
        <literalValue>1</literalValue>
        <name>DG_Capture_Analytics_Closed_Case_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iPOB_PA_Desk_Owner_assignement</fullName>
        <field>OwnerId</field>
        <lookupValue>Ipob_PA_Desk_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>iPOB PA Desk Owner assignement</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iPOB_medical_case_email</fullName>
        <field>iPOB_Medical_Case_Email__c</field>
        <formula>&apos;frontline@klm.com&apos;</formula>
        <name>iPOB medical case email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iPOB_owner_queue_assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>TA_Desk_INTL_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>iPOB owner queue assignment</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ipob_high_priority_owner_queue_assignmen</fullName>
        <field>OwnerId</field>
        <lookupValue>Ipob_High_Priority_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>ipob high priority owner queue assignmen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_email_address</fullName>
        <field>In_progress_email_alert__c</field>
        <formula>$User.Email</formula>
        <name>update email address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assign normal priority queue when ConnectingFlight date passed</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Connecting_Flight__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Return_Flight__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>For iPad on Board cases, if Connecting Flight Date time crossed then it should assign to Normal priority Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.Connecting_Flight_DateTime__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Assign normal priority queue when ReturnFlight date passed</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Return_Flight__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>For iPad on Board cases, if return Flight Date time crossed then it should assign to Normal priority Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Ipob_normal_priority_owner_assignment</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Case.Return_Flight_Date_time__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Assign to WG queue</fullName>
        <actions>
            <name>Assign_to_WG_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Wannagives</value>
        </criteriaItems>
        <description>When a wanngives order is created, it will be assigned to WG queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Reopened</fullName>
        <actions>
            <name>Set_Date_Reopened_to_Now</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>WF - Customer Care</description>
        <formula>NOT(IsClosed) &amp;&amp; (ISPICKVAL(PRIORVALUE(Status), &apos;Closed&apos;) || ISPICKVAL(PRIORVALUE(Status), &apos;Closed - No Response&apos;) || ISPICKVAL(PRIORVALUE(Status), &apos;Sent to Customer Care&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case_Engaged_Date_Time</fullName>
        <actions>
            <name>Case_Engaged_Date_Time_Set</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the engaged time the moment the state changes from open to another state</description>
        <formula>AND( OR( ISNULL( PRIORVALUE( Status )), ISPICKVAL(PRIORVALUE( Status ), &apos;New&apos;)) , NOT( ISPICKVAL( Status, &apos;New&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Data Prefill Field on Case closure</fullName>
        <actions>
            <name>Clear_Data_Prefill_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed,Closed - No Response,Closed By KLM Social Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Data_for_Prefill_By_DG__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DeltaAirlinesCaseStatus</fullName>
        <actions>
            <name>SetStatusToDelta</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.CaseNumber</field>
            <operation>equals</operation>
            <value>01953693</value>
        </criteriaItems>
        <description>Set Case status to &quot;Waiting for internal answer Delta&quot; , If Account is DeltaAirlines</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>E-Support Case Received From Field Update</fullName>
        <actions>
            <name>Update_Case_Received_From</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) Case Received From Field always keep up the first input (Case Origin) during the record creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email alert</fullName>
        <actions>
            <name>update_email_address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Division</field>
            <operation>equals</operation>
            <value>cygnific</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Servicing</value>
        </criteriaItems>
        <description>An email will be sent to the agents when the cases are in progress for more than two hours.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>email_alert</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Email to SMH on new Ipad On Board Case</fullName>
        <actions>
            <name>New_Ipad_on_Board_Case_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>iPad on Board</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Group update for AirFrance</fullName>
        <actions>
            <name>Group_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>contains</operation>
            <value>SCS - AF</value>
        </criteriaItems>
        <description>When changing the Owner,update the Group field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Heroku new support Case alert</fullName>
        <actions>
            <name>Clientele_New_Heroku_Case_alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele Heroku Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>New App Request,Support Request</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) When a new Heroku support case has been submitted an alert is sent</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IMO Ancillary Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_IMO_Ancillary</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Suppor_IMO_Ancillary_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>IMO Ancillary</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) IMO Ancillary Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IMO Ancillary Queue AutoReply</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>out of office</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Description</field>
            <operation>notContain</operation>
            <value>out of office</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>out of the office</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Description</field>
            <operation>notContain</operation>
            <value>out of the office</value>
        </criteriaItems>
        <description>Auto reply on the Email 2 Case with out of office filter test</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IMO E-Booking Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_IMO_E_Booking</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_IMO_E_Booking_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>IMO E-Booking</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) IMO E-Booking Queue Email Routing</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IMO Fam E-Acquisition Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_IMO_Fam_E_Acquisition</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_IMO_Fam_E_Acquisition_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>IMO Fam E-Acquisition</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) IMO Fam E-Acquisition Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IMO Heroku Queue</fullName>
        <actions>
            <name>E_Support_IMO_Heroku_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele Heroku Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Other</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) IMO Heroku Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IMO Mobile App Queue</fullName>
        <actions>
            <name>E_Support_Mobile_App_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mobile App</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) Mobile App Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IMO Mobile Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_IMO_Mobile</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_IMO_Mobile_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>IMO Mobile</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) IMO Mobile Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IMO Product Team Checkout Update</fullName>
        <actions>
            <name>Clientele_auto_reply_IMO_Support_Checkout_and_Payment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_IMO_Support_C_and_P_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>IMO PRODUCT TEAM CHECKOUT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) IMO Support Checkout and Payment Queue Email Routing</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Ipob high priority queue owner assignment</fullName>
        <actions>
            <name>ipob_high_priority_owner_queue_assignmen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName==&apos;iPad_on_Board_Case&apos;  &amp;&amp; (IF( AccountId !=null, IF( ISBLANK(TEXT(Account.TA_account__c)), True, False) , True)   &amp;&amp; IF(Subject!=null,!(CONTAINS( Subject , &apos;Pre-order Webshop&apos;)),True) &amp;&amp; (Text(Status) ==&apos;New&apos; || TEXT(Status)==&apos;Reopened&apos;) &amp;&amp; IF(Subject!=null,!(CONTAINS( Subject , &apos;Medical&apos;)),True) &amp;&amp; IF(Subject!=null,!(CONTAINS( Subject , &apos;Passenger &gt; injury&apos;)),True)) &amp;&amp; ((ISPICKVAL(Origin, &apos;Lounge Case&apos;)) || (Connecting_Flight__c ==true ||  Return_Flight__c ==True))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ipob normal priority queue assignment</fullName>
        <actions>
            <name>Ipob_normal_priority_owner_assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName==&apos;iPad_on_Board_Case&apos;  &amp;&amp;  IF( AccountId !=null,  IF( ISBLANK(TEXT(Account.TA_account__c)), True, False) , True)  &amp;&amp;  Connecting_Flight__c ==False &amp;&amp;  Return_Flight__c ==False &amp;&amp; IF(Subject!=null,!(CONTAINS( Subject , &apos;Pre-order Webshop&apos;)),True) &amp;&amp;  (Text(Status) ==&apos;New&apos; || Text(Status)==&apos;Reopened&apos;) &amp;&amp; IF(Subject!=null,!(CONTAINS( Subject , &apos;Medical&apos;)),True) &amp;&amp; IF(Subject!=null,!(CONTAINS( Subject , &apos;Passenger &gt; injury&apos;)),True) &amp;&amp; !( ISPICKVAL(Origin, &apos;Lounge Case&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mark as FlyingBlue</fullName>
        <actions>
            <name>Mark_as_FlyingBlue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SCS - KLM - Flying Blue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Servicing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Is_Flying_Blue__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>To Mark as the Is_Flying Blue field on owner change</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Medeam 1st line e-support Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_Medeam_1st_line_e_support</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_Medeam_1st_Line_e_Supp_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Medeam 1st line e-support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) Medeam 1st line e-support Queue Email Routing</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Medeam Mobile Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_Mobile</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_Medeam_Mobile_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Mobile</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) Medeam Mobile Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PA Desk Restricted Account Case Flag</fullName>
        <actions>
            <name>Restricted_Account_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PA-Desk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Restricted_Account__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Set Restricted Account Case flag for Cases that are related to Restricted Accounts.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PA-Desk - Case Ownership Manual Case Creation</fullName>
        <actions>
            <name>PA_Desk_Case_Ownership_to_PA_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PA-Desk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Subject__c</field>
            <operation>equals</operation>
            <value>E-Service,Flying Blue,Flying Blue Award,Others,Revenue,Service Element</value>
        </criteriaItems>
        <description>Change ownership to PA-Desk queue when Case Subject is not equal to &apos;Complaint&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PA-Desk - Close case for due date is Today</fullName>
        <actions>
            <name>PA_Desk_status_to_closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Due_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PA-Desk</value>
        </criteriaItems>
        <description>Logged case with due date equals today, set on status is close directly.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Remove default bookingcode</fullName>
        <actions>
            <name>Update_bookingcode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.web_bookingcode__c</field>
            <operation>equals</operation>
            <value>BookingCode</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Remove default email</fullName>
        <actions>
            <name>REmove_default_email_add</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Visitor_Email__c</field>
            <operation>equals</operation>
            <value>default@email.com</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Remove default fbnumber</fullName>
        <actions>
            <name>Update_FBNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Visitor_Flying_Blue_number__c</field>
            <operation>equals</operation>
            <value>FBNumber</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - CX Group Owner Change</fullName>
        <actions>
            <name>TWC_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When changing the Owner,update the Group field</description>
        <formula>OR( Owner:User.Division = &apos;CX&apos;,  CONTAINS( Owner:Queue.QueueName , &apos;CX&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - CX Group rule</fullName>
        <actions>
            <name>TWC_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>English</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Twitter</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notContain</operation>
            <value>SCS - AF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Agent Blue link,Sup Blue link</value>
        </criteriaItems>
        <description>Set Group field on Cases object based on criteria</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Channel update</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.CaseNumber</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Chatter Post to IGT</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Internal Answer IGT</value>
        </criteriaItems>
        <description>When the case status is changed to &quot;Waiting for IGT&quot; send a notification to the Queue</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Cygnific Group</fullName>
        <actions>
            <name>Cygnific_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 AND 2) OR (5 AND 6)) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>English,Dutch</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Facebook</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notContain</operation>
            <value>SCS - AF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Agent Blue link,Sup Blue link</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>Dutch</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Twitter</value>
        </criteriaItems>
        <description>Set Group field on Cases object based on criteria</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Cygnific Group Owner Change</fullName>
        <actions>
            <name>Cygnific_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When changing the Owner,update the Group field</description>
        <formula>OR( Owner:User.Division  = &apos;Cygnific&apos;,  CONTAINS( Owner:Queue.QueueName , &apos;CYG&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - IGT Group</fullName>
        <actions>
            <name>IGT_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR (5 AND 6)) AND 3 AND 4) OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>German,Japanese,Portuguese,Italian,Russian,Norwegian,French,Korean,Thai,Turkish,Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>WeChat</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notContain</operation>
            <value>SCS - AF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Agent Blue link,Sup Blue link</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>equals</operation>
            <value>English</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Twitter</value>
        </criteriaItems>
        <description>Set Group field on Cases object based on criteria</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - IGT Group Owner Change</fullName>
        <actions>
            <name>IGT_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When changing the Owner,update the Group field</description>
        <formula>OR( Owner:User.Division = &apos;IGT&apos;,  CONTAINS( Owner:Queue.QueueName , &apos;IGT&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Inform Digital team</fullName>
        <actions>
            <name>New_Digital_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Digital__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send an email to inform the digital team that they have a new case in their view</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Sent Reminder CC NL</fullName>
        <actions>
            <name>Sent_remider_to_CC_NL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Sent reminder to CC NL</value>
        </criteriaItems>
        <description>If status of IPOB case is &apos;send reminder CC NL&apos; an email to Customer Care Netherlands will be sent.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Statuschange to IGT B%2EO%2E</fullName>
        <actions>
            <name>Set_Statuschange_to_IGT_B_O_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Internal Answer IGT B.O.</value>
        </criteriaItems>
        <description>Set value for field &quot;Set statuschange to IGT B.O&quot; to TRUE, when case status has been set to &quot;Waiting for Internal Answer IGT B.O.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Subject to Case detail</fullName>
        <actions>
            <name>Set_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Detail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SetFlyingBlueToFalse</fullName>
        <actions>
            <name>SetFlyingBlueToFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed,Closed - No Response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Language__c</field>
            <operation>notEqual</operation>
            <value>English,Dutch</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Is_Flying_Blue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>To Set FlyingBlue to false if the language is not equal to English/Dutch, When case is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Shop Management Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_Shop_Management</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_Shop_Management_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Shop Management</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) Shop Management Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TCS_1st_line_e-support_Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_TCS_1st_line_e_support</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_TCS_1st_Line_e_Support_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>TCS 1st line e-support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) TCS 1st line e-support Queue Email Routing</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Trigger Email Notification to new owners</fullName>
        <actions>
            <name>Trigger_Email_Notification_to_new_owners</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 4) AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Notify_New_Owner__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>TA Desk INTL Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Ipob PA Desk Queue</value>
        </criteriaItems>
        <description>To notify Queue Email address when case is created by tripreport via MyFlight app</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update PNR</fullName>
        <actions>
            <name>UpdatePNR</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Updateetkt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Webchat</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>User Sub-Division at Case level</fullName>
        <actions>
            <name>Sub_Division_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Servicing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Sub_Division__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Webshop Assignment</fullName>
        <actions>
            <name>Webshop_Record_Type_Update_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Webshop_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR (2 AND 3) OR 4</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>pre-order webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Webshop Queue</value>
        </criteriaItems>
        <description>The webshop cases are assigned to webshop queue when the case is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Webshop Case Reopen Owner Update WFR</fullName>
        <actions>
            <name>Webshop_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR (3 AND 4))</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Reopened,Answer Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>pre-order webshop</value>
        </criteriaItems>
        <description>Update the owner as Webshop Queue when a webshop case is Reopened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Webshop Case Subject Update WFR</fullName>
        <actions>
            <name>Webshop_Case_Subject_Update_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Webshop Form,Wannagives Form</value>
        </criteriaItems>
        <description>Update subject for cases coming in through webshop and wannagives websites.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Webshop Queue</fullName>
        <actions>
            <name>Clientele_auto_reply_Webshop</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>E_Support_Webshop_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <description>E-Support old name was (Clientele) Webshop Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Webshop Salutation Update WFR</fullName>
        <actions>
            <name>Webshop_Salutation_Update_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR (3 AND 4))</booleanFilter>
        <criteriaItems>
            <field>Case.Salutation__c</field>
            <operation>equals</operation>
            <value>dhr.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>pre-order webshop</value>
        </criteriaItems>
        <description>Update Salutation (Dutch) full-form so that it can be used in email templates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Webshop Salutation Update2 WFR</fullName>
        <actions>
            <name>Webshop_Salutation_Update2_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR (3 AND 4))</booleanFilter>
        <criteriaItems>
            <field>Case.Salutation__c</field>
            <operation>equals</operation>
            <value>mevr.</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>pre-order webshop</value>
        </criteriaItems>
        <description>Update Salutation (Dutch) full-form so that it can be used in email templates</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Webshop cases Payment Transaction Failed</fullName>
        <actions>
            <name>Webshop_case_reason_to_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Webshop_case_sub_reason_to_Notification</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Webshop_set_status_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>equals</operation>
            <value>Payment Transaction Failed Reminder</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Webshop</value>
        </criteriaItems>
        <description>When a webshop case contains for subject &apos;Payment Transaction Failed Reminder’ than set case reason to ‘Other’ and sub reason ‘Notification from Magento’.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Webshop%3A Notification to SMH</fullName>
        <actions>
            <name>New_Webshop_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Send an email to socialmediahub@klm.com to inform about a new Webshop case.</description>
        <formula>RecordType.DeveloperName == &apos;Webshop&apos; &amp;&amp; ISCHANGED(Last_Email_Received_Date__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>dgAI2__DG_Capture_Analytics_Closed_Case</fullName>
        <actions>
            <name>dgAI2__DG_Capture_Analytics_Closed_Case_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>DG_Capture_Analytics__c checkbox should updated to true when Case Status equals Closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>iPOB Medical Case</fullName>
        <actions>
            <name>iPOB_Medical_cases_forwarded_to_frontlinestaff</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Reason_for_ipob_medical_cases</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_as_Closed_customer_care_info</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sub_reason_for_ipob_medical_cases</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>iPOB_medical_case_email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>2 AND (1 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Medical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>contains</operation>
            <value>Passenger &gt; injury</value>
        </criteriaItems>
        <description>To close the ipob medical cases automatically and send a mail to frontline@klm.com</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>iPOB PA Desk Queue Owner assignement</fullName>
        <actions>
            <name>Notify_Owner_checkbox_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>iPOB_PA_Desk_Owner_assignement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TA_account__c</field>
            <operation>equals</operation>
            <value>Travel assistance NL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Pre-order Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Reopened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Medical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Passenger &gt; injury</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>iPOB TA Desk Queue Owner assignement</fullName>
        <actions>
            <name>Notify_Owner_checkbox_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>iPOB_owner_queue_assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TA_account__c</field>
            <operation>equals</operation>
            <value>Travel Assistance INTL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Pre-order Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Reopened</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Medical</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Subject</field>
            <operation>notContain</operation>
            <value>Passenger &gt; injury</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
