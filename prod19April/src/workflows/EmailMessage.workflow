<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CaseLastEmailReceivedDate</fullName>
        <description>Clientele Update field Last Email Received Date</description>
        <field>Last_Email_Received_Date__c</field>
        <formula>NOW()</formula>
        <name>CaseLastEmailReceivedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CaseLastEmailSendDate</fullName>
        <description>Clientele If is outbound E-mail set a Date-Time field on the parent Case if connected</description>
        <field>Last_Email_Send_Date__c</field>
        <formula>NOW()</formula>
        <name>CaseLastEmailSendDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CaseStatusReOpen</fullName>
        <description>Reopen the Case status</description>
        <field>Status</field>
        <literalValue>Reopened</literalValue>
        <name>CaseStatusReOpen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_new_email_webshop_checkbox</fullName>
        <field>New_Webshop_email__c</field>
        <literalValue>1</literalValue>
        <name>Check &apos;new email webshop&apos; checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopen_the_Status_field</fullName>
        <field>Status</field>
        <literalValue>Reopened</literalValue>
        <name>Reopen the Status field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Webshop_Case_Status_Update_to_Replied_FU</fullName>
        <description>update case status to Replied when there is a customer or internal response</description>
        <field>Status</field>
        <literalValue>Answer Received</literalValue>
        <name>Webshop Case Status Update to Replied FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>EmailMessageParentCaseUpdate</fullName>
        <actions>
            <name>CaseLastEmailReceivedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele,Clientele Heroku Support,Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Clientele If is inbound E-mail set a Date-Time field on the parent Case if connected</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EmailMessageParentCaseUpdate2</fullName>
        <actions>
            <name>CaseLastEmailSendDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele,Clientele Heroku Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Clientele If is outbound E-mail set a Date-Time field on the parent Case if connected</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email_ReopenCase</fullName>
        <actions>
            <name>CaseStatusReOpen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR (3 AND 4))</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Clientele</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Handled &amp; Closed,Closed within 5 days,Closed without solution</value>
        </criteriaItems>
        <description>Clientele Reopen case on Email receive</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reopen IPOB and Webshop cases</fullName>
        <actions>
            <name>Reopen_the_Status_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>iPad on Board Case,Webshop</value>
        </criteriaItems>
        <description>Reopening of IPOB cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Webshop Replied status on response update WFR</fullName>
        <actions>
            <name>Webshop_Case_Status_Update_to_Replied_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Webshop</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Client Answer,Waiting for Internal Answer KLM</value>
        </criteriaItems>
        <description>Update the case status to Replied when there is a response from customer or internal team</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Webshop%3A  Check %27new webshop mail%27</fullName>
        <actions>
            <name>Check_new_email_webshop_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Checks the field &apos;new webshop&apos; mail to true in order to activate the workflow &apos;Webshop: Sent email to SMH&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
