<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>dgAI2__DG_Capture_Analytics_Closed_Lead_Update</fullName>
        <description>DG_Capture_Analytics__c checkbox should updated to true when Case Status equals Closed</description>
        <field>dgAI2__DG_Capture_Analytics__c</field>
        <literalValue>1</literalValue>
        <name>DG_Capture_Analytics_Closed_Lead_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>dgAI2__DG_Capture_Analytics_Closed_Lead</fullName>
        <actions>
            <name>dgAI2__DG_Capture_Analytics_Closed_Lead_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>contains</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>DG_Capture_Analytics__c checkbox should updated to true when Case Status equals Closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
