<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_power_Plug_Flag</fullName>
        <field>Power_Plug__c</field>
        <literalValue>1</literalValue>
        <name>Set power Plug Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_wifi_to_true</fullName>
        <field>Wi_Fi__c</field>
        <literalValue>1</literalValue>
        <name>Set wifi to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LTC Set Power Plug Info%2E</fullName>
        <actions>
            <name>Set_power_Plug_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evaluate the rule when a record is created</description>
        <formula>(Aircraft_type__c != Null) &amp;&amp; (Aircraft_type__r.Power_Plug__c == true)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LTC set aircraft wifi to true</fullName>
        <actions>
            <name>Set_wifi_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set aircarft wifi to true if aircrafttype has wifi as true</description>
        <formula>(Aircraft_type__c != Null) &amp;&amp; ( Aircraft_type__r.WIFI__c == true)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
