<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>LtcSendEmailQueueMembers</fullName>
        <description>Ltc Send queue members an email on puting a ratingin the queue</description>
        <protected>false</protected>
        <recipients>
            <recipient>Like_The_Crew_Ratings</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>noreply.flightguide@klm.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Ltc_Queue_Rating_Received</template>
    </alerts>
    <fieldUpdates>
        <fullName>Comment2</fullName>
        <description>from 255 to 508</description>
        <field>Positive_Comment_2__c</field>
        <formula>MID( Positive_comments__c , 251, 250)</formula>
        <name>Comment2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comment3</fullName>
        <field>Positive_Comment_3__c</field>
        <formula>MID( Positive_comments__c , 501,250)</formula>
        <name>Comment3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comment4</fullName>
        <field>Positive_Comment_4__c</field>
        <formula>MID( Positive_comments__c , 751,250)</formula>
        <name>Comment4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Neg_Comment1</fullName>
        <field>Negative_Comment_1__c</field>
        <formula>MID( Negative_comments__c , 0, 250)</formula>
        <name>Neg Comment1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Neg_Comment2</fullName>
        <field>Negative_Comment_2__c</field>
        <formula>MID( Negative_comments__c , 251, 250)</formula>
        <name>Neg Comment2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Neg_Comment3</fullName>
        <field>Negative_Comment_3__c</field>
        <formula>MID( Negative_comments__c , 501, 250)</formula>
        <name>Neg Comment3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Neg_Comment4</fullName>
        <field>Negative_Comment_4__c</field>
        <formula>MID( Negative_comments__c , 751,250)</formula>
        <name>Neg Comment4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Negatice_Comm_Length</fullName>
        <field>Neg_Comments_length__c</field>
        <formula>LEN( Negative_comments__c )</formula>
        <name>Negatice Comm Length</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Positive_Comm_Length</fullName>
        <field>Comment_Length__c</field>
        <formula>LEN(Positive_comments__c)</formula>
        <name>Positive Comm Length</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerId</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_RU_UK</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdRU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdCN</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_CN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdCN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdDE</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_DE</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdDE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdEN</fullName>
        <description>Place newly created Ratings in to the rating queue</description>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_NL_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdEN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdES</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_ES</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdFR</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_FR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdFR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdIT</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_IT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdIT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdJP</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_JP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdJP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdKR</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_KR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdKR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdNO</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_NO</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdNO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdPT</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_PT_BR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdPT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdTH</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_TH</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdTH</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdTR</fullName>
        <field>OwnerId</field>
        <lookupValue>Rate_your_flight_TR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdTR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRatingOwnerIdUnsupportedLanguage</fullName>
        <field>OwnerId</field>
        <lookupValue>RYF_unsupprted_language</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SetRatingOwnerIdUnsupportedLanguage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>split_comment</fullName>
        <field>Positive_Comment_1__c</field>
        <formula>MID( Positive_comments__c , 0, 250)</formula>
        <name>split comment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LTC_split Comments</fullName>
        <actions>
            <name>Comment2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comment3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comment4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Neg_Comment1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Neg_Comment2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Neg_Comment3</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Neg_Comment4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Negatice_Comm_Length</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Positive_Comm_Length</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>split_comment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Rating__c.Positive_comments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Rating__c.Negative_comments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueue</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdEN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the EN NL queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, OR(language__c = &apos;en&apos;, language__c = &apos;nl&apos;)), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueCN</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdCN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the CN queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;cn&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueDE</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdDE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the DE queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;de&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueES</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdES</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the ES queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;es&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueFR</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdFR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the FR queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;fr&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueIT</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdIT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the IT queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;it&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueJP</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdJP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the JP queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;jp&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ),CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueKR</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdKR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the KR queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;kr&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueNO</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdNO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the NO queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;no&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueuePT</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdPT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the PT BR queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, OR(language__c = &apos;pt&apos;, language__c = &apos;br&apos;)), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueRU</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the RU UK queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, OR(language__c = &apos;ru&apos;, language__c = &apos;uk&apos;)), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueTH</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdTH</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the TH queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;th&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueTR</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdTR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the TR queue</description>
        <formula>AND(AND(Rating_Number__c &lt;&gt; 0, language__c = &apos;tr&apos;), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LtcRatingQueueUnsuportedLanguage</fullName>
        <actions>
            <name>LtcSendEmailQueueMembers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>SetRatingOwnerIdUnsupportedLanguage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Put all newly created Ratings into the Unsupported Language queue</description>
        <formula>AND(NOT(OR(language__c = &apos;nl&apos;,language__c = &apos;en&apos;,language__c = &apos;es&apos;,language__c = &apos;pt&apos;,language__c = &apos;br&apos;,language__c = &apos;ru&apos;,language__c = &apos;uk&apos;,language__c = &apos;it&apos;,language__c = &apos;fr&apos;,language__c = &apos;de&apos;,language__c = &apos;kr&apos;,language__c = &apos;th&apos;,language__c = &apos;jp&apos;,language__c = &apos;cn&apos;,language__c = &apos;tr&apos;,language__c = &apos;no&apos;)), OR(!ISBLANK(Positive_comments__c), !ISBLANK(Negative_comments__c),  ! ISBLANK( Passenger__r.Family_Name__c ), ! ISBLANK( Passenger__r.First_Name__c ) ), CONTAINS( UPPER(Flight_Number__c) , UPPER(&apos;KL&apos;)) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
