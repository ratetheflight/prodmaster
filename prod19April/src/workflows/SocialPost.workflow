<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_when_Social_Post_is_not_sent</fullName>
        <description>Alert when Social Post is not sent</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SCS_Notify_when_SP_is_failed</template>
    </alerts>
    <alerts>
        <fullName>Sends_an_email_to_SocialMediaHub_team</fullName>
        <ccEmails>twc-klm@webcarecompany.com</ccEmails>
        <description>Sends an email to SocialMediaHub team</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SMH_DeltaAssist_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>AF_Group_to_Null</fullName>
        <description>Group field to Null</description>
        <field>Group__c</field>
        <name>AF Group to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_KLM_UK_queue</fullName>
        <description>Assign KLM_UK tweets and field update language equals english and put on KLM_UK english queue</description>
        <field>OwnerId</field>
        <lookupValue>SCS_KL_KLM_UK</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to KLM_UK queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Owner_to_Unsupported</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Unsupported</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Owner to Unsupported</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Turkish_queue</fullName>
        <description>Assign a case to the Turkish queue</description>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_TR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign Turkish queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Chinese_Queue</fullName>
        <description>When post tag contains &apos;AF-Chinese&apos; set owner to &apos;SCS - AF - Chinese&apos; queue and set language to &apos;Chinese&apos;</description>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Chinese</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF Chinese Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_English_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_English</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF English Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_French_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_French</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF French Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_German_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_German</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF German Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Italian_Queue</fullName>
        <description>When post tag contains &apos;Italian&apos; set owner to &apos;SCS - AF - Italian&apos; queue and set language to &apos;Italian&apos;</description>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Italian</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF Italian Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Japanese_Queue</fullName>
        <description>When post tag contains AF Japanese&apos; set owner to &apos;SCS - AF - Japanese&apos; queue and set language to Japanese&apos;</description>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Japanese</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF Japanese Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Korean_Queue</fullName>
        <description>When post tag contains AF-Korean&apos; set owner to &apos;SCS - AF - Korean&apos; queue and set language to &apos;Korean&apos;</description>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Korean</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF Korean Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Newsroom_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Newsroom</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF Newsroom Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Portuguese_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Portuguese</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF Portuguese Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Spanish_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Spanish</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF Spanish Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_AF_Unpublished_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_AF_Unpublished_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to AF - Unpublished Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_CYG_Dutch</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_CYG_NL</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to CYG Dutch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_CYG_English</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_CYG_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to CYG English</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Dutch_Queue_TW</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_TWC_NL</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Dutch Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_English_Queue_TW</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_TWC_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to English Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_French_Queue_2</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_FR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to French Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_German_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_DE</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to German Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_IGT_English_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_EN</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to IGT English Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_IGT_Spanish_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_ES</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to IGT Spanish Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Italian_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_IT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Italian Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Japenese_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_JP</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Japenese Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_KLM_Unpublished_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>niels.jonker@klm.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Assign to KLM - Unpublished Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Korean_queue</fullName>
        <description>Assign a case to the Korean queue</description>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_KO</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Korean queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Norwegian_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_Unsupported_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unsupported Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Portuguese_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_PT</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Portuguese Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_RUS_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_Unsupported_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unsupported queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Spanish_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_ES</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Spanish Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Thai_queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_IGT_TH</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Thai queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Unsupported_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_Unsupported_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unsupported Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Unsuppported_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_Unsupported_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unsuppported Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Unsuppported_Queue_Chinese</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_Unsupported_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unsuppported Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Unsuppported_Queue_Thai</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_Unsupported_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unsuppported Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_Unsuppported_Queue_Turkish</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_Unsupported_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Unsuppported Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_WhatsApp_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>SCS_WhatsApp</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to WhatsApp Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_company_name_to_AirFrance</fullName>
        <field>Company__c</field>
        <literalValue>AirFrance</literalValue>
        <name>Change company name to AirFrance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cygnific_Group</fullName>
        <field>Group__c</field>
        <literalValue>Cygnific</literalValue>
        <name>Cygnific Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>H2H_campaign_Ignore_SLA</fullName>
        <field>Ignore_for_SLA__c</field>
        <literalValue>1</literalValue>
        <name>H2H campaign - Ignore SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IGT_Group</fullName>
        <field>Group__c</field>
        <literalValue>IGT</literalValue>
        <name>IGT Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ignore_for_SLA</fullName>
        <description>Set checkbox &apos;Ignore for SLA&apos; to true</description>
        <field>Ignore_for_SLA__c</field>
        <literalValue>1</literalValue>
        <name>SCS - Ignore for SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_Group_update_Cygnific</fullName>
        <field>Initial_Group__c</field>
        <literalValue>Cygnific</literalValue>
        <name>Initial Group update Cygnific</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Initial_Group_update_IGT</fullName>
        <field>Initial_Group__c</field>
        <literalValue>IGT</literalValue>
        <name>Initial Group update IGT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Social_Post_as_Spam</fullName>
        <field>SCS_Spam__c</field>
        <literalValue>1</literalValue>
        <name>Mark Social Post as Spam</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marksocial_post_as_spam</fullName>
        <field>SCS_Spam__c</field>
        <literalValue>1</literalValue>
        <name>Mark social post as spam</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetIgnore_for_SLA_to_true</fullName>
        <field>Ignore_for_SLA__c</field>
        <literalValue>1</literalValue>
        <name>Set Ignore for SLA to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetMAR_to_True</fullName>
        <field>SCS_MarkAsReviewed__c</field>
        <literalValue>1</literalValue>
        <name>Set MAR to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Ignore_for_SLA_to_true</fullName>
        <field>Ignore_for_SLA__c</field>
        <literalValue>1</literalValue>
        <name>Set Ignore for SLA to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MAR_to_true</fullName>
        <field>SCS_MarkAsReviewed__c</field>
        <literalValue>1</literalValue>
        <name>Set MAR to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Replied_date_to_NOW</fullName>
        <field>Replied_date__c</field>
        <formula>NOW()</formula>
        <name>Set Replied date to NOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Chinese</fullName>
        <field>Language__c</field>
        <literalValue>Chinese</literalValue>
        <name>Set language to Chinese</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Dutch</fullName>
        <field>Language__c</field>
        <literalValue>Dutch</literalValue>
        <name>Set language to Dutch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_English</fullName>
        <field>Language__c</field>
        <literalValue>English</literalValue>
        <name>Set language to English</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_English_TW</fullName>
        <field>Language__c</field>
        <literalValue>English</literalValue>
        <name>Set language to English</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_French</fullName>
        <field>Language__c</field>
        <literalValue>French</literalValue>
        <name>Set language to French</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_German</fullName>
        <field>Language__c</field>
        <literalValue>German</literalValue>
        <name>Set language to German</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Italian</fullName>
        <field>Language__c</field>
        <literalValue>Italian</literalValue>
        <name>Set language to Italian</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Japanese</fullName>
        <field>Language__c</field>
        <literalValue>Japanese</literalValue>
        <name>Set language to Japanese</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Korean</fullName>
        <field>Language__c</field>
        <literalValue>Korean</literalValue>
        <name>Set language to Korean</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Norwegian</fullName>
        <field>Language__c</field>
        <literalValue>Unsupported</literalValue>
        <name>Set language to Unsupported</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Portuguese</fullName>
        <field>Language__c</field>
        <literalValue>Portuguese</literalValue>
        <name>Set language to Portuguese</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Russian</fullName>
        <field>Language__c</field>
        <literalValue>Unsupported</literalValue>
        <name>Set language to Unsupported</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Spanish</fullName>
        <field>Language__c</field>
        <literalValue>Spanish</literalValue>
        <name>Set language to Spanish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Thai</fullName>
        <field>Language__c</field>
        <literalValue>Thai</literalValue>
        <name>Set language to Thai</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Turkish</fullName>
        <field>Language__c</field>
        <literalValue>Turkish</literalValue>
        <name>Set language to Turkish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Unsupported</fullName>
        <field>Language__c</field>
        <literalValue>Unsupported</literalValue>
        <name>Set language to Unsupported</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_Unsupported2</fullName>
        <field>Language__c</field>
        <literalValue>Unsupported</literalValue>
        <name>Set language to Unsupported</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_language_to_unsup</fullName>
        <field>Language__c</field>
        <literalValue>Unsupported</literalValue>
        <name>Set language to unsup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_langugage_to_Dutch_TW</fullName>
        <field>Language__c</field>
        <literalValue>Dutch</literalValue>
        <name>Set langugage to Dutch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TWC_Group</fullName>
        <field>Group__c</field>
        <literalValue>CX</literalValue>
        <name>CX Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Agent_status_to_Mark_as_reviewed</fullName>
        <field>SCS_Status__c</field>
        <literalValue>Mark as reviewed</literalValue>
        <name>Update Agent status to Mark as reviewed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SCS_Status</fullName>
        <field>Status_SCS__c</field>
        <formula>TEXT(Status)</formula>
        <name>Update SCS Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SCS_Status_to_Replied</fullName>
        <field>Status_SCS__c</field>
        <formula>Text(Status)</formula>
        <name>Update SCS Status to Replied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SCS_Status_to_Sent</fullName>
        <field>Status_SCS__c</field>
        <formula>Text(Status)</formula>
        <name>Update SCS Status to Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SCS_Tag</fullName>
        <field>SCS_Tag__c</field>
        <formula>PostTags</formula>
        <name>Update SCS Tag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_company_name_to_KL</fullName>
        <field>Company__c</field>
        <literalValue>KLM</literalValue>
        <name>Update company name to KL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_language</fullName>
        <field>Language__c</field>
        <literalValue>Unpublished</literalValue>
        <name>Update language</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_language_AF</fullName>
        <field>Language__c</field>
        <literalValue>Unpublished</literalValue>
        <name>Update language</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>unsup</fullName>
        <field>Language</field>
        <name>unsup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AirFrance Company Change</fullName>
        <actions>
            <name>AF_Group_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Change_company_name_to_AirFrance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.OwnerId</field>
            <operation>contains</operation>
            <value>SCS - AF</value>
        </criteriaItems>
        <description>When changing the Owner to AF queue the update the Company and Group field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>H2H campaign - Ignore SLA</fullName>
        <actions>
            <name>H2H_campaign_Ignore_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>H2H Campaign</value>
        </criteriaItems>
        <description>Set ignore for SLA to true if the post is regarding H2H campaign</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Rule SCS - AF - Newsroom English Logic</fullName>
        <actions>
            <name>Assign_to_AF_Newsroom_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_English_TW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-NewsroomEN</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Newsroom&apos; set owner to &apos;SCS - AF - Newsroom&apos; queue and set language to &apos;AF-English&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Chinese Logic</fullName>
        <actions>
            <name>Assign_to_AF_Chinese_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Chinese</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Chinese</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Chinese&apos; set owner to &apos;SCS - AF -Chinese&apos; queue and set language to &apos;Chinese&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - English Logic</fullName>
        <actions>
            <name>Assign_to_AF_English_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_English</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-English</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-English&apos; set owner to &apos;SCS - AF - English&apos; queue and set language to &apos;English&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - French Logic</fullName>
        <actions>
            <name>Assign_to_AF_French_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_French</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-French</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-French&apos; set owner to &apos;SCS - AF - French&apos; queue and set language to &apos;French</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - German Logic</fullName>
        <actions>
            <name>Assign_to_AF_German_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_German</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-German</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-German&apos; set owner to &apos;SCS - AF - German&apos; queue and set language to &apos;German&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Italian Logic</fullName>
        <actions>
            <name>Assign_to_AF_Italian_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Italian</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Italian</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Italian&apos; set owner to &apos;SCS - AF - Italian&apos; queue and set language to &apos;Italian&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Japanese Logic</fullName>
        <actions>
            <name>Assign_to_AF_Japanese_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Japanese</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Japanese</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Japanese&apos; set owner to &apos;SCS - AF -Japanese&apos; queue and set language to &apos;Japanese&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Korean Logic</fullName>
        <actions>
            <name>Assign_to_AF_Korean_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Korean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Korean</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Korean&apos; set owner to &apos;SCS - AF -Korean&apos; queue and set language to &apos;Korean&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Newsroom French Logic</fullName>
        <actions>
            <name>Assign_to_AF_Newsroom_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_French</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-NewsroomFR</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Newsroom&apos; set owner to &apos;SCS - AF - Newsroom&apos; queue and set language to &apos;AF-French&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Portuguese Logic</fullName>
        <actions>
            <name>Assign_to_AF_Portuguese_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Portuguese</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Portuguese</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Portuguese&apos; set owner to &apos;SCS - AF - Portuguese&apos; queue and set language to &apos;Portuguese&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Spanish Logic</fullName>
        <actions>
            <name>Assign_to_AF_Spanish_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Spanish</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Spanish</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Spanish&apos; set owner to &apos;SCS - AF - Spanish&apos; queue and set language to &apos;Spanish&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Unpublished logic</fullName>
        <actions>
            <name>Assign_to_AF_Unpublished_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_language_AF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Unpublished</value>
        </criteriaItems>
        <description>When the post tag contains &quot;AF-Unpublished&quot;, assign the social post to the &quot;AF Unpublished Queue&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - AF - Unsupported Logic</fullName>
        <actions>
            <name>Assign_Owner_to_Unsupported</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Unsupported</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>AF-Unsupported</value>
        </criteriaItems>
        <description>When post tag contains &apos;AF-Unsupported&apos; set owner to &apos;SCS - AF -Unsupported&apos; queue and set language to &apos;Unsupported&apos;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - CX Group Owner Change</fullName>
        <actions>
            <name>TWC_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_company_name_to_KL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When changing the Owner,update the Group field</description>
        <formula>OR( Owner:User.Division = &apos;CX&apos;,  CONTAINS( Owner:Queue.QueueName , &apos;CX&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - CX Group rule</fullName>
        <actions>
            <name>TWC_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>Dutch</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Provider</field>
            <operation>equals</operation>
            <value>Twitter</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Company__c</field>
            <operation>notEqual</operation>
            <value>AirFrance</value>
        </criteriaItems>
        <description>Assign Social Posts to the CX group based on criteria</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Chinese logic</fullName>
        <actions>
            <name>Assign_to_Unsuppported_Queue_Chinese</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Chinese</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Chinese</value>
        </criteriaItems>
        <description>When the post tag contains KLM - Chinese, assign the social post to the Chinese Queue and set language to Chinese</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Cygnific Group Owner Change</fullName>
        <actions>
            <name>Cygnific_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_company_name_to_KL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When changing the Owner,update the Group field</description>
        <formula>OR( Owner:User.Division = &apos;Cygnific&apos;,  CONTAINS( Owner:Queue.QueueName , &apos;CYG&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Cygnific Group rule</fullName>
        <actions>
            <name>Cygnific_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>Dutch,English</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Provider</field>
            <operation>equals</operation>
            <value>Facebook,Twitter</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Company__c</field>
            <operation>notEqual</operation>
            <value>AirFrance</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Post_Tags__c</field>
            <operation>notEqual</operation>
            <value>IGT English</value>
        </criteriaItems>
        <description>Assign Social Posts to the Cygnific group based on criteria</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Cygnific Initial Group rule</fullName>
        <actions>
            <name>Initial_Group_update_Cygnific</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>Dutch,English</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Provider</field>
            <operation>equals</operation>
            <value>Facebook,Twitter</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Company__c</field>
            <operation>notEqual</operation>
            <value>AirFrance</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Post_Tags__c</field>
            <operation>notEqual</operation>
            <value>IGT English</value>
        </criteriaItems>
        <description>Assign Social Posts to the Cygnific group based on criteria</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Facebook Dutch logic</fullName>
        <actions>
            <name>Assign_to_CYG_Dutch</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Dutch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Dutch</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Social_Network__c</field>
            <operation>contains</operation>
            <value>facebook,Twitter</value>
        </criteriaItems>
        <description>When the post tag contains Dutch, assign the social post to the Dutch Queue and set language to Dutch - FACEBOOK</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Facebook English Logic</fullName>
        <actions>
            <name>Assign_to_CYG_English</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_English</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>English</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Social_Network__c</field>
            <operation>contains</operation>
            <value>facebook</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Post_Tags__c</field>
            <operation>notEqual</operation>
            <value>IGT English</value>
        </criteriaItems>
        <description>When the post tag contains English, assign the social post to the English Queue and set language to English - FACEBOOK</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - French logic</fullName>
        <actions>
            <name>Assign_to_French_Queue_2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_French</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>French</value>
        </criteriaItems>
        <description>When the post tag contains French, assign the social post to the French Queue and set language to French</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - German logic</fullName>
        <actions>
            <name>Assign_to_German_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_German</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>German</value>
        </criteriaItems>
        <description>When the post tag contains German, assign the social post to the German Queue and set language to German</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - IGT English logic</fullName>
        <actions>
            <name>Assign_to_IGT_English_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_English</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>IGT English</value>
        </criteriaItems>
        <description>When the post tag contains IGT Enlish, assign the social post to the English IGT Queue and set language to English.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - IGT Group Owner Change</fullName>
        <actions>
            <name>IGT_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_company_name_to_KL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When changing the Owner,update the Group field</description>
        <formula>OR( Owner:User.Division = &apos;IGT&apos;,  CONTAINS( Owner:Queue.QueueName , &apos;IGT&apos;) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - IGT Group rule</fullName>
        <actions>
            <name>IGT_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR 3) AND 2) OR 4</booleanFilter>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>French,Portuguese,Norwegian,Japanese,German,Italian,Russian,Korean,Thai,Unsupported,Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Company__c</field>
            <operation>notEqual</operation>
            <value>AirFrance</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Post_Tags__c</field>
            <operation>equals</operation>
            <value>IGT English</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Provider</field>
            <operation>equals</operation>
            <value>WeChat</value>
        </criteriaItems>
        <description>Assign Social Posts to the IGT group based on criteria</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - IGT Initial Group rule</fullName>
        <actions>
            <name>Initial_Group_update_IGT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 OR 3) AND 2) OR 4</booleanFilter>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>French,Portuguese,Norwegian,Japanese,German,Italian,Russian,Korean,Thai,Unsupported,Spanish</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Company__c</field>
            <operation>notEqual</operation>
            <value>AirFrance</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Post_Tags__c</field>
            <operation>equals</operation>
            <value>IGT English</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Provider</field>
            <operation>equals</operation>
            <value>WeChat</value>
        </criteriaItems>
        <description>Assign Social Posts to the IGT group based on criteria</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Italian logic</fullName>
        <actions>
            <name>Assign_to_Italian_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Italian</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Italian</value>
        </criteriaItems>
        <description>When the post tag contains Italian, assign the social post to the Italian Queue and set language to Italian</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Japenese logic</fullName>
        <actions>
            <name>Assign_to_Japenese_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Japanese</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Japanese</value>
        </criteriaItems>
        <description>When the post tag contains Japenese, assign the social post to the Japenese Queue and set language to Japenese</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - KLM - Unpublished logic</fullName>
        <actions>
            <name>Assign_to_KLM_Unpublished_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_language</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>KLM-Unpublished</value>
        </criteriaItems>
        <description>When the post tag contains &quot;KLM-Unpublished&quot;, assign the social post to the &quot;KLM Unpublished Queue&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Korean logic</fullName>
        <actions>
            <name>Assign_to_Korean_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Korean</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Korean</value>
        </criteriaItems>
        <description>When the post tag contains Korean, assign the social post to the KoreanQueue and set language to Korean</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Norwegian logic</fullName>
        <actions>
            <name>Assign_to_Norwegian_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Norwegian</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Norwegian</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>Norwegian</value>
        </criteriaItems>
        <description>When the post tag contains Norwegian, assign the social post to the Norwegian Queue and set language to Norwegian</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Notify when SP is failed</fullName>
        <actions>
            <name>Alert_when_Social_Post_is_not_sent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.Status</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <description>When the Social Post status is failed, notify the SP owner</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Other supported languages</fullName>
        <actions>
            <name>Update_SCS_Tag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>other supported languages</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Portuguese logic</fullName>
        <actions>
            <name>Assign_to_Portuguese_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Portuguese</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Portuguese</value>
        </criteriaItems>
        <description>When the post tag contains Portuguese, assign the social post to the Portuguese Queue and set language to Portuguese</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Reply speed</fullName>
        <actions>
            <name>Set_Replied_date_to_NOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.Status</field>
            <operation>equals</operation>
            <value>Sent,Replied</value>
        </criteriaItems>
        <description>When the social post status equals SENT or REPLIED, set the Reply Date to NOW()</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Russian logic</fullName>
        <actions>
            <name>Assign_to_RUS_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Russian</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Russian</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>Russian</value>
        </criteriaItems>
        <description>When the post tag contains Russian, assign the social post to the Unsupported Queue and set language to Unsupported</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Spanish logic</fullName>
        <actions>
            <name>Assign_to_Spanish_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Spanish</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Spanish</value>
        </criteriaItems>
        <description>When the post tag contains Spanish, assign the social post to the Spanish Queue and set language to Spanish</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Thai logic</fullName>
        <actions>
            <name>Assign_to_Thai_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Thai</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Thai</value>
        </criteriaItems>
        <description>When the post tag contains Thai , assign the social post to the Thai Queue and set language to Thai</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Turkish logic</fullName>
        <actions>
            <name>Assign_Turkish_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Turkish</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Turkish</value>
        </criteriaItems>
        <description>When the post tag contains Turkish, assign the social post to the Turkish Queue and set language to Turkish</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Twitter Dutch logic</fullName>
        <actions>
            <name>Assign_to_Dutch_Queue_TW</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_langugage_to_Dutch_TW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Dutch</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Social_Network__c</field>
            <operation>contains</operation>
            <value>Twitter</value>
        </criteriaItems>
        <description>When the post tag contains Dutch, assign the social post to the Dutch Queue and set language to Dutch - TWITTER</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Twitter English KLM_UK Logic</fullName>
        <actions>
            <name>Assign_KLM_UK_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_English_TW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>English</value>
        </criteriaItems>
        <description>When the posttag contains English assign the social post to the KLM_UK Queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Twitter English logic</fullName>
        <actions>
            <name>Assign_to_English_Queue_TW</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_English_TW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>English</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Social_Network__c</field>
            <operation>contains</operation>
            <value>Twitter</value>
        </criteriaItems>
        <description>When the post tag contains English, assign the social post to the EnglishQueue and set language to English- TWITTER</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Ukrainian posts ignore for SLA</fullName>
        <actions>
            <name>Ignore_for_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.Language__c</field>
            <operation>equals</operation>
            <value>Ukrainian</value>
        </criteriaItems>
        <description>If the Social Post language is Ukrainian set checkbox &apos;ignore for SLA&apos; to true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Unsupported logic</fullName>
        <actions>
            <name>Assign_to_Unsupported_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_language_to_Unsupported</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.PostTags</field>
            <operation>equals</operation>
            <value>Unsupported</value>
        </criteriaItems>
        <description>When the post tag contains Unsupported, assign the social post to the Unsupported Queue and set language to Unsupported</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Update company name to AF</fullName>
        <actions>
            <name>Change_company_name_to_AirFrance</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.TopicProfileName</field>
            <operation>contains</operation>
            <value>france,AF</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.TopicProfileName</field>
            <operation>notContain</operation>
            <value>KLM</value>
        </criteriaItems>
        <description>Update company name based on the topic profile name</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Update company name to KL</fullName>
        <actions>
            <name>Update_company_name_to_KL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.TopicProfileName</field>
            <operation>notContain</operation>
            <value>france,AF</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.IsOutbound</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Company__c</field>
            <operation>notContain</operation>
            <value>France</value>
        </criteriaItems>
        <description>Update company name based on the topic profile name</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Update status to Replied</fullName>
        <actions>
            <name>Update_SCS_Status_to_Replied</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.Status</field>
            <operation>equals</operation>
            <value>Replied</value>
        </criteriaItems>
        <description>When the status field equals &quot;Replied&quot; update the SCS Status field to &quot;Replied&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Update status to Sent</fullName>
        <actions>
            <name>Update_SCS_Status_to_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.Status</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <description>When the status field equals &quot;Sent&quot; update the SCS Status field to &quot;Sent&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - WeChat language to Chinese</fullName>
        <actions>
            <name>Set_language_to_Chinese</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.SCS_Social_Network__c</field>
            <operation>equals</operation>
            <value>WeChat</value>
        </criteriaItems>
        <description>When the social network is WeChat set language to Chinese.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS - WhatsApp logic</fullName>
        <actions>
            <name>Assign_to_WhatsApp_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPost.Provider</field>
            <operation>equals</operation>
            <value>WhatsApp</value>
        </criteriaItems>
        <description>Assign WhatsApp social posts to the WhatsApp queue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS SMH DeltaAssist</fullName>
        <actions>
            <name>Sends_an_email_to_SocialMediaHub_team</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>DeltaAssist</value>
        </criteriaItems>
        <description>Sends an email to SocialMediaHub team if the social post is from &quot;DetlaAssist&quot;.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SCS_Spam Facebook Username</fullName>
        <actions>
            <name>Marksocial_post_as_spam</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetIgnore_for_SLA_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetMAR_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SocialPersona.Provider</field>
            <operation>equals</operation>
            <value>Facebook</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPersona.ExternalId</field>
            <operation>equals</operation>
            <value>979126575508842,10153595968496981</value>
        </criteriaItems>
        <description>Rule built to &quot;Mark as Spam&quot; the post from the Following social handles</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS_Spam Twitter Usernames</fullName>
        <actions>
            <name>Mark_Social_Post_as_Spam</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Ignore_for_SLA_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_MAR_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14 OR 15 OR 16 OR 17 OR 18 OR 19 OR 20 OR 21 OR 22 OR 23 OR 24</booleanFilter>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>Safuan_KLm,Cupcake_klm,KLM__s,zec_klm,Eyna_KLM,KlM_KAl,xyz_klm,Sunarno_klm,BI_KlM,KlM_KAl,KLM_wao,Eyna_KLM,KlM_TAEYEON309,EXOpect_KLM</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>frmn_klm,klm_stella,exo_klm_sm,Lupop_KLM,byunseul_klm,klm_enter1,klm_pcy61,KlM_KAl,frmn_klm,Eyna_KLM,DaOne_KLM,Safuan_KLm,baseball_17_KLM,KLM_wao,katon_klm,KLM_XI,irem_klm,jas_klm,klm_013,or_klm,rabiaa_klm,kathiia_klm,hiro_klm</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>11214_KLM,weareEXO_KLM,um_exo_klm,galaxy_KLM,klm_store,KLM_Official,domanais_klm,klm__n,Lupop_KLM,4_klm,KlM__JONGlN,klm_o,12_klm,KlM_KAl,Eyna_KLM,KLM_wao,irem_klm,__klm_,KLM_Thoughts</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>EyeExoL_EXO_KLM,tukenmez_klm,Apocmn_klm,KLM_ovo,Sila_klm,for_klm,klm__n,klm_1649_10,KLM_LocalEyes</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>marthu_klm,klm_48,klm_630,eyna_klm,t_klm,klm_revolution,Marthu,ana_klm,Atgime_klm,exo12_klm,klm_id,klm_babe,klm_enter1,akh_klm,l_____klm,cupcake_klm,breathe_klm,klm_revolution,EXOfficial_KLM,@with_exo_KLM</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>KlM_HANBlN96,andamiro_with_exo_KLM,klm_exo_ ma_klm,KlM_KASPER,exofficial_klm,K_exo_klm,kanaoka_klm,Summer_klm,enzo_klm,Eyna_KLM,klm_13_,kLm_oX,klm_xoxo,LayNa_KLM,sanskrit_klm,Rizat_Klm,Nastya_klm</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Content</field>
            <operation>contains</operation>
            <value>@KLM_Official,@EXOfficial_KLM,@Eyna_KLM,EXOfficial_KLM</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>klm_gfd,klm_7R,klm_ahc,klm_0522_10,Angelique_klm,x_o_x_o_klm,klm_Architekten,klm__xX,sharpton_klm,tuba_klm,ece_klm,bsr_klm,yzr_klm,iam_klm,klm_rrr,kelly_klm_xd,exol_klm,rabiaa_klm,LAYna_KLM,SocalPlane,KLM_baby,BSQ_KLM,KLM_Official,KlM_TAEYEON309</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>KimberrlyNaomi,KlM_TAEYEON309,hiro_klm,klm_97,thodoris_klm,reizenisrael</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>efi_klm,klm_Architekten,KlM__JONGlN,klm_ahc,klm_gfd,KLM_74,klm_coffee,klm_ahc,KLM_Online,KlM_YUGYEOM,CaT_KlM,klm_jn7,eau_klm,Aren_kLm_rn,klm_hyuna,DeGrijzeDuif26,klm_almighty,KLM_cely,KIM_SEOKJIN,ZHANG_YlXlNG,KLM_1127,klm_luv,KLM_1D,KlM_JIWON,KLM_Blessed</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>KLM_Tweetline,KlM_SEOKJIN,KlM_MINGYU,klm_yeri,ohsehun_klm,klm_120408,KlM_JONGIN,Exoplanet_KLM,klm_army,KlM_JENNIE,LayNa_KLM,JONGIN_KlM,KlM_MlNSEOK,KLM_888,Jan_exo,Klm_xoxo,KLM_1980,_KLM_Kennedy,KLM_89,KLM_03,Keyshwan_KLM,Klm_stephaniee</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>Klm_97,Luv_klm,Exol_klm,Thodoris_klm,KLM_lynette,KlM_SEOKJlN,kahina_klm,1RENEKIM,xoxo_klm,19_klm,klm_97,klm_yeri,KlM_MlNGYU,klm_2014,KlM_JlWON,klm_army,Said_Klm,DrEdwardG,_akanshagautam,Cynthiapoet,DrEdwardG,drkent,exo_klm_612,0722_exo_KLM,KlM_MlNGYU</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>KLM_Air_France,_E_X_O_KLM,Miss_KLM_,t_dim,toni_klm,trhk_klm,exo1888_KLM,purine_klm,sir_klm,Tink_KLM,exo_klm_kuu,KLM_ELF31,Yvssine_klm,exo_KLM_485,sndrm_klm,klm_KATE ,Nami_Klm,EXO_S2_KLM,klm_0913,sndrm_klm ,klm_bm,klm_XXxx,klm_kh,exo_o_klm,21_klm,Tink_KLM</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>thodoris_klm,Club_KLM,abcdexo_klm,120408_KLM,Layko_klm,exo_KLM_485,exo1888_KLM,aysenur_klm,Nami_Klm,dzerkalo_klm,KlM_J0NGIN,KLM_fan,klm_333,Klm_Iua,klm_oune,yrn_klm,hacer_Klm,sddcca_KLM,klm_jenni,klm_oune,EXO_KLM_ERI,Klm_Iua,exo_sehun_klm,120408_KLM</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>exo_sehun_klm,cream_klm,exo1888_KLM,fxm_klm,klm_bambam,Layko_klm,milk_klm,Muay_klm,Tink_KLM,Vee_KLM,0722_exo_KLM,120408_KLM,MlNGYU_KlM,AlexCleary10,@KLM_blessed,@KLM_89,JONGlN_KlM,@KLM_7R,@mary_lu_exo_KLM,HEADWAYshoes,@KLM_INTL,@klm_is_a_carat</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>CHUNGHA_KlM,KlM_MlNGYU,KlM_WOOBIN,thibault_klm,KlM__JIWON,Sejeong_klm,DOYEON_KlM,ume_klm,hiro_klm,klm_ranguiba,mmsg__klm,812exo_KLM,nkm_124,EXO_KLM_7,klm_nlo,BdrechtVliegen,DOYEON_KlM,KlM_CHUNGHA,KlM_JENNIE,KlM_JISOO,klm_21_,FT_KLM,EXO_KLM_Vote,flor_klm</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>exo_klm_120408,KlM_JONG_IN,Klm_kdh,klm_one,ngs_klm_ast,KlM_YULHEE,TAEYEON_KlM,KLM_963,KRsss_klm,exo_rin_KLM,miku__klm,exo_kLm_0921,mai_klm,_____KlM___,KLM_SLG,klm_222,angie_klm,eldorado_klm,klm_rajab,Rn_klm_mtc,taw_klm,louise_klm,_EXO_KLM,melih_klm</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>kmm_klm,KLM_AJA,H_klm_1029,bay_klm,klm_knc,Fran_klm,klm__15,SIM_KLM,KLM_000,exoklm_klm</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>kimluna_klm,KlM_MlNJAE,Chloe_KLM,irem_klm_,KLM_313,KlM__khn,ParkCB_KLM,KLM_ChanB,BBH_KLM,Juni_KlM,Klm_nx,lenaki_mt,HirotakaKLM,KLM_120408_,klm_3k,XOXO__KLM,Moonlight_KLM,Alice_Klm,KLM_61,velhasl_kLm,91_klm_91,KlM_DAHYUN,klm_1231,urawa24_KLM,KlM_DOYE0N</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>klm_soda,klm_95,aaron_klm,klm__07,exo_klm_48,Klm_misha,klm_j_h,KLM_313,yurika_klm,Lumiere_klm,KLM_sss,klm_rk,KlM_DANl,3_klm,klm_prono,ozge_klm,Ozgur_Klm,zorro_klm,di_klm</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>KlM_MSL,capital_klm,exoexo_klm,KLM_720,silver_klm,kwan_klm,Klm_cxz,krbn_klm,theReal_KLM,klm_mb,Klm_m618,klm_3almy,sinem_klm,KLM_1999,klm_8o12,REDS_KLM,klm___s2,fuatt_klm,143__klm,bts_k_i_m,____klm_xoxo,May_KLM,MightyFaresEU,flight_base,Simply_KlM</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>klm_0001,DOYEON_KlM,pino_klm,KLM_900,xo_04_klm,klm____,KIM_Preekai,____klm_xoxo,KlM_WOOBIN,KlM_YERIM,deerkanggun,FlightrNet,supert_asia,Ha_Neul_KlM,we_klm,kouki_0229_klm,Sophia_Klm,Klm__2022,maitha_klm,KlM_SOHEE,GBwAZ_g_kLm,BlobWeird,suhodorable</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>KLM_365,exo_0408_klm,H_lose_klm,Klm__km_gm__edn,e__klm,KlM_YERlM,velhasl_kLm,exo_apple_klm,9011_klm,moon_klm,baekhyun_klm,klm_jun,Javi_KLM,KIMS0H,KLM__KKH​,PKW_Center_Klm,Lookpla_ta_klm,Megatama_klm,klm_ekrem,KLM___MacronPDT,melanie_klm,Edb_klm,klm_tweet</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.Handle</field>
            <operation>equals</operation>
            <value>YR__KLM,kelam_klm,gf_exo_KLM,exiLeZ_klm,exo_klm_wsbl,klm_yn,ai_klm,ekso_klm,KLM_vl​</value>
        </criteriaItems>
        <description>Rule built to &quot;Mark as Spam&quot; the tweets comming from following usernames:
@frmn_klm,@klm_stella,@exo_klm_sm,@Safuan_KLm,@Cupcake_klm,@KLM__s,@zec_klm,@Eyna_KLM,@KlM_KAl,@xyz_klm,@Sunarno_klm,@BI_KlM,</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update total new posts</fullName>
        <active>false</active>
        <criteriaItems>
            <field>SocialPost.SCS_IsOutbound__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>SocialPost.SCS_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
