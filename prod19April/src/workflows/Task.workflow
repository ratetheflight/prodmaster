<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PA_Auto_Complete</fullName>
        <description>Luuk Vermaas - 3-10-2014 - Auto Complete a logged call if due date is same is today</description>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>PA-Desk - Auto Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Task_Subject</fullName>
        <field>Subject</field>
        <formula>IF(ISPICKVAL(FB_KLM_profile__c, &apos;Rick Kleinsma&apos;), 
&apos;PMR: &apos;+ Subject, 
&apos;PMA: &apos;+ Subject)</formula>
        <name>Set Task Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PA-Desk - Auto Complete</fullName>
        <actions>
            <name>PA_Auto_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.ActivityDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Logged calls without a case can be set to complete if due date is the same day</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SCS - Set Task Subject</fullName>
        <actions>
            <name>Set_Task_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Task.FB_KLM_profile__c</field>
            <operation>equals</operation>
            <value>Rick Kleinsma</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.FB_KLM_profile__c</field>
            <operation>equals</operation>
            <value>Amber Groothuis</value>
        </criteriaItems>
        <description>Rule to set subject on Task object dedicated for &quot;Log a PM&quot; actions. Subject is automatically set to PMA:&apos;Subject&apos; / PMR: &apos;Subject&apos; based on FB KLM Profile account (Rick/Amber)</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
