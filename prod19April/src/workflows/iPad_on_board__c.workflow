<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Close_by_due_date_field_update</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Close by due date field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_by_due_date_field_update_1</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Close by due date field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_by_purser_reply_field_update</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Close by purser reply field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Integration_Status_field_update</fullName>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <name>Integration Status field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Field_update_UI</fullName>
        <field>Status_in_UI__c</field>
        <literalValue>New</literalValue>
        <name>Status Field update (UI)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_field_UI</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Status field UI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Closed</fullName>
        <field>Status_in_UI__c</field>
        <literalValue>Closed</literalValue>
        <name>Status to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iPad_record_Account_FALSE</fullName>
        <field>New_iPad_record__c</field>
        <literalValue>0</literalValue>
        <name>iPad record Account = FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>PersonAccount__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iPad_record_Account_TRUE</fullName>
        <field>New_iPad_record__c</field>
        <literalValue>1</literalValue>
        <name>iPad record Account = TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>PersonAccount__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Close by due date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>iPad_on_board__c.When_close_comment__c</field>
            <operation>equals</operation>
            <value>Fixed date</value>
        </criteriaItems>
        <description>Closes a iPad on board case automatically  when &apos;due date&apos; is passed by 1 day.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Close_by_due_date_field_update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>iPad_on_board__c.Fixed_date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Close by purser reply</fullName>
        <actions>
            <name>Close_by_purser_reply_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3)</booleanFilter>
        <criteriaItems>
            <field>iPad_on_board__c.When_close_comment__c</field>
            <operation>equals</operation>
            <value>Close by reply purser</value>
        </criteriaItems>
        <criteriaItems>
            <field>iPad_on_board__c.Purser_reply__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>iPad_on_board__c.Flight_number_reply__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Closes an iPad on board case automatically when the purser has left a comment.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>iPOB Specific and NextFlight</fullName>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>iPad_on_board__c.When_close_comment__c</field>
            <operation>equals</operation>
            <value>Valid for Specific Flight</value>
        </criteriaItems>
        <criteriaItems>
            <field>iPad_on_board__c.When_close_comment__c</field>
            <operation>equals</operation>
            <value>Valid for Next Flight</value>
        </criteriaItems>
        <description>ipob status to be Closed after 24 hours of Flight Departure Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Status_field_UI</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Status_to_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>iPad_on_board__c.Specified_Flight_Departure_Date_Time__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>iPOB_Valid for SpecificFlight</fullName>
        <active>true</active>
        <criteriaItems>
            <field>iPad_on_board__c.When_close_comment__c</field>
            <operation>equals</operation>
            <value>Valid for Specific Flight</value>
        </criteriaItems>
        <criteriaItems>
            <field>iPad_on_board__c.Status_in_UI__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Integration_Status_field_update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Status_Field_update_UI</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>iPad_on_board__c.Specified_Flight_Departure_Date_Time__c</offsetFromField>
            <timeLength>-24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>iPad record Account %3D FALSE</fullName>
        <actions>
            <name>iPad_record_Account_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>iPad_on_board__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>iPad record Account %3D TRUE</fullName>
        <actions>
            <name>iPad_record_Account_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>iPad_on_board__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
