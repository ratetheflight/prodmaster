<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Creating_Havas_Link</fullName>
        <field>Havas_Link__c</field>
        <formula>Doubleclick_Link__c&amp;URL__c&amp;Webtrends_Link__c</formula>
        <name>Creating Havas Link</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Creating Havas Link</fullName>
        <actions>
            <name>Creating_Havas_Link</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>klmf_ly__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
